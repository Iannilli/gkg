#include <gkg-anatomist-plugin-io/AOdfFieldProcessingContext.h>
#include <gkg-dmri-container/DiffusionCartesianField_i.h>
#include <gkg-processing-algobase/MinimumFilter_i.h>
#include <gkg-processing-algobase/MaximumFilter_i.h>
#include <gkg-processing-algobase/Rescaler_i.h>
#include <gkg-core-exception/Exception.h>


gkg::AOdfFieldProcessingContext::AOdfFieldProcessingContext(
                       gkg::OdfCartesianField* odfCartesianField,
                       std::vector< std::vector< float > >& transformations,
                       std::vector< gkg::Translation3d< float > >& translations,
                       float meshScale )
                                : gkg::LoopContext(),
                                  _odfCartesianField( odfCartesianField ),
                                  _transformations( transformations ),
                                  _translations( translations ),
                                  _meshScale( meshScale ),
                                  _resolution( 
                                            odfCartesianField->getResolution() )
{

  try
  {

    std::cout << "resolution=" << _resolution << std::endl;

  }
  GKG_CATCH( "gkg::AOdfFieldProcessingContext::AOdfFieldProcessingContext( "
             "gkg::OdfCartesianField* odfCartesianField, "
             "std::vector< std::vector< float > >& transformations, "
             "std::vector< gkg::Translation3d< float > >& translations, "
             "float meshScale )" );

}


gkg::AOdfFieldProcessingContext::~AOdfFieldProcessingContext()
{
}


void gkg::AOdfFieldProcessingContext::doIt( int32_t startIndex,
                                            int32_t indexCount )
{

  try
  {

    // initializing iterator(s)
    int32_t rank = _odfCartesianField->getSiteMap().getRank( 0 );
    const std::list< gkg::Vector3d< int32_t > >&
      sites = _odfCartesianField->getSiteMap().getSites( rank );

    int32_t siteCount = ( int32_t )sites.size();

    std::list< gkg::Vector3d< int32_t > >::const_iterator
      s = sites.begin(),
      se = sites.begin();
    advance( s, startIndex );
    if ( startIndex + indexCount != siteCount )
    {

      advance( se, ( startIndex + indexCount + 1 ) );

    }
    else
    {

      se = sites.end();

    }

    // preparing min max filter(s)
    gkg::MinimumFilter< std::vector< float >, float > minimumFilter;
    gkg::MaximumFilter< std::vector< float >, float > maximumFilter;

    // looping over site(s)
    int32_t index = startIndex;
    std::vector< float > orientationProbabilities;
    float minimumProbability = 0.0;
    float maximumProbability = 0.0;
    while ( s != se )
    {

      lock();
      const gkg::OrientationDistributionFunction*
        item = _odfCartesianField->getItem( *s );
      unlock();
            
      orientationProbabilities =  item->getOrientationProbabilities();

      minimumFilter.filter( orientationProbabilities, minimumProbability );
      maximumFilter.filter( orientationProbabilities, maximumProbability );
      gkg::Rescaler< std::vector< float >, std::vector< float > >
        rescaler( minimumProbability, maximumProbability,
                  0.0, _meshScale );

      rescaler.rescale( orientationProbabilities, _transformations[ index ] );

      _translations[ index ].setDirectTranslation(
                                       ( float )s->x * ( float )_resolution.x,
                                       ( float )s->y * ( float )_resolution.y,
                                       ( float )s->z * ( float )_resolution.z );

      ++ s;
      ++ index;

    }

  }
  GKG_CATCH( "void ProcessOdfContext::doIt("
             "int32_t startIndex,"
             "int32_t indexCount )" );
}
