# - Try to find the pio library
#
# Once done this will define
#
#  KOKKOS_FOUND - system has pio
#  KOKKOS_INCLUDE_DIR - the pio include directory
#  KOKKOS_LIBRARIES - the libraries needed to use pio

find_path( KOKKOS_INCLUDE_DIR Kokkos_Core.hpp
           PATHS 
           /usr/include/Kokkos
           /usr/local/include/Kokkos
	   $ENV{GKG_SVN}/Kokkos/include )

find_library( KOKKOS_LIBRARIES kokkos
              PATHS
              /usr/lib
              /usr/local/lib
              $ENV{GKG_SVN}/Kokkos/lib )

# handle the QUIETLY and REQUIRED arguments and set KOKKOS_FOUND to TRUE if
# all listed variables are TRUE
include( FindPackageHandleStandardArgs )
FIND_PACKAGE_HANDLE_STANDARD_ARGS( KOKKOS REQUIRED_VARS KOKKOS_INCLUDE_DIR KOKKOS_LIBRARIES )

if ( KOKKOS_FOUND )
message( STATUS "KOKKOS found" )
endif( KOKKOS_FOUND )

mark_as_advanced( KOKKOS_LIBRARIES KOKKOS_INCLUDE_DIR )
