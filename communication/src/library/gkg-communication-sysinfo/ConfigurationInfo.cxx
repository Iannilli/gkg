#include <gkg-communication-sysinfo/ConfigurationInfo.h>
#include <gkg-communication-sysinfo/ConfigurationInfoImplementation.h>
#include <gkg-core-cppext/StdInt.h>
#include <gkg-communication-sysinfo/Directory.h>
#include <gkg-communication-core/CommunicationImplementationFactory.h>
#include <gkg-communication-sysinfo/File.h>
#include <gkg-core-io/BaseObjectReader.h>
#include <gkg-core-io/BaseObjectWriter.h>
#include <gkg-core-object/TypedObject_i.h>
#include <gkg-core-object/Dictionary.h>
#include <gkg-core-object/GenericObject_i.h>
#include <gkg-core-cppext/UncommentCounterInputFileStream.h>
#include <gkg-core-exception/Exception.h>
#include <fstream>
#include <iostream>
#include <sstream>


gkg::ConfigurationInfo::ConfigurationInfo()
{

  _configurationInfoImplementation = 
                                    gkg::getCommunicationImplementationFactory()
                                 .createConfigurationInfoImplementation( this );

  gkg::Directory directoryGkg( getGkgPath() );
  if ( !directoryGkg.isValid() )
  {

    directoryGkg.mkdir();

  }

  gkg::Directory directoryHiddenGkg( getHiddenGkgPath() );
  if ( !directoryHiddenGkg.isValid() )
  {

    directoryHiddenGkg.mkdir();

  }

}


gkg::ConfigurationInfo::~ConfigurationInfo()
{

  delete _configurationInfoImplementation;

}


std::string gkg::ConfigurationInfo::getHomePath() const
{

  return _configurationInfoImplementation->getHomePath();

}


std::string gkg::ConfigurationInfo::getGkgVersion() const
{

  std::ostringstream os;

  os << GKG_VERSION;

  return os.str();

}


std::string gkg::ConfigurationInfo::getGkgVersionAndPatch() const
{

  std::ostringstream os;

  os << GKG_VERSION_AND_PATCH;

  return os.str();

}


std::string gkg::ConfigurationInfo::getSharePath() const
{

  return _configurationInfoImplementation->getSharePath();

}


std::string gkg::ConfigurationInfo::getHiddenGkgPath() const
{

  return getHomePath() + ".gkg" + getDirectorySeparator();

}


std::string gkg::ConfigurationInfo::getGkgPath() const
{

  if ( getSharePath() == getDirectorySeparator() )
  {

    return getHiddenGkgPath();

  }
  return getSharePath() + "gkg" + getDirectorySeparator();

}


std::string gkg::ConfigurationInfo::getGkgPrivatePath() const
{

  if ( getSharePath() == getDirectorySeparator() )
  {

    return getHiddenGkgPath();

  }
  return getSharePath() + "gkg-private" + getDirectorySeparator();

}


std::string gkg::ConfigurationInfo::getTmpPath() const
{

  return _configurationInfoImplementation->getTmpPath();

}


std::string gkg::ConfigurationInfo::getEnvPath() const
{

  return _configurationInfoImplementation->getEnvPath();

}


std::string gkg::ConfigurationInfo::getMemoryMappingPath() const
{

  return _configurationInfoImplementation->getMemoryMappingPath();

}


std::string gkg::ConfigurationInfo::getIconPath(
                                             const std::string& iconName ) const
{

  return getGkgPath() + "icons" + getDirectorySeparator() + iconName;

}


std::string gkg::ConfigurationInfo::getUiPath(
                                             const std::string& applicationName,
                                             const std::string& uiName ) const
{

  return getGkgPath() + "ui" + getDirectorySeparator() +
         applicationName + getDirectorySeparator() +
         uiName;

}


std::string gkg::ConfigurationInfo::getDataPath() const
{

  return getGkgPath() + "data" + getDirectorySeparator();

}


std::string 
gkg::ConfigurationInfo::getTemplatesPath( const std::string& specy,
                                          const std::string& fileName ) const
{

  return getDataPath() + specy + getDirectorySeparator() + "templates" +
         getDirectorySeparator() + fileName;

}


std::string gkg::ConfigurationInfo::getGkgResourceConfigFileName() const
{

  return getGkgPath() + "gkgrc";

}


std::string gkg::ConfigurationInfo::getGkgPluginConfigFileName() const
{

  return getGkgPath() + "pluginrc";

}


std::string gkg::ConfigurationInfo::getGkgPrivatePluginConfigFileName() const
{

  return getGkgPrivatePath() + "pluginrc";

}


std::string gkg::ConfigurationInfo::getGkgFunctorConfigFileName() const
{

  return getGkgPath() + "functorrc";

}


std::string gkg::ConfigurationInfo::getGkgPrivateFunctorConfigFileName() const
{

  return getGkgPrivatePath() + "functorrc";

}


std::string gkg::ConfigurationInfo::getHeaderExtensionName() const
{

  try
  {

    std::string headerExtensionName = ".minf";

    gkg::File file( gkg::ConfigurationInfo::getInstance().
                                               getGkgResourceConfigFileName() );
    if ( !file.isExisting() )
    {

      createEmptyResourceConfigFile( file );
      file.update();

    }
    if ( file.isUserReadable() )
    {

      gkg::UncommentCounterInputFileStream is( file.getName().c_str() );
      gkg::TypedObject< gkg::Dictionary > resourceDictionary;
      gkg::BaseObjectReader baseObjectReader;
      baseObjectReader.read( is, resourceDictionary );

      if ( !resourceDictionary.hasAttribute( "header_extension_name" ) )
      {

        headerExtensionName = ".minf";

      }
      else
      {
      
        resourceDictionary.getAttribute( "header_extension_name",
                                         headerExtensionName );

      }

    }
    else
    {

      throw std::runtime_error( std::string( "GKG resource config file '" ) +
                                file.getName() + "' is not readable" );

    }

    return headerExtensionName;

  }
  GKG_CATCH( "std::string "
             "gkg::ConfigurationInfo::getHeaderExtensionName() const" );

}


std::string gkg::ConfigurationInfo::getFunctionalVolumeDimension() const
{

  try
  {

    std::string functionalVolumeDimension = "4D";

    gkg::File file( gkg::ConfigurationInfo::getInstance().
                                               getGkgResourceConfigFileName() );
    if ( !file.isExisting() )
    {

      createEmptyResourceConfigFile( file );
      file.update();

    }
    if ( file.isUserReadable() )
    {

      gkg::UncommentCounterInputFileStream is( file.getName().c_str() );
      gkg::TypedObject< gkg::Dictionary > resourceDictionary;
      gkg::BaseObjectReader baseObjectReader;
      baseObjectReader.read( is, resourceDictionary );

      if ( !resourceDictionary.hasAttribute( "functional_volume_dimension" ) )
      {

        functionalVolumeDimension = "4D";

      }
      else
      {
      
        resourceDictionary.getAttribute( "functional_volume_dimension",
                                         functionalVolumeDimension );

      }

    }
    else
    {

      throw std::runtime_error( std::string( "GKG resource config file '" ) +
                                file.getName() + "' is not readable" );

    }

    return functionalVolumeDimension;

  }
  GKG_CATCH( "std::string "
             "gkg::ConfigurationInfo::getFunctionalVolumeDimension() const" );

}


std::string gkg::ConfigurationInfo::getCSVSeparator() const
{

  try
  {

    std::string csvSeparator = " ";

    gkg::File file( gkg::ConfigurationInfo::getInstance().
                                               getGkgResourceConfigFileName() );
    if ( !file.isExisting() )
    {

      createEmptyResourceConfigFile( file );
      file.update();

    }
    if ( file.isUserReadable() )
    {

      gkg::UncommentCounterInputFileStream is( file.getName().c_str() );
      gkg::TypedObject< gkg::Dictionary > resourceDictionary;
      gkg::BaseObjectReader baseObjectReader;
      baseObjectReader.read( is, resourceDictionary );

      if ( !resourceDictionary.hasAttribute( "csv_separator" ) )
      {

        csvSeparator = " ";

      }
      else
      {
      
        resourceDictionary.getAttribute( "csv_separator",
                                         csvSeparator );

      }

    }
    else
    {

      throw std::runtime_error( std::string( "GKG resource config file '" ) +
                                file.getName() + "' is not readable" );

    }

    return csvSeparator;

  }
  GKG_CATCH( "std::string "
             "gkg::ConfigurationInfo::getCSVSeparator() const" );

}


uint64_t gkg::ConfigurationInfo::getMemoryMappingPageSize() const
{

  try
  {

    uint64_t memoryMappingPageSize = 1048576U;   // = 1 MBytes

    gkg::File file( gkg::ConfigurationInfo::getInstance().
                                               getGkgResourceConfigFileName() );
    if ( !file.isExisting() )
    {

      createEmptyResourceConfigFile( file );
      file.update();

    }
    if ( file.isUserReadable() )
    {

      gkg::UncommentCounterInputFileStream is( file.getName().c_str() );
      gkg::TypedObject< gkg::Dictionary > resourceDictionary;
      gkg::BaseObjectReader baseObjectReader;
      baseObjectReader.read( is, resourceDictionary );

      if ( !resourceDictionary.hasAttribute( "memory_mapping_page_size" ) )
      {

        memoryMappingPageSize = 1048576U;

      }
      else
      {

        double memory_mapping_page_size = 0.0;
        resourceDictionary.getAttribute( "memory_mapping_page_size",
                                         memory_mapping_page_size );
        memoryMappingPageSize = ( uint64_t )( memory_mapping_page_size + 0.5 );

      }

    }
    else
    {

      throw std::runtime_error( std::string( "GKG resource config file '" ) +
                                file.getName() + "' is not readable" );

    }

    return memoryMappingPageSize;

  }
  GKG_CATCH( "uint64_t "
             "gkg::ConfigurationInfo::getMemoryMappingPageSize() const" );

}


double gkg::ConfigurationInfo::getMemoryMappingSwitchOnFactor() const
{

  try
  {

    // memory mapping is switched on when 90% of the free RAM is already used
    double memoryMappingSwitchOnFactor = 0.9;

    gkg::File file( gkg::ConfigurationInfo::getInstance().
                                               getGkgResourceConfigFileName() );
    if ( !file.isExisting() )
    {

      createEmptyResourceConfigFile( file );
      file.update();

    }
    if ( file.isUserReadable() )
    {

      gkg::UncommentCounterInputFileStream is( file.getName().c_str() );
      gkg::TypedObject< gkg::Dictionary > resourceDictionary;
      gkg::BaseObjectReader baseObjectReader;
      baseObjectReader.read( is, resourceDictionary );

      if ( !resourceDictionary.hasAttribute(
                                           "memory_mapping_switch_on_factor" ) )
      {

        memoryMappingSwitchOnFactor = 0.9;

      }
      else
      {

        resourceDictionary.getAttribute( "memory_mapping_switch_on_factor",
                                         memoryMappingSwitchOnFactor );

      }

    }
    else
    {

      throw std::runtime_error( std::string( "GKG resource config file '" ) +
                                file.getName() + "' is not readable" );

    }

    return memoryMappingSwitchOnFactor;

  }
  GKG_CATCH( "double "
             "gkg::ConfigurationInfo::getMemoryMappingSwitchOnFactor() const" );

}


bool gkg::ConfigurationInfo::isMemoryMappingVerbose() const
{

  try
  {

    bool memoryMappingVerbosity = false;

    gkg::File file( gkg::ConfigurationInfo::getInstance().
                                               getGkgResourceConfigFileName() );
    if ( !file.isExisting() )
    {

      createEmptyResourceConfigFile( file );
      file.update();

    }
    if ( file.isUserReadable() )
    {

      gkg::UncommentCounterInputFileStream is( file.getName().c_str() );
      gkg::TypedObject< gkg::Dictionary > resourceDictionary;
      gkg::BaseObjectReader baseObjectReader;
      baseObjectReader.read( is, resourceDictionary );

      if ( !resourceDictionary.hasAttribute( "memory_mapping_verbosity" ) )
      {

        memoryMappingVerbosity = false;

      }
      else
      {

        double memory_mapping_verbosity = 0.0;
        resourceDictionary.getAttribute( "memory_mapping_verbosity",
                                         memory_mapping_verbosity );
        memoryMappingVerbosity = ( memory_mapping_verbosity > 0.0 ? true :
                                                                    false );

      }

    }
    else
    {

      throw std::runtime_error( std::string( "GKG resource config file '" ) +
                                file.getName() + "' is not readable" );

    }

    return memoryMappingVerbosity;

  }
  GKG_CATCH( "bool "
             "gkg::ConfigurationInfo::getMemoryMappingVerbosity() const" );

}


bool gkg::ConfigurationInfo::isMemoryMappingDisabled() const
{

  try
  {

    bool memoryMappingDisabled = false;

    gkg::File file( gkg::ConfigurationInfo::getInstance().
                                               getGkgResourceConfigFileName() );
    if ( !file.isExisting() )
    {

      createEmptyResourceConfigFile( file );
      file.update();

    }
    if ( file.isUserReadable() )
    {

      gkg::UncommentCounterInputFileStream is( file.getName().c_str() );
      gkg::TypedObject< gkg::Dictionary > resourceDictionary;
      gkg::BaseObjectReader baseObjectReader;
      baseObjectReader.read( is, resourceDictionary );

      if ( !resourceDictionary.hasAttribute( "memory_mapping_disabled" ) )
      {

        memoryMappingDisabled = false;

      }
      else
      {

        double memory_mapping_disabled = 0.0;
        resourceDictionary.getAttribute( "memory_mapping_disabled",
                                         memory_mapping_disabled );
        memoryMappingDisabled = ( memory_mapping_disabled > 0.0 ? true :
                                                                  false );

      }

    }
    else
    {

      throw std::runtime_error( std::string( "GKG resource config file '" ) +
                                file.getName() + "' is not readable" );

    }

    return memoryMappingDisabled;

  }
  GKG_CATCH( "bool "
             "gkg::ConfigurationInfo::getMemoryMappingVerbosity() const" );

}


int32_t gkg::ConfigurationInfo::getNumberOfCpuToUse() const
{

  try
  {

    int32_t numberOfCpuToUse = 0;

    gkg::File file( gkg::ConfigurationInfo::getInstance().
                                               getGkgResourceConfigFileName() );
    if ( !file.isExisting() )
    {

      createEmptyResourceConfigFile( file );
      file.update();

    }
    if ( file.isUserReadable() )
    {

      gkg::UncommentCounterInputFileStream is( file.getName().c_str() );
      gkg::TypedObject< gkg::Dictionary > resourceDictionary;
      gkg::BaseObjectReader baseObjectReader;
      baseObjectReader.read( is, resourceDictionary );

      if ( !resourceDictionary.hasAttribute( "number_of_cpu_to_use" ) )
      {

        numberOfCpuToUse = 0;

      }
      else
      {

        double number_of_cpu_to_use = 0.0;
        resourceDictionary.getAttribute( "number_of_cpu_to_use", 
                                         number_of_cpu_to_use );
        numberOfCpuToUse = (int32_t)( number_of_cpu_to_use + 0.5 );

      }

    }
    else
    {

      throw std::runtime_error( std::string( "GKG resource config file '" ) +
                                file.getName() + "' is not readable" );

    }

    return numberOfCpuToUse;

  }
  GKG_CATCH( "int32_t gkg::ConfigurationInfo::getNumberOfCpuToUse() const" );

}


void gkg::ConfigurationInfo::setAnonymousName( bool value )
{

  _configurationInfoImplementation->setAnonymousName( value );

}


bool gkg::ConfigurationInfo::hasAnonymousName() const
{

  return _configurationInfoImplementation->hasAnonymousName();

}


void gkg::ConfigurationInfo::createEmptyResourceConfigFile(
                                                   const gkg::File& file ) const
{

  std::ofstream os( file.getName().c_str() );

  os << "attributes = {" << std::endl
     << "  'header_extension_name' : '.minf'" << std::endl
     << "}" << std::endl;

  os.close();

}


gkg::ConfigurationInfoImplementation*
gkg::ConfigurationInfo::getImplementation() const
{

  return _configurationInfoImplementation;

}
