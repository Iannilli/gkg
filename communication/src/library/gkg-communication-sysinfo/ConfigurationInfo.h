#ifndef _gkg_communication_sysinfo_ConfigurationInfo_h_
#define _gkg_communication_sysinfo_ConfigurationInfo_h_


#include <gkg-core-pattern/Singleton.h>
#include <gkg-core-cppext/StdInt.h>
#include <string>


namespace gkg
{


class ConfigurationInfoImplementation;
class File;

class ConfigurationInfo : public Singleton< ConfigurationInfo >
{

  public:

    ~ConfigurationInfo();

    std::string getHomePath() const;
    std::string getGkgVersion() const;
    std::string getGkgVersionAndPatch() const;
    std::string getSharePath() const;
    std::string getHiddenGkgPath() const;
    std::string getGkgPath() const;
    std::string getGkgPrivatePath() const;
    std::string getTmpPath() const;
    std::string getEnvPath() const;
    std::string getMemoryMappingPath() const;
    std::string getIconPath( const std::string& iconName ) const;
    std::string getUiPath( const std::string& applicationName,
                           const std::string& uiName ) const;
    std::string getDataPath() const;
    std::string getTemplatesPath( const std::string& specy,
                                  const std::string& fileName ) const;
    std::string getGkgResourceConfigFileName() const;
    std::string getGkgPluginConfigFileName() const;
    std::string getGkgPrivatePluginConfigFileName() const;
    std::string getGkgFunctorConfigFileName() const;
    std::string getGkgPrivateFunctorConfigFileName() const;

    std::string getHeaderExtensionName() const;
    std::string getFunctionalVolumeDimension() const;
    std::string getCSVSeparator() const;

    uint64_t getMemoryMappingPageSize() const;
    double getMemoryMappingSwitchOnFactor() const;
    bool isMemoryMappingVerbose() const;
    bool isMemoryMappingDisabled() const;

    int32_t getNumberOfCpuToUse() const;

    void setAnonymousName( bool value );
    bool hasAnonymousName() const;

    ConfigurationInfoImplementation* getImplementation() const;

  protected:

    friend class Singleton< ConfigurationInfo >;

    ConfigurationInfo();

    void createEmptyResourceConfigFile( const File& file ) const;

    ConfigurationInfoImplementation* _configurationInfoImplementation;

};


// this function is used to get the separator pattern for the paths list
// and should be implemented in the concrete library
// (in libgkg-communication-X11.so for instance)
std::string getEnvironmentPathSeparator();


}


#endif

