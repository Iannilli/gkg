#include <gkg-communication-sysinfo/Timer.h>
#include <gkg-core-exception/Exception.h>
#include <iostream>


gkg::Timer::Timer()
{
}


gkg::Timer::~Timer()
{
}


void gkg::Timer::storeTime()
{

  try
  {

    _times.push_back( clock() );

  }
  GKG_CATCH( "void gkg::Timer::storeTime()" );

}


void gkg::Timer::printTimesInMilliseconds() const
{

  try
  {

    std::list< clock_t >::const_iterator 
      t = _times.begin(),
      te = _times.end();

    while ( t != te )
    {

      std::cout << ( double )*t / (CLOCKS_PER_SEC / 1000) << " " << std::flush;
      ++ t;

    }

  }
  GKG_CATCH( "void gkg::Timer::printTimesInMilliseconds() const" );

}


void gkg::Timer::printLapsInMilliseconds() const
{

  try
  {

    std::list< clock_t >::const_iterator 
      t1 = _times.begin(),
      te = _times.end();

    std::list< clock_t >::const_iterator t2 = t1;
    ++ t2;

    while ( t2 != te )
    {

      std::cout << getClockDifferenceInMilliseconds( *t1, *t2 )
                << " " << std::flush;
      ++ t1;
      ++ t2;

    }

  }
  GKG_CATCH( "void gkg::Timer::printLapsInMilliseconds() const" );

}


double gkg::Timer::getClockDifferenceInMilliseconds( clock_t clock1, 
                                                     clock_t clock2 ) const
{

  try
  {

    double differenceTicks = clock2 - clock1;
    double differenceInMilliseconds = ( differenceTicks ) /
                                      ( CLOCKS_PER_SEC / 1000 );
    return differenceInMilliseconds;

  }
  GKG_CATCH( "double gkg::Timer::getClockDifferenceInMilliseconds( "
             "clock_t clock1, clock_t clock2 ) const" );

}
