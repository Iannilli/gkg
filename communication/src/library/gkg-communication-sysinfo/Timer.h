#ifndef _gkg_communication_sysinfo_Timer_h_
#define _gkg_communication_sysinfo_Timer_h_


#include <ctime>
#include <list>


namespace gkg
{

class Timer
{

  public:

    Timer();
    virtual ~Timer();

    void storeTime();

    void printTimesInMilliseconds() const;
    void printLapsInMilliseconds() const;

  private:

    double getClockDifferenceInMilliseconds( clock_t clock1, 
                                             clock_t clock2 ) const;

    std::list< clock_t > _times;

};


}


#endif
