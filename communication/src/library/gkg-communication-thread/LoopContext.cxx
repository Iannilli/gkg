#include <gkg-communication-thread/LoopContext.h>
#include <gkg-communication-thread/ThreadGauge.h>
#include <gkg-communication-thread/ThreadCancel.h>


gkg::LoopContext::LoopContext( gkg::ThreadGauge* gauge,
                               gkg::ThreadCancel* cancel )
                 : gkg::ExclusiveContext(),
                   _gauge( gauge ),
                   _cancel( cancel )
{
}


gkg::LoopContext::~LoopContext()
{
}


void gkg::LoopContext::gaugeAdd( int32_t value )
{

  if ( _gauge )
  {

    _gauge->add( value );

  }

}


bool gkg::LoopContext::cancel()
{

  if ( _cancel )
  {

    return _cancel->check();

  }
  return false;

}


void gkg::LoopContext::resetGauge()
{

  if ( _gauge )
  {

    _gauge->reset();

  }

}


void gkg::LoopContext::resetCancel()
{

  if ( _cancel )
  {

    _cancel->reset();

  }

}
