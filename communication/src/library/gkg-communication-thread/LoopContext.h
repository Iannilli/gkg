#ifndef _gkg_communication_thread_LoopContext_h_
#define _gkg_communication_thread_LoopContext_h_


#include <gkg-communication-thread/ExclusiveContext.h>
#include <gkg-core-cppext/StdInt.h>


namespace gkg
{


class ThreadGauge;
class ThreadCancel;


class LoopContext : public ExclusiveContext
{

  public:

    LoopContext( ThreadGauge* gauge = 0, ThreadCancel* cancel = 0 );
    virtual ~LoopContext();

    virtual void doIt( int32_t startIndex, int32_t countIndex ) = 0;
    virtual void gaugeAdd( int32_t value );
    virtual bool cancel();
    virtual void resetGauge();
    virtual void resetCancel();

  protected:

    ThreadGauge* _gauge;
    ThreadCancel* _cancel;

};


}


#endif
