#ifndef _gkg_communication_thread_LoopHelper_h_
#define _gkg_communication_thread_LoopHelper_h_


#include <gkg-core-cppext/StdInt.h>


namespace gkg
{


class ThreadedLoop;


class LoopHelper
{

  public:

    LoopHelper();
    virtual ~LoopHelper();

    virtual void launch( ThreadedLoop* threadedLoop,
                         int32_t threadCount,
                         int32_t grain,
                         int32_t count ) = 0;
    virtual void join() = 0;

};


}


#endif
