#include <gkg-communication-thread/LoopTask.h>
#include <gkg-communication-thread/ThreadedLoop.h>


gkg::LoopTask::LoopTask( gkg::ThreadedLoop* parent )
             : gkg::Task(),
               _parent( parent )
{
}


void gkg::LoopTask::run()
{

  if ( _parent )
  {

    _parent->run();

  }

}
