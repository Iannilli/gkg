#ifndef _gkg_communication_thread_LoopTask_h_
#define _gkg_communication_thread_LoopTask_h_


#include <gkg-communication-thread/Task.h>


namespace gkg
{


class ThreadedLoop;


class LoopTask : public Task
{

  public:

    LoopTask( ThreadedLoop* parent );

    void run();

  protected:

    ThreadedLoop* _parent;


};


}


#endif
