#include <gkg-communication-thread/LoopThread.h>
#include <gkg-communication-thread/ThreadedLoop.h>
#include <gkg-communication-sysinfo/SystemInfo.h>
#include <iostream>


gkg::LoopThread::LoopThread( gkg::ThreadedLoop* parent )
                : gkg::Thread(),
                  _parent( parent )
{

  if ( _parent )
  {

    setVerbose( parent->isVerbose() );

  }

  if ( _verbose )
  {

    std::cerr << "gkg::LoopThread::LoopThread() ["
              << gkg::SystemInfo::getInstance().getCurrentThreadId()
              << "]" << std::endl;

  }

}


gkg::LoopThread::~LoopThread()
{

  if ( _verbose )
  {

    std::cerr << "gkg::LoopThread::~LoopThread() ["
              << gkg::SystemInfo::getInstance().getCurrentThreadId()
              << "]" << std::endl;

  }

}


void gkg::LoopThread::doRun()
{

  if ( _parent )
  {

    _parent->run();

  }

}
