#ifndef _gkg_communication_thread_LoopThread_h_
#define _gkg_communication_thread_LoopThread_h_


#include <gkg-communication-thread/Thread.h>


namespace gkg
{


class ThreadedLoop;


class LoopThread : public Thread
{

  public:

    LoopThread( ThreadedLoop* parent );
    virtual ~LoopThread();

  protected:

    virtual void doRun();

    ThreadedLoop* _parent;

};


}


#endif
