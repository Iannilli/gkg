#ifndef _gkg_communication_thread_ThreadGauge_h_
#define _gkg_communication_thread_ThreadGauge_h_


#include <gkg-core-cppext/StdInt.h>


namespace gkg
{


class ThreadGauge
{

  public:

    ThreadGauge();
    virtual ~ThreadGauge();

    virtual void add( int32_t );
    virtual void reset();

};


}


#endif
