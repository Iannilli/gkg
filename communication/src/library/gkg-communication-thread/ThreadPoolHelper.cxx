#include <gkg-communication-thread/ThreadPoolHelper.h>
#include <gkg-communication-thread/ThreadedLoop.h>
#include <gkg-communication-thread/LoopTask.h>
#include <gkg-communication-thread/ThreadPool.h>


gkg::ThreadPoolHelper::ThreadPoolHelper()
{
}


void gkg::ThreadPoolHelper::launch( gkg::ThreadedLoop* threadedLoop,
                                    int32_t threadCount,
                                    int32_t grain,
                                    int32_t count )
{

  if ( threadedLoop )
  {

    int32_t n;

    for ( n = 0; ( n < threadCount ) && ( n * grain < count ); n++ )
    {

      _taskList.push_back( new gkg::LoopTask( threadedLoop ) );

    }

  }

}


void gkg::ThreadPoolHelper::join()
{

  gkg::ThreadPool::getInstance().launch( _taskList );

  _taskList.clear();

}
