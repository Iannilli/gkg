#ifndef _gkg_communication_thread_ThreadPoolHelper_h_
#define _gkg_communication_thread_ThreadPoolHelper_h_


#include <gkg-communication-thread/LoopHelper.h>

#include <list>


namespace gkg
{


class Task;


class ThreadPoolHelper : public LoopHelper
{

  public:

    ThreadPoolHelper();

    void launch( ThreadedLoop* threadedLoop,
                 int32_t threadCount,
                 int32_t grain,
                 int32_t count );
    void join();

  private:

    std::list< Task* > _taskList;

};


}


#endif
