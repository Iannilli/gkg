#include <gkg-communication-thread/ThreadedLoop.h>
#include <gkg-communication-thread/LoopContext.h>
//#include <gkg-communication-thread/ThreadedLoopHelper.h>
#include <gkg-communication-thread/ThreadPoolHelper.h>
#include <gkg-communication-sysinfo/SystemInfo.h>

#include <iostream>


gkg::ThreadedLoop::ThreadedLoop( gkg::LoopContext* loopContext,
                                 int32_t startIndex, 
                                 int32_t count, 
                                 int32_t maxThreadCount,
                                 int32_t threadsByCpu,
                                 bool verbose )

                  : _loopContext( loopContext ),
                    _grain( 0 ),
                    _startIndex( startIndex ),
                    _count( count ),
                    _maxThreadCount( maxThreadCount ),
                    _threadsByCpu( threadsByCpu ),
                    _currentIndex( startIndex ),
                    _todo( count ),
                    _remain( 0 ),
                    _verbose( verbose )
{

  //_loopHelper = new gkg::ThreadedLoopHelper();
  _loopHelper = new gkg::ThreadPoolHelper();

}


gkg::ThreadedLoop::~ThreadedLoop()
{

  delete _loopHelper;

}


void gkg::ThreadedLoop::setVerbose( bool value )
{

  _verbose = value;

}


bool gkg::ThreadedLoop::isVerbose() const
{

  return _verbose;

}


void gkg::ThreadedLoop::run()
{

  if ( _loopContext )
  {

    _loopContext->lock();

    if ( ( _todo <= 0 ) || ( _grain < 1 ) || ( _loopContext->cancel() ) )
    {

      _loopContext->unlock();

      if ( _loopContext->cancel() && _verbose )
      {

        std::cerr << "=======================> aborted by cancel" << std::endl;

      }

      if ( _verbose )
      {

        std::cerr << "  ThreadedLoop::run() return immediatly" << std::endl;

      }

    }
    else
    {

      int32_t startIndex = _currentIndex;
      int32_t count = _grain;

      _todo -= _grain;

      if ( _remain )
      {

        count++;
        _todo--;
        _remain--;

      }

      _currentIndex += count;

      if ( _verbose )
      {

        std::cerr << "  ThreadedLoop::run() "
                  << "{ start=" << startIndex << ", count=" << count << " } "
                  << "[" << gkg::SystemInfo::getInstance().getCurrentThreadId()
                  << "]" << std::endl;

      }

      _loopContext->unlock();

      if ( _verbose )
      {

        std::cerr << "  ThreadedLoop::run() succeeded" << std::endl;

      }

      _loopContext->doIt( startIndex, count );

    }

  }

}


bool gkg::ThreadedLoop::launch( bool resetGauge, bool resetCancel )
{

  if ( !_loopContext )
  {

    return false;

  }

  bool result = true;
  int32_t nCpu = gkg::SystemInfo::getInstance().getCpuCount() - 1;

  if ( nCpu < 1 )
  {

    nCpu = 1;

  }

  if ( ( _maxThreadCount >= 1 ) && ( _maxThreadCount < nCpu ) )
  {

    nCpu = _maxThreadCount;

  }

  int32_t threadCount = nCpu * _threadsByCpu;

  if ( _verbose )
  {

    std::cerr << "CPU count     : " << nCpu << std::endl;
    std::cerr << "thread by CPU : " << _threadsByCpu << std::endl;

  }

  _loopContext->unlock();
  _loopContext->lock();

  _currentIndex = _startIndex;
  _todo = _count;
  _grain = ( _count <= threadCount ) ? 1 : _count / threadCount;
  _remain = ( _count <= threadCount ) ? 0 : _count % threadCount;

  _loopHelper->launch( this, threadCount, _grain, _count );
  _loopContext->unlock();
  _loopHelper->join();

  if ( _loopContext->cancel() )
  {

    result = false;

  }

  if ( resetGauge )
  {

    _loopContext->resetGauge();

  }

  if ( resetCancel )
  {

    _loopContext->resetCancel();

  }

  return result;

}
