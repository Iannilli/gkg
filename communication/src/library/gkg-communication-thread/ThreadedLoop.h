#ifndef _gkg_communication_thread_ThreadedLoop_h_
#define _gkg_communication_thread_ThreadedLoop_h_


#include <gkg-communication-thread/LoopHelper.h>
#include <gkg-core-cppext/StdInt.h>


namespace gkg
{


class LoopContext;


class ThreadedLoop
{

  public:

    ThreadedLoop( LoopContext* loopContext,
                  int32_t startIndex, int32_t count,
                  int32_t maxThreadCount = 0,
                  int32_t threadsByCpu = 1,
                  bool verbose = false );
    virtual ~ThreadedLoop();

    void setVerbose( bool value );
    bool isVerbose() const;

    void run();

    bool launch( bool resetGauge = true, bool resetCancel = true );

  protected:

    LoopContext* _loopContext;
    LoopHelper* _loopHelper;

    int32_t _grain;
    int32_t _startIndex;
    int32_t _count;
    int32_t _maxThreadCount;
    int32_t _threadsByCpu;

    int32_t _currentIndex;
    int32_t _todo;
    int32_t _remain;

    bool _verbose;

};


}


#endif
