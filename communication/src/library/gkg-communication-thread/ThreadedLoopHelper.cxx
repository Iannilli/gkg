#include <gkg-communication-thread/ThreadedLoopHelper.h>
#include <gkg-communication-thread/ThreadedLoop.h>
#include <gkg-communication-thread/LoopThread.h>


gkg::ThreadedLoopHelper::ThreadedLoopHelper()
{
}


void gkg::ThreadedLoopHelper::launch( gkg::ThreadedLoop* threadedLoop,
                                      int32_t threadCount,
                                      int32_t grain,
                                      int32_t count )
{

  if ( threadedLoop )
  {

    int32_t n;

    for ( n = 0; ( n < threadCount ) && ( n * grain < count ); n++ )
    {

      gkg::LoopThread* loopThread = new gkg::LoopThread( threadedLoop );

      _threadList.push_back( loopThread );
      loopThread->launch();

    }

  }

}


void gkg::ThreadedLoopHelper::join()
{

  std::list< gkg::LoopThread* >::iterator
    t = _threadList.begin(),
    te = _threadList.end();

  while ( t != te )
  {

    (*t)->join();
    delete *t;
    ++t;

  }

  _threadList.clear();

}
