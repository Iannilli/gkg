#ifndef _gkg_communication_thread_ThreadedLoopHelper_h_
#define _gkg_communication_thread_ThreadedLoopHelper_h_


#include <gkg-communication-thread/LoopHelper.h>

#include <list>


namespace gkg
{


class LoopThread;


class ThreadedLoopHelper : public LoopHelper
{

  public:

    ThreadedLoopHelper();

    void launch( ThreadedLoop* threadedLoop,
                 int32_t threadCount,
                 int32_t grain,
                 int32_t count );
    void join();

  private:

    std::list< LoopThread* > _threadList;

};


}


#endif
