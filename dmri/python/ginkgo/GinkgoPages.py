from DwiDataImportAndQSpaceSamplingAlgorithm import *
from DwiDataImportAndQSpaceSamplingAlgorithmGUI import *
from DwiDataImportAndQSpaceSamplingViewer import *

from DwiToAnatomyMatchingAlgorithm import *
from DwiToAnatomyMatchingAlgorithmGUI import *
from DwiToAnatomyMatchingViewer import *

from DwiRoughMaskExtractionAlgorithm import *
from DwiRoughMaskExtractionAlgorithmGUI import *
from DwiRoughMaskExtractionViewer import *

from DwiOutlierDetectionAlgorithm import *
from DwiOutlierDetectionAlgorithmGUI import *
from DwiOutlierDetectionViewer import *

from DwiSusceptibilityArtifactCorrectionAlgorithm import *
from DwiSusceptibilityArtifactCorrectionAlgorithmGUI import *
from DwiSusceptibilityArtifactCorrectionViewer import *

from DwiEddyCurrentAndMotionCorrectionAlgorithm import *
from DwiEddyCurrentAndMotionCorrectionAlgorithmGUI import *
from DwiEddyCurrentAndMotionCorrectionViewer import *

from DwiQualityCheckReportingAlgorithm import *
from DwiQualityCheckReportingAlgorithmGUI import *
from DwiQualityCheckReportingViewer import *

from DwiLocalModelingAlgorithm import *
from DwiLocalModelingAlgorithmGUI import *
from DwiLocalModelingViewer import *

from DwiTractographyMaskAlgorithm import *
from DwiTractographyMaskAlgorithmGUI import *
from DwiTractographyMaskViewer import *

from DwiTractographyAlgorithm import *
from DwiTractographyAlgorithmGUI import *
from DwiTractographyViewer import *

from DwiBundleFilteringAlgorithm import *
from DwiBundleFilteringAlgorithmGUI import *
from DwiBundleFilteringViewer import *

from DwiBundleStatisticsAlgorithm import *
from DwiBundleStatisticsAlgorithmGUI import *
from DwiBundleStatisticsViewer import *

from DwiConnectivityMatrixAlgorithm import *
from DwiConnectivityMatrixAlgorithmGUI import *
from DwiConnectivityMatrixViewer import *

#from DwiIntraSubjectFiberClusteringAlgorithm import *
#from DwiIntraSubjectFiberClusteringAlgorithmGUI import *
#from DwiIntraSubjectFiberClusteringViewer import *

#from DwiInterSubjectFiberClusteringAlgorithm import *
#from DwiInterSubjectFiberClusteringAlgorithmGUI import *
#from DwiInterSubjectFiberClusteringViewer import *

from DwiLongWhiteMatterBundleLabelingAlgorithm import *
from DwiLongWhiteMatterBundleLabelingAlgorithmGUI import *
from DwiLongWhiteMatterBundleLabelingViewer import *

from DwiShortWhiteMatterBundleLabelingAlgorithm import *
from DwiShortWhiteMatterBundleLabelingAlgorithmGUI import *
from DwiShortWhiteMatterBundleLabelingViewer import *

from DwiDataConvertionToDtiTkAlgorithm import *
from DwiDataConvertionToDtiTkAlgorithmGUI import *
from DwiDataConvertionToDtiTkViewer import *

from DwiDtiTemplateConstructionAlgorithm import *
from DwiDtiTemplateConstructionAlgorithmGUI import *
from DwiDtiTemplateConstructionViewer import *

from DwiSubjectRegistrationToTemplateAlgorithm import *
from DwiSubjectRegistrationToTemplateAlgorithmGUI import *
from DwiSubjectRegistrationToTemplateViewer import *

from DwiSubjectResamplingToTemplateAlgorithm import *
from DwiSubjectResamplingToTemplateAlgorithmGUI import *
from DwiSubjectResamplingToTemplateViewer import *

from DwiAtlasCreationAlgorithm import *
from DwiAtlasCreationAlgorithmGUI import *
from DwiAtlasCreationViewer import *

from DwiPosterAlgorithm import *
from DwiPosterAlgorithmGUI import *
from DwiPosterViewer import *

from DwiTrackDensityImagingAlgorithm import *
from DwiTrackDensityImagingAlgorithmGUI import *
from DwiTrackDensityImagingViewer import *

from DwiGeometrySimulatorAlgorithm import *
from DwiGeometrySimulatorAlgorithmGUI import *
from DwiGeometrySimulatorViewer import *

from DwiMonteCarloSimulatorAlgorithm import *
from DwiMonteCarloSimulatorAlgorithmGUI import *
from DwiMonteCarloSimulatorViewer import *

from DwiMRISynthesisAlgorithm import *
from DwiMRISynthesisAlgorithmGUI import *
from DwiMRISynthesisViewer import *

from DwiBlockDataImportAndQSpaceSamplingAlgorithm import *
from DwiBlockDataImportAndQSpaceSamplingAlgorithmGUI import *
from DwiBlockDataImportAndQSpaceSamplingViewer import *

from DwiBlockFilteringAlgorithm import *
from DwiBlockFilteringAlgorithmGUI import *
from DwiBlockFilteringViewer import *

from DwiBlockT2MultipleRegistrationAlgorithm import *
from DwiBlockT2MultipleRegistrationAlgorithmGUI import *
from DwiBlockT2MultipleRegistrationViewer import *

from DwiBlockRoughMaskExtractionAlgorithm import *
from DwiBlockRoughMaskExtractionAlgorithmGUI import *
from DwiBlockRoughMaskExtractionViewer import *

from DwiBlockPdfFieldAlgorithm import *
from DwiBlockPdfFieldAlgorithmGUI import *
from DwiBlockPdfFieldViewer import *

from DwiBlockDtiModelingAlgorithm import *
from DwiBlockDtiModelingAlgorithmGUI import *
from DwiBlockDtiModelingViewer import *


def getPages():

    return \
      [ { 'Import and QC': [
          { 'algorithm': DwiDataImportAndQSpaceSamplingAlgorithm,
            'algorithmGUI': DwiDataImportAndQSpaceSamplingAlgorithmGUI,
            'createViewer': createDwiDataImportAndQSpaceSamplingViewer,
            'tabName' : 'DWI && Q-space',
            'pageName' : 'DWI-Data-Import-And-QSpace-Sampling' },
          { 'algorithm': DwiToAnatomyMatchingAlgorithm,
            'algorithmGUI': DwiToAnatomyMatchingAlgorithmGUI,
            'createViewer': createDwiToAnatomyMatchingViewer,
            'tabName' : 'Anatomy && Talairach',
            'pageName' : 'DWI-To-Anatomy-Matching' },
          { 'algorithm': DwiRoughMaskExtractionAlgorithm,
            'algorithmGUI': DwiRoughMaskExtractionAlgorithmGUI,
            'createViewer': createDwiRoughMaskExtractionViewer,
            'tabName' : 'Rough mask',
            'pageName' : 'DWI-Rough-Mask-Extraction' },
          { 'algorithm': DwiOutlierDetectionAlgorithm,
            'algorithmGUI': DwiOutlierDetectionAlgorithmGUI,
            'createViewer': createDwiOutlierDetectionViewer,
            'tabName' : 'Outliers',
            'pageName' : 'DWI-Outlier-Detection' },
          { 'algorithm': DwiSusceptibilityArtifactCorrectionAlgorithm,
            'algorithmGUI': DwiSusceptibilityArtifactCorrectionAlgorithmGUI,
            'createViewer': createDwiSusceptibilityArtifactCorrectionViewer,
            'tabName' : 'Susceptibility',
            'pageName' : 'DWI-Susceptibility-Artifact-Correction' },
          { 'algorithm': DwiEddyCurrentAndMotionCorrectionAlgorithm,
            'algorithmGUI': DwiEddyCurrentAndMotionCorrectionAlgorithmGUI,
            'createViewer': createDwiEddyCurrentAndMotionCorrectionViewer,
            'tabName' : 'Eddy current && motion',
            'pageName' : 'DWI-Eddy-Current-And-Motion-Correction' },
          { 'algorithm': DwiQualityCheckReportingAlgorithm,
            'algorithmGUI': DwiQualityCheckReportingAlgorithmGUI,
            'createViewer': createDwiQualityCheckReportingViewer,
            'tabName' : 'QC Reporting',
            'pageName' : 'DWI-Quality-Check-Reporting' } ] },
        { 'Tractography': [
          { 'algorithm': DwiLocalModelingAlgorithm,
            'algorithmGUI': DwiLocalModelingAlgorithmGUI,
            'createViewer': createDwiLocalModelingViewer,
            'tabName' : 'Local Modeling',
            'pageName' : 'DWI-Local-Modeling' },
          { 'algorithm': DwiTractographyMaskAlgorithm,
            'algorithmGUI': DwiTractographyMaskAlgorithmGUI,
            'createViewer': createDwiTractographyMaskViewer,
            'tabName' : 'Tractography Mask',
            'pageName' : 'DWI-Tractography-Mask' },
          { 'algorithm': DwiTractographyAlgorithm,
            'algorithmGUI': DwiTractographyAlgorithmGUI,
            'createViewer': createDwiTractographyViewer,
            'tabName' : 'Tractography',
            'pageName' : 'DWI-Tractography' },
          { 'algorithm': DwiBundleFilteringAlgorithm,
            'algorithmGUI': DwiBundleFilteringAlgorithmGUI,
            'createViewer': createDwiBundleFilteringViewer,
            'tabName' : 'Bundle Filtering',
            'pageName' : 'DWI-Bundle-Filtering' },
          { 'algorithm': DwiTrackDensityImagingAlgorithm,
            'algorithmGUI': DwiTrackDensityImagingAlgorithmGUI,
            'createViewer': createDwiTrackDensityImagingViewer,
            'tabName' : 'Track Density Imaging',
            'pageName' : 'DWI-Track-Density-Imaging' } ] },
        { 'Connectomics': [
          { 'algorithm': DwiConnectivityMatrixAlgorithm,
            'algorithmGUI': DwiConnectivityMatrixAlgorithmGUI,
            'createViewer': createDwiConnectivityMatrixViewer,
            'tabName' : 'Connectivity Matrix',
            'pageName' : 'DWI-Connectivity-Matrix' } ] },
#        { 'Fiber clustering': [
#          { 'algorithm': DwiIntraSubjectFiberClusteringAlgorithm,
#            'algorithmGUI': DwiIntraSubjectFiberClusteringAlgorithmGUI,
#            'createViewer': createDwiIntraSubjectFiberClusteringViewer,
#            'tabName' : 'Intra-subject Fiber Clustering',
#            'pageName' : 'DWI-IntraSubject-Fiber-Clustering' },
#          { 'algorithm': DwiInterSubjectFiberClusteringAlgorithm,
#            'algorithmGUI': DwiInterSubjectFiberClusteringAlgorithmGUI,
#            'createViewer': createDwiInterSubjectFiberClusteringViewer,
#            'tabName' : 'Inter-subject Fiber Clustering',
#            'pageName' : 'DWI-InterSubject-Fiber-Clustering' },
        { 'Fiber clustering': [
          { 'algorithm': DwiLongWhiteMatterBundleLabelingAlgorithm,
            'algorithmGUI': DwiLongWhiteMatterBundleLabelingAlgorithmGUI,
            'createViewer': createDwiLongWhiteMatterBundleLabelingViewer,
            'tabName' : 'Long White Matter Bundle Labeling',
            'pageName' : 'DWI-Long-White-Matter-Bundle-Labeling' },
          { 'algorithm': DwiShortWhiteMatterBundleLabelingAlgorithm,
            'algorithmGUI': DwiShortWhiteMatterBundleLabelingAlgorithmGUI,
            'createViewer': createDwiShortWhiteMatterBundleLabelingViewer,
            'tabName' : 'Short White Matter Bundle Labeling',
            'pageName' : 'DWI-Short-White-Matter-Bundle-Labeling' } ] },
        { 'Group analysis': [
          { 'algorithm': DwiBundleStatisticsAlgorithm,
            'algorithmGUI': DwiBundleStatisticsAlgorithmGUI,
            'createViewer': createDwiBundleStatisticsViewer,
            'tabName' : 'Bundle Statistics',
            'pageName' : 'DWI-Bundle-Statistics' } ] },
        { 'Atlasing': [
          { 'algorithm': DwiDataConvertionToDtiTkAlgorithm,
            'algorithmGUI': DwiDataConvertionToDtiTkAlgorithmGUI,
            'createViewer': createDwiDataConvertionToDtiTkViewer,
            'tabName' : 'DTI-TK convertion',
            'pageName' : 'DWI-Data-Convertion-To-DtiTk' },
          { 'algorithm': DwiDtiTemplateConstructionAlgorithm,
            'algorithmGUI': DwiDtiTemplateConstructionAlgorithmGUI,
            'createViewer': createDwiDtiTemplateConstructionViewer,
            'tabName' : 'DTI Template Construction',
            'pageName' : 'DWI-Dti-Template-Construction' },
          { 'algorithm': DwiSubjectRegistrationToTemplateAlgorithm,
            'algorithmGUI': DwiSubjectRegistrationToTemplateAlgorithmGUI,
            'createViewer': createDwiSubjectRegistrationToTemplateViewer,
            'tabName' : 'Subject registration to template',
            'pageName' : 'DWI-Subject-Registration-To-Template' },
          { 'algorithm': DwiSubjectResamplingToTemplateAlgorithm,
            'algorithmGUI': DwiSubjectResamplingToTemplateAlgorithmGUI,
            'createViewer': createDwiSubjectResamplingToTemplateViewer,
            'tabName' : 'Subject resampling to template',
            'pageName' : 'DWI-Subject-Resampling-To-Template' },
          { 'algorithm': DwiAtlasCreationAlgorithm,
            'algorithmGUI': DwiAtlasCreationAlgorithmGUI,
            'createViewer': createDwiAtlasCreationViewer,
            'tabName' : 'Atlas creation',
            'pageName' : 'DWI-Atlas-Creation' } ] },
        { 'Reporting': [
          { 'algorithm': DwiPosterAlgorithm,
            'algorithmGUI': DwiPosterAlgorithmGUI,
            'createViewer': createDwiPosterViewer,
            'tabName' : 'Poster',
            'pageName' : 'DWI-Poster' } ] },
        { 'Monte-Carlo Simulator' : [
          { 'algorithm': DwiGeometrySimulatorAlgorithm,
            'algorithmGUI': DwiGeometrySimulatorAlgorithmGUI,
            'createViewer': createDwiGeometrySimulatorViewer,
            'tabName' : 'Geometry Simulator',
            'pageName' : 'DWI-Geometry-Simulator' },
          { 'algorithm': DwiMonteCarloSimulatorAlgorithm,
            'algorithmGUI': DwiMonteCarloSimulatorAlgorithmGUI,
            'createViewer': createDwiMonteCarloSimulatorViewer,
            'tabName' : 'Monte Carlo Simulator',
            'pageName' : 'DWI-Monte-Carlo-Simulator' },
          { 'algorithm': DwiMRISynthesisAlgorithm,
            'algorithmGUI': DwiMRISynthesisAlgorithmGUI,
            'createViewer': createDwiMRISynthesisViewer,
            'tabName' : 'MR Image Synthesis',
            'pageName' : 'DWI-MRImage-Synthesis' } ] },
        { 'Chenonceau Block Processing' : [
          { 'algorithm': DwiBlockDataImportAndQSpaceSamplingAlgorithm,
            'algorithmGUI': DwiBlockDataImportAndQSpaceSamplingAlgorithmGUI,
            'createViewer': createDwiBlockDataImportAndQSpaceSamplingViewer,
            'tabName' : 'Data Import And QSpace Sampling',
            'pageName' : 'DWI-Block-Data-Import-And-QSpace-Sampling' },
          { 'algorithm': DwiBlockT2MultipleRegistrationAlgorithm,
            'algorithmGUI': DwiBlockT2MultipleRegistrationAlgorithmGUI,
            'createViewer': createDwiBlockT2MultipleRegistrationViewer,
            'tabName' : 'T2 Registration',
            'pageName' : 'DWI-Block-T2-Multiple-Registration' },
          { 'algorithm': DwiBlockFilteringAlgorithm,
            'algorithmGUI': DwiBlockFilteringAlgorithmGUI,
            'createViewer': createDwiBlockFilteringViewer,
            'tabName' : 'Filtering',
            'pageName' : 'DWI-Block-Filtering' },
          { 'algorithm': DwiBlockRoughMaskExtractionAlgorithm,
            'algorithmGUI': DwiBlockRoughMaskExtractionAlgorithmGUI,
            'createViewer': createDwiRoughMaskExtractionViewer,
            'tabName' : 'Rough Mask',
            'pageName' : 'DWI-Block-Rough-Mask-Extraction' },
          { 'algorithm': DwiBlockPdfFieldAlgorithm,
            'algorithmGUI': DwiBlockPdfFieldAlgorithmGUI,
            'createViewer': createDwiBlockPdfFieldViewer,
            'tabName' : 'PDF Field',
            'pageName' : 'DWI-Block-Pdf-Field' },
          { 'algorithm': DwiBlockDtiModelingAlgorithm,
            'algorithmGUI': DwiBlockDtiModelingAlgorithmGUI,
            'createViewer': createDwiBlockDtiModelingViewer,
            'tabName' : 'DTI Modeling',
            'pageName' : 'DWI-Block-Dti-Modeling' } ] }

      ]

