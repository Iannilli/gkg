from DwiDataImportAndQSpaceSamplingAlgorithm import *
from DwiDataImportAndQSpaceSamplingAlgorithmGUI import *
from DwiDataImportAndQSpaceSamplingViewer import *

from DwiToAnatomyMatchingAlgorithm import *
from DwiToAnatomyMatchingAlgorithmGUI import *
from DwiToAnatomyMatchingViewer import *

from DwiRoughMaskExtractionAlgorithm import *
from DwiRoughMaskExtractionAlgorithmGUI import *
from DwiRoughMaskExtractionViewer import *

from DwiOutlierDetectionAlgorithm import *
from DwiOutlierDetectionAlgorithmGUI import *
from DwiOutlierDetectionViewer import *

from DwiSusceptibilityArtifactCorrectionAlgorithm import *
from DwiSusceptibilityArtifactCorrectionAlgorithmGUI import *
from DwiSusceptibilityArtifactCorrectionViewer import *

from DwiEddyCurrentAndMotionCorrectionAlgorithm import *
from DwiEddyCurrentAndMotionCorrectionAlgorithmGUI import *
from DwiEddyCurrentAndMotionCorrectionViewer import *

from DwiQualityCheckReportingAlgorithm import *
from DwiQualityCheckReportingAlgorithmGUI import *
from DwiQualityCheckReportingViewer import *

from DwiLocalModelingAlgorithm import *
from DwiLocalModelingAlgorithmGUI import *
from DwiLocalModelingViewer import *

from DwiTractographyMaskAlgorithm import *
from DwiTractographyMaskAlgorithmGUI import *
from DwiTractographyMaskViewer import *

from DwiTractographyAlgorithm import *
from DwiTractographyAlgorithmGUI import *
from DwiTractographyViewer import *

from DwiBundleFilteringAlgorithm import *
from DwiBundleFilteringAlgorithmGUI import *
from DwiBundleFilteringViewer import *

from DwiBundleStatisticsAlgorithm import *
from DwiBundleStatisticsAlgorithmGUI import *
from DwiBundleStatisticsViewer import *

from DwiConnectivityMatrixAlgorithm import *
from DwiConnectivityMatrixAlgorithmGUI import *
from DwiConnectivityMatrixViewer import *

from DwiLongWhiteMatterBundleLabelingAlgorithm import *
from DwiLongWhiteMatterBundleLabelingAlgorithmGUI import *
from DwiLongWhiteMatterBundleLabelingViewer import *

from DwiShortWhiteMatterBundleLabelingAlgorithm import *
from DwiShortWhiteMatterBundleLabelingAlgorithmGUI import *
from DwiShortWhiteMatterBundleLabelingViewer import *



def getPages():

    return \
      [ { 'Import and QC': [
          { 'algorithm': DwiDataImportAndQSpaceSamplingAlgorithm,
            'algorithmGUI': DwiDataImportAndQSpaceSamplingAlgorithmGUI,
            'createViewer': createDwiDataImportAndQSpaceSamplingViewer,
            'tabName' : 'DWI && Q-space',
            'pageName' : 'DWI-Data-Import-And-QSpace-Sampling' },
          { 'algorithm': DwiToAnatomyMatchingAlgorithm,
            'algorithmGUI': DwiToAnatomyMatchingAlgorithmGUI,
            'createViewer': createDwiToAnatomyMatchingViewer,
            'tabName' : 'Anatomy && Talairach',
            'pageName' : 'DWI-To-Anatomy-Matching' },
          { 'algorithm': DwiRoughMaskExtractionAlgorithm,
            'algorithmGUI': DwiRoughMaskExtractionAlgorithmGUI,
            'createViewer': createDwiRoughMaskExtractionViewer,
            'tabName' : 'Rough mask',
            'pageName' : 'DWI-Rough-Mask-Extraction' },
          { 'algorithm': DwiOutlierDetectionAlgorithm,
            'algorithmGUI': DwiOutlierDetectionAlgorithmGUI,
            'createViewer': createDwiOutlierDetectionViewer,
            'tabName' : 'Outliers',
            'pageName' : 'DWI-Outlier-Detection' },
          { 'algorithm': DwiSusceptibilityArtifactCorrectionAlgorithm,
            'algorithmGUI': DwiSusceptibilityArtifactCorrectionAlgorithmGUI,
            'createViewer': createDwiSusceptibilityArtifactCorrectionViewer,
            'tabName' : 'Susceptibility',
            'pageName' : 'DWI-Susceptibility-Artifact-Correction' },
          { 'algorithm': DwiEddyCurrentAndMotionCorrectionAlgorithm,
            'algorithmGUI': DwiEddyCurrentAndMotionCorrectionAlgorithmGUI,
            'createViewer': createDwiEddyCurrentAndMotionCorrectionViewer,
            'tabName' : 'Eddy current && motion',
            'pageName' : 'DWI-Eddy-Current-And-Motion-Correction' },
          { 'algorithm': DwiQualityCheckReportingAlgorithm,
            'algorithmGUI': DwiQualityCheckReportingAlgorithmGUI,
            'createViewer': createDwiQualityCheckReportingViewer,
            'tabName' : 'QC Reporting',
            'pageName' : 'DWI-Quality-Check-Reporting' } ] },
        { 'Tractography': [
          { 'algorithm': DwiLocalModelingAlgorithm,
            'algorithmGUI': DwiLocalModelingAlgorithmGUI,
            'createViewer': createDwiLocalModelingViewer,
            'tabName' : 'Local Modeling',
            'pageName' : 'DWI-Local-Modeling' },
          { 'algorithm': DwiTractographyMaskAlgorithm,
            'algorithmGUI': DwiTractographyMaskAlgorithmGUI,
            'createViewer': createDwiTractographyMaskViewer,
            'tabName' : 'Tractography Mask',
            'pageName' : 'DWI-Tractography-Mask' },
          { 'algorithm': DwiTractographyAlgorithm,
            'algorithmGUI': DwiTractographyAlgorithmGUI,
            'createViewer': createDwiTractographyViewer,
            'tabName' : 'Tractography',
            'pageName' : 'DWI-Tractography' },
          { 'algorithm': DwiBundleFilteringAlgorithm,
            'algorithmGUI': DwiBundleFilteringAlgorithmGUI,
            'createViewer': createDwiBundleFilteringViewer,
            'tabName' : 'Bundle Filtering',
            'pageName' : 'DWI-Bundle-Filtering' } ] },
        { 'Connectomics': [
          { 'algorithm': DwiConnectivityMatrixAlgorithm,
            'algorithmGUI': DwiConnectivityMatrixAlgorithmGUI,
            'createViewer': createDwiConnectivityMatrixViewer,
            'tabName' : 'Connectivity Matrix',
            'pageName' : 'DWI-Connectivity-Matrix' } ] },
        { 'Fiber clustering': [
          { 'algorithm': DwiLongWhiteMatterBundleLabelingAlgorithm,
            'algorithmGUI': DwiLongWhiteMatterBundleLabelingAlgorithmGUI,
            'createViewer': createDwiLongWhiteMatterBundleLabelingViewer,
            'tabName' : 'Long White Matter Bundle Labeling',
            'pageName' : 'DWI-Long-White-Matter-Bundle-Labeling' },
          { 'algorithm': DwiShortWhiteMatterBundleLabelingAlgorithm,
            'algorithmGUI': DwiShortWhiteMatterBundleLabelingAlgorithmGUI,
            'createViewer': createDwiShortWhiteMatterBundleLabelingViewer,
            'tabName' : 'Short White Matter Bundle Labeling',
            'pageName' : 'DWI-Short-White-Matter-Bundle-Labeling' } ] },
        { 'Group analysis': [
          { 'algorithm': DwiBundleStatisticsAlgorithm,
            'algorithmGUI': DwiBundleStatisticsAlgorithmGUI,
            'createViewer': createDwiBundleStatisticsViewer,
            'tabName' : 'Bundle Statistics',
            'pageName' : 'DWI-Bundle-Statistics' } ] }
      ]
