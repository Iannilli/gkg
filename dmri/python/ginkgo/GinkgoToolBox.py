from ToolBox import *


class GinkgoToolBox( ToolBox ):

  def __init__( self,
                pages,
                batchMode,
                distributionSettings,
                verbose = False,
                parent = None ):

    ToolBox.__init__( \
             self,
             'Ginkgo',
             pages,
             '<b>Ginkgo</b> toolbox was developed using GKG and '
             'BrainVISA/pyAnatomist toolkits.<br>' +
             'It was developed, under the supervision of Cyril & Fabrice ' +
             'Poupon, by the following members of the NeuroSpin department \n' +
             '(CEA, Fundamental Research Division,' +
             ' Frederic Joliot Life Sciences Institute): <br>' +
             '- Justine Beaujoin<br>' +
             '- Veronique Brion<br>' +
             '- Delphine Duclap<br>' +
             '- Kevin Ginsburger<br>' +
             '- Pamela Guevara<br>' +
             '- Alice Lebois<br>' +
             '- Linda Marrakchi<br>' +
             '- Alexandros Popov<br>' +
             '- Olivier Riff<br>' +
             '- Benoit Schmitt<br>' +
             '- Achille Teillac<br>' +
             '- Cyrille Turpin<br>' +
             '- Ivy Uszynski<br>' +
             '- Raissa Yebga Hot<br>' +
             '- Chun-Hung Yeh<br>.',
             batchMode,
             distributionSettings,
             verbose,
             parent )
 
