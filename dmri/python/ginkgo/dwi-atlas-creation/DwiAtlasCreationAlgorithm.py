from Algorithm import *
from DwiAtlasCreationTask import *


class DwiAtlasCreationAlgorithm( Algorithm ):

  def __init__( self, verbose ):

    Algorithm.__init__( self, 'DWI-Atlas-Creation', verbose, False )

    # input directory names
    self.addParameter( StringParameter( 'inputDirectoryNames', '' ) )


  def launch( self ):

    if ( self._verbose ):

      print \
        'running atlas creation'

    task = DwiAtlasCreationTask( self._application )
    task.launch( False )


  def view( self ):

    if ( self._verbose ):

      print \
        'viewing atlas creation'

    task = DwiAtlasCreationTask( self._application )
    task.launch( True )

