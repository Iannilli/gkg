from AlgorithmGUI import *
from ResourceManager import *


class DwiAtlasCreationAlgorithmGUI( AlgorithmGUI ):

  def __init__( self, algorithm ):

    AlgorithmGUI.__init__( self,
                           algorithm,
                           ResourceManager().getUIFileName( \
                                                       'dmri',
                                                       'DwiAtlasCreation.ui' ) )

    ###########################################################################
    # connecting input data
    ###########################################################################

    # input data
    self.connectStringParameterToLineEdit( 'inputDirectoryNames',
                                           'lineEdit_InputDirectoryNames' )
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit( \
                                            'pushButton_InputDirectoryNames',
                                            'lineEdit_InputDirectoryNames' )


