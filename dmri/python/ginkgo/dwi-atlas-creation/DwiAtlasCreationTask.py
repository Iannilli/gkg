from Task import *
from VolumeInformationHandler import *
import os
import shutil

class DwiAtlasCreationTask( Task ):

  def __init__( self, application ):

    Task.__init__( self, application )

  ##############################################################################
  # execute()
  ##############################################################################

  def execute( self, functors, parameters, subjectName, verbose, viewOnly ):

    try:
      ####################### acquiring conditions #############################
      functors[ 'condition-acquire' ]( subjectName, 'atlas-processed' )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 0 )

      ####################### collecting output work directory #################
      outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()
      if( verbose ):

        print  'Output work directory :', outputWorkDirectory

      ####################### collecting input data filename(s) ################
      inputDirectoryNames = \
                          parameters[ 'inputDirectoryNames' ].getValue().split()

      inputScalarAtlasFileNames = {}
      inputRgbComponentAtlasFileNames = {}

      outputScalarAtlasFileName = {}
      outputRgbComponentAtlasFileNames = {}
      outputRgbAtlasFileName = {}

      for d in inputDirectoryNames:
      
        fileNames = os.listdir( d )

        for f in fileNames:
        
          if ( re.match( '.*_resampled.ima', f ) and
               not re.match( '.*_resampled.ima.minf', f ) ):
          
            baseName = f[ 0: -14 ]
            fileName = os.path.join( d, f )

            itemType = getItemTypeOfVolume( fileName )
            
            if ( itemType != 'gkg_RGBComponent' ):

              if baseName in inputScalarAtlasFileNames:
 
                inputScalarAtlasFileNames[ baseName ] += ' ' + fileName

              else:
 
                inputScalarAtlasFileNames[ baseName ] = fileName
                outputScalarAtlasFileName[ baseName ] = \
                                             os.path.join( outputWorkDirectory,
                                                           'atlas_' + baseName )
 
            elif ( itemType == 'gkg_RGBComponent' ):

              fileNameR = os.path.join( d, baseName + '_resampled_r.ima' )
              fileNameG = os.path.join( d, baseName + '_resampled_g.ima' )
              fileNameB = os.path.join( d, baseName + '_resampled_b.ima' )
              if baseName in inputRgbComponentAtlasFileNames:
 
                inputRgbComponentAtlasFileNames[ baseName ][ 0 ] += \
                                                                 ' ' + fileNameR
                inputRgbComponentAtlasFileNames[ baseName ][ 1 ] += \
                                                                 ' ' + fileNameG
                inputRgbComponentAtlasFileNames[ baseName ][ 2 ] += \
                                                                 ' ' + fileNameB
 
              else:
 
                inputRgbComponentAtlasFileNames[ baseName ] = [ fileNameR,
                                                                fileNameG,
                                                                fileNameB ]

                outputRgbComponentAtlasFileNames[ baseName ] = \
                                  [ os.path.join( outputWorkDirectory,
                                                  'atlas_' + baseName + '_r' ),
                                    os.path.join( outputWorkDirectory,
                                                  'atlas_' + baseName + '_g' ),
                                    os.path.join( outputWorkDirectory,
                                                  'atlas_' + baseName + '_b' ) ]
                                    
                outputRgbAtlasFileName[ baseName ] = \
                                             os.path.join( outputWorkDirectory,
                                                           'atlas_' + baseName )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 20 )


      ################################ processing scalar maps ##################

      count = 1
      scalarAtlasCount = len( inputScalarAtlasFileNames.keys() )
      for name in inputScalarAtlasFileNames.keys():
      
        fileNameCount = len( inputScalarAtlasFileNames[ name ].split() )
        
        stringNumerator1 = ''
        stringDenominator1 = ''
        for f in range( fileNameCount ):

          stringNumerator1 += '1.0 '
          stringDenominator1 += str( fileNameCount ) + ' '

        stringNumerator1 += '0.0'
        stringDenominator1 += str( fileNameCount )
        
        command = 'GkgExecuteCommand Combiner ' + \
                  ' -i ' + inputScalarAtlasFileNames[ name ] + \
                  ' -o ' + outputScalarAtlasFileName[ name ] + \
                  ' -num1 ' + stringNumerator1 + \
                  ' -den1 ' + stringDenominator1 + \
                  ' -verbose true'
        executeCommand( self, subjectName, command, viewOnly )

        functors[ 'update-progress-bar' ]( subjectName,
                                           20 + 40 * count / scalarAtlasCount )

        count += 1

      ################################ processing RGB maps #####################

      count = 1
      rgbAtlasCount = len( inputRgbComponentAtlasFileNames.keys() )
      for name in inputRgbComponentAtlasFileNames.keys():
      
        fileNameCount = \
                     len( inputRgbComponentAtlasFileNames[ name ][ 0 ].split() )
        
        stringNumerator1 = ''
        stringDenominator1 = ''
        for f in range( fileNameCount ):

          stringNumerator1 += '1.0 '
          stringDenominator1 += str( fileNameCount ) + ' '

        stringNumerator1 += '0.0'
        stringDenominator1 += str( fileNameCount )

        for component in range( 3 ):
        
          command = 'GkgExecuteCommand Combiner ' + \
                    ' -i ' + inputRgbComponentAtlasFileNames[ name ][ \
                                                                 component ] + \
                    ' -o ' + outputRgbComponentAtlasFileNames[ name ][ \
                                                                 component ] + \
                    ' -num1 ' + stringNumerator1 + \
                    ' -den1 ' + stringDenominator1 + \
                    ' -verbose true'
          executeCommand( self, subjectName, command, viewOnly )

        command = 'AimsMerge2Rgb ' + \
                  ' -r ' + outputRgbComponentAtlasFileNames[ name ][ 0 ] + \
                  ' -g ' + outputRgbComponentAtlasFileNames[ name ][ 1 ] + \
                  ' -b ' + outputRgbComponentAtlasFileNames[ name ][ 2 ] + \
                  ' -o ' + outputRgbAtlasFileName[ name ] + '.ima'
        executeCommand( self, subjectName, command, viewOnly )

        functors[ 'update-progress-bar' ]( subjectName,
                                           60 + 40 * count / rgbAtlasCount )
        count += 1


      ######################### notifying end of process #######################
      functors[ 'condition-notify-and-release' ]( subjectName,
                                                  'atlas-processed' )

      ########################## updating progress bar #########################
      functors[ 'update-progress-bar' ]( subjectName, 100 )

    except:

      self._functors[ 'task-set-status-as-failed' ]( subjectName,
                                                     sys.exc_info() )

  ##############################################################################
  # display()
  ##############################################################################

  def display( self, functors, parameters, subjectName, verbose,
               isReportModeActive ):

    ####################### collecting output work directory ###################
    outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()
    if ( verbose ):

      print  'Output work directory :', outputWorkDirectory
      

    ######################## waiting for result ################################
    functors[ 'condition-wait-and-release' ]( subjectName, 'atlas-processed' )

      
    ####################### collecting atlas file names ########################

    fileNames = os.listdir( outputWorkDirectory )
    atlasCount = 0
    fileIndex = 0
    while ( fileIndex < len( fileNames ) ) and ( atlasCount < 4 ):
    
      if ( re.match( 'atlas_.*.ima', fileNames[ fileIndex ] ) and
           not re.match( 'atlas_.*.ima.minf', fileNames[ fileIndex ] )and
           not re.match( 'atlas_.*_r.ima', fileNames[ fileIndex ] )and
           not re.match( 'atlas_.*_g.ima', fileNames[ fileIndex ] )and
           not re.match( 'atlas_.*_b.ima', fileNames[ fileIndex ] ) ):

        baseName = fileNames[ fileIndex ][ 6 : -4 ]
        
        fileNameAtlas = os.path.join( outputWorkDirectory,
                                      fileNames[ fileIndex ] )
        
        functors[ 'viewer-load-object' ]( fileNameAtlas,
                                          'atlas-' + baseName )
        functors[ 'viewer-add-object-to-window' ]( \
                                              'atlas-' + baseName,
                                              'first_view',
                                              'Atlas ' + str( atlasCount + 1 ) )
        atlasCount += 1

      fileIndex += 1

