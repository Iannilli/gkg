from Viewer import *


class DwiAtlasCreationViewer( Viewer ):

  def __init__( self, minimumSize, parent ):

    Viewer.__init__( self, minimumSize, parent )

    ########################## First View ######################################

    self.createView( 'first_view' )

    # input data window
    self.addAxialWindow( 'first_view', 0, 0, 1, 1, 'Atlas 1', 'Atlas 1' )
    self.addAxialWindow( 'first_view', 0, 1, 1, 1, 'Atlas 2', 'Atlas 2' )
    self.addAxialWindow( 'first_view', 1, 0, 1, 1, 'Atlas 3', 'Atlas 3' )
    self.addAxialWindow( 'first_view', 1, 1, 1, 1, 'Atlas 4', 'Atlas 4' )


def createDwiAtlasCreationViewer( minimumSize, parent ):

  return DwiAtlasCreationViewer( minimumSize, parent )

