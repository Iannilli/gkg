from Algorithm import *
from DwiBlockDataImportAndQSpaceSamplingTask import *


class DwiBlockDataImportAndQSpaceSamplingAlgorithm( Algorithm ):

  def __init__( self, verbose ):

    Algorithm.__init__( self, 'DWI-Block-Data-Import-And-QSpace-Sampling',
                        verbose,
                        True )

    # block Definition
    self.addParameter( StringParameter( 'rawDataDirectory', '' ) )
    self.addParameter( StringParameter( 'blockDirectory', '' ) )
    self.addParameter( BooleanParameter( 'leftHemisphere', True ) ) 
    self.addParameter( StringParameter( 'blockLetter', '' ) )
    self.addParameter( IntegerParameter( 'fovNumber', \
                                          1, 1, 4, 1 ) )
                               
    # Sequences Number

    self.addParameter( IntegerParameter ('numAnat150u', -1, -1, 100, 1))
    self.addParameter( IntegerParameter ('numAnat100u', -1, -1, 100, 1))
    self.addParameter( IntegerParameter ('numDiffb1500', 0, 0, 100, 1))
    self.addParameter( IntegerParameter ('numDiffb4500', 0, 0, 100, 1))
    self.addParameter( IntegerParameter ('numDiffb8000', 0, 0, 100, 1))


  def launch( self ):

    if ( self._verbose ):

      print \
        'running Block Data import and Q-space sampling'

    task = DwiBlockDataImportAndQSpaceSamplingTask( self._application )
    task.launch( False )


  def view( self ):

    if ( self._verbose ):

      print \
        'viewing Block Data import and Q-space sampling'

    task = DwiBlockDataImportAndQSpaceSamplingTask( self._application )
    task.launch( True )

