from AlgorithmGUI import *
from ResourceManager import *


class DwiBlockDataImportAndQSpaceSamplingAlgorithmGUI( AlgorithmGUI ):

  def __init__( self, algorithm ):

    AlgorithmGUI.__init__( self,
                           algorithm,
                           ResourceManager().getUIFileName(
                                          'dmri',
                                          'DwiBlockDataImportAndQSpaceSampling.ui' ) )

    ############################################################################
    # connecting Block & FOV Interface
    ############################################################################
    self.connectStringParameterToLineEdit( 'rawDataDirectory',
                                           'lineEdit_RawDataDirectory' )    
    self.connectDirectoryBrowserToPushButtonAndLineEdit( \
                                                   'pushButton_RawDataDirectory',
                                                   'lineEdit_RawDataDirectory' )
    self.connectStringParameterToLineEdit( 'blockDirectory',
                                           'lineEdit_BlockDirectory' )    
    self.connectDirectoryBrowserToPushButtonAndLineEdit( \
                                                   'pushButton_BlockDirectory',
                                                   'lineEdit_BlockDirectory' )
    self.connectBooleanParameterToRadioButton( \
                                         'leftHemisphere',
                                         'radioButton_LeftHemisphere' )
    self.connectStringParameterToLineEdit( 'blockLetter',
                                           'lineEdit_BlockLetter' )    
    self.connectIntegerParameterToSpinBox( \
                                      'fovNumber',
                                      'spinBox_FovNumber' )

    ############################################################################
    # connecting Sequences number
    ############################################################################

    self.connectIntegerParameterToSpinBox('numAnat150u','spinBox_Anat150u')
    self.connectIntegerParameterToSpinBox('numAnat100u','spinBox_Anat100u')

    self.connectIntegerParameterToSpinBox('numDiffb1500',\
      'spinBox_Diffb1500')
    self.connectIntegerParameterToSpinBox('numDiffb4500',\
      'spinBox_Diffb4500')
    self.connectIntegerParameterToSpinBox('numDiffb8000',\
      'spinBox_Diffb8000')
