from Task import *
import shutil
import os

class DwiBlockDataImportAndQSpaceSamplingTask( Task ):

  def __init__( self, application ):

    Task.__init__( self, application )


  ##############################################################################
  # execute()
  ##############################################################################

  def execute( self, functors, parameters, subjectName, verbose, viewOnly ):

    try:

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 0 )

      ####################### collecting output work directory #################
      rawDataDirectory = parameters[ 'rawDataDirectory'].getValue()
      fovDirectory = self.generateFovPath(parameters)

      if ( verbose ):
      
        print('fov Directory : ' + fovDirectory)

      ########################### creating all files ###########################
      self.createOutputDirectories(fovDirectory)
      self.moveRawData(rawDataDirectory,fovDirectory)

      
      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 5 )


      ################### IMA Conversion Anat images  #########################
      numAnat150u = parameters[ 'numAnat150u' ].getValue()
      numAnat100u = parameters[ 'numAnat100u' ].getValue()
      self.convertAnatDicomToIma(rawDataDirectory,fovDirectory,\
        {'150u':numAnat150u,'100u':numAnat100u},subjectName,viewOnly)
      
      ################### IMA Conversion Diff images  #########################
      numDiffb1500 = parameters[ 'numDiffb1500' ].getValue()
      numDiffb4500 = parameters[ 'numDiffb4500' ].getValue()
      numDiffb8000 = parameters[ 'numDiffb8000' ].getValue()
      self.convertDiffDicomToIma( rawDataDirectory, fovDirectory,dwBValue=1500,
          firstAcq=numDiffb1500,
          lastAcq=numDiffb1500+1,
          subjectName=subjectName,
          viewOnly=viewOnly )
      self.convertDiffDicomToIma( rawDataDirectory, fovDirectory,dwBValue=4500,
          firstAcq=numDiffb4500,
          lastAcq=numDiffb4500+4,
          subjectName=subjectName,
          viewOnly=viewOnly)
      self.convertDiffDicomToIma( rawDataDirectory, fovDirectory,dwBValue=8000,
          firstAcq=numDiffb8000,
          lastAcq=numDiffb8000+8,
          subjectName=subjectName,
          viewOnly=viewOnly )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 25 )

      ################ move acqp files & method files  ########################
      self.moveImasAcpsAndMethodsInfiles( fovDirectory )

      ################### apply 10K combiner  ##########################
      self.applyCombiner10K( fovDirectory , subjectName, viewOnly)

      ###################### reading all input volumes ########################
      dwiCount, \
      listOfSizeX, \
      listOfSizeY, \
      listOfSizeZ, \
      listOfSizeT, \
      listOfResolutionX, \
      listOfResolutionY, \
      listOfResolutionZ, \
      fileNameDWIs = self.readDWI( parameters,
                                   functors,
                                   fovDirectory,
                                   subjectName,
                                   verbose,
                                   viewOnly )

      ####################### determining slice & phase axis ###################
      sliceAxis = 'z'
      phaseAxis = 'y'

      ####################### determining size and resolution ##################
      sizeX = listOfSizeX[ 0 ]
      sizeY = listOfSizeY[ 0 ]
      sizeZ = listOfSizeZ[ 0 ]
      resolutionX = listOfResolutionX[ 0 ]
      resolutionY = listOfResolutionY[ 0 ]
      resolutionZ = listOfResolutionZ[ 0 ]

      ####################### strogin information to a dictionary ##############
      acquisitionParameters = {}
      acquisitionParameters[ 'manufacturer' ] = 'Brucker BioSpin'
      acquisitionParameters[ 'sliceAxis' ] = sliceAxis
      acquisitionParameters[ 'phaseAxis' ] = phaseAxis
      acquisitionParameters[ 'sizeX' ] = sizeX
      acquisitionParameters[ 'sizeY' ] = sizeY
      acquisitionParameters[ 'sizeZ' ] = sizeZ
      acquisitionParameters[ 'resolutionX' ] = resolutionX
      acquisitionParameters[ 'resolutionY' ] = resolutionY
      acquisitionParameters[ 'resolutionZ' ] = resolutionZ

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 50 )


      ########################### Creating  ########################

      self.createEveryT2AndDW(fovDirectory,verbose,subjectName,viewOnly)

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 90 )

      ########################## creating raw RGB map ##########################
      self.createRGB( fovDirectory,
                      subjectName,
                      verbose,
                      viewOnly )      

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 100 )

      
    except:

      self._functors[ 'task-set-status-as-failed' ]( subjectName,
                                                     sys.exc_info() )


  def rescaleWithSlopeAndIntercept(self,outputWorkDirectory,inputFileNameDWI,n,
    subjectName,viewOnly):
    floatInputFileNameDWI = os.path.join( outputWorkDirectory, 
                                          'dwi_float_' + str( n + 1 ) )
    command = 'GkgExecuteCommand RescalerWithSlopeAndIntercept' + \
                ' -i ' + inputFileNameDWI + \
                ' -o ' + floatInputFileNameDWI + \
                ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )

    return floatInputFileNameDWI

  def readDwiSanityCheck(self,currentFileNameDWI):

    isFileExisting = False
    for fileName in os.listdir( os.path.dirname( currentFileNameDWI ) ):

      if( os.path.splitext( os.path.basename( fileName ) )[ 0 ] == \
            os.path.splitext( os.path.basename( currentFileNameDWI ) )[ 0 ] ):

        isFileExisting = True

    if not( isFileExisting ) and not( viewOnly ):
  
      raise RuntimeError, currentFileNameDWI + ': no such file or directory'

  def getSizeAndResolutionInformation(self,currentFileNameDWI, axisOI):
    
    command = 'GkgExecuteCommand VolumeInformation' + \
        ' -i ' + currentFileNameDWI + \
        ' -info ' + axisOI

    fd = os.popen( command )
    sizeInfo = fd.read()
    ret = fd.close()

    return sizeInfo

  def readDWI( self, parameters, functors, fovDirectory, subjectName,verbose, 
    viewOnly ):
  
    afterCombinerDirectory = fovDirectory + '02-DW/00-Data/after10KCombiner/'
    outputWorkDirectory = fovDirectory + '02-DW/01-DWInQSpace-b1500-4500-8000/' 

    inputFileNameDWIs = filter( lambda x: x.endswith( '.ima' ),\
      os.listdir( afterCombinerDirectory  ) )
    inputFileNameDWIs.sort()

    if ( verbose ):
    
      print  'input DWI filename(s) :', inputFileNameDWIs
      

    # looping over input file(s)
    dwiCount = len( inputFileNameDWIs )
    listOfSizeX = list()
    listOfSizeY = list()
    listOfSizeZ = list()
    listOfSizeT = list()
    listOfResolutionX = list()
    listOfResolutionY = list()
    listOfResolutionZ = list()
    outputFileNameDWIs = list()
    
    for n in range( 0, dwiCount ):
      
      inputFileNameDWI = inputFileNameDWIs[ n ]

      currentFileNameDWI = self.rescaleWithSlopeAndIntercept\
      (outputWorkDirectory,inputFileNameDWI,n,subjectName,viewOnly)

      self.readDwiSanityCheck(currentFileNameDWI)

      # processing volume size(s)
      stringSizeX = self.getSizeAndResolutionInformation(currentFileNameDWI, \
        axisOI = 'sizeX ')
      listOfSizeX.append( int( stringSizeX[ : -1 ] ) )

      stringSizeY = self.getSizeAndResolutionInformation(currentFileNameDWI, \
        axisOI = 'sizeY')
      listOfSizeY.append( int( stringSizeY[ : -1 ] ) )

      stringSizeZ = self.getSizeAndResolutionInformation(currentFileNameDWI, \
        axisOI = 'sizeZ')
      listOfSizeZ.append( int( stringSizeZ[ : -1 ] ) )

      stringSizeT = self.getSizeAndResolutionInformation(currentFileNameDWI, \
        axisOI = 'sizeT')
      listOfSizeT.append( int( stringSizeT[ : -1 ] ) )

      stringResolutionX = self.getSizeAndResolutionInformation(\
        currentFileNameDWI, axisOI = 'resolutionX')
      listOfResolutionX.append( float( stringResolutionX[ : -1 ] ) )

      stringResolutionY = self.getSizeAndResolutionInformation(\
        currentFileNameDWI, axisOI = 'resolutionY')
      listOfResolutionY.append( float( stringResolutionY[ : -1 ] ) )

      stringResolutionZ = self.getSizeAndResolutionInformation(\
        currentFileNameDWI, axisOI = 'resolutionZ')
      listOfResolutionZ.append( float( stringResolutionZ[ : -1 ] ) )

    return dwiCount, \
           listOfSizeX, \
           listOfSizeY, \
           listOfSizeZ, \
           listOfSizeT, \
           listOfResolutionX, \
           listOfResolutionY, \
           listOfResolutionZ, \
           outputFileNameDWIs
  
  def getGradientCharacterics( self,gradientCharacteristicsFileNames,
    outputWorkDirectory ):
                               
    fileType = None
    diffusionScheme = None

    concatenatedGradientCharacteristics = dict()
    
    print 'gradientCharacteristicsFileNames=', gradientCharacteristicsFileNames
                              
    if ( len( gradientCharacteristicsFileNames[ 0 ] ) ):

      concatenatedGradientCharacteristics[ 'PGSE' ] = \
                                                   { 'gradient-magnitudes' : [],
                                                     'little-delta' : [],
                                                     'big-delta' : [] }
                                          

      # looping of diffusion gradient characteristics filename(s)
      for gradientCharacteristicsFileName in gradientCharacteristicsFileNames:

        # reading the gradient characteristics file
        if ( not os.path.exists( gradientCharacteristicsFileName ) ):
 
          message = '\'' + gradientCharacteristicsFileName + '\' file not found'
          raise RuntimeError, message
 
        f = open( gradientCharacteristicsFileName, 'r' )
        gradientCharacteristicsLines = f.readlines()
        f.close()

        # detecting the type of file (self-made dictionary, BRUKER method)
                           
        ########################################################################
        # case of BRUKER method file
        ########################################################################
        if ( '##TITLE=Parameter' in gradientCharacteristicsLines[ 0 ] ):

          if ( fileType is None ):
 
            fileType = 'BrukerMethod'
 
          else:
 
            if ( fileType != 'BrukerMethod' ):

              raise RuntimeError, 'incoherent file types schemes across ' + \
                                  'gradient characteristics file(s)'


          PVM_DiffPrepMode=None
          PVM_DwGradAmpLineIndex = 0
          PVM_DwGradDurLineIndex = 0
          PVM_DwGradSepLineIndex = 0
          diffusionType=None
          index = 0
          dwiCount = 0
          t2Count = 0
          gammaMHzperT = 42.576
          gradMaxMTperM = 0

          for line in gradientCharacteristicsLines:
 
            if ( '##$PVM_DiffPrepMode=' in line ):
 
              PVM_DiffPrepMode = line.split( '=' )[ 1 ][ : -1 ]
 
            if ( '##$PVM_DwNDiffExp=' in line ):
 
              dwiCount = int( line.split( '=' )[ 1 ] )

            if ( '##$PVM_DwAoImages' in line ):
 
              t2Count = int( line.split( '=' )[ 1 ] )

            if ( '##$PVM_GradCalConst' in line ):
 
              gradMaxHzperMm = float( line.split( '=' )[ 1 ] )

            if ( '##$PVM_DwGradAmp' in line ):
 
              PVM_DwGradAmpLineIndex = index + 1

            if ( '##$PVM_DwGradDur=' in line ):
 
              PVM_DwGradDurLineIndex = index + 1
 
            if ( '##$PVM_DwGradSep=' in line ):
 
              PVM_DwGradSepLineIndex = index + 1

            index += 1
          dwCount = dwiCount - t2Count
          gradMaxMTperM = gradMaxHzperMm/(gammaMHzperT * 1e6) * 1e6
          gradMaxTperM = gradMaxMTperM * 1e-3
 
          PVM_DwGradAmp = float( gradientCharacteristicsLines[ \
                                                      PVM_DwGradAmpLineIndex ] )
          PVM_DwGradDur = float( gradientCharacteristicsLines[ \
                                                      PVM_DwGradDurLineIndex ] )
          PVM_DwGradSep = float( gradientCharacteristicsLines[ \
                                                      PVM_DwGradSepLineIndex ] )


          print 'PVM_DiffPrepMode=', PVM_DiffPrepMode
          print 'dwiCount=', dwiCount
          print 't2Count=', t2Count
          print 'gradMax=', gradMaxMTperM
          print 'gradMaxHzperMm=', gradMaxHzperMm 
          print 'PVM_DwGradAmp=', PVM_DwGradAmp
          print 'PVM_DwGradDur=', PVM_DwGradDur
          print 'PVM_DwGradSep=', PVM_DwGradSep

          if ( diffusionScheme is None ):
 
            if ( PVM_DiffPrepMode == '<SpinEcho>' ):
 
              diffusionType = 'PGSE'
              diffusionScheme = diffusionType
 
          else:
            
            if ( PVM_DiffPrepMode == '<SpinEcho>'):

              diffusionType = 'PGSE'

 
            if ( diffusionType != diffusionScheme ):
 
              raise RuntimeError, 'incoherent diffusion schemes across ' + \
                                  'gradient characteristics file(s)'

          if ( diffusionScheme == 'PGSE' ):
 
            concatenatedGradientCharacteristics[ 'PGSE' ] \
                                               [ 'type' ] = 'PGSE'
            concatenatedGradientCharacteristics[ 'PGSE' ] \
                                               [ 'gradient-magnitudes' ] += \
                                 [ gradMaxTperM * PVM_DwGradAmp/100 ] * dwCount
            concatenatedGradientCharacteristics[ 'PGSE' ] \
                                               [ 'little-delta' ] += \
                                             [ PVM_DwGradDur * 0.001 ] * dwCount
            concatenatedGradientCharacteristics[ 'PGSE' ] \
                                               [ 'big-delta' ] += \
                                             [ PVM_DwGradSep * 0.001 ] * dwCount

    else:
    
      diffusionScheme = 'unknown'
      concatenatedGradientCharacteristics[ 'unknown' ] = dict()
      concatenatedGradientCharacteristics[ 'unknown' ][ 'type' ] = 'unknown'

    gradientFileName = os.path.join( outputWorkDirectory, 'gradients.txt' )
    
    f = open( gradientFileName, 'w' )
    if ( diffusionScheme == 'PGSE' ):
    
      f.write( 'attributes = ' + \
               repr( concatenatedGradientCharacteristics[ 'PGSE' ] ) + '\n' )
               
    else:
    
      f.write( 'attributes = ' + \
               repr( concatenatedGradientCharacteristics[ 'unknown' ] ) + '\n' )

    f.close()
    
    return gradientFileName

  def createRGB( self,fovDirectory,subjectName,verbose,viewOnly ):

    fileNameAverageT2 = fovDirectory + '02-DW/01-DWInQSpace-b1500-4500-8000/t2'
    fileNameDW = fovDirectory + '02-DW/01-DWInQSpace-b1500-4500-8000/dw'
    outputWorkDirectory = fovDirectory + '02-DW/01-DWInQSpace-b1500-4500-8000'

    fileNameRGB = os.path.join( outputWorkDirectory, 'dti_rgb' )
    command = 'GkgExecuteCommand DwiTensorField' + \
              ' -t2 ' + fileNameAverageT2 + \
              ' -dw ' + fileNameDW + \
              ' -f rgb ' + \
              ' -o ' + fileNameRGB + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )

  def createODF( self,functors,fileNameAverageT2,fileNameDW,outputWorkDirectory,
    subjectName,verbose,viewOnly ):

    fileNameShellDW = fileNameDW
    
    # in case of multiple shell q-space sampling, using the highest q shell
    highestShellIndex = 0
    for f in os.listdir( outputWorkDirectory ):
    
      
      if ( ( len( f ) > 8 ) & ( f[ 0 : 7 ] == 'dw_shell' ) ):
      
        shellIndex = string.atoi( f[ 8 : string.find( f, '.' ) ] )
        if ( shellIndex > highestShellIndex ):
        
          highestShellIndex = shellIndex
          
    if ( highestShellIndex > 0 ):
    
      fileNameShellDW = os.path.join( outputWorkDirectory,
                                      'dw_shell' + str( highestShellIndex ) )

    # building analytical QBI ODF map
    fileNameODFTextureMap = os.path.join( outputWorkDirectory,
                                          'aqbi_odf_texture_map.texturemap' )
    fileNameODFSiteMap = os.path.join( outputWorkDirectory,
                                       'aqbi_odf_site_map.sitemap' )
    command = 'GkgExecuteCommand DwiOdfField' + \
              ' -t2 ' + \
              fileNameAverageT2 + \
              ' -dw ' + \
              fileNameShellDW + \
              ' -type aqball_odf_cartesian_field ' + \
              ' -f odf_texture_map odf_site_map ' + \
              ' -o ' + fileNameODFTextureMap + ' ' + fileNameODFSiteMap + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )

    return fileNameODFTextureMap, fileNameODFSiteMap

  def convertAnatDicomToIma(self, rawDataDirectory, fovDirectory, 
    numOfAcquisitionsForResolutions, subjectName, viewOnly ):
    
    acquisitionsDirectory = fovDirectory + '00-InputData/' + \
     os.path.basename(rawDataDirectory) + '/'

    for resolution in numOfAcquisitionsForResolutions.keys():

      print(numOfAcquisitionsForResolutions[resolution])
      if numOfAcquisitionsForResolutions[resolution] > -1:

        dicomDirectory = acquisitionsDirectory + \
        str( numOfAcquisitionsForResolutions[resolution] ) + '/pdata/1/dicom/'
        anatomyDirectory = fovDirectory + '01-T2W/' +  resolution 

        command = 'GkgExecuteCommand Dicom2GisConverter \
        -i '+ dicomDirectory + '\
        -o ' + anatomyDirectory + '\
        -verbose'
        executeCommand( self, subjectName, command, viewOnly )

        shutil.copyfile( \
          acquisitionsDirectory + \
          str( numOfAcquisitionsForResolutions[resolution])\
          + '/method', fovDirectory + "01-T2W/method_" + resolution )
        shutil.copyfile(  \
          acquisitionsDirectory + \
          str( numOfAcquisitionsForResolutions[resolution])\
           +'/acqp',fovDirectory + "01-T2W/acqp_" + resolution )

  def convertDiffDicomToIma(self, inputDataDirectoryName,fovDirectory,dwBValue,
    firstAcq,lastAcq, subjectName, viewOnly ):

    acquisitionsDirectory = fovDirectory + '00-InputData/' + \
    os.path.basename(inputDataDirectoryName) + '/'
    dwDirectory = fovDirectory + '02-DW/00-Data/'

    for numAcquisition in range( firstAcq,lastAcq + 1 ):

      dicomDirectory = acquisitionsDirectory + str( numAcquisition ) + \
       '/pdata/1/dicom/'

      command =  'GkgExecuteCommand Dicom2GisConverter \
      -i '+ dicomDirectory +'\
      -o ' +  dwDirectory  + 'b' +  str( dwBValue ) + '_b' + \
       str( numAcquisition-firstAcq + 1 ) +'\
      -verbose'
      executeCommand( self, subjectName, command, viewOnly )


      shutil.copyfile( acquisitionsDirectory + str( numAcquisition )+'/method',\
          dwDirectory + '/method_b' +  str( dwBValue ) + '_b'  + \
           str( numAcquisition-firstAcq + 1 ) )
      shutil.copyfile( acquisitionsDirectory + str( numAcquisition ) + \
          '/acqp',dwDirectory + '/acqp_b'+ str( dwBValue ) + '_b' + \
           str( numAcquisition-firstAcq + 1 ) )

  def moveImasAcpsAndMethodsInfiles(self, fovDirectory ):
    dwDataDirectory = fovDirectory + '02-DW/00-Data/'
    os.makedirs( dwDataDirectory + 'before10KCombiner/' )
    os.makedirs( dwDataDirectory + 'after10KCombiner/' )
    os.makedirs( dwDataDirectory + 'acqpAndMethods/' )

    filesInDwDataDir = os.listdir( dwDataDirectory )
    imaFiles = filter( lambda x : x.endswith( '.ima' ), filesInDwDataDir )
    dimFiles = filter( lambda x : '.dim' in x, filesInDwDataDir )
    acqpFiles = filter( lambda x : 'acqp_' in x, filesInDwDataDir )
    methodFiles = filter( lambda x : 'method_' in x, filesInDwDataDir )

    map( lambda x: os.rename( dwDataDirectory + x,dwDataDirectory + \
      'before10KCombiner/' + x ),imaFiles )
    map( lambda x: os.rename( dwDataDirectory + x,dwDataDirectory + \
      'before10KCombiner/' + x ),dimFiles )
    map( lambda x: os.rename( dwDataDirectory + x,dwDataDirectory + \
      'acqpAndMethods/' + x ),acqpFiles )
    map( lambda x: os.rename( dwDataDirectory + x,dwDataDirectory + \
      'acqpAndMethods/' + x ),methodFiles )

  def applyCombiner10K( self, fovDirectory, subjectName, viewOnly):

    combinerInputDir = fovDirectory + '02-DW/00-Data/before10KCombiner/'
    combinerOutputDir = fovDirectory + '02-DW/00-Data/after10KCombiner/'

    images = filter( lambda x : x.endswith( '.ima' ), \
      os.listdir( combinerInputDir))

    for image in images:

      bashCommand = "GkgExecuteCommand Combiner \
      -i " + combinerInputDir + image + "  \
      -o " + combinerOutputDir + image + " \
      -op '*' \
      -f id \
      -g id \
      -num1 10000 1 \
      -den1 1 1  \
      -num2 1 \
      -den2 1 \
      -verbose"

      executeCommand( self, subjectName, bashCommand, viewOnly )   

  def generateFovPath(self,parameters):
    blocksDirectory = parameters['blockDirectory'].getValue()
    leftHemisphere = parameters[ 'leftHemisphere' ].getValue()
    blockLetter = parameters[ 'blockLetter' ].getValue()
    fovNumber = parameters[ 'fovNumber' ].getValue()
    rawDataDirectory = parameters[ 'rawDataDirectory' ].getValue()

    if leftHemisphere:  

        hemisphereDirectory = blocksDirectory + '/' + "LeftHemisphere" 
    else:

        hemisphereDirectory = blocksDirectory + '/' + "RightHemisphere" 

    return hemisphereDirectory + '/' + str(blockLetter) + '/' \
    + str(blockLetter) + str(fovNumber) +'/'

  def createOutputDirectories(self, FOVDirectory ):
    os.makedirs( FOVDirectory + '00-InputData' )
    os.makedirs( FOVDirectory + '01-T2W' )
    os.makedirs( FOVDirectory + '02-DW' )

    os.makedirs( FOVDirectory + '02-DW/00-Data' )
    os.makedirs( FOVDirectory + '02-DW/01-DWInQSpace-b1500-4500-8000' )
    os.makedirs( FOVDirectory + '02-DW/02-Registration' )
    os.makedirs( FOVDirectory + '02-DW/03-Filtering' )
    os.makedirs( FOVDirectory + '02-DW/04-Mask' )
    os.makedirs( FOVDirectory + '02-DW/05-ShoreModeling' )
    os.makedirs( FOVDirectory + '02-DW/06-DtiModeling' )

  def moveRawData( self,rawDataDirectory,fovDirectory ):
    
    print(rawDataDirectory,fovDirectory + '00-InputData/' + \
      os.path.basename(rawDataDirectory) )

    os.rename( rawDataDirectory, \
      fovDirectory + '00-InputData/' + os.path.basename(rawDataDirectory) )

  def checkAllFilesSameSize(self, dwiCount, listOfSizeX, sizeX, listOfSizeY,
    sizeY, listOfSizeZ, sizeZ, listOfResolutionX, resolutionX,listOfResolutionY,
    resolutionY,listOfResolutionZ, resolutionZ ):
    for n in range( 1, dwiCount ):

      if ( ( listOfSizeX[ n ] != sizeX ) or \
           ( listOfSizeY[ n ] != sizeY ) or \
           ( listOfSizeZ[ n ] != sizeZ ) ):
          
        print 'incoherent volume size(s)'
        return

      if ( ( listOfResolutionX[ n ] != resolutionX ) or \
           ( listOfResolutionY[ n ] != resolutionY ) or \
           ( listOfResolutionZ[ n ] != resolutionZ ) ):
          
        print 'incoherent volume resolution(s)'
        return

  def readAndClose():
    fd = os.popen( command )
    stringQSpacePointCount = fd.read()
    ret = fd.close()

  def mergingAndAveragingT2(self,dwInQSpaceDir,fileNameT2s,
    subjectName,viewOnly):

    fileNameT2Multiple = os.path.join( dwInQSpaceDir, 't2_multiple' )
    command = 'GkgExecuteCommand Cat'  + \
              ' -i '  + fileNameT2s  + \
              ' -o '  + fileNameT2Multiple  + \
              ' -t t'  + \
              ' -verbose true -verbosePluginLoading false'
    executeCommand( self, subjectName, command, viewOnly )

    fileNameAverageT2 = os.path.join( dwInQSpaceDir, 't2' )
    command = 'GkgExecuteCommand VolumeAverager'  + \
              ' -i '  + fileNameT2Multiple  + \
              ' -o '  + fileNameAverageT2  + \
              ' -axis t'  + \
              ' -verbose true -verbosePluginLoading false'
    executeCommand( self, subjectName, command, viewOnly )

  def concatenatingDw(self,dwInQSpaceDir,fileNameDWs,subjectName,viewOnly):
   
    fileNameDW = os.path.join( dwInQSpaceDir, 'dw' )
    command = 'GkgExecuteCommand Cat'  + \
              ' -i '  + fileNameDWs  + \
              ' -o '  + fileNameDW  + \
              ' -t t'  + \
              ' -verbose true -verbosePluginLoading false'
    executeCommand( self, subjectName, command, viewOnly )

  def createEveryT2AndDW(self, fovDirectory, verbose, subjectName, viewOnly):
    
    listOfFileNameT2s = list()
    listOfFileNameDWs = list()
    fileNameT2s = ''
    fileNameDWs = ''
    globalDwOrientations = list()
    globalDwBValues = list()
    globalReceiverGains = list()

    dwDataDirectory = fovDirectory + '02-DW/00-Data/'
    dwInQSpaceDir = fovDirectory + '02-DW/01-DWInQSpace-b1500-4500-8000/'
    imaFiles = filter( lambda x: x.endswith( '.ima' ),\
      os.listdir( dwDataDirectory +'after10KCombiner/' ) )
    methodFiles = filter( lambda x: 'method_' in x,\
      os.listdir( dwDataDirectory + 'acqpAndMethods/' ) )
    imaFiles.sort()
    methodFiles.sort()

    for indexImg in range( len( imaFiles ) ):

      orientationFileName = dwDataDirectory + 'acqpAndMethods/' + \
          methodFiles[indexImg]
      fileNameDWI = dwDataDirectory + 'after10KCombiner/' + imaFiles[indexImg]
      bValueThreshold = 1000

      fileNameT2, fileNameDW, dwOrientations, dwBValues, receiverGain =\
       self.separatingDwAndT2( extension = str( indexImg ),
        orientationFileName = orientationFileName,
        bValueThreshold = bValueThreshold,
        fileNameDWI = fileNameDWI,
        outputWorkDirectory = dwInQSpaceDir,
        verbose = True ,
        subjectName = subjectName,
        viewOnly = viewOnly)

      listOfFileNameT2s  +=  [ fileNameT2 ]
      listOfFileNameDWs  +=  [ fileNameDW ]
      fileNameT2s  +=  fileNameT2  + ' '
      fileNameDWs  +=  fileNameDW  + ' '
      globalDwOrientations  +=  dwOrientations
      globalDwBValues  +=  dwBValues
      globalReceiverGains  +=  [ receiverGain ]

    self.mergingAndAveragingT2(dwInQSpaceDir,fileNameT2s,subjectName,viewOnly)

    self.concatenatingDw(dwInQSpaceDir,fileNameDWs,subjectName,viewOnly)

    self.updateDwMinf( globalDwOrientations,dwInQSpaceDir,globalDwBValues,\
      fileNameDW,methodFiles,bValueThreshold,fovDirectory, dwDataDirectory,\
      verbose,subjectName,viewOnly)

  def checkDoFilesExist( self,orientationFileName ):
    # reading the orientation file
    if ( not os.path.exists( orientationFileName ) ):

      message = '\''  + orientationFileName  + '\' file not found'
      raise RuntimeError, message

  def separatingDwAndT2(self, extension,orientationFileName,  bValueThreshold, 
    fileNameDWI,  outputWorkDirectory, verbose,subjectName,viewOnly):

    self.checkDoFilesExist( orientationFileName )

    # receiver gain; by default, we assume it is 1.0
    receiverGain = 1.0

    f = open( orientationFileName, 'r' )
    orientationLines = f.readlines()
    f.close()

    t2Indices,dwIndices,dwOrientations,dwBValues,index,t2Count,dwiCount,\
    repetitionCount,dwNDiffExpEach,firstOrientationLineIndex,\
    lastOrientationLineIndex = \
    self.extractMethodInfo( orientationLines,bValueThreshold,True )


    if ( verbose ):

      print 'T2 indices : ', t2Indices
      print 'DW indices : ', dwIndices
      print 'DW orientations : ', dwOrientations

    t2FileName = os.path.join( outputWorkDirectory, 't2_'  + extension )
    dwFileName = os.path.join( outputWorkDirectory, 'dw_'  + extension )

    if ( verbose ):

      print 'extracting T2 volume ', extension, ' : \'', t2FileName, '\''

    command = 'GkgExecuteCommand SubVolume'  + \
              ' -i '  + fileNameDWI  + \
              ' -o '  + t2FileName  + \
              ' -tIndices '  + t2Indices  + \
              ' -verbose true -verbosePluginLoading false'
    executeCommand( self, subjectName, command, viewOnly )

    if ( verbose ):

      print 'extracting DW volume ', extension, ' : \'', dwFileName, '\''

    command = 'GkgExecuteCommand SubVolume'  + \
              ' -i '  + \
              fileNameDWI  + \
              ' -o '  + \
              dwFileName  + \
              ' -tIndices '  + dwIndices  + \
              ' -verbose true -verbosePluginLoading false'
    executeCommand( self, subjectName, command, viewOnly )


    return t2FileName, dwFileName, dwOrientations, dwBValues, receiverGain

  def writeOrientationTxt( self,globalDwOrientations,outputOrientationFileName,
    verbose):
    if verbose:

      print 'global orientation set :'
      print globalDwOrientations
    f = open( outputOrientationFileName, 'w' )
    globalDwOrientationCount = len( globalDwOrientations )
    f.write( str( globalDwOrientationCount )  + '\n' )
    for orientation in globalDwOrientations:

      f.write( str( orientation[ 0 ] )  + ' '  + \
               str( orientation[ 1 ] )  + ' '  + \
               str( orientation[ 2 ] )  + '\n' )
    f.close()

  def writeOutputBValueFileName(self, globalDwBValues,outputBValueFileName,
    verbose):
    if verbose:

      print 'global b-value set :'
      print globalDwBValues

    f = open( outputBValueFileName, 'w' )
    globalDwBValueCount = len( globalDwBValues )
    f.write( str( globalDwBValueCount )  + '\n' )
    for b in globalDwBValues:

      f.write( str( b )  + '\n' )
    f.close()

  def updateDwMinf( self,globalDwOrientations,DWAndT2Output,globalDwBValues,
    fileNameDW,methodFiles,bValueThreshold,fovDirectory, dwDataDirectory, 
    verbose,subjectName,viewOnly):

    outputOrientationFileName = os.path.join( DWAndT2Output,'orientations.txt' )
    outputBValueFileName = os.path.join( DWAndT2Output,'bvalues.txt')

    self.writeOrientationTxt( globalDwOrientations,outputOrientationFileName,\
      verbose)
    self.writeOutputBValueFileName( globalDwBValues,outputBValueFileName,\
      verbose)

    fileNameDW = fovDirectory + '02-DW/01-DWInQSpace-b1500-4500-8000/dw'
    gradientCharacteristicsFileNames = map(lambda x : dwDataDirectory + \
      'acqpAndMethods/' + x,methodFiles)

    command = 'GkgExecuteCommand DwiQSpaceSampling' + \
              ' -dw ' + \
              fileNameDW + \
              ' -stringParameters spherical multiple-shell' + \
              ' different-orientation-sets custom ' + \
                outputBValueFileName + ' ' + \
                outputOrientationFileName + \
              ' -scalarParameters ' + str( bValueThreshold ) + \
              ' -gradientCharacteristics ' + \
                self.getGradientCharacterics( \
                                          gradientCharacteristicsFileNames, \
                                          DWAndT2Output ) + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )
    
    command = 'GkgExecuteCommand DwiMultipleShellQSpaceSamplingSplitter' + \
              ' -dw ' + fileNameDW + \
              ' -o ' + fileNameDW + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )

  def extractMethodInfo( self, orientationLines,bValueThreshold,verbose ):
    t2Indices = ''
    dwIndices = ''
    dwOrientations = list()
    dwBValues = list()
      
    # reading to detect diffusion orientation information ####################
    index = 0
    t2Count = 0
    dwiCount = 0
    repetitionCount = 0
    dwNDiffExpEach = 1
    firstOrientationLineIndex = 0
    lastOrientationLineIndex = 0
    for line in orientationLines:

      if ( '##$PVM_DwNDiffExp=' in line ):

        dwiCount = int( line.split( '=' )[ 1 ] )
        if ( verbose ):

          print 'DWI count :', dwiCount

      if ( '##$PVM_DwNDiffExpEach=' in line ): # ?! J'ai enlever les espaces


        if ( verbose ):

          print 'Number of b-values per orientation :', dwNDiffExpEach

      if ( '##$PVM_DwAoImages' in line ):

        t2Count = int( line.split( '=' )[ 1 ] )
        if ( verbose ):

          print 'T2 count :', t2Count

      if ( '##$PVM_NRepetitions' in line ):
        repetitionCount = int( line.split( '=' )[ 1 ] )
        if ( verbose ):

          print 'repetition count :', repetitionCount

      if ( '##$PVM_DwDir' in line ):

        firstOrientationLineIndex = index  + 1

      if ( '##$PVM_SPackArrGradOrient' in line ):

        orientationLineIndex = index  + 1

      index  +=  1

    index = firstOrientationLineIndex  + 1
    while ( not '##$PVM_' in orientationLines[ index ] ):
      index  +=  1

    lastOrientationLineIndex = index - 1

    index = orientationLineIndex
    r = []
    while ( not '##$PVM_' in orientationLines[ index ] ):

      r  +=  orientationLines[ index ].split()
      index  +=  1

        # IT MIGHT BE THAT WE DON'T NEED TO APPLY ROTATION ???
    rotation = [ [ float( r[ 0 ] ), float( r[ 3 ] ), float( r[ 6 ] ) ],
                 [ float( r[ 1 ] ), float( r[ 4 ] ), float( r[ 7 ] ) ],
                 [ float( r[ 2 ] ), float( r[ 5 ] ), float( r[ 8 ] ) ] ]

    #rotation = [ [ float( 1 ), float( 0 ), float( 0 ) ],
    #             [ float( 0 ), float( 1 ), float( 0 ) ],
    #             [ float( 0 ), float( 0 ), float( 1 ) ] ]

    coordinates = []
    for index in range( firstOrientationLineIndex,
                        lastOrientationLineIndex  + 1 ):

      coordinates  +=  orientationLines[ index ].split()

    directions = []

    for d in range( 0, t2Count ):

      directions.append( [ 0.0, 0.0, 0.0 ] )

    for d in range( 0, ( dwiCount - t2Count ) / dwNDiffExpEach ):

      for b in range( 0, dwNDiffExpEach ):

        directions.append( [ coordinates[ 0  + 3 * d ],
                             coordinates[ 1  + 3 * d ],
                             coordinates[ 2  + 3 * d ] ] )

    # reading to detect b-value information ##################################
    firstBValueLineIndex = 0
    lastBValueLineIndex = 0
    index = 0
    for line in orientationLines:

      if ( '##$PVM_DwEffBval' in line ):

        firstBValueLineIndex = index  + 1

      index  +=  1

    index = firstBValueLineIndex  + 1
    while ( not '##$PVM_' in orientationLines[ index ] ):

      index  +=  1

    lastBValueLineIndex = index - 1

    bValues = []
    for index in range( firstBValueLineIndex,
                        lastBValueLineIndex  + 1 ):

      bValues  +=  orientationLines[ index ].split()

    # adding orientation & b-value information ###############################
    for index in range( 0, dwiCount * repetitionCount ):

      x = float( directions[ index % dwiCount ][ 0 ] )
      y = float( directions[ index % dwiCount ][ 1 ] )
      z = float( directions[ index % dwiCount ][ 2 ] )
      bValue = float( bValues[ index % dwiCount ] )
      if ( ( ( x * x  + y * y  + z * z ) < 1e-3 ) or
           ( ( bValue < bValueThreshold ) and
             ( bValue < 50.0 ) ) ):

        t2Indices  +=  str( index )  + ' '

      else:
        dwIndices  +=  str( index )  + ' '
        dwOrientations.append( [ rotation[ 0 ][ 0 ] * x  + 
                                 rotation[ 0 ][ 1 ] * y  + 
                                 rotation[ 0 ][ 2 ] * z,
                                 rotation[ 1 ][ 0 ] * x  + 
                                 rotation[ 1 ][ 1 ] * y  + 
                                 rotation[ 1 ][ 2 ] * z,
                                 rotation[ 2 ][ 0 ] * x  + 
                                 rotation[ 2 ][ 1 ] * y  + 
                                 rotation[ 2 ][ 2 ] * z ] )
        dwBValues.append( bValue )
    return t2Indices,dwIndices,dwOrientations,dwBValues,index,t2Count,dwiCount,\
    repetitionCount,dwNDiffExpEach,firstOrientationLineIndex,\
    lastOrientationLineIndex


