from Viewer import *


class DwiBlockDataImportAndQSpaceSamplingViewer( Viewer ):

  def __init__( self, minimumSize, parent ):

    Viewer.__init__( self, minimumSize, parent )
    
    ########################## First View ######################################

    self.createView( 'first_view' )
    
    self.addAxialWindow( 'first_view', 0, 0, 1, 1,
                         'Average T2(b=0)', 'Average T2(b=0)' )

  
    self.addAxialWindow( 'first_view', 1, 0, 1, 1,
                         'DW', 'DW' )

    self.add3DWindow( 'first_view', 0, 1, 1, 1,
                      'Q-space sampling', 'Q-space sampling' )

    self.add3DWindow( 'first_view', 1, 1, 1, 1,
                      'QBI ODFs', 'QBI ODFs' )


def createDwiBlockDataImportAndQSpaceSamplingViewer( minimumSize, parent ):

  return DwiBlockDataImportAndQSpaceSamplingViewer( minimumSize, parent )

