from Algorithm import *
from DwiBlockDtiModelingTask import *


class DwiBlockDtiModelingAlgorithm( Algorithm ):

  def __init__( self, verbose ):

    Algorithm.__init__( self, 'DWI-Block-Dti-Modeling', verbose, True )
    
    # Input data
    self.addParameter( StringParameter( 'blockDirectory', '' ) )
    self.addParameter( BooleanParameter( 'leftHemisphere', True ) ) 
    self.addParameter( StringParameter( 'blockLetter', '' ) )
    self.addParameter( IntegerParameter( 'fovNumber', \
                                          1, 1, 4, 1 ) )
    
    # DTI model type
    self.addParameter( ChoiceParameter( 'dtiEstimatorType',
                                        1,
                                        ( 'linear_square',
                                          'robust_positive_definite' ) ) )
    	
  def launch( self ):
  
    if ( self._verbose ):
    
      print \
        'running DTI local modeling'

    task = DwiBlockDtiModelingTask( self._application )
    task.launch( False )


  def view( self ):
  
    if ( self._verbose ):
    
      print \
        'viewing DWI local modeling'

    # task = DwiBlockDtiModelingTask( self._application, defaultViewIndex )
    # task.launch( True )

