from AlgorithmGUI import *
from ViewerCallbackManager import *
from ResourceManager import *


class DwiBlockDtiModelingAlgorithmGUI( AlgorithmGUI ):

  def __init__( self, algorithm ):

    AlgorithmGUI.__init__( self,
                           algorithm,
                           ResourceManager().getUIFileName(
                                         'dmri',
                                         'DwiBlockDtiModeling.ui' ) )
    

    ############################################################################
    # connecting Block & FOV Interface
    ############################################################################

    self.connectStringParameterToLineEdit( 'blockDirectory',
                                           'lineEdit_BlockDirectory' )    
    self.connectDirectoryBrowserToPushButtonAndLineEdit( \
                                                   'pushButton_BlockDirectory',
                                                   'lineEdit_BlockDirectory' )
    self.connectBooleanParameterToRadioButton( \
                                         'leftHemisphere',
                                         'radioButton_LeftHemisphere' )
    self.connectStringParameterToLineEdit( 'blockLetter',
                                           'lineEdit_BlockLetter' )    
    self.connectIntegerParameterToSpinBox( \
                                      'fovNumber',
                                      'spinBox_FovNumber' )


    ############################################################################
    # Connecting local model parameters interface
    ############################################################################

    # Connecting DTI model type
    self.connectChoiceParameterToComboBox( 'dtiEstimatorType',
                                           'comboBox_DtiEstimatorType' )

