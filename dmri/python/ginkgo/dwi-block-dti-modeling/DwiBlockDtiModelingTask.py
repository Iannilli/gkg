# coding: utf8

from Task import *
import time
import os


class DwiBlockDtiModelingTask( Task ):

  def __init__( self, application ):
  
    Task.__init__( self, application )

  def getPrefixedOdfFunctorNamesAndFileNames( self, odfFunctorList, prefix,
                                              outputWorkDirectory ):
  
    odfFunctorNames = ''
    prefixedOdfFunctorFileNameMap = {}
    prefixedOdfFunctorFileNames = ''
    for functor in odfFunctorList:
    
      odfFunctorNames += functor + ' '
      prefixedOdfFunctorFileNameMap[ functor ] = os.path.join( \
                                                   outputWorkDirectory,
                                                   prefix + '_' + functor )
      prefixedOdfFunctorFileNames += prefixedOdfFunctorFileNameMap[ functor ] +\
                                     ' '
                                     
    return odfFunctorNames, \
           prefixedOdfFunctorFileNameMap, \
           prefixedOdfFunctorFileNames
  
  def generateFovPath(self,parameters):
    blocksDirectory = parameters['blockDirectory'].getValue()
    leftHemisphere = parameters[ 'leftHemisphere' ].getValue()
    blockLetter = parameters[ 'blockLetter' ].getValue()
    fovNumber = parameters[ 'fovNumber' ].getValue()

    if leftHemisphere:  

        hemisphereDirectory = blocksDirectory + '/' + "LeftHemisphere" 
    else:

        hemisphereDirectory = blocksDirectory + '/' + "RightHemisphere" 

    return hemisphereDirectory + '/' + str(blockLetter) + '/' \
    + str(blockLetter) + str(fovNumber) +'/'

      
  ##############################################################################
  # execute()
  ##############################################################################

  def execute( self, functors, parameters, subjectName, verbose, viewOnly ):

    try:

      ####################### acquiring conditions #############################
      functors[ 'condition-acquire' ]( subjectName, 'odf-field-processed' )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 0 )

      ####################### collecting output work directory #################
      fovDirectory = self.generateFovPath(parameters)

      if ( verbose ):
      
        print('fov Directory : ' + fovDirectory)

      outputFovDirectory = fovDirectory + '02-DW/06-DtiModeling/'

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 5 )

      ########################### DTI model ####################################
      
      odfType = 'tensor_odf_cartesian_field'
      viewType = 'dti_view'

      stringParameters = parameters[ 'dtiEstimatorType' ].getChoice() + ' '

      odfFunctorList = [ 
                        'gfa',
                        'mean_diffusivity',
                        'mean_dwi',
                        'nematic_order',
                        'normalized_entropy',
                        'normalized_gfa',
                        'normalized_mean_diffusivity',
                        'normalized_mean_dwi',
                        'odf_site_map',
                        'odf_texture_map',
                        'principal_orientation',
                        'rgb',
                         'standard_deviation' ]
      
      prefix = 'dti'
      odfFunctorString = reduce(lambda x,y : x + ' ' + y, odfFunctorList)
      prefixedOdfFunctorString = reduce(lambda x,y : x + ' ' + \
        outputFovDirectory + prefix + '_' + y, odfFunctorList, ' ')

      fileNameT2 = fovDirectory + '02-DW/03-Filtering/t2_filtered.ima'
      fileNameDw = fovDirectory + '02-DW/03-Filtering/dw_filtered.ima'
      fileNameMask = fovDirectory + '02-DW/04-Mask/mask.ima'

      command = 'GkgExecuteCommand DwiOdfField' + \
                ' -t2 ' + fileNameT2 + \
                ' -dw ' + fileNameDw + \
                ' -m ' + fileNameMask + \
                ' -type tensor_odf_cartesian_field' + \
                ' -f ' + odfFunctorString + \
                ' -o ' + prefixedOdfFunctorString + \
                ' -stringParameters ' + stringParameters + \
                ' -outputOrientationCount 500' + \
                ' -rgbScale 1.0'  + \
                ' -meshScale 1.0'  + \
                ' -verbose true'
      executeCommand( self, subjectName, command, viewOnly )

      dwiTensorFieldTasks= ['adc',
                            'diffusion_weighted_t2' ,
                            'dij',
                            'eigenvalues',
                            'fa',
                            'lambda_parallel',
                            'lambda_transverse',
                            'rgb',
                            'tensor_site_map' ,
                            'tensor_texture_map' ,
                            'volume_ratio' ]

      dwiTensorFieldTasksString = reduce(lambda x,y : x + ' ' + y, dwiTensorFieldTasks)
      prefixedDwiTensorFieldTasksString = reduce(lambda x,y : x + ' ' + \
        outputFovDirectory + prefix + '_' + y, dwiTensorFieldTasks, ' ')

      command = 'GkgExecuteCommand DwiTensorField' + \
                ' -t2 ' + fileNameT2 + \
                ' -dw ' + fileNameDw + \
                ' -m ' + fileNameMask + \
                ' -f ' + dwiTensorFieldTasksString + ' ' + \
                ' -o ' + prefixedDwiTensorFieldTasksString + ' ' + \
                ' -stringParameters ' + stringParameters + \
                ' -rgbScale 1.0' + \
                ' -meshScale 1.0' +\
                ' -verbose true'
      executeCommand( self, subjectName, command, viewOnly )

      ####################### notifying display thread #########################
      functors[ 'condition-notify-and-release' ]( subjectName,
                                                  'odf-field-processed' )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 100 )

    except:

      self._functors[ 'task-set-status-as-failed' ]( subjectName,
                                                     sys.exc_info() )



  ##############################################################################
  # display()
  ##############################################################################

  def display( self, functors, parameters, subjectName, verbose,isReportModeActive ):

    ########################### resetting the viewer ###########################
    functors[ 'viewer-reset' ]()

    ########################### collecting file names ##########################
    outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()

    fileNameT2 = parameters[ 'fileNameT2' ].getValue()
    fileNameDw = parameters[ 'fileNameDw' ].getValue()
    fileNameMask = parameters[ 'fileNameMask' ].getValue()
    fileNameT1 = parameters[ 'fileNameT1' ].getValue()
    fileNameTransformationDwToT1 = \
                        parameters[ 'fileNameTransformationDwToT1' ].getValue()

    odfTypeIndex = parameters[ 'odfType' ].getValue()

    odfType = ''
    viewType = ''
    stringParameters = ''
    scalarParameters = ''
    prefix = ''
    odfFunctorNames = ''
    prefixedOdfFunctorFileNameMap = None
    prefixedOdfFunctorFileNames = ''

    ########################### DOT model ######################################
    if ( odfTypeIndex == 0 ):
    
      odfType = 'dot_odf_cartesian_field'
      viewType = 'dot_view'
      functors[ 'viewer-set-view' ]( viewType )

      scalarParameters = \
        str( parameters[ 'dotMaximumSHOrder' ].getValue() ) + ' ' +  \
        str( parameters[ 'dotEffectiveDiffusionTime' ].getValue() ) + ' ' + \
        str( parameters[ 'dotR0' ].getValue() ) + ' ' + \
        str( parameters[ 'dotOdfComputation' ].getValue() == 2 ) + ' '
        
      odfFunctorList = [ 'gfa',
                         'mean_diffusivity',
                         'mean_dwi',
                         'nematic_order',
                         'normalized_entropy',
                         'normalized_gfa',
                         'normalized_mean_diffusivity',
                         'normalized_mean_dwi',
                         'odf_site_map',
                         'odf_texture_map',
                         'principal_orientation',
                         'rgb',
                         'standard_deviation' ]
      prefix = 'dot'
      odfFunctorNames, \
      prefixedOdfFunctorFileNameMap, \
      prefixedOdfFunctorFileNames = \
        self.getPrefixedOdfFunctorNamesAndFileNames( odfFunctorList, prefix,
                                                     outputWorkDirectory )

    ########################### DSI model ######################################
    elif ( odfTypeIndex == 1 ):
    
      odfType = 'dsi_odf_cartesian_field'
      viewType = 'dsi_view'
      functors[ 'viewer-set-view' ]( viewType )

      scalarParameters = \
        str( parameters[ 'dsiFilteringDataBeforeFFT' ].getValue() ) + ' ' + \
        str( parameters[ 'dsiMinimumR0' ].getValue() ) + ' ' +  \
        str( parameters[ 'dsiMaximumR0' ].getValue() ) + ' ' + \
        str( parameters[ 'dsiMarginalOdf' ].getValue() ) + ' '
    
      odfFunctorList = [ 'gfa',
                         'mean_diffusivity',
                         'mean_dwi',
                         'mean_squared_displacement',
                         'nematic_order',
                         'normalized_entropy',
                         'normalized_gfa',
                         'normalized_mean_diffusivity',
                         'normalized_mean_dwi',
                         'normalized_mean_squared_displacement',
                         'normalized_return_to_origin_probability',
                         'odf_site_map',
                         'odf_texture_map',
                         'principal_orientation',
                         'return_to_origin_probability',
                         'rgb',
                         'standard_deviation' ]
      prefix = 'dsi'
      odfFunctorNames, \
      prefixedOdfFunctorFileNameMap, \
      prefixedOdfFunctorFileNames = \
        self.getPrefixedOdfFunctorNamesAndFileNames( odfFunctorList, prefix,
                                                     outputWorkDirectory )

    ########################### QBI model ######################################
    elif ( odfTypeIndex == 2 ):
    
      odfType = 'qball_odf_cartesian_field'
      viewType = 'qbi_view'
      functors[ 'viewer-set-view' ]( viewType )

      scalarParameters = \
        str( parameters[ 'qbiEquatorPointCount' ].getValue() ) + ' ' + \
        str( parameters[ 'qbiPhiFunctionAngle' ].getValue() ) + ' ' +  \
        str( parameters[ 'qbiPhiFunctionMaximumAngle' ].getValue() ) + ' '
    
      stringParameters = parameters[ 'qbiPhiFunctionType' ].getChoice() + ' '

      odfFunctorList = [ 'gfa',
                         'mean_diffusivity',
                         'mean_dwi',
                         'nematic_order',
                         'normalized_entropy',
                         'normalized_gfa',
                         'normalized_mean_diffusivity',
                         'normalized_mean_dwi',
                         'odf_site_map',
                         'odf_texture_map',
                         'principal_orientation',
                         'rgb',
                         'standard_deviation' ]
      prefix = 'qbi'
      odfFunctorNames, \
      prefixedOdfFunctorFileNameMap, \
      prefixedOdfFunctorFileNames = \
        self.getPrefixedOdfFunctorNamesAndFileNames( odfFunctorList, prefix,
                                                     outputWorkDirectory )

    ########################### SD model #######################################
    elif ( odfTypeIndex == 3 ):
    
      odfType = 'sd_odf_cartesian_field'
      viewType = 'sd_view'
      functors[ 'viewer-set-view' ]( viewType )

      scalarParameters = \
        str( parameters[ 'sdKernelVoxelCount' ].getValue() ) + ' ' + \
        str( parameters[ 'sdKernelLowerFAThreshold' ].getValue() ) + ' ' +  \
        str( parameters[ 'sdKernelUpperFAThreshold' ].getValue() ) + ' ' +  \
        str( parameters[ 'sdMaximumSHOrder' ].getValue() ) + ' ' +  \
        str( parameters[ 'sdFilterCoefficients' ].getValue() ) + ' ' +  \
        str( parameters[ 'sdUseCSD' ].getValue() ) + ' '
    
      stringParameters = parameters[ 'sdKernelType' ].getChoice() + ' '

      odfFunctorList = [ 'dw_sh',
                         'gfa',
                         'mean_diffusivity',
                         'mean_dwi',
                         'nematic_order',
                         'normalized_entropy',
                         'normalized_gfa',
                         'normalized_mean_diffusivity',
                         'normalized_mean_dwi',
                         'odf_sh',
                         'odf_site_map',
                         'odf_texture_map',
                         'principal_orientation',
                         'rgb',
                         'standard_deviation' ]
      prefix = 'sd'
      odfFunctorNames, \
      prefixedOdfFunctorFileNameMap, \
      prefixedOdfFunctorFileNames = \
        self.getPrefixedOdfFunctorNamesAndFileNames( odfFunctorList, prefix,
                                                     outputWorkDirectory )

    ########################### SDT model ######################################
    elif ( odfTypeIndex == 4 ):
    
      odfType = 'sdt_odf_cartesian_field'
      viewType = 'sdt_view'
      functors[ 'viewer-set-view' ]( viewType )

      scalarParameters = \
        str( parameters[ 'sdtKernelVoxelCount' ].getValue() ) + ' ' + \
        str( parameters[ 'sdtKernelLowerFAThreshold' ].getValue() ) + ' ' +  \
        str( parameters[ 'sdtKernelUpperFAThreshold' ].getValue() ) + ' ' +  \
        str( parameters[ 'sdtMaximumSHOrder' ].getValue() ) + ' ' +  \
        str( parameters[ 'sdtRegularizationLcurveFactor' ].getValue() ) + \
        ' ' +  \
        str( parameters[ 'sdtUseCSD' ].getValue() ) + ' '
    
      stringParameters = parameters[ 'sdtKernelType' ].getChoice() + ' '

      odfFunctorList = [ 'dw_sh',
                         'gfa',
                         'mean_diffusivity',
                         'mean_dwi',
                         'nematic_order',
                         'normalized_entropy',
                         'normalized_gfa',
                         'normalized_mean_diffusivity',
                         'normalized_mean_dwi',
                         'odf_sh',
                         'odf_site_map',
                         'odf_texture_map',
                         'principal_orientation',
                         'rgb',
                         'standard_deviation' ]
      prefix = 'sdt'
      odfFunctorNames, \
      prefixedOdfFunctorFileNameMap, \
      prefixedOdfFunctorFileNames = \
        self.getPrefixedOdfFunctorNamesAndFileNames( odfFunctorList, prefix,
                                                     outputWorkDirectory )

    ########################### aQBI model #####################################
    elif ( odfTypeIndex == 5 ):
    
      odfType = 'aqball_odf_cartesian_field'
      viewType = 'aqbi_view'
      functors[ 'viewer-set-view' ]( viewType )

      scalarParameters = \
        str( parameters[ 'aqbiMaximumSHOrder' ].getValue() ) + ' ' + \
        str( parameters[ 'aqbiRegularizationLcurveFactor' ].getValue() ) + \
        ' ' +  \
        str( parameters[ 'aqbiLaplaceBeltramiSharpeningFactor' ].getValue() ) +\
        ' '

      odfFunctorList = [ 'dw_sh',
                         'gfa',
                         'mean_diffusivity',
                         'mean_dwi',
                         'nematic_order',
                         'normalized_entropy',
                         'normalized_gfa',
                         'normalized_mean_diffusivity',
                         'normalized_mean_dwi',
                         'odf_sh',
                         'odf_site_map',
                         'odf_texture_map',
                         'principal_orientation',
                         'rgb',
                         'standard_deviation' ]
      prefix = 'aqbi'
      odfFunctorNames, \
      prefixedOdfFunctorFileNameMap, \
      prefixedOdfFunctorFileNames = \
        self.getPrefixedOdfFunctorNamesAndFileNames( odfFunctorList, prefix,
                                                     outputWorkDirectory )

    ########################### sa-aQBI model ##################################
    elif ( odfTypeIndex == 6 ):
    
      odfType = 'sa-aqball_odf_cartesian_field'
      viewType = 'sa-aqbi_view'
      functors[ 'viewer-set-view' ]( viewType )

      scalarParameters = \
        str( parameters[ 'saAqbiMaximumSHOrder' ].getValue() ) + ' ' + \
        str( parameters[ 'saAqbiRegularizationLcurveFactor' ].getValue() ) + \
        ' ' +  \
        str( parameters[ 'saAqbiLaplaceBeltramiSharpeningFactor' ].getValue() ) +\
        ' '

      odfFunctorList = [ 'dw_sh',
                         'gfa',
                         'mean_diffusivity',
                         'mean_dwi',
                         'nematic_order',
                         'normalized_entropy',
                         'normalized_gfa',
                         'normalized_mean_diffusivity',
                         'normalized_mean_dwi',
                         'odf_sh',
                         'odf_site_map',
                         'odf_texture_map',
                         'principal_orientation',
                         'rgb',
                         'standard_deviation' ]
      prefix = 'sa-aqbi'
      odfFunctorNames, \
      prefixedOdfFunctorFileNameMap, \
      prefixedOdfFunctorFileNames = \
        self.getPrefixedOdfFunctorNamesAndFileNames( odfFunctorList, prefix,
                                                     outputWorkDirectory )

    ########################### DTI model ######################################
    elif ( odfTypeIndex == 7 ):
    
      odfType = 'tensor_odf_cartesian_field'
      viewType = 'dti_view'
      functors[ 'viewer-set-view' ]( viewType )

      stringParameters = parameters[ 'dtiEstimatorType' ].getChoice() + ' '

      odfFunctorList = [ 'gfa',
                         'mean_diffusivity',
                         'mean_dwi',
                         'nematic_order',
                         'normalized_entropy',
                         'normalized_gfa',
                         'normalized_mean_diffusivity',
                         'normalized_mean_dwi',
                         'odf_site_map',
                         'odf_texture_map',
                         'principal_orientation',
                         'rgb',
                         'standard_deviation' ]
      prefix = 'dti'
      odfFunctorNames, \
      prefixedOdfFunctorFileNameMap, \
      prefixedOdfFunctorFileNames = \
        self.getPrefixedOdfFunctorNamesAndFileNames( odfFunctorList, prefix,
                                                     outputWorkDirectory )

    ########################### RGB scale ######################################
    rgbScale = str( parameters[ 'rgbScale' ].getValue() ) + ' '

    ########################### output orientation count #######################
    outputOrientationCount = \
      str( parameters[ 'outputOrientationCount' ].getValue() ) + ' '

    ########################### ODF volume computation #########################
    computeOdfVolume = parameters[ 'computeOdfVolume' ].getValue()
    if ( computeOdfVolume == 2 ):

      odfFunctorList.append( 'minmax_normalized_volume' )

    ########################### loading T2,DW and mask #########################

    functors[ 'viewer-create-referential' ]( 'frameDW' )
    
    functors[ 'viewer-load-object' ]( fileNameT2, 'T2' )
    functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'T2' )

    functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                       viewType, 'T2(b=0)' )
    functors[ 'viewer-add-object-to-window' ]( 'T2', viewType, 'T2(b=0)' )

    functors[ 'viewer-load-object' ]( fileNameDw, 'DW' )
    functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'DW' )

    functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                       viewType, 'DW' )
    functors[ 'viewer-add-object-to-window' ]( 'DW', viewType, 'DW' )

    functors[ 'viewer-load-object' ]( fileNameMask, 'mask' )
    functors[ 'viewer-set-colormap' ]( 'mask', 'BLUE-ufusion' )
    functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'mask' )

    functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                       viewType, 'mask' )
    functors[ 'viewer-add-object-to-window' ]( 'mask', viewType, 'mask' )

    functors[ 'viewer-fusion-objects' ]( [ 'T2', 'mask'], 'fusionT2AndMask',
                                         'Fusion2DMethod' )
    functors[ 'viewer-assign-referential-to-object' ]( 'frameDW',
                                                       'fusionT2AndMask' )

    functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                       viewType, 'T2 + mask' )
    functors[ 'viewer-add-object-to-window' ]( 'fusionT2AndMask', viewType,
                                               'T2 + mask' )

    ########################### loading T1 and DW->T1 transform ################

    if ( len( fileNameT1 ) ):
    
      functors[ 'viewer-create-referential' ]( 'frameT1' )

      functors[ 'viewer-load-object' ]( fileNameT1, 'T1' )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameT1', 'T1' )

      if ( len( fileNameTransformationDwToT1 ) ):
      

        functors[ 'viewer-load-transformation' ]( fileNameTransformationDwToT1,
                                                  'frameDW', 'frameT1' )

      functors[ 'viewer-fusion-objects' ]( [ 'T1', 'mask'], 'fusionT1AndMask',
                                             'Fusion2DMethod' )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameT1',
                                                         'fusionT1AndMask' )
                                                         
      functors[ 'viewer-assign-referential-to-window' ]( 'frameT1',
                                                         viewType, 'T1 + mask' )
      functors[ 'viewer-add-object-to-window' ]( 'fusionT1AndMask', viewType,
                                                 'T1 + mask' )


    ########################### GkgDwiOdfField #################################
    scalarParametersOption = ''
    if ( len( scalarParameters ) ):

      scalarParametersOption = ' -scalarParameters ' + scalarParameters

    stringParametersOption = ''
    if ( len( stringParameters ) ):

      stringParametersOption = ' -stringParameters ' + stringParameters
    
    ########################### further things #################################

    if ( odfTypeIndex == 7 ):
    
      prefixedOdfFunctorFileNameMap[ 'adc' ] = \
        os.path.join( outputWorkDirectory,
                      prefix + '_' + 'adc' )
      prefixedOdfFunctorFileNameMap[ 'diffusion_weighted_t2' ] = \
        os.path.join( outputWorkDirectory,
                      prefix + '_' + 'diffusion_weighted_t2' )
      prefixedOdfFunctorFileNameMap[ 'dij' ] = \
        os.path.join( outputWorkDirectory,
                      prefix + '_' + 'dij' )
      prefixedOdfFunctorFileNameMap[ 'eigenvalues' ] = \
        os.path.join( outputWorkDirectory,
                      prefix + '_' + 'eigenvalues' )
      prefixedOdfFunctorFileNameMap[ 'fa' ] = \
        os.path.join( outputWorkDirectory,
                      prefix + '_' + 'fa' )
      prefixedOdfFunctorFileNameMap[ 'lambda_parallel' ] = \
        os.path.join( outputWorkDirectory,
                      prefix + '_' + 'lambda_parallel' )
      prefixedOdfFunctorFileNameMap[ 'lambda_transverse' ] = \
        os.path.join( outputWorkDirectory,
                      prefix + '_' + 'lambda_transverse' )
      prefixedOdfFunctorFileNameMap[ 'rgb' ] = \
        os.path.join( outputWorkDirectory,
                      prefix + '_' + 'rgb' )
      prefixedOdfFunctorFileNameMap[ 'tensor_site_map' ] = \
        os.path.join( outputWorkDirectory,
                      prefix + '_' + 'tensor_site_map' )
      prefixedOdfFunctorFileNameMap[ 'tensor_texture_map' ] = \
        os.path.join( outputWorkDirectory,
                      prefix + '_' + 'tensor_texture_map' )
      prefixedOdfFunctorFileNameMap[ 'volume_ratio' ] = \
        os.path.join( outputWorkDirectory,
                      prefix + '_' + 'volume_ratio' )

    ####################### waiting for result ################################
    functors[ 'condition-wait-and-release' ]( subjectName,
                                              'odf-field-processed' )

    ########################### DOT model ######################################
    if ( odfTypeIndex == 0 ):
    
      functors[ 'viewer-load-object' ]( prefixedOdfFunctorFileNameMap[ \
                                        'nematic_order'],
                                        'Nematic Order' )
      functors[ 'viewer-set-colormap' ]( 'Nematic Order',
                                         'B-W LINEAR', -2.0, 10.0 )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW',
                                                         'Nematic Order' )
 
      functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                         viewType,
                                                         'Nematic order' )
      functors[ 'viewer-add-object-to-window' ]( 'Nematic Order',
                                                 viewType, 
                                                 'Nematic order' )
 

      functors[ 'viewer-load-object' ]( prefixedOdfFunctorFileNameMap[ 'gfa' ],
                                        'GFA' )
      functors[ 'viewer-set-colormap' ]( 'GFA', 'RED TEMPERATURE', 0.0, 0.7 )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'GFA' )

      functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                         viewType, 'GFA' )
      functors[ 'viewer-add-object-to-window' ]( 'GFA', viewType, 'GFA' )


      functors[ 'viewer-load-object' ]( prefixedOdfFunctorFileNameMap[ \
                                        'normalized_entropy' ],
                                        'Normalized Entropy' )
      functors[ 'viewer-set-colormap' ]( 'Normalized Entropy',
                                         'B-W LINEAR', -2.0, 10.0 )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW',
                                                         'Normalized Entropy' )

      functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                         viewType,
                                                         'Normalized entropy' )
      functors[ 'viewer-add-object-to-window' ]( 'Normalized Entropy',
                                                 viewType,
                                                 'Normalized entropy' )

      functors[ 'viewer-load-object' ]( prefixedOdfFunctorFileNameMap[ \
                                          'mean_diffusivity' ],
                                        'MD' )
      functors[ 'viewer-set-colormap' ]( 'MD', 'RAINBOW', 0.0, 5e-9 )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW',
                                                         'MD' )

      functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                         viewType,
                                                         'Mean diffusivity' )
      functors[ 'viewer-add-object-to-window' ]( 'MD',
                                                 viewType,
                                                 'Mean diffusivity' )

    ########################### DSI model ######################################
    elif ( odfTypeIndex == 1 ):
    
      functors[ 'viewer-load-object' ]( prefixedOdfFunctorFileNameMap[ \
                                        'mean_squared_displacement'],
                                        'MSD' )
      functors[ 'viewer-set-colormap' ]( 'MSD',
                                         'B-W LINEAR', 0.0, 1e-4 )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW',
                                                         'Nematic Order' )
 
      functors[ 'viewer-assign-referential-to-window' ]( \
                                                       'frameDW',
                                                       viewType,
                                                       'Mean squared displac.' )
      functors[ 'viewer-add-object-to-window' ]( 'MSD',
                                                 viewType, 
                                                 'Mean squared displac.' )
 

      functors[ 'viewer-load-object' ]( prefixedOdfFunctorFileNameMap[ 'gfa' ],
                                        'GFA' )
      functors[ 'viewer-set-colormap' ]( 'GFA', 'RED TEMPERATURE', 0.0, 0.7 )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'GFA' )

      functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                         viewType, 'GFA' )
      functors[ 'viewer-add-object-to-window' ]( 'GFA', viewType, 'GFA' )


      functors[ 'viewer-load-object' ]( prefixedOdfFunctorFileNameMap[ \
                                        'return_to_origin_probability' ],
                                        'RTO' )
      functors[ 'viewer-set-colormap' ]( 'RTO',
                                         'RAINBOW', 0.0, 1.0 )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW',
                                                         'RTO' )

      functors[ 'viewer-assign-referential-to-window' ]( \
                                                       'frameDW',
                                                       viewType,
                                                       'Return to orig. prob.' )
      functors[ 'viewer-add-object-to-window' ]( 'RTO',
                                                 viewType,
                                                 'Return to orig. prob.' )

      functors[ 'viewer-load-object' ]( prefixedOdfFunctorFileNameMap[ \
                                          'mean_diffusivity' ],
                                        'MD' )
      functors[ 'viewer-set-colormap' ]( 'MD', 'RAINBOW', 0.0, 5e-9 )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW',
                                                         'MD' )

      functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                         viewType,
                                                         'Mean diffusivity' )
      functors[ 'viewer-add-object-to-window' ]( 'MD',
                                                 viewType,
                                                 'Mean diffusivity' )

    ########################### QBI model ######################################
    elif ( odfTypeIndex == 2 ):
    
      functors[ 'viewer-load-object' ]( prefixedOdfFunctorFileNameMap[ \
                                        'nematic_order'],
                                        'Nematic Order' )
      functors[ 'viewer-set-colormap' ]( 'Nematic Order',
                                         'B-W LINEAR', -2.0, 10.0 )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW',
                                                         'Nematic Order' )
 
      functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                         viewType,
                                                         'Nematic order' )
      functors[ 'viewer-add-object-to-window' ]( 'Nematic Order',
                                                 viewType, 
                                                 'Nematic order' )
 

      functors[ 'viewer-load-object' ]( prefixedOdfFunctorFileNameMap[ 'gfa' ],
                                        'GFA' )
      functors[ 'viewer-set-colormap' ]( 'GFA', 'RED TEMPERATURE', 0.0, 0.7 )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'GFA' )

      functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                         viewType, 'GFA' )
      functors[ 'viewer-add-object-to-window' ]( 'GFA', viewType, 'GFA' )


      functors[ 'viewer-load-object' ]( prefixedOdfFunctorFileNameMap[ \
                                        'normalized_entropy' ],
                                        'Normalized Entropy' )
      functors[ 'viewer-set-colormap' ]( 'Normalized Entropy',
                                         'B-W LINEAR', -2.0, 10.0 )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW',
                                                         'Normalized Entropy' )

      functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                         viewType,
                                                         'Normalized entropy' )
      functors[ 'viewer-add-object-to-window' ]( 'Normalized Entropy',
                                                 viewType,
                                                 'Normalized entropy' )

      functors[ 'viewer-load-object' ]( prefixedOdfFunctorFileNameMap[ \
                                          'mean_diffusivity' ],
                                        'MD' )
      functors[ 'viewer-set-colormap' ]( 'MD', 'RAINBOW', 0.0, 5e-9 )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW',
                                                         'MD' )

      functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                         viewType,
                                                         'Mean diffusivity' )
      functors[ 'viewer-add-object-to-window' ]( 'MD',
                                                 viewType,
                                                 'Mean diffusivity' )
 
    ########################### SD / SDT / aQBI / sa-aQBI model(s) #############
    elif ( ( odfTypeIndex == 3 ) or \
           ( odfTypeIndex == 4 ) or \
           ( odfTypeIndex == 5 ) or \
           ( odfTypeIndex == 6 ) ):
    
      functors[ 'viewer-load-object' ]( prefixedOdfFunctorFileNameMap[ 'dw_sh'],
                                        'DW-SH' )
      functors[ 'viewer-set-colormap' ]( 'DW-SH', 'B-W LINEAR', -2.0, 10.0 )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'DW-SH' )
 
      functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                         viewType,
                                                         'DW SH coef.' )
      functors[ 'viewer-add-object-to-window' ]( 'DW-SH',
                                                 viewType, 
                                                 'DW SH coef.' )
 

      functors[ 'viewer-load-object' ]( prefixedOdfFunctorFileNameMap[ 'gfa' ],
                                        'GFA' )
      functors[ 'viewer-set-colormap' ]( 'GFA', 'RED TEMPERATURE', 0.0, 0.7 )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'GFA' )

      functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                         viewType, 'GFA' )
      functors[ 'viewer-add-object-to-window' ]( 'GFA', viewType, 'GFA' )


      functors[ 'viewer-load-object' ]( prefixedOdfFunctorFileNameMap[ \
                                          'odf_sh' ],
                                        'ODF-SH' )
      functors[ 'viewer-set-colormap' ]( 'ODF-SH', 'B-W LINEAR', -2.0, 10.0 )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW',
                                                         'ODF-SH' )

      functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                         viewType,
                                                         'ODF SH coef.' )
      functors[ 'viewer-add-object-to-window' ]( 'ODF-SH',
                                                 viewType,
                                                 'ODF SH coef.' )

      functors[ 'viewer-load-object' ]( prefixedOdfFunctorFileNameMap[ \
                                          'mean_diffusivity' ],
                                        'MD' )
      functors[ 'viewer-set-colormap' ]( 'MD', 'RAINBOW', 0.0, 5e-9 )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW',
                                                         'MD' )

      functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                         viewType,
                                                         'Mean diffusivity' )
      functors[ 'viewer-add-object-to-window' ]( 'MD',
                                                 viewType,
                                                 'Mean diffusivity' )

    ########################### DTI model ######################################
    elif ( odfTypeIndex == 7 ):
    
      functors[ 'viewer-load-object' ]( prefixedOdfFunctorFileNameMap[ 'adc' ],
                                        'ADC' )
      functors[ 'viewer-set-colormap' ]( 'ADC', 'RAINBOW', 0.0, 5e-9 )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'ADC' )
 
      functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                         viewType, 'ADC' )
      functors[ 'viewer-add-object-to-window' ]( 'ADC', viewType, 'ADC' )
 

      functors[ 'viewer-load-object' ]( prefixedOdfFunctorFileNameMap[ 'fa' ],
                                        'FA' )
      functors[ 'viewer-set-colormap' ]( 'FA', 'RED TEMPERATURE', 0.0, 0.9 )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'FA' )

      functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                         viewType, 'FA' )
      functors[ 'viewer-add-object-to-window' ]( 'FA', viewType, 'FA' )


      functors[ 'viewer-load-object' ]( prefixedOdfFunctorFileNameMap[ \
                                          'lambda_parallel' ],
                                        'D(parallel)' )
      functors[ 'viewer-set-colormap' ]( 'D(parallel)', 'RAINBOW', 0.0, 3e-9 )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW',
                                                         'D(parallel)' )

      functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                         viewType,
                                                         'D(parallel)' )
      functors[ 'viewer-add-object-to-window' ]( 'D(parallel)',
                                                 viewType,
                                                 'D(parallel)' )

      functors[ 'viewer-load-object' ]( prefixedOdfFunctorFileNameMap[ \
                                          'lambda_transverse' ],
                                          'D(transverse)' )
      functors[ 'viewer-set-colormap' ]( 'D(transverse)', 'RAINBOW', 0.0, 3e-9 )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW',
                                                         'D(transverse)' )

      functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                         viewType,
                                                         'D(transverse)' )
      functors[ 'viewer-add-object-to-window' ]( 'D(transverse)',
                                                 viewType,
                                                 'D(transverse)' )

    ########################### adding T1 & RGB fusion #########################
    
    if ( len( fileNameT1 ) ):
    
      functors[ 'viewer-load-object' ]( prefixedOdfFunctorFileNameMap[ 'rgb' ],
                                        'RGB' )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'RGB' )

      functors[ 'viewer-fusion-objects' ]( [ 'T1', 'RGB'], 'fusionT1AndRGB',
                                           'Fusion2DMethod' )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameT1',
                                                         'fusionT1AndRGB' )

      functors[ 'viewer-assign-referential-to-window' ]( 'frameT1',
                                                         viewType,
                                                         'T1 + RGB (ax)' )
      functors[ 'viewer-add-object-to-window' ]( 'fusionT1AndRGB', viewType,
                                                 'T1 + RGB (ax)' )

      functors[ 'viewer-assign-referential-to-window' ]( 'frameT1',
                                                         viewType,
                                                         'T1 + RGB (cor)' )
      functors[ 'viewer-add-object-to-window' ]( 'fusionT1AndRGB', viewType,
                                                 'T1 + RGB (cor)' )

      functors[ 'viewer-assign-referential-to-window' ]( 'frameT1',
                                                         viewType,
                                                         'T1 + RGB (sag)' )
      functors[ 'viewer-add-object-to-window' ]( 'fusionT1AndRGB', viewType,
                                                 'T1 + RGB (sag)' )


    ########################### adding ODF field in viewer #####################
    odfTextureMapFileName = \
      prefixedOdfFunctorFileNameMap[ 'odf_texture_map' ] + '.texturemap'
    functors[ 'viewer-load-object' ]( odfTextureMapFileName, 'ODF field' )
    functors[ 'viewer-add-object-to-window' ]( 'ODF field',
                                               viewType, 'ODF field' )

    ########################### updating progress bar ##########################
    functors[ 'update-progress-bar' ]( subjectName, 100 )
