from Algorithm import *
from DwiBlockFilteringTask import *


class DwiBlockFilteringAlgorithm( Algorithm ):

  def __init__( self, verbose ):

    Algorithm.__init__( self, 'DWI-Block-Filtering', verbose,
                        True )

    # diffusion-weighted input data
    self.addParameter( StringParameter( 'blockDirectory', '' ) )
    self.addParameter( BooleanParameter( 'leftHemisphere', True ) ) 
    self.addParameter( StringParameter( 'blockLetter', '' ) )
    self.addParameter( IntegerParameter( 'fovNumber', \
                                          1, 1, 4, 1 ) )

    #Filtering parameters
    self.addParameter( DoubleParameter( 'noiseSigma', \
                                        20, 1, 100, 0.1 ) )
    self.addParameter( DoubleParameter( 'filteringRate', \
                                        0.3, 0.0, 5, 0.01 ) )
                                        

  def launch( self ):

    if ( self._verbose ):

      print \
        'running Block filtering'

    task = DwiBlockFilteringTask( self._application ) 
    task.launch( False )


  def view( self ):

    if ( self._verbose ):

      print \
        'viewing Block Filtering'

    task = DwiBlockFilteringTask( self._application )
    task.launch( True )

