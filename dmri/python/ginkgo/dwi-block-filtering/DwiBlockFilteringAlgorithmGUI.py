from AlgorithmGUI import *
from ResourceManager import *


class DwiBlockFilteringAlgorithmGUI( AlgorithmGUI ):

  def __init__( self, algorithm ):

    AlgorithmGUI.__init__( self,
                           algorithm,
                           ResourceManager().getUIFileName(
                                         'dmri',
                                         'DwiBlockFiltering.ui' ) )

    ############################################################################
    # connecting block & FOV directory 
    ############################################################################

    self.connectStringParameterToLineEdit( 'blockDirectory',
                                           'lineEdit_BlockDirectory' )    
    self.connectDirectoryBrowserToPushButtonAndLineEdit( \
                                                   'pushButton_BlockDirectory',
                                                   'lineEdit_BlockDirectory' )
    self.connectBooleanParameterToRadioButton( \
                                         'leftHemisphere',
                                         'radioButton_LeftHemisphere' )
    self.connectStringParameterToLineEdit( 'blockLetter',
                                           'lineEdit_BlockLetter' )    
    self.connectIntegerParameterToSpinBox( \
                                      'fovNumber',
                                      'spinBox_FovNumber' )

    ############################################################################
    # connecting filtering parameters
    ############################################################################


    self.connectDoubleParameterToSpinBox( \
                                      'noiseSigma',
                                      'doubleSpinBox_NoiseSigma' )
    self.connectDoubleParameterToSpinBox( 'filteringRate',
                                          'doubleSpinBox_FilteringRate' )
  
                                          
