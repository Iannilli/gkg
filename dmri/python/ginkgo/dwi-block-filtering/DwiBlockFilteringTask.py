from Task import *
import shutil
from os import listdir

class DwiBlockFilteringTask( Task ):

  def __init__( self, application ):

    Task.__init__( self, application )

    self.addFunctor( 'viewer-set-dw-information',
                     self.viewerSetDwInformation )


  def viewerSetDwInformation( self,
                              sliceAxis,
                              sizeX, sizeY, sizeZ,
                              resolutionX, resolutionY, resolutionZ ):

    if ( self._application.getBatchMode() == False ):

      self._application.getMainThreadActionManager().call(
        self._application.getViewer().setDwInformation,
        sliceAxis, sizeX, sizeY, sizeZ, resolutionX, resolutionY, resolutionZ )

  ##############################################################################
  # execute()
  ##############################################################################



  def execute( self, functors, parameters, subjectName, verbose, viewOnly ):

    try:

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 0 )

      ####################### calculating output work directory ################

      fovDirectory = self.generateFovPath(parameters)

      if ( verbose ):

        print  'Output work directory :', fovDirectory + "02-DW/03-Filtering/dw_filtered "

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 5 )

      ####################### creating mask from average T2 ####################
        
      noiseSigma = parameters[ 'noiseSigma' ].getValue()
      filteringRate = parameters[ 'filteringRate' ].getValue()

      self.launchFilteringDW(fovDirectory, noiseSigma,filteringRate, 
        subjectName,verbose,viewOnly )

      functors[ 'update-progress-bar' ]( subjectName, 80 )
      self.launchFilteringT2(fovDirectory, noiseSigma,filteringRate, 
        subjectName,verbose,viewOnly )
                                        
      functors[ 'update-progress-bar' ]( subjectName, 90 )
      self.createRGB(fovDirectory,subjectName,verbose,viewOnly)

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 100 )

    except:

      self._functors[ 'task-set-status-as-failed' ]( subjectName,
                                                     sys.exc_info() )

  ##############################################################################
  # creating mask
  ##############################################################################

  def generateFovPath(self,parameters):

    blocksDirectory = parameters['blockDirectory'].getValue()
    leftHemisphere = parameters[ 'leftHemisphere' ].getValue()
    blockLetter = parameters[ 'blockLetter' ].getValue()
    fovNumber = parameters[ 'fovNumber' ].getValue()

    if leftHemisphere:  

        hemisphereDirectory = blocksDirectory + '/' + "LeftHemisphere" 

    else:

        hemisphereDirectory = blocksDirectory + '/' + "RightHemisphere" 

    return hemisphereDirectory + '/' + str(blockLetter) + '/' + \
    str(blockLetter) + str(fovNumber) +'/'

  def launchFilteringDW( self,fovDirectory, noiseSigma,filteringRate, \
    subjectName,verbose,viewOnly ):

    dwRawFile = fovDirectory + "02-DW/01-DWInQSpace-b1500-4500-8000/dw.ima"
    dwFilteredFile = fovDirectory + "02-DW/03-Filtering/dw_filtered.ima"

    command = "GkgExecuteCommand NonLocalMeansFilter -i " + dwRawFile + " \
      -o " + dwFilteredFile + " \
      -s " + str(noiseSigma) + " \
      -d " + str(filteringRate)

    executeCommand( self, subjectName, command, viewOnly )

    shutil.copyfile(dwRawFile + ".minf" , dwFilteredFile + ".minf")

  def launchFilteringT2( self,fovDirectory, noiseSigma,filteringRate, \
    subjectName,verbose,viewOnly ):

    if 't2_still.ima' in listdir(fovDirectory + "02-DW/02-Registration"):

      t2RawFile = fovDirectory + "02-DW/02-Registration/t2_still.ima"

    else:

      t2RawFile = fovDirectory + "02-DW/01-DWInQSpace-b1500-4500-8000/t2.ima"

    t2FilteredFile = fovDirectory + "02-DW/03-Filtering/t2_filtered.ima"

    command = "GkgExecuteCommand NonLocalMeansFilter -i " + t2RawFile + " \
      -o " + t2FilteredFile + " \
      -s " + str(noiseSigma) + " \
      -d " + str(filteringRate)

    executeCommand( self, subjectName, command, viewOnly )

  def createRGB( self,fovDirectory,subjectName,verbose,viewOnly ):

    fileNameAverageT2 = fovDirectory + '02-DW/03-Filtering/t2_filtered'
    fileNameDW = fovDirectory + "02-DW/03-Filtering/dw_filtered"
    outputWorkDirectory = fovDirectory + '02-DW/03-Filtering'

    fileNameRGB = os.path.join( outputWorkDirectory, 'dti_rgb_filtered' )
    command = 'GkgExecuteCommand DwiTensorField' + \
              ' -t2 ' + fileNameAverageT2 + \
              ' -dw ' + fileNameDW + \
              ' -f rgb ' + \
              ' -o ' + fileNameRGB + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )




  ##############################################################################
  # display()
  ##############################################################################

  def display( self, 
    functors, 
    parameters, 
    subjectName, 
    verbose, 
    isReportModeActive ):

    ########################### resetting the viewer ###########################
    functors[ 'viewer-reset' ]()

    ####################### collecting output work directory ###################
    outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()
    rawDwiDirectory = parameters[ 'rawDwiDirectory' ].getValue()
    fileNameAverageT2 = os.path.join( rawDwiDirectory, 't2' )
    if ( verbose ):
    
      print  'Output work directory :', outputWorkDirectory

    if ( verbose ):

      print 'reading \'' + fileNameAverageT2 + '\''


    ####################### creating mask from average T2 ######################
    if ( parameters[ 'strategyRoughMaskFromT2' ].getValue() ):
    
      viewType = 'first_view'
      functors[ 'viewer-set-view' ]( viewType )  

      functors[ 'viewer-create-referential' ]( 'frameDW' )

      functors[ 'viewer-load-object' ]( fileNameAverageT2, 'averageT2' )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'averageT2' )

      functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                         'first_view',
                                                         'Average T2(b=0)' )
      functors[ 'viewer-add-object-to-window' ]( 'averageT2',
                                                 'first_view',
                                                 'Average T2(b=0)' )
                                                 
      noiseThresholdPercentage = \
                               parameters[ 'noiseThresholdPercentage' ].getValue()
      maskClosingRadius = parameters[ 'maskClosingRadius' ].getValue()
      maskDilationRadius = parameters[ 'maskDilationRadius' ].getValue()
      fileNameMask = self.createMask( functors,
                                      fileNameAverageT2,
                                      noiseThresholdPercentage,
                                      maskClosingRadius,
                                      maskDilationRadius,
                                      outputWorkDirectory,
                                      subjectName,
                                      verbose,
                                      True )

      functors[ 'condition-wait-and-release' ]( subjectName, 'mask-processed' )

      if ( verbose ):

        print 'reading \'' + fileNameMask + '\''
      functors[ 'viewer-load-object' ]( fileNameMask, 'mask' )
      functors[ 'viewer-set-colormap' ]( 'mask', 'BLUE-ufusion' )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'mask' )

      functors[ 'viewer-fusion-objects' ]( [ 'averageT2', 'mask' ],
                                             'fusionAverageT2AndMask',
                                             'Fusion2DMethod' )


      functors[ 'viewer-assign-referential-to-object' ]( \
                                                        'frameDW',
                                                        'fusionAverageT2AndMask' )
      functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                         'first_view',
                                                         'Average T2(b=0)+mask' )
      functors[ 'viewer-add-object-to-window' ]( 'fusionAverageT2AndMask',
                                                 'first_view',
                                                 'Average T2(b=0)+mask' )

    ####################### creating mask from morphologist  ###################
    else : 
    
      viewType = 'second_view'
      functors[ 'viewer-set-view' ]( viewType )        
      
      functors[ 'viewer-create-referential' ]( 'frameDW' )

      functors[ 'viewer-load-object' ]( fileNameAverageT2, 'AverageT2' )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW',
                                                         'AverageT2' )

      functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                         'second_view',
                                                         'Average T2(b=0)' )
      functors[ 'viewer-add-object-to-window' ]( 'AverageT2',
                                                 'second_view',
                                                 'Average T2(b=0)' )
      
      ####################### collecting output work directory ###################
      rawDwiDirectory = parameters[ 'rawDwiDirectory' ].getValue()
      outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()

      fileNameAnatomy = parameters[ 'anatomy' ].getValue()
      fileNameAverageT2 = os.path.join( rawDwiDirectory,
                                        't2.ima' )
      fileNameDwToT1Transform3d = os.path.join( outputWorkDirectory,
                                                'dw_to_t1.trm' )

      ############################# reading T2(b=0) ##############################
      #functors[ 'viewer-create-referential' ]( 'frameDW' )
      #functors[ 'viewer-load-object' ]( fileNameAverageT2, 'AverageT2' )
      #functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'AverageT2' )
      functors[ 'viewer-set-colormap' ]( 'AverageT2', 'RED TEMPERATURE' )

      ####################### reading and displaying T1 ##########################
      functors[ 'viewer-create-referential' ]( 'frameT1' )
      functors[ 'viewer-load-object' ]( fileNameAnatomy, 'T1' )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameT1', 'T1' )

      functors[ 'condition-wait-and-release' ]( subjectName,
                                                't1-to-dw-matching-processed' )
                                                
      functors[ 'viewer-load-transformation' ]( fileNameDwToT1Transform3d,
                                                'frameDW', 'frameT1' )

      functors[ 'viewer-fusion-objects' ]( [ 'T1', 'AverageT2' ],
                                           'fusionT1AndAverageT2',
                                           'Fusion2DMethod' )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameT1',
                                                         'fusionT1AndAverageT2' )
      functors[ 'viewer-assign-referential-to-window' ]( 'frameT1',    
                                                         'second_view',
                                                         'T1 + Average T2(b=0)' )
      functors[ 'viewer-add-object-to-window' ]( 'fusionT1AndAverageT2',
                                                 'second_view',
                                                 'T1 + Average T2(b=0)' )     
                                                 
      ####################### waiting for result ################################
      functors[ 'condition-wait-and-release' ]( subjectName, 'mask-processed' )
      
      ########################### display mask ###############################
      
      fileNameMask = os.path.join( outputWorkDirectory,
                                   'mask.ima' )
      functors[ 'viewer-load-object' ]( fileNameMask, 'mask' )
      functors[ 'viewer-set-colormap' ]( 'mask', 'BLUE-ufusion' )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'mask' )

      functors[ 'viewer-fusion-objects' ]( [ 'AverageT2', 'mask' ],
                                             'fusionAverageT2AndMask',
                                             'Fusion2DMethod' )


      functors[ 'viewer-assign-referential-to-object' ]( \
                                                        'frameDW',
                                                        'fusionAverageT2AndMask' )
      functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                         'second_view',
                                                         'Average T2(b=0)+mask' )
      functors[ 'viewer-add-object-to-window' ]( 'fusionAverageT2AndMask',
                                                 'second_view',
                                                 'Average T2(b=0)+mask' )

