from Algorithm import *
from DwiBlockPdfFieldTask import *


class DwiBlockPdfFieldAlgorithm( Algorithm ):

  def __init__( self, verbose ):

    Algorithm.__init__( self, 'DWI-Block-Pdf-Field', verbose,
                        True )

    self.addParameter( StringParameter( 'blockDirectory', '' ) )
    self.addParameter( BooleanParameter( 'leftHemisphere', True ) ) 
    self.addParameter( StringParameter( 'blockLetter', '' ) )
    self.addParameter( IntegerParameter( 'fovNumber', \
                                          1, 1, 4, 1 ) )

                                        
  def launch( self ):

    if ( self._verbose ):

      print \
        'running Block rough mask extraction'

    task = DwiBlockPdfFieldTask( self._application )
    task.launch( False )


  def view( self ):

    if ( self._verbose ):

      print \
        'viewing Block rough mask extraction'

    task = DwiBlockPdfFieldTask( self._application )
    task.launch( True )

