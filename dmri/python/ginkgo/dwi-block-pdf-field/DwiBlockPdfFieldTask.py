from Task import *
import shutil

class DwiBlockPdfFieldTask( Task ):

  def __init__( self, application ):

    Task.__init__( self, application )

    self.addFunctor( 'viewer-set-dw-information',
                     self.viewerSetDwInformation )


  def viewerSetDwInformation( self,
                              sliceAxis,
                              sizeX, sizeY, sizeZ,
                              resolutionX, resolutionY, resolutionZ ):

    if ( self._application.getBatchMode() == False ):

      self._application.getMainThreadActionManager().call(
        self._application.getViewer().setDwInformation,
        sliceAxis, sizeX, sizeY, sizeZ, resolutionX, resolutionY, resolutionZ )

  ##############################################################################
  # execute()
  ##############################################################################

  def execute( self, functors, parameters, subjectName, verbose, viewOnly ):

    try:

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 0 )

      ####################### collecting output work directory #################
      fovDirectory = self.generateFovPath(parameters)

      if ( verbose ):
      
        print('fov Directory : ' + fovDirectory)

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 5 )

      ####################### creating mask from average T2 ####################
        
      fileNameMask = self.calculateShorePdf( functors,
                                      fovDirectory,
                                      subjectName,
                                      verbose,
                                      viewOnly )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 100 )

    except:

      self._functors[ 'task-set-status-as-failed' ]( subjectName,
                                                     sys.exc_info() )

  def calculateShorePdf( self,
                  functors,
                  fovDirectory,
                  subjectName,
                  verbose,
                  viewOnly ):


    t2_filtered_file = fovDirectory + '02-DW/03-Filtering/t2_filtered.ima'
    dw_filtered_file = fovDirectory + '02-DW/03-Filtering/dw_filtered.ima'
    mask_file = fovDirectory + '02-DW/04-Mask/mask.ima'

    outputDirectory = fovDirectory + '02-DW/05-ShoreModeling/' 

    command = 'GkgExecuteCommand DwiPdfField \
                    -t2 ' +  t2_filtered_file + '\
                    -dw ' + dw_filtered_file + '\
                    -m ' + mask_file + '\
                    -type shore_pdf_cartesian_field \
                    -f odf_site_map \
                       odf_texture_map \
                       pdf_site_map \
                       pdf_texture_map \
                    -o ' + outputDirectory + 'odf_site_map '  \
                         + outputDirectory + 'odf_texture_map '  \
                         + outputDirectory + 'pdf_site_map '  \
                         + outputDirectory + 'pdf_texture_map  \
                    -outputSampling 1 1 60 10 60 100 60 \
                    -scalarParameters 4 0.006 \
                    -furtherStringParameters shore-to-sh-odf \
                    -furtherScalarParameters 200 6 0.006 \
                    -verbose'


    executeCommand( self, subjectName, command, viewOnly )

  ##############################################################################
  # display()
  ##############################################################################

  def display( self, functors, parameters, subjectName, verbose,
               isReportModeActive ):

    pass

  def generateFovPath(self,parameters):
    blocksDirectory = parameters['blockDirectory'].getValue()
    leftHemisphere = parameters[ 'leftHemisphere' ].getValue()
    blockLetter = parameters[ 'blockLetter' ].getValue()
    fovNumber = parameters[ 'fovNumber' ].getValue()

    if leftHemisphere:  

        hemisphereDirectory = blocksDirectory + '/' + "LeftHemisphere" 
    else:

        hemisphereDirectory = blocksDirectory + '/' + "RightHemisphere" 

    return hemisphereDirectory + '/' + str(blockLetter) + '/' \
    + str(blockLetter) + str(fovNumber) +'/'