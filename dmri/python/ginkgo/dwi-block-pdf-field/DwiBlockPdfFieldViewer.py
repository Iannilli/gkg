from Viewer import *


class DwiBlockPdfFieldViewer( Viewer ):

  def __init__( self, minimumSize, parent ):

    Viewer.__init__( self, minimumSize, parent )
    
    ########################## First View ######################################

    self.createView( 'first_view' )
    
    self.addAxialWindow( 'first_view', 0, 0, 2, 1,
                         'Average T2(b=0)', 'Average T2(b=0)' )
    self.addAxialWindow( 'first_view', 0, 1, 2, 1,
                         'Average T2(b=0)+mask', 'Average T2(b=0) + mask' )

    ########################## Second View ######################################

    self.createView( 'second_view' )
    
    self.addAxialWindow( 'second_view', 0, 0, 1, 1,
                         'Average T2(b=0)', 'Average T2(b=0)' )
    self.addAxialWindow( 'second_view', 1, 0, 1, 1,
                         'T1 + Average T2(b=0)', 'T1 + Average T2(b=0)' )
    self.addAxialWindow( 'second_view', 0, 1, 2, 1,
                         'Average T2(b=0)+mask', 'Average T2(b=0) + mask' )
                         
def createDwiBlockPdfFieldViewer( minimumSize, parent ):

  return DwiBlockPdfFieldViewer( minimumSize, parent )

