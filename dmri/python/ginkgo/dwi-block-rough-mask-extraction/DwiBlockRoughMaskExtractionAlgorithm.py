from Algorithm import *
from DwiBlockRoughMaskExtractionTask import *


class DwiBlockRoughMaskExtractionAlgorithm( Algorithm ):

  def __init__( self, verbose ):

    Algorithm.__init__( self, 'DWI-Block-Rough-Mask-Extraction', verbose,
                        True )

    self.addParameter( StringParameter( 'blockDirectory', '' ) )
    self.addParameter( BooleanParameter( 'leftHemisphere', True ) ) 
    self.addParameter( StringParameter( 'blockLetter', '' ) )
    self.addParameter( IntegerParameter( 'fovNumber', \
                                          1, 1, 4, 1 ) )

    # rough mask from T2(b=0)
    self.addParameter( DoubleParameter( 'noiseThresholdPercentage', \
                                        5, 1, 100, 0.5 ) )
    self.addParameter( DoubleParameter( 'maskClosingRadius', \
                                        0.0, 0.0, 100.0, 0.01 ) )
    self.addParameter( DoubleParameter( 'maskDilationRadius', \
                                        0.01, 0.0, 100.0, 0.01 ) )
                                        
  def launch( self ):

    if ( self._verbose ):

      print \
        'running Block rough mask extraction'

    task = DwiBlockRoughMaskExtractionTask( self._application )
    task.launch( False )


  def view( self ):

    if ( self._verbose ):

      print \
        'viewing Block rough mask extraction'

    task = DwiBlockRoughMaskExtractionTask( self._application )
    task.launch( True )

