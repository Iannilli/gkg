from AlgorithmGUI import *
from ResourceManager import *


class DwiBlockRoughMaskExtractionAlgorithmGUI( AlgorithmGUI ):

  def __init__( self, algorithm ):

    AlgorithmGUI.__init__( self,
                           algorithm,
                           ResourceManager().getUIFileName(
                                          'dmri',
                                          'DwiBlockRoughMaskExtraction.ui' ) )

    ############################################################################
    # connecting Block & FOV Interface
    ############################################################################

    self.connectStringParameterToLineEdit( 'blockDirectory',
                                           'lineEdit_BlockDirectory' )    
    self.connectDirectoryBrowserToPushButtonAndLineEdit( \
                                                   'pushButton_BlockDirectory',
                                                   'lineEdit_BlockDirectory' )
    self.connectBooleanParameterToRadioButton( \
                                         'leftHemisphere',
                                         'radioButton_LeftHemisphere' )
    self.connectStringParameterToLineEdit( 'blockLetter',
                                           'lineEdit_BlockLetter' )    
    self.connectIntegerParameterToSpinBox( \
                                      'fovNumber',
                                      'spinBox_FovNumber' )

   ############################################################################
    # connecting rough mask from T2(b=0) interface
    ############################################################################

    self.connectDoubleParameterToSpinBox( \
                                      'noiseThresholdPercentage',
                                      'doubleSpinBox_NoiseThresholdPercentage' )
    self.connectDoubleParameterToSpinBox( 'maskClosingRadius',
                                          'doubleSpinBox_MaskClosingRadius' )
    self.connectDoubleParameterToSpinBox( 'maskDilationRadius',
                                          'doubleSpinBox_MaskDilationRadius' )

