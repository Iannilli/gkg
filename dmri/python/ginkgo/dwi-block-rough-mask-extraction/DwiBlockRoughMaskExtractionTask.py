from Task import *
import os 

class DwiBlockRoughMaskExtractionTask( Task ):

  def __init__( self, application ):

    Task.__init__( self, application )

    self.addFunctor( 'viewer-set-dw-information',
                     self.viewerSetDwInformation )


  def viewerSetDwInformation( self,
                              sliceAxis,
                              sizeX, sizeY, sizeZ,
                              resolutionX, resolutionY, resolutionZ ):

    if ( self._application.getBatchMode() == False ):

      self._application.getMainThreadActionManager().call(
        self._application.getViewer().setDwInformation,
        sliceAxis, sizeX, sizeY, sizeZ, resolutionX, resolutionY, resolutionZ )

  ##############################################################################
  # execute()
  ##############################################################################

  def execute( self, functors, parameters, subjectName, verbose, viewOnly ):

    try:

      ####################### acquiring conditions #############################
      functors[ 'condition-acquire' ]( subjectName, 'mask-processed' )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 0 )

      ####################### collecting output work directory #################
      fovDirectory = self.generateFovPath(parameters)
      
      if ( verbose ):

        outputWorkDirectory = fovDirectory + "02-DW/04-Mask"

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 5 )

      ####################### creating mask from average T2 ####################
      if 't2_filtered.ima' in os.listdir(fovDirectory + "02-DW/03-Filtering"):

        fileNameT2 = fovDirectory + "02-DW/03-Filtering/t2_filtered.ima"
      
      else:
      
        fileNameT2 = fovDirectory + "02-DW/01-DWInQSpace-b1500-4500-8000/t2.ima"

      noiseThresholdPercentage = \
                           parameters[ 'noiseThresholdPercentage' ].getValue()
      maskClosingRadius = parameters[ 'maskClosingRadius' ].getValue()
      maskDilationRadius = parameters[ 'maskDilationRadius' ].getValue()
      
      fileNameMask = self.createMask( functors,
                                      fileNameT2,
                                      noiseThresholdPercentage,
                                      maskClosingRadius,
                                      maskDilationRadius,
                                      outputWorkDirectory,
                                      subjectName,
                                      verbose,
                                      viewOnly )

      ####################### notifying display thread #########################
      functors[ 'condition-notify-and-release' ]( subjectName,
                                                  'mask-processed' )


      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 100 )

    except:

      self._functors[ 'task-set-status-as-failed' ]( subjectName,
                                                     sys.exc_info() )

  ##############################################################################
  # creating mask
  ##############################################################################

  def createMask( self,
                  functors,
                  fileNameAverageT2, 
                  noiseThresholdPercentage,
                  maskClosingRadius,
                  maskDilationRadius,
                  outputWorkDirectory, 
                  subjectName,
                  verbose,
                  viewOnly ):

    furtherOption = ''
    if ( maskClosingRadius > 0.0 ):
    
      furtherOption = ' -r ' + str( maskClosingRadius )

    fileNameMask = os.path.join( outputWorkDirectory, 'mask' )
    command = 'GkgExecuteCommand GetMask' + \
              ' -i ' + \
              fileNameAverageT2 + \
              ' -o ' + \
              fileNameMask + \
              ' -a 2 ' + \
              ' -thresholdRatio ' + str( noiseThresholdPercentage / 100.0 ) + \
              furtherOption + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )

    command = 'GkgExecuteCommand MorphologicalOperation' + \
              ' -i ' + \
              fileNameMask + \
              ' -o ' + \
              fileNameMask + \
              ' -op dilation ' + \
              ' -r ' + str( maskDilationRadius ) + \
              ' -m gt -t1 0' + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )


  def generateFovPath(self,parameters):

    blocksDirectory = parameters['blockDirectory'].getValue()
    leftHemisphere = parameters[ 'leftHemisphere' ].getValue()
    blockLetter = parameters[ 'blockLetter' ].getValue()
    fovNumber = parameters[ 'fovNumber' ].getValue()

    if leftHemisphere:  

        hemisphereDirectory = blocksDirectory + '/' + "LeftHemisphere" 

    else:

        hemisphereDirectory = blocksDirectory + '/' + "RightHemisphere" 

    return hemisphereDirectory + '/' + str(blockLetter) + '/' \
    + str(blockLetter) + str(fovNumber) +'/'
    
    return fileNameMask

  ##############################################################################
  # display()
  ##############################################################################

  def display( self, functors, parameters, subjectName, verbose,
               isReportModeActive ):

    ########################### resetting the viewer ###########################
    functors[ 'viewer-reset' ]()

    ####################### collecting output work directory ###################
    outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()
    rawDwiDirectory = parameters[ 'rawDwiDirectory' ].getValue()
    fileNameAverageT2 = os.path.join( rawDwiDirectory, 't2' )
    if ( verbose ):
    
      print  'Output work directory :', outputWorkDirectory

    if ( verbose ):

      print 'reading \'' + fileNameAverageT2 + '\''


    ####################### creating mask from average T2 ######################
    if ( parameters[ 'strategyRoughMaskFromT2' ].getValue() ):
    
      viewType = 'first_view'
      functors[ 'viewer-set-view' ]( viewType )  

      functors[ 'viewer-create-referential' ]( 'frameDW' )

      functors[ 'viewer-load-object' ]( fileNameAverageT2, 'averageT2' )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW',
                                                         'averageT2' )

      functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                         'first_view',
                                                         'Average T2(b=0)' )
      functors[ 'viewer-add-object-to-window' ]( 'averageT2',
                                                 'first_view',
                                                 'Average T2(b=0)' )
                                                 
      noiseThresholdPercentage = \
                             parameters[ 'noiseThresholprdPercentage' ].getValue()
      maskClosingRadius = parameters[ 'maskClosingRadius' ].getValue()
      maskDilationRadius = parameters[ 'maskDilationRadius' ].getValue()
      fileNameMask = self.createMask( functors,
                                      fileNameAverageT2,
                                      noiseThresholdPercentage,
                                      maskClosingRadius,
                                      maskDilationRadius,
                                      outputWorkDirectory,
                                      subjectName,
                                      verbose,
                                      True )

      functors[ 'condition-wait-and-release' ]( subjectName, 'mask-processed' )

      if ( verbose ):

        print 'reading \'' + fileNameMask + '\''
      functors[ 'viewer-load-object' ]( fileNameMask, 'mask' )
      functors[ 'viewer-set-colormap' ]( 'mask', 'BLUE-ufusion' )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'mask' )

      functors[ 'viewer-fusion-objects' ]( [ 'averageT2', 'mask' ],
                                             'fusionAverageT2AndMask',
                                             'Fusion2DMethod' )


      functors[ 'viewer-assign-referential-to-object' ]( \
                                                        'frameDW',
                                                        'fusionAverageT2AndMask' )
      functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                         'first_view',
                                                         'Average T2(b=0)+mask' )
      functors[ 'viewer-add-object-to-window' ]( 'fusionAverageT2AndMask',
                                                 'first_view',
                                                 'Average T2(b=0)+mask' )

    ####################### creating mask from morphologist  ###################
    else : 
    
      viewType = 'second_view'
      functors[ 'viewer-set-view' ]( viewType )        
      
      functors[ 'viewer-create-referential' ]( 'frameDW' )

      functors[ 'viewer-load-object' ]( fileNameAverageT2, 'AverageT2' )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW',
                                                         'AverageT2' )

      functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                         'second_view',
                                                         'Average T2(b=0)' )
      functors[ 'viewer-add-object-to-window' ]( 'AverageT2',
                                                 'second_view',
                                                 'Average T2(b=0)' )
      
      ####################### collecting output work directory #################
      rawDwiDirectory = parameters[ 'rawDwiDirectory' ].getValue()
     
      outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()

      fileNameAnatomy = parameters[ 'anatomy' ].getValue()
      fileNameAverageT2 = os.path.join( rawDwiDirectory,
                                        't2.ima' )
      fileNameDwToT1Transform3d = os.path.join( outputWorkDirectory,
                                                'dw_to_t1.trm' )

      ############################# reading T2(b=0) ############################

      functors[ 'viewer-set-colormap' ]( 'AverageT2', 'RED TEMPERATURE' )

      ####################### reading and displaying T1 ########################
      functors[ 'viewer-create-referential' ]( 'frameT1' )
      functors[ 'viewer-load-object' ]( fileNameAnatomy, 'T1' )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameT1', 'T1' )

      functors[ 'condition-wait-and-release' ]( subjectName,
                                                't1-to-dw-matching-processed' )
                                                
      functors[ 'viewer-load-transformation' ]( fileNameDwToT1Transform3d,
                                                'frameDW', 'frameT1' )

      functors[ 'viewer-fusion-objects' ]( [ 'T1', 'AverageT2' ],
                                           'fusionT1AndAverageT2',
                                           'Fusion2DMethod' )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameT1',
                                                         'fusionT1AndAverageT2' )
      functors[ 'viewer-assign-referential-to-window' ]( 'frameT1',    
                                                         'second_view',
                                                         'T1 + Average T2(b=0)' )
      functors[ 'viewer-add-object-to-window' ]( 'fusionT1AndAverageT2',
                                                 'second_view',
                                                 'T1 + Average T2(b=0)' )     
                                                 
      ####################### waiting for result ################################
      functors[ 'condition-wait-and-release' ]( subjectName, 'mask-processed' )
      
      ########################### display mask ###############################
      
      fileNameMask = os.path.join( outputWorkDirectory,
                                   'mask.ima' )
      functors[ 'viewer-load-object' ]( fileNameMask, 'mask' )
      functors[ 'viewer-set-colormap' ]( 'mask', 'BLUE-ufusion' )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'mask' )

      functors[ 'viewer-fusion-objects' ]( [ 'AverageT2', 'mask' ],
                                             'fusionAverageT2AndMask',
                                             'Fusion2DMethod' )


      functors[ 'viewer-assign-referential-to-object' ]( \
                                                        'frameDW',
                                                        'fusionAverageT2AndMask' )
      functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                         'second_view',
                                                         'Average T2(b=0)+mask' )
      functors[ 'viewer-add-object-to-window' ]( 'fusionAverageT2AndMask',
                                                 'second_view',
                                                 'Average T2(b=0)+mask' )

