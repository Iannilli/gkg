
from Algorithm import *
from DwiBlockT2MultipleRegistrationTask import *


class DwiBlockT2MultipleRegistrationAlgorithm( Algorithm ):

  def __init__( self, verbose ):

    Algorithm.__init__( self, 'DWI-Block-T2-Multiple-Registration', verbose,
                        True )

    self.addParameter( StringParameter( 'blockDirectory', '' ) )
    self.addParameter( BooleanParameter( 'leftHemisphere', True ) ) 
    self.addParameter( StringParameter( 'blockLetter', '' ) )
    self.addParameter( IntegerParameter( 'fovNumber', \
                                          1, 1, 4, 1 ) )

    # rough mask from T2(b=0)
    self.addParameter( StringParameter ('T2sToRemove','') )
                                        
  def launch( self ):

    if ( self._verbose ):

      print \
        'running T2 multiple registration '

    task = DwiBlockT2MultipleRegistrationTask( self._application )
    task.launch( False )


  def view( self ):

    if ( self._verbose ):

      print \
        'running T2 multiple registration '

    task = BlockT2MultipleRegistrationTask( self._application )
    task.launch( True )

