from AlgorithmGUI import *
from ResourceManager import *


class DwiBlockT2MultipleRegistrationAlgorithmGUI( AlgorithmGUI ):

  def __init__( self, algorithm ):

    AlgorithmGUI.__init__( self,
                           algorithm,
                           ResourceManager().getUIFileName(
                                          'dmri',
                                          'DwiBlockT2MultipleRegistration.ui' ) )


    ############################################################################
    # connecting Block & FOV Interface
    ############################################################################

    self.connectStringParameterToLineEdit( 'blockDirectory', 
                                           'lineEdit_BlockDirectory' )    
    self.connectDirectoryBrowserToPushButtonAndLineEdit( \
                                                   'pushButton_BlockDirectory',
                                                   'lineEdit_BlockDirectory' )
    self.connectBooleanParameterToRadioButton( \
                                         'leftHemisphere',
                                         'radioButton_LeftHemisphere' )
    self.connectStringParameterToLineEdit( 'blockLetter',
                                           'lineEdit_BlockLetter' )    
    self.connectIntegerParameterToSpinBox( \
                                      'fovNumber',
                                      'spinBox_FovNumber' )

    ############################################################################
    # connecting Sequences number
    ############################################################################

    self.connectStringParameterToLineEdit('T2sToRemove','lineEdit_T2sToRemove')
