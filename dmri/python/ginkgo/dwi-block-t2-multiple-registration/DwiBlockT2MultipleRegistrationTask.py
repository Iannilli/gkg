from Task import *
from shutil import copyfile


class DwiBlockT2MultipleRegistrationTask( Task ):

  def __init__( self, application ):

    Task.__init__( self, application )

    self.addFunctor( 'viewer-set-dw-information',
                     self.viewerSetDwInformation )


  def viewerSetDwInformation( self,
                              sliceAxis,
                              sizeX, sizeY, sizeZ,
                              resolutionX, resolutionY, resolutionZ ):

    if ( self._application.getBatchMode() == False ):

      self._application.getMainThreadActionManager().call(
        self._application.getViewer().setDwInformation,
        sliceAxis, sizeX, sizeY, sizeZ, resolutionX, resolutionY, resolutionZ )

  ##############################################################################
  # execute()
  ##############################################################################

  def execute( self, functors, parameters, subjectName, verbose, viewOnly ):

    try:
      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 0 )

      ####################### collecting output work directory #################
      fovDirectory = self.generateFovPath(parameters)

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 5 )

      ####################### removing several T2 ##############################
      
      T2sToRemove = parameters['T2sToRemove'].getValue()
      self.removeSeveralT2(T2sToRemove,fovDirectory,subjectName,viewOnly) 

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 100 )

    except:

      self._functors[ 'task-set-status-as-failed' ]( subjectName,
                                                     sys.exc_info() )

  def generateFovPath(self,parameters):

    blocksDirectory = parameters['blockDirectory'].getValue()
    leftHemisphere = parameters[ 'leftHemisphere' ].getValue()
    blockLetter = parameters[ 'blockLetter' ].getValue()
    fovNumber = parameters[ 'fovNumber' ].getValue()

    if leftHemisphere:  

        hemisphereDirectory = blocksDirectory + '/' + "LeftHemisphere" 
    else:

        hemisphereDirectory = blocksDirectory + '/' + "RightHemisphere" 

    return hemisphereDirectory + '/' + str(blockLetter) + '/' \
    + str(blockLetter) + str(fovNumber) +'/'

  def removeSeveralT2(self, T2sToRemove,fovDirectory,subjectName,viewOnly):

    listT2sToRemove=map(lambda x : int(x),T2sToRemove.split(','))
    listT2sToRemove.sort()

    if not os.path.isfile(fovDirectory + '/02-DW/02-Registration/t2_multiple_registration.ima'):
        
        self.copyT2MultipleToRegistration(fovDirectory)

    lengthT2Multiple = self.readLengthT2Multiple(fovDirectory)
    print('List of t to remove',listT2sToRemove)

    for t2ToRemove in listT2sToRemove:
    
        self.removeSingleT2(t2ToRemove,fovDirectory,lengthT2Multiple,subjectName,viewOnly)
        listT2sToRemove=map(lambda x: x-1,listT2sToRemove)

  def copyT2MultipleToRegistration(self, fovDirectory):
    copyfile(fovDirectory+'02-DW/01-DWInQSpace-b1500-4500-8000/t2_multiple.ima',fovDirectory+'02-DW/02-Registration/t2_multiple_registration.ima')
    copyfile(fovDirectory+'02-DW/01-DWInQSpace-b1500-4500-8000/t2_multiple.dim',fovDirectory+'02-DW/02-Registration/t2_multiple_registration.dim')
   
  def readLengthT2Multiple(self, fovDirectory):
    f=open(fovDirectory+'02-DW/02-Registration/t2_multiple_registration.dim')
    firstLine=f.readline()
    lengthT2Multiple=int(firstLine.replace('\n','').split(' ')[-1])
    return lengthT2Multiple

  def removeSingleT2(self,T2ToRemove,fovDirectory,lengthT2Multiple,subjectName,viewOnly):

    if T2ToRemove > 0 and T2ToRemove < (lengthT2Multiple - 1):

      self.commandSubVolume(0,T2ToRemove-1,'firstHalf',fovDirectory,subjectName,viewOnly)
      self.commandSubVolume(T2ToRemove+1,lengthT2Multiple-1,'secondHalf',fovDirectory,subjectName,viewOnly)
      self.commandConcatenate(fovDirectory,subjectName,viewOnly)
      self.removeHalfs(fovDirectory)
      self.commandAverager(fovDirectory,subjectName,viewOnly)

    elif T2ToRemove == 0 :

      self.commandSubVolume(1,lengthT2Multiple-1,'t2_multiple_registration',fovDirectory,subjectName,viewOnly)
      self.commandAverager(fovDirectory,subjectName,viewOnly)

    elif T2ToRemove == lengthT2Multiple - 1 : 

      self.commandSubVolume(0,lengthT2Multiple-2,'t2_multiple_registration',fovDirectory,subjectName,viewOnly)
      self.commandAverager(fovDirectory,subjectName,viewOnly)

  def commandSubVolume(self, firstSlice,lastSlice,name,fovDirectory,subjectName,viewOnly):

    t2MultipleFile = fovDirectory + '02-DW/02-Registration/t2_multiple_registration.ima'  
    outputFile = fovDirectory + '02-DW/02-Registration/' + name + '.ima'

    command = 'GkgExecuteCommand SubVolume \
            -i ' + t2MultipleFile + '\
            -o ' + outputFile + ' \
            -t ' + str(firstSlice) + '\
            -T ' + str(lastSlice) + '\
            -verbose'
    executeCommand( self, subjectName, command, viewOnly )

  def commandConcatenate(self, fovDirectory,subjectName,viewOnly):

    registrationFile=fovDirectory+'02-DW/02-Registration/'

    command = 'GkgExecuteCommand Cat \
    -i '+ registrationFile+'firstHalf.ima ' + registrationFile+'secondHalf.ima  \
    -o ' + registrationFile + 't2_multiple_registration \
    -t t \
    -verbose'
    executeCommand( self, subjectName, command, viewOnly )

  def removeHalfs(self, fovDirectory):

    os.remove(fovDirectory+'02-DW/02-Registration/firstHalf.ima')
    os.remove(fovDirectory+'02-DW/02-Registration/firstHalf.dim')
    os.remove(fovDirectory+'02-DW/02-Registration/firstHalf.ima.minf')
    os.remove(fovDirectory+'02-DW/02-Registration/secondHalf.ima')
    os.remove(fovDirectory+'02-DW/02-Registration/secondHalf.ima.minf')
    os.remove(fovDirectory+'02-DW/02-Registration/secondHalf.dim')

  def commandAverager(self, fovDirectory,subjectName,viewOnly):

    registrationFile=fovDirectory+'02-DW/02-Registration/'
    command = 'GkgExecuteCommand VolumeAverager \
    -i '+ registrationFile+'t2_multiple_registration.ima \
    -o ' + registrationFile + 't2_still.ima \
    -axis t \
    -verbose'
    executeCommand( self, subjectName, command, viewOnly )



  ##############################################################################
  # display()
  ##############################################################################

  def display( self, functors, parameters, subjectName, verbose, isReportModeActive ):

    pass