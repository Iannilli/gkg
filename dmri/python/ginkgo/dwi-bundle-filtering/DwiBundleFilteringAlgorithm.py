from Algorithm import *
from DwiBundleFilteringTask import *


class DwiBundleFilteringAlgorithm( Algorithm ):

  def __init__( self, verbose ):

    Algorithm.__init__( self, 'DWI-Bundle-Filtering', verbose, True )

    ############################################################################
    # Input data
    ############################################################################

    self.addParameter( StringParameter( 'fileNameBundleMaps', '' ) )
    self.addParameter( StringParameter( 'fileNameRgb', '' ) )
    self.addParameter( StringParameter( 'fileNameT1', '' ) )
    self.addParameter( StringParameter( 'fileNameTransformationDwToT1',
                                        '' ) )

    ############################################################################
    # Filter type interface
    ############################################################################

    self.addParameter( DoubleParameter( 'inputResamplingStep',
                                         0.1, 0.001, 10.0, 0.001 ) )
    self.addParameter( DoubleParameter( 'outputResamplingStep',
                                         0.5, 0.001, 10.0, 0.001 ) )

    self.addParameter( ProcessListParameter( 'processes',
         {  'Fiber - length filter': \
                    { 'index': 0,
                      'fiberLengthFilter_ThresholdMode': \
                             {'name': 'Threshold mode:',
                              'type': 'choices',
                              'widgetType': 'comboBox',
                              'choices': [ 'lower than',
                                          'lower or equal to',
                                          'equal to',
                                          'different from',
                                          'greater or equal to',
                                          'greater than',
                                          'between or equal',
                                          'between',
                                          'outside or equal',
                                          'outside' ],
                              'defaultValue': 1,
                              'index': 0,
                              'values': [ 0,
                                          1,
                                          2,
                                          3,
                                          4,
                                          5,
                                          6,
                                          7,
                                          8,
                                          9 ],
                              'parametersToShow' : [ \
                                [ 'fiberLengthFilter_Threshold' ],
                                [ 'fiberLengthFilter_Threshold' ],
                                [ 'fiberLengthFilter_Threshold' ],
                                [ 'fiberLengthFilter_Threshold' ],
                                [ 'fiberLengthFilter_Threshold' ],
                                [ 'fiberLengthFilter_Threshold' ],
                                [ 'fiberLengthFilter_LowerThreshold',
                                  'fiberLengthFilter_UpperThreshold'  ],
                                [ 'fiberLengthFilter_LowerThreshold',
                                  'fiberLengthFilter_UpperThreshold'  ],
                                [ 'fiberLengthFilter_LowerThreshold',
                                  'fiberLengthFilter_UpperThreshold'  ],
                                [ 'fiberLengthFilter_LowerThreshold',
                                  'fiberLengthFilter_UpperThreshold'  ]
                                                    ]
                             },
                     'fiberLengthFilter_Threshold': \
                            { 'name': 'Threshold:',
                              'type': 'double',
                              'widgetType': 'doubleSpinBox',
                              'minimumValue': 0.0,
                              'maximumValue': 100000.0,
                              'defaultValue': 1000.0,
                              'incrementStep': 1.0,
                              'index': 1
                             },
                     'fiberLengthFilter_LowerThreshold': \
                            { 'name': 'Lower threshold:',
                              'type': 'double',
                              'widgetType': 'doubleSpinBox',
                              'minimumValue': 0.0,
                              'maximumValue': 100000.0,
                              'defaultValue': 0.0,
                              'incrementStep': 1.0,
                              'index': 2
                             },

                     'fiberLengthFilter_UpperThreshold': \
                             {'name': 'Upper threshold:',
                              'type': 'double',
                              'widgetType': 'doubleSpinBox',
                              'minimumValue': 0.0,
                              'maximumValue': 100000.0,
                              'defaultValue': 1000.0,
                              'incrementStep': 1.0,
                              'index': 3
                             }
                     },
            'Fiber - curvature filter': \
                    { 'index': 1,
                      'fiberCurvatureFilter_ThresholdMode': \
                             {'name': 'Threshold mode:',
                              'type': 'choices',
                              'widgetType': 'comboBox',
                              'choices': [ 'lower than',
                                          'lower or equal to',
                                          'equal to',
                                          'different from',
                                          'greater or equal to',
                                          'greater than',
                                          'between or equal',
                                          'between',
                                          'outside or equal',
                                          'outside' ],
                              'defaultValue': 1,
                              'index': 0,
                              'values': [ 0,
                                          1,
                                          2,
                                          3,
                                          4,
                                          5,
                                          6,
                                          7,
                                          8,
                                          9 ],
                              'parametersToShow' : [ \
                                [ 'fiberCurvatureFilter_Threshold' ],
                                [ 'fiberCurvatureFilter_Threshold' ],
                                [ 'fiberCurvatureFilter_Threshold' ],
                                [ 'fiberCurvatureFilter_Threshold' ],
                                [ 'fiberCurvatureFilter_Threshold' ],
                                [ 'fiberCurvatureFilter_Threshold' ],
                                [ 'fiberCurvatureFilter_LowerThreshold',
                                  'fiberCurvatureFilter_UpperThreshold'  ],
                                [ 'fiberCurvatureFilter_LowerThreshold',
                                  'fiberCurvatureFilter_UpperThreshold'  ],
                                [ 'fiberCurvatureFilter_LowerThreshold',
                                  'fiberCurvatureFilter_UpperThreshold'  ],
                                [ 'fiberCurvatureFilter_LowerThreshold',
                                  'fiberCurvatureFilter_UpperThreshold'  ]
                                                    ]
                             },
                     'fiberCurvatureFilter_Threshold': \
                            { 'name': 'Threshold:',
                              'type': 'double',
                              'widgetType': 'doubleSpinBox',
                              'minimumValue': 0.0,
                              'maximumValue': 100000.0,
                              'defaultValue': 1000.0,
                              'incrementStep': 1.0,
                              'index': 1
                             },
                     'fiberCurvatureFilter_LowerThreshold': \
                            { 'name': 'Lower threshold:',
                              'type': 'double',
                              'widgetType': 'doubleSpinBox',
                              'minimumValue': 0.0,
                              'maximumValue': 100000.0,
                              'defaultValue': 0.0,
                              'incrementStep': 1.0,
                              'index': 2
                             },

                       'fiberCurvatureFilter_UpperThreshold': \
                             {'name': 'Upper threshold:',
                              'type': 'double',
                              'widgetType': 'doubleSpinBox',
                              'minimumValue': 0.0,
                              'maximumValue': 100000.0,
                              'defaultValue': 1000.0,
                              'incrementStep': 1.0,
                              'index': 3
                             }
                     },
             'Fiber - tortuosity filter': \
                    { 'index': 2,
                      'fiberTortuosityFilter_ThresholdMode': \
                             {'name': 'Threshold mode:',
                              'type': 'choices',
                              'widgetType': 'comboBox',
                              'choices': [ 'lower than',
                                          'lower or equal to',
                                          'equal to',
                                          'different from',
                                          'greater or equal to',
                                          'greater than',
                                          'between or equal',
                                          'between',
                                          'outside or equal',
                                          'outside' ],
                              'defaultValue': 1,
                              'index': 0,
                              'values': [ 0,
                                          1,
                                          2,
                                          3,
                                          4,
                                          5,
                                          6,
                                          7,
                                          8,
                                          9 ],
                              'parametersToShow' : [ \
                                [ 'fiberTortuosityFilter_Threshold' ],
                                [ 'fiberTortuosityFilter_Threshold' ],
                                [ 'fiberTortuosityFilter_Threshold' ],
                                [ 'fiberTortuosityFilter_Threshold' ],
                                [ 'fiberTortuosityFilter_Threshold' ],
                                [ 'fiberTortuosityFilter_Threshold' ],
                                [ 'fiberTortuosityFilter_LowerThreshold',
                                  'fiberTortuosityFilter_UpperThreshold'  ],
                                [ 'fiberTortuosityFilter_LowerThreshold',
                                  'fiberTortuosityFilter_UpperThreshold'  ],
                                [ 'fiberTortuosityFilter_LowerThreshold',
                                  'fiberTortuosityFilter_UpperThreshold'  ],
                                [ 'fiberTortuosityFilter_LowerThreshold',
                                  'fiberTortuosityFilter_UpperThreshold'  ]
                                                    ]
                             },
                     'fiberTortuosityFilter_Threshold': \
                            { 'name': 'Threshold:',
                              'type': 'double',
                              'widgetType': 'doubleSpinBox',
                              'minimumValue': 0.0,
                              'maximumValue': 100000.0,
                              'defaultValue': 1000.0,
                              'incrementStep': 1.0,
                              'index': 1
                             },
                     'fiberTortuosityFilter_LowerThreshold': \
                            { 'name': 'Lower threshold:',
                               'type': 'double',
                              'widgetType': 'doubleSpinBox',
                              'minimumValue': 0.0,
                              'maximumValue': 100000.0,
                              'defaultValue': 0.0,
                              'incrementStep': 1.0,
                              'index': 2
                             },

                       'fiberTortuosityFilter_UpperThreshold': \
                             {'name': 'Upper threshold:',
                                 'type': 'double',
                              'widgetType': 'doubleSpinBox',
                              'minimumValue': 0.0,
                              'maximumValue': 100000.0,
                              'defaultValue': 1000.0,
                              'incrementStep': 1.0,
                              'index': 3
                             }
                     },
             'Bundle - average fiber filter': \
                    {'index': 3 },
             'Bundle - change labels filter': \
                    { 'index': 4,
                      'bundleChangeLabelsFilter_OldLabelList': \
                            { 'name': 'Old label list:',
                              'type': 'string',
                              'widgetType': 'lineEdit',
                              'defaultValue': '',
                              'index': 0
                             },
                       'bundleChangeLabelsFilter_NewLabelList': \
                            { 'name': 'New label list:',
                              'type': 'string',
                              'widgetType': 'lineEdit',
                              'defaultValue': '',
                              'index': 1
                             }
                     },
             'Bundle - merge labels filter': \
                    { 'index': 5,
                      'bundleMergeLabelsFilter_MergedLabelList': \
                            { 'name': 'merged bundle label:',
                              'type': 'string',
                              'widgetType': 'lineEdit',
                              'defaultValue': '',
                              'index': 0
                             }
                     },
             'Bundle - select labels filter': \
                    { 'index': 6,
                      'bundleSelectLabelsFilter_SelectedLabelList': \
                            { 'name': 'Selected label list:',
                              'type': 'string',
                              'widgetType': 'lineEdit',
                              'defaultValue': '',
                              'index': 0
                             }
                     },
             'Bundle - discard labels filter': \
                    { 'index': 7,
                      'bundleDiscardLabelsFilter_DiscardedLabelList': \
                            { 'name': 'Discarded label list:',
                              'type': 'string',
                              'widgetType': 'lineEdit',
                              'defaultValue': '',
                              'index': 0
                             }
                     },
             'Bundle - fiber count filter': \
                    { 'index': 8,
                      'bundleFiberCountFilter_ThresholdMode': \
                             {'name': 'Threshold mode:',
                              'type': 'choices',
                              'widgetType': 'comboBox',
                              'choices': [ 'lower than',
                                          'lower or equal to',
                                          'equal to',
                                          'different from',
                                          'greater or equal to',
                                          'greater than',
                                          'between or equal',
                                          'between',
                                          'outside or equal',
                                          'outside' ],
                              'defaultValue': 1,
                              'index': 0,
                              'values': [ 0,
                                          1,
                                          2,
                                          3,
                                          4,
                                          5,
                                          6,
                                          7,
                                          8,
                                          9 ],
                              'parametersToShow' : [ \
                                [ 'bundleFiberCountFilter_Threshold' ],
                                [ 'bundleFiberCountFilter_Threshold' ],
                                [ 'bundleFiberCountFilter_Threshold' ],
                                [ 'bundleFiberCountFilter_Threshold' ],
                                [ 'bundleFiberCountFilter_Threshold' ],
                                [ 'bundleFiberCountFilter_Threshold' ],
                                [ 'bundleFiberCountFilter_LowerThreshold',
                                  'bundleFiberCountFilter_UpperThreshold'  ],
                                [ 'bundleFiberCountFilter_LowerThreshold',
                                  'bundleFiberCountFilter_UpperThreshold'  ],
                                [ 'bundleFiberCountFilter_LowerThreshold',
                                  'bundleFiberCountFilter_UpperThreshold'  ],
                                [ 'bundleFiberCountFilter_LowerThreshold',
                                  'bundleFiberCountFilter_UpperThreshold'  ]
                                                    ]
                             },
                     'bundleFiberCountFilter_Threshold': \
                            { 'name': 'Threshold:',
                              'type': 'double',
                              'widgetType': 'doubleSpinBox',
                              'minimumValue': 0.0,
                              'maximumValue': 100000.0,
                              'defaultValue': 1000.0,
                              'incrementStep': 1.0,
                              'index': 1
                             },
                     'bundleFiberCountFilter_LowerThreshold': \
                            { 'name': 'Lower threshold:',
                              'type': 'double',
                              'widgetType': 'doubleSpinBox',
                              'minimumValue': 0.0,
                              'maximumValue': 100000.0,
                              'defaultValue': 0.0,
                              'incrementStep': 1.0,
                              'index': 2
                             },

                       'bundleFiberCountFilter_UpperThreshold': \
                             {'name': 'Upper threshold:',
                              'type': 'double',
                              'widgetType': 'doubleSpinBox',
                              'minimumValue': 0.0,
                              'maximumValue': 100000.0,
                              'defaultValue': 1000.0,
                              'incrementStep': 1.0,
                              'index': 3
                             }
                     },
             'Bundle - fusion filter': \
                    {'index': 9 },
             'Bundle - random fiber sampling filter': \
                    { 'index': 10,
                      'bundleRandomFiberSamplingFilter_KeptFiberPercentage': \
                            { 'name': 'Kept fiber %',
                              'type': 'double',
                              'widgetType': 'doubleSpinBox',
                              'minimumValue': 0.0,
                              'maximumValue': 100.0,
                              'defaultValue': 10.0,
                              'incrementStep': 1.0,
                              'index': 0
                             }
                     },
             'Bundle - cut extremities filter': \
              { 'index': 11,
                'bundleCutExtremitiesFilter_PointCountToDiscardAtExtremities': \
                            { 'name': 'Point count to discard at extremities:',
                              'type': 'integer',
                              'widgetType': 'spinBox',
                              'minimumValue': 1,
                              'maximumValue': 1000,
                              'defaultValue': 1,
                              'incrementStep': 1,
                              'index': 0
                             }
                     },
             'Fiber - ROI based filter': \
                    { 'index': 12,
                      'fiberRoiBasedFilter_FileNameRoi': \
                            { 'name': 'ROI volume:',
                              'type': 'string',
                              'widgetType': 'lineEditAndBrowser',
                              'defaultValue': '',
                              'index': 0
                             },
                      'fiberRoiBasedFilter_FileNameTransformationRoiToDw': \
                             { 'name': 'ROI => DW 3D transf.:',
                              'type': 'string',
                              'widgetType': 'lineEditAndBrowser',
                              'defaultValue': '',
                              'index': 1
                             },
                      'fiberRoiBasedFilter_ThresholdMode': \
                             {'name': 'Threshold mode:',
                              'type': 'choices',
                              'widgetType': 'comboBox',
                              'choices': [ 'lower than',
                                          'lower or equal to',
                                          'equal to',
                                          'different from',
                                          'greater or equal to',
                                          'greater than',
                                          'between or equal',
                                          'between',
                                          'outside or equal',
                                          'outside' ],
                              'defaultValue': 1,
                              'index': 2,
                              'values': [ 0,
                                          1,
                                          2,
                                          3,
                                          4,
                                          5,
                                          6,
                                          7,
                                          8,
                                          9 ],
                              'parametersToShow' : [ \
                                [ 'fiberRoiBasedFilter_Threshold' ],
                                [ 'fiberRoiBasedFilter_Threshold' ],
                                [ 'fiberRoiBasedFilter_Threshold' ],
                                [ 'fiberRoiBasedFilter_Threshold' ],
                                [ 'fiberRoiBasedFilter_Threshold' ],
                                [ 'fiberRoiBasedFilter_Threshold' ],
                                [ 'fiberRoiBasedFilter_LowerThreshold',
                                  'fiberRoiBasedFilter_UpperThreshold'  ],
                                [ 'fiberRoiBasedFilter_LowerThreshold',
                                  'fiberRoiBasedFilter_UpperThreshold'  ],
                                [ 'fiberRoiBasedFilter_LowerThreshold',
                                  'fiberRoiBasedFilter_UpperThreshold'  ],
                                [ 'fiberRoiBasedFilter_LowerThreshold',
                                  'fiberRoiBasedFilter_UpperThreshold'  ]
                                                    ]
                             },
                     'fiberRoiBasedFilter_Threshold': \
                            { 'name': 'Threshold:',
                              'type': 'double',
                              'widgetType': 'doubleSpinBox',
                              'minimumValue': 0.0,
                              'maximumValue': 100000.0,
                              'defaultValue': 1000.0,
                              'incrementStep': 1.0,
                              'index': 3
                             },
                     'fiberRoiBasedFilter_LowerThreshold': \
                            { 'name': 'Lower threshold:',
                              'type': 'double',
                              'widgetType': 'doubleSpinBox',
                              'minimumValue': 0.0,
                              'maximumValue': 100000.0,
                              'defaultValue': 0.0,
                              'incrementStep': 1.0,
                              'index': 4
                             },
                       'fiberRoiBasedFilter_UpperThreshold': \
                             {'name': 'Upper threshold:',
                              'type': 'double',
                              'widgetType': 'doubleSpinBox',
                              'minimumValue': 0.0,
                              'maximumValue': 100000.0,
                              'defaultValue': 1000.0,
                              'incrementStep': 1.0,
                              'index': 5
                             },
                      'fiberRoiBasedFilter_MinimumIntersectionLength': \
                            { 'name': 'Minimum intersection length (mm)',
                              'type': 'double',
                              'widgetType': 'doubleSpinBox',
                              'minimumValue': 0.0,
                              'maximumValue': 300.0,
                              'defaultValue': 1.0,
                              'incrementStep': 0.1,
                              'index': 6
                             },
                     'fiberRoiBasedFilter_IntersectionType': \
                             {'name': 'Intersection type:',
                              'type': 'choices',
                              'widgetType': 'comboBox',
                              'choices': [ 'with intersection',
                                          'without intersection' ],
                              'defaultValue': 0,
                              'index': 7,
                             }
                     },
             'Bundle - ROI based filter': \
                    { 'index': 13,
                      'bundleRoiBasedFilter_FileNameRoi': \
                            { 'name': 'ROI volume:',
                              'type': 'string',
                              'widgetType': 'lineEditAndBrowser',
                              'defaultValue': '',
                              'index': 0
                             },
                      'bundleRoiBasedFilter_FileNameTransformationRoiToDw': \
                             { 'name': 'ROI => DW 3D transf.:',
                              'type': 'string',
                              'widgetType': 'lineEditAndBrowser',
                              'defaultValue': '',
                              'index': 1
                             },
                      'bundleRoiBasedFilter_DoNotProcessInterRoiBundles': \
                             {'name': 'Do not process inter-ROI bundles',
                              'type': 'boolean',
                              'widgetType': 'checkBox',
                              'defaultValue': False,
                              'values': [ 0, 2 ],
                              'parametersToShow' : [ [ \
                             'bundleRoiBasedFilter_MinimumIntersectionLength' ],
                                               [] ],
                              'index': 2
                             },
                      'bundleRoiBasedFilter_Offset': \
                             {'name': 'Offset:',
                              'type': 'integer',
                              'widgetType': 'spinBox',
                              'minimumValue': 0,
                              'maximumValue': 1000000,
                              'defaultValue': 0,
                              'incrementStep': 1,
                              'index': 3
                             },
                      'bundleRoiBasedFilter_MinimumIntersectionLength': \
                             {'name': 'Minimum intersection length:',
                              'type': 'double',
                              'widgetType': 'doubleSpinBox',
                              'minimumValue': 0.0,
                              'maximumValue': 300.0,
                              'defaultValue': 1.0,
                              'incrementStep': 0.1,
                              'index': 4
                             }
                     },
             'Fiber - density map based filter': \
                    { 'index': 14,
                      'fiberDensityMapBasedFilter_FileNameDensityMap': \
                            { 'name': 'Density map:',
                              'type': 'string',
                              'widgetType': 'lineEditAndBrowser',
                              'defaultValue': '',
                              'index': 0
                             },
                     'fiberDensityMapBasedFilter_ThresholdMode': \
                             {'name': 'Threshold mode:',
                              'type': 'choices',
                              'widgetType': 'comboBox',
                              'choices': [ 'lower than',
                                          'lower or equal to',
                                          'equal to',
                                          'different from',
                                          'greater or equal to',
                                          'greater than',
                                          'between or equal',
                                          'between',
                                          'outside or equal',
                                          'outside' ],
                              'defaultValue': 2,
                              'index': 1,
                              'values': [ 0,
                                          1,
                                          2,
                                          3,
                                          4,
                                          5,
                                          6,
                                          7,
                                          8,
                                          9 ],
                              'parametersToShow' : [ \
                                [ 'fiberDensityMapBasedFilter_Threshold' ],
                                [ 'fiberDensityMapBasedFilter_Threshold' ],
                                [ 'fiberDensityMapBasedFilter_Threshold' ],
                                [ 'fiberDensityMapBasedFilter_Threshold' ],
                                [ 'fiberDensityMapBasedFilter_Threshold' ],
                                [ 'fiberDensityMapBasedFilter_Threshold' ],
                                [ 'fiberDensityMapBasedFilter_LowerThreshold',
                                 'fiberDensityMapBasedFilter_UpperThreshold'  ],
                                [ 'fiberDensityMapBasedFilter_LowerThreshold',
                                 'fiberDensityMapBasedFilter_UpperThreshold'  ],
                                [ 'fiberDensityMapBasedFilter_LowerThreshold',
                                 'fiberDensityMapBasedFilter_UpperThreshold'  ],
                                [ 'fiberDensityMapBasedFilter_LowerThreshold',
                                  'fiberDensityMapBasedFilter_UpperThreshold'  ]
                                                    ]
                             },
                     'fiberDensityMapBasedFilter_Threshold': \
                            { 'name': 'Threshold:',
                              'type': 'double',
                              'widgetType': 'doubleSpinBox',
                              'minimumValue': 0.0,
                              'maximumValue': 100000.0,
                              'defaultValue': 1000.0,
                              'incrementStep': 1.0,
                              'index': 2
                             },
                     'fiberDensityMapBasedFilter_LowerThreshold': \
                            { 'name': 'Lower threshold:',
                              'type': 'double',
                              'widgetType': 'doubleSpinBox',
                              'minimumValue': 0.0,
                              'maximumValue': 100000.0,
                              'defaultValue': 0.0,
                              'incrementStep': 1.0,
                              'index': 3
                             },

                       'fiberDensityMapBasedFilter_UpperThreshold': \
                             {'name': 'Upper threshold:',
                              'type': 'double',
                              'widgetType': 'doubleSpinBox',
                              'minimumValue': 0.0,
                              'maximumValue': 100000.0,
                              'defaultValue': 1000.0,
                              'incrementStep': 1.0,
                              'index': 4
                             },
               'fiberDensityMapBasedFilter_FileNameTransformationDensityToDw': \
                             { 'name': 'Dens. => DW 3D transf.:',
                              'type': 'string',
                              'widgetType': 'lineEditAndBrowser',
                              'defaultValue': '',
                              'index': 5
                             },
                   'fiberDensityMapBasedFilter_MinimumIntersectionLength': \
                            { 'name': 'Minimum intersection length (mm)',
                              'type': 'double',
                              'widgetType': 'doubleSpinBox',
                              'minimumValue': 0.0,
                              'maximumValue': 300.0,
                              'defaultValue': 1.0,
                              'incrementStep': 0.1,
                              'index': 6
                             }
                     }
         } ) )

    ###########################################################################
    # Output
    ###########################################################################

    self.addParameter( ChoiceParameter( 'bundleMapFormat',
                                        0,
                                        [ 'aimsbundlemap',
                                          'bundlemap',
                                          'vtkbundlemap',
                                          'trkbundlemap' ] ) )
    self.addParameter( BooleanParameter( 'fusionOutputBundleMaps', 2 ) )

    ###########################################################################
    # Viewer options
    ###########################################################################

    self.addParameter( BooleanParameter( 'keepIntermediateResults', 2 ) )
    self.addParameter( BooleanParameter( 'displayRoiAndDensityMap', 2 ) )

  def launch( self ):

    if ( self._verbose ):

      print \
        'running DWI bundle filtering'

    task = DwiBundleFilteringTask( self._application )
    task.launch( False )


  def view( self ):

    if ( self._verbose ):

      print \
        'viewing DWI bundle filtering'

    task = DwiBundleFilteringTask( self._application )
    task.launch( True )

