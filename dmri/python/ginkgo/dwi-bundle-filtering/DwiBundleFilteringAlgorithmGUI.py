from AlgorithmGUI import *
from ResourceManager import *
from ProcessListWidget import *
import shutil

class DwiBundleFilteringAlgorithmGUI( AlgorithmGUI ):

  def __init__( self, algorithm ):

    AlgorithmGUI.__init__( self,
                           algorithm,
                           ResourceManager().getUIFileName(
                                                    'dmri',
                                                    'DwiBundleFiltering.ui' ) )

    ###########################################################################
    # Connecting data interface
    ###########################################################################

    # Input Bundle map name and push button
    self.connectStringParameterToLineEdit( 'fileNameBundleMaps',
                                           'lineEdit_FileNameBundleMaps' )
    self.connectFileListBrowserToPushButtonAndLineEdit(
                                               'pushButton_FileNameBundleMaps',
                                               'lineEdit_FileNameBundleMaps' )

    # Input RGB map name and push button
    self.connectStringParameterToLineEdit( 'fileNameRgb',
                                           'lineEdit_FileNameRgb' )
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit(
                                                      'pushButton_FileNameRgb',
                                                      'lineEdit_FileNameRgb' )

    # Input T1 file name and push button
    self.connectStringParameterToLineEdit( 'fileNameT1',
                                           'lineEdit_FileNameT1' )
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit( \
                                                       'pushButton_FileNameT1',
                                                       'lineEdit_FileNameT1' )

    # Input DW to T1 3D Transform file name and push button
    self.connectStringParameterToLineEdit(
                                      'fileNameTransformationDwToT1',
                                      'lineEdit_FileNameTransformationDwToT1' )
    self.connectFileBrowserToPushButtonAndLineEdit(
                                     'pushButton_FileNameTransformationDwToT1',
                                     'lineEdit_FileNameTransformationDwToT1' )

    ###########################################################################
    # Connecting filter interface
    ###########################################################################

    self.connectDoubleParameterToSpinBox( 'inputResamplingStep',
                                          'doubleSpinBox_InputResamplingStep' )
    self.connectDoubleParameterToSpinBox( 'outputResamplingStep',
                                         'doubleSpinBox_OutputResamplingStep' )

    ###########################################################################
    # Connecting viewer options
    ###########################################################################

    self.connectBooleanParameterToCheckBox( \
                                           'keepIntermediateResults',
                                           'checkBox_KeepIntermediateResults' )
    self.connectBooleanParameterToCheckBox( \
                                           'displayRoiAndDensityMap',
                                           'checkBox_DisplayRoiAndDensityMap' )

    self._findChild( self._awin,
                     'pushButton_RemoveDisplayFiles'
                    ).clicked.connect( self.removeDisplayFiles )
    self._findChild( self._awin,
                     'pushButton_RemoveIntermediateResults'
                    ).clicked.connect( self.removeIntermediateResults )
    self._findChild( self._awin,
                     'pushButton_Run'
                    ).clicked.connect( self.enableRemovePushButtons )


    ###########################################################################
    # Connecting output bundle map format interface
    ###########################################################################

    self.connectChoiceParameterToComboBox( 'bundleMapFormat',
                                           'comboBox_BundleMapFormat' )
    self.connectBooleanParameterToCheckBox( 'fusionOutputBundleMaps',
                                            'checkBox_FusionOutputBundleMaps' )

    ###########################################################################
    # Filter chain
    ###########################################################################

    self._processListWidget = ProcessListWidget( \
                                    self._algorithm.getParameter( 'processes' ),
                                    self,
                                    'filter',
                                    'groupBox_FilterChain',
                                    'Bundle - fiber count filter' )

    self.connectProcessListParameterToProcessListWidget( \
                                                       'processes',
                                                       self._processListWidget )

    ###########################################################################
    # Connecting outputWorkDirectory and Launch
    ###########################################################################

    self._findChild( self._awin,
                     'lineEdit_OutputWorkDirectory'
                    ).textChanged.connect( self.enableRemovePushButtons )


  def connectProcessListParameterToProcessListWidget( \
                                                     self,
                                                     processListParameterName,
                                                     processListWidget ):

      parameter = self._algorithm.getParameter( processListParameterName )
      parameter.setWidget( 'processListWidget', processListWidget )


  def removeDisplayFiles( self ):

    outputWorkDirectory = os.path.join( str( self._findChild( self._awin,
                                              'lineEdit_OutputWorkDirectory' )\
                                                                     .text() ),
                                              'temporaryDisplay' )
    if ( os.path.exists( outputWorkDirectory ) ):

      shutil.rmtree( outputWorkDirectory )

    self._findChild( self._awin,
                     'pushButton_RemoveDisplayFiles' ).setEnabled( False )


  def removeIntermediateResults( self ):

    outputWorkDirectory = os.path.join( str( self._findChild( self._awin,
                                           'lineEdit_OutputWorkDirectory' )\
                                                                     .text() ),
                                           'intermediateResults' )
    if ( os.path.exists( outputWorkDirectory ) ):

      shutil.rmtree( outputWorkDirectory )

    self._findChild( self._awin,
                   'pushButton_RemoveIntermediateResults' ).setEnabled( False )


  def enableRemovePushButtons( self, value = None ):

    if not ( value ):

      value = str( self._findChild( self._awin,
                                    'lineEdit_OutputWorkDirectory' ).text() )
    else:

      value = str( value )

    if ( os.path.exists( os.path.join( value, 'temporaryDisplay' ) ) ):

      self._findChild( self._awin,
                    'pushButton_RemoveDisplayFiles' ).setEnabled( True )
    else:

      self._findChild( self._awin,
                    'pushButton_RemoveDisplayFiles' ).setEnabled( False )

    if ( os.path.exists( os.path.join( value, 'intermediateResults' ) ) ):

      self._findChild( self._awin,
                   'pushButton_RemoveIntermediateResults' ).setEnabled( True )

    else:

      self._findChild( self._awin,
                  'pushButton_RemoveIntermediateResults' ).setEnabled( False )
