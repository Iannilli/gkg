from Task import *
import time
from PyQt5 import QtCore, QtGui
from soma import aims

class DwiBundleFilteringTask( Task ):

  def __init__( self, application ):

    Task.__init__( self, application )

    self._modeAbbreviation = dict()
    self._modeAbbreviation[ 'lower than' ] = 'lt'
    self._modeAbbreviation[ 'lower or equal to' ] = 'le'
    self._modeAbbreviation[ 'different from' ] = 'di'
    self._modeAbbreviation[ 'equal to' ] = 'eq'
    self._modeAbbreviation[ 'greater or equal to' ] = 'ge'
    self._modeAbbreviation[ 'greater than' ] = 'gt'
    self._modeAbbreviation[ 'between or equal' ] = 'be'
    self._modeAbbreviation[ 'between' ] = 'bt'
    self._modeAbbreviation[ 'outside or equal' ] = 'oe'
    self._modeAbbreviation[ 'outside' ] = 'ou'

    self._filterWithRoiOrDensityMap = list()
    self._roiSliders = list()

    self._bundleSlider = None
    self._bundleSliderLabel = None

    self._meshes = list()
    self._meshIndices = list()
    self._meshesTransformationROIToDW = list()
    self._currentStep = 0
    self._functor = None
    self._steps = list()

    self._currentMesh = None
    self._currentTractograms = list()

    self._displayROI = False
    self._displayIntermediateResults = False

    self._outputBundleMapCount = 1
    self._test = 0

    self._roiSliderConnected = False
    self._bundleRoiSliderConnected = False

  ##############################################################################
  # execute()
  ##############################################################################

  def execute( self, functors, parameters, subjectName, verbose, viewOnly ):

    try:

      ###################### acquiring conditions ##############################
      functors[ 'condition-acquire' ]( subjectName,
                                       'roi-concatenation-processed' )

      self._filterWithRoiOrDensityMap = list()
      self._roiSliders = list()

      self._meshes = list()
      self._meshIndices = list()

      self._meshesTransformationROIToDW = list()
      self._currentStep = 0
      self._functor = functors
      self._steps = list()

      self._outputBundleMapCount = 1

      ########################## collecting file names #########################

      fileNameInputBundleMapsList = \
                                   parameters[ 'fileNameBundleMaps' ].getValue()
 
      processes = parameters[ 'processes' ].getValue()
       
      bundleMapFormat = parameters[ 'bundleMapFormat' ].getChoice()
      fusionOutputBundleMaps = parameters[ 'fusionOutputBundleMaps' ].getValue()
 
      displayROI = parameters[ 'displayRoiAndDensityMap' ].getValue()
      displayIntermediateResults = parameters[ \
                                          'keepIntermediateResults' ].getValue()
      outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()
      inputResamplingStep = parameters[ 'inputResamplingStep' ].getValue()
      fileNameTransformationDwToT1 = \
                        parameters[ 'fileNameTransformationDwToT1' ]. getValue()
      fileNameT1 = parameters[ 'fileNameT1' ].getValue()
      if ( displayIntermediateResults ):

        for process in processes:

          self._steps.append( process[ 'processName' ] )
          self._test += 1

        if not ( os.path.exists( os.path.join ( outputWorkDirectory,
                                                'intermediateResults' ) ) ):

          os.mkdir( os.path.join ( outputWorkDirectory,
                                   'intermediateResults' ) )

        for processIndex in xrange( len( processes ) ):

          functors[ 'condition-acquire' ]( subjectName,
                              'filtering' + str( processIndex ) + '-processed' )

      else:

        functors[ 'condition-acquire' ]( subjectName, 'filtering-processed' )

      ########################## updating progress bar #########################
      functors[ 'update-progress-bar' ]( subjectName, 0 )


      filters = dict()
      filters[ 'Bundle - fiber count filter' ] = self.filterBundleFiberCount
      filters[ 'Fiber - length filter' ] =  self.filterFiberLength
      filters[ 'Bundle - cut extremities filter' ] = \
                                                 self.filterBundleCutExtremities
      filters[ 'Bundle - discard labels filter' ] = \
                                                  self.filterBundleDiscardLabels
      filters[ 'Fiber - tortuosity filter' ] = self.filterFiberTortuosity
      filters[ 'Fiber - density map based filter' ] = \
                                                 self.filterFiberDensityMapBased
      filters[ 'Bundle - average fiber filter' ] = self.filterBundleAverageFiber
      filters[ 'Bundle - change labels filter' ] = self.filterBundleChangeLabels
      filters[ 'Bundle - ROI based filter' ] = self.filterBundleRoiBased
      filters[ 'Fiber - ROI based filter' ] = self.filterFiberRoiBased
      filters[ 'Bundle - fusion filter' ] = self.filterBundleFusion
      filters[ 'Bundle - random fiber sampling filter' ] = \
                                            self.filterBundleRandomFiberSampling
      filters[ 'Fiber - curvature filter' ] = self.filterFiberCurvature
      filters[ 'Bundle - merge labels filter' ] = self.filterBundleMergeLabels
      filters[ 'Bundle - select labels filter' ] = self.filterBundleSelectLabels

      functors[ 'update-progress-bar' ]( subjectName, 2 )
      
      ################ processing ROI and/or density map files #################

      if ( displayROI ):

        index = 0
        inputFileNameRoiOrDensityMap = ''
        outputDisplayDirectory =  os.path.join( outputWorkDirectory,
                                                'temporaryDisplay' )
        if not( os.path.exists( outputDisplayDirectory ) ):

          os.mkdir( outputDisplayDirectory )

        for process in processes:

          ######################################################################
          if ( process[ 'processName' ] == 'Fiber - density map based filter' ):

            processStr = 'Process' + str( index ) + '_'
            fileNameRoiOrDensityMap = process[ processStr + \
                    'fiberDensityMapBasedFilter_FileNameDensityMap' ].getValue()
            outputFileNameRoiOrDensityMap = os.path.join ( \
                                     outputDisplayDirectory,
                                     'roiOrDensityMap' + str( index ) + '.ima' )

            # writing Roi -> T1 transformation
            fileNameTransformationDensityToDw = process[ processStr + \
                'fiberDensityMapBasedFilter_FileNameTransformationDensityToDw' \
                                                                 ].getValue()

            if ( fileNameTransformationDensityToDw == 'identity' or \
                                    fileNameTransformationDensityToDw == 'id' ):

              fileNameTransformationDensityToDw = os.path.join( \
                                                 outputDisplayDirectory,
                                                 'trm' + str( index ) + '.trm' )
              file = open( fileNameTransformationDensityToDw, 'w' )
              file.write( '0 0 0\n1 0 0\n 0 1 0\n0 0 1' )
              file.close()

            transformationDwToT1 = aims.read( fileNameTransformationDwToT1 )
            transformationDensityToDw = aims.read( \
                                         fileNameTransformationDensityToDw )
            transformationDensityToT1 = transformationDwToT1 * \
                                          transformationDensityToDw
            fileNameTransformationDensityToT1 = os.path.join( \
                                          outputDisplayDirectory,
                                          'trmRoiToT1' + str( index ) + '.trm' )
            aims.write( transformationDensityToT1,
                        fileNameTransformationDensityToT1 )


            command = 'GkgExecuteCommand Resampling3d ' + \
                      ' -reference ' + fileNameRoiOrDensityMap
            if ( len( fileNameT1 ) ):

              command +=  ' -template ' + fileNameT1 

            command += ' -output ' + outputFileNameRoiOrDensityMap + \
                       ' -transforms ' + fileNameTransformationDensityToT1 + \
                       ' -order 0'

            executeCommand( self, subjectName, command, viewOnly )


            command = 'GkgExecuteCommand Combiner ' + \
                      ' -i ' + outputFileNameRoiOrDensityMap + \
                      ' -o ' + outputFileNameRoiOrDensityMap + \
                      ' -t float'

            executeCommand( self, subjectName, command, viewOnly )

            inputFileNameRoiOrDensityMap += ' ' + outputFileNameRoiOrDensityMap
            self._filterWithRoiOrDensityMap.append( index )

          ######################################################################
          elif ( process[ 'processName' ] == 'Bundle - ROI based filter' ):

            processStr = 'Process' + str( index ) + '_'
            fileNameRoiOrDensityMap = process[ processStr + \
                                 'bundleRoiBasedFilter_FileNameRoi' ].getValue()
            outputFileNameRoiOrDensityMap = os.path.join ( \
                                     outputDisplayDirectory,
                                     'roiOrDensityMap' + str( index ) + '.ima' )

            inputFileNameRoiOrDensityMap += ' ' + outputFileNameRoiOrDensityMap

            # writing Roi -> T1 transformation
            fileNameTransformationROIToDW = process[ processStr + \
                          'bundleRoiBasedFilter_FileNameTransformationRoiToDw' \
                                                ].getValue()
            transformationDwToT1 = aims.read( fileNameTransformationDwToT1 )
            transformationRoiToDw = aims.read( fileNameTransformationROIToDW )
            transformationRoiToT1 = transformationDwToT1 * transformationRoiToDw
            fileNameTransformationRoiToT1 = os.path.join( \
                                          outputDisplayDirectory,
                                          'trmRoiToT1' + str( index ) + '.trm' )
            aims.write( transformationRoiToT1, fileNameTransformationRoiToT1 )

            self._meshesTransformationROIToDW.append( \
                                                 fileNameTransformationROIToDW )

            command = 'GkgExecuteCommand Resampling3d ' + \
                      ' -reference ' + fileNameRoiOrDensityMap
            if ( len( fileNameT1 ) ):

              command +=  ' -template ' + fileNameT1 

            command += ' -output ' + outputFileNameRoiOrDensityMap + \
                       ' -transforms ' + fileNameTransformationRoiToT1 + \
                       ' -order 0'
            executeCommand( self, subjectName, command, viewOnly )

            command = 'AimsFileConvert' + \
                      ' -i ' + outputFileNameRoiOrDensityMap + \
                      ' -o ' + outputFileNameRoiOrDensityMap + \
                      ' -t S16'
            executeCommand( self, subjectName, command, viewOnly )

            command = 'AimsMesh' + \
                      ' -i ' + outputFileNameRoiOrDensityMap + \
                      ' -o ' + os.path.join( outputDisplayDirectory,
                                            'mesh' + str( index ) + '.mesh' )

            executeCommand( self, subjectName, command, viewOnly )

            command = 'AimsFileConvert' + \
                      ' -i ' + outputFileNameRoiOrDensityMap + \
                      ' -o ' + outputFileNameRoiOrDensityMap + \
                      ' -t FLOAT'
            executeCommand( self, subjectName, command, viewOnly )

            self._filterWithRoiOrDensityMap.append( index )

            for file in os.listdir( outputDisplayDirectory ):

              if not( file.find( 'mesh' + str( index ) + '_' ) ):

                if ( os.path.splitext( file )[ 1 ] == '.mesh' ):

                  self._meshes.append( os.path.join( outputDisplayDirectory,
                                                     file ) )
                  break

            self._meshIndices.append( index )

          ######################################################################
          elif ( process[ 'processName' ] == 'Fiber - ROI based filter' ):

            processStr = 'Process' + str( index ) + '_'
            fileNameRoiOrDensityMap = process[ processStr + \
                                  'fiberRoiBasedFilter_FileNameRoi' ].getValue()
            outputFileNameRoiOrDensityMap = os.path.join ( \
                                     outputDisplayDirectory,
                                     'roiOrDensityMap' + str( index ) + '.ima' )

            inputFileNameRoiOrDensityMap += ' ' + outputFileNameRoiOrDensityMap

            # writing Roi -> T1 transformation
            fileNameTransformationROIToDW = process[ processStr + \
                           'fiberRoiBasedFilter_FileNameTransformationRoiToDw' \
                                                             ].getValue()
            transformationDwToT1 = aims.read( fileNameTransformationDwToT1 )
            transformationRoiToDw = aims.read( fileNameTransformationROIToDW )
            transformationRoiToT1 = transformationDwToT1 * transformationRoiToDw
            fileNameTransformationRoiToT1 = os.path.join( \
                                          outputDisplayDirectory,
                                          'trmRoiToT1' + str( index ) + '.trm' )
            aims.write( transformationRoiToT1, fileNameTransformationRoiToT1 )

            self._meshesTransformationROIToDW.append( \
                                                 fileNameTransformationROIToDW )

            command = 'GkgExecuteCommand Resampling3d ' + \
                      ' -reference ' + fileNameRoiOrDensityMap
            if ( len( fileNameT1 ) ):

              command +=  ' -template ' + fileNameT1 

            command += ' -output ' + outputFileNameRoiOrDensityMap + \
                       ' -transforms ' + fileNameTransformationRoiToT1 + \
                       ' -order 0'
            executeCommand( self, subjectName, command, viewOnly )

            command = 'AimsFileConvert' + \
                      ' -i ' + outputFileNameRoiOrDensityMap + \
                      ' -o ' + outputFileNameRoiOrDensityMap + \
                      ' -t S16'
            executeCommand( self, subjectName, command, viewOnly )

            command = 'AimsMesh' + \
                      ' -i ' + outputFileNameRoiOrDensityMap + \
                      ' -o ' + os.path.join( outputDisplayDirectory,
                                            'mesh' + str( index ) + '.mesh' )

            executeCommand( self, subjectName, command, viewOnly )

            command = 'AimsFileConvert' + \
                      ' -i ' + outputFileNameRoiOrDensityMap + \
                      ' -o ' + outputFileNameRoiOrDensityMap + \
                      ' -t FLOAT'
            executeCommand( self, subjectName, command, viewOnly )

            self._filterWithRoiOrDensityMap.append( index )
            for file in os.listdir( outputDisplayDirectory ):

              if not( file.find( 'mesh' + str( index ) + '_' ) ):

                if ( os.path.splitext( file )[ 1 ] == '.mesh' ):

                  self._meshes.append( os.path.join( outputDisplayDirectory,
                                                    file ) )
                  break

            self._meshIndices.append( index )

          index += 1

        concatenatedFileNameRoiOrDensityMap = os.path.join( \
                                          outputDisplayDirectory,
                                          'concatenatedRoiAndDensityMap.ima' )

        if ( len( inputFileNameRoiOrDensityMap ) ):

          command = 'AimsTCat' + \
                    ' -i ' + inputFileNameRoiOrDensityMap + \
                    ' -o ' + concatenatedFileNameRoiOrDensityMap
          executeCommand( self, subjectName, command, viewOnly )

      functors[ 'condition-notify-and-release' ]( \
                                                 subjectName,
                                                 'roi-concatenation-processed' )

      functors[ 'update-progress-bar' ]( subjectName, 10 )

      ################### setting output bundle map extension ##################
      if ( bundleMapFormat == 'aimsbundlemap' ):

        extension = '.bundles'

      elif ( bundleMapFormat == 'bundlemap' ):

        extension = '.bundlemap'

      elif ( bundleMapFormat == 'vtkbundlemap' ):

        extension = '.fib'

      elif ( bundleMapFormat == 'trkbundlemap' ):

        extension = '.trk'

      ############################ launching filters ###########################
      progress = 10
      progressStep = 90 / len( processes )
      index = 0
      bundleMapFileNamePrefixes = list()
      fileNameInputBundleMapsList = fileNameInputBundleMapsList.split( ';' )
      for fileNameInputBundleMap in fileNameInputBundleMapsList:

        baseName = os.path.basename( fileNameInputBundleMap )
        fileName = os.path.splitext( baseName )[ 0 ]
        bundleMapFileNamePrefixes.append( fileName + '_' )


      fileNameBundleMaps = None
      for process in processes:
        
        # input
        fileNameInputBundleMaps = ''

        if ( fileNameBundleMaps == None ):

          for fileNameInputBundleMap in fileNameInputBundleMapsList:

            fileNameInputBundleMaps += fileNameInputBundleMap + ' '

        else:
 
          fileNameInputBundleMaps = fileNameBundleMaps

        # output
        fileNameOutputBundleMaps = ''
        if ( displayIntermediateResults ):

          if ( index < len( processes  ) - 1 ):

            stepOutputWorkDirectory = os.path.join( outputWorkDirectory,
                                                    'intermediateResults',
                                                    'step' + \
                                                    str( index + 1 ) )

            if not ( os.path.exists( stepOutputWorkDirectory ) ):

              os.mkdir( stepOutputWorkDirectory )

            if ( fusionOutputBundleMaps ):

              fileNameOutputBundleMaps += os.path.join( \
                                          stepOutputWorkDirectory,
                                          'intermediateFilteredBundleMap' + \
                                          str( index + 1 ) + extension + ' ' )
            else:

              self._outputBundleMapCount = len( bundleMapFileNamePrefixes )
              for bundleMapFileNamePrefix in bundleMapFileNamePrefixes:

                fileNameOutputBundleMaps += os.path.join( \
                                           stepOutputWorkDirectory,
                                           bundleMapFileNamePrefix + \
                                           'intermediateFilteredBundleMap' + \
                                            str( index + 1 ) + extension + ' ' )

          else:

            if ( fusionOutputBundleMaps ):
 
              fileNameOutputBundleMaps += os.path.join( \
                                          outputWorkDirectory,
                                          'filteredBundleMap' + \
                                          extension + ' ' )
            else:

              self._outputBundleMapCount = len( bundleMapFileNamePrefixes )
              for bundleMapFileNamePrefix in bundleMapFileNamePrefixes:

                fileNameOutputBundleMaps += os.path.join( \
                                            outputWorkDirectory,
                                            bundleMapFileNamePrefix + \
                                            'filteredBundleMap' + \
                                            extension + ' ' )

        else:

          if ( fusionOutputBundleMaps ):

            fileNameOutputBundleMaps += os.path.join( \
                                        outputWorkDirectory,
                                        'filteredBundleMap' + extension + ' ' )
          else:

            self._outputBundleMapCount = len( bundleMapFileNamePrefixes )
            for bundleMapFileNamePrefix in bundleMapFileNamePrefixes:

              fileNameOutputBundleMaps += os.path.join( \
                                          outputWorkDirectory,
                                          bundleMapFileNamePrefix + \
                                          'filteredBundleMap' + \
                                          extension + ' ' )
        for  fileNameInputBundleMap in fileNameInputBundleMaps.split( ' ' ):

          if ( fileNameInputBundleMap != '' ):

            if not ( os.path.exists( fileNameInputBundleMap ) ):

              message = fileNameInputBundleMap + ' does not exist'
              raise RuntimeError, message

            if not ( os.path.getsize( fileNameInputBundleMap ) ):

              message = fileNameInputBundleMap + ' is empty'
              raise RuntimeError, message



        print 'fileNameInputBundleMaps', fileNameInputBundleMaps
        print 'Filter', index + 1, ' : ', process[ 'processName' ]
        processStr = 'Process' + str( index ) + '_'
        filters[ process[ 'processName' ] ]( processStr,
                                             fileNameInputBundleMaps,
                                             fileNameOutputBundleMaps,
                                             bundleMapFormat,
                                             inputResamplingStep,
                                             process,
                                             functors,
                                             subjectName,
                                             viewOnly )

        for fileNameOutputBundleMap in fileNameOutputBundleMaps.split():

          if ( os.path.exists( fileNameOutputBundleMap ) ):

            bundleMapInfo = dict()
            execfile( fileNameOutputBundleMap, bundleMapInfo )
            curves_count = bundleMapInfo[ 'attributes' ][ 'curves_count' ]

            if( curves_count == 0 ):

              message = 'No more fiber in bundleMap ' + fileNameOutputBundleMap
              raise RuntimeError, message

          else:

            message = 'BundleMap file ' + fileNameOutputBundleMap + \
                      ' not found.' 
            raise RuntimeError, message

        if ( displayIntermediateResults ):

          functors[ 'condition-notify-and-release' ]( subjectName,
                                     'filtering' + str( index ) + '-processed' )

        index += 1
        fileNameBundleMaps = fileNameOutputBundleMaps
        progress += progressStep
        functors[ 'update-progress-bar' ]( subjectName, progress )

      ###################### notifying display thread ##########################
      if not( displayIntermediateResults ):

        functors[ 'condition-notify-and-release' ]( subjectName,
                                                    'filtering-processed' )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 100 )

    except:

      self._functors[ 'viewer-abort' ]()
      self._functors[ 'task-set-status-as-failed' ]( subjectName,
                                                     sys.exc_info() )


  def filterBundleFiberCount( self,
                              processStr,
                              fileNameInputBundleMaps,
                              fileNameOutputBundleMaps,
                              bundleMapFormat,
                              inputResamplingStep,
                              parameters,
                              functors,
                              subjectName,
                              viewOnly ):

    mode = self._modeAbbreviation [ parameters[ processStr + \
                          'bundleFiberCountFilter_ThresholdMode' ].getChoice() ]
    command = 'GkgExecuteCommand DwiBundleOperator' + \
             ' -i ' + fileNameInputBundleMaps + \
             ' -o ' + fileNameOutputBundleMaps + \
             ' -of ' + bundleMapFormat + \
             ' -op fiber-count-filter' + \
             ' -stringParameters ' + mode + \
             ' -scalarParameters '
    if ( mode == 'be' or mode == 'bt' or mode == 'oe' or mode == 'ou' ):

      command += str( parameters[ processStr + \
                      'bundleFiberCountFilter_LowerThreshold' ].getValue() ) + \
                 ' ' + \
                 str( parameters[ processStr + \
                      'bundleFiberCountFilter_UpperThreshold' ].getValue() )

    else:

      command += str( parameters[ processStr + \
                      'bundleFiberCountFilter_Threshold' ].getValue() ) + \
                 ' 0.0'

    executeCommand( self, subjectName, command, viewOnly )


  def filterFiberLength( self,
                         processStr,
                         fileNameInputBundleMaps,
                         fileNameOutputBundleMaps,
                         bundleMapFormat,
                         inputResamplingStep,
                         parameters,
                         functors,
                         subjectName,
                         viewOnly ):

    mode = self._modeAbbreviation [ parameters[ processStr + \
                            'fiberLengthFilter_ThresholdMode' ].getChoice() ]
    command = 'GkgExecuteCommand DwiFiberFilter' + \
             ' -b ' + fileNameInputBundleMaps + \
             ' -o ' + fileNameOutputBundleMaps + \
             ' -s length' + \
             ' -bundleMapFormat ' + bundleMapFormat + \
             ' -stringParameters ' + mode + \
             ' -scalarParameters '
    if ( mode == 'be' or mode == 'bt' or mode == 'oe' or mode == 'ou' ):

      command += str( parameters[ processStr + \
                            'fiberLengthFilter_LowerThreshold' ].getValue() )+ \
                 ' ' + \
                 str( parameters[ processStr + \
                            'fiberLengthFilter_UpperThreshold' ].getValue() )

    else:

      command += str( parameters[ processStr + \
                            'fiberLengthFilter_Threshold' ].getValue() ) + \
                 ' 0.0'

    executeCommand( self, subjectName, command, viewOnly )


  def filterBundleCutExtremities( self,
                                  processStr,
                                  fileNameInputBundleMaps,
                                  fileNameOutputBundleMaps,
                                  bundleMapFormat,
                                  inputResamplingStep,
                                  parameters,
                                  functors,
                                  subjectName,
                                  viewOnly ):

    command = 'GkgExecuteCommand DwiBundleOperator' + \
             ' -i ' + fileNameInputBundleMaps + \
             ' -o ' + fileNameOutputBundleMaps + \
             ' -of ' + bundleMapFormat + \
             ' -op cut-extremities' + \
             ' -scalarParameters ' + \
             str( parameters[ processStr + \
                 'bundleCutExtremitiesFilter_PointCountToDiscardAtExtremities' \
                            ].getValue() )

    executeCommand( self, subjectName, command, viewOnly )


  def filterBundleDiscardLabels( self,
                                 processStr,
                                 fileNameInputBundleMaps,
                                 fileNameOutputBundleMaps,
                                 bundleMapFormat,
                                 inputResamplingStep,
                                 parameters,
                                 functors,
                                 subjectName,
                                 viewOnly ):

    command = 'GkgExecuteCommand DwiBundleOperator' + \
             ' -i ' + fileNameInputBundleMaps + \
             ' -o ' + fileNameOutputBundleMaps + \
             ' -of ' + bundleMapFormat + \
             ' -op discard-labels' + \
             ' -stringParameters '+ \
             parameters[ processStr + \
                       'bundleDiscardLabelsFilter_DiscardedLabelList' \
                       ].getValue()

    executeCommand( self, subjectName, command, viewOnly )


  def filterFiberTortuosity( self,
                             processStr,
                             fileNameInputBundleMaps,
                             fileNameOutputBundleMaps,
                             bundleMapFormat,
                             inputResamplingStep,
                             parameters,
                             functors,
                             subjectName,
                             viewOnly ):

    mode = self._modeAbbreviation [ parameters[ processStr + \
                           'fiberTortuosityFilter_ThresholdMode' ].getChoice() ]
    command = 'GkgExecuteCommand DwiFiberFilter' + \
             ' -b ' + fileNameInputBundleMaps + \
             ' -o ' + fileNameOutputBundleMaps + \
             ' -s tortuosity' + \
             ' -bundleMapFormat ' + bundleMapFormat + \
             ' -stringParameters ' + mode + \
             ' -scalarParameters '
    if ( mode == 'be' or mode == 'bt' or mode == 'oe' or mode == 'ou' ):

      command += str( parameters[ processStr + \
                      'fiberTortuosityFilter_LowerThreshold' ].getValue() ) + \
                 ' ' + \
                 str( parameters[ processStr + \
                      'fiberTortuosityFilter_UpperThreshold' ].getValue() )

    else:

      command += str( parameters[ processStr + \
                      'fiberTortuosityFilter_Threshold' ].getValue() ) + \
                 ' 0.0'

    executeCommand( self, subjectName, command, viewOnly )


  def filterFiberDensityMapBased( self,
                                  processStr,
                                  fileNameInputBundleMaps,
                                  fileNameOutputBundleMaps,
                                  bundleMapFormat,
                                  inputResamplingStep,
                                  parameters,
                                  functors,
                                  subjectName,
                                  viewOnly ):


    mode = self._modeAbbreviation [ parameters[ processStr + \
                      'fiberDensityMapBasedFilter_ThresholdMode' ].getChoice() ]
    filenameTransformation = parameters[ processStr + \
     'fiberDensityMapBasedFilter_FileNameTransformationDensityToDw' ].getValue()
    if( filenameTransformation == 'identity' ):

      filenameTransformation = 'id'

    command = 'GkgExecuteCommand DwiFiberFilter' + \
             ' -b ' + fileNameInputBundleMaps + \
             ' -o ' + fileNameOutputBundleMaps + \
             ' -s density_map_based' + \
             ' -bundleMapFormat ' + bundleMapFormat + \
             ' -stringParameters ' + parameters[ processStr + \
                'fiberDensityMapBasedFilter_FileNameDensityMap' ].getValue() + \
             ' ' + filenameTransformation + \
             ' ' + mode + \
             ' -scalarParameters '
    if ( mode == 'be' or mode == 'bt' or mode == 'oe' or mode == 'ou' ):

      command += str( parameters[ processStr + \
                 'fiberDensityMapBasedFilter_LowerThreshold' ].getValue() ) + \
                 ' ' + \
                 str( parameters[ processStr + \
                 'fiberDensityMapBasedFilter_UpperThreshold' ].getValue() )

    else:

      command += str( parameters[ processStr + \
                      'fiberDensityMapBasedFilter_Threshold' ].getValue() ) + \
                 ' 0.0'

    command += ' ' + str( parameters[ processStr + \
                    'fiberDensityMapBasedFilter_MinimumIntersectionLength' \
                                                              ].getValue() ) + \
               ' ' + str( inputResamplingStep )

    executeCommand( self, subjectName, command, viewOnly )


  def filterBundleAverageFiber( self,
                                processStr,
                                fileNameInputBundleMaps,
                                fileNameOutputBundleMaps,
                                bundleMapFormat,
                                inputResamplingStep,
                                parameters,
                                functors,
                                subjectName,
                                viewOnly ):

    command = 'GkgExecuteCommand DwiBundleMeasure' + \
             ' -b ' + fileNameInputBundleMaps + \
             ' -o ' + fileNameOutputBundleMaps + \
             ' -m bundle_average_fiber'

    executeCommand( self, subjectName, command, viewOnly )


  def filterBundleChangeLabels( self,
                                processStr,
                                fileNameInputBundleMaps,
                                fileNameOutputBundleMaps,
                                bundleMapFormat,
                                inputResamplingStep,
                                parameters,
                                functors,
                                subjectName,
                                viewOnly ):
    newLabels = parameters[ processStr + \
                            'bundleChangeLabelsFilter_NewLabelList' ].getValue()
    oldLabels = parameters[ processStr + \
                            'bundleChangeLabelsFilter_OldLabelList' ].getValue()
    labels = ''
    oldLabels = oldLabels.split()
    newLabels = newLabels.split()
    for oldLabel, newLabel in zip( oldLabels, newLabels ):

      labels += oldLabel + ' ' + newLabel + ' '

    command = 'GkgExecuteCommand DwiBundleOperator' + \
             ' -i ' + fileNameInputBundleMaps + \
             ' -o ' + fileNameOutputBundleMaps + \
             ' -of ' + bundleMapFormat + \
             ' -op change-labels' + \
             ' -stringParameters ' + labels

    executeCommand( self, subjectName, command, viewOnly )


  def filterBundleRoiBased( self,
                             processStr,
                             fileNameInputBundleMaps,
                             fileNameOutputBundleMaps,
                             bundleMapFormat,
                             inputResamplingStep,
                             parameters,
                             functors,
                             subjectName,
                             viewOnly ):

    command = 'GkgExecuteCommand DwiBundleOperator' + \
             ' -i ' + fileNameInputBundleMaps + \
             ' -o ' + fileNameOutputBundleMaps + \
             ' -of ' + bundleMapFormat + \
             ' -op roi-based-selection' + \
             ' -stringParameters ' + \
             parameters[ processStr + \
                         'bundleRoiBasedFilter_FileNameRoi' ].getValue() + \
             ' ' + \
             parameters[ processStr + \
                         'bundleRoiBasedFilter_FileNameTransformationRoiToDw' \
                        ].getValue() + \
             ' -scalarParameters ' + \
             str( parameters[ processStr + \
                             'bundleRoiBasedFilter_MinimumIntersectionLength' \
                            ].getValue() ) + \
             ' ' + \
             str( parameters[ processStr + \
                            'bundleRoiBasedFilter_DoNotProcessInterRoiBundles' \
                            ].getValue() ) + \
             ' ' + \
             str( parameters[ processStr + \
                              'bundleRoiBasedFilter_Offset' ].getValue() )


    executeCommand( self, subjectName, command, viewOnly )


  def filterFiberRoiBased( self,
                           processStr,
                           fileNameInputBundleMaps,
                           fileNameOutputBundleMaps,
                           bundleMapFormat,
                           inputResamplingStep,
                           parameters,
                           functors,
                           subjectName,
                           viewOnly ):

    mode = self._modeAbbreviation [ parameters[ processStr + \
                        'fiberRoiBasedFilter_ThresholdMode' ].getChoice() ]
    command = 'GkgExecuteCommand DwiFiberFilter' + \
             ' -b ' + fileNameInputBundleMaps + \
             ' -o ' + fileNameOutputBundleMaps + \
             ' -s roi_based' + \
             ' -bundleMapFormat ' + bundleMapFormat + \
             ' -stringParameters ' + \
             parameters[ processStr + \
                         'fiberRoiBasedFilter_FileNameRoi' ].getValue() + \
             ' ' + \
             parameters[ processStr + \
                         'fiberRoiBasedFilter_FileNameTransformationRoiToDw' \
                        ].getValue() + \
             ' ' + mode 

    if ( parameters[ processStr + \
                    'fiberRoiBasedFilter_IntersectionType' ].getChoice() == \
                    'with intersection' ):

      command += ' intersection  -scalarParameters '

    else:

       command += ' no_intersection  -scalarParameters '

    if ( mode == 'be' or mode == 'bt' or mode == 'oe' or mode == 'ou' ):

      command += ' ' + str( parameters[ processStr + \
                 'fiberRoiBasedFilter_LowerThreshold' ].getValue() ) + \
                 ' ' + \
                 str( parameters[ processStr + \
                 'fiberRoiBasedFilter_UpperThreshold' ].getValue() )

    else:

      command += ' ' + str( parameters[ processStr + \
                 'fiberRoiBasedFilter_Threshold' ].getValue() ) + \
                 ' 0.0'

    command += ' ' + str( parameters[ processStr + \
              'fiberRoiBasedFilter_MinimumIntersectionLength' ].getValue() ) + \
              ' ' + str( inputResamplingStep )

    executeCommand( self, subjectName, command, viewOnly )


  def filterBundleFusion( self,
                          processStr,
                          fileNameInputBundleMaps,
                          fileNameOutputBundleMaps,
                          bundleMapFormat,
                          inputResamplingStep,
                          parameters,
                          functors,
                          subjectName,
                          viewOnly ):

    command = 'GkgExecuteCommand DwiBundleOperator' + \
             ' -i ' + fileNameInputBundleMaps + \
             ' -o ' + fileNameOutputBundleMaps + \
             ' -of ' + bundleMapFormat + \
             ' -op fusion'

    executeCommand( self, subjectName, command, viewOnly )


  def filterBundleRandomFiberSampling( self,
                                       processStr,
                                       fileNameInputBundleMaps,
                                       fileNameOutputBundleMaps,
                                       bundleMapFormat,
                                       inputResamplingStep,
                                       parameters,
                                       functors,
                                       subjectName,
                                       viewOnly ):

    command = 'GkgExecuteCommand DwiBundleOperator' + \
            ' -i ' + fileNameInputBundleMaps + \
            ' -o ' + fileNameOutputBundleMaps + \
            ' -of ' + bundleMapFormat + \
            ' -op random-selection' + \
            ' -scalarParameters ' + \
            str( parameters[ processStr + \
            'bundleRandomFiberSamplingFilter_KeptFiberPercentage' ].getValue() )

    executeCommand( self, subjectName, command, viewOnly )


  def filterFiberCurvature( self,
                            processStr,
                            fileNameInputBundleMaps,
                            fileNameOutputBundleMaps,
                            bundleMapFormat,
                            inputResamplingStep,
                            parameters,
                            functors,
                            subjectName,
                            viewOnly ):

    mode = self._modeAbbreviation [ parameters[ processStr + \
                        'fiberCurvatureFilter_ThresholdMode' ].getChoice() ]
    command = 'GkgExecuteCommand DwiFiberFilter' + \
             ' -b ' + fileNameInputBundleMaps + \
             ' -o ' + fileNameOutputBundleMaps + \
             ' -s maximum_curvature' + \
             ' -bundleMapFormat '+ bundleMapFormat + \
             ' -stringParameters ' + mode + \
             ' -scalarParameters '
    if ( mode == 'be' or mode == 'bt' or mode == 'oe' or mode == 'ou' ):

      command += str( parameters[ processStr + \
                 'fiberCurvatureFilter_LowerThreshold' ].getValue() ) + \
                 ' ' + \
                 str( parameters[ processStr + \
                 'fiberCurvatureFilter_UpperThreshold' ].getValue() )

    else:

      command += str( parameters[ processStr + \
                      'fiberCurvatureFilter_Threshold' ].getValue() ) + \
                 ' 0.0'

    executeCommand( self, subjectName, command, viewOnly )


  def filterBundleMergeLabels( self,
                               processStr,
                               fileNameInputBundleMaps,
                               fileNameOutputBundleMaps,
                               bundleMapFormat,
                               inputResamplingStep,
                               parameters,
                               functors,
                               subjectName,
                               viewOnly ):

    command = 'GkgExecuteCommand DwiBundleOperator' + \
             ' -i ' + fileNameInputBundleMaps + \
             ' -o ' + fileNameOutputBundleMaps + \
             ' -of ' + bundleMapFormat + \
             ' -op merge-labels' + \
             ' -stringParameters ' + \
             parameters[ processStr + \
                         'bundleMergeLabelsFilter_MergedLabelList' ].getValue()

    executeCommand( self, subjectName, command, viewOnly )


  def filterBundleSelectLabels( self,
                                processStr,
                                fileNameInputBundleMaps,
                                fileNameOutputBundleMaps,
                                bundleMapFormat,
                                inputResamplingStep,
                                parameters,
                                functors,
                                subjectName,
                                viewOnly ):

    command = 'GkgExecuteCommand DwiBundleOperator' + \
             ' -i ' + fileNameInputBundleMaps + \
             ' -o ' + fileNameOutputBundleMaps + \
             ' -of ' + bundleMapFormat + \
             ' -op select-labels' + \
             ' -stringParameters ' + \
             parameters[ processStr + \
             'bundleSelectLabelsFilter_SelectedLabelList' ].getValue()

    executeCommand( self, subjectName, command, viewOnly )



  ##############################################################################
  # display()
  ##############################################################################
  def display( self, functors, parameters, subjectName, verbose,
               isReportModeActive ):

    ########################### resetting the viewer ###########################
    functors[ 'viewer-reset' ]()

    self._currentMesh = None
    self._currentTractograms = list()

    if ( self._application.getViewer()._bundleSlider != None ):

      self._application.getViewer()._bundleSliderLayout.removeWidget( \
                                   self._application.getViewer()._bundleSlider ) 
      self._application.getViewer()._bundleSlider.hide()
      self._application.getViewer()._bundleSlider.deleteLater()
      self._application.getViewer()._bundleSlider = None

    if ( self._application.getViewer()._bundleSliderLabel != None ):

      self._application.getViewer()._bundleSliderLayout.removeWidget( \
                              self._application.getViewer()._bundleSliderLabel )
      self._application.getViewer()._bundleSliderLabel.hide()
      self._application.getViewer()._bundleSliderLabel.deleteLater()
      self._application.getViewer()._bundleSlider = None

    ######################### collecting display options #######################
    self._displayROI = parameters[ 'displayRoiAndDensityMap' ].getValue()
    self._displayIntermediateResults = parameters[ \
                                          'keepIntermediateResults' ].getValue()

    view = 'first_view'
    ################################## setting view ############################
    if ( self._displayROI ):

      functors[ 'viewer-set-view' ]( 'first_view' )

    else:

      functors[ 'viewer-set-view' ]( 'second_view' )
      view = 'second_view'

    #################### setting and connecting step slider ####################
    self._bundleSlider = self._application.getMainThreadActionManager().call( \
                                           QtGui.QSlider, QtCore.Qt.Horizontal )
    self._application.getMainThreadActionManager().call( \
                                             self._bundleSlider.setTickPosition,
                                             QtGui.QSlider.TicksBelow )
    self._application.getMainThreadActionManager().call( \
                                             self._bundleSlider.setTickInterval,
                                             1 )
    self._application.getMainThreadActionManager().call( \
                                                  self._bundleSlider.setMinimum,
                                                  0 )
    self._application.getMainThreadActionManager().call( \
                                                  self._bundleSlider.setMaximum,
                                                  0 )
    self._application.getMainThreadActionManager().call( \
                                                       self._bundleSlider.hide )

    self._application.getViewer()._bundleSlider = self._bundleSlider
    self._bundleSliderLabel = \
                          self._application.getMainThreadActionManager().call( \
                          QtGui.QLabel,
                          '' )
    self._application.getMainThreadActionManager().call( \
                                           self._bundleSliderLabel.setAlignment,
                                           QtCore.Qt.AlignHCenter )
    self._application.getMainThreadActionManager().call( \
                                                  self._bundleSliderLabel.hide )

    windowLayoutSlider = self._application.getMainThreadActionManager().call( \
                                                             QtGui.QVBoxLayout )
    self._application.getViewer()._bundleSliderLayout = windowLayoutSlider
    windowLayoutLabel = self._application.getMainThreadActionManager().call( \
                                                             QtGui.QVBoxLayout )
    self._application.getViewer()._bundleSliderLabel = self._bundleSliderLabel

    self._application.getMainThreadActionManager().call(
                                                   windowLayoutSlider.addWidget,
                                                   self._bundleSlider )
    self._application.getMainThreadActionManager().call(
                                                    windowLayoutLabel.addWidget,
                                                    self._bundleSliderLabel )
    viewLayout = self._application.getViewer().getLayoutFromName( view )
    self._application.getMainThreadActionManager().call( \
                                                viewLayout.addLayout,
                                                windowLayoutSlider, 4, 0, 1, 4 )
    self._application.getMainThreadActionManager().call( \
                                                 viewLayout.addLayout,
                                                 windowLayoutLabel, 5, 0, 1, 4 )

    window = self._application.getViewer()._windows[ view ] \
                                            [ 'T1 + RGB + filtered tractogram' ]

    if ( self._displayIntermediateResults ):

      self._application.getMainThreadActionManager().call(
                                           window.connect,
                                           self._bundleSlider,
                                           QtCore.SIGNAL( 'valueChanged(int)' ),
                                           self.setRoiSliderValues,
                                           QtCore.Qt.UniqueConnection )

    if ( self._displayROI ):

      window = self._application.getViewer()._windows[ view ]\
                                                    [ 'T1 + ROI (axial)' ]
      slider = window.findChild( QtGui.QSlider,
                                PyQt4.QtCore.QString( u'sliderT' ) )
      self._roiSliders.append( slider )

      self._application.getMainThreadActionManager().call(
                                          window.connect,
                                          slider,
                                          QtCore.SIGNAL( 'valueChanged(int)' ),
                                          self.setBundleSliderValue,
                                          QtCore.Qt.UniqueConnection )

      window = self._application.getViewer()._windows[ view ]\
                                                    [ 'T1 + ROI (sagittal)' ]
      slider = window.findChild( QtGui.QSlider,
                                PyQt4.QtCore.QString( u'sliderT' ) )
      self._application.getMainThreadActionManager().call(
                                          window.connect,
                                          slider,
                                          QtCore.SIGNAL( 'valueChanged(int)' ),
                                          self.setBundleSliderValue,
                                          QtCore.Qt.UniqueConnection )
      self._roiSliders.append( slider )
      window = self._application.getViewer()._windows[ view ]\
                                                    [ 'T1 + ROI (coronal)' ]
      slider = window.findChild( QtGui.QSlider,
                                PyQt4.QtCore.QString( u'sliderT' ) )
      self._application.getMainThreadActionManager().call(
                                          window.connect,
                                          slider,
                                          QtCore.SIGNAL( 'valueChanged(int)' ),
                                          self.setBundleSliderValue,
                                          QtCore.Qt.UniqueConnection )
      self._roiSliders.append( slider )

    processes = parameters[ 'processes' ].getValue()
    self._bundleSliderLabel.setText( 'Filter 1 : ' + \
                                     processes[ 0 ][ 'processName' ] )
    self._functors = functors

    #################### setting output bundle map extension ###################
    bundleMapFormat = parameters[ 'bundleMapFormat' ].getChoice()
    if ( bundleMapFormat == 'aimsbundlemap' ):

      extension = '.bundles'

    elif ( bundleMapFormat == 'bundlemap' ):

      extension = '.bundlemap'

    elif ( bundleMapFormat == 'vtkbundlemap' ):

      extension = '.fib'

    elif ( bundleMapFormat == 'trkbundlemap' ):

      extension = '.trk'

    ########################### collecting file names ##########################
    outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()

    fileNameBundleMaps = parameters[ 'fileNameBundleMaps' ].getValue()
    fileNameRgb = parameters[ 'fileNameRgb' ].getValue()
    fileNameT1 = parameters[ 'fileNameT1' ].getValue()
    fileNameTransformationDwToT1 = \
                        parameters[ 'fileNameTransformationDwToT1' ]. getValue()

    fileNameOutputBundleMaps = os.path.join( \
                               parameters[ 'outputWorkDirectory' ].getValue(),
                               'filteredBundleMap' + extension )

    outputDisplayDirectory = os.path.join( outputWorkDirectory,
                                           'temporaryDisplay' )
    fusionOutputBundleMaps = parameters[ 'fusionOutputBundleMaps' ].getValue()

    ################### collecting ROI and density file name ###################
    concatenatedFileNameRoiOrDensityMap = os.path.join( \
                                          outputDisplayDirectory,
                                          'concatenatedRoiAndDensityMap.ima' )

    ########################### loading RGB and T1 #############################
    functors[ 'viewer-create-referential' ]( 'frameDW' )
    functors[ 'viewer-create-referential' ]( 'frameT1' )

    functors[ 'viewer-load-object' ]( fileNameRgb, 'RGB' )
    functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'RGB' )

    if ( len( fileNameT1 ) ):

      # loading T1
      functors[ 'viewer-load-object' ]( fileNameT1, 'T1' )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameT1', 'T1' )

      if ( len( fileNameTransformationDwToT1 ) ):

        functors[ 'viewer-load-transformation' ]( fileNameTransformationDwToT1,
                                                  'frameDW', 'frameT1' )

      # T1 and RGB fusion
      functors[ 'viewer-fusion-objects' ]( [ 'T1', 'RGB'],
                                           'fusionT1AndRGB',
                                           'Fusion2DMethod' )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameT1',
                                                         'fusionT1AndRGB' )

      functors[ 'viewer-assign-referential-to-window' ]( 'frameT1',
                                                         view,
                                                         'T1 + RGB (axial)' )
      functors[ 'viewer-add-object-to-window' ]( 'fusionT1AndRGB',
                                                 view,
                                                 'T1 + RGB (axial)' )

      functors[ 'viewer-assign-referential-to-window' ]( 'frameT1',
                                                         view,
                                                         'T1 + RGB (sagittal)' )
      functors[ 'viewer-add-object-to-window' ]( 'fusionT1AndRGB',
                                                 view,
                                                 'T1 + RGB (sagittal)' )

      functors[ 'viewer-assign-referential-to-window' ]( 'frameT1',
                                                         view,
                                                         'T1 + RGB (coronal)' )
      functors[ 'viewer-add-object-to-window' ]( 'fusionT1AndRGB',
                                                 view,
                                                 'T1 + RGB (coronal)' )

    else:

      # RGB
      functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                         view,
                                                         'T1 + RGB (axial)' )
      functors[ 'viewer-add-object-to-window' ]( 'RGB',
                                                 view,
                                                 'T1 + RGB (axial)' )

      functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                         view,
                                                         'T1 + RGB (sagittal)' )
      functors[ 'viewer-add-object-to-window' ]( 'RGB',
                                                 view,
                                                 'T1 + RGB (sagittal)' )

      functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                         view,
                                                         'T1 + RGB (coronal)' )
      functors[ 'viewer-add-object-to-window' ]( 'RGB',
                                                 view,
                                                 'T1 + RGB (coronal)' )

    ########################### loading original tractogram ####################
    fileNameBundleMaps = fileNameBundleMaps.split( ';' )
    bundleMapIndex = 0
    bundleMapObjectNames = list()
    for fileNameBundleMap in fileNameBundleMaps:

      functors[ 'viewer-load-object' ]( fileNameBundleMap,
                                        'original tractogram ' + \
                                        str( bundleMapIndex ) )
      bundleMapObjectNames.append( 'original tractogram ' + \
                                   str( bundleMapIndex ) ) 
      bundleMapIndex += 1

    for bundleMapObjectName in bundleMapObjectNames:

      functors[ 'viewer-set-diffuse' ]( bundleMapObjectName,
                                        [ 0.1, 1.0, 1.0, 0.1 ] )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW',
                                                         bundleMapObjectName )

      functors[ 'viewer-add-object-to-window' ]( \
                                              bundleMapObjectName,
                                              view,
                                              'T1 + RGB + original tractogram' )

    if ( len( fileNameT1 ) ):

      functors[ 'viewer-assign-referential-to-window' ](
                                              'frameT1',
                                              view,
                                              'T1 + RGB + original tractogram' )
      functors[ 'viewer-assign-referential-to-window' ](
                                              'frameT1',
                                              view,
                                              'T1 + RGB + original tractogram' )

    else:

      functors[ 'viewer-assign-referential-to-window' ](
                                              'frameDW',
                                              view,
                                              'T1 + RGB + original tractogram' )
      functors[ 'viewer-assign-referential-to-window' ](
                                             'frameDW',
                                              view,
                                              'T1 + RGB + original tractogram' )

    if ( len( fileNameT1 ) ):

      functors[ 'viewer-add-object-to-window' ](
                                              'fusionT1AndRGB',
                                              view,
                                              'T1 + RGB + original tractogram' )

      functors[ 'viewer-assign-referential-to-window' ](
                                              'frameT1',
                                              view,
                                              'T1 + RGB + filtered tractogram' )
      functors[ 'viewer-assign-referential-to-window' ](
                                              'frameT1',
                                              view,
                                              'T1 + RGB + filtered tractogram' )
      functors[ 'viewer-add-object-to-window' ](
                                              'fusionT1AndRGB',
                                              view,
                                              'T1 + RGB + filtered tractogram' )
    else:

      functors[ 'viewer-add-object-to-window' ](
                                              'RGB',
                                              view,
                                              'T1 + RGB + original tractogram' )

      functors[ 'viewer-assign-referential-to-window' ](
                                              'frameDW',
                                              view,
                                              'T1 + RGB + filtered tractogram' )
      functors[ 'viewer-assign-referential-to-window' ](
                                              'frameDW',
                                              view,
                                              'T1 + RGB + filtered tractogram' )
      functors[ 'viewer-add-object-to-window' ](
                                              'RGB',
                                              view,
                                              'T1 + RGB + filtered tractogram' )

    ####################### loading roi and density map ########################
    functors[ 'condition-wait-and-release' ]( subjectName,
                                              'roi-concatenation-processed' )
    if ( self._displayIntermediateResults ):

      self._application.getMainThreadActionManager().call( \
                                                       self._bundleSlider.show )
      self._application.getMainThreadActionManager().call( \
                                                  self._bundleSliderLabel.show )

    if ( self._displayROI ):

      if ( os.path.exists( concatenatedFileNameRoiOrDensityMap ) ):

        functors[ 'viewer-load-object' ]( concatenatedFileNameRoiOrDensityMap,
                                          'ROIDensityMap' )
        functors[ 'viewer-assign-referential-to-object' ]( 'frameT1',
                                                          'ROIDensityMap' )
        functors[ 'viewer-set-colormap' ]( 'ROIDensityMap', 'BLUE-lfusion' )
        functors[ 'viewer-fusion-objects' ]( [ 'T1', 'ROIDensityMap'],
                                            'fusionT1AndRoiDensityMap',
                                            'Fusion2DMethod' )
      if ( len( fileNameT1 ) ):

        functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                           view,
                                                           'T1 + ROI (axial)' )

        if ( os.path.exists( concatenatedFileNameRoiOrDensityMap ) ):

          functors[ 'viewer-add-object-to-window' ]( 'fusionT1AndRoiDensityMap',
                                                     view,
                                                     'T1 + ROI (axial)' )

        else:

          functors[ 'viewer-add-object-to-window' ]( 'T1',
                                                     view,
                                                     'T1 + ROI (axial)' )

        if ( os.path.exists( concatenatedFileNameRoiOrDensityMap ) ):

          functors[ 'viewer-add-object-to-window' ]( 'fusionT1AndRoiDensityMap',
                                                     view,
                                                     'T1 + ROI (sagittal)' )

        else:

          functors[ 'viewer-add-object-to-window' ]( 'T1',
                                                     view,
                                                     'T1 + ROI (sagittal)' )

        if ( os.path.exists( concatenatedFileNameRoiOrDensityMap ) ):

          functors[ 'viewer-add-object-to-window' ]( 'fusionT1AndRoiDensityMap',
                                                     view,
                                                     'T1 + ROI (coronal)' )

        else:

          functors[ 'viewer-add-object-to-window' ]( 'T1',
                                                     view,
                                                     'T1 + ROI (coronal)' )


      else:

        if ( os.path.exists( concatenatedFileNameRoiOrDensityMap ) ):

          functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                            view,
                                                            'T1 + ROI (axial)' )
          functors[ 'viewer-add-object-to-window' ]( 'ROIDensityMap',
                                                     view,
                                                     'T1 + ROI (axial)' )

          functors[ 'viewer-assign-referential-to-window' ]( \
                                                        'frameDW',
                                                        view,
                                                        'T1 + ROI (sagittal)' )
          functors[ 'viewer-add-object-to-window' ]( 'ROIDensityMap',
                                                     view,
                                                     'T1 + ROI (sagittal)' )

          functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                          view,
                                                          'T1 + ROI (coronal)' )
          functors[ 'viewer-add-object-to-window' ]( 'ROIDensityMap',
                                                     view,
                                                     'T1 + ROI (coronal)' )

      index = 0
      for mesh, meshTransformationROITODW in zip( \
                                          self._meshes,
                                          self._meshesTransformationROIToDW ):

        functors[ 'viewer-create-referential' ]( 'Roi' + str( index ) )
        functors[ 'viewer-load-transformation' ]( meshTransformationROITODW,
                                                  'Roi' + str( index ),
                                                  'frameDW' )
        functors[ 'viewer-load-object' ]( mesh,
                                            'mesh' + str( index ) )
        functors[ 'viewer-assign-referential-to-object' ]( \
                                                         'Roi' + str( index ),
                                                         'mesh' + str( index ) )

        index += 1

    ######################### display output bundles #########################
    bundleMapFileNamePrefixes = list()
    fileNameInputBundleMapsList = parameters[ 'fileNameBundleMaps' ].getValue()
    fileNameInputBundleMapsList = fileNameInputBundleMapsList.split( ';' )
    for fileNameInputBundleMap in fileNameInputBundleMapsList:

      baseName = os.path.basename( fileNameInputBundleMap )
      fileName = os.path.splitext( baseName )[ 0 ]
      bundleMapFileNamePrefixes.append( fileName + '_' )

    fileNameBundleMaps = None

    index = 0
    for process in processes:

      #################### setting output names ############################
      fileNameOutputBundleMaps = list()
      if (  self._displayIntermediateResults ):

        conditionName = 'filtering' + str( index ) + '-processed'

        functors[ 'condition-wait-and-release' ]( subjectName,
                                                  conditionName )
        if ( index < len( processes  ) - 1 ):

          stepOutputWorkDirectory = os.path.join( outputWorkDirectory,
                                                  'intermediateResults',
                                                  'step' + \
                                                  str( index + 1 ) )

          if not ( os.path.exists( stepOutputWorkDirectory ) ):

            os.mkdir( stepOutputWorkDirectory )

          if ( fusionOutputBundleMaps ):

            fileNameOutputBundleMaps.append( os.path.join( \
                                          stepOutputWorkDirectory,
                                          'intermediateFilteredBundleMap' + \
                                          str( index + 1 ) + extension + ' ' ) )
          else:

            for bundleMapFileNamePrefix in bundleMapFileNamePrefixes:

              fileNameOutputBundleMaps.append( os.path.join( \
                                         stepOutputWorkDirectory,
                                         bundleMapFileNamePrefix + \
                                         'intermediateFilteredBundleMap' + \
                                          str( index + 1 ) + extension + ' ' ) )

        else:

          if ( fusionOutputBundleMaps ):

            fileNameOutputBundleMaps.append( os.path.join( \
                                             outputWorkDirectory,
                                             'filteredBundleMap' + \
                                             extension + ' ' ) )
          else:

            for bundleMapFileNamePrefix in bundleMapFileNamePrefixes:

              fileNameOutputBundleMaps.append( os.path.join( \
                                               outputWorkDirectory,
                                               bundleMapFileNamePrefix + \
                                               'filteredBundleMap' + \
                                               extension + ' ' ) )
        bundleMapIndex = 0
        for fileNameOutputBundleMap in fileNameOutputBundleMaps:

          fileNameOutputBundleMap = fileNameOutputBundleMap.replace( ' ', '' )
          functors[ 'viewer-load-object' ]( \
                                   fileNameOutputBundleMap,
                                   'filtered' + str( index ) + \
                                   '_' + str( bundleMapIndex ) + '_tractogram' )
          functors[ 'viewer-set-diffuse' ]( \
                                    'filtered' + str( index ) + \
                                    '_' + str( bundleMapIndex ) + '_tractogram',
                                    [ 0.1, 1.0, 1.0, 0.1 ] )
          functors[ 'viewer-assign-referential-to-object' ]( \
                                   'frameDW',
                                   'filtered' + str( index ) + \
                                   '_' + str( bundleMapIndex ) + '_tractogram' )
          bundleMapIndex += 1

      else:

        functors[ 'condition-wait-and-release' ]( subjectName,
                                                 'filtering-processed' )
        if ( fusionOutputBundleMaps ):

          fileNameOutputBundleMaps.append( os.path.join( \
                                       outputWorkDirectory,
                                       'filteredBundleMap' + extension + ' ' ) )
        else:

          for bundleMapFileNamePrefix in bundleMapFileNamePrefixes:

            fileNameOutputBundleMaps.append( os.path.join( \
                                                    outputWorkDirectory,
                                                    bundleMapFileNamePrefix + \
                                                    'filteredBundleMap' + \
                                                    extension + ' ' ) )
        bundleMapIndex = 0
        for fileNameOutputBundleMap in fileNameOutputBundleMaps:

          fileNameOutputBundleMap = fileNameOutputBundleMap.replace( ' ', '' )
          functors[ 'viewer-load-object' ]( \
                                   fileNameOutputBundleMap,
                                   'filtered' + str( index ) + \
                                   '_' + str( bundleMapIndex ) + '_tractogram' )
          functors[ 'viewer-set-diffuse' ]( \
                                    'filtered' + str( index ) + \
                                    '_' + str( bundleMapIndex ) + '_tractogram',
                                    [ 0.1, 1.0, 1.0, 0.1 ] )
          functors[ 'viewer-assign-referential-to-object' ]( \
                                   'frameDW',
                                   'filtered' + str( index ) + \
                                   '_' + str( bundleMapIndex ) + '_tractogram' )
          bundleMapIndex += 1

        self._application.getMainThreadActionManager().call(
                                                self._bundleSlider.setMaximum,
                                                index )
        self._application.getMainThreadActionManager().call(
                                                self._bundleSlider.setValue,
                                                index )
        self.setRoiSliderValues( 0 )


        self._application.getAlgorithmGUI().enableRemovePushButtons( \
                                                           outputWorkDirectory )
        break


      self._application.getMainThreadActionManager().call(
                                                self._bundleSlider.setMaximum,
                                                index )
      self._application.getMainThreadActionManager().call(
                                                self._bundleSlider.setValue,
                                                index )

      if ( index == 0 ):

        self.setRoiSliderValues( 0 )
          
      index += 1

    self._application.getAlgorithmGUI().enableRemovePushButtons( \
                                                           outputWorkDirectory )

  def setBundleSliderValue( self, value ):

    if ( self._displayROI ):

      view = 'first_view'

    else:

      view = 'second_view'

    if not ( self._bundleSlider.value() == self._filterWithRoiOrDensityMap[ \
                                                                      value ] ):
      if not ( self._functors == None and self._currentStep == value ):

        if ( self._displayROI ):

          if ( self._currentMesh != None ):

            self._functors[ 'viewer-remove-object-from-window' ](
                            self._currentMesh,
                            view,
                            'T1 + RGB + filtered tractogram' )
            self._currentMesh = None

          if ( self._currentMesh == None \
            and self._meshIndices.count( \
                                   self._filterWithRoiOrDensityMap[ value ] ) ):

            self._functors[ 'viewer-add-object-to-window' ](
                            'mesh' + \
                            str( self._meshIndices.index( \
                                self._filterWithRoiOrDensityMap[ value ] ) ),
                            view,
                            'T1 + RGB + filtered tractogram' )
            self._currentMesh = 'mesh' + \
                str( self._meshIndices.index( \
                    self._filterWithRoiOrDensityMap[ value ] ) )

        if ( self._displayIntermediateResults ):

          self._bundleSliderLabel.setText( \
                      'Filter ' + \
                      str( self._filterWithRoiOrDensityMap[ value ] + 1 ) + \
                      ' : ' + \
                      self._steps[ self._filterWithRoiOrDensityMap[ value ] ] )

          if ( self._currentTractograms != list() ):

            for currentTractogram in self._currentTractograms:

              self._functors[ 'viewer-remove-object-from-window' ](
                                              currentTractogram,
                                              view,
                                              'T1 + RGB + filtered tractogram' )
            self._currentTractograms = list()

          for i in xrange( self._outputBundleMapCount ):

            self._functors[ 'viewer-add-object-to-window' ]( \
                'filtered' + \
                str( self._filterWithRoiOrDensityMap[ value ] ) + \
                '_' + str( i ) + '_tractogram',
                view,
                'T1 + RGB + filtered tractogram' )
          self._currentStep = value
          self._currentTractograms.append( 'filtered' + str( value ) + \
                                         '_' + str( i ) + '_tractogram' )

        self.updateRoiSliders( value )

      self._application.getMainThreadActionManager().call(
                                    self._bundleSlider.setValue,
                                    self._filterWithRoiOrDensityMap[ value ] )

  def setRoiSliderValues( self, value ):

    if ( self._displayROI ):

      view = 'first_view'

    else:

      view = 'second_view'

    if not( self._functors == None and self._currentStep == value ):

      if ( self._displayROI ):

        if ( self._filterWithRoiOrDensityMap.count( value ) ):

          roiValue = self._filterWithRoiOrDensityMap.index( value )
          if ( self._currentMesh != None ):

            self._functors[ 'viewer-remove-object-from-window' ](
                                             self._currentMesh,
                                             view,
                                             'T1 + RGB + filtered tractogram' )
            self._currentMesh = None

          if ( self._currentMesh == None and self._meshIndices.count( value ) ):

            self._functors[ 'viewer-add-object-to-window' ](
                               'mesh' + str( self._meshIndices.index( value ) ),
                               view,
                               'T1 + RGB + filtered tractogram' )
            self._currentMesh = 'mesh' + str( self._meshIndices.index( value ) )

          self.updateRoiSliders( roiValue )

      if ( self._displayIntermediateResults ):

        self._bundleSliderLabel.setText( \
                                        'Filter ' + str( value + 1 ) + ' : ' + \
                                        self._steps[ value ] )
      if ( self._currentTractograms != list() ):

        for currentTractogram in self._currentTractograms:
           
          self._functors[ 'viewer-remove-object-from-window' ](
                          currentTractogram,
                          view,
                          'T1 + RGB + filtered tractogram' )

      self._currentTractograms = list()

      for i in xrange( self._outputBundleMapCount ):

        self._functors[ 'viewer-add-object-to-window' ]( \
                                         'filtered' + str( value ) + \
                                         '_' + str( i ) + '_tractogram',
                                         view,
                                         'T1 + RGB + filtered tractogram' )
        self._currentTractograms.append( 'filtered' + str( value ) + \
                                         '_' + str( i ) + '_tractogram' )

      self._currentStep = value


  def updateRoiSliders( self, value ):

    for roiSlider in self._roiSliders:

      if not ( roiSlider.value() == value ):

        self._application.getMainThreadActionManager().call( roiSlider.setValue,
                                                             value )


#GkgHistogrammAnalyser -i ppn.ima -o tmp
#f = open( tmp )
#lines = f.readlines()
#for line in lines:
#    line = line.split( '\t' )
#    if line[1] != '0':
#        print line[0]
#        print line[1]
#f.close()





