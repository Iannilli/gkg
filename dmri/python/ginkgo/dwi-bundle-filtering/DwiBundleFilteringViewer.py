from Viewer import *


class DwiBundleFilteringViewer( Viewer ):

  def __init__( self, minimumSize, parent ):

    Viewer.__init__( self, minimumSize, parent )

    ########################## First View ######################################

    self.createView( 'first_view' )

    self.addAxialWindow( 'first_view', 0, 0, 1, 1,
                         'T1 + RGB (axial)', 'T1 + RGB (axial)',
                         [ 0.0, 0.0, 0.0, 1.0 ], False )
    self.addSagittalWindow( 'first_view', 0, 1, 1, 1,
                            'T1 + RGB (sagittal)', 'T1 + RGB (sagittal)',
                            [ 0.0, 0.0, 0.0, 1.0 ], False )
    self.addCoronalWindow( 'first_view', 1, 0, 1, 1,
                           'T1 + RGB (coronal)', 'T1 + RGB (coronal)',
                           [ 0.0, 0.0, 0.0, 1.0 ], False )

    self.addAxialWindow( 'first_view', 2, 0, 1, 1,
                         'T1 + ROI (axial)', 'T1 + ROI (axial)',
                         [ 0.0, 0.0, 0.0, 1.0 ], False )
    self.addSagittalWindow( 'first_view', 2, 1, 1, 1,
                            'T1 + ROI (sagittal)', 'T1 + ROI (sagittal)',
                            [ 0.0, 0.0, 0.0, 1.0 ], False )
    self.addCoronalWindow( 'first_view', 3, 0, 1, 1,
                           'T1 + ROI (coronal)', 'T1 + ROI (coronal)',
                           [ 0.0, 0.0, 0.0, 1.0 ], False )

  
    self.add3DWindow( 'first_view', 0, 2, 2, 2,
                      'T1 + RGB + original tractogram',
                      'T1 + RGB + original tractogram' )
    self.add3DWindow( 'first_view', 2, 2, 2, 2,
                      'T1 + RGB + filtered tractogram',
                      'T1 + RGB + filtered tractogram' )

    ########################## Second View ######################################

    self.createView( 'second_view' )

    self.addAxialWindow( 'second_view', 0, 0, 1, 1,
                         'T1 + RGB (axial)', 'T1 + RGB (axial)',
                         [ 0.0, 0.0, 0.0, 1.0 ], False )
    self.addSagittalWindow( 'second_view', 0, 1, 1, 1,
                            'T1 + RGB (sagittal)', 'T1 + RGB (sagittal)',
                            [ 0.0, 0.0, 0.0, 1.0 ], False )
    self.addCoronalWindow( 'second_view', 1, 0, 1, 1,
                           'T1 + RGB (coronal)', 'T1 + RGB (coronal)',
                           [ 0.0, 0.0, 0.0, 1.0 ], False )

    self.add3DWindow( 'second_view', 0, 2, 2, 2,
                      'T1 + RGB + original tractogram',
                      'T1 + RGB + original tractogram' )
    self.add3DWindow( 'second_view', 2, 2, 2, 2,
                      'T1 + RGB + filtered tractogram',
                      'T1 + RGB + filtered tractogram' )

    self._bundleSlider = None
    self._bundleSliderLabel = None
    self._bundleSliderLayout = None


def createDwiBundleFilteringViewer( minimumSize, parent ):

  return DwiBundleFilteringViewer( minimumSize, parent )

