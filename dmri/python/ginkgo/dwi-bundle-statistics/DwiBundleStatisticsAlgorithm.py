from Algorithm import *
from DwiBundleStatisticsTask import *


class DwiBundleStatisticsAlgorithm( Algorithm ):

  def __init__( self, verbose ):

    Algorithm.__init__( self, 'DWI-Bundle-Statistics', verbose, True )

    ############################################################################
    # Input data
    ############################################################################

    self.addParameter( StringParameter( 'fileNameBundleMaps', '' ) )

    ############################################################################
    # Filter type interface
    ############################################################################

    self.addParameter( ProcessListParameter( 'processes',
         {   'Bundle - fiber count statistics': \
                    { 'index': 0 },
             'Bundle - length statistics': \
                    { 'index': 1 },
             'Bundle - tortuosity statistics': \
                    { 'index': 2 },
             'Bundle - volume profile along average fiber': \
                    { 'index': 3,
                      'bundleVolumeProfileAlongAverageFiber_fileNameVolume': \
                            { 'name': 'Volume fileName:',
                              'type': 'string',
                              'widgetType': 'lineEditAndBrowser',
                              'defaultValue': '',
                              'index': 0
                             },
                      'bundleVolumeProfileAlongAverageFiber_backgroundIntensityLevel': \
                            { 'name': 'Background intensity level:',
                              'type': 'integer',
                              'minimumValue': 0,
                              'maximumValue': 10000,
                              'defaultValue': 0,
                              'incrementStep': 1,
                              'widgetType': 'spinBox',
                              'defaultValue': 0,
                              'index': 1
                             },
                      'bundleVolumeProfileAlongAverageFiber_rankOfVolume': \
                            { 'name': 'Rank of the volume to be used:',
                              'type': 'integer',
                              'minimumValue': 0,
                              'maximumValue': 10000,
                              'defaultValue': 0,
                              'incrementStep': 1,
                              'widgetType': 'spinBox',
                              'defaultValue': 0,
                              'index': 2
                             }
                    },
             'Bundle - volume statistics along average fiber': \
                    { 'index': 4,
                      'bundleVolumeStatAlongAverageFiber_fileNameVolume': \
                            { 'name': 'Volume fileName:',
                              'type': 'string',
                              'widgetType': 'lineEditAndBrowser',
                              'defaultValue': '',
                              'index': 0
                             },
                     'bundleVolumeStatAlongAverageFiber_backgroundIntensityLevel': \
                            { 'name': 'Background intensity level:',
                              'type': 'integer',
                              'minimumValue': 0,
                              'maximumValue': 10000,
                              'defaultValue': 0,
                              'incrementStep': 1,
                              'widgetType': 'spinBox',
                              'defaultValue': 0,
                              'index': 1
                             },
                     'bundleVolumeStatAlongAverageFiber_rankOfVolume': \
                            { 'name': 'Rank of the volume to be used:',
                              'type': 'integer',
                              'minimumValue': 0,
                              'maximumValue': 10000,
                              'defaultValue': 0,
                              'incrementStep': 1,
                              'widgetType': 'spinBox',
                              'defaultValue': 0,
                              'index': 2
                             }
                    },
             'Fiber - curvature profile': \
                    {'index': 5 },
             'Fiber - curvature statistics': \
                    {'index': 6 },
             'Fiber - volume profile': \
                    { 'index': 7,
                      'fiberVolumeProfile_fileNameVolume': \
                            { 'name': 'Volume fileName:',
                              'type': 'string',
                              'widgetType': 'lineEditAndBrowser',
                              'defaultValue': '',
                              'index': 0
                             },
                     'fiberVolumeProfile_backgroundIntensityLevel': \
                            { 'name': 'Background intensity level:',
                              'type': 'integer',
                              'minimumValue': 0,
                              'maximumValue': 10000,
                              'defaultValue': 0,
                              'incrementStep': 1,
                              'widgetType': 'spinBox',
                              'defaultValue': 0,
                              'index': 1
                             },
                     'fiberVolumeProfile_rankOfVolume': \
                            { 'name': 'Rank of the volume to be used:',
                              'type': 'integer',
                              'minimumValue': 0,
                              'maximumValue': 10000,
                              'defaultValue': 0,
                              'incrementStep': 1,
                              'widgetType': 'spinBox',
                              'defaultValue': 0,
                              'index': 2
                             }
                    },
             'Fiber - volume statistics': \
                    { 'index': 8,
                      'fiberVolumeStatistics_fileNameVolume': \
                            { 'name': 'Volume fileName:',
                              'type': 'string',
                              'widgetType': 'lineEditAndBrowser',
                              'defaultValue': '',
                              'index': 0
                             },
                     'fiberVolumeStatistics_backgroundIntensityLevel': \
                            { 'name': 'Background intensity level:',
                              'type': 'integer',
                              'minimumValue': 0,
                              'maximumValue': 10000,
                              'defaultValue': 0,
                              'incrementStep': 1,
                              'widgetType': 'spinBox',
                              'defaultValue': 0,
                              'index': 1
                             },
                     'fiberVolumeStatistics_rankOfVolume': \
                            { 'name': 'Rank of the volume to be used:',
                              'type': 'integer',
                              'minimumValue': 0,
                              'maximumValue': 10000,
                              'defaultValue': 0,
                              'incrementStep': 1,
                              'widgetType': 'spinBox',
                              'defaultValue': 0,
                              'index': 2
                             }
                    },
             'Fiber - length statistics': \
                    {'index': 9 },
             'Fiber - maximum curvature statistics': \
                    {'index': 10 },
             'Fiber - tortuosity statistics': \
                    {'index': 11 }
         } ) )

    ###########################################################################
    # Output
    ###########################################################################

    self.addParameter( ChoiceParameter( 'viewType',
                                        0,
                                        [ 'Simple View',
                                          'Multiple View' ] ) )

  def launch( self ):

    if ( self._verbose ):

      print \
        'running DWI bundle statisctics'

    task = DwiBundleStatisticsTask( self._application )
    task.launch( False )


  def view( self ):

    if ( self._verbose ):

      print \
        'viewing DWI bundle Statistics'

    task = DwiBundleStatisticsTask( self._application )
    task.launch( True )

