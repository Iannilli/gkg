from AlgorithmGUI import *
from ResourceManager import *
from ViewerCallbackManager import *
from ProcessListWidget import *

class DwiBundleStatisticsAlgorithmGUI( AlgorithmGUI ):

  def __init__( self, algorithm ):

    AlgorithmGUI.__init__( self,
                           algorithm,
                           ResourceManager().getUIFileName(
                                              'dmri',
                                              'DwiBundleStatistics.ui' ) )

    ############################################################################
    # creating a viewer callback manager
    ############################################################################

    self._viewerCallbackManager = ViewerCallbackManager( self.getAlgorithm() )


    ############################################################################
    # Connecting data interface
    ############################################################################


    # Input Bundle map name and push button
    self.connectStringParameterToLineEdit( 'fileNameBundleMaps',
                                           'lineEdit_FileNameBundleMaps' )
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit(\
                                           'pushButton_FileNameBundleMaps',
                                           'lineEdit_FileNameBundleMaps' )

    ###########################################################################
    # Measure chain
    ###########################################################################

    self._processListWidget = ProcessListWidget( \
                                    self._algorithm.getParameter( 'processes' ),
                                    self,
                                    'measure',
                                    'groupBox_MeasureChain',
                                    'Bundle - fiber count statistics' )

    self.connectProcessListParameterToProcessListWidget( \
                                                       'processes',
                                                       self._processListWidget )


  def connectProcessListParameterToProcessListWidget( \
                                                     self,
                                                     processListParameterName,
                                                     processListWidget ):

      parameter = self._algorithm.getParameter( processListParameterName )
      parameter.setWidget( 'processListWidget', processListWidget )


