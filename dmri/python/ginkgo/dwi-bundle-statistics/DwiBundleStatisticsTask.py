from Task import *
import time


class DwiBundleStatisticsTask( Task ):

  def __init__( self, application ):
  
    Task.__init__( self, application )

    self._modeAbbreviation = dict()
    self._modeAbbreviation [ 'lower than' ] = 'lt'
    self._modeAbbreviation [ 'lower or equal to' ] = 'le'
    self._modeAbbreviation [ 'different from' ] = 'di'
    self._modeAbbreviation [ 'equal to' ] = 'eq'
    self._modeAbbreviation [ 'greater or equal to' ] = 'ge'
    self._modeAbbreviation [ 'greater than' ] = 'gt'
    self._modeAbbreviation [ 'between or equal' ] = 'be'
    self._modeAbbreviation [ 'between' ] = 'bt'
    self._modeAbbreviation [ 'outside or equal' ] = 'oe'
    self._modeAbbreviation [ 'outside' ] = 'ou'

    self.addFunctor( 'viewer-listWidget-insert-items',
                     self.viewerListWidgetInsertItems )
    self.addFunctor( 'viewer-listWidget-set-current-row',
                     self.viewerListWidgetsetCurrentRow )
    self.addFunctor( 'viewer-connect-listWidget-to-callBack',
                     self.viewerConnectListWidgetToCallBack )
    self.addFunctor( 'viewer-add-multi-subject-outpuWorkDirectory',
                     self.viewerAddMultiSubjectOutputWorkDirectory )

  ##############################################################################
  # execute()
  ##############################################################################

  def execute( self, functors, parameters, subjectName, verbose, viewOnly ):

    try:

      ####################### acquiring conditions #############################
      functors[ 'condition-acquire' ]( subjectName, 'measures-processed' )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 0 )

      ########################### collecting file names ########################
      outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()

      fileNameBundleMaps = parameters[ 'fileNameBundleMaps' ].getValue().split()

      processes = parameters[ 'processes' ].getValue()

      measures = dict()

      measures[ 'Bundle - fiber count statistics' ] = \
                                                    self.measureBundleFiberCount
      measures[ 'Bundle - length statistics' ] =  self.measureBundleLength
      measures[ 'Bundle - tortuosity statistics' ] = \
                                                    self.measureBundleTortuosity
      measures[ 'Bundle - volume profile along average fiber' ] = \
                                self.measureBundleVolumeProfileAlongAverageFiber
      measures[ 'Bundle - volume statistics along average fiber' ] = \
                                       self.measureBundleVolumeAlongAverageFiber

      progress = 0
      progressStep = 100 / len( processes )
      index = 0
      for process in processes:

        print 'Filter:', process[ 'processName' ]
        for fileNameBundleMap in fileNameBundleMaps:

          measures[ process[ 'processName' ] ]( fileNameBundleMap.strip(),
                                                outputWorkDirectory,
                                                process,
                                                index,
                                                functors,
                                                subjectName,
                                                viewOnly )
        index += 1
        progress += progressStep
        functors[ 'update-progress-bar' ]( subjectName, progress )

      functors[ 'viewer-add-multi-subject-outpuWorkDirectory' ]( 'first_view',
                                                          'results',
                                                           subjectName,
                                                           outputWorkDirectory )
                                                           
      ####################### notifying display thread #########################
      functors[ 'condition-notify-and-release' ]( subjectName,
                                                  'measures-processed' )

      ############################ updating progress bar #######################
      functors[ 'update-progress-bar' ]( subjectName, 100 )

    except:

      self._functors[ 'task-set-status-as-failed' ]( subjectName,
                                                     sys.exc_info() )

  def measureBundleFiberCount( self,
                               fileNameInputBundleMap,
                               outputWorkDirectory,
                               parameters,
                               index,
                               functors,
                               subjectName,
                               viewOnly ):

    fileNameBundleMap = os.path.splitext( \
                             os.path.split( fileNameInputBundleMap )[ 1 ] )[ 0 ]
    bundleMapName = fileNameBundleMap
    if( fileNameBundleMap.count( subjectName ) != 0 ):

      subjectNameLocation = fileNameBundleMap.find( subjectName )
      bundleMapName = fileNameBundleMap[ 0 : subjectNameLocation ] + \
              fileNameBundleMap[ ( subjectNameLocation + len( subjectName ) ): \
                                                      len( fileNameBundleMap ) ]

    command = 'GkgExecuteCommand DwiBundleMeasure' + \
              ' -b ' + fileNameInputBundleMap + \
              ' -o ' + os.path.join( outputWorkDirectory,bundleMapName + \
                         '_bundle_fiber_count' ) + \
              ' -m bundle_fiber_count'

    executeCommand( self, subjectName, command, viewOnly )


  def measureBundleLength( self,
                           fileNameInputBundleMap,
                           outputWorkDirectory,
                           parameters,
                           index,
                           functors,
                           subjectName,
                           viewOnly ):

    fileNameBundleMap = os.path.splitext( \
                             os.path.split( fileNameInputBundleMap )[ 1 ] )[ 0 ]
    bundleMapName = fileNameBundleMap
    if( fileNameBundleMap.count( subjectName ) != 0 ):

      subjectNameLocation = fileNameBundleMap.find( subjectName )
      bundleMapName = fileNameBundleMap[ 0 : subjectNameLocation ] + \
             fileNameBundleMap[ subjectNameLocation : len( fileNameBundleMap ) ]

    command = 'GkgExecuteCommand DwiBundleMeasure' + \
              ' -b ' + fileNameInputBundleMap + \
              ' -o ' + os.path.join( outputWorkDirectory,
                         os.path.splitext( \
                         os.path.split( fileNameInputBundleMap )[ 1 ] )[ 0 ] + \
                         '_bundle_fiber_length_statistics' ) + \
              ' -m bundle_fiber_length_statistics'

    executeCommand( self, subjectName, command, viewOnly )


  def measureBundleTortuosity( self,
                               fileNameInputBundleMap,
                               outputWorkDirectory,
                               parameters,
                               index,
                               functors,
                               subjectName,
                               viewOnly ):

    fileNameBundleMap = os.path.splitext( \
                             os.path.split( fileNameInputBundleMap )[ 1 ] )[ 0 ]
    bundleMapName = fileNameBundleMap
    if( fileNameBundleMap.count( subjectName ) != 0 ):

      subjectNameLocation = fileNameBundleMap.find( subjectName )
      bundleMapName = fileNameBundleMap[ 0 : subjectNameLocation ] + \
             fileNameBundleMap[ subjectNameLocation : len( fileNameBundleMap ) ]

    command = 'GkgExecuteCommand DwiBundleMeasure' + \
              ' -b ' + fileNameInputBundleMap + \
              ' -o ' + os.path.join( outputWorkDirectory,
                         os.path.splitext( \
                         os.path.split( fileNameInputBundleMap )[ 1 ] )[ 0 ] + \
                         '_bundle_fiber_tortuosity_statistics' ) + \
              ' -m bundle_fiber_tortuosity_statistics'

    executeCommand( self, subjectName, command, viewOnly )


  def measureBundleVolumeProfileAlongAverageFiber( self,
                                                   fileNameInputBundleMap,
                                                   outputWorkDirectory,
                                                   parameters,
                                                   index,
                                                   functors,
                                                   subjectName,
                                                   viewOnly ):

    fileNameBundleMap = os.path.splitext( \
                             os.path.split( fileNameInputBundleMap )[ 1 ] )[ 0 ]
    bundleMapName = fileNameBundleMap
    if( fileNameBundleMap.count( subjectName ) != 0 ):

      subjectNameLocation = fileNameBundleMap.find( subjectName )
      bundleMapName = fileNameBundleMap[ 0 : subjectNameLocation ] + \
             fileNameBundleMap[ subjectNameLocation : len( fileNameBundleMap ) ]

    command = 'GkgExecuteCommand DwiBundleMeasure' + \
              ' -b ' + fileNameInputBundleMap + \
              ' -o ' + os.path.join( outputWorkDirectory,
                         os.path.splitext( \
                         os.path.split( fileNameInputBundleMap )[ 1 ] )[ 0 ] + \
                         '_from_volume_profile_along_average_fiber' ) + \
              ' -m from_volume_profile_along_average_fiber' + \
              ' -stringParameters ' + \
              parameters[ 'Process' + str( index ) + \
                        '_bundleVolumeProfileAlongAverageFiber_fileNameVolume' \
                        ].getValue() + \
              ' -scalarParameters ' + \
              str( parameters[ 'Process' + str( index ) + \
              '_bundleVolumeProfileAlongAverageFiber_backgroundIntensityLevel' \
                        ].getValue() ) + \
              ' ' + str( parameters[ 'Process' + str( index ) + \
                          '_bundleVolumeProfileAlongAverageFiber_rankOfVolume' \
                              ].getValue() )

    executeCommand( self, subjectName, command, viewOnly )


  def measureBundleVolumeAlongAverageFiber( self,
                                            fileNameInputBundleMap,
                                            outputWorkDirectory,
                                            parameters,
                                            index,
                                            functors,
                                            subjectName,
                                            viewOnly ):

    fileNameBundleMap = os.path.splitext( \
                             os.path.split( fileNameInputBundleMap )[ 1 ] )[ 0 ]
    bundleMapName = fileNameBundleMap
    if( fileNameBundleMap.count( subjectName ) != 0 ):

      subjectNameLocation = fileNameBundleMap.find( subjectName )
      bundleMapName = fileNameBundleMap[ 0 : subjectNameLocation ] + \
             fileNameBundleMap[ subjectNameLocation : len( fileNameBundleMap ) ]

    command = 'GkgExecuteCommand DwiBundleMeasure' + \
              ' -b ' + fileNameInputBundleMap + \
              ' -o ' + os.path.join( outputWorkDirectory,
                         os.path.splitext( \
                         os.path.split( fileNameInputBundleMap )[ 1 ] )[ 0 ] + \
                         '_from_volume_statistics_along_average_fiber' ) + \
              ' -m from_volume_statistics_along_average_fiber' + \
              ' -stringParameters ' + \
              parameters[ 'Process' + str( index ) + \
                          '_bundleVolumeStatAlongAverageFiber_fileNameVolume' \
                        ].getValue() + \
              ' -scalarParameters ' + \
              str( parameters[ 'Process' + str( index ) + \
                 '_bundleVolumeStatAlongAverageFiber_backgroundIntensityLevel' \
                        ].getValue() )+ \
              ' ' + str( parameters[ 'Process' + str( index ) + \
                             '_bundleVolumeStatAlongAverageFiber_rankOfVolume' \
                              ].getValue() )

    executeCommand( self, subjectName, command, viewOnly )


  ##############################################################################
  # display()
  ##############################################################################

  def display( self, functors, parameters, subjectName, verbose,
               isReportModeActive ):

    ########################### resetting the viewer ###########################
    functors[ 'viewer-reset' ]()

    view = 'first_view'
    functors[ 'viewer-create-referential' ]( 'frameDW' )

    ########################### collecting file names ##########################
    outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()

    fileNameBundleMaps = parameters[ 'fileNameBundleMaps' ].getValue().split()

    processes = parameters[ 'processes' ].getValue()

    ########################### fill subject listwidgets #######################

    subjectList = functors[ 'get-subject-list' ]()

    ########################### fill measure listwidgets #######################

    measures = list()
    for process in processes:

       measures.append( process[ 'processName' ] )

    ############################ fill bundle listwidgets #######################

    bundleList = list()

    for f in fileNameBundleMaps:

      fileNameBundleMap = f.strip()
      bundleMapInfo = dict()
      execfile( fileNameBundleMap, bundleMapInfo )
      bundleNames = bundleMapInfo[ 'attributes' ][ 'bundles' ]

      bundleMapName = os.path.splitext( \
                           os.path.split( fileNameBundleMap )[ 1 ] )[ 0 ]
      if( bundleMapName.count( subjectName ) != 0 ):

        subjectNameLocation = bundleMapName.find( subjectName )
        bundleMapName = bundleMapName[ 0 : subjectNameLocation ] + \
                  bundleMapName[ ( subjectNameLocation + len( subjectName ) ): \
                                                          len( bundleMapName ) ]

      for n in xrange( len( bundleNames ) / 2 ):

        bundleList.append(  bundleMapName + \
                           ' - ' + bundleNames[ n * 2 ] )

      functors[ 'viewer-load-object' ]( fileNameBundleMap,
                                        bundleMapName )
      functors[ 'viewer-set-diffuse' ]( bundleMapName,
                                        [ 0.9, 0.9, 0.9, 0.1 ] )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW',
                                                         bundleMapName )
      functors[ 'viewer-add-object-to-window' ](
                                              bundleMapName,
                                              view,
                                              'T1 + RGB + original tractogram' )

    ###################### waiting for result #################################
    functors[ 'condition-wait-and-release' ]( subjectName,
                                              'measures-processed' )

    functors[ 'viewer-listWidget-insert-items' ]( view,
                                                  'Subjects', subjectList )
    functors[ 'viewer-listWidget-set-current-row' ]( view,
                                                     'Subjects', 0 )
    functors[ 'viewer-listWidget-insert-items' ]( view,
                                                  'Measures', measures )
    functors[ 'viewer-listWidget-set-current-row' ]( view,
                                                     'Measures', 0 )
    functors[ 'viewer-listWidget-insert-items' ]( view,
                                                  'Bundles', bundleList )

    functors[ 'viewer-connect-listWidget-to-callBack' ]( view,
                                          'Subjects',
                                          'itemSelectionChanged()',
                                           self.updateBundleStatisticsWidget )
    functors[ 'viewer-connect-listWidget-to-callBack' ]( view,
                                          'Measures',
                                          'itemSelectionChanged()',
                                           self.updateBundleStatisticsWidget )
    functors[ 'viewer-connect-listWidget-to-callBack' ]( view,
                                          'Bundles',
                                          'itemSelectionChanged()',
                                           self.updateBundleStatisticsWidget )
    functors[ 'viewer-listWidget-set-current-row' ]( view,
                                                     'Bundles', 0 )


  def updateBundleStatisticsWidget( self ):

    if ( self._application.getBatchMode() == False ):

      view = 'first_view'

      subjectSelected = self._application.getViewer().getListWidgetValue( \
                                                              view, 'Subjects' )

      measureSelected = self._application.getViewer().getListWidgetValue( 
                                                              view, 'Measures' )

      bundleSelected = self._application.getViewer().getListWidgetValue( 
                                                               view, 'Bundles' )

      if( len( measureSelected ) ):

        self._application.getViewer().updateBundleStatisticsWidget( \
                                                           view,
                                                           'results',
                                                           subjectSelected,
                                                           measureSelected[ 0 ],
                                                           bundleSelected )

      colors = self._application.getViewer().getBundleStatisticsWidgetColors( \
                                                           view,
                                                           'results' )
      listWidget = self._application.getViewer().getWidget( view, 'Bundles' )

      for itemIndex in xrange( listWidget.count() ):

        item = listWidget.item( itemIndex )
        bundleMapObjectName = str( item.text() ).split( '-' )[ 0 ].strip()
        bundleMapObject = self._application.getViewer().getObject( \
                                                           bundleMapObjectName )
      
        if( item.isSelected() ):

          bundleMapChildName = str( item.text() ).split( '-' )[ 1 ].strip()
          for child in bundleMapObject.children:

            if( child.name == bundleMapChildName ):

              bundleIndex = bundleSelected.index( str( item.text() ) )
              child.setMaterial( diffuse = [ colors[ bundleIndex ][ 0 ],
                                             colors[ bundleIndex ][ 1 ],
                                             colors[ bundleIndex ][ 2 ],
                                             0.1 ] )

        else:

          for child in bundleMapObject.children:

            child.setMaterial( diffuse = [ 0.9, 0.9, 0.9, 0.1 ] )


  def viewerListWidgetInsertItems( self, viewName, widgetName, items ):

    if ( self._application.getBatchMode() == False ):

      self._application.getMainThreadActionManager().call(    
         self._application.getViewer().insertItems,
         viewName, widgetName, items )    


  def viewerListWidgetsetCurrentRow( self, viewName, widgetName, value ):

    if ( self._application.getBatchMode() == False ):

      self._application.getMainThreadActionManager().call(    
         self._application.getViewer().setCurrentRow,
         viewName, widgetName, value )    


  def viewerConnectListWidgetToCallBack( self,
                                         viewName,
                                         widgetName,
                                         signalName,
                                         callBack ):

    if ( self._application.getBatchMode() == False ):

      self._application.getMainThreadActionManager().call(    
         self._application.getViewer().connectListWidgetToCallBack,
         viewName, widgetName, signalName, callBack )


  def viewerAddMultiSubjectOutputWorkDirectory( self,
                                                viewName,
                                                widgetName,
                                                subjectName,
                                                outputWorkDirectory ):

    if ( self._application.getBatchMode() == False ):

      self._application.getMainThreadActionManager().call(    
         self._application.getViewer().addMultiSubjectOutputWorkDirectory,
         viewName, widgetName, subjectName, outputWorkDirectory )



