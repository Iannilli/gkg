from Viewer import *
from MultipleTabWindow import *

import matplotlib
matplotlib.use( 'Qt5Agg' )
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
from matplotlib.figure import Figure
from matplotlib.lines import Line2D
from matplotlib.widgets import Slider
import matplotlib.pyplot as plt
import numpy as np


class DwiBundleStatisticsViewer( Viewer ):

  def __init__( self, minimumSize, parent ):

    Viewer.__init__( self, minimumSize, parent )
    
    ########################## First View ######################################
    self.createView( 'first_view' )
    
    self.add3DWindow( 'first_view', 0, 0, 1, 1,
                      'T1 + RGB + original tractogram',
                      'T1 + RGB + original tractogram' )

    self.addListWidget( 'first_view', 0, 1, 1, 1,
                        'Subjects',
                        'Subjects',
                        QtGui.QAbstractItemView.ExtendedSelection )

    self.addListWidget( 'first_view', 1, 1, 1, 1,
                        'Measures',
                        'Measures' )

    self.addListWidget( 'first_view', 2, 1, 1, 1,
                        'Bundles',
                        'Bundles',
                        QtGui.QAbstractItemView.ExtendedSelection )
    self.getWindow( 'first_view',
                     'T1 + RGB + original tractogram' ).setMaximumHeight( 200 )
    self.getWidget( 'first_view', 'Subjects' ).setMaximumWidth( 200 )
    self.getWidget( 'first_view', 'Subjects' ).setMaximumHeight( 200 )
    self.getWidget( 'first_view', 'Measures' ).setMaximumWidth( 200 )
    self.getWidget( 'first_view', 'Bundles' ).setMaximumWidth( 200 )

    bundleStatisticsWidgetDictionnary = \
      { 'Bundle - fiber count statistics' : 'bundle value',
        'Bundle - length statistics' : 'bundle value',
        'Bundle - tortuosity statistics' : 'bundle value',
        'Bundle - volume profile along average fiber' : 'bundle profile',
        'Bundle - volume statistics along average fiber' : 'bundle statistics' }

    #create random color vector
    palette = self.getAnatomist().palettes().find( 'random' )
    randomColors = list()
    colorIndex = 2
    for i in xrange( 256 ):

      red = float( palette.value( colorIndex ).red() ) / 255
      green = float( palette.value( colorIndex ).green() ) / 255
      blue = float( palette.value( colorIndex ).blue() ) / 255
      if( len( randomColors ) ):

        previousColor = randomColors[ len( randomColors ) - 1 ]
        if ( previousColor != ( red, green, blue ) and \
             ( red, green, blue ) != ( 0.0, 0.0, 0.0 ) ):

          randomColors.append( ( red, green, blue ) )

      else:

        if ( ( red, green, blue ) != ( 0.0, 0.0, 0.0 ) ):

          randomColors.append( ( red, green, blue ) )

      colorIndex += 1

    self.addBundleStatisticsWidget( 'first_view', 1, 0, 2, 1,
                                    'results',
                                     None,
                                     bundleStatisticsWidgetDictionnary,
                                     randomColors )

    self._connections = list()


  def getBundleStatisticsWidgetColors( self, viewName, widgetName ):

    return self.getWidget( viewName, widgetName )._colors


  def addBundleStatisticsWidget( self, viewName,
                                row, column, rowSpan, columnSpan,
                                name, label,
                                widgetOptionsDictionnary,
                                colors,
                                layout = None ):

    if ( self._isActivated == False ):

      return

    widgetLayout = QtGui.QVBoxLayout()
    if ( layout == None ):

      layout = self.getLayoutFromName( viewName )

    layout.addLayout( widgetLayout, row, column, rowSpan, columnSpan )

    bundleStatisticsWidget = BundleStatisticsWidget( widgetOptionsDictionnary,
                                                     colors )
    widgetLayout.addWidget( bundleStatisticsWidget )
    horizontalLayout = QtGui.QHBoxLayout()

    if ( label is not None ):

      labelWidget = QtGui.QLabel( label )
      labelWidget.setAlignment( QtCore.Qt.AlignHCenter |
                                QtCore.Qt.AlignBottom )
      labelWidget.setFont( QtGui.QFont( 'Arial', 8, 1, True ) )

      horizontalLayout.addWidget( labelWidget )

    widgetLayout.addLayout( horizontalLayout )

    qSizePolicy = QtGui.QSizePolicy( QtGui.QSizePolicy.MinimumExpanding,
                                     QtGui.QSizePolicy.MinimumExpanding )
    qSizePolicy.setHeightForWidth( True )

    self._widgets[ viewName ][ name ] = bundleStatisticsWidget
    self._widgetLayouts[ viewName ][ name ] = widgetLayout


  def setCurrentRow( self, viewName, widgetName, value ) :

    listWidget = self.getWidget( viewName, widgetName ).setCurrentRow( value )


  def updateBundleStatisticsWidget( self,
                                    viewName,
                                    widgetName,
                                    subjectSelected,
                                    measureSelected,
                                    bundleSelected ):

    self.getWidget( viewName, widgetName ).update( subjectSelected,
                                                   measureSelected,
                                                   bundleSelected )


  def getListWidgetValue( self, viewName, widgetName ):

    items = self.getWidget( viewName, widgetName ).selectedItems()
    itemValues = list()

    for item in items:

      itemValues.append( str( item.text() ) )

    return itemValues


  def connectListWidgetToCallBack( self,
                                   viewName,
                                   widgetName,
                                   signalName,
                                   callBack ):

    connectionName = \
        viewName + '_' + widgetName + '_' + signalName + '_' + callBack.__name__
    if ( self._connections.count( connectionName ) == 0 ):

      self._connections.append( connectionName ) 
      view = self.getViewFromName( viewName )
      view.connect( self.getWidget( viewName, widgetName ),
                    QtCore.SIGNAL( signalName ),
                    callBack )


  def addMultiSubjectOutputWorkDirectory( self,
                                          viewName,
                                          widgetName,
                                          subjectName,
                                          outputWorkDirectory ):

    self.getWidget( viewName, widgetName ).addOutputWorkDirectory( subjectName,
                                                           outputWorkDirectory )


  def getMultiSubjectOutputWorkDirectories( self,
                                            viewName,
                                            widgetName ):

    self.getWidget( viewName, widgetName ).getOutputWorkDirectories()



def createDwiBundleStatisticsViewer( minimumSize, parent ):

  return DwiBundleStatisticsViewer( minimumSize, parent )


class BundleStatisticsWidget( QtGui.QWidget ):

  def __init__( self, dictionnary, colors, parent = None ):

    QtGui.QWidget.__init__( self, parent )
    self._dictionnary = dictionnary
    self._layout = QtGui.QGridLayout()
    self.setLayout( self._layout )
    self._widgets = list()
    self._outputWorkDirectory = dict()
    self._symbols = [ 'o', 'v', '^', '>', '<',
                      '1', '2', '3', '4', 's', 'p', '*', 'h', 'H', '+', 'x',
                      'D', 'd', '|', '_' ]
    self._colors = colors
    self._measuresFileName = { \
        'Bundle - fiber count statistics' : 'bundle_fiber_count',
        'Bundle - length statistics' : 'bundle_fiber_length_statistics',
        'Bundle - tortuosity statistics' : 'bundle_fiber_tortuosity_statistics',
        'Bundle - volume profile along average fiber' : \
                                       'from_volume_profile_along_centroid',
        'Bundle - volume statistics along average fiber' : \
                                       'from_volume_statistics_along_centroid' }

  def clear( self ):

    #clear les widgets d'avant
    for widget in self._widgets:

      self._layout.removeWidget( widget )
      widget.deleteLater()

    self._widgets = list()


  def update( self, subjectSelected, measureSelected, bundleSelected ):


    self.clear()
    if( len( subjectSelected ) and \
        len( measureSelected ) and \
        len( bundleSelected ) ):

      selectedBundleMaps = set()
      bundlesDictionnary = dict()
      for bundle in bundleSelected:

        bundleMap = bundle.split( ' - ' )[ 0 ]
        bundle = bundle.split( ' - ' )[ 1 ]
        selectedBundleMaps.add( bundleMap )

        if( bundlesDictionnary.has_key( bundleMap ) ):

          bundlesDictionnary[ bundleMap ] += [ bundle ]

        else:

          bundlesDictionnary[ bundleMap ] = [ bundle ]


      ########################## bundle profile ################################
      if( self._dictionnary[ measureSelected ] == 'bundle profile' ):

        figureProfile = Figure( figsize = ( 1.2, 0.6 ),
                                    dpi = 60,
                                    facecolor = 'w',
                                    frameon = True )
        axesProfile = figureProfile.add_subplot( 111, alpha = 0.2 )
        canvasProfile = FigureCanvasQTAgg( figureProfile )
        self._layout.addWidget( canvasProfile, 0, 0, 1, 1 )
        canvasProfile.setSizePolicy( QtGui.QSizePolicy.MinimumExpanding,
                              QtGui.QSizePolicy.MinimumExpanding )

        figureBoxPlots = Figure( figsize = ( 1.2, 0.6 ),
                                    dpi = 60,
                                    facecolor = 'w',
                                    frameon = True )
        axesBoxPlots = figureBoxPlots.add_subplot( 111, alpha = 0.2 )
        canvasBoxPlots = FigureCanvasQTAgg( figureBoxPlots )
        self._layout.addWidget( canvasBoxPlots, 1, 0, 1, 1 )
        canvasBoxPlots.setSizePolicy( QtGui.QSizePolicy.MinimumExpanding,
                                      QtGui.QSizePolicy.MinimumExpanding )

        boxPlotData = list()
        subjectIndex = 0
        legendPlot = list()
        bundleIndexVector = list()
        for subjectName in subjectSelected:

          legendPlot += [ plt.plot( [ 0 ],
                                    [ 1 ],
                                    marker = self._symbols[ subjectIndex ],
                                    color = 'k',
                                    markerfacecolor = 'w',
                                    markeredgecolor = 'k',
                                    linestyle = '-' ) ]
          for selectedBundleMap in selectedBundleMaps:

            fileName = os.path.join( self._outputWorkDirectory[ subjectName ],
                                   selectedBundleMap + '_' + \
                                   self._measuresFileName[ measureSelected ] + \
                                   '.bundlemeasure_spreadsheet' )

            f = open( fileName, 'r' )
            lines = f.readlines()
            f.close()
            bundles = bundlesDictionnary[ selectedBundleMap ]
            for line in lines:

              lineSplitted = line.split()
              if ( bundles.count( lineSplitted[ 0 ] ) != 0 ):

                pointIndex = 0
                profileY = list()
                profileX = list()
                for i in range( 4, len( lineSplitted ) ):

                  profileX.append( pointIndex )
                  profileY.append( float( lineSplitted[ i ] ) )
                  pointIndex += 1

                boxPlotData.append( list( profileY ) )
                bundleIndex = bundleSelected.index( selectedBundleMap + \
                                                    ' - ' + \
                                                    lineSplitted[ 0 ] )
                bundleIndexVector.append( bundleIndex )
                axesProfile.plot( profileX,
                                  profileY,
                                  marker = self._symbols[ subjectIndex ],
                                  color = self._colors[ bundleIndex ],
                                  linestyle = '-',
                                  picker = 5 )

              else:

                message = 'Bundle ' + lineSplitted[ 0 ] + ' not processed.'
                raise RuntimeError, message
               
          subjectIndex += 1

        figureProfile.legend( legendPlot,
                              subjectSelected )
        bp = axesBoxPlots.boxplot( boxPlotData, sym = '' )

        numBoxes = len( bundleSelected ) * len( subjectSelected )
        medians = range( numBoxes )
        subjectIndex = -1
        #create 2 points to separate boxplots from edges
        axesBoxPlots.plot( [ 0.5, numBoxes + 0.5 ],
                           [ boxPlotData[ 0 ][ 0 ], boxPlotData[ 0 ][ 0 ] ],
                           'w' )
        #add mean and median lines and legends
        for i in range( numBoxes ):

          if( i % len( bundleSelected ) == 0 ):

            subjectIndex += 1

          med = bp[ 'medians' ][ i ]
          medianX = []
          averageVector = []
          average = np.average( boxPlotData[ i ] )
          for j in range( 2 ):

            medianX.append( med.get_xdata()[ j ] )
            averageVector.append( average )
            meanPlot = axesBoxPlots.plot( medianX, averageVector, 'k' )

          bundleIndex = bundleIndexVector[ i % len( bundleSelected ) ]
          axesBoxPlots.plot( \
                  [ np.average( med.get_xdata() ) ] * len( boxPlotData[ i ] ),
                    boxPlotData[ i ],
                    color = self._colors[ bundleIndex ],
                    marker = self._symbols[ subjectIndex ],
                    markeredgecolor = 'k' )

        figureBoxPlots.legend( [ plt.plot( [ 0, 1 ],[ 1, 1 ], '-r' ),
                               meanPlot ],
                               [ 'median', 'mean' ] )

        if( len( bundleSelected ) <= 8 ):

          subjectNames = np.repeat( subjectSelected, len( bundleSelected ) )

          bundleNamesForLegend = list()
          for bundleIndex, subjectName in zip( bundleIndexVector, subjectNames ):

            bundleNamesForLegend.append( \
                          subjectName + '\n' + \
                          bundleSelected[ bundleIndex ].split( ' - ' )[ 0 ] + \
                          '\n' + \
                          bundleSelected[ bundleIndex ].split( ' - ' )[ 1 ] )

          plt.setp( axesBoxPlots,
                    xticklabels = bundleNamesForLegend )
          figureBoxPlots.autofmt_xdate()

        else:

          plt.setp( axesBoxPlots,
                    xticklabels = [''] * len( bundleSelected ) )

        self._widgets.append( canvasProfile )
        self._layout.addWidget( canvasProfile )
        self._widgets.append( canvasBoxPlots )
        self._layout.addWidget( canvasBoxPlots )


      ######################## bundle statistics ###############################
      elif( self._dictionnary[ measureSelected ] == 'bundle statistics' ):

        figure = Figure( figsize = ( 1.2, 0.6 ),
                                    dpi = 60,
                                    facecolor = 'w',
                                    frameon = True )
        axes = figure.add_subplot( 111, alpha = 0.2 )
        canvas = FigureCanvasQTAgg( figure )
        self._layout.addWidget( canvas, 0, 0, 1, 1 )
        canvas.setSizePolicy( QtGui.QSizePolicy.MinimumExpanding,
                              QtGui.QSizePolicy.MinimumExpanding )

        subjectIndex = 0
        legendPlot = list()
        bundleIndexVector = list()
        for subjectName in subjectSelected:

          minValues = list()
          maxValues = list()
          meanValues = list()
          standardDeviationValues = list()
          xValue = xrange( len( bundleSelected ) )

          legendPlot += [ plt.plot( [ 0 ],
                                    [ 1 ],
                                    marker = self._symbols[ subjectIndex ],
                                    color = 'k',
                                    markerfacecolor = 'w',
                                    markeredgecolor = 'k',
                                    linestyle = '-' ) ]

          for selectedBundleMap in selectedBundleMaps:

            fileName = os.path.join( self._outputWorkDirectory[ subjectName ],
                                    selectedBundleMap + '_' + \
                                    self._measuresFileName[ measureSelected ] + \
                                    '.bundlemeasure_spreadsheet' )

            f = open( fileName, 'r' )
            lines = f.readlines()
            f.close()
            bundles = bundlesDictionnary[ selectedBundleMap ]
            for line in lines:

              lineSplitted = line.split()
              if ( bundles.count( lineSplitted[ 0 ] ) != 0 ):

                bundleIndex = bundleSelected.index( selectedBundleMap + \
                                                    ' - ' + \
                                                    lineSplitted[ 0 ] )
                bundleIndexVector.append( bundleIndex )
                #plot bundle points
                axes.plot( [ len( minValues ) ],
                    [ float( lineSplitted[ 4 ] ) ],
                    self._symbols[ subjectIndex ],
                    color = self._colors[ bundleIndex ],
                    picker = 5 )
                axes.plot( [ len( maxValues ) ],
                    [ float( lineSplitted[ 5 ] ) ],
                    self._symbols[ subjectIndex ],
                    color = self._colors[ bundleIndex ],
                    picker = 5 )

                axes.errorbar( [ len( meanValues ) ],
                        [ float( lineSplitted[ 6 ] ) ],
                        yerr = [ float( lineSplitted[ 7 ] ) ],
                        color = self._colors[ bundleIndex ],
                        fmt = self._symbols[ subjectIndex ] )

                minValues.append( float( lineSplitted[ 4 ] ) )
                maxValues.append( float( lineSplitted[ 5 ] ) )
                meanValues.append( float( lineSplitted[ 6 ] ) )
                standardDeviationValues.append( float( lineSplitted[ 7 ] ) )

              else:

                message = 'Bundle ' + lineSplitted[ 0 ] + ' not processed.'
                raise RuntimeError, message


          #subject lines
          axes.plot( xValue,
                    minValues,
                    '--',
                    color = 'grey',
                    picker = 5 )
          axes.plot( xValue,
                    maxValues,
                    '--',
                    color = 'grey',
                    picker = 5 )

          axes.plot( xValue,
                        meanValues,
                        linestyle = '-',
                        color = 'black' )

          subjectIndex += 1

        axes.plot( [ -0.1, len( bundleSelected ) - 0.9 ],
                   [ minValues[ 0 ], minValues[ 0 ] ],
                   'w', 
                   linestyle = 'None' )
        figure.legend( legendPlot, subjectSelected )
        bundleNamesForLegend = list()
        for bundleIndex in bundleIndexVector:

          bundleNamesForLegend.append( \
                          bundleSelected[ bundleIndex ].split( ' - ' )[ 0 ] + \
                          '\n' + \
                          bundleSelected[ bundleIndex ].split( ' - ' )[ 1 ] )

        plt.setp( axes,
                  xticks = xValue,
                  xticklabels = bundleNamesForLegend )
        figure.autofmt_xdate()
        self._widgets.append( canvas )
        self._layout.addWidget( canvas )

      ######################## fiber profile ##################################
      elif( self._dictionnary[ measureSelected ] == 'bundle value' ):

        figure = Figure( figsize = ( 1.2, 0.6 ),
                                    dpi = 60,
                                    facecolor = 'w',
                                    frameon = True )
        axes = figure.add_subplot( 111, alpha = 0.2 )
        canvas = FigureCanvasQTAgg( figure )
        self._layout.addWidget( canvas, 0, 0, 1, 1 )
        canvas.setSizePolicy( QtGui.QSizePolicy.MinimumExpanding,
                              QtGui.QSizePolicy.MinimumExpanding )

        subjectIndex = 0
        legendPlot = list()
        bundleIndexVector = list()
        for subjectName in subjectSelected:

          values = list()
          xValue = xrange( len( bundleSelected ) )

          legendPlot += [ plt.plot( [ 0 ],
                                    [ 1 ],
                                    marker = self._symbols[ subjectIndex ],
                                    color = 'k',
                                    markerfacecolor = 'w',
                                    markeredgecolor = 'k',
                                    linestyle = '-' ) ]

          for selectedBundleMap in selectedBundleMaps:

            fileName = os.path.join( self._outputWorkDirectory[ subjectName ],
                                    selectedBundleMap + '_' + \
                                    self._measuresFileName[ measureSelected ] + \
                                    '.bundlemeasure_spreadsheet' )

            f = open( fileName, 'r' )
            lines = f.readlines()
            f.close()
            bundles = bundlesDictionnary[ selectedBundleMap ]
            for line in lines:

              lineSplitted = line.split()
              if ( bundles.count( lineSplitted[ 0 ] ) != 0 ):

                bundleIndex = bundleSelected.index( selectedBundleMap + \
                                                    ' - ' + \
                                                    lineSplitted[ 0 ] )
                bundleIndexVector.append( bundleIndex )
                #plot bundle points
                axes.plot( [ len( values ) ],
                    [ float( lineSplitted[ 4 ] ) ],
                    self._symbols[ subjectIndex ],
                    color = self._colors[ bundleIndex ],
                    picker = 5 )

                values.append( float( lineSplitted[ 4 ] ) )
 
              else:

                message = 'Bundle ' + lineSplitted[ 0 ] + ' not processed.'
                raise RuntimeError, message


          #subject lines
          axes.plot( xValue,
                    values,
                    '-',
                    color = 'grey',
                    picker = 5 )


          subjectIndex += 1

        axes.plot( [ -0.1, len( bundleSelected ) - 0.9 ],
                   [ values[ 0 ], values[ 0 ] ],
                   'w', 
                   linestyle = 'None' )
        figure.legend( legendPlot, subjectSelected )
        bundleNamesForLegend = list()
        for bundleIndex in bundleIndexVector:

          bundleNamesForLegend.append( \
                           bundleSelected[ bundleIndex ].split( ' - ' )[ 0 ] + \
                           '\n' + \
                           bundleSelected[ bundleIndex ].split( ' - ' )[ 1 ] )

        plt.setp( axes,
                  xticks = xValue,
                  xticklabels = bundleNamesForLegend )
        figure.autofmt_xdate()
        self._widgets.append( canvas )
        self._layout.addWidget( canvas )

      else:

        print 'No such widget type defined.'


  def addOutputWorkDirectory( self, subjectName, outputWorkDirectory ):

    self._outputWorkDirectory[ subjectName ] = outputWorkDirectory


  def getOutputWorkDirectory( self ):

    return self._outputWorkDirectory



