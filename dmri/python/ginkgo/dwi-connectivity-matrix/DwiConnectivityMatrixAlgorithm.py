from Algorithm import *
from DwiConnectivityMatrixTask import *


class DwiConnectivityMatrixAlgorithm( Algorithm ):

  def __init__( self, verbose ):

    Algorithm.__init__( self, 'DWI-Connectivity-Matrix', verbose, True )

    ###########################################################################
    # Input data
    ###########################################################################

    self.addParameter( StringParameter( 'fileNameInputBundleMap', '' ) )
    self.addParameter( StringParameter( 'fileNameT1', '' ) )
    self.addParameter( StringParameter( 'fileNameDwToT1Transform', '' ) )
    
    ###########################################################################
    # Connectivity matrix type
    ###########################################################################

    self.addParameter( ChoiceParameter(
                       'connectivityMatrixType',
                       3,
                       ( 'mesh to mesh',
                         'mesh to mesh-and-texture',
                         'mesh-and-texture to mesh-and-texture',
                         'mesh-and-texture alone',
                         'volume to mesh',
                         'volume to mesh-and-texture',
                         'volume alone',
                         'volume to volume' ) ) )
    
    ############################################################################
    # ROI Sets and parameters
    ############################################################################
    
    # Mesh to Mesh -> Not implemented yet
    # Mesh to mesh-and-texture -> Not implemented yet
    # Mesh-and-texture to mesh-and-texture -> Not implemented yet
    # Mesh-and-texture alone
    self.addParameter( StringParameter( 'fileNameMeshAndTextureAlone_ROIMesh', 
                                        '' ) ) 
    self.addParameter( StringParameter( 
                                       'fileNameMeshAndTextureAlone_ROITexture',
                                        '' ) )
    self.addParameter( StringParameter( 
                          'fileNameMeshAndTextureAlone_ROIToBundleMapTransform',
                          '' ) )
    
    # Volume to mesh
    self.addParameter( StringParameter( 'fileNameVolumeToMesh_ROIVolume', 
                                        '' ) ) 
    self.addParameter( StringParameter( 
                                 'fileNameVolumeToMesh_ROIToBundleMapTransform',
                                 '' ) )
    self.addParameter( StringParameter( 'fileNameVolumeToMesh_ROIMesh', 
                                        '' ) )
    self.addParameter( StringParameter( 
                             'fileNameVolumeToMesh_ROIMeshToBundleMapTransform',
                             '' ) )
    
    
    # Volume to mesh-and-texture
    self.addParameter( StringParameter( 
                                     'fileNameVolumeToMeshAndTexture_ROIVolume', 
                                     '' ) ) 
    
    self.addParameter( StringParameter( 
                       'fileNameVolumeToMeshAndTexture_ROIToBundleMapTransform',
                       '' ) )
    
    self.addParameter( StringParameter( 
                                       'fileNameVolumeToMeshAndTexture_ROIMesh', 
                                       '' ) )
    self.addParameter( StringParameter( 
                                    'fileNameVolumeToMeshAndTexture_ROITexture', 
                                    '' ) )
    
    self.addParameter( StringParameter( 
            'fileNameVolumeToMeshAndTexture_ROIMeshAndTextToBundleMapTransform',
            '' ) )
    
    # Volume alone
    self.addParameter( StringParameter( 'fileNameVolumeAlone_ROIVolume', '' ) ) 
    self.addParameter( StringParameter( 
                                  'fileNameVolumeAlone_ROIToBundleMapTransform', 
                                  '' ) )
    self.addParameter( StringParameter( 'fileNameVolumeAlone_FurtherInterface', 
                                        '' ) ) 
    self.addParameter( StringParameter( 
                               'fileNameVolumeAlone_InterfaceToBundleTransform', 
                               '' ) ) 
    
    # Volume to volume -> Not implemented yet
    
    ############################################################################
    # Connectivity matrix parameters
    ############################################################################
    
    # Mesh to Mesh -> Not implemented yet
    # Mesh to mesh-and-texture -> Not implemented yet
    # Mesh-and-texture to mesh-and-texture -> Not implemented yet
    # Mesh-and-texture alone
    
    self.addParameter( DoubleParameter( 
                                  'meshAndTextureAloneInputFiberResamplingStep',
                                  0.1, 0.01, 10, 0.01 ) )
    self.addParameter( DoubleParameter( 
                                     'meshAndTextureAloneMaximumDistanceToMesh',
                                     1.0, 0.05, 10, 0.05 ) )
    self.addParameter( DoubleParameter( 
                                 'meshAndTextureAloneOutputFiberResamplingStep',
                                  4.0, 0.05, 10, 0.05 ) )
    self.addParameter( BooleanParameter( 
                                        'meshAndTextureAloneFiberTangent', 0 ) )
    self.addParameter( IntegerParameter( \
                                 'meshAndTextureAloneMaximumFiberToNormalAngle',
                                 30, 0, 360, 1  ) )
    self.addParameter( BooleanParameter( 
                               'meshAndTextureAloneSmoothConnectivityMap', 0 ) )
    self.addParameter( DoubleParameter( 
                              'meshAndTextureAloneStandardDeviationOfSmoothing',
                              1.0, 0.01, 10, 0.01 ) )
    self.addParameter( IntegerParameter( 'meshAndTextureAloneCacheXSize',
                                         100, 1, 1000, 1  ) )
    self.addParameter( IntegerParameter( 'meshAndTextureAloneCacheYSize',
                                         100, 1, 1000, 1  ) )
    self.addParameter( IntegerParameter( 'meshAndTextureAloneCacheZSize',
                                         100, 1, 1000, 1  ) )
    
    
    # Volume to mesh
    self.addParameter( DoubleParameter( 
                                  'volumeToMeshMinimumIntersectionLength',
                                  1.0, 0.0, 300.0, 0.1 ) )
    self.addParameter( DoubleParameter( 'volumeToMeshInputFiberResamplingStep',
                                        0.1, 0.01, 10, 0.01 ) )
    self.addParameter( DoubleParameter( 
                                     'volumeToMeshMaximumDistanceToMesh',
                                     1.0, 0.05, 10, 0.05 ) )
    self.addParameter( DoubleParameter( 
                                 'volumeToMeshOutputFiberResamplingStep',
                                  4.0, 0.05, 10, 0.05 ) )
    self.addParameter( BooleanParameter( 
                                        'volumeToMeshFiberTangent', 0 ) )
    self.addParameter( IntegerParameter( \
                                       'volumeToMeshMaximumFiberToNormalAngle',
                                        30, 0, 360, 1  ) )
    self.addParameter( BooleanParameter( 
                               'volumeToMeshSmoothConnectivityMap', 0 ) )
    self.addParameter( DoubleParameter( 
                              'volumeToMeshStandardDeviationOfSmoothing',
                              1.0, 0.01, 10, 0.01 ) )
    self.addParameter( IntegerParameter( 'volumeToMeshCacheXSize',
                                         100, 1, 1000, 1  ) )
    self.addParameter( IntegerParameter( 'volumeToMeshCacheYSize',
                                         100, 1, 1000, 1  ) )
    self.addParameter( IntegerParameter( 'volumeToMeshCacheZSize',
                                         100, 1, 1000, 1  ) )
    
    
    # Volume to mesh-and-texture
    self.addParameter( DoubleParameter( 
                        'volumeToMeshAndTextureMinimumIntersectionLength',
                        1.0, 0.0, 300.0, 0.1 ) )
    self.addParameter( DoubleParameter( 
                               'volumeToMeshAndTextureInputFiberResamplingStep',
                               0.1, 0.01, 10, 0.01 ) )
    self.addParameter( DoubleParameter( 
                                  'volumeToMeshAndTextureMaximumDistanceToMesh',
                                  1.0, 0.05, 10, 0.05 ) )
    self.addParameter( DoubleParameter( 
                              'volumeToMeshAndTextureOutputFiberResamplingStep',
                              4.0, 0.05, 10, 0.05 ) )
    self.addParameter( BooleanParameter( 'volumeToMeshAndTextureFiberTangent',
                                          0 ) )
    self.addParameter( IntegerParameter( \
                              'volumeToMeshAndTextureMaximumFiberToNormalAngle',
                              30, 0, 360, 1  ) )
    self.addParameter( BooleanParameter( 
                                  'volumeToMeshAndTextureSmoothConnectivityMap',
                                  0 ) )
    self.addParameter( DoubleParameter( 
                           'volumeToMeshAndTextureStandardDeviationOfSmoothing',
                           1.0, 0.01, 10, 0.01 ) )
    self.addParameter( IntegerParameter( 'volumeToMeshAndTextureCacheXSize',
                                         100, 1, 1000, 1  ) )
    self.addParameter( IntegerParameter( 'volumeToMeshAndTextureCacheYSize',
                                         100, 1, 1000, 1  ) )
    self.addParameter( IntegerParameter( 'volumeToMeshAndTextureCacheZSize',
                                         100, 1, 1000, 1  ) )
    self.addParameter( IntegerParameter( 
                                   'volumeToMeshAndTextureOffsetForOutputLabel',
                                   0, 0, 100, 1  ) )
    # Volume alone
    self.addParameter( DoubleParameter( 
                                   'volumeAloneMinimumIntersectionLength',
                                   1.0, 0.0, 300.0, 0.1 ) )
    self.addParameter( DoubleParameter( 'volumeAloneInputFiberResamplingStep',
                                        0.1, 0.01, 10, 0.01 ) )
    self.addParameter( DoubleParameter( 'volumeAloneOutputFiberResamplingStep',
                                        4.0, 0.05, 10, 0.05 ) )
    self.addParameter( BooleanParameter( 'volumeAloneFurtherInterface', 0 ) )
    self.addParameter( IntegerParameter( 'volumeAloneOffsetForOutputLabel',
                                         0, 0, 100, 1  ) )
    self.addParameter( DoubleParameter( 'volumeAloneMaximumDistanceToMesh',
                                        1.0, 0.05, 10, 0.05 ) )
    self.addParameter( IntegerParameter( 'volumeAloneCacheXSize',
                                         100, 1, 1000, 1  ) )
    self.addParameter( IntegerParameter( 'volumeAloneCacheYSize',
                                         100, 1, 1000, 1  ) )
    self.addParameter( IntegerParameter( 'volumeAloneCacheZSize',
                                         100, 1, 1000, 1  ) )
    self.addParameter( StringParameter( 'fileNameVolumeAloneFurtherInterface', 
                                        '' ) )
    self.addParameter( StringParameter( 
                                'fileNameVolumeAloneInterfaceToBundleTransform', 
                                '' ) )
    
    # Volume to volume -> Not implemented yet

    ###########################################################################
    # Output
    ###########################################################################

    self.addParameter( ChoiceParameter( 'outputBundleMapFormat',
                                        0,
                                        ( 'aimsbundlemap',
                                          'bundlemap',
                                          'vtkbundlemap',
                                          'trkbundlemap' ) ) )
    
    self.addParameter( ChoiceParameter( 'outputTextureMapFormat',
                                        0,
                                        ( 'aimstexture',
                                          'texturemap' ) ) )    

  def launch( self ):

    if ( self._verbose ):

      print \
        'running DWI connectivity matrix'

    task = DwiConnectivityMatrixTask( self._application )
    task.launch( False )

  def view( self ):

    if ( self._verbose ):

      print \
        'viewing DWI connectivity matrix'

    task = DwiConnectivityMatrixTask( self._application )
    task.launch( True )

