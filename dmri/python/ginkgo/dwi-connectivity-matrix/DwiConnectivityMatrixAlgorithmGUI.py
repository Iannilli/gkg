from AlgorithmGUI import *
from ResourceManager import *


class DwiConnectivityMatrixAlgorithmGUI( AlgorithmGUI ):

  def __init__( self, algorithm ):

    AlgorithmGUI.__init__( self,
                           algorithm,
                           ResourceManager().getUIFileName(
                                                  'dmri',
                                                  'DwiConnectivityMatrix.ui' ) )

    ############################################################################
    # Connecting input bundle map
    ############################################################################

    self.connectStringParameterToLineEdit( 'fileNameInputBundleMap',
                                           'lineEdit_FileNameInputBundleMap' )

    self.connectFileListBrowserToPushButtonAndLineEdit(
                                            'pushButton_FileNameInputBundleMap',
                                            'lineEdit_FileNameInputBundleMap' )
    
    self.connectStringParameterToLineEdit( 'fileNameT1',
                                           'lineEdit_FileNameT1' )

    self.connectFileBrowserToPushButtonAndLineEdit( 'pushButton_FileNameT1',
                                                    'lineEdit_FileNameT1' )
    
    self.connectStringParameterToLineEdit( 'fileNameDwToT1Transform',
                                           'lineEdit_FileNameDwToT1Transform' )

    self.connectFileBrowserToPushButtonAndLineEdit(
                                           'pushButton_FileNameDwToT1Transform',
                                           'lineEdit_FileNameDwToT1Transform' )

    ############################################################################
    # Connecting connectivity matrix type interface
    ############################################################################
    
    
    self.connectChoiceParameterToComboBox( 'connectivityMatrixType',
                                           'comboBox_ConnectivityMatrixType' )
    self.connectComboBoxToStackedWidget( 
                                     'comboBox_ConnectivityMatrixType',
                                     'stackedWidget_ConnectivityMatrixTypeROI' )
    self.connectComboBoxToStackedWidget( 
                              'comboBox_ConnectivityMatrixType',
                              'stackedWidget_ConnectivityMatrixTypeParameters' )
    
    ############################################################################
    # Connecting ROI Sets interface
    ############################################################################
    
    # Mesh to Mesh -> Not implemented yet
    # Mesh to mesh-and-texture -> Not implemented yet
    # Mesh-and-texture to mesh-and-texture -> Not implemented yet
    # Mesh-and-texture alone
    
    self.connectStringParameterToLineEdit( 
                                        'fileNameMeshAndTextureAlone_ROIMesh',
                                        'lineEdit_MeshAndTextureAlone_ROIMesh' )

    self.connectFileBrowserToPushButtonAndLineEdit(
                              'pushButton_FileNameMeshAndTextureAlone_ROIMesh',
                              'lineEdit_MeshAndTextureAlone_ROIMesh' )
    
    self.connectStringParameterToLineEdit( 
                                    'fileNameMeshAndTextureAlone_ROITexture',
                                    'lineEdit_MeshAndTextureAlone_ROITexture' )

    self.connectFileBrowserToPushButtonAndLineEdit(
                            'pushButton_FileNameMeshAndTextureAlone_ROITexture',
                            'lineEdit_MeshAndTextureAlone_ROITexture' )


    self.connectStringParameterToLineEdit( 
                        'fileNameMeshAndTextureAlone_ROIToBundleMapTransform',
                        'lineEdit_MeshAndTextureAlone_ROIToBundleMapTransform' )

    self.connectFileBrowserToPushButtonAndLineEdit(
               'pushButton_FileNameMeshAndTextureAlone_ROIToBundleMapTransform',
               'lineEdit_MeshAndTextureAlone_ROIToBundleMapTransform' )
    
    # Volume to mesh
    self.connectStringParameterToLineEdit( 'fileNameVolumeToMesh_ROIVolume',
                                           'lineEdit_VolumeToMesh_ROIVolume' )

    self.connectFileBrowserToPushButtonAndLineEdit(
                                    'pushButton_FileNameVolumeToMesh_ROIVolume',
                                    'lineEdit_VolumeToMesh_ROIVolume' )
    
    self.connectStringParameterToLineEdit( 
                               'fileNameVolumeToMesh_ROIToBundleMapTransform',
                               'lineEdit_VolumeToMesh_ROIToBundleMapTransform' )

    self.connectFileBrowserToPushButtonAndLineEdit(
                      'pushButton_FileNameVolumeToMesh_ROIToBundleMapTransform',
                      'lineEdit_VolumeToMesh_ROIToBundleMapTransform' )
    
    self.connectStringParameterToLineEdit( 'fileNameVolumeToMesh_ROIMesh',
                                           'lineEdit_VolumeToMesh_ROIMesh' )

    self.connectFileBrowserToPushButtonAndLineEdit(
                                      'pushButton_FileNameVolumeToMesh_ROIMesh',
                                      'lineEdit_VolumeToMesh_ROIMesh' )
    
    self.connectStringParameterToLineEdit( 
                          'fileNameVolumeToMesh_ROIMeshToBundleMapTransform',
                          'lineEdit_VolumeToMesh_ROIMeshToBundleMapTransform' )

    self.connectFileBrowserToPushButtonAndLineEdit(
                  'pushButton_FileNameVolumeToMesh_ROIMeshToBundleMapTransform',
                  'lineEdit_VolumeToMesh_ROIMeshToBundleMapTransform' )

    
    # Volume to mesh-and-texture
    self.connectStringParameterToLineEdit( 
                                   'fileNameVolumeToMeshAndTexture_ROIVolume',
                                   'lineEdit_VolumeToMeshAndTexture_ROIVolume' )

    self.connectFileBrowserToPushButtonAndLineEdit(
                          'pushButton_FileNameVolumeToMeshAndTexture_ROIVolume',
                          'lineEdit_VolumeToMeshAndTexture_ROIVolume' )
    
    self.connectStringParameterToLineEdit( 
                     'fileNameVolumeToMeshAndTexture_ROIToBundleMapTransform',
                     'lineEdit_VolumeToMeshAndTexture_ROIToBundleMapTransform' )

    self.connectFileBrowserToPushButtonAndLineEdit(
            'pushButton_FileNameVolumeToMeshAndTexture_ROIToBundleMapTransform',
            'lineEdit_VolumeToMeshAndTexture_ROIToBundleMapTransform' )    
    
    self.connectStringParameterToLineEdit( 
                                     'fileNameVolumeToMeshAndTexture_ROIMesh',
                                     'lineEdit_VolumeToMeshAndTexture_ROIMesh' )

    self.connectFileBrowserToPushButtonAndLineEdit(
                            'pushButton_FileNameVolumeToMeshAndTexture_ROIMesh',
                            'lineEdit_VolumeToMeshAndTexture_ROIMesh' )
    
    self.connectStringParameterToLineEdit( 
                                  'fileNameVolumeToMeshAndTexture_ROITexture',
                                  'lineEdit_VolumeToMeshAndTexture_ROITexture' )

    self.connectFileBrowserToPushButtonAndLineEdit(
                         'pushButton_FileNameVolumeToMeshAndTexture_ROITexture',
                         'lineEdit_VolumeToMeshAndTexture_ROITexture' )

    self.connectStringParameterToLineEdit( 
          'fileNameVolumeToMeshAndTexture_ROIMeshAndTextToBundleMapTransform',
          'lineEdit_VolumeToMeshAndTexture_ROIMeshAndTextToBundleMapTransform' )

    self.connectFileBrowserToPushButtonAndLineEdit(
 'pushButton_FileNameVolumeToMeshAndTexture_ROIMeshAndTextToBundleMapTransform',
 'lineEdit_VolumeToMeshAndTexture_ROIMeshAndTextToBundleMapTransform' )

    # Volume alone
    self.connectStringParameterToLineEdit( 'fileNameVolumeAlone_ROIVolume',
                                           'lineEdit_VolumeAlone_ROIVolume' )

    self.connectFileBrowserToPushButtonAndLineEdit(
                                     'pushButton_FileNameVolumeAlone_ROIVolume',
                                     'lineEdit_VolumeAlone_ROIVolume' )
    
    self.connectStringParameterToLineEdit( 
                                'fileNameVolumeAlone_ROIToBundleMapTransform',
                                'lineEdit_VolumeAlone_ROIToBundleMapTransform' )

    self.connectFileBrowserToPushButtonAndLineEdit(
                       'pushButton_FileNameVolumeAlone_ROIToBundleMapTransform',
                       'lineEdit_VolumeAlone_ROIToBundleMapTransform' )
    
    # Volume to volume -> Not implemented yet
    
    
    ############################################################################
    # Connecting connectivity matrix parameters interface
    ############################################################################

    # Mesh to Mesh -> Not implemented yet
    # Mesh to mesh-and-texture -> Not implemented yet
    # Mesh-and-texture to mesh-and-texture -> Not implemented yet
    # Mesh-and-texture alone
    self.connectDoubleParameterToSpinBox(
                  'meshAndTextureAloneInputFiberResamplingStep',
                  'doubleSpinBox_MeshAndTextureAlone_InputFiberResamplingStep' )
    
    self.connectDoubleParameterToSpinBox(
                     'meshAndTextureAloneMaximumDistanceToMesh',
                     'doubleSpinBox_MeshAndTextureAlone_MaximumDistanceToMesh' )
    
    self.connectDoubleParameterToSpinBox(
                 'meshAndTextureAloneOutputFiberResamplingStep',
                 'doubleSpinBox_MeshAndTextureAlone_OutputFiberResamplingStep' )
    
    self.connectBooleanParameterToCheckBox( 
                                   'meshAndTextureAloneFiberTangent',
                                   'checkBox_MeshAndTextureAlone_FiberTangent' )
    self.connectCheckBoxToCustomCallback(
            'checkBox_MeshAndTextureAlone_FiberTangent',
            self.disableOrEnableMaximumFiberToNormalAngleInMeshAndTextureAlone )
    self.connectIntegerParameterToSpinBox(
                       'meshAndTextureAloneMaximumFiberToNormalAngle',
                       'spinBox_MeshAndTextureAlone_MaximumFiberToNormalAngle' )
    
    self.connectBooleanParameterToCheckBox( 
                          'meshAndTextureAloneSmoothConnectivityMap',
                          'checkBox_MeshAndTextureAlone_SmoothConnectivityMap' )
    
    self.connectCheckBoxToCustomCallback(
                           'checkBox_MeshAndTextureAlone_SmoothConnectivityMap',
                           self.disableOrEnableSmoothingInMeshAndTextureAlone )
    
    self.connectDoubleParameterToSpinBox(
              'meshAndTextureAloneStandardDeviationOfSmoothing',
              'doubleSpinBox_MeshAndTextureAlone_StandardDeviationOfSmoothing' )
    
    self.connectIntegerParameterToSpinBox(
                                      'meshAndTextureAloneCacheXSize',
                                      'spinBox_MeshAndTextureAlone_CacheXSize' )
    
    self.connectIntegerParameterToSpinBox(
                                      'meshAndTextureAloneCacheYSize',
                                      'spinBox_MeshAndTextureAlone_CacheYSize' )
    
    self.connectIntegerParameterToSpinBox(
                                      'meshAndTextureAloneCacheZSize',
                                      'spinBox_MeshAndTextureAlone_CacheZSize' )
    
    # Volume to mesh
    self.connectDoubleParameterToSpinBox(
                        'volumeToMeshMinimumIntersectionLength',
                        'doubleSpinBox_VolumeToMesh_MinimumIntersectionLength' )
    
    self.connectDoubleParameterToSpinBox(
                         'volumeToMeshInputFiberResamplingStep',
                         'doubleSpinBox_VolumeToMesh_InputFiberResamplingStep' )
    
    self.connectDoubleParameterToSpinBox(
                            'volumeToMeshMaximumDistanceToMesh',
                            'doubleSpinBox_VolumeToMesh_MaximumDistanceToMesh' )
    
    self.connectDoubleParameterToSpinBox(
                        'volumeToMeshOutputFiberResamplingStep',
                        'doubleSpinBox_VolumeToMesh_OutputFiberResamplingStep' )
    
    self.connectBooleanParameterToCheckBox( 
                                          'volumeToMeshFiberTangent',
                                          'checkBox_VolumeToMesh_FiberTangent' )
    self.connectCheckBoxToCustomCallback(
                   'checkBox_VolumeToMesh_FiberTangent',
                   self.disableOrEnableMaximumFiberToNormalAngleInVolumeToMesh )
    self.connectIntegerParameterToSpinBox(
                              'volumeToMeshMaximumFiberToNormalAngle',
                              'spinBox_VolumeToMesh_MaximumFiberToNormalAngle' )
    
    self.connectBooleanParameterToCheckBox( 
                                 'volumeToMeshSmoothConnectivityMap',
                                 'checkBox_VolumeToMesh_SmoothConnectivityMap' )
    
    self.connectCheckBoxToCustomCallback(
                                  'checkBox_VolumeToMesh_SmoothConnectivityMap',
                                  self.disableOrEnableSmoothingInVolumeToMesh )
    
    self.connectDoubleParameterToSpinBox(
                     'volumeToMeshStandardDeviationOfSmoothing',
                     'doubleSpinBox_VolumeToMesh_StandardDeviationOfSmoothing' )
    
    self.connectIntegerParameterToSpinBox( 'volumeToMeshCacheXSize',
                                           'spinBox_VolumeToMesh_CacheXSize' )
    
    self.connectIntegerParameterToSpinBox( 'volumeToMeshCacheYSize',
                                           'spinBox_VolumeToMesh_CacheYSize' )
    
    self.connectIntegerParameterToSpinBox( 'volumeToMeshCacheZSize',
                                           'spinBox_VolumeToMesh_CacheZSize' )
    
    # Volume to mesh-and-texture
    self.connectDoubleParameterToSpinBox(
              'volumeToMeshAndTextureMinimumIntersectionLength',
              'doubleSpinBox_VolumeToMeshAndTexture_MinimumIntersectionLength' )
    
    self.connectDoubleParameterToSpinBox(
               'volumeToMeshAndTextureInputFiberResamplingStep',
               'doubleSpinBox_VolumeToMeshAndTexture_InputFiberResamplingStep' )
    
    self.connectDoubleParameterToSpinBox(
                  'volumeToMeshAndTextureMaximumDistanceToMesh',
                  'doubleSpinBox_VolumeToMeshAndTexture_MaximumDistanceToMesh' )
    
    self.connectDoubleParameterToSpinBox(
              'volumeToMeshAndTextureOutputFiberResamplingStep',
              'doubleSpinBox_VolumeToMeshAndTexture_OutputFiberResamplingStep' )
    
    self.connectBooleanParameterToCheckBox( 
                                'volumeToMeshAndTextureFiberTangent',
                                'checkBox_VolumeToMeshAndTexture_FiberTangent' )
    self.connectCheckBoxToCustomCallback(
         'checkBox_VolumeToMeshAndTexture_FiberTangent',
         self.disableOrEnableMaximumFiberToNormalAngleInVolumeToMeshAndTexture )
    self.connectIntegerParameterToSpinBox(
                    'volumeToMeshAndTextureMaximumFiberToNormalAngle',
                    'spinBox_VolumeToMeshAndTexture_MaximumFiberToNormalAngle' )
    
    self.connectBooleanParameterToCheckBox( 
                       'volumeToMeshAndTextureSmoothConnectivityMap',
                       'checkBox_VolumeToMeshAndTexture_SmoothConnectivityMap' )
    
    self.connectCheckBoxToCustomCallback(
                        'checkBox_VolumeToMeshAndTexture_SmoothConnectivityMap',
                        self.disableOrEnableSmoothingInVolumeToMeshAndTexture )
    
    self.connectDoubleParameterToSpinBox(
           'volumeToMeshAndTextureStandardDeviationOfSmoothing',
           'doubleSpinBox_VolumeToMeshAndTexture_StandardDeviationOfSmoothing' )
    
    self.connectIntegerParameterToSpinBox( 
                                   'volumeToMeshAndTextureCacheXSize',
                                   'spinBox_VolumeToMeshAndTexture_CacheXSize' )
    
    self.connectIntegerParameterToSpinBox( 
                                   'volumeToMeshAndTextureCacheYSize',
                                   'spinBox_VolumeToMeshAndTexture_CacheYSize' )
    
    self.connectIntegerParameterToSpinBox( 
                                   'volumeToMeshAndTextureCacheZSize',
                                   'spinBox_VolumeToMeshAndTexture_CacheZSize' )
    
    self.connectIntegerParameterToSpinBox( 
                         'volumeToMeshAndTextureOffsetForOutputLabel',
                         'spinBox_VolumeToMeshAndTexture_OffsetForOutputLabel' )
    
    # Volume alone
    self.connectDoubleParameterToSpinBox(
                         'volumeAloneMinimumIntersectionLength',
                         'doubleSpinBox_VolumeAlone_MinimumIntersectionLength' )
    
    self.connectDoubleParameterToSpinBox(
                          'volumeAloneInputFiberResamplingStep',
                          'doubleSpinBox_VolumeAlone_InputFiberResamplingStep' )
    
    self.connectDoubleParameterToSpinBox(
                         'volumeAloneOutputFiberResamplingStep',
                         'doubleSpinBox_VolumeAlone_OutputFiberResamplingStep' )
    
    self.connectBooleanParameterToCheckBox( 
                                       'volumeAloneFurtherInterface',
                                       'checkBox_VolumeAlone_FurtherInterface' )
    
    self.connectCheckBoxToCustomCallback(
                             'checkBox_VolumeAlone_FurtherInterface',
                             self.disableOrEnableFurtherInterfaceInVolumeAlone )
    
    self.connectIntegerParameterToSpinBox( 
                                    'volumeAloneOffsetForOutputLabel',
                                    'spinBox_VolumeAlone_OffsetForOutputLabel' )
    
    self.connectDoubleParameterToSpinBox(
                             'volumeAloneMaximumDistanceToMesh',
                             'doubleSpinBox_VolumeAlone_MaximumDistanceToMesh' )
    
    self.connectIntegerParameterToSpinBox( 'volumeAloneCacheXSize',
                                           'spinBox_VolumeAlone_CacheXSize' )
    
    self.connectIntegerParameterToSpinBox( 'volumeAloneCacheYSize',
                                           'spinBox_VolumeAlone_CacheYSize' )
    
    self.connectIntegerParameterToSpinBox( 'volumeAloneCacheZSize',
                                           'spinBox_VolumeAlone_CacheZSize' )
    
    self.connectStringParameterToLineEdit( 
                                       'fileNameVolumeAlone_FurtherInterface',
                                       'lineEdit_VolumeAlone_FurtherInterface' )

    self.connectFileBrowserToPushButtonAndLineEdit(
                              'pushButton_FileNameVolumeAlone_FurtherInterface',
                              'lineEdit_VolumeAlone_FurtherInterface' )
    
    self.connectStringParameterToLineEdit( 
                                       'fileNameVolumeAlone_InterfaceToBundleTransform',
                                       'lineEdit_VolumeAlone_InterfaceToBundleTransform' )

    self.connectFileBrowserToPushButtonAndLineEdit(
                    'pushButton_FileNameVolumeAlone_InterfaceToBundleTransform',
                    'lineEdit_VolumeAlone_InterfaceToBundleTransform' )
    
    # Volume to volume -> Not implemented yet


    ############################################################################
    # Connecting output interface
    ############################################################################

    self.connectChoiceParameterToComboBox( 'outputBundleMapFormat',
                                           'comboBox_OutputBundleMapFormat' )
    
    self.connectChoiceParameterToComboBox( 'outputTextureMapFormat',
                                           'comboBox_OutputTextureMapFormat' )
    
    
  ############################################################################
  # Define callbacks
  ############################################################################
    
  def disableOrEnableSmoothingInMeshAndTextureAlone( self, state ):

    doubleSpinBox = self._findChild( self._awin,
              'doubleSpinBox_MeshAndTextureAlone_StandardDeviationOfSmoothing' )
    labelWidget = self._findChild( self._awin,
                      'label_MeshAndTextureAlone_StandardDeviationOfSmoothing' )
    

    if ( state == 2 ):

      doubleSpinBox.setEnabled( True )
      labelWidget.setEnabled( True )

    else:

      doubleSpinBox.setEnabled( False )
      labelWidget.setEnabled( False )
      
      
  def disableOrEnableSmoothingInVolumeToMesh( self, state ):

    doubleSpinBox = self._findChild( self._awin,
                     'doubleSpinBox_VolumeToMesh_StandardDeviationOfSmoothing' )
    labelWidget = self._findChild( self._awin,
                             'label_VolumeToMesh_StandardDeviationOfSmoothing' )
    
    if ( state == 2 ):

      doubleSpinBox.setEnabled( True )
      labelWidget.setEnabled( True )

    else:

      doubleSpinBox.setEnabled( False )
      labelWidget.setEnabled( False )
      
      
  def disableOrEnableSmoothingInVolumeToMeshAndTexture( self, state ):

    doubleSpinBox = self._findChild( self._awin,
           'doubleSpinBox_VolumeToMeshAndTexture_StandardDeviationOfSmoothing' )
    labelWidget = self._findChild( self._awin,
                   'label_VolumeToMeshAndTexture_StandardDeviationOfSmoothing' )
    
    if ( state == 2 ):

      doubleSpinBox.setEnabled( True )
      labelWidget.setEnabled( True )

    else:

      doubleSpinBox.setEnabled( False )
      labelWidget.setEnabled( False )
      

  def disableOrEnableFurtherInterfaceInVolumeAlone( self, state ):

    lineEdit = self._findChild( self._awin,
                                'lineEdit_VolumeAlone_FurtherInterface' )
    lineEdit2 = self._findChild( 
                             self._awin,
                             'lineEdit_VolumeAlone_InterfaceToBundleTransform' )
    pushButton = self._findChild( 
                            self._awin,
                            'pushButton_FileNameVolumeAlone_FurtherInterface' )
    pushButton2 = self._findChild( 
                  self._awin,
                  'pushButton_FileNameVolumeAlone_InterfaceToBundleTransform' )
    labelWidget = self._findChild( self._awin,
                                   'label_VolumeAlone_FurtherInterface' )
    labelWidget2 = self._findChild( 
                                self._awin,
                                'label_VolumeAlone_InterfaceToBundleTransform' )
    labelWidget3 = self._findChild( self._awin,
                                    'label_VolumeAlone_CacheXSize' )
    labelWidget4 = self._findChild( self._awin,
                                    'label_VolumeAlone_CacheYSize' )
    labelWidget5 = self._findChild( self._awin,
                                    'label_VolumeAlone_CacheZSize' )
    spinBoxWidget = self._findChild( self._awin,
                                     'spinBox_VolumeAlone_CacheXSize' )
    spinBoxWidget2 = self._findChild( self._awin,
                                     'spinBox_VolumeAlone_CacheYSize' )
    spinBoxWidget3 = self._findChild( self._awin,
                                     'spinBox_VolumeAlone_CacheZSize' )
    
    if ( state == 2 ):

      lineEdit.setEnabled( True )
      lineEdit2.setEnabled( True )
      pushButton.setEnabled( True )
      pushButton2.setEnabled( True )
      labelWidget.setEnabled( True )
      labelWidget2.setEnabled( True )
      labelWidget3.setEnabled( True )
      labelWidget4.setEnabled( True )
      labelWidget5.setEnabled( True )
      spinBoxWidget.setEnabled( True )
      spinBoxWidget2.setEnabled( True )
      spinBoxWidget3.setEnabled( True )

    else:

      lineEdit.setEnabled( False )
      lineEdit2.setEnabled( False )
      pushButton.setEnabled( False )
      pushButton2.setEnabled( False )
      labelWidget.setEnabled( False )
      labelWidget2.setEnabled( False )
      labelWidget3.setEnabled( False )
      labelWidget4.setEnabled( False )
      labelWidget5.setEnabled( False )
      spinBoxWidget.setEnabled( False )
      spinBoxWidget2.setEnabled( False )
      spinBoxWidget3.setEnabled( False )

  def disableOrEnableMaximumFiberToNormalAngleInMeshAndTextureAlone( self,
                                                                     state ):

    label = self._findChild( \
                         self._awin,
                         'label_MeshAndTextureAlone_MaximumFiberToNormalAngle' )
    spinBoxWidget = self._findChild( \
                       self._awin,
                       'spinBox_MeshAndTextureAlone_MaximumFiberToNormalAngle' )
    if ( state == 2 ):

      label.setEnabled( True )
      spinBoxWidget.setEnabled( True )

    else:

      label.setEnabled( False )
      spinBoxWidget.setEnabled( False )


  def disableOrEnableMaximumFiberToNormalAngleInVolumeToMesh( self, state ):

    label = self._findChild( self._awin,
                             'label_VolumeToMesh_MaximumFiberToNormalAngle' )
    spinBoxWidget = self._findChild( \
                              self._awin,
                              'spinBox_VolumeToMesh_MaximumFiberToNormalAngle' )
    if ( state == 2 ):

      label.setEnabled( True )
      spinBoxWidget.setEnabled( True )

    else:

      label.setEnabled( False )
      spinBoxWidget.setEnabled( False )


  def disableOrEnableMaximumFiberToNormalAngleInVolumeToMeshAndTexture( self,
                                                                        state ):

    label = self._findChild( \
                      self._awin,
                      'label_VolumeToMeshAndTexture_MaximumFiberToNormalAngle' )
    spinBoxWidget = self._findChild( \
                    self._awin,
                    'spinBox_VolumeToMeshAndTexture_MaximumFiberToNormalAngle' )
    if ( state == 2 ):

      label.setEnabled( True )
      spinBoxWidget.setEnabled( True )

    else:

      label.setEnabled( False )
      spinBoxWidget.setEnabled( False )

