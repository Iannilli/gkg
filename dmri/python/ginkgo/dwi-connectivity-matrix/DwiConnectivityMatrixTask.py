from Task import *
from matplotlib.pylab import *
import glob
import re
import colorsys
from random import randint


class DwiConnectivityMatrixTask( Task ):

  def __init__( self, application ):

    Task.__init__( self, application )


  ##############################################################################
  # execute()
  ##############################################################################

  def execute( self, functors, parameters, subjectName, verbose, viewOnly ):

    try:

      ####################### acquiring conditions #############################
      functors[ 'condition-acquire' ]( subjectName,
                                      'connectivity-matrix-processed' )
      functors[ 'condition-acquire' ]( subjectName, 'mesh-created' )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 0 )

      ####################### collecting input info ############################
      fileNameInputBundleMap = \
                  parameters[ 'fileNameInputBundleMap' ].getValue().split( ';' )
      # Sanity check
      for bundle in fileNameInputBundleMap:
        
        if not os.path.exists( bundle ):
        
          print "Bundle map specified does not exist: nothing to do..."
          return None
      
      fileNameT1 = parameters[ 'fileNameT1' ].getValue()
      fileNameDwToT1Transform = \
                              parameters[ 'fileNameDwToT1Transform' ].getValue()
      
      # Sanity check
      self.checkIfFilesExist( fileNameT1,
                              fileNameDwToT1Transform )
      
      
      ####################### collecting output information ####################
      outputBundleMapFormat = parameters[ 'outputBundleMapFormat' ].getChoice()
      outputTextureMapFormat = \
                              parameters[ 'outputTextureMapFormat' ].getChoice()
      outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()
      
      if not os.path.exists( outputWorkDirectory ):

        print "Creating output directory: ", outputWorkDirectory
        os.mkdir( outputWorkDirectory )
      
      if ( verbose ):

        print 'Output bundle format: ', outputBundleMapFormat
        print 'Output texture format: ', outputTextureMapFormat
        print 'Output work directory: ', outputWorkDirectory
        
      ####################### Initialize command arguments #####################
      
      # Input bundle map file name list 
      argumentB = ''
      
      # First input ROIs volume file name 
      argumentV1 = ''
      
      # First input ROIs mesh file name
      argumentM1 = ''
      
      # First input ROIs texture file name
      argumentT1 = ''
      
      # 3D rigid transform from ROIs1 to bundle map referential
      argumentTR1 = ''
      
      # Second input ROIs volume file name
      argumentV2 = ''
      
      # Second input ROIs mesh file name
      argumentM2 = ''
      
      # Second input ROIs texture file name
      argumentT2 = ''
      
      # 3D rigid transform from ROIs2 to bundle map referential
      argumentTR2 = ''
      
      # Scalar parameters according to ROIs1/ROIs2
      argumentScalarParameters = ''
      
      # String parameters according to ROIs1/ROIs2
      argumentStringParameters = ''
      
      # Functors according to ROIs1/ROIs2 
      argumentF = ''
      
      # Output file name(s) 
      argumentO = ''
      
      # Output bundle map format 
      argumentOutputBundleMapFormat = outputBundleMapFormat
      
      # Output texture map format
      argumentOutputTextureMapFormat = outputTextureMapFormat
      
      ################### setting options according to connectivity type #######
      connectivityMatrixType = \
                              parameters[ 'connectivityMatrixType' ].getChoice()
      
      if ( verbose ):

        print  'Connectivity matrix type: ', connectivityMatrixType
        
      
      # Common parameters
      bundlesColor = [ 0.1, 0.1, 0.8, 0.3 ]
      
      # Common output file
      fileNameOutputBundleMap = ''
      fileNameResTexture = ''
      fileNameConnectivityMatrix = os.path.join( outputWorkDirectory,
                                                'connectivity-matrix.mat' )
        
      ##########################################################################
      ############# mesh-and-texture alone #####################################
      ##########################################################################
        
      if ( connectivityMatrixType == 'mesh-and-texture alone' ):
      
          # Get ROI sets
          fileNameMeshAndTextureAlone_ROIMesh = parameters[ 
                              'fileNameMeshAndTextureAlone_ROIMesh' ].getValue()
          fileNameMeshAndTextureAlone_ROITexture = parameters[ 
                           'fileNameMeshAndTextureAlone_ROITexture' ].getValue()
          fileNameMeshAndTextureAlone_ROIToBundleMapTransform = parameters[ 
              'fileNameMeshAndTextureAlone_ROIToBundleMapTransform' ].getValue()
                  
          # Sanity check
          self.checkIfFilesExist( 
                           fileNameMeshAndTextureAlone_ROIMesh, 
                           fileNameMeshAndTextureAlone_ROITexture,
                           fileNameMeshAndTextureAlone_ROIToBundleMapTransform )
                
          # Get Connectivity matrix parameters
          meshAndTextureAloneInputFiberResamplingStep = parameters[
                      'meshAndTextureAloneInputFiberResamplingStep' ].getValue()
          
          meshAndTextureAloneMaximumDistanceToMesh = parameters[
                         'meshAndTextureAloneMaximumDistanceToMesh' ].getValue()
          
          meshAndTextureAloneOutputFiberResamplingStep = parameters[
                     'meshAndTextureAloneOutputFiberResamplingStep' ].getValue()
          
          meshAndTextureAloneFiberTangent = parameters[
                                  'meshAndTextureAloneFiberTangent' ].getValue()

          meshAndTextureAloneMaximumFiberToNormalAngle = parameters[
                     'meshAndTextureAloneMaximumFiberToNormalAngle' ].getValue()
          if( meshAndTextureAloneFiberTangent == 0 ):

            meshAndTextureAloneMaximumFiberToNormalAngle = 0
                                          
          meshAndTextureAloneCacheXSize = parameters[
                                    'meshAndTextureAloneCacheXSize' ].getValue()
          
          meshAndTextureAloneCacheYSize = parameters[
                                    'meshAndTextureAloneCacheYSize' ].getValue()
          
          meshAndTextureAloneCacheZSize = parameters[
                                    'meshAndTextureAloneCacheZSize' ].getValue()

          meshAndTextureAloneSmoothConnectivityMap = parameters[
                         'meshAndTextureAloneSmoothConnectivityMap' ].getValue()
          
          meshAndTextureAloneStandardDeviationOfSmoothing = parameters[
                  'meshAndTextureAloneStandardDeviationOfSmoothing' ].getValue()
                      
          
          # Fill arguments
          argumentM1 = fileNameMeshAndTextureAlone_ROIMesh
          
          argumentT1 = fileNameMeshAndTextureAlone_ROITexture
          
          argumentTR1 = fileNameMeshAndTextureAlone_ROIToBundleMapTransform
                            
          argumentScalarParameters = \
                str( meshAndTextureAloneInputFiberResamplingStep ) + ' ' + \
                str( meshAndTextureAloneMaximumDistanceToMesh ) + ' ' + \
                str( meshAndTextureAloneOutputFiberResamplingStep ) + ' ' + \
                str( meshAndTextureAloneFiberTangent ) + ' ' + \
                str( meshAndTextureAloneSmoothConnectivityMap ) + ' ' + \
                str( meshAndTextureAloneStandardDeviationOfSmoothing ) + ' ' + \
                str( meshAndTextureAloneCacheXSize ) + ' ' + \
                str( meshAndTextureAloneCacheYSize ) + ' ' + \
                str( meshAndTextureAloneCacheZSize )
          
          # Set functor parameters                   
          argumentF = 'connectivity-matrix' + ' ' + \
                      'average-length-matrix' + ' ' + \
                      'roi-to-roi-bundle-map' + ' ' + \
                      'roi-to-mesh-connectivity-texture-map'
                      
          # Set output file names 
          fileNameAverageLengthMatrix = os.path.join( 
                                                 outputWorkDirectory,
                                                 'average-length-matrix.mat' )
          fileNameOutputBundleMap = os.path.join( outputWorkDirectory,
                                                'roi-to-roi-bundle-map.bundles' )
          
          fileNameResTexture = os.path.join( outputWorkDirectory,
                                            'roi-to-mesh-texture-map.tex' )
          
          argumentO = fileNameConnectivityMatrix + ' ' + \
                      fileNameAverageLengthMatrix + ' ' + \
                      fileNameOutputBundleMap + ' ' + \
                      fileNameResTexture
                      
      ##########################################################################
      ############# volume to mesh #############################################
      ##########################################################################
      
      elif ( connectivityMatrixType == 'volume to mesh' ):
          
        # Get ROI sets
        fileNameVolumeToMesh_ROIVolume = parameters[ 
                                   'fileNameVolumeToMesh_ROIVolume' ].getValue()
        
        fileNameVolumeToMesh_ROIToBundleMapTransform = parameters[ 
                     'fileNameVolumeToMesh_ROIToBundleMapTransform' ].getValue()
        
        fileNameVolumeToMesh_ROIMesh = parameters[ 
                                     'fileNameVolumeToMesh_ROIMesh' ].getValue()
        
        fileNameVolumeToMesh_ROIMeshToBundleMapTransform = parameters[ 
                 'fileNameVolumeToMesh_ROIMeshToBundleMapTransform' ].getValue()
        
        # Sanity check
        self.checkIfFilesExist( \
                              fileNameVolumeToMesh_ROIVolume, 
                              fileNameVolumeToMesh_ROIToBundleMapTransform,
                              fileNameVolumeToMesh_ROIMesh,
                              fileNameVolumeToMesh_ROIMeshToBundleMapTransform )
        
        # Get Connectivity matrix parameters
        volumeToMeshMinimumIntersectionLength = parameters[
                            'volumeToMeshMinimumIntersectionLength' ].getValue()
        
        volumeToMeshInputFiberResamplingStep = parameters[
                             'volumeToMeshInputFiberResamplingStep' ].getValue()
        
        volumeToMeshMaximumDistanceToMesh = parameters[
                                'volumeToMeshMaximumDistanceToMesh' ].getValue()
        
        volumeToMeshOutputFiberResamplingStep = parameters[
                            'volumeToMeshOutputFiberResamplingStep' ].getValue()
        
        volumeToMeshFiberTangent = parameters[
                                         'volumeToMeshFiberTangent' ].getValue()

        volumeToMeshMaximumFiberToNormalAngle = parameters[
                     'meshAndTextureAloneMaximumFiberToNormalAngle' ].getValue()
        if( volumeToMeshFiberTangent == 0 ):

          volumeToMeshMaximumFiberToNormalAngle = 0
                                        
        volumeToMeshCacheXSize = \
                               parameters[ 'volumeToMeshCacheXSize' ].getValue()
        
        volumeToMeshCacheYSize = \
                               parameters[ 'volumeToMeshCacheYSize' ].getValue()
        
        volumeToMeshCacheZSize = \
                               parameters[ 'volumeToMeshCacheZSize' ].getValue()

        volumeToMeshSmoothConnectivityMap = parameters[
                                'volumeToMeshSmoothConnectivityMap' ].getValue()
        
        volumeToMeshStandardDeviationOfSmoothing = parameters[
                         'volumeToMeshStandardDeviationOfSmoothing' ].getValue()
                    
        
        # Fill arguments
        argumentV1 = fileNameVolumeToMesh_ROIVolume
        
        argumentTR1 = fileNameVolumeToMesh_ROIToBundleMapTransform
        
        argumentM2 = fileNameVolumeToMesh_ROIMesh
        
        argumentTR2 = fileNameVolumeToMesh_ROIMeshToBundleMapTransform
                          
        argumentScalarParameters = \
                    str( volumeToMeshMinimumIntersectionLength ) + ' ' + \
                    str( volumeToMeshInputFiberResamplingStep ) + ' ' + \
                    str( volumeToMeshMaximumDistanceToMesh ) + ' ' + \
                    str( volumeToMeshOutputFiberResamplingStep ) + ' ' + \
                    str( volumeToMeshFiberTangent ) + ' ' + \
                    str( volumeToMeshSmoothConnectivityMap ) + ' ' + \
                    str( volumeToMeshStandardDeviationOfSmoothing ) + ' ' + \
                    str( volumeToMeshCacheXSize ) + ' ' + \
                    str( volumeToMeshCacheYSize ) + ' ' + \
                    str( volumeToMeshCacheZSize )
                    
        
        # Set functor parameters                   
        argumentF = 'connectivity-matrix' + ' ' + \
                    'average-length-matrix' + ' ' + \
                    'roi-to-mesh-bundle-map' + ' ' + \
                    'roi-to-roi-connectivity-texture-map'
                    
        # Set output file names 
        fileNameAverageLengthMatrix = os.path.join( 
                                                 outputWorkDirectory,
                                                 'average-length-matrix.mat' )
        fileNameOutputBundleMap = os.path.join( \
                                               outputWorkDirectory,
                                               'roi-to-mesh-bundle-map.bundles' )
        
        fileNameResTexture = os.path.join( outputWorkDirectory,
                                          'roi-to-roi-texture-map.tex' )
        
        argumentO = fileNameConnectivityMatrix + ' ' + \
                    fileNameAverageLengthMatrix + ' ' + \
                    fileNameOutputBundleMap + ' ' + \
                    fileNameResTexture
                    
        
        # Create Mesh   
        roiBaseName = os.path.splitext( os.path.basename( \
                                          fileNameVolumeToMesh_ROIVolume) )[ 0 ]
        
        if ( verbose ):
          
          print 'roiBaseName: ', roiBaseName
          print 'AimsMesh on ROI volume: ', fileNameVolumeToMesh_ROIVolume         

        directoryTmpROIsMesh = os.path.join( outputWorkDirectory,
                                             'TmpROIsMesh' )
                                             
        if ( ( not viewOnly ) and ( not os.path.exists( directoryTmpROIsMesh ) ) ):
        
          os.mkdir( directoryTmpROIsMesh )
        
        outputTmpROIsMesh = os.path.join( directoryTmpROIsMesh, 
                                          str( roiBaseName ) )
        
        command = 'AimsMesh ' + \
                  ' -i ' + fileNameVolumeToMesh_ROIVolume + \
                  ' -o ' + outputTmpROIsMesh + \
                  ' --smooth 20'
                  
        executeCommand( self, subjectName, command, viewOnly )

        command = 'GkgExecuteCommand MeshSelectorUsingFileSize ' + \
                  ' -i ' + directoryTmpROIsMesh + \
                  ' -o ' + outputWorkDirectory + \
                  ' -verbose true'
                  
        executeCommand( self, subjectName, command, viewOnly )
        
        functors[ 'condition-notify-and-release' ]( subjectName,
                                                    'mesh-created' )
                    
      ##########################################################################
      ############# volume to mesh-and-texture #################################
      ##########################################################################
          
      elif ( connectivityMatrixType == 'volume to mesh-and-texture' ):
          
        # Get ROI sets
        fileNameVolumeToMeshAndTexture_ROIVolume = parameters[ 
                         'fileNameVolumeToMeshAndTexture_ROIVolume' ].getValue()
        fileNameVolumeToMeshAndTexture_ROIToBundleMapTransform = parameters[ 
           'fileNameVolumeToMeshAndTexture_ROIToBundleMapTransform' ].getValue()
        fileNameVolumeToMeshAndTexture_ROIMesh = parameters[ 
                           'fileNameVolumeToMeshAndTexture_ROIMesh' ].getValue()
        fileNameVolumeToMeshAndTexture_ROITexture = parameters[ 
                        'fileNameVolumeToMeshAndTexture_ROITexture' ].getValue()
        fileNameVolumeToMeshAndTexture_ROIMeshAndTextToBundleMapTransform = \
         parameters[  
         'fileNameVolumeToMeshAndTexture_ROIMeshAndTextToBundleMapTransform' ].\
                                                                      getValue()
                    
        # Sanity check
        self.checkIfFilesExist( 
             fileNameVolumeToMeshAndTexture_ROIVolume,
             fileNameVolumeToMeshAndTexture_ROIToBundleMapTransform, 
             fileNameVolumeToMeshAndTexture_ROIMesh,
             fileNameVolumeToMeshAndTexture_ROITexture,
             fileNameVolumeToMeshAndTexture_ROIMeshAndTextToBundleMapTransform )
              
        # Get Connectivity matrix parameters
        volumeToMeshAndTextureMinimumIntersectionLength = parameters[
                  'volumeToMeshAndTextureMinimumIntersectionLength' ].getValue()
        
        volumeToMeshAndTextureInputFiberResamplingStep = parameters[
                   'volumeToMeshAndTextureInputFiberResamplingStep' ].getValue()
        
        volumeToMeshAndTextureMaximumDistanceToMesh = parameters[
                      'volumeToMeshAndTextureMaximumDistanceToMesh' ].getValue()
        
        volumeToMeshAndTextureOutputFiberResamplingStep = parameters[
                  'volumeToMeshAndTextureOutputFiberResamplingStep' ].getValue()
        
        volumeToMeshAndTextureFiberTangent = parameters[
                               'volumeToMeshAndTextureFiberTangent' ].getValue()

        volumeToMeshAndTextureMaximumFiberToNormalAngle = parameters[
                  'volumeToMeshAndTextureMaximumFiberToNormalAngle' ].getValue()
        if( volumeToMeshAndTextureFiberTangent == 0 ):

          volumeToMeshAndTextureMaximumFiberToNormalAngle = 0
                                        
        volumeToMeshAndTextureCacheXSize = parameters[
                                 'volumeToMeshAndTextureCacheXSize' ].getValue()
        
        volumeToMeshAndTextureCacheYSize = parameters[
                                 'volumeToMeshAndTextureCacheYSize' ].getValue()
        
        volumeToMeshAndTextureCacheZSize = parameters[
                                 'volumeToMeshAndTextureCacheZSize' ].getValue()

        volumeToMeshAndTextureOffsetForOutputLabel = parameters[
                       'volumeToMeshAndTextureOffsetForOutputLabel' ].getValue()

        volumeToMeshAndTextureSmoothConnectivityMap = parameters[
                      'volumeToMeshAndTextureSmoothConnectivityMap' ].getValue()
        
        volumeToMeshAndTextureStandardDeviationOfSmoothing = parameters[
               'volumeToMeshAndTextureStandardDeviationOfSmoothing' ].getValue()
                    
        # Fill arguments
        argumentV1 = fileNameVolumeToMeshAndTexture_ROIVolume
        
        argumentTR1 = fileNameVolumeToMeshAndTexture_ROIToBundleMapTransform
        
        argumentM2 = fileNameVolumeToMeshAndTexture_ROIMesh
        
        argumentT2 = fileNameVolumeToMeshAndTexture_ROITexture
        
        argumentTR2 = \
               fileNameVolumeToMeshAndTexture_ROIMeshAndTextToBundleMapTransform
        
        argumentScalarParameters = \
          str( volumeToMeshAndTextureMinimumIntersectionLength ) + ' ' + \
          str( volumeToMeshAndTextureInputFiberResamplingStep ) + ' ' + \
          str( volumeToMeshAndTextureMaximumDistanceToMesh ) + ' ' + \
          str( volumeToMeshAndTextureOutputFiberResamplingStep ) + ' ' + \
          str( volumeToMeshAndTextureFiberTangent ) + ' ' + \
          str( volumeToMeshAndTextureSmoothConnectivityMap ) + ' ' + \
          str( volumeToMeshAndTextureStandardDeviationOfSmoothing ) + ' ' + \
          str( volumeToMeshAndTextureCacheXSize ) + ' ' + \
          str( volumeToMeshAndTextureCacheYSize ) + ' ' + \
          str( volumeToMeshAndTextureCacheZSize ) + ' ' + \
          str( volumeToMeshAndTextureOffsetForOutputLabel )
        
        # Set functor parameters                   
        argumentF = 'connectivity-matrix' + ' ' + \
                    'average-length-matrix' + ' ' + \
                    'roi-to-mesh-and-texture-bundle-map' + ' ' + \
                    'roi-to-roi-connectivity-texture-map'
                    
        # Set output file names 
        fileNameAverageLengthMatrix = os.path.join( 
                                                 outputWorkDirectory,
                                                 'average-length-matrix.mat' )
        fileNameOutputBundleMap = os.path.join( outputWorkDirectory,
                                   'roi-to-mesh-and-texture-bundle-map.bundles' )
        fileNameResTexture = os.path.join( outputWorkDirectory,
                                     'roi-to-roi-connectivity-texture-map.tex' )
        
        argumentO = fileNameConnectivityMatrix + ' ' + \
                    fileNameAverageLengthMatrix + ' ' + \
                    fileNameOutputBundleMap + ' ' + \
                    fileNameResTexture
                    
        # Create mesh  
        roiBaseName = os.path.splitext( os.path.basename( \
                                fileNameVolumeToMeshAndTexture_ROIVolume) )[ 0 ]
        
        if ( verbose ):
          
          print 'roiBaseName: ', roiBaseName
          print 'AimsMesh on ROI volume: ', \
                fileNameVolumeToMeshAndTexture_ROIVolume      
        
        directoryTmpROIsMesh = os.path.join( outputWorkDirectory,
                                             'TmpROIsMesh' )
        if ( ( not viewOnly ) and ( not os.path.exists( directoryTmpROIsMesh ) ) ):
        
          os.mkdir( directoryTmpROIsMesh )
        
        outputTmpROIsMesh = os.path.join( directoryTmpROIsMesh, 
                                          str( roiBaseName ) )
        
        command = 'AimsMesh ' + \
                  ' -i ' + fileNameVolumeToMeshAndTexture_ROIVolume + \
                  ' -o ' + outputTmpROIsMesh + \
                  ' --smooth 20'
                  
        executeCommand( self, subjectName, command, viewOnly )

        command = 'GkgExecuteCommand MeshSelectorUsingFileSize ' + \
                  ' -i ' + directoryTmpROIsMesh + \
                  ' -o ' + outputWorkDirectory + \
                  ' -verbose true'
                  
        executeCommand( self, subjectName, command, viewOnly )
        
        functors[ 'condition-notify-and-release' ]( subjectName,
                                                    'mesh-created' )
      
      ##########################################################################
      ############# volume alone ###############################################
      ##########################################################################
          
      elif ( connectivityMatrixType == 'volume alone' ):
          
          # Get ROI sets
          fileNameVolumeAlone_ROIVolume = parameters[ 
                                    'fileNameVolumeAlone_ROIVolume' ].getValue()
          fileNameVolumeAlone_ROIToBundleMapTransform = parameters[ 
                      'fileNameVolumeAlone_ROIToBundleMapTransform' ].getValue()
                  
          # Get Connectivity matrix parameters
          volumeAloneMinimumIntersectionLength = parameters[ 
                             'volumeAloneMinimumIntersectionLength' ].getValue()
          
          volumeAloneInputFiberResamplingStep = parameters[
                              'volumeAloneInputFiberResamplingStep' ].getValue()
          volumeAloneOutputFiberResamplingStep = parameters[
                             'volumeAloneOutputFiberResamplingStep' ].getValue()
          volumeAloneFurtherInterface = parameters[
                                      'volumeAloneFurtherInterface' ].getValue()
          volumeAloneOffsetForOutputLabel = parameters[
                                  'volumeAloneOffsetForOutputLabel' ].getValue()
          volumeAloneMaximumDistanceToMesh = parameters[
                                 'volumeAloneMaximumDistanceToMesh' ].getValue()
          volumeAloneCacheXSize = \
                                parameters[ 'volumeAloneCacheXSize' ].getValue()
          volumeAloneCacheYSize = \
                                parameters[ 'volumeAloneCacheYSize' ].getValue()
          volumeAloneCacheZSize = \
                                parameters[ 'volumeAloneCacheZSize' ].getValue()
                      
          fileNameVolumeAloneFurtherInterface = parameters[ 
                             'fileNameVolumeAlone_FurtherInterface' ].getValue()
                                
          fileNameVolumeAloneInterfaceToBundleTransform = parameters[ 
                   'fileNameVolumeAlone_InterfaceToBundleTransform' ].getValue()


          # Fill arguments
          argumentV1 = fileNameVolumeAlone_ROIVolume
          
          argumentTR1 = fileNameVolumeAlone_ROIToBundleMapTransform
                            
          argumentScalarParameters = \
                     str( volumeAloneMinimumIntersectionLength ) + ' ' + \
                     str( volumeAloneInputFiberResamplingStep ) + ' ' + \
                     str( volumeAloneOutputFiberResamplingStep ) + ' ' + \
                     str( volumeAloneFurtherInterface ) + ' ' + \
                     str( volumeAloneOffsetForOutputLabel ) + ' ' + \
                     str( volumeAloneMaximumDistanceToMesh ) + ' ' + \
                     str( volumeAloneCacheXSize ) + ' ' + \
                     str( volumeAloneCacheYSize ) + ' ' + \
                     str( volumeAloneCacheZSize )

          if ( volumeAloneFurtherInterface == 2 ):
          
            argumentStringParameters = \
                     fileNameVolumeAloneFurtherInterface + ' ' + \
                     fileNameVolumeAloneInterfaceToBundleTransform
          
          # Set functor parameters                   
          argumentF = 'connectivity-matrix' + ' ' + \
                      'average-length-matrix' + ' ' + \
                      'roi-to-roi-bundle-map'
                      
          # Set output file names 
          fileNameAverageLengthMatrix = os.path.join( 
                                                 outputWorkDirectory,
                                                 'average-length-matrix.mat' )
          fileNameOutputBundleMap = os.path.join( \
                                                outputWorkDirectory,
                                                'roi-to-roi-bundle-map.bundles' )
          
          argumentO = fileNameConnectivityMatrix + ' ' + \
                      fileNameAverageLengthMatrix + ' ' + \
                      fileNameOutputBundleMap
          
          # Create Mesh 
          roiBaseName = os.path.splitext( os.path.basename( \
                                          fileNameVolumeAlone_ROIVolume) )[ 0 ]
        
          if ( verbose ):
            
            print 'roiBaseName: ', roiBaseName
            print 'AimsMesh on ROI volume: ', fileNameVolumeAlone_ROIVolume
          
        
          # Create mesh
          directoryTmpROIsMesh = os.path.join( outputWorkDirectory,
                                               'TmpROIsMesh' )
          if ( ( not viewOnly ) and ( not os.path.exists( directoryTmpROIsMesh ) ) ):
        
            os.mkdir( directoryTmpROIsMesh )
 
          outputTmpROIsMesh = os.path.join( directoryTmpROIsMesh,
                                            str( roiBaseName ) )
 
          command = 'AimsMesh ' + \
                    ' -i ' + fileNameVolumeAlone_ROIVolume + \
                    ' -o ' + outputTmpROIsMesh + \
                    ' --smooth 20'
 
          executeCommand( self, subjectName, command, viewOnly )

          command = 'GkgExecuteCommand MeshSelectorUsingFileSize ' + \
                    ' -i ' + directoryTmpROIsMesh + \
                    ' -o ' + outputWorkDirectory + \
                    ' -verbose true'
                  
          executeCommand( self, subjectName, command, viewOnly )
          
          functors[ 'condition-notify-and-release' ]( subjectName,
                                                      'mesh-created' )
          
      else:
        
        print connectivityMatrixType, ' not implemented yet... Nothing done...'
        return None
      
      
      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 10 )
      
      
      # Storing list of bundles
      argumentB = ''
      
      for bundle in fileNameInputBundleMap:
        
        argumentB += bundle
        argumentB += ' '
      
      # Start computation of connectivity matrix
      command = 'GkgExecuteCommand DwiConnectivityMatrix' 
                
      if ( len( argumentB ) ):          
        command += ' -b ' + argumentB
      if ( len( argumentV1 ) ):          
        command += ' -v1 ' + argumentV1
      if ( len( argumentM1 ) ):          
        command += ' -m1 ' + argumentM1
      if ( len( argumentT1 ) ):          
        command += ' -t1 ' + argumentT1
      if ( len( argumentTR1 ) ):          
        command += ' -tr1 ' + argumentTR1
      if ( len( argumentV2 ) ):          
        command += ' -v2 ' + argumentV2
      if ( len( argumentM2 ) ):          
        command += ' -m2 ' + argumentM2
      if ( len( argumentT2 ) ):          
        command += ' -t2 ' + argumentT2
      if ( len( argumentTR2 ) ):          
        command += ' -tr2 ' + argumentTR2
      if ( len( argumentScalarParameters ) ):          
        command += ' -scalarParameters ' + argumentScalarParameters
      if ( len( argumentStringParameters ) ):          
        command += ' -stringParameters ' + argumentStringParameters
      if ( len( argumentF ) ):          
        command += ' -f ' + argumentF
      if ( len( argumentO ) ):          
        command += ' -o ' + argumentO
          
      command += ' -outputBundleMapFormat ' + argumentOutputBundleMapFormat + \
                ' -outputTextureMapFormat ' + argumentOutputTextureMapFormat + \
                ' -verbose true'  


      ########################### start command ################################
      executeCommand( self, subjectName, command, viewOnly )
    
      ####################### notifying display thread #########################
      functors[ 'condition-notify-and-release' ]( \
                                               subjectName,
                                               'connectivity-matrix-processed' )
    
      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 100 )

    except:

      self._functors[ 'task-set-status-as-failed' ]( subjectName,
                                                     sys.exc_info() )

  ##############################################################################
  # display()
  ##############################################################################

  def display( self, functors, parameters, subjectName, verbose,
               isReportModeActive ):

    ########################### resetting the viewer ###########################
    functors[ 'viewer-reset' ]()

    ####################### collecting input info ##############################
    fileNameT1 = parameters[ 'fileNameT1' ].getValue()
    fileNameDwToT1Transform = \
                             parameters[ 'fileNameDwToT1Transform' ].getValue()
    
    # Sanity check
    self.checkIfFilesExist( fileNameT1,
                            fileNameDwToT1Transform )
    
    ####################### collecting output information ######################
    outputBundleMapFormat = parameters[ 'outputBundleMapFormat' ].getChoice()
    outputTextureMapFormat = parameters[ 'outputTextureMapFormat' ].getChoice()
    outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()
    
    ####################### creating referentials ##############################
    functors[ 'viewer-create-referential' ]( 'referentialT1' )
    functors[ 'viewer-create-referential' ]( 'referentialDwi' )
    functors[ 'viewer-create-referential' ]( 'referentialROIs1' )
    functors[ 'viewer-create-referential' ]( 'referentialROIs2' )
    
    # link T1 referential with anatomist talairach referential
    # in order not to have separated referential groups
    identityTransform = [ 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1 ]
    functors[ 'viewer-load-transformation' ]( identityTransform,
                                              'referentialT1', 
                                              'centralRef' )
    
    viewType = 'first_view'

    # Second input ROIs volume file name
    # For future use : no connectivity type implemented yet using it
    argumentV2 = ''
    
    ####################### setting options according to connectivity type #####
    connectivityMatrixType = parameters[ 'connectivityMatrixType' ].getChoice()
    
    if ( verbose ):

      print  'Connectivity matrix type: ', connectivityMatrixType
      
    
    # Common parameters
    bundlesColor = [ 0.1, 0.1, 0.8, 0.3 ]
   
    # Common output file
    fileNameOutputBundleMap = ''
    fileNameResTexture = ''
    fileNameConnectivityMatrix = os.path.join( outputWorkDirectory,
                                               'connectivity-matrix.mat' )
      
    ############################################################################
    ############# mesh-and-texture alone #######################################
    ############################################################################
      
    if ( connectivityMatrixType == 'mesh-and-texture alone' ):
    
        # Get ROI sets
        fileNameMeshAndTextureAlone_ROIMesh = parameters[ 
                              'fileNameMeshAndTextureAlone_ROIMesh' ].getValue()
        fileNameMeshAndTextureAlone_ROITexture = parameters[ 
                           'fileNameMeshAndTextureAlone_ROITexture' ].getValue()
        fileNameMeshAndTextureAlone_ROIToBundleMapTransform = parameters[ 
              'fileNameMeshAndTextureAlone_ROIToBundleMapTransform' ].getValue()
                
        # Sanity check
        self.checkIfFilesExist( 
                           fileNameMeshAndTextureAlone_ROIMesh, 
                           fileNameMeshAndTextureAlone_ROITexture,
                           fileNameMeshAndTextureAlone_ROIToBundleMapTransform )
              
        fileNameOutputBundleMap = os.path.join( outputWorkDirectory,
                                                'roi-to-roi-bundle-map.bundles' )
        
        fileNameResTexture = os.path.join( outputWorkDirectory,
                                           'roi-to-mesh-texture-map.tex' )
        
        # Display fusion of mesh and texture
        functors[ 'viewer-load-object' ]( fileNameMeshAndTextureAlone_ROIMesh,
                                          'roiMesh' )
        functors[ 'viewer-assign-referential-to-object' ]( 'referentialROIs1',
                                                           'roiMesh' )
        
        functors[ 'viewer-load-object' ]( 
                                         fileNameMeshAndTextureAlone_ROITexture,
                                         'roiTexture' )
        functors[ 'viewer-assign-referential-to-object' ]( 'referentialROIs1',
                                                           'roiTexture' )

        functors[ 'viewer-load-transformation' ]( 
                            fileNameMeshAndTextureAlone_ROIToBundleMapTransform,
                            'referentialROIs1', 
                            'referentialDwi' )
        functors[ 'viewer-assign-referential-to-window' ]( 'referentialT1',
                                                           viewType, 
                                                           'ROIs-window' )
        functors[ 'viewer-fusion-objects' ]( [ 'roiMesh', 'roiTexture'],
                                             'fusionMeshAndTexture',
                                             'FusionTexSurfMethod' )
        
        functors[ 'viewer-add-object-to-window' ]( 'fusionMeshAndTexture',
                                                   viewType,
                                                   'ROIs-window' )
        
        # Add T1 to Tractography window if available
        if ( len( fileNameT1 ) and len( fileNameDwToT1Transform )  ):
          
          functors[ 'viewer-load-object' ]( fileNameT1, 'T1' )
          functors[ 'viewer-assign-referential-to-object' ]( 'referentialT1',
                                                             'T1' )
          functors[ 'viewer-load-transformation' ]( fileNameDwToT1Transform,
                                                    'referentialDwi', 
                                                    'referentialT1' )
          
          functors[ 'viewer-assign-referential-to-window' ]( 
                                                         'referentialT1',
                                                         viewType,
                                                         'Tractography-window' )
          
          functors[ 'viewer-add-object-to-window' ]( 'T1',
                                                     viewType,
                                                     'Tractography-window' )
        
        
    ############################################################################
    ############# volume to mesh ###############################################
    ############################################################################
    
    elif ( connectivityMatrixType == 'volume to mesh' ):
        
      # Get ROI sets
      fileNameVolumeToMesh_ROIVolume = parameters[ 
                                   'fileNameVolumeToMesh_ROIVolume' ].getValue()
      
      fileNameVolumeToMesh_ROIToBundleMapTransform = parameters[ 
                     'fileNameVolumeToMesh_ROIToBundleMapTransform' ].getValue()
      
      fileNameVolumeToMesh_ROIMesh = parameters[ 
                                     'fileNameVolumeToMesh_ROIMesh' ].getValue()
      
      fileNameVolumeToMesh_ROIMeshToBundleMapTransform = parameters[ 
                 'fileNameVolumeToMesh_ROIMeshToBundleMapTransform' ].getValue()
      
      # Sanity check
      self.checkIfFilesExist( fileNameVolumeToMesh_ROIVolume, 
                              fileNameVolumeToMesh_ROIToBundleMapTransform,
                              fileNameVolumeToMesh_ROIMesh,
                              fileNameVolumeToMesh_ROIMeshToBundleMapTransform )
      
      # Load transformations
      functors[ 'viewer-load-transformation' ](
                                   fileNameVolumeToMesh_ROIToBundleMapTransform,
                                   'referentialROIs1',
                                   'referentialDwi' )
      
      functors[ 'viewer-load-transformation' ](
                               fileNameVolumeToMesh_ROIMeshToBundleMapTransform,
                               'referentialROIs2',
                               'referentialDwi' )
      
      fileNameOutputBundleMap = os.path.join( outputWorkDirectory,
                                              'roi-to-mesh-bundle-map.bundles' )
      
      fileNameResTexture = os.path.join( outputWorkDirectory,
                                         'roi-to-roi-texture-map.tex' )
                  
      # Add input mesh to view
      functors[ 'viewer-load-object' ]( fileNameVolumeToMesh_ROIMesh, 
                                        'roiMesh' )
      functors[ 'viewer-assign-referential-to-object' ]( 'referentialT1',
                                                          'roiMesh' )
      functors[ 'viewer-add-object-to-window' ]( 'roiMesh',
                                                 viewType,
                                                 'Tractography-window' )
      functors[ 'viewer-assign-referential-to-window' ]( 'referentialDwi',
                                                         viewType,
                                                        'Tractography-window' 
                                                        )
  
      # Create Mesh 
      roiBaseName = os.path.splitext( os.path.basename( \
                                          fileNameVolumeToMesh_ROIVolume) )[ 0 ]
      
      if ( verbose ):
        
        print 'roiBaseName: ', roiBaseName
        print 'AimsMesh on ROI volume: ', fileNameVolumeToMesh_ROIVolume
      
      
      ####################### waiting for result ###############################
      functors[ 'condition-wait-and-release' ]( subjectName, 'mesh-created' )
      
      # Add all ROIs to window
      regularExpressionROIsCreatedByAimsMesh = \
        os.path.join( os.path.join( outputWorkDirectory, roiBaseName ) + '*' )
      listOfROIsCreatedByAimsMesh = \
        glob.glob( regularExpressionROIsCreatedByAimsMesh )
      listOfROIsCreatedByAimsMesh.sort()
        
        
      # Add T1 to Rois window if available
      if ( len( fileNameT1 ) and len( fileNameDwToT1Transform )  ):
        
        functors[ 'viewer-load-object' ]( fileNameT1, 'T1' )
        functors[ 'viewer-assign-referential-to-object' ]( 'referentialT1', 
                                                           'T1' )
        functors[ 'viewer-load-transformation' ]( fileNameDwToT1Transform,
                                                  'referentialDwi', 
                                                  'referentialT1' )
        functors[ 'viewer-assign-referential-to-window' ]( 'referentialT1',
                                                           viewType, 
                                                           'ROIs-window' )
        functors[ 'viewer-add-object-to-window' ]( 'T1',
                                                   viewType,
                                                   'ROIs-window' )
        
        functors[ 'viewer-adjust-camera' ]( viewType, 'ROIs-window' )
        
      # Create roi to mesh hierarchy
      labelsAndMeshes = self.findListOfLabelsAndMeshes( outputWorkDirectory, 
                                                        roiBaseName,
                                                        verbose )
      listOfLabels = labelsAndMeshes[ 0 ]
      listOfMeshes = labelsAndMeshes[ 1 ]  
        
      # Add ROI to view (filter extensions...)
      numRoi = 0
      for roi in listOfROIsCreatedByAimsMesh:
        
        fileExtension = os.path.splitext( roi )[ 1 ]
        
        if ( fileExtension != '.dim' ) and ( fileExtension != '.minf' ):
          
          functors[ 'viewer-load-object' ]( roi, listOfMeshes[ numRoi ] )
          numRoi += 1
      
          
      # Create graph and assign object to 
      functors[ 'viewer-create-roi-graph' ]( 'roiGraph' )
  
      # All mesh will be represented with the same color
      listOfColors = []
      for i in range ( 0, len( listOfMeshes ) ):
      
        listOfColors.append( [ randint( 0, 255 ),
                               randint( 0, 255 ),
                               randint( 0, 255 ) ] )
      
      
      fileNameHierarchyRoi = os.path.join( os.path.join( outputWorkDirectory, 
                                                         'select-rois.hie' ) )
      functors[ 'viewer-create-hierarchy' ]( 'select-roi-hierarchy',
                                             'RoiArg',
                                             listOfMeshes,
                                             listOfColors,
                                             fileNameHierarchyRoi )
      
      # assign every mesh to graph
      for mesh in listOfMeshes:

        functors[ 'viewer-assign-object-to-graph' ]( mesh, 'roiGraph' )
        functors[ 'viewer-assign-referential-to-object' ]( 'referentialROIs1',
                                                           'roiGraph' )
      
      
      # display graph in both windows
      functors[ 'viewer-add-object-to-window' ]( 'roiGraph',
                                                 viewType,
                                                 'ROIs-window',
                                                 addChildren = True,
                                                 addGraphNodes = False,
                                                 addGraphRelations = False )
      
      functors[ 'viewer-add-object-to-window' ]( 'roiGraph',
                                                 viewType,
                                                 'Tractography-window',
                                                 addChildren = True,
                                                 addGraphNodes = False,
                                                 addGraphRelations = False )
      
      functors[ 'viewer-load-object' ]( fileNameHierarchyRoi, 
                                        'hierarchy-roi' )
      functors[ 'viewer-add-object-to-window' ]( 'hierarchy-roi',
                                                 viewType, 
                                                 'browser-rois-window' )
      
    ############################################################################
    ############# volume to mesh-and-texture ###################################
    ############################################################################
        
    elif ( connectivityMatrixType == 'volume to mesh-and-texture' ):
        
      # Get ROI sets
      fileNameVolumeToMeshAndTexture_ROIVolume = parameters[ 
                         'fileNameVolumeToMeshAndTexture_ROIVolume' ].getValue()
      fileNameVolumeToMeshAndTexture_ROIToBundleMapTransform = parameters[ 
           'fileNameVolumeToMeshAndTexture_ROIToBundleMapTransform' ].getValue()
      fileNameVolumeToMeshAndTexture_ROIMesh = parameters[ 
                           'fileNameVolumeToMeshAndTexture_ROIMesh' ].getValue()
      fileNameVolumeToMeshAndTexture_ROITexture = parameters[ 
                        'fileNameVolumeToMeshAndTexture_ROITexture' ].getValue()
      fileNameVolumeToMeshAndTexture_ROIMeshAndTextToBundleMapTransform = \
         parameters[  
         'fileNameVolumeToMeshAndTexture_ROIMeshAndTextToBundleMapTransform' ].\
                                                                      getValue()
                   
      functors[ 'viewer-load-transformation' ]( 
                         fileNameVolumeToMeshAndTexture_ROIToBundleMapTransform,
                         'referentialROIs1', 
                         'referentialDwi' )
      
      functors[ 'viewer-load-transformation' ]( 
              fileNameVolumeToMeshAndTexture_ROIMeshAndTextToBundleMapTransform,
              'referentialROIs2', 
              'referentialDwi' )
                   
      # Sanity check
      self.checkIfFilesExist( 
             fileNameVolumeToMeshAndTexture_ROIVolume,
             fileNameVolumeToMeshAndTexture_ROIToBundleMapTransform, 
             fileNameVolumeToMeshAndTexture_ROIMesh,
             fileNameVolumeToMeshAndTexture_ROITexture,
             fileNameVolumeToMeshAndTexture_ROIMeshAndTextToBundleMapTransform )
            
      fileNameOutputBundleMap = os.path.join( outputWorkDirectory,
                                   'roi-to-mesh-and-texture-bundle-map.bundles' )
      fileNameResTexture = os.path.join( outputWorkDirectory,
                                     'roi-to-roi-connectivity-texture-map.tex' )
  
      # Add input mesh to view
      functors[ 'viewer-load-object' ]( fileNameVolumeToMeshAndTexture_ROIMesh, 
                                        'roiMesh' )
      functors[ 'viewer-assign-referential-to-object' ]( 'referentialT1',
                                                          'roiMesh' )
      functors[ 'viewer-add-object-to-window' ]( 'roiMesh',
                                                 viewType,
                                                 'Tractography-window' )
      functors[ 'viewer-assign-referential-to-window' ]( 'referentialDwi',
                                                         viewType,
                                                        'Tractography-window' )
  
      # Create Mesh 
      roiBaseName = os.path.splitext( os.path.basename( \
                                fileNameVolumeToMeshAndTexture_ROIVolume) )[ 0 ]
      
      if ( verbose ):
        
        print 'roiBaseName: ', roiBaseName
        print 'AimsMesh on ROI volume: ', \
              fileNameVolumeToMeshAndTexture_ROIVolume
      
      ####################### waiting for result ###############################
      functors[ 'condition-wait-and-release' ]( subjectName, 'mesh-created' )

      # Add all ROIs to window
      regularExpressionROIsCreatedByAimsMesh = \
        os.path.join( os.path.join( outputWorkDirectory, roiBaseName) + '*' )
      listOfROIsCreatedByAimsMesh = \
        glob.glob( regularExpressionROIsCreatedByAimsMesh )
      listOfROIsCreatedByAimsMesh.sort()
        
      # Add T1 to Rois window if available
      if ( len( fileNameT1 ) and len( fileNameDwToT1Transform )  ):
        
        functors[ 'viewer-load-object' ]( fileNameT1, 'T1' )
        functors[ 'viewer-assign-referential-to-object' ]( 'referentialT1', 
                                                           'T1' )
        functors[ 'viewer-load-transformation' ]( fileNameDwToT1Transform,
                                                  'referentialDwi', 
                                                  'referentialT1' )
        functors[ 'viewer-assign-referential-to-window' ]( 'referentialT1',
                                                           viewType, 
                                                           'ROIs-window' )
        functors[ 'viewer-add-object-to-window' ]( 'T1',
                                                   viewType,
                                                   'ROIs-window' )
        
        functors[ 'viewer-adjust-camera' ]( viewType, 'ROIs-window' )
        
      # Find list of labels and meshes 
      labelsAndMeshes = self.findListOfLabelsAndMeshes( outputWorkDirectory, 
                                                        roiBaseName,
                                                        verbose )
      listOfLabels = labelsAndMeshes[ 0 ]
      listOfMeshes = labelsAndMeshes[ 1 ]   
       
      # Add ROI to view (filter extensions...)
      numRoi = 0
      for roi in listOfROIsCreatedByAimsMesh:
        
        fileExtension = os.path.splitext( roi )[ 1 ]
        
        if ( fileExtension != '.dim' ) and ( fileExtension != '.minf' ):
          
          functors[ 'viewer-load-object' ]( roi, listOfMeshes[ numRoi ] )
          numRoi += 1
      
      
      # Create rois hierarchy
      fileNameHierarchyRoi = os.path.join( os.path.join( outputWorkDirectory, 
                                                         'select-rois.hie' ) )
  
      # Create graph and assign object to 
      functors[ 'viewer-create-roi-graph' ]( 'roiGraph' )
  
      # All mesh will be represented with the same color
      listOfColors = []
      for i in range ( 0, len( listOfMeshes ) ):
      
        listOfColors.append( [ randint( 0, 255 ),
                               randint( 0, 255 ),
                               randint( 0, 255 ) ] )
      
      
      functors[ 'viewer-create-hierarchy' ]( 'select-roi-hierarchy',
                                             'RoiArg',
                                             listOfMeshes,
                                             listOfColors,
                                             fileNameHierarchyRoi )
      
      for mesh in listOfMeshes:

        functors[ 'viewer-assign-object-to-graph' ]( mesh, 'roiGraph' )
        functors[ 'viewer-assign-referential-to-object' ]( 'referentialROIs1',
                                                           'roiGraph' )
      
      
      functors[ 'viewer-assign-referential-to-object' ]( 'referentialROIs1',
                                                         'roiGraph' )
      
      functors[ 'viewer-add-object-to-window' ]( 'roiGraph',
                                                 viewType,
                                                 'ROIs-window',
                                                 addChildren = True,
                                                 addGraphNodes = False,
                                                 addGraphRelations = False )
      
      functors[ 'viewer-add-object-to-window' ]( 'roiGraph',
                                                 viewType,
                                                 'Tractography-window',
                                                 addChildren = True,
                                                 addGraphNodes = False,
                                                 addGraphRelations = False )
      
      
      functors[ 'viewer-load-object' ]( fileNameHierarchyRoi, 
                                        'hierarchy-roi' )
      functors[ 'viewer-add-object-to-window' ]( 'hierarchy-roi',
                                                 viewType, 
                                                 'browser-rois-window' )
              
    
    ############################################################################
    ############# volume alone #################################################
    ############################################################################
        
    elif ( connectivityMatrixType == 'volume alone' ):
        
        # Get ROI sets
        fileNameVolumeAlone_ROIVolume = parameters[ 
                                    'fileNameVolumeAlone_ROIVolume' ].getValue()
        fileNameVolumeAlone_ROIToBundleMapTransform = parameters[ 
                      'fileNameVolumeAlone_ROIToBundleMapTransform' ].getValue()
                
        functors[ 'viewer-load-transformation' ]( 
                                    fileNameVolumeAlone_ROIToBundleMapTransform,
                                    'referentialROIs1', 
                                    'referentialDwi' )
        
        fileNameOutputBundleMap = os.path.join( outputWorkDirectory,
                                                'roi-to-roi-bundle-map.bundles' )
        
        # Display Mesh 
        roiBaseName = os.path.splitext( os.path.basename( \
                                          fileNameVolumeAlone_ROIVolume) )[ 0 ]
        
        if ( verbose ):
          
          print 'roiBaseName: ', roiBaseName
          print 'AimsMesh on ROI volume: ', fileNameVolumeAlone_ROIVolume
        
        outputROIsMesh = os.path.join( outputWorkDirectory, str( roiBaseName ) )
        
        ####################### waiting for result #############################
        functors[ 'condition-wait-and-release' ]( subjectName, 'mesh-created' )
        
        # List all mesh created
        regularExpressionROIsCreatedByAimsMesh = \
          os.path.join( os.path.join( outputWorkDirectory, roiBaseName ) + '*' )
        listOfROIsCreatedByAimsMesh = \
          glob.glob( regularExpressionROIsCreatedByAimsMesh )
        listOfROIsCreatedByAimsMesh.sort()
          
        if ( verbose ):
        
          print 'List of mesh created: ', listOfROIsCreatedByAimsMesh
          
        # Add T1 to Rois and Tractography windows if available
        if ( len( fileNameT1 ) and len( fileNameDwToT1Transform )  ):
          
          functors[ 'viewer-load-object' ]( fileNameT1, 'T1' )
          functors[ 'viewer-assign-referential-to-object' ]( 'referentialT1',
                                                             'T1' )
          functors[ 'viewer-load-transformation' ]( fileNameDwToT1Transform,
                                                    'referentialDwi',
                                                    'referentialT1' )
          functors[ 'viewer-assign-referential-to-window' ]( 'referentialT1',
                                                             viewType, 
                                                             'ROIs-window' )
          functors[ 'viewer-add-object-to-window' ]( 'T1',
                                                     viewType,
                                                     'ROIs-window' )
          functors[ 'viewer-add-object-to-window' ]( 'T1',
                                                     viewType,
                                                     'Tractography-window' )

          functors[ 'viewer-adjust-camera' ]( viewType, 'ROIs-window' )
          
        # Find list of labels and meshes 
        labelsAndMeshes = self.findListOfLabelsAndMeshes( outputWorkDirectory, 
                                                          roiBaseName,
                                                          verbose )
        listOfLabels = labelsAndMeshes[ 0 ]
        listOfMeshes = labelsAndMeshes[ 1 ]
        
        
        fileNameHierarchyRoi = os.path.join( os.path.join( outputWorkDirectory, 
                                                           'select-rois.hie' ) )
    
        # Create graph and assign object to 
        functors[ 'viewer-create-roi-graph' ]( 'roiGraph' )
    
        # All mesh will be represented with the same color
        listOfColors = []
        for i in range ( 0, len( listOfMeshes ) ):
        
          listOfColors.append( [ randint( 0, 255 ),
                                 randint( 0, 255 ),
                                 randint( 0, 255 ) ] )
        
        
        functors[ 'viewer-create-hierarchy' ]( 'select-roi-hierarchy',
                                               'RoiArg',
                                               listOfMeshes,
                                               listOfColors,
                                               fileNameHierarchyRoi )
        
        # Add ROIs to view (filter extensions...)
        numRoi = 0
        
        for roi in listOfROIsCreatedByAimsMesh:
          
          fileExtension = os.path.splitext( roi )[ 1 ]
          
          if ( fileExtension != '.dim' ) and ( fileExtension != '.minf' ):
            
            functors[ 'viewer-load-object' ]( roi, listOfMeshes[ numRoi ] )
            numRoi += 1
            
        
        for mesh in listOfMeshes:

          functors[ 'viewer-assign-object-to-graph' ]( mesh, 'roiGraph' )
          functors[ 'viewer-assign-referential-to-object' ]( 'referentialROIs1',
                                                             'roiGraph' )
        
        
        functors[ 'viewer-assign-referential-to-object' ]( 'referentialROIs1',
                                                           'roiGraph' )
        
        functors[ 'viewer-add-object-to-window' ]( 'roiGraph',
                                                   viewType,
                                                   'ROIs-window',
                                                   addChildren = True,
                                                   addGraphNodes = False,
                                                   addGraphRelations = False )
        
        functors[ 'viewer-add-object-to-window' ]( 'roiGraph',
                                                   viewType,
                                                   'Tractography-window',
                                                   addChildren = True,
                                                   addGraphNodes = False,
                                                   addGraphRelations = False )
        
        
        functors[ 'viewer-load-object' ]( fileNameHierarchyRoi, 
                                          'hierarchy-roi' )
        functors[ 'viewer-add-object-to-window' ]( 'hierarchy-roi',
                                                   viewType, 
                                                   'browser-rois-window' )
    
    else:
      
      print connectivityMatrixType, ' not implemented yet... Nothing done...'
      return None
    
    
    if ( len( argumentV2 ) ):          
      
      functors[ 'viewer-load-object' ]( argumentV2, 'ROIs2' )
      functors[ 'viewer-assign-referential-to-object' ]( 'referentialROIs2',
                                                         'ROIs2' )
      functors[ 'viewer-add-object-to-window' ]( 'ROIs2',
                                                 viewType,
                                                 'ROIs-window' )
    
    ####################### waiting for result ################################
    functors[ 'condition-wait-and-release' ]( subjectName,
                                              'connectivity-matrix-processed' )
    
    ####################### Display textures if created ########################
    
    # Display fusion of mesh and texture
    if os.path.exists( fileNameResTexture ):
    
      functors[ 'viewer-load-object' ]( fileNameResTexture, 'resTexture' )
      functors[ 'viewer-assign-referential-to-object' ]( 'referentialROIs1',
                                                         'resTexture' )
  
      functors[ 'viewer-fusion-objects' ]( [ 'roiMesh', 'resTexture'],
                                           'fusionResMeshAndTexture',
                                           'FusionTexSurfMethod' )
      
      functors[ 'viewer-add-object-to-window' ]( 'fusionResMeshAndTexture',
                                                 viewType,
                                                 'Tractography-window' )
    
    ####################### Display all output bundles #########################
    
    if ( verbose ):
    
      print 'fileNameOutputBundleMap = ', fileNameOutputBundleMap
    
    if os.path.exists( fileNameOutputBundleMap ):
      
      if ( verbose ):
      
        print 'Display bundles'
      
      # Create bundles hierarchy    
      fileNameHierarchyBundles = os.path.join( os.path.join( 
                                                           outputWorkDirectory, 
                                                        'select-bundles.hie' ) )
  
      # Create and display bundles hierarchy file
      self.createBundlesHierarchyFile( fileNameOutputBundleMap, 
                                       fileNameHierarchyBundles )
      
      # Load hierarchy file
      if os.path.exists( fileNameHierarchyBundles ):
      
        functors[ 'viewer-load-object' ]( fileNameHierarchyBundles , 
                                          'hierarchy' )
        functors[ 'viewer-add-object-to-window' ]( 'hierarchy',
                                                   viewType, 
                                                   'browser-bundles-window' )
      else:
      
        print 'No hierarchy file ', fileNameHierarchyBundles, 'created.'
      
      # Display bundles   
       
      functors[ 'viewer-load-object' ]( fileNameOutputBundleMap, 'bundles' )
      functors[ 'viewer-set-diffuse' ]( 'bundles', bundlesColor )
      functors[ 'viewer-assign-referential-to-object' ]( 'referentialDwi',
                                                         'bundles' )
      functors[ 'viewer-assign-referential-to-window' ]( 'referentialDwi',
                                                         viewType,
                                                         'Tractography-window' )
      functors[ 'viewer-add-object-to-window' ]( 'bundles',
                                                 viewType, 
                                                 'Tractography-window' )
      functors[ 'viewer-set-colormap' ]( 'bundles', 'Blue-Red' )
    
    else:
      
      print 'fileNameOutputBundleMap not found!'    
   
    ####################### Display connectivity matrix ########################
    self.displayConnectivityMatrix( fileNameConnectivityMatrix, functors,
                                    verbose  )
    


  def findListOfLabelsAndMeshes( self, 
                                 directory, roiMeshBaseName, verbose = False ):
      
    dirList = os.listdir( directory )
    
    matchStringBefore = roiMeshBaseName + '_'
    matchStringAfterForLabel = '_'
    matchStringAfterForMesh = '.'
    regExpr = re.compile( matchStringBefore )
    
    listOfLabels = []
    listOfMeshes = []
    
    for filename in dirList:
        
        result = regExpr.match( filename )
        if ( result != None ):
    
          splittedFilename = filename.partition( matchStringBefore )
          
          if ( splittedFilename[ 2 ] != '' ):
    
            
            # Find label value
            labelValue = splittedFilename[ 2 ].partition( 
                                                     matchStringAfterForLabel )
            if ( labelValue[ 0 ] != '' ):
    
               # Search if label already stored, if not a it to the label list
               flagAddLabelToList = True
               for value in listOfLabels:
                 
                 if ( labelValue[ 0 ] == value ):
                   
                   #label already stored
                   flagAddLabelToList = False
                   break
               
               if flagAddLabelToList:
                 
                 listOfLabels.append( labelValue[ 0 ] )
            
            # Find mesh value
            meshValue = splittedFilename[ 2 ].partition( 
                                                      matchStringAfterForMesh )
            if ( meshValue[ 0 ] != '' ):
    
               # Search if label already stored, if not a it to the mesh list
               flagAddMeshToList = True
               for value in listOfMeshes:
                 
                 if ( meshValue[ 0 ] == value ):
                   
                   #label already stored
                   flagAddMeshToList = False
                   break
               
               if flagAddMeshToList:
                 
                 listOfMeshes.append( meshValue[ 0 ] )    
             
     
    # Sort lists
    listOfLabels.sort()
    listOfMeshes.sort()
    
    return listOfLabels, listOfMeshes
  
  
  def generateColors( self, nbOfColors ):
  
    if nbOfColors <= 0:
        return []
  
    # To generate colors, we use the HSL colorspace
    hueStart = 0.0
    hueEnd = 1.0
    saturation = 1.0
    lightness = 0.5
    

    if nbOfColors > 1:
 
      hueValues = [ hueStart + float( i ) * ( hueEnd - hueStart ) / 
                  ( float( nbOfColors ) - 1 ) for i in range( nbOfColors ) ]
    else:

      hueValues = [ hueStart ]  

    listOfRGBColors = []
  
    for hue in hueValues:
      rgbFloat = colorsys.hls_to_rgb( hue, lightness, saturation )
      rgb = [ int( rgbFloat[ 0 ] * 255.0 + 0.5 ),
              int( rgbFloat[ 1 ] * 255.0 + 0.5 ),
              int( rgbFloat[ 2 ] * 255.0 + 0.5 ) ]
      listOfRGBColors += [ rgb ]
  
    return listOfRGBColors
  

  def createBundlesHierarchyFile( self, fileNameBundleMap, OutputFileName ):
     
     
     # Find list of label according to names find in bundlemap file
     listOfLabels = self.extractListOfBundleNames( fileNameBundleMap )
     
     # Sanity check
     if ( len( listOfLabels ) == 0 ):
       
       print 'Empty label list, no hierarchy created'
       return
     
     f = open( OutputFileName, 'w')
     f.write('# tree 1.0\n\n')
     f.write('*BEGIN TREE hierarchy\n')
     f.write('graph_syntax RoiArg\n\n')
     
     # Colors will be assigned to each label
     listOfColors = self.generateColors( len( listOfLabels ) ) 
     colorIndex = 0
     
     # Add all roi to roi name
     for label in listOfLabels:
         
       # Compute new value for color
       R = listOfColors[ colorIndex ][ 0 ]
       G = listOfColors[ colorIndex ][ 1 ]
       B = listOfColors[ colorIndex ][ 2 ]
       
       # Save new tree entry 
       f.write('*BEGIN TREE fold_name\n' )
       f.write( 'name ' + label +'\n' )
       f.write( 'color ' + str( B ) + ' ' + str( G ) + ' ' + str( R ) + 
                '\n' )
       f.write('*END\n\n')
       
       colorIndex += 1
        
     # End of graph
     f.write('*END\n')
     
     f.close()
     
     
  def checkIfFilesExist( self, *args ):

    for file in args:

      if ( file != '' ):

        if ( not os.path.exists( file ) ):
        
          message = '\'' + file + '\' does not exist:\n' + \
                    'Stop here because mandatory file has not been found'
          raise RuntimeError, message
          

  def displayConnectivityMatrix( self, fileNameConnectivityMatrix, functors,
                                 verbose = False ):
  
    if ( verbose ):
      
      print 'Display ConnectivityMatrix...'
      
    # Sanity check
    self.checkIfFilesExist( fileNameConnectivityMatrix )
    
    f = open( fileNameConnectivityMatrix, 'r' )
    fileData = f.read()
    f.close()
    
    fileDataSplitted = fileData.split( ' ' )
    
    # Sanity check
    if( len ( fileDataSplitted ) < 3 ):
      
      print 'Error while reading ', fileName, 'Expecting at least 3 data in it'
    
    # Matrix syntax:
    # dim1 dim2 nbPairs labelx labely value labelx labely value...
    
    firstIndexInList = 3
    nblabelPairs = int( fileDataSplitted[ 2 ] )
    
    xlabels = set()
    ylabels = set()
    
    setw = 9
    nblabelsMaxSize = firstIndexInList + 3 * nblabelPairs
    
    # Find label names
    for i in range( firstIndexInList, nblabelsMaxSize ):
      
      
      dataRead = fileDataSplitted[ i ]
      indexRead = i - firstIndexInList
      
      if ( dataRead == '' ):
        
        continue
      
      backSpaces = ''
      
      # Display progress
      if ( i and i % 100 == 0 ):
        
        for j in range( 0, setw * 2 + 3 ): 
          
          backSpaces += '\b'
        
      
      if ( i % 100 == 0 ):  
      
        sys.stdout.write( backSpaces + '[%*i' % ( setw, i ) + '/' + 
                          '%*i]' % ( setw, nblabelsMaxSize ) )
        sys.stdout.flush();
      
      
      # Find x labels
      if ( indexRead % 3 == 1 ):
        
        xlabels.add( dataRead )
      
      # Find y labels
      elif ( indexRead % 3 == 0 ):
        
        ylabels.add( dataRead )
    
    if ( verbose ):
      
      print 'sort x and y labels'  
    
    # Sort labels using value of the string    
    xlabels = sorted( xlabels, key = lambda v: float( v ) )
    ylabels = sorted( ylabels, key = lambda v: float( v ) )
    
    # create dictionary to quickly get label indexes
    dictXLabelIndexes = {}
    dictYLabelIndexes = {}
    
    num = 0
    for label in xlabels:
      
      dictXLabelIndexes[ label ] = num
      num += 1
    
    num = 0
    for label in ylabels:
      
      dictYLabelIndexes[ label ] = num
      num += 1
    
      
    # Fill matrix with 0 (sparse matrix)
    matrixDims = [ len( ylabels ), len( xlabels ) ]
    dataMatrix = zeros( matrixDims )
    
    # Fill matrix with connectivity values
    
    # matrix indexes
    xIndex = 0
    yIndex = 0
    
    
    # Find label names
    for i in range( firstIndexInList, nblabelsMaxSize ):
      
      # Sanity check
      if ( dataRead == '' ):
        
        continue
      
      # Display progress
      if ( i and i % 100 == 0 ):
        
        for j in range( 0, setw * 2 + 3 ): 
          
          backSpaces += '\b'
        
      
      if ( i % 100 == 0 ):  
      
        sys.stdout.write( backSpaces + '[%*i' % ( setw, i ) + '/' + 
                          '%*i]' % ( setw, nblabelsMaxSize ) )
        sys.stdout.flush();
        
      dataRead = fileDataSplitted[ i ]
      indexRead = i - firstIndexInList
      
      # Get x label index
      if ( indexRead % 3 == 1 ):
            
        xIndex = dictXLabelIndexes[ dataRead ]  
          
      # Get y label index
      elif ( indexRead % 3 == 0 ):
            
        yIndex = dictYLabelIndexes[ dataRead ]
        
      # Get connectivity value
      else:
        
        dataMatrix[ yIndex, xIndex ] = float( dataRead )
  
    
    if ( verbose ):
        
      print 'ConnectivityMatrix', dataMatrix 
      
      
    # Display connectivity matrix
    functors[ 'viewer-add-object-to-matrix-graph' ]( 
                                            'first_view',
                                            'Connectivity-matrix-graph-window',
                                            dataMatrix,
                                            'labels',
                                            'labels',
                                            xlabels,
                                            ylabels )
    
    
  def extractListOfBundleNames( self, fileNameBundleMap, verbose = False ):
  
    if ( verbose ):
      
      print 'extractListOfBundleNames'
      
    # Sanity check
    self.checkIfFilesExist( fileNameBundleMap )
    
    listOfBundleNames = []
    
    f = open( fileNameBundleMap, 'r' )
    fileData = f.read()
    f.close()
    
    matchStringBefore = 'bundles'
    charsToRemove = [ '\'', ' ' ]
    
    pos = fileData.find( matchStringBefore )
    
    if ( pos > 0 ):
      
      subfileData = fileData[ pos + len( matchStringBefore ) : -1 ]
    
      posStart = fileData.find( '[' )
      subfileData = fileData[ posStart + 1 : -1 ]
      
      posEnd = fileData.find( ']' )
      subfileData = fileData[ posStart + 1 : posEnd ]
      
      subfileDataSplitted = subfileData.split( ',' )
      
      # Extract only bundles names and get rid of bundles values
      index = 0
      for i in subfileDataSplitted:
      
        if ( index % 2 == 0 ):
        
          cleanedName = i
          # Remove special characters in name like space, ' ...
          for char in charsToRemove: 
            
            cleanedName = cleanedName.replace( char, '' )
            
          listOfBundleNames.append( cleanedName )
        
        index += 1  
          
    else:
      
      print 'No bundles attribute found in bundles file'
    
    
    # Sort lists
    listOfBundleNames.sort()
    
    if ( verbose ):
      
      print 'List of bundle names found: ', listOfBundleNames
      
      
    return listOfBundleNames
