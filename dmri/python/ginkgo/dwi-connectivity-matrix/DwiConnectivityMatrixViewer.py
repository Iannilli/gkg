from Viewer import *


class DwiConnectivityMatrixViewer( Viewer ):

  def __init__( self, minimumSize, parent ):

    Viewer.__init__( self, minimumSize, parent )

    ########################## First View ######################################

    viewName = 'first_view'
    self.createView( viewName )
    
    self.addAxialWindow( viewName, 0, 0, 2, 2,
                         'ROIs-window', 'ROIs' )
    
    self.add3DWindow( viewName,
                      0, 2, 2, 2,
                      'Tractography-window',
                      'Tractography',
                      [ 0, 0, 0, 1 ],
                      False,
                      'fileNameTractography',
                      'display tractography' )
        
    self.addMatrixGraph( viewName, 2, 2, 2, 2,
                         'Connectivity-matrix-graph-window', 
                         'Connectivity matrix-graph',
                         'labels', 'labels', None )
    
    # Browser window
    self.addBrowserWindow( viewName, 2, 0, 1, 2, 'browser-bundles-window',
                           'Select bundles to display' )
    self.addBrowserWindow( viewName, 3, 0, 1, 2, 'browser-rois-window',
                           'Select rois to display' )
    

def createDwiConnectivityMatrixViewer( minimumSize, parent ):

  return DwiConnectivityMatrixViewer( minimumSize, parent )
