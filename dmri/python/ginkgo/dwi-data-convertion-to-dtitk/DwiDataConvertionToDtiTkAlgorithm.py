from Algorithm import *
from DwiDataConvertionToDtiTkTask import *


class DwiDataConvertionToDtiTkAlgorithm( Algorithm ):

  def __init__( self, verbose ):

    Algorithm.__init__( self, 'DWI-Data-Convertion-To-DtiTk', verbose, True )

    # input data
    self.addParameter( StringParameter( 'fileNameDij', '' ) )
    self.addParameter( StringParameter( 'fileNameTractographyMask', '' ) )
    self.addParameter( StringParameter( 'fileNameRoughMask', '' ) )
    self.addParameter( StringParameter( \
                                  'fileNameTractographyMaskToDijTransformation',
                                  '' ) )

    self.addParameter( BooleanParameter( 'removeTemporaryFiles', 2 ) )


  def launch( self ):

    if ( self._verbose ):

      print 'running Data convertion to DTI-TK'

    task = DwiDataConvertionToDtiTkTask( self._application )
    task.launch( False )


  def view( self ):

    if ( self._verbose ):

      print 'viewing Data convertion to DTI-TK'

    task = DwiDataConvertionToDtiTkTask( self._application )
    task.launch( True )

