from AlgorithmGUI import *
from ResourceManager import *


class DwiDataConvertionToDtiTkAlgorithmGUI( AlgorithmGUI ):

  def __init__( self, algorithm ):

    AlgorithmGUI.__init__( self,
                           algorithm,
                           ResourceManager().getUIFileName( 'dmri',
                                               'DwiDataConvertionToDtiTk.ui' ) )

    ###########################################################################
    # connecting input data
    ###########################################################################

    # Dij
    self.connectStringParameterToLineEdit( 'fileNameDij',
                                           'lineEdit_FileNameDij' )
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit(
                                                       'pushButton_FileNameDij',
                                                       'lineEdit_FileNameDij' )

    # tractography mask
    self.connectStringParameterToLineEdit( 'fileNameTractographyMask',
                                           'lineEdit_FileNameTractographyMask' )
    self.connectFileBrowserToPushButtonAndLineEdit( \
                                          'pushButton_FileNameTractographyMask',
                                          'lineEdit_FileNameTractographyMask' )

    # rough mask
    self.connectStringParameterToLineEdit( 'fileNameRoughMask',
                                           'lineEdit_FileNameRoughMask' )
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit(
                                           'pushButton_FileNameRoughMask',
                                           'lineEdit_FileNameRoughMask' )

    # Mask to dij transformation
    self.connectStringParameterToLineEdit( \
                        'fileNameTractographyMaskToDijTransformation',
                        'lineEdit_FileNameTractographyMaskToDijTransformation' )
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit(
                        'pushButton_FileNameTractographyMaskToDijTransformation',
                        'lineEdit_FileNameTractographyMaskToDijTransformation' )

    # remove temporary files
    self.connectBooleanParameterToCheckBox( 'removeTemporaryFiles',
                                            'checkBox_RemoveTemporaryFiles' )


