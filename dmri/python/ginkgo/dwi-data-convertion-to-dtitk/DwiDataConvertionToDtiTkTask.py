from Task import *
import os
import shutil


class DwiDataConvertionToDtiTkTask( Task ):

  def __init__( self, application ):

    Task.__init__( self, application )

  ##############################################################################
  # execute()
  ##############################################################################

  def execute( self, functors, parameters, subjectName, verbose, viewOnly ):

    try:

      ####################### acquiring conditions #############################
      functors[ 'condition-acquire' ]( subjectName, 'output-dij-converted' )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 0 )

      ####################### collecting output work directory #################
      outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()
      if ( verbose ):

        print  'Output work directory :', outputWorkDirectory

      ####################### collecting input data filename(s) ################
      fileNameDij = parameters[ 'fileNameDij' ].getValue()
      fileNameTractographyMask = parameters[ 'fileNameTractographyMask' ].getValue()
      fileNameRoughMask = parameters[ 'fileNameRoughMask' ].getValue()
      fileNameTractographyMaskToDijTransformation = parameters[ \
                      'fileNameTractographyMaskToDijTransformation' ].getValue()

      temporaryDirectory = os.path.join( outputWorkDirectory, 'tmp' )
      if not ( os.path.exists( temporaryDirectory ) ):

        os.mkdir( temporaryDirectory ) 

      fileNameDxx = os.path.join( temporaryDirectory, 'dxx.ima' )
      fileNameDxy = os.path.join( temporaryDirectory, 'dxy.ima' )
      fileNameDxz = os.path.join( temporaryDirectory, 'dxz.ima' )
      fileNameDyy = os.path.join( temporaryDirectory, 'dyy.ima' )
      fileNameDyz = os.path.join( temporaryDirectory, 'dyz.ima' )
      fileNameDzz = os.path.join( temporaryDirectory, 'dzz.ima' )

      fileNameReorganizedDij = os.path.join( temporaryDirectory,
                                             'dti_dij_reorganized.ima' )
      reorganizedDijInNiftiFormat = os.path.join( temporaryDirectory,
                                                  'dti_dij_reorganized.nii' )
      reorganizedDijScaled = os.path.join( temporaryDirectory,
                                           'dti_dij_reorganized_scaled.nii' )
      fileNameTractographyMaskResampled = os.path.join( temporaryDirectory,
                                             'tractography_mask_resampled.ima' )
      fileNameTractographyMaskResampledInNiftiFormat = \
                               os.path.join( temporaryDirectory,
                                             'tractography_mask_resampled.nii' )
      reorganizedDijScaledBrain = os.path.join( temporaryDirectory,
                                 'dti_dij_reorganized_scaled_brain.nii.gz' )
      reorganizedDijScaledBrainSPD = os.path.join( temporaryDirectory,
                                 'dti_dij_reorganized_scaled_brain_SPD.nii.gz' )
      reorganizedDijScaledBrainSPDAdjusted = os.path.join( outputWorkDirectory,
                                                        'dti_dij_dtitk.nii.gz' )
      reorganizedDijScaledBrainSPDAdjustedInGISFormat = \
                                             os.path.join( outputWorkDirectory,
                                                           'dti_dij_dtitk.ima' )

      ############################ save origin position ########################
      originTrmFile = open( os.path.join( outputWorkDirectory,
                                          'origin.trm' ), 'w' )
      
      imageMinf = fileNameDij + '.minf'
      imageInfo = dict()
      execfile( imageMinf, imageInfo )
      sizeX = imageInfo[ 'attributes' ][ 'sizeX' ]
      sizeY = imageInfo[ 'attributes' ][ 'sizeY' ]
      sizeZ = imageInfo[ 'attributes' ][ 'sizeZ' ]
      resolutionX = imageInfo[ 'attributes' ][ 'resolutionX' ]
      resolutionY = imageInfo[ 'attributes' ][ 'resolutionY' ]
      resolutionZ = imageInfo[ 'attributes' ][ 'resolutionZ' ]

      originTrmFile.write( str( ( sizeX / 2 - 1 ) * resolutionX ) + ' ' )
      originTrmFile.write( str( ( sizeY / 2 ) * resolutionY ) + ' ' )
      originTrmFile.write( str( ( sizeZ / 2 ) * resolutionZ ) )

      originTrmFile.close()

      ################################ processing ##############################
      command = 'GkgExecuteCommand SubVolume ' + \
                ' -i ' + fileNameDij + \
                ' -o ' + fileNameDxx + \
                ' -t 0 -T 0' + \
                ' -verbose true'
      executeCommand( self, subjectName, command, viewOnly )

      command = 'GkgExecuteCommand SubVolume ' + \
                ' -i ' + fileNameDij + \
                ' -o ' + fileNameDxy + \
                ' -t 1 -T 1' + \
                ' -verbose true'
      executeCommand( self, subjectName, command, viewOnly )

      command = 'GkgExecuteCommand SubVolume ' + \
                ' -i ' + fileNameDij + \
                ' -o ' + fileNameDxz + \
                ' -t 2 -T 2' + \
                ' -verbose true'
      executeCommand( self, subjectName, command, viewOnly )

      command = 'GkgExecuteCommand SubVolume ' + \
                ' -i ' + fileNameDij + \
                ' -o ' + fileNameDyy + \
                ' -t 3 -T 3' + \
                ' -verbose true'
      executeCommand( self, subjectName, command, viewOnly )


      command = 'GkgExecuteCommand SubVolume ' + \
                ' -i ' + fileNameDij + \
                ' -o ' + fileNameDyz + \
                ' -t 4 -T 4' + \
                ' -verbose true'
      executeCommand( self, subjectName, command, viewOnly )

      command = 'GkgExecuteCommand SubVolume ' + \
                ' -i ' + fileNameDij + \
                ' -o ' + fileNameDzz + \
                ' -t 5 -T 5' + \
                ' -verbose true'
      executeCommand( self, subjectName, command, viewOnly )

      ########################## updating progress bar #########################
      functors[ 'update-progress-bar' ]( subjectName, 10 )

      command = 'GkgExecuteCommand Cat ' + \
                ' -i ' + fileNameDxx + ' ' + \
                         fileNameDxy + ' ' + \
                         fileNameDyy + ' ' + \
                         fileNameDxz + ' ' + \
                         fileNameDyz + ' ' + \
                         fileNameDzz + ' ' + \
                ' -o ' + fileNameReorganizedDij + \
                ' -t t' + \
                ' -verbose true'
      executeCommand( self, subjectName, command, viewOnly )

      ########################## updating progress bar #########################
      functors[ 'update-progress-bar' ]( subjectName, 20 )

      command = 'GkgExecuteCommand Gis2NiftiConverter  ' + \
                ' -i ' + fileNameReorganizedDij + \
                ' -o ' + reorganizedDijInNiftiFormat + \
                ' -verbose true'
      executeCommand( self, subjectName, command, viewOnly )

      ########################## updating progress bar #########################
      functors[ 'update-progress-bar' ]( subjectName, 30 )


      command = 'TVtool ' + \
                ' -in ' + reorganizedDijInNiftiFormat + \
                ' -out ' + reorganizedDijScaled + \
                ' -scale 1000000000'
      executeCommand( self, subjectName, command, viewOnly )

      ########################## updating progress bar #########################
      functors[ 'update-progress-bar' ]( subjectName, 40 )

      command = 'GkgExecuteCommand Resampling3d ' + \
                ' -reference ' + fileNameTractographyMask + \
                ' -template ' + fileNameRoughMask + \
               ' -transforms ' + fileNameTractographyMaskToDijTransformation + \
                ' -output ' + fileNameTractographyMaskResampled + \
                ' -order 0 ' + \
                ' -verbose true'
      executeCommand( self, subjectName, command, viewOnly )

      ########################## updating progress bar #########################
      functors[ 'update-progress-bar' ]( subjectName, 50 )

      command = 'GkgExecuteCommand Gis2NiftiConverter ' + \
                ' -i ' + fileNameTractographyMaskResampled + \
                ' -o ' + fileNameTractographyMaskResampledInNiftiFormat + \
                ' -verbose true'
      executeCommand( self, subjectName, command, viewOnly )

      ########################## updating progress bar #########################
      functors[ 'update-progress-bar' ]( subjectName, 70 )

      command = 'TVtool ' + \
                ' -in ' + reorganizedDijScaled + \
                ' -out ' + reorganizedDijScaledBrain + \
                ' -mask ' + fileNameTractographyMaskResampledInNiftiFormat
      executeCommand( self, subjectName, command, viewOnly )

      ########################## updating progress bar #########################
      functors[ 'update-progress-bar' ]( subjectName, 80 )

      command = 'TVtool ' + \
                ' -in ' + reorganizedDijScaledBrain + \
                ' -out ' + reorganizedDijScaledBrainSPD + \
                ' -spd'
      executeCommand( self, subjectName, command, viewOnly )

      ########################## updating progress bar #########################
      functors[ 'update-progress-bar' ]( subjectName, 90 )

      command = 'TVAdjustVoxelspace ' + \
                ' -in ' + reorganizedDijScaledBrainSPD + \
                ' -out ' + reorganizedDijScaledBrainSPDAdjusted + \
                ' -origin 0 0 0 '
      executeCommand( self, subjectName, command, viewOnly )

      command = 'AimsFileConvert  ' + \
                ' -i ' + reorganizedDijScaledBrainSPDAdjusted + \
                ' -o ' + reorganizedDijScaledBrainSPDAdjustedInGISFormat
      executeCommand( self, subjectName, command, viewOnly )

      functors[ 'condition-notify-and-release' ]( subjectName,
                                                  'output-dij-converted' )

      if ( parameters[ 'removeTemporaryFiles' ].getValue() ):

          shutil.rmtree( temporaryDirectory )

      ########################## updating progress bar #########################
      functors[ 'update-progress-bar' ]( subjectName, 100 )

    except:

      self._functors[ 'task-set-status-as-failed' ]( subjectName,
                                                     sys.exc_info() )
                                                     

  ##############################################################################
  # display()
  ##############################################################################

  def display( self, functors, parameters, subjectName, verbose,
               isReportModeActive ):

    ####################### collecting output work directory ###################
    outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()
    if ( verbose ):

      print  'Output work directory :', outputWorkDirectory

    ####################### collecting input data filename(s) #################
    fileNameDij = parameters[ 'fileNameDij' ].getValue()
    fileNameTractographyMask = \
                             parameters[ 'fileNameTractographyMask' ].getValue()
    fileNameRoughMask = parameters[ 'fileNameRoughMask' ].getValue()
    fileNameTractographyMaskToDijTransformation = parameters[ \
                      'fileNameTractographyMaskToDijTransformation' ].getValue()

    fileNameDijDtiTk = os.path.join( outputWorkDirectory,
                                     'dti_dij_dtitk.ima' )

    ######################## adding result to viewer ###########################

    view = 'first_view'

    self.viewerSetView( view )

    functors[ 'viewer-create-referential' ]( 'frameDij' )
    functors[ 'viewer-create-referential' ]( 'frameTractographyMask' )
    functors[ 'viewer-load-transformation' ]( \
                                    fileNameTractographyMaskToDijTransformation,
                                    'frameTractographyMask',
                                    'frameDij' )

    ############################# loading input Dij ############################
    functors[ 'viewer-load-object' ]( fileNameDij, 'Dij' )
    functors[ 'viewer-assign-referential-to-object' ]( 'frameDij',
                                                       'Dij' )
    functors[ 'viewer-assign-referential-to-window' ]( 'frameDij',
                                                        view,
                                                       'Dij' )
    functors[ 'viewer-add-object-to-window' ]( 'Dij', view, 'Dij' )
    functors[ 'viewer-set-colormap' ]( 'Dij', 'B-W LINEAR', -2.e-9, 2e-9 )

    ################################# loading mask #############################
    functors[ 'viewer-load-object' ]( fileNameTractographyMask,
                                      'tractographyMask' )
    functors[ 'viewer-assign-referential-to-object' ]( 'frameTractographyMask',
                                                       'tractographyMask' )
    functors[ 'viewer-assign-referential-to-window' ]( 'frameDij',
                                                        view,
                                                       'Tractography mask + Dij' )
    functors[ 'viewer-set-colormap' ]( 'tractographyMask', 'BLUE-lfusion' )
    functors[ 'viewer-fusion-objects' ]( [ 'Dij', 'tractographyMask' ],
                                           'fusionDijAndTractographyMask',
                                           'Fusion2DMethod' )
    functors[ 'viewer-add-object-to-window' ]( 'fusionDijAndTractographyMask',
                                               view,
                                               'Tractography mask + Dij' )

    ############################### loading template ###########################
    functors[ 'viewer-load-object' ]( fileNameRoughMask, 'roughMask' )
    functors[ 'viewer-assign-referential-to-object' ]( 'frameDij',
                                                       'roughMask' )
    functors[ 'viewer-assign-referential-to-window' ]( 'frameDij',
                                                        view,
                                                       'Rough mask + Dij' )
    functors[ 'viewer-set-colormap' ]( 'roughMask', 'BLUE-lfusion' )
    functors[ 'viewer-fusion-objects' ]( [ 'Dij', 'roughMask'],
                                           'fusionDijAndRoughMask',
                                           'Fusion2DMethod' )
    functors[ 'viewer-add-object-to-window' ]( 'fusionDijAndRoughMask',
                                               view,
                                               'Rough mask + Dij' )

    ####################### waiting for result ################################
    functors[ 'condition-wait-and-release' ]( subjectName,
                                              'output-dij-converted' )

    functors[ 'viewer-load-object' ]( fileNameDijDtiTk, 'DtiDtiTk' )
    functors[ 'viewer-assign-referential-to-object' ]( 'frameDij',
                                                       'DtiDtiTk' )
    functors[ 'viewer-assign-referential-to-window' ]( 'frameDij',
                                                        view,
                                                       'Converted Dij' )
    functors[ 'viewer-add-object-to-window' ]( 'DtiDtiTk',
                                               view,
                                               'Converted Dij' )

