from Viewer import *


class DwiDataConvertionToDtiTkViewer( Viewer ):

  def __init__( self, minimumSize, parent ):

    Viewer.__init__( self, minimumSize, parent )

    ########################## First View ######################################

    self.createView( 'first_view' )

    # mask / Dij windows
    self.addAxialWindow( 'first_view', 0, 0, 1, 1,
                         'Tractography mask + Dij', 'Tractography mask + Dij' )

    # rough mask / Dij
    self.addAxialWindow( 'first_view', 0, 1, 1, 1,
                         'Rough mask + Dij', 'Rough mask + Dij' )

    # Dij window
    self.addAxialWindow( 'first_view', 1, 0, 1, 1,
                         'Dij', 'Dij' )

    # converted Dij window
    self.addAxialWindow( 'first_view', 1, 1, 1, 1,
                         'Converted Dij', 'Converted Dij' )


def createDwiDataConvertionToDtiTkViewer( minimumSize, parent ):

  return DwiDataConvertionToDtiTkViewer( minimumSize, parent )

