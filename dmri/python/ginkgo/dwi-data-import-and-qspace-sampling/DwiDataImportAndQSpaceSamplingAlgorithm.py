from Algorithm import *
from DwiDataImportAndQSpaceSamplingTask import *


class DwiDataImportAndQSpaceSamplingAlgorithm( Algorithm ):

  def __init__( self, verbose ):

    Algorithm.__init__( self, 'DWI-Data-Import-And-QSpace-Sampling', verbose,
                        True )

    # diffusion-weighted input data
    self.addParameter( StringParameter( 'fileNameDwi', '' ) )
    self.addParameter( ChoiceParameter( 'sliceAxis',
                                        2,
                                        [ 'X axis',
                                          'Y axis',
                                          'Z axis' ] ) )
    self.addParameter( ChoiceParameter( 'phaseAxis',
                                        1,
                                        [ 'X axis',
                                          'Y axis',
                                          'Z axis' ] ) )
    self.addParameter( ChoiceParameter( 'manufacturer',
                                        3,
                                        [ 'Bruker BioSpin',
                                          'GE HealthCare',
                                          'Philips HealthCare', 
                                          'Siemens HealthCare' ] ) )

    # Q-space sampling
    self.addParameter( ChoiceParameter( \
                             'qSpaceSamplingType',
                             1,
                             [ 'cartesian',
                               'spherical single-shell',
                               'spherical multiple-shell',
                               'arbitrary' ] ) )
                               
    # parameters in case of Siemens/GE/Philips dataset(s)
    self.addParameter( StringParameter( 'bValFileNames', '', True ) )
    self.addParameter( StringParameter( 'bVecFileNames', '', True ) )
    
    # parameters in case of Bruker dataset(s)
    self.addParameter( StringParameter( 'methodFileNames', '', True ) )
    self.addParameter( StringParameter( 'acqpFileNames', '', True ) )
    self.addParameter( StringParameter( 'visuParsFileNames', '', True ) )
    self.addParameter( BooleanParameter( 'applyVisuCoreTransformation', True ) )

    # other parameter(s)                               
    self.addParameter( DoubleParameter( 'bValueThreshold',
                                         10.0, 0.0, 100000.0, 0.1 ) )
    self.addParameter( DoubleParameter( 'bValueStdDev',
                                         10.0, 0.0, 100000.0, 0.1 ) )
    self.addParameter( BooleanParameter( 'invertXAxis', 0 ) )
    self.addParameter( BooleanParameter( 'invertYAxis', 0 ) )
    self.addParameter( BooleanParameter( 'invertZAxis', 0 ) )

    # diffusion gradient characteristics for microstructure models
    self.addParameter( StringParameter( 'gradientCharacteristicsFileNames',
                                        '', True ) )



  def launch( self ):

    if ( self._verbose ):

      print \
        'running DWI Data import and Q-space sampling'

    task = DwiDataImportAndQSpaceSamplingTask( self._application )
    task.launch( False )


  def view( self ):

    if ( self._verbose ):

      print \
        'viewing DWI Data import and Q-space sampling'

    task = DwiDataImportAndQSpaceSamplingTask( self._application )
    task.launch( True )

