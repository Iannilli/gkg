from AlgorithmGUI import *
from ResourceManager import *


class DwiDataImportAndQSpaceSamplingAlgorithmGUI( AlgorithmGUI ):

  def __init__( self, algorithm ):

    AlgorithmGUI.__init__( self,
                           algorithm,
                           ResourceManager().getUIFileName(
                                         'dmri',
                                         'DwiDataImportAndQSpaceSampling.ui' ) )

    ############################################################################
    # connecting input DWI interface
    ############################################################################

    # input DW data file name and push button
    self.connectStringParameterToLineEdit( 'fileNameDwi',
                                           'lineEdit_FileNameDwi' )    
    self.connectFileOrDirectoryListBrowserToPushButtonAndLineEdit(
                                                      'pushButton_FileNameDwi',
                                                      'lineEdit_FileNameDwi' )

    # connecting slice and phase axis interface
    self.connectChoiceParameterToComboBox( 'sliceAxis',
                                           'comboBox_SliceAxis' )
    self.connectChoiceParameterToComboBox( 'phaseAxis',
                                           'comboBox_PhaseAxis' )

    # connecting manufacturer interface
    self.connectChoiceParameterToComboBox( 'manufacturer',
                                           'comboBox_Manufacturer' )
    self.connectComboBoxToCustomCallback( 'comboBox_Manufacturer',
                                          self.setManufacturerParameters )
                                                 

    ############################################################################
    # connecting Q-space sampling interface
    ############################################################################

    # Q-space sampling choice
    self.connectChoiceParameterToComboBox( 'qSpaceSamplingType',
                                           'comboBox_QSpaceSamplingType' )


    # Siemens/GE/Philips parameter(s)
    self.connectStringParameterToLineEdit( 'bValFileNames',
                                           'lineEdit_BValFileNames' )
    self.connectFileListBrowserToPushButtonAndLineEdit( 
                                                     'pushButton_BValFileNames',
                                                     'lineEdit_BValFileNames' )
    self.connectStringParameterToLineEdit( 'bVecFileNames',
                                           'lineEdit_BVecFileNames' )
    self.connectFileListBrowserToPushButtonAndLineEdit( 
                                                     'pushButton_BVecFileNames',
                                                     'lineEdit_BVecFileNames' )

    # Bruker parameter(s)
    self.connectStringParameterToLineEdit( 'methodFileNames',
                                           'lineEdit_MethodFileNames' )
    self.connectFileListBrowserToPushButtonAndLineEdit( 
                                                   'pushButton_MethodFileNames',
                                                   'lineEdit_MethodFileNames' )
    self.connectStringParameterToLineEdit( 'acqpFileNames',
                                           'lineEdit_AcqpFileNames' )
    self.connectFileListBrowserToPushButtonAndLineEdit( 
                                                     'pushButton_AcqpFileNames',
                                                     'lineEdit_AcqpFileNames' )
    self.connectStringParameterToLineEdit( 'visuParsFileNames',
                                           'lineEdit_VisuParsFileNames' )
    self.connectFileListBrowserToPushButtonAndLineEdit( 
                                                 'pushButton_VisuParsFileNames',
                                                 'lineEdit_VisuParsFileNames' )
    self.connectBooleanParameterToCheckBox( \
                                        'applyVisuCoreTransformation',
                                        'checkBox_ApplyVisuCoreTransformation' )

    # other parameter(s)
    self.connectDoubleParameterToSpinBox( 'bValueThreshold',
                                          'doubleSpinBox_BValueThreshold' )
    self.connectDoubleParameterToSpinBox( 'bValueStdDev',
                                          'doubleSpinBox_BValueStdDev' )
    # axis invertions
    self.connectBooleanParameterToCheckBox( 'invertXAxis',
                                            'checkBox_InvertXAxis' )    
    self.connectBooleanParameterToCheckBox( 'invertYAxis',
                                            'checkBox_InvertYAxis' )    
    self.connectBooleanParameterToCheckBox( 'invertZAxis',
                                            'checkBox_InvertZAxis' )    
 
    # diffusion gradient characteristics
    self.connectStringParameterToLineEdit(
                                   'gradientCharacteristicsFileNames',
                                   'lineEdit_GradientCharacteristicsFileNames' )
    self.connectFileListBrowserToPushButtonAndLineEdit(
                                 'pushButton_GradientCharacteristicsFileNames',
                                 'lineEdit_GradientCharacteristicsFileNames' )


  def setManufacturerParameters( self, index ):
  
    stackedWidget = self._findChild( self._awin,
                                     'stackedWidget_ManufacturerParameters' )
    if ( index == 0 ):
    
      stackedWidget.setCurrentIndex( 0 )
      
    else:
    
      stackedWidget.setCurrentIndex( 1 )

