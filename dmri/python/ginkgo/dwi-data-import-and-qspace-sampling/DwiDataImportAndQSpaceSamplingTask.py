from Task import *
from BrukerInformationParser import *
from GkgOrientationAndBValueInformationParser import *
from TrackvisOrientationAndBValueInformationParser import *
from FslOrientationAndBValueInformationParser import *
import shutil

class DwiDataImportAndQSpaceSamplingTask( Task ):

  def __init__( self, application ):

    Task.__init__( self, application )


  ##############################################################################
  # execute()
  ##############################################################################

  def execute( self, functors, parameters, subjectName, verbose, viewOnly ):

    try:

      ####################### acquiring conditions #############################
      functors[ 'condition-acquire' ]( subjectName, 't2-and-dw-processed' )
      functors[ 'condition-acquire' ]( subjectName, 'qspace-processed' )
      functors[ 'condition-acquire' ]( subjectName, 'odf-processed' )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 0 )

      ####################### collecting output work directory #################
      outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()
      if ( verbose ):
      
        print  'Output work directory :', outputWorkDirectory

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 5 )


      ####################### reading all input volumes ########################
      dwiCount, \
      listOfSizeX, listOfSizeY, listOfSizeZ, listOfSizeT, \
      listOfResolutionX, listOfResolutionY, listOfResolutionZ, \
      fileNameDWIs = self.readDWI( parameters,
                                   functors,
                                   outputWorkDirectory,
                                   subjectName,
                                   verbose,
                                   viewOnly )

      ####################### determining size and resolution ##################
      sizeX = listOfSizeX[ 0 ]
      sizeY = listOfSizeY[ 0 ]
      sizeZ = listOfSizeZ[ 0 ]
      resolutionX = listOfResolutionX[ 0 ]
      resolutionY = listOfResolutionY[ 0 ]
      resolutionZ = listOfResolutionZ[ 0 ]

      ####################### sanity checks ####################################
      for n in range( 1, dwiCount ):
      
        if ( ( listOfSizeX[ n ] != sizeX ) or \
             ( listOfSizeY[ n ] != sizeY ) or \
             ( listOfSizeZ[ n ] != sizeZ ) ):
            
          print 'incoherent volume size(s)'
          return

        if ( ( listOfResolutionX[ n ] != resolutionX ) or \
             ( listOfResolutionY[ n ] != resolutionY ) or \
             ( listOfResolutionZ[ n ] != resolutionZ ) ):
            
          print 'incoherent volume resolution(s)'
          return

      ####################### storing information to a dictionary ##############
      axis = [ 'x', 'y', 'z' ]
      acquisitionParameters = {}
      acquisitionParameters[ 'manufacturer' ] = \
                                        parameters[ 'manufacturer' ].getChoice()
      acquisitionParameters[ 'sliceAxis' ] = \
                             axis[ int( parameters[ 'sliceAxis' ].getValue() ) ]
      acquisitionParameters[ 'phaseAxis' ] = \
                             axis[ int( parameters[ 'phaseAxis' ].getValue() ) ]
      acquisitionParameters[ 'dwiCount' ] = int( dwiCount )
      acquisitionParameters[ 'sizeX' ] = int( sizeX )
      acquisitionParameters[ 'sizeY' ] = int( sizeY )
      acquisitionParameters[ 'sizeZ' ] = int( sizeZ )
      acquisitionParameters[ 'resolutionX' ] = float( resolutionX )
      acquisitionParameters[ 'resolutionY' ] = float( resolutionY )
      acquisitionParameters[ 'resolutionZ' ] = float( resolutionZ )

      if ( acquisitionParameters[ 'manufacturer' ] == 'Bruker BioSpin' ):
      
        acquisitionParameters[ 'methodFileNames' ] = \
          parameters[ 'methodFileNames' ].getValue().split( ';' )
        acquisitionParameters[ 'acqpFileNames' ] = \
           parameters[ 'acqpFileNames' ].getValue().split( ';' )
        acquisitionParameters[ 'visuParsFileNames' ] = \
           parameters[ 'visuParsFileNames' ].getValue().split( ';' )
        if ( len( acquisitionParameters[ 'methodFileNames' ] ) != dwiCount ):
             
          message = 'bad method filename count'
          raise RuntimeError, message
        if ( len( acquisitionParameters[ 'acqpFileNames' ] ) != dwiCount ):
             
          message = 'bad acqp filename count'
          raise RuntimeError, message
        if ( len( acquisitionParameters[ 'visuParsFileNames' ] ) != dwiCount ):
             
          message = 'bad visu_pars filename count'
          raise RuntimeError, message

      else:

        acquisitionParameters[ 'bValFileNames' ] = \
          parameters[ 'bValFileNames' ].getValue().split( ';' )
        acquisitionParameters[ 'bVecFileNames' ] = \
          parameters[ 'bVecFileNames' ].getValue().split( ';' )
        if ( len( acquisitionParameters[ 'bValFileNames' ] ) != dwiCount ):
             
          message = 'bad .bval filename count'
          raise RuntimeError, message
        if ( len( acquisitionParameters[ 'bVecFileNames' ] ) != dwiCount ):
             
          message = 'bad .bvec filename count'
          raise RuntimeError, message

      ####################### creating average T2 and DW volume(s) #############

      # collecting b-value thresholed and standard deviation
      bValueThreshold = parameters[ 'bValueThreshold' ].getValue()
      bValueStdDev = parameters[ 'bValueStdDev' ].getValue()

      # preparing list of T2 files, DW files, orientations and b-values
      listOfFileNameT2s = list()
      listOfFileNameDWs = list()
      fileNameT2s = ''
      fileNameDWs = ''
      globalDwOrientations = list()
      globalDwBValues = list()
      globalReceiverGains = list()
    
      maximumBValue = 0
      referenceReceiverGain = 1.0

      # looping over DWI to extract T2, DW, orientations, b-values
      for index in range( 0, dwiCount ):
    
        # filename extension
        extension = str( index + 1 )

        # receiver gain; by default, we assume it is 1.0
        receiverGain = 1.0

        # preparing T2, DW index strings as well as orientation & bvalue lists
        t2Indices = ''
        dwIndices = ''
        dwBValues = list()
        dwOrientations = list()

        # collecting the current b-values and orientations
        currentDwBValues = list()
        currentDwOrientations = list()
        if ( acquisitionParameters[ 'manufacturer' ] == 'Bruker BioSpin' ):

          brukerInformationParser = None
          if ( parameters[ 'applyVisuCoreTransformation' ].getValue() == 2 ):
          
            brukerInformationParser = BrukerInformationParser(
                         acquisitionParameters[ 'methodFileNames' ][ index ],
                         acquisitionParameters[ 'acqpFileNames' ][ index ],
                         acquisitionParameters[ 'visuParsFileNames' ][ index ],
                         True )
                         
          else:
          
            brukerInformationParser = BrukerInformationParser(
                         acquisitionParameters[ 'methodFileNames' ][ index ],
                         acquisitionParameters[ 'acqpFileNames' ][ index ],
                         acquisitionParameters[ 'visuParsFileNames' ][ index ],
                         False )
          
          currentDwBValues = brukerInformationParser.getBValues()
          currentDwOrientations = brukerInformationParser.\
                                              getDiffusionGradientOrientations()
          receiverGain = brukerInformationParser.getReceiverGain()
       
        else:
        
          try:
          
            gkgOrientationAndBValueInformationParser = \
                            GkgOrientationAndBValueInformationParser(
                             acquisitionParameters[ 'bValFileNames' ][ index ],
                             acquisitionParameters[ 'bVecFileNames' ][ index ] )
            currentDwBValues = gkgOrientationAndBValueInformationParser.\
                                                                    getBValues()
            currentDwOrientations = gkgOrientationAndBValueInformationParser.\
                                              getDiffusionGradientOrientations()

          except:
          
            try:
          
              trackvisOrientationAndBValueInformationParser = \
                            TrackvisOrientationAndBValueInformationParser(
                             acquisitionParameters[ 'bValFileNames' ][ index ],
                             acquisitionParameters[ 'bVecFileNames' ][ index ] )
              currentDwBValues = trackvisOrientationAndBValueInformationParser.\
                                                                    getBValues()
              currentDwOrientations = \
                                 trackvisOrientationAndBValueInformationParser.\
                                              getDiffusionGradientOrientations()

            except:
          
              try:
          
                fslOrientationAndBValueInformationParser = \
                            FslOrientationAndBValueInformationParser(
                             acquisitionParameters[ 'bValFileNames' ][ index ],
                             acquisitionParameters[ 'bVecFileNames' ][ index ] )
                currentDwBValues = fslOrientationAndBValueInformationParser.\
                                                                    getBValues()
                currentDwOrientations = \
                                      fslOrientationAndBValueInformationParser.\
                                              getDiffusionGradientOrientations()

              except:

                message = 'unable to find adequate b-value/orientation format'
                raise RuntimeError, message

        # sanity check
        if ( len( currentDwBValues ) != len( currentDwOrientations ) ):
        
          message = 'b-value and orientation list do not have the same length'
          raise RuntimeError, message

        # determining T2 and DW indices
        for dwIndex in range( 0, len( currentDwBValues ) ):
        
          bValue = currentDwBValues[ dwIndex ]
          orientation = currentDwOrientations[ dwIndex ]
          orientationSquareNorm = orientation[ 0 ] * orientation[ 0 ] + \
                                  orientation[ 1 ] * orientation[ 1 ] + \
                                  orientation[ 2 ] * orientation[ 2 ];
                            
          if ( ( bValue < bValueThreshold ) or
               ( orientationSquareNorm < 1e-3 ) ):

            t2Indices += str( dwIndex ) + ' '
        
          else:
      
            dwIndices += str( dwIndex ) + ' '
            dwBValues.append( bValue )
            dwOrientations.append( orientation )

        if ( verbose ):
    
          print 'T2 indices : ', t2Indices      
          print 'DW indices : ', dwIndices
          print 'DW b-values : ', dwBValues
          print 'DW orientations : ', dwOrientations

        # extracting current T2 multiple volume
        t2FileName = os.path.join( outputWorkDirectory, 't2_' + extension )
        if ( verbose ):
 
          print 'extracting T2 volume ', extension, ' : \'', t2FileName, '\''

        command = 'GkgExecuteCommand SubVolume' + \
                  ' -i ' + \
                  fileNameDWIs[ index ] + \
                  ' -o ' + \
                  t2FileName + \
                  ' -tIndices ' + t2Indices + \
                  ' -verbose true'
        executeCommand( self, subjectName, command, viewOnly )

        # extracting current DW volume
        dwFileName = os.path.join( outputWorkDirectory, 'dw_' + extension )
        if ( verbose ):
    
          print 'extracting DW volume ', extension, ' : \'', dwFileName, '\''

        command = 'GkgExecuteCommand SubVolume' + \
                  ' -i ' + \
                  fileNameDWIs[ index ] + \
                  ' -o ' + \
                  dwFileName + \
                  ' -tIndices ' + dwIndices + \
                  ' -verbose true'
        executeCommand( self, subjectName, command, viewOnly )

        # adding current multiple T2, DW volumes, b-values, orientations to 
        # the global list(s)
        listOfFileNameT2s += [ t2FileName ]
        listOfFileNameDWs += [ dwFileName ]
        fileNameT2s += t2FileName + ' '
        fileNameDWs += dwFileName + ' '
        globalDwOrientations += dwOrientations 
        globalDwBValues += dwBValues
        globalReceiverGains += [ receiverGain ]
        for b in dwBValues:
      
          if ( b > maximumBValue ):
        
            maximumBValue = b
            referenceReceiverGain = receiverGain

      # correcting for receiver gains
      for index in range( 0, dwiCount ):
    
        fileNameT2 = listOfFileNameT2s[ index ]
        receiverGain = globalReceiverGains[ index ]
 
        command = 'GkgExecuteCommand Combiner' + \
                  ' -i ' + \
                  fileNameT2 + \
                  ' -o ' + \
                  fileNameT2 + \
                  ' -num1 ' + str( referenceReceiverGain ) + ' 0 ' \
                  ' -den1 ' + str( receiverGain ) + ' 1 ' \
                  ' -verbose true'
        executeCommand( self, subjectName, command, viewOnly )

        fileNameDW = listOfFileNameDWs[ index ]
        receiverGain = globalReceiverGains[ index ]
 
        command = 'GkgExecuteCommand Combiner' + \
                  ' -i ' + \
                  fileNameDW + \
                  ' -o ' + \
                  fileNameDW + \
                  ' -num1 ' + str( referenceReceiverGain ) + ' 0 ' \
                  ' -den1 ' + str( receiverGain ) + ' 1 ' \
                  ' -verbose true'
        executeCommand( self, subjectName, command, viewOnly )

      # merging T2 volume(s) together
      fileNameT2Multiple = os.path.join( outputWorkDirectory, 't2_multiple' )
      command = 'GkgExecuteCommand Cat' + \
                ' -i ' + \
                fileNameT2s + \
                ' -o ' + \
                fileNameT2Multiple + \
                ' -t t' + \
                ' -verbose true'
      executeCommand( self, subjectName, command, viewOnly )

      # averaging T2 volume(s)
      fileNameAverageT2 = os.path.join( outputWorkDirectory, 't2' )
      command = 'GkgExecuteCommand VolumeAverager' + \
                ' -i ' + \
                fileNameT2Multiple + \
                ' -o ' + \
                fileNameAverageT2 + \
                ' -axis t' + \
                ' -verbose true'
      executeCommand( self, subjectName, command, viewOnly )

      # concatenating DW volume(s)
      fileNameDW = os.path.join( outputWorkDirectory, 'dw' )
      command = 'GkgExecuteCommand Cat' + \
                ' -i ' + \
                fileNameDWs + \
                ' -o ' + \
                fileNameDW + \
                ' -t t' + \
                ' -verbose true'
      executeCommand( self, subjectName, command, viewOnly )

      # saving output global b-value file
      if verbose:
    
        print 'global b-value set :'
        print globalDwBValues

      outputBValueFileName = os.path.join( outputWorkDirectory,
                                           'bvalues.txt' )
      f = open( outputBValueFileName, 'w' )
      globalDwBValueCount = len( globalDwBValues )
      f.write( str( globalDwBValueCount ) + '\n' )
      for b in globalDwBValues:
    
        f.write( str( b ) + '\n' )
      f.close()

      # saving output global orientation file
      if verbose:
    
        print 'global orientation set :'
        print globalDwOrientations

      outputOrientationFileName = os.path.join( outputWorkDirectory,
                                                'orientations.txt' )
      f = open( outputOrientationFileName, 'w' )
      globalDwOrientationCount = len( globalDwOrientations )
      f.write( str( globalDwOrientationCount ) + '\n' )
      for orientation in globalDwOrientations:
    
        f.write( str( orientation[ 0 ] ) + ' ' + \
                 str( orientation[ 1 ] ) + ' ' + \
                 str( orientation[ 2 ] ) + '\n' )
      f.close()

      # managing invertion of axis
      inversionMatrix = [ [ 1.0, 0.0, 0.0, 0.0 ],
                          [ 0.0, 1.0, 0.0, 0.0 ],
                          [ 0.0, 0.0, 1.0, 0.0 ],
                          [ 0.0, 0.0, 0.0, 1.0 ] ]
      if ( parameters[ 'invertXAxis' ].getValue() ):
      
        inversionMatrix[ 0 ][ 0 ] = -1.0
        
      if ( parameters[ 'invertYAxis' ].getValue() ):
      
        inversionMatrix[ 1 ][ 1 ] = -1.0

      if ( parameters[ 'invertZAxis' ].getValue() ):
      
        inversionMatrix[ 2 ][ 2 ] = -1.0


      # collecting information relative to diffusion gradient profiles
      gradientCharacteristicsFileNames = \
        parameters[ 'gradientCharacteristicsFileNames' ].getValue().split( ';' )

      gradientCharacteristics = self.getGradientCharacterics(
                                               gradientCharacteristicsFileNames,
                                               outputWorkDirectory )

      # adding q-space to DW volume
      qSpaceSamplingType = parameters[ 'qSpaceSamplingType' ].getValue()
      stringQSpaceSamplingTypes = \
               [ ' cartesian ',
                 ' spherical single-shell custom ',
                 ' spherical multiple-shell different-orientation-sets custom ',
                 ' arbitrary ' ]

      command = 'GkgExecuteCommand DwiQSpaceSampling' + \
                ' -dw ' + \
                fileNameDW + \
                ' -stringParameters ' + \
                stringQSpaceSamplingTypes[ qSpaceSamplingType ] + \
                outputBValueFileName + ' ' + \
                outputOrientationFileName + \
                ' -homogeneousTransform3d ' + \
                str( inversionMatrix[ 0 ][ 0 ] ) + ' ' + \
                str( inversionMatrix[ 0 ][ 1 ] ) + ' ' + \
                str( inversionMatrix[ 0 ][ 2 ] ) + ' ' + \
                str( inversionMatrix[ 0 ][ 3 ] ) + ' ' + \
                str( inversionMatrix[ 1 ][ 0 ] ) + ' ' + \
                str( inversionMatrix[ 1 ][ 1 ] ) + ' ' + \
                str( inversionMatrix[ 1 ][ 2 ] ) + ' ' + \
                str( inversionMatrix[ 1 ][ 3 ] ) + ' ' + \
                str( inversionMatrix[ 2 ][ 0 ] ) + ' ' + \
                str( inversionMatrix[ 2 ][ 1 ] ) + ' ' + \
                str( inversionMatrix[ 2 ][ 2 ] ) + ' ' + \
                str( inversionMatrix[ 2 ][ 3 ] ) + ' ' + \
                str( inversionMatrix[ 3 ][ 0 ] ) + ' ' + \
                str( inversionMatrix[ 3 ][ 1 ] ) + ' ' + \
                str( inversionMatrix[ 3 ][ 2 ] ) + ' ' + \
                str( inversionMatrix[ 3 ][ 3 ] ) + ' ' + \
                ' -gradientCharacteristics ' + gradientCharacteristics + \
                ' -verbose true'
      # in case of spherical multiple-shell type, adding b-value std-dev
      if ( qSpaceSamplingType == 2 ):

        command += ' -scalarParameters ' + str( bValueStdDev )      

      executeCommand( self, subjectName, command, viewOnly )

      # in case of spherical multiple-shell type, also splitting the shells
      if ( qSpaceSamplingType == 2 ):
      
        command = 'GkgExecuteCommand DwiMultipleShellQSpaceSamplingSplitter' + \
                  ' -dw ' + \
                  fileNameDW + \
                  ' -o ' + \
                  fileNameDW + \
                  ' -verbose true'
        executeCommand( self, subjectName, command, viewOnly )

      # sanity check
      isFileExisting = False
      for fileName in os.listdir( os.path.dirname( fileNameDW ) ):

        if( os.path.splitext( os.path.basename( fileName ) )[ 0 ] == \
                      os.path.splitext( os.path.basename( fileNameDW ) )[ 0 ] ):

          isFileExisting = True

      if not( isFileExisting ):
    
        raise RuntimeError, fileNameDW + ': no such file or directory'

      # collecting Q-space point count (ie the T size of the DWI volume)
      command = 'GkgExecuteCommand VolumeInformation' + \
                ' -i ' + \
                fileNameDW + \
                ' -info sizeT '
      fd = os.popen( command )
      stringQSpacePointCount = fd.read()
      ret = fd.close()
      qSpacePointCount = int( stringQSpacePointCount[ : -1 ] )
      
      
      ####################### adding q-space point count to the dictionary #####
      acquisitionParameters[ 'qSpacePointCount' ] = qSpacePointCount
      if ( verbose ):

        print 'acquisition parameters :', acquisitionParameters

      ####################### storing acquisition parameters to disk ###########
      acquisitionParametersFileName = os.path.join( \
                                                   outputWorkDirectory,
                                                   'acquisition_parameters.py' )
      f = open( acquisitionParametersFileName, 'w' )
      f.write( "acquisitionParameters = " + \
               repr( acquisitionParameters ) + '\n' )
      f.close()


      ####################### notifying display thread #########################
      functors[ 'condition-notify-and-release' ]( subjectName,
                                                  't2-and-dw-processed' )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 50 )

      ########################### building Q-space sampling rendering ##########
      fileNameQSpace = os.path.join( outputWorkDirectory, 'qspace' )
      command = 'GkgExecuteCommand DwiQSpaceSamplingRendering' + \
                ' -dw ' + \
                fileNameDW + \
                ' -o ' + \
                fileNameQSpace + \
                ' -verbose true'
      executeCommand( self, subjectName, command, viewOnly )


      ####################### notifying display thread #########################
      functors[ 'condition-notify-and-release' ]( subjectName,
                                                  'qspace-processed' )

      ########################## creating raw RGB map ##########################
      fileNameRGB = os.path.join( outputWorkDirectory, 'dti_rgb' )
      command = 'GkgExecuteCommand DwiTensorField' + \
                ' -t2 ' + fileNameAverageT2 + \
                ' -dw ' + fileNameDW + \
                ' -f rgb ' + \
                ' -o ' + fileNameRGB + \
                ' -verbose true'
      executeCommand( self, subjectName, command, viewOnly )
      
      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 80 )

      ############################# computing odf ##############################
      fileNameSingleShellDW = fileNameDW
    
      # in case of multiple shell q-space sampling, using the highest q shell
      highestShellIndex = 0
      for f in os.listdir( outputWorkDirectory ):
    
      
        if ( ( len( f ) > 8 ) & ( f[ 0 : 7 ] == 'dw_shell' ) ):
      
          shellIndex = string.atoi( f[ 8 : string.find( f, '.' ) ] )
          if ( shellIndex > highestShellIndex ):
        
            highestShellIndex = shellIndex
          
      if ( highestShellIndex > 0 ):
    
        fileNameSingleShellDW = os.path.join( outputWorkDirectory,
                                         'dw_shell' + str( highestShellIndex ) )

      # building analytical QBI ODF map
      fileNameODFTextureMap = os.path.join( outputWorkDirectory,
                                            'aqbi_odf_texture_map.texturemap' )
      fileNameODFSiteMap = os.path.join( outputWorkDirectory,
                                         'aqbi_odf_site_map.sitemap' )
      command = 'GkgExecuteCommand DwiOdfField' + \
                ' -t2 ' + \
                fileNameAverageT2 + \
                ' -dw ' + \
                fileNameSingleShellDW + \
                ' -type aqball_odf_cartesian_field ' + \
                ' -f odf_texture_map odf_site_map ' + \
                ' -o ' + fileNameODFTextureMap + ' ' + fileNameODFSiteMap + \
                ' -verbose true'
      executeCommand( self, subjectName, command, viewOnly )

      ####################### notifying display thread #########################
      functors[ 'condition-notify-and-release' ]( subjectName, 'odf-processed' )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 100 )


    except:

      self._functors[ 'task-set-status-as-failed' ]( subjectName,
                                                     sys.exc_info() )

  ##############################################################################
  # reading all input volumes and obtaining size informations
  ##############################################################################

  def readDWI( self, 
               parameters, 
               functors, 
               outputWorkDirectory, 
               subjectName,
               verbose, 
               viewOnly ):
  
    # collecting DWI data filename(s)
    fileNameDwi = parameters[ 'fileNameDwi' ].getValue()
    inputFileNameDWIs = fileNameDwi.split( ';' )
    if ( verbose ):
    
      print  'input DWI filename(s) :', inputFileNameDWIs
      

    # looping over input file(s)
    dwiCount = len( inputFileNameDWIs )
    listOfSizeX = list()
    listOfSizeY = list()
    listOfSizeZ = list()
    listOfSizeT = list()
    listOfResolutionX = list()
    listOfResolutionY = list()
    listOfResolutionZ = list()
    outputFileNameDWIs = list()
    
    for n in range( 0, dwiCount ):

      inputFileNameDWI = inputFileNameDWIs[ n ]
      
      # rescaling with slope and intercept to float volume
      floatInputFileNameDWI = os.path.join( outputWorkDirectory, 
                                            'dwi_float_' + str( n + 1 ) )

      command = 'GkgExecuteCommand RescalerWithSlopeAndIntercept' + \
                  ' -i ' + \
                  inputFileNameDWI + \
                  ' -o ' + \
                  floatInputFileNameDWI + \
                  ' -verbose true'
      executeCommand( self, subjectName, command, viewOnly )

      # storing to output DWI filename list
      outputFileNameDWIs.append( floatInputFileNameDWI )

      # processing volume size(s)
      command = 'GkgExecuteCommand VolumeInformation' + \
                ' -i ' + \
                floatInputFileNameDWI + \
                ' -info sizeX '

      # sanity check
      isFileExisting = False
      for fileName in os.listdir( os.path.dirname( floatInputFileNameDWI ) ):

        if( os.path.splitext( os.path.basename( fileName ) )[ 0 ] == \
              os.path.splitext( os.path.basename( \
                                   floatInputFileNameDWI ) )[ 0 ] ):

          isFileExisting = True

      if not( isFileExisting ) and not( viewOnly ):
    
        raise RuntimeError, currentFileNameDWI + ': no such file or directory'

      fd = os.popen( command )
      stringSizeX = fd.read()
      ret = fd.close()
      listOfSizeX.append( int( stringSizeX[ : -1 ] ) )

      command = 'GkgExecuteCommand VolumeInformation' + \
                ' -i ' + \
                floatInputFileNameDWI + \
                ' -info sizeY '
      fd = os.popen( command )
      stringSizeY = fd.read()
      ret = fd.close()
      listOfSizeY.append( int( stringSizeY[ : -1 ] ) )

      command = 'GkgExecuteCommand VolumeInformation' + \
                ' -i ' + \
                floatInputFileNameDWI + \
                ' -info sizeZ '
      fd = os.popen( command )
      stringSizeZ = fd.read()
      ret = fd.close()
      listOfSizeZ.append( int( stringSizeZ[ : -1 ] ) )

      command = 'GkgExecuteCommand VolumeInformation' + \
                ' -i ' + \
                floatInputFileNameDWI + \
                ' -info sizeT '
      fd = os.popen( command )
      stringSizeT = fd.read()
      ret = fd.close()
      listOfSizeT.append( int( stringSizeT[ : -1 ] ) )

      command = 'GkgExecuteCommand VolumeInformation' + \
                ' -i ' + \
                floatInputFileNameDWI + \
                ' -info resolutionX '
      fd = os.popen( command )
      stringResolutionX = fd.read()
      ret = fd.close()
      listOfResolutionX.append( float( stringResolutionX[ : -1 ] ) )

      command = 'GkgExecuteCommand VolumeInformation' + \
                ' -i ' + \
                floatInputFileNameDWI + \
                ' -info resolutionY '
      fd = os.popen( command )
      stringResolutionY = fd.read()
      ret = fd.close()
      listOfResolutionY.append( float( stringResolutionY[ : -1 ] ) )

      command = 'GkgExecuteCommand VolumeInformation' + \
                ' -i ' + \
                floatInputFileNameDWI + \
                ' -info resolutionZ '
      fd = os.popen( command )
      stringResolutionZ = fd.read()
      ret = fd.close()
      listOfResolutionZ.append( float( stringResolutionZ[ : -1 ] ) )

    return dwiCount, \
           listOfSizeX, \
           listOfSizeY, \
           listOfSizeZ, \
           listOfSizeT, \
           listOfResolutionX, \
           listOfResolutionY, \
           listOfResolutionZ, \
           outputFileNameDWIs


  ##############################################################################
  # extracting diffusion gradient characteristics
  ##############################################################################

  def getGradientCharacterics( self,
                               gradientCharacteristicsFileNames,
                               outputWorkDirectory ):
                               
    fileType = None
    diffusionScheme = None

    concatenatedGradientCharacteristics = dict()
    
    print 'gradientCharacteristicsFileNames=', gradientCharacteristicsFileNames
                              
    if ( len( gradientCharacteristicsFileNames[ 0 ] ) ):

      concatenatedGradientCharacteristics[ 'PGSE' ] = \
                                                   { 'gradient-magnitudes' : [],
                                                     'little-delta' : [],
                                                     'big-delta' : [] }
                                          

      # looping of diffusion gradient characteristics filename(s)
      for gradientCharacteristicsFileName in gradientCharacteristicsFileNames:

        # reading the gradient characteristics file
        if ( not os.path.exists( gradientCharacteristicsFileName ) ):
 
          message = '\'' + gradientCharacteristicsFileName + '\' file not found'
          raise RuntimeError, message
 
        f = open( gradientCharacteristicsFileName, 'r' )
        gradientCharacteristicsLines = f.readlines()
        f.close()

        # detecting the type of file (self-made dictionary, BRUKER method)
        ########################################################################
        # case of self-made python dictionary
        ########################################################################
        if ( 'attributes' in gradientCharacteristicsLines[ 0 ]  ):

          if ( fileType is None ):
 
            fileType = 'SelfMadePythonDictionary'
 
          else:
 
            if ( fileType != 'SelfMadePythonDictionary' ):

              raise RuntimeError, 'incoherent file types schemes across ' + \
                                  'gradient characteristics file(s)'
 
          globalVariables = dict()
          localVariables = dict()
          execfile( gradientCharacteristicsFileName,
                    globalVariables,
                    localVariables )
          gradientCharacteristics = localVariables[ 'attributes' ]
 
          if ( diffusionScheme is None ):
 
            diffusionScheme = gradientCharacteristics[ 'type' ]
 
          else:
 
            if ( gradientCharacteristics[ 'type' ] != diffusionScheme ):
 
              raise RuntimeError, 'incoherent diffusion schemes across ' + \
                                  'gradient characteristics file(s)'

          if ( diffusionScheme == 'PGSE' ):
 
            concatenatedGradientCharacteristics[ 'PGSE' ] \
                                               [ 'type' ] = 'PGSE'
            concatenatedGradientCharacteristics[ 'PGSE' ] \
                                               [ 'gradient-magnitudes' ] += \
              gradientCharacteristics[ 'gradient-magnitudes' ]
            concatenatedGradientCharacteristics[ 'PGSE' ] \
                                               [ 'little-delta' ] += \
              gradientCharacteristics[ 'little-delta' ]
            concatenatedGradientCharacteristics[ 'PGSE' ] \
                                               [ 'big-delta' ] += \
              gradientCharacteristics[ 'big-delta' ]
                                      
        ########################################################################
        # case of BRUKER method file
        ########################################################################
        elif ( '##TITLE=Parameter' in gradientCharacteristicsLines[ 0 ] ):

          if ( fileType is None ):
 
            fileType = 'BrukerMethod'
 
          else:
 
            if ( fileType != 'BrukerMethod' ):

              raise RuntimeError, 'incoherent file types schemes across ' + \
                                  'gradient characteristics file(s)'


          PVM_DiffPrepMode=None
          PVM_DwGradAmpLineIndex = 0
          PVM_DwGradDurLineIndex = 0
          PVM_DwGradSepLineIndex = 0
          diffusionType=None
          index = 0
          dwiCount = 0
          t2Count = 0
          gammaMHzperT = 42.576
          gradMaxMTperM = 0

          for line in gradientCharacteristicsLines:
 
            if ( '##$PVM_DiffPrepMode=' in line ):
 
              PVM_DiffPrepMode = line.split( '=' )[ 1 ][ : -1 ]
 
            if ( '##$PVM_DwNDiffExp=' in line ):
 
              dwiCount = int( line.split( '=' )[ 1 ] )

            if ( '##$PVM_DwAoImages' in line ):
 
              t2Count = int( line.split( '=' )[ 1 ] )

            if ( '##$PVM_GradCalConst' in line ):
 
              gradMaxHzperMm = float( line.split( '=' )[ 1 ] )

            if ( '##$PVM_DwGradAmp' in line ):
 
              PVM_DwGradAmpLineIndex = index + 1

            if ( '##$PVM_DwGradDur=' in line ):
 
              PVM_DwGradDurLineIndex = index + 1
 
            if ( '##$PVM_DwGradSep=' in line ):
 
              PVM_DwGradSepLineIndex = index + 1

            index += 1
          dwCount = dwiCount - t2Count
          gradMaxMTperM = gradMaxHzperMm/(gammaMHzperT * 1e6) * 1e6
          gradMaxTperM = gradMaxMTperM * 1e-3
 
          PVM_DwGradAmp = float( gradientCharacteristicsLines[ \
                                                      PVM_DwGradAmpLineIndex ] )
          PVM_DwGradDur = float( gradientCharacteristicsLines[ \
                                                      PVM_DwGradDurLineIndex ] )
          PVM_DwGradSep = float( gradientCharacteristicsLines[ \
                                                      PVM_DwGradSepLineIndex ] )


          print 'PVM_DiffPrepMode=', PVM_DiffPrepMode
          print 'dwiCount=', dwiCount
          print 't2Count=', t2Count
          print 'gradMax=', gradMaxMTperM
          print 'gradMaxHzperMm=', gradMaxHzperMm 
          print 'PVM_DwGradAmp=', PVM_DwGradAmp
          print 'PVM_DwGradDur=', PVM_DwGradDur
          print 'PVM_DwGradSep=', PVM_DwGradSep

          if ( diffusionScheme is None ):
 
            if ( PVM_DiffPrepMode == '<SpinEcho>' ):
 
              diffusionType = 'PGSE'
              diffusionScheme = diffusionType
 
          else:
            
            if ( PVM_DiffPrepMode == '<SpinEcho>'):

              diffusionType = 'PGSE'

 
            if ( diffusionType != diffusionScheme ):
 
              raise RuntimeError, 'incoherent diffusion schemes across ' + \
                                  'gradient characteristics file(s)'

          if ( diffusionScheme == 'PGSE' ):
 
            concatenatedGradientCharacteristics[ 'PGSE' ] \
                                               [ 'type' ] = 'PGSE'
            concatenatedGradientCharacteristics[ 'PGSE' ] \
                                               [ 'gradient-magnitudes' ] += \
                                 [ gradMaxTperM * PVM_DwGradAmp/100 ] * dwCount
            concatenatedGradientCharacteristics[ 'PGSE' ] \
                                               [ 'little-delta' ] += \
                                             [ PVM_DwGradDur * 0.001 ] * dwCount
            concatenatedGradientCharacteristics[ 'PGSE' ] \
                                               [ 'big-delta' ] += \
                                             [ PVM_DwGradSep * 0.001 ] * dwCount

    else:
    
      diffusionScheme = 'unknown'
      concatenatedGradientCharacteristics[ 'unknown' ] = dict()
      concatenatedGradientCharacteristics[ 'unknown' ][ 'type' ] = 'unknown'

    gradientFileName = os.path.join( outputWorkDirectory, 'gradients.txt' )
    
    f = open( gradientFileName, 'w' )
    if ( diffusionScheme == 'PGSE' ):
    
      f.write( 'attributes = ' + \
               repr( concatenatedGradientCharacteristics[ 'PGSE' ] ) + '\n' )
               
    else:
    
      f.write( 'attributes = ' + \
               repr( concatenatedGradientCharacteristics[ 'unknown' ] ) + '\n' )

    f.close()
    
    return gradientFileName


  ##############################################################################
  # display()
  ##############################################################################

  def display( self, functors, parameters, subjectName, verbose,
               isReportModeActive ):

    ########################### resetting the viewer ###########################
    functors[ 'viewer-reset' ]()


    ####################### collecting output work directory ###################
    outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()
    if ( verbose ):
    
      print  'Output work directory :', outputWorkDirectory


    ####################### waiting for result ################################
    functors[ 'condition-wait-and-release' ]( subjectName,
                                              't2-and-dw-processed' )


    ####################### displaying T2 and DW volume(s) #####################
    fileNameAverageT2 = os.path.join( outputWorkDirectory, 't2' )
    if ( verbose ):

      print 'reading \'' + fileNameAverageT2 + '\''

    functors[ 'viewer-create-referential' ]( 'frameDW' )

    functors[ 'viewer-load-object' ]( fileNameAverageT2, 'averageT2' )
    functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'averageT2' )

    functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                       'first_view',
                                                       'Average T2(b=0)' )
    functors[ 'viewer-add-object-to-window' ]( 'averageT2',
                                               'first_view',
                                               'Average T2(b=0)' )

    fileNameDW = os.path.join( outputWorkDirectory, 'dw' )
    if ( verbose ):

      print 'reading \'' + fileNameDW + '\''
    functors[ 'viewer-load-object' ]( fileNameDW, 'DW' )
    functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'DW' )

    functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                       'first_view',
                                                       'DW' )
    functors[ 'viewer-add-object-to-window' ]( 'DW', 'first_view', 'DW' )


    ########################### waiting for Q-space sampling rendering #########
    functors[ 'condition-wait-and-release' ]( subjectName, 'qspace-processed' )


    ####################### displaying Q-space #################################
    fileNameQSpace = os.path.join( outputWorkDirectory, 'qspace' )
    if ( verbose ):

      print 'reading \'' + fileNameQSpace + '\''
    functors[ 'viewer-load-object' ]( fileNameQSpace + '_part1.mesh',
                                     'qSpacePart1' )
    functors[ 'viewer-add-object-to-window' ]( 'qSpacePart1', 
                                               'first_view',
                                               'Q-space sampling' )
    functors[ 'viewer-set-diffuse' ]( 'qSpacePart1', [ 1.0, 0.0, 0.0, 1.0 ] )

    if ( os.path.exists( fileNameQSpace + '_part2.mesh' ) ):
    
      functors[ 'viewer-load-object' ]( fileNameQSpace + '_part2.mesh',
                                        'qSpacePart2' )
      functors[ 'viewer-add-object-to-window' ]( 'qSpacePart2', 
                                                 'first_view',
                                                 'Q-space sampling' )  
      functors[ 'viewer-set-diffuse' ]( 'qSpacePart2', [ 0.8, 0.8, 0.8, 0.5 ] )


    ####################### waiting for ODF ####################################
    functors[ 'condition-wait-and-release' ]( subjectName, 'odf-processed' )


    ####################### displaying ODF #####################################
    fileNameODFTextureMap = os.path.join( outputWorkDirectory,
                                          'aqbi_odf_texture_map.texturemap' )
    if ( verbose ):

      print 'reading \'' + fileNameODFTextureMap + '\''

    functors[ 'viewer-load-object' ]( fileNameODFTextureMap, 'ODF field' )
    functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'ODF field' )

    functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',    
                                                       'first_view',
                                                       'QBI ODFs' )
    functors[ 'viewer-add-object-to-window' ]( 'ODF field',
                                               'first_view',
                                               'QBI ODFs' )

