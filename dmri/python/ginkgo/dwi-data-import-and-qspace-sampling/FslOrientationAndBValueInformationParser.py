import os
import sys
import shutil
import math



class FslOrientationAndBValueInformationParser:

  
  def __init__( self, bValFileName, bVecFileName ):
  
  
    f = open( bValFileName, 'r' )
    bValLines = f.readlines()
    f.close()

    f = open( bVecFileName, 'r' )
    bVecLines = f.readlines()
    f.close()

    xCoordinates = bVecLines[ 0 ][ : -1 ].split()
    yCoordinates = bVecLines[ 1 ][ : -1 ].split()
    zCoordinates = bVecLines[ 2 ][ : -1 ].split()
    bValues = bValLines[ 0 ][ : -1 ].split()

    count = len( xCoordinates )
    if ( len( bValues ) != count ):
    
      raise RuntimeError, 'incoherent b-value and orientation files'


    self._bValues = []
    self._diffusionGradientOrientations = []

    for index in range( 0, count ):
    
      self._diffusionGradientOrientations.append( \
                                            [ float( xCoordinates[ index ] ),
                                              float( yCoordinates[ index ] ),
                                              float( zCoordinates[ index ] ) ] )
      self._bValues.append( float( bValues[ index ] ) )


  def getDiffusionGradientOrientations( self ):
  
    return self._diffusionGradientOrientations


  def getBValues( self ):
  
    return self._bValues


  def saveBvecAndBval( self, fileName ):

    f = open( fileName + '.bvec', 'w' )

    for o in self._diffusionGradientOrientations:
 
      f.write( str( o[ 0 ] ) + ' ' )
 
    f.write( '\n' ) 

    for o in self._diffusionGradientOrientations:

      f.write( str( o[ 1 ] ) + ' ' )

    f.write( '\n' ) 

    for o in self._diffusionGradientOrientations:

      f.write( str( o[ 2 ] ) + ' ' )

    f.write( '\n' ) 

    f.close()
    
    f = open( fileName + '.bval', 'w' )

    for b in self._dwEffBValues:
    
      f.write( str( b ) + ' ' )
    
    f.write( '\n' ) 

    f.close()


