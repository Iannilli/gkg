from Algorithm import *
from DwiDtiTemplateConstructionTask import *


class DwiDtiTemplateConstructionAlgorithm( Algorithm ):

  def __init__( self, verbose ):

    Algorithm.__init__( self, 'DWI-Dti-Template-Construction',
                        verbose, False )

    # input data
    self.addParameter( StringParameter( 'directoryNamesDij', '' ) )
    self.addParameter( IntegerParameter( 'sizeX', \
                                          128, 4, 2048, 1 ) )
    self.addParameter( IntegerParameter( 'sizeY', \
                                          128, 4, 2048, 1 ) )
    self.addParameter( IntegerParameter( 'sizeZ', \
                                          128, 4, 2048, 1 ) )
    self.addParameter( DoubleParameter( 'resolutionX', \
                                        1, 0.1, 100, 0.1 ) )
    self.addParameter( DoubleParameter( 'resolutionY', \
                                        1, 0.1, 100, 0.1 ) )
    self.addParameter( DoubleParameter( 'resolutionZ', \
                                        1, 0.1, 100, 0.1 ) )
    self.addParameter( StringParameter( 'fileNameTemplateDij', '' ) )

    self.addParameter( BooleanParameter( 'addOriginTransformation', 2 ) )

    self.addParameter( ChoiceParameter( 'template',
                                        0,
                                        [ 'CONNECT/Archi 18-40yo template',
                                          'custom template' ] ) )


  def launch( self ):

    if ( self._verbose ):

      print \
        'running template creation'

    task = DwiDtiTemplateConstructionTask( self._application )
    task.launch( False )


  def view( self ):

    if ( self._verbose ):

      print \
        'viewing template creation'

    task = DwiDtiTemplateConstructionTask( self._application )
    task.launch( True )

