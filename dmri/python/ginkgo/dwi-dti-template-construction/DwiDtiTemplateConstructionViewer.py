from Viewer import *


class DwiDtiTemplateConstructionViewer( Viewer ):

  def __init__( self, minimumSize, parent ):

    Viewer.__init__( self, minimumSize, parent )

    ########################## First View ######################################

    self.createView( 'first_view' )

    # first dti window
    self.addAxialWindow( 'first_view', 0, 0, 1, 1,
                         'First Dij', 'First Dij' )

    # template window
    self.addAxialWindow( 'first_view', 0, 1, 1, 1,
                         'Template Dij', 'Template Dij' )

    # output dti window
    self.addAxialWindow( 'first_view', 1, 0, 1, 2,
                         'New template Dij', 'New template Dij' )


def createDwiDtiTemplateConstructionViewer( minimumSize, parent ):

  return DwiDtiTemplateConstructionViewer( minimumSize, parent )

