from Algorithm import *
from DwiEddyCurrentAndMotionCorrectionTask import *


class DwiEddyCurrentAndMotionCorrectionAlgorithm( Algorithm ):

  def __init__( self, verbose ):

    Algorithm.__init__( self, 'DWI-Eddy-Current-And-Motion-Correction', verbose,
                        True )

    # diffusion-weighted input data
    self.addParameter( StringParameter( 'rawDwiDirectory', '' ) )
    self.addParameter( StringParameter( 'correctedDwiDirectory', '' ) )
    self.addParameter( StringParameter( 'roughMaskDirectory', '' ) )

    # eddy current correction
    self.addParameter( BooleanParameter( 'eddyCurrentCorrection', False ) )
    self.addParameter( 
      AdvancedParameter(
        'eddyCurrentCorrectionOptions',
        { 'similarityMeasureName':
                      { 'widget': 'comboBox_SimilarityMeasureName',
                        'widgetType': 'comboBox',
                        'defaultValue': 1,
                        'choices': [ 'correlation-coefficient',
                                     'mutual-information',
                                     'normalized-mutual-information',
                                     'correlation-ratio' ]
                      },
          'lowerThreshold':
                      { 'widget': 'doubleSpinBox_LowerThreshold',
                        'widgetType': 'spinBox',
                        'defaultValue': 0.0,
                        'minimumValue': 0.0,
                        'maximumValue': 10000,
                        'incrementValue': 0.01
                      },
          'registrationResamplingOrder':
                      { 'widget': 'spinBox_RegistrationResamplingOrder',
                        'widgetType': 'spinBox',
                        'defaultValue': 1,
                        'minimumValue': 0,
                        'maximumValue': 7,
                        'incrementValue': 1
                      },
          'subSamplingMaximumSizes':
                      { 'widget': 'lineEdit_SubSamplingMaximumSizes',
                        'widgetType': 'lineEdit',
                        'defaultValue': '64'
                      },
          'levelCount':
                      { 'widget': 'spinBox_LevelCount',
                        'widgetType': 'spinBox',
                        'defaultValue': 32,
                        'minimumValue': 0,
                        'maximumValue': 1000,
                        'incrementValue': 1,
                        'dependency': [ 'similarityMeasureName' ],
                        'dependencyValues': [ { 0: False,
                                                1: True,
                                                2: True,
                                                3: True  } ]
                      },
          'applySmoothing':
                      { 'widget': 'checkBox_ApplySmoothing',
                        'widgetType': 'checkBox',
                        'defaultValue': 1,
                        'dependency': [ 'similarityMeasureName' ],
                        'dependencyValues': [ { 0: False,
                                                1: True,
                                                2: True,
                                                3: True  } ]
                      },
          'maximumIterationCount':
                      { 'widget': 'spinBox_MaximumIterationCount',
                        'widgetType': 'spinBox',
                        'defaultValue': 1000,
                        'minimumValue': 0,
                        'maximumValue': 1000,
                        'incrementValue': 1
                      },
          'initialParametersTranslationX':
                      { 'widget': 'doubleSpinBox_InitialParametersTranslationX',
                        'widgetType': 'spinBox',
                        'defaultValue': 0,
                        'minimumValue': -10000,
                        'maximumValue': 10000,
                        'incrementValue': 0.01
                      },
          'initialParametersTranslationY':
                      { 'widget': 'doubleSpinBox_InitialParametersTranslationY',
                        'widgetType': 'spinBox',
                        'defaultValue': 0,
                        'minimumValue': -10000,
                        'maximumValue': 10000,
                        'incrementValue': 0.01
                      },
          'initialParametersTranslationZ':
                      { 'widget': 'doubleSpinBox_InitialParametersTranslationZ',
                        'widgetType': 'spinBox',
                        'defaultValue': 0,
                        'minimumValue': -10000,
                        'maximumValue': 10000,
                        'incrementValue': 0.01
                      },
          'initialParametersRotationX':
                      { 'widget': 'doubleSpinBox_InitialParametersRotationX',
                        'widgetType': 'spinBox',
                        'defaultValue': 0,
                        'minimumValue': 0,
                        'maximumValue': 360,
                        'incrementValue': 0.01
                      },
          'initialParametersRotationY':
                      { 'widget': 'doubleSpinBox_InitialParametersRotationY',
                        'widgetType': 'spinBox',
                        'defaultValue': 0,
                        'minimumValue': 0,
                        'maximumValue': 360,
                        'incrementValue': 0.01
                      },
          'initialParametersRotationZ':
                      { 'widget': 'doubleSpinBox_InitialParametersRotationZ',
                        'widgetType': 'spinBox',
                        'defaultValue': 0,
                        'minimumValue': 0,
                        'maximumValue': 360,
                        'incrementValue': 0.01
                      },
          'initialParametersScalingX':
                      { 'widget': 'doubleSpinBox_InitialParametersScalingX',
                        'widgetType': 'spinBox',
                        'defaultValue': 1.0
                      },
          'initialParametersScalingY':
                      { 'widget': 'doubleSpinBox_InitialParametersScalingY',
                        'widgetType': 'spinBox',
                        'defaultValue': 1.0,
                        'minimumValue': 0.0,
                        'maximumValue': 10000,
                        'incrementValue': 0.01
                      },
          'initialParametersScalingZ':
                      { 'widget': 'doubleSpinBox_InitialParametersScalingZ',
                        'widgetType': 'spinBox',
                        'defaultValue': 1.0,
                        'minimumValue': 0.0,
                        'maximumValue': 10000,
                        'incrementValue': 0.01
                      },
          'initialParametersShearingXY':
                      { 'widget': 'doubleSpinBox_InitialParametersShearingXY',
                        'widgetType': 'spinBox',
                        'defaultValue': 0.0,
                        'minimumValue': 0.0,
                        'maximumValue': 10000,
                        'incrementValue': 0.01
                      },
          'initialParametersShearingXZ':
                      { 'widget': 'doubleSpinBox_InitialParametersShearingXZ',
                        'widgetType': 'spinBox',
                        'defaultValue': 0.0,
                        'minimumValue': 0.0,
                        'maximumValue': 10000,
                        'incrementValue': 0.01
                      },
          'initialParametersShearingYZ':
                      { 'widget': 'doubleSpinBox_InitialParametersShearingYZ',
                        'widgetType': 'spinBox',
                        'defaultValue': 0.0,
                        'minimumValue': 0.0,
                        'maximumValue': 10000,
                        'incrementValue': 0.01
                      },
          'optimizerName':
                      { 'widget': 'comboBox_OptimizerName',
                        'widgetType': 'comboBox',
                        'defaultValue': 0,
                        'choices': [ 'nelder-mead',
                                     'fletcher-reeves' ]
                      },
          'stoppingCriterionError':
                      { 'widget': 'doubleSpinBox_StoppingCriterionError',
                        'widgetType': 'spinBox',
                        'defaultValue': 0.01,
                        'minimumValue': 0.00001,
                        'maximumValue': 10000,
                        'incrementValue': 0.00001,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: True,
                                                1: False } ]
                      },
          'stepSize':
                      { 'widget': 'doubleSpinBox_StepSize',
                        'widgetType': 'spinBox',
                        'defaultValue': 1e-1,
                        'minimumValue': 1e-10,
                        'maximumValue': 100,
                        'incrementValue': 1e-10,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: False,
                                                1: True } ]
                      },
          'maximumTestGradient':
                      { 'widget': 'doubleSpinBox_MaximumTestGradient',
                        'widgetType': 'spinBox',
                        'defaultValue': 1000.0,
                        'minimumValue': 0.0,
                        'maximumValue': 1000000.0,
                        'incrementValue': 1.0,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: False,
                                                1: True } ]
                      },
          'maximumTolerance':
                      { 'widget': 'doubleSpinBox_MaximumTolerance',
                        'widgetType': 'spinBox',
                        'defaultValue': 1e-2,
                        'minimumValue': 1e-10,
                        'maximumValue': 100,
                        'incrementValue': 1e-10,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: False,
                                                1: True } ]
                      },
          'optimizerParametersTranslationX':
                      { 'widget': 'doubleSpinBox_OptimizerParametersTranslationX',
                        'widgetType': 'spinBox',
                        'defaultValue': 2,
                        'minimumValue': -10000,
                        'maximumValue': 10000,
                        'incrementValue': 0.01,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: True,
                                                1: False } ]
                      },
          'optimizerParametersTranslationY':
                      { 'widget': 'doubleSpinBox_OptimizerParametersTranslationY',
                        'widgetType': 'spinBox',
                        'defaultValue': 2,
                        'minimumValue': -10000,
                        'maximumValue': 10000,
                        'incrementValue': 0.01,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: True,
                                                1: False } ]
                      },
          'optimizerParametersTranslationZ':
                      { 'widget': 'doubleSpinBox_OptimizerParametersTranslationZ',
                        'widgetType': 'spinBox',
                        'defaultValue': 2,
                        'minimumValue': -10000,
                        'maximumValue': 10000,
                        'incrementValue': 0.01,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: True,
                                                1: False } ]
                      },
          'optimizerParametersRotationX':
                      { 'widget': 'doubleSpinBox_OptimizerParametersRotationX',
                        'widgetType': 'spinBox',
                        'defaultValue': 2,
                        'minimumValue': 0,
                        'maximumValue': 360,
                        'incrementValue': 0.01,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: True,
                                                1: False } ]
                      },
          'optimizerParametersRotationY':
                      { 'widget': 'doubleSpinBox_OptimizerParametersRotationY',
                        'widgetType': 'spinBox',
                        'defaultValue': 2,
                        'minimumValue': 0,
                        'maximumValue': 360,
                        'incrementValue': 0.01,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: True,
                                                1: False } ]
                      },
          'optimizerParametersRotationZ':
                      { 'widget': 'doubleSpinBox_OptimizerParametersRotationZ',
                        'widgetType': 'spinBox',
                        'defaultValue': 2,
                        'minimumValue': 0,
                        'maximumValue': 360,
                        'incrementValue': 0.01,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: True,
                                                1: False } ]
                      },
          'optimizerParametersScalingX':
                      { 'widget': 'doubleSpinBox_OptimizerParametersScalingX',
                        'widgetType': 'spinBox',
                        'defaultValue': 0.01,
                        'minimumValue': 0.0,
                        'maximumValue': 10000,
                        'incrementValue': 0.01,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: True,
                                                1: False } ]
                      },
          'optimizerParametersScalingY':
                      { 'widget': 'doubleSpinBox_OptimizerParametersScalingY',
                        'widgetType': 'spinBox',
                        'defaultValue': 0.01,
                        'minimumValue': 0.0,
                        'maximumValue': 10000,
                        'incrementValue': 0.01,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: True,
                                                1: False } ]
                      },
          'optimizerParametersScalingZ':
                      { 'widget': 'doubleSpinBox_OptimizerParametersScalingZ',
                        'widgetType': 'spinBox',
                        'defaultValue': 0.01,
                        'minimumValue': 0.0,
                        'maximumValue': 10000,
                        'incrementValue': 0.01,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: True,
                                                1: False } ]
                      },
          'optimizerParametersShearingXY':
                      { 'widget': 'doubleSpinBox_OptimizerParametersShearingXY',
                        'widgetType': 'spinBox',
                        'defaultValue': 0.01,
                        'minimumValue': 0.0,
                        'maximumValue': 10000,
                        'incrementValue': 0.01,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: True,
                                                1: False } ]
                      },
          'optimizerParametersShearingXZ':
                      { 'widget': 'doubleSpinBox_OptimizerParametersShearingXZ',
                        'widgetType': 'spinBox',
                        'defaultValue': 0.01,
                        'minimumValue': 0.0,
                        'maximumValue': 10000,
                        'incrementValue': 0.01,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: True,
                                                1: False } ]
                      },
          'optimizerParametersShearingYZ':
                      { 'widget': 'doubleSpinBox_OptimizerParametersShearingYZ',
                        'widgetType': 'spinBox',
                        'defaultValue': 0.01,
                        'minimumValue': 0.0,
                        'maximumValue': 10000,
                        'incrementValue': 0.01,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: True,
                                                1: False } ]
                      },
          'outputResamplingOrder':
                      { 'widget': 'spinBox_OutputResamplingOrder',
                        'widgetType': 'spinBox',
                        'defaultValue': 3,
                        'minimumValue': 0,
                        'maximumValue': 7,
                        'incrementValue': 1
                      },
          'backgroundResamplingLevel':
                      { 'widget': 'spinBox_BackgroundResamplingLevel',
                        'widgetType': 'spinBox',
                        'defaultValue': 0,
                        'minimumValue': 0,
                        'maximumValue': 10000,
                        'incrementValue': 1
                      }
          #'furtherSliceCountAlongX':
          #            { 'widget': 'spinBox_FurtherSliceCountAlongX',
          #              'widgetType': 'spinBox',
          #              'defaultValue': 0,
          #              'minimumValue': 0,
          #              'maximumValue': 10000,
          #              'incrementValue': 1
          #            },
          #'furtherSliceCountAlongY':
          #            { 'widget': 'spinBox_FurtherSliceCountAlongY',
          #              'widgetType': 'spinBox',
          #              'defaultValue': 0,
          #              'minimumValue': 0,
          #              'maximumValue': 10000,
          #              'incrementValue': 1
          #            },
          #'furtherSliceCountAlongZ':
          #            { 'widget': 'spinBox_FurtherSliceCountAlongZ',
          #              'widgetType': 'spinBox',
          #              'defaultValue': 0,
          #              'minimumValue': 0,
          #              'maximumValue': 10000,
          #             'incrementValue': 1
          #            }

        } ) )


    # motion correction
    self.addParameter( BooleanParameter( 'motionCorrection',False ) )
    self.addParameter( StringParameter( 'fileNameMotionTransform', '' ) )
    self.addParameter( \
      AdvancedParameter( \
        'motionCorrectionOptions',
        { 'similarityMeasureName':
                        { 'widget': 'comboBox_SimilarityMeasureName',
                          'widgetType': 'comboBox',
                          'defaultValue': 1,
                          'choices': [ 'correlation-coefficient',
                                       'mutual-information',
                                       'normalized-mutual-information',
                                       'correlation-ratio' ]
                        },
          'lowerThreshold':
                        { 'widget': 'doubleSpinBox_LowerThreshold',
                          'widgetType': 'spinBox',
                          'defaultValue': 0.0,
                          'minimumValue': 0.0,
                          'maximumValue': 10000,
                          'incrementValue': 0.01
                        },
          'registrationResamplingOrder':
                        { 'widget': 'spinBox_RegistrationResamplingOrder',
                          'widgetType': 'spinBox',
                          'defaultValue': 1,
                          'minimumValue': 0,
                          'maximumValue': 7,
                          'incrementValue': 1
                        },
          'subSamplingMaximumSizes':
                        { 'widget': 'lineEdit_SubSamplingMaximumSizes',
                          'widgetType': 'lineEdit',
                          'defaultValue': '64'
                        },
          'levelCount':
                        { 'widget': 'spinBox_LevelCount',
                          'widgetType': 'spinBox',
                          'defaultValue': 32,
                          'minimumValue': 0,
                          'maximumValue': 1000,
                          'incrementValue': 1,
                          'dependency': [ 'similarityMeasureName' ],
                          'dependencyValues': [ { 0: False,
                                                  1: True,
                                                  2: True,
                                                  3: True } ]
                        },
          'applySmoothing':
                        { 'widget': 'checkBox_ApplySmoothing',
                          'widgetType': 'checkBox',
                          'defaultValue': 1,
                          'dependency': [ 'similarityMeasureName' ],
                          'dependencyValues': [ { 0: False,
                                                  1: True,
                                                  2: True,
                                                  3: True } ]
                        },
          'maximumIterationCount':
                        { 'widget': 'spinBox_MaximumIterationCount',
                          'widgetType': 'spinBox',
                          'defaultValue': 1000,
                          'minimumValue': 0,
                          'maximumValue': 1000,
                          'incrementValue': 1
                        },
          'initialParametersTranslationX':
                        { 'widget': 'doubleSpinBox_InitialParametersTranslationX',
                          'widgetType': 'spinBox',
                          'defaultValue': 0,
                          'minimumValue': -10000,
                          'maximumValue': 10000,
                          'incrementValue': 0.01
                        },
          'initialParametersTranslationY':
                        { 'widget': 'doubleSpinBox_InitialParametersTranslationY',
                          'widgetType': 'spinBox',
                          'defaultValue': 0,
                          'minimumValue': -10000,
                          'maximumValue': 10000,
                          'incrementValue': 0.01
                        },
          'initialParametersTranslationZ':
                        { 'widget': 'doubleSpinBox_InitialParametersTranslationZ',
                          'widgetType': 'spinBox',
                          'defaultValue': 0,
                          'minimumValue': -10000,
                          'maximumValue': 10000,
                          'incrementValue': 0.01
                        },
          'initialParametersRotationX':
                        { 'widget': 'doubleSpinBox_InitialParametersRotationX',
                          'widgetType': 'spinBox',
                          'defaultValue': 0,
                          'minimumValue': 0,
                          'maximumValue': 360,
                          'incrementValue': 0.01
                        },
          'initialParametersRotationY':
                        { 'widget': 'doubleSpinBox_InitialParametersRotationY',
                          'widgetType': 'spinBox',
                          'defaultValue': 0,
                          'minimumValue': 0,
                          'maximumValue': 360,
                          'incrementValue': 0.01
                        },
          'initialParametersRotationZ':
                        { 'widget': 'doubleSpinBox_InitialParametersRotationZ',
                          'widgetType': 'spinBox',
                          'defaultValue': 0,
                          'minimumValue': 0,
                          'maximumValue': 360,
                          'incrementValue': 0.01
                        },
          'optimizerName':
                      { 'widget': 'comboBox_OptimizerName',
                        'widgetType': 'comboBox',
                        'defaultValue': 0,
                        'choices': [ 'nelder-mead',
                                     'fletcher-reeves' ]
                      },
          'stoppingCriterionError':
                      { 'widget': 'doubleSpinBox_StoppingCriterionError',
                        'widgetType': 'spinBox',
                        'defaultValue': 0.01,
                        'minimumValue': 0.00001,
                        'maximumValue': 10000,
                        'incrementValue': 0.00001,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: True,
                                                1: False } ]
                      },
          'stepSize':
                      { 'widget': 'doubleSpinBox_StepSize',
                        'widgetType': 'spinBox',
                          'defaultValue': 1e-1,
                          'minimumValue': 1e-10,
                          'maximumValue': 100,
                          'incrementValue': 1e-10,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: False,
                                                1: True } ]
                      },
          'maximumTestGradient':
                      { 'widget': 'doubleSpinBox_MaximumTestGradient',
                        'widgetType': 'spinBox',
                        'defaultValue': 1000.0,
                        'minimumValue': 0.0,
                        'maximumValue': 1000000.0,
                        'incrementValue': 1.0,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: False,
                                                1: True } ]
                      },
          'maximumTolerance':
                      { 'widget': 'doubleSpinBox_MaximumTolerance',
                        'widgetType': 'spinBox',
                        'defaultValue': 1e-2,
                        'minimumValue': 1e-10,
                        'maximumValue': 100,
                        'incrementValue': 1e-10,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: False,
                                                1: True } ]
                      },
          'optimizerParametersTranslationX':
                        { 'widget': 'doubleSpinBox_OptimizerParametersTranslationX',
                          'widgetType': 'spinBox',
                          'defaultValue': 2,
                          'minimumValue': -10000,
                          'maximumValue': 10000,
                          'incrementValue': 0.01,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: True,
                                                1: False } ]
                        },
          'optimizerParametersTranslationY':
                        { 'widget': 'doubleSpinBox_OptimizerParametersTranslationY',
                          'widgetType': 'spinBox',
                          'defaultValue': 2,
                          'minimumValue': -10000,
                          'maximumValue': 10000,
                          'incrementValue': 0.01,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: True,
                                                1: False } ]
                        },
          'optimizerParametersTranslationZ':
                        { 'widget': 'doubleSpinBox_OptimizerParametersTranslationZ',
                          'widgetType': 'spinBox',
                          'defaultValue': 2,
                          'minimumValue': -10000,
                          'maximumValue': 10000,
                          'incrementValue': 0.01,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: True,
                                                1: False } ]
                        },
          'optimizerParametersRotationX':
                        { 'widget': 'doubleSpinBox_OptimizerParametersRotationX',
                          'widgetType': 'spinBox',
                          'defaultValue': 2,
                          'minimumValue': 0,
                          'maximumValue': 360,
                          'incrementValue': 0.01,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: True,
                                                1: False } ]
                        },
          'optimizerParametersRotationY':
                        { 'widget': 'doubleSpinBox_OptimizerParametersRotationY',
                          'widgetType': 'spinBox',
                          'defaultValue': 2,
                          'minimumValue': 0,
                          'maximumValue': 360,
                          'incrementValue': 0.01,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: True,
                                                1: False } ]
                        },
          'optimizerParametersRotationZ':
                        { 'widget': 'doubleSpinBox_OptimizerParametersRotationZ',
                          'widgetType': 'spinBox',
                          'defaultValue': 2,
                          'minimumValue': 0,
                          'maximumValue': 360,
                          'incrementValue': 0.01,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: True,
                                                1: False } ]
                        },
          'outputResamplingOrder':
                        { 'widget': 'spinBox_OutputResamplingOrder',
                          'widgetType': 'spinBox',
                          'defaultValue': 3,
                          'minimumValue': 0,
                          'maximumValue': 7,
                          'incrementValue': 1
                        },
          'backgroundResamplingLevel':
                        { 'widget': 'spinBox_BackgroundResamplingLevel',
                          'widgetType': 'spinBox',
                          'defaultValue': 0,
                          'minimumValue': 0,
                          'maximumValue': 10000,
                          'incrementValue': 1
                        }
          #'furtherSliceCountAlongX':
          #              { 'widget': 'spinBox_FurtherSliceCountAlongX',
          #                'widgetType': 'spinBox',
          #                'defaultValue': 0,
          #                'minimumValue': 0,
          #                'maximumValue': 10000,
          #                'incrementValue': 1
          #              },
          #'furtherSliceCountAlongY':
          #              { 'widget': 'spinBox_FurtherSliceCountAlongY',
          #                'widgetType': 'spinBox',
          #                'defaultValue': 0,
          #                'minimumValue': 0,
          #                'maximumValue': 10000,
          #                'incrementValue': 1
          #              },
          #'furtherSliceCountAlongZ':
          #              { 'widget': 'spinBox_FurtherSliceCountAlongZ',
          #                'widgetType': 'spinBox',
          #                'defaultValue': 0,
          #                'minimumValue': 0,
          #                'maximumValue': 10000,
          #                'incrementValue': 1
          #              }
        } ) )


  def launch( self ):

    if ( self._verbose ):

      print \
        'running DWI eddy current and motion correction'

    task = DwiEddyCurrentAndMotionCorrectionTask( self._application )
    task.launch( False )


  def view( self ):

    if ( self._verbose ):

      print \
        'viewing DWI eddy current and motion correction'

    task = DwiEddyCurrentAndMotionCorrectionTask( self._application )
    task.launch( True )

