from AlgorithmGUI import *
from ResourceManager import *


class DwiEddyCurrentAndMotionCorrectionAlgorithmGUI( AlgorithmGUI ):

  def __init__( self, algorithm ):

    AlgorithmGUI.__init__( self,
                           algorithm,
                           ResourceManager().getUIFileName(
                                      'dmri',
                                      'DwiEddyCurrentAndMotionCorrection.ui' ) )

    ############################################################################
    # connecting input directory interface
    ############################################################################

    # raw DW data directory interface
    self.connectStringParameterToLineEdit( 'rawDwiDirectory',
                                           'lineEdit_RawDwiDirectory' )    
    self.connectDirectoryBrowserToPushButtonAndLineEdit(
                                                   'pushButton_RawDwiDirectory',
                                                   'lineEdit_RawDwiDirectory' )

    # corrected DW data directory interface
    self.connectStringParameterToLineEdit( 'correctedDwiDirectory',
                                           'lineEdit_CorrectedDwiDirectory' )
    self.connectDirectoryBrowserToPushButtonAndLineEdit(
                                             'pushButton_CorrectedDwiDirectory',
                                             'lineEdit_CorrectedDwiDirectory' )

    # rough mask directory interface
    self.connectStringParameterToLineEdit( 'roughMaskDirectory',
                                           'lineEdit_RoughMaskDirectory' )    
    self.connectDirectoryBrowserToPushButtonAndLineEdit(
                                                'pushButton_RoughMaskDirectory',
                                                'lineEdit_RoughMaskDirectory' )

    ############################################################################
    # connecting eddy current and motion correction parameters
    ############################################################################

    self.connectBooleanParameterToCheckBox( 'motionCorrection',
                                            'checkBox_MotionCorrection' )
    self.connectBooleanParameterToCheckBox( 'eddyCurrentCorrection',
                                            'checkBox_EddyCurrentCorrection' )

    self.connectCheckBoxToCustomCallback( 'checkBox_EddyCurrentCorrection',
                                           self.enableMotionCorrection )
    self.connectCheckBoxToCustomCallback( 'checkBox_MotionCorrection',
                                           self.setChecked )
    self._motionCorrectionOptions = \
                              self.connectPushButtonToAdvancedParameterWidget( \
                              'pushButton_MotionCorrectionOptions',
                               ResourceManager().getUIFileName( 'dmri',
                               'MotionCorrectionSettings.ui' ),
                               'motionCorrectionOptions' )

    self._eddyCurrentCorrectionOptions = \
                              self.connectPushButtonToAdvancedParameterWidget( \
                              'pushButton_EddyCurrentCorrectionOptions',
                              ResourceManager().getUIFileName( 'dmri',
                              'EddyCurrentCorrectionSettings.ui' ),
                              'eddyCurrentCorrectionOptions' )

    self.connectStringParameterToLineEdit( 'fileNameMotionTransform',
                                            'lineEdit_FileNameMotionTransform' )
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit(
                                           'pushButton_FileNameMotionTransform',
                                           'lineEdit_FileNameMotionTransform' )


  def enableMotionCorrection( self, value ):

    widget = self._findChild( self._awin, 'checkBox_MotionCorrection' )
    if( value ):

      if ( widget.checkState() == 0 ):

        widget.setCheckState( 1 )

      widget.setEnabled( False )

    else:

      if ( widget.checkState() == 1 ):

        widget.setCheckState( 0 )
      widget.setEnabled( True )

    self.enablePushButtonCorrectionOptions( True )


  def setChecked( self, value ):

    checkBoxMotion = self._findChild( self._awin, 'checkBox_MotionCorrection' )
    checkBoxEddyCurrent = self._findChild( \
                                  self._awin, 'checkBox_EddyCurrentCorrection' )
    if not( checkBoxEddyCurrent.isChecked() ):

      checkBoxMotion.setChecked( value )

    self.enablePushButtonCorrectionOptions( value )


  def enablePushButtonCorrectionOptions( self, value ):

    pushButtonMotionCorrection = self._findChild( \
                              self._awin, 'pushButton_MotionCorrectionOptions' )
    pushButtonEddyCurrentCorrection = self._findChild( \
                         self._awin, 'pushButton_EddyCurrentCorrectionOptions' )

    checkBoxMotionCorrection = self._findChild( \
                                       self._awin, 'checkBox_MotionCorrection' )
    checkBoxEddyCurrentCorrection = self._findChild( \
                                  self._awin, 'checkBox_EddyCurrentCorrection' )

    label = self._findChild( self._awin, 'label_FileNameMotionTransform' )
    lineEdit = self._findChild( self._awin, 'lineEdit_FileNameMotionTransform' )
    browser = self._findChild( self._awin,
                               'pushButton_FileNameMotionTransform' )

    stackedWidgetCorrectionOptions = self._findChild( \
                                 self._awin, 'stackedWidget_CorrectionOptions' )

    if( checkBoxEddyCurrentCorrection.isChecked() ):

      stackedWidgetCorrectionOptions.setCurrentIndex( 1 )

    else:

      stackedWidgetCorrectionOptions.setCurrentIndex( 0 )

    if( checkBoxMotionCorrection.isChecked() or \
                                    checkBoxEddyCurrentCorrection.isChecked() ):

      pushButtonMotionCorrection.setEnabled( True )
      pushButtonEddyCurrentCorrection.setEnabled( True )
      label.setEnabled( True )
      lineEdit.setEnabled( True )
      browser.setEnabled( True )

    else:

      pushButtonMotionCorrection.setEnabled( False )
      pushButtonEddyCurrentCorrection.setEnabled( False )
      label.setEnabled( False )
      lineEdit.setEnabled( False )
      browser.setEnabled( False )


