from Task import *
import shutil

class DwiEddyCurrentAndMotionCorrectionTask( Task ):

  def __init__( self, application ):

    Task.__init__( self, application )


  ##############################################################################
  # execute()
  ##############################################################################

  def execute( self, functors, parameters, subjectName, verbose, viewOnly ):

    try:

      ####################### acquiring conditions #############################
      functors[ 'condition-acquire' ]( subjectName,
                                       'eddy-current-and-motion-processed' )
      functors[ 'condition-acquire' ]( subjectName,
                                       'corrected-rgb-processed' )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 0 )

      ####################### collecting output work directory #################
      outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()
      if ( verbose ):
      
        print  'Output work directory :', outputWorkDirectory

      ####################### collecting raw DWI directory #####################
      rawDwiDirectory = parameters[ 'rawDwiDirectory' ].getValue()
      if ( verbose ):
      
        print  'Raw DWI directory :', rawDwiDirectory

      ####################### collecting corrected DWI directory ###############
      correctedDwiDirectory = parameters[ 'correctedDwiDirectory' ].getValue()
      t2Extension = ''
      dwExtension = ''
      if ( os.path.exists( os.path.join( correctedDwiDirectory,
                           'dw_wo_outlier_and_discarded_orientations.ima' ) ) ):
                                         
        t2Extension = '_wo_outlier'
        dwExtension = '_wo_outlier_and_discarded_orientations'

      elif ( os.path.exists( os.path.join( correctedDwiDirectory,
                                         'dw_wo_outlier.ima' ) ) ):
                                         
        t2Extension = '_wo_outlier'
        dwExtension = '_wo_outlier'

      elif ( os.path.exists( os.path.join( correctedDwiDirectory,
                                           'dw_wo_susceptibility.ima' ) ) ):
        
        t2Extension = '_wo_susceptibility'
        dwExtension = '_wo_susceptibility'

      if ( verbose ):
      
        print  'Corrected DWI directory :', correctedDwiDirectory
      fileNameAverageT2 = os.path.join( correctedDwiDirectory,
                                        't2' + t2Extension )
      fileNameDW = os.path.join( correctedDwiDirectory, 'dw' + dwExtension )

      ####################### collecting raw DWI directory #####################
      roughMaskDirectory = parameters[ 'roughMaskDirectory' ].getValue()
      if ( verbose ):
      
        print  'Rough mask directory :', roughMaskDirectory
      fileNameMask = os.path.join( roughMaskDirectory, 'mask' )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 5 )


      ####################### Eddy current motion ##############################
      fileNameDWWoEddyCurrentAndMotion = ''
      if( parameters[ 'eddyCurrentCorrection' ].getValue() ):

        fileNameDWWoEddyCurrentAndMotion = self.correctEddyCurrentAndMotion( \
                                                            parameters,
                                                            fileNameDW,
                                                            outputWorkDirectory,
                                                            subjectName,
                                                            viewOnly )

      ####################### correcting motion ################################
      elif( parameters[ 'motionCorrection' ].getValue() ):

        fileNameDWWoEddyCurrentAndMotion = self.correctMotion( \
                                                            parameters,
                                                            fileNameDW,
                                                            outputWorkDirectory,
                                                            subjectName,
                                                            viewOnly )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 70 )


      ############################ copying T2 ##################################
      fileNameAverageT2WoEddyCurrentAndMotion = os.path.join( \
                                               outputWorkDirectory,
                                               't2_wo_eddy_current_and_motion' )
      command = 'GkgExecuteCommand Combiner' + \
                ' -i ' +  fileNameAverageT2 + \
                ' -o ' + fileNameAverageT2WoEddyCurrentAndMotion + \
                ' -num1 1 0 -den1 1 1 ' + \
                ' -verbose true'
      executeCommand( self, subjectName, command, viewOnly )

      ####################### notifying display thread #########################
      functors[ 'condition-notify-and-release' ]( \
                                           subjectName,
                                           'eddy-current-and-motion-processed' )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 90 )

      ########################### computing uncorrected RGB map ################
      self.createRGB( fileNameAverageT2WoEddyCurrentAndMotion,
                      fileNameDWWoEddyCurrentAndMotion,
                      fileNameMask,
                      outputWorkDirectory,
                      subjectName,
                      verbose,
                      viewOnly )

      ####################### notifying display thread #########################
      functors[ 'condition-notify-and-release' ]( subjectName,
                                                  'corrected-rgb-processed' )

       ########################### updating progress bar #######################
      functors[ 'update-progress-bar' ]( subjectName, 100 )

    except:

      self._functors[ 'task-set-status-as-failed' ]( subjectName,
                                                     sys.exc_info() )


  ##############################################################################
  # creating RGB map
  ##############################################################################

  def createRGB( self,
                 fileNameAverageT2,
                 fileNameDW,
                 fileNameMask,
                 outputWorkDirectory,
                 subjectName,
                 verbose,
                 viewOnly ):

    # building RGB map
    fileNameRGB = os.path.join( outputWorkDirectory,
                                'dti_rgb_wo_eddy_current_and_motion' )
    command = 'GkgExecuteCommand DwiTensorField' + \
              ' -t2 ' + fileNameAverageT2 + \
              ' -dw ' + fileNameDW + \
              ' -m ' +  fileNameMask + \
              ' -f rgb ' + \
              ' -o ' + fileNameRGB + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )

    return fileNameRGB


  ##############################################################################
  # correcting eddy current and motion
  ##############################################################################

  def correctEddyCurrentAndMotion( self, 
                                   parameters,
                                   fileNameDW,
                                   outputWorkDirectory,
                                   subjectName,
                                   viewOnly ):
    
    fileNameDWWoEddyCurrentAndMotion = os.path.join( \
                                               outputWorkDirectory,
                                               'dw_wo_eddy_current_and_motion' )

    eddyCurrentCorrectionOptions = parameters[ \
                                     'eddyCurrentCorrectionOptions' ].getValue()

    similarityMeasureName = str( parameters[ \
         'eddyCurrentCorrectionOptions' ].getChoice( 'similarityMeasureName' ) )
    optimizerName = str( parameters[ \
         'eddyCurrentCorrectionOptions' ].getChoice( 'optimizerName' ) )
    lowerThreshold = eddyCurrentCorrectionOptions[ 'lowerThreshold' ]
    resamplingOrder = eddyCurrentCorrectionOptions[ \
                                                 'registrationResamplingOrder' ]
    subSamplingMaximumSizes = eddyCurrentCorrectionOptions[ \
                                                     'subSamplingMaximumSizes' ]
    levelCount = eddyCurrentCorrectionOptions[ 'levelCount' ]
    applySmoothing = eddyCurrentCorrectionOptions[ 'applySmoothing' ]
    maximumIterationCount = eddyCurrentCorrectionOptions[ \
                                                       'maximumIterationCount' ]
    initialParametersScalingX = eddyCurrentCorrectionOptions[ \
                                                   'initialParametersScalingX' ]
    initialParametersScalingY = eddyCurrentCorrectionOptions[ \
                                                   'initialParametersScalingY' ]
    initialParametersScalingZ = eddyCurrentCorrectionOptions[ \
                                                   'initialParametersScalingZ' ]
    initialParametersShearingXY = eddyCurrentCorrectionOptions[ \
                                                 'initialParametersShearingXY' ]
    initialParametersShearingXZ = eddyCurrentCorrectionOptions[ \
                                                 'initialParametersShearingXZ' ]
    initialParametersShearingYZ = eddyCurrentCorrectionOptions[ \
                                                 'initialParametersShearingYZ' ]
    initialParametersRotationX = eddyCurrentCorrectionOptions[ \
                                                  'initialParametersRotationX' ]
    initialParametersRotationY = eddyCurrentCorrectionOptions[ \
                                                  'initialParametersRotationY' ]
    initialParametersRotationZ = eddyCurrentCorrectionOptions[ \
                                                  'initialParametersRotationZ' ]
    initialParametersTranslationX = eddyCurrentCorrectionOptions[ \
                                               'initialParametersTranslationX' ]
    initialParametersTranslationY = eddyCurrentCorrectionOptions[ \
                                               'initialParametersTranslationY' ]
    initialParametersTranslationZ = eddyCurrentCorrectionOptions[ \
                                               'initialParametersTranslationZ' ]
    stoppingCriterionError = eddyCurrentCorrectionOptions[ \
                                               'stoppingCriterionError' ]
    stepSize = eddyCurrentCorrectionOptions[ 'stepSize' ]
    maximumTestGradient = eddyCurrentCorrectionOptions[ 'maximumTestGradient' ]
    maximumTolerance = eddyCurrentCorrectionOptions[ 'maximumTolerance' ]
    optimizerParametersScalingX = eddyCurrentCorrectionOptions[ \
                                                     'optimizerParametersScalingX' ]
    optimizerParametersScalingY = eddyCurrentCorrectionOptions[ \
                                                     'optimizerParametersScalingY' ]
    optimizerParametersScalingZ = eddyCurrentCorrectionOptions[ \
                                                     'optimizerParametersScalingZ' ]
    optimizerParametersShearingXY = eddyCurrentCorrectionOptions[ \
                                                   'optimizerParametersShearingXY' ]
    optimizerParametersShearingXZ = eddyCurrentCorrectionOptions[ \
                                                   'optimizerParametersShearingXZ' ]
    optimizerParametersShearingYZ = eddyCurrentCorrectionOptions[ \
                                                   'optimizerParametersShearingYZ' ]
    optimizerParametersRotationX = eddyCurrentCorrectionOptions[ \
                                                    'optimizerParametersRotationX' ]
    optimizerParametersRotationY = eddyCurrentCorrectionOptions[ \
                                                    'optimizerParametersRotationY' ]
    optimizerParametersRotationZ = eddyCurrentCorrectionOptions[ \
                                                    'optimizerParametersRotationZ' ]
    optimizerParametersTranslationX = eddyCurrentCorrectionOptions[ \
                                                 'optimizerParametersTranslationX' ]
    optimizerParametersTranslationY = eddyCurrentCorrectionOptions[ \
                                                 'optimizerParametersTranslationY' ]
    optimizerParametersTranslationZ = eddyCurrentCorrectionOptions[ \
                                                 'optimizerParametersTranslationZ' ]
    outputResamplingOrder = eddyCurrentCorrectionOptions[ \
                                                       'outputResamplingOrder' ]
    backgroundResamplingLevel = eddyCurrentCorrectionOptions[ \
                                                   'backgroundResamplingLevel' ]
    #furtherSliceCountAlongX = eddyCurrentCorrectionOptions[ \
    #                                                 'furtherSliceCountAlongX' ]
    #furtherSliceCountAlongY = eddyCurrentCorrectionOptions[ \
    #                                                 'furtherSliceCountAlongY' ]
    #furtherSliceCountAlongZ = eddyCurrentCorrectionOptions[ \
    #                                                 'furtherSliceCountAlongZ' ]
    fileNameMotionTransform = parameters[ 'fileNameMotionTransform' ].getValue()
    
    command = 'GkgExecuteCommand DwiEddyCurrent ' + \
              ' -dw ' + fileNameDW + \
              ' -o ' + fileNameDWWoEddyCurrentAndMotion + \
              ' -similarityMeasureName ' + similarityMeasureName + \
              ' -optimizerName ' + optimizerName + \
              ' -lowerThreshold ' + str( lowerThreshold ) + \
              ' -registrationResamplingOrder ' + str( resamplingOrder ) + \
              ' -subSamplingMaximumSizes ' + str( subSamplingMaximumSizes )
              
    if ( similarityMeasureName != 'correlation-coefficient' ):

      command += ' -similarityMeasureParameters ' + str( levelCount ) + ' ' + \
                                                   str( int( applySmoothing ) )

    command += ' -correctQSpaceSampling true'+ \
               ' -initialParameters ' + str( initialParametersScalingX ) + \
                                  ' ' + str( initialParametersScalingY ) + \
                                  ' ' + str( initialParametersScalingZ ) + \
                                  ' ' + str( initialParametersShearingXY ) + \
                                  ' ' + str( initialParametersShearingXZ ) + \
                                  ' ' + str( initialParametersShearingYZ ) + \
                                  ' ' + str( initialParametersRotationX ) + \
                                  ' ' + str( initialParametersRotationY ) + \
                                  ' ' + str( initialParametersRotationZ ) + \
                                  ' ' + str( initialParametersTranslationX ) + \
                                  ' ' + str( initialParametersTranslationY ) + \
                                  ' ' + str( initialParametersTranslationZ )

    command += ' -optimizerParameters ' + str( maximumIterationCount )
    if ( optimizerName == 'nelder-mead' ):

      command += ' ' + str( stoppingCriterionError ) + \
                 ' ' + str( optimizerParametersScalingX ) + \
                 ' ' + str( optimizerParametersScalingY ) + \
                 ' ' + str( optimizerParametersScalingZ ) + \
                 ' ' + str( optimizerParametersShearingXY ) + \
                 ' ' + str( optimizerParametersShearingXZ ) + \
                 ' ' + str( optimizerParametersShearingYZ ) + \
                 ' ' + str( optimizerParametersRotationX ) + \
                 ' ' + str( optimizerParametersRotationY ) + \
                 ' ' + str( optimizerParametersRotationZ ) + \
                 ' ' + str( optimizerParametersTranslationX ) + \
                 ' ' + str( optimizerParametersTranslationY ) + \
                 ' ' + str( optimizerParametersTranslationZ )

    else:

      command += ' ' + str( stepSize ) + \
                 ' ' + str( maximumTestGradient ) + \
                 ' ' + str( maximumTolerance ) 

    command += ' -outputResamplingOrder ' + str( outputResamplingOrder ) + \
               ' -b ' + str( backgroundResamplingLevel )
               #' -furtherSliceCountAlongX ' + str( furtherSliceCountAlongX ) + \
               #' -furtherSliceCountAlongY ' + str( furtherSliceCountAlongY ) + \
               #' -furtherSliceCountAlongZ ' + str( furtherSliceCountAlongZ )

    if ( fileNameMotionTransform != '' ):

      command += ' -m ' + fileNameMotionTransform

    command += ' -verbose true'
    executeCommand( self, subjectName, command + ' -r 0', viewOnly )

    # geometric mean dw
    fileNameGeometricMeanDw = os.path.join( outputWorkDirectory,
                                            'dw_geometric_mean' )
    geometricMeanDwCommand = 'GkgExecuteCommand DwiGeometricMean ' + \
                             ' -dw ' + fileNameDWWoEddyCurrentAndMotion + \
                             ' -o ' + fileNameGeometricMeanDw + \
                             ' -verbose true'
    executeCommand( self, subjectName, geometricMeanDwCommand, viewOnly )

    # eddy current correction with geometric mean dw as reference
    
    fileNameMotionProfile = os.path.join( outputWorkDirectory,
                                          'motion_profile.py' )
    executeCommand( self, subjectName,
                    command + ' -t2 ' + fileNameGeometricMeanDw + \
                              ' -motionProfile ' + fileNameMotionProfile,
                    viewOnly )

    return fileNameDWWoEddyCurrentAndMotion


  ##############################################################################
  # correcting motion
  ##############################################################################

  def correctMotion( self,
                     parameters,
                     fileNameDW,
                     outputWorkDirectory,
                     subjectName,
                     viewOnly ):

    fileNameDWWoMotion = os.path.join( outputWorkDirectory,
                                       'dw_wo_eddy_current_and_motion' )
                                       
    motionCorrectionOptions = parameters[ 'motionCorrectionOptions' ].getValue()

    similarityMeasureName = str( parameters[ \
              'motionCorrectionOptions' ].getChoice( 'similarityMeasureName' ) )
    optimizerName = str( parameters[ \
              'motionCorrectionOptions' ].getChoice( 'optimizerName' ) )
    lowerThreshold = motionCorrectionOptions[ 'lowerThreshold' ]
    resamplingOrder = motionCorrectionOptions[ 'registrationResamplingOrder' ]
    subSamplingMaximumSizes = motionCorrectionOptions[ \
                                                     'subSamplingMaximumSizes' ]
    levelCount = motionCorrectionOptions[ 'levelCount' ]
    applySmoothing = motionCorrectionOptions[ 'applySmoothing' ]
    maximumIterationCount = motionCorrectionOptions[ 'maximumIterationCount' ]
    initialParametersRotationX = motionCorrectionOptions[ \
                                                  'initialParametersRotationX' ]
    initialParametersRotationY = motionCorrectionOptions[ \
                                                  'initialParametersRotationY' ]
    initialParametersRotationZ = motionCorrectionOptions[ \
                                                  'initialParametersRotationZ' ]
    initialParametersTranslationX = motionCorrectionOptions[ \
                                               'initialParametersTranslationX' ]
    initialParametersTranslationY = motionCorrectionOptions[ \
                                               'initialParametersTranslationY' ]
    initialParametersTranslationZ = motionCorrectionOptions[ \
                                               'initialParametersTranslationZ' ]
    stoppingCriterionError = motionCorrectionOptions[ \
                                               'stoppingCriterionError' ]
    stepSize = motionCorrectionOptions[ 'stepSize' ]
    maximumTestGradient = motionCorrectionOptions[ 'maximumTestGradient' ]
    maximumTolerance = motionCorrectionOptions[ 'maximumTolerance' ]
    optimizerParametersRotationX = motionCorrectionOptions[ \
                                             'optimizerParametersRotationX' ]
    optimizerParametersRotationY = motionCorrectionOptions[ \
                                             'optimizerParametersRotationY' ]
    optimizerParametersRotationZ = motionCorrectionOptions[ \
                                             'optimizerParametersRotationZ' ]
    optimizerParametersTranslationX = motionCorrectionOptions[ \
                                             'optimizerParametersTranslationX' ]
    optimizerParametersTranslationY = motionCorrectionOptions[ \
                                             'optimizerParametersTranslationY' ]
    optimizerParametersTranslationZ = motionCorrectionOptions[ \
                                             'optimizerParametersTranslationZ' ]
    outputResamplingOrder = motionCorrectionOptions[ \
                                                       'outputResamplingOrder' ]
    backgroundResamplingLevel = motionCorrectionOptions[ \
                                                   'backgroundResamplingLevel' ]
    #furtherSliceCountAlongX = motionCorrectionOptions[ \
    #                                                 'furtherSliceCountAlongX' ]
    #furtherSliceCountAlongY = motionCorrectionOptions[ \
    #                                                 'furtherSliceCountAlongY' ]
    #furtherSliceCountAlongZ = motionCorrectionOptions[ \
    #                                                 'furtherSliceCountAlongZ' ]
    fileNameMotionTransform = parameters[ 'fileNameMotionTransform' ].getValue()
 
    command = 'GkgExecuteCommand DwiMotionCorrection ' + \
              ' -i ' + fileNameDW + \
              ' -o ' + fileNameDWWoMotion + \
              ' -similarityMeasureName ' + similarityMeasureName + \
              ' -optimizerName ' + optimizerName + \
              ' -lowerThreshold ' + str( lowerThreshold ) + \
              ' -registrationResamplingOrder ' + str( resamplingOrder ) + \
              ' -subSamplingMaximumSizes ' + str( subSamplingMaximumSizes )

    if ( similarityMeasureName != 'correlation-coefficient' ):

      command += ' -similarityMeasureParameters ' + str( levelCount ) + ' ' + \
                                                   str( int( applySmoothing ) )

    command += ' -correctQSpaceSampling true'+ \
               ' -t rigid ' + \
               ' -initialParameters ' + str( initialParametersRotationX ) + \
                                  ' ' + str( initialParametersRotationY ) + \
                                  ' ' + str( initialParametersRotationZ ) + \
                                  ' ' + str( initialParametersTranslationX ) + \
                                  ' ' + str( initialParametersTranslationY ) + \
                                  ' ' + str( initialParametersTranslationZ )

    command += ' -optimizerParameters ' + str( maximumIterationCount )
    if ( optimizerName == 'nelder-mead' ):

      command += ' ' + str( stoppingCriterionError ) + \
                 ' ' + str( optimizerParametersRotationX ) + \
                 ' ' + str( optimizerParametersRotationY ) + \
                 ' ' + str( optimizerParametersRotationZ ) + \
                 ' ' + str( optimizerParametersTranslationX ) + \
                 ' ' + str( optimizerParametersTranslationY ) + \
                 ' ' + str( optimizerParametersTranslationZ )

    else:

      command += ' ' + str( stepSize ) + \
                 ' ' + str( maximumTestGradient ) + \
                 ' ' + str( maximumTolerance ) 

    command += ' -outputResamplingOrder ' + str( outputResamplingOrder ) + \
               ' -b ' + str( backgroundResamplingLevel ) 
               #' -furtherSliceCountAlongX ' + str( furtherSliceCountAlongX ) + \
               #' -furtherSliceCountAlongY ' + str( furtherSliceCountAlongY ) + \
               #' -furtherSliceCountAlongZ ' + str( furtherSliceCountAlongZ )

    if ( fileNameMotionTransform != '' ):

      command += ' -m ' + fileNameMotionTransform

    command += ' -verbose true'
    executeCommand( self, subjectName, command + ' -r 0', viewOnly )

    # geometric mean dw
    fileNameGeometricMeanDw = os.path.join( outputWorkDirectory,
                                            'dw_geometric_mean' )
    geometricMeanDwCommand = 'GkgExecuteCommand DwiGeometricMean ' + \
                             ' -dw ' + fileNameDWWoMotion + \
                             ' -o ' + fileNameGeometricMeanDw + \
                             ' -verbose true'
    executeCommand( self, subjectName, geometricMeanDwCommand, viewOnly )

    # motion correction with geometric mean dw as reference
    fileNameMotionProfile = os.path.join( outputWorkDirectory,
                                          'motion_profile.py' )
    executeCommand( self, subjectName,
                    command + ' -rv ' + fileNameGeometricMeanDw + \
                              ' -motionProfile ' + fileNameMotionProfile,
                    viewOnly )

    return fileNameDWWoMotion


  ##############################################################################
  # display()
  ##############################################################################

  def display( self, functors, parameters, subjectName, verbose,
               isReportModeActive ):

    ########################### resetting the viewer ###########################
    functors[ 'viewer-reset' ]()

    ####################### collecting raw DWI directory #####################
    rawDwiDirectory = parameters[ 'rawDwiDirectory' ].getValue()
    if ( verbose ):
    
      print  'Raw DWI directory :', rawDwiDirectory


    ####################### collecting corrected DWI directory ###############
    correctedDwiDirectory = parameters[ 'correctedDwiDirectory' ].getValue()
    t2Extension = ''
    dwExtension = ''
    rgbExtension = ''
    if ( os.path.exists( os.path.join( correctedDwiDirectory,
                         'dw_wo_outlier_and_discarded_orientations.ima' ) ) ):
                                       
      t2Extension = '_wo_outlier'
      dwExtension = '_wo_outlier_and_discarded_orientations'
      rgbExtension = '_wo_outlier'

    elif ( os.path.exists( os.path.join( correctedDwiDirectory,
                                       'dw_wo_outlier.ima' ) ) ):
                                       
      t2Extension = '_wo_outlier'
      dwExtension = '_wo_outlier'
      rgbExtension = '_wo_outlier'

    elif ( os.path.exists( os.path.join( correctedDwiDirectory,
                                         'dw_wo_susceptibility.ima' ) ) ):
      
      t2Extension = '_wo_susceptibility'
      dwExtension = '_wo_susceptibility'
      rgbExtension = '_wo_susceptibility'

    if ( verbose ):
    
      print  'Corrected DWI directory :', correctedDwiDirectory

    fileNameAverageT2 = os.path.join( correctedDwiDirectory,
                                      't2' + t2Extension )
    fileNameDW = os.path.join( correctedDwiDirectory, 'dw' + dwExtension )
    fileNameDtiRgb = os.path.join( correctedDwiDirectory,
                                   'dti_rgb' + rgbExtension )


    ####################### collecting output work directory ###################
    outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()
    if ( verbose ):
    
      print  'Output work directory :', outputWorkDirectory

    fileNameGeometricMeanDW = os.path.join( outputWorkDirectory,
                                           'dw_geometric_mean' )
    fileNameDWWoEddyCurrentAndMotion = os.path.join( outputWorkDirectory,
                                               'dw_wo_eddy_current_and_motion' )
    fileNameDtiRgbWoEddyCurrentAndMotion = os.path.join( \
                                          outputWorkDirectory,
                                          'dti_rgb_wo_eddy_current_and_motion' )
 

    ################################ creating DW ###############################
    functors[ 'viewer-create-referential' ]( 'frameDW' )

    ################################ reading average T2 ########################
    if ( verbose ):

      print 'reading \'' + fileNameAverageT2 + '\''
    functors[ 'viewer-load-object' ]( fileNameAverageT2, 'averageT2' )
    functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'averageT2' )

    functors[ 'viewer-assign-referential-to-window' ]( \
                                                 'frameDW',
                                                 'first_view',
                                                 'uncorrected Average T2(b=0)' )
    functors[ 'viewer-add-object-to-window' ]( 'averageT2',
                                               'first_view',
                                               'uncorrected Average T2(b=0)' )

    ################################ reading DW ################################
    if ( verbose ):

      print 'reading \'' + fileNameDW + '\''
    functors[ 'viewer-load-object' ]( fileNameDW, 'DW' )
    functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'DW' )

    functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                       'first_view',
                                                       'uncorrected DW' )
    functors[ 'viewer-add-object-to-window' ]( 'DW',
                                               'first_view',
                                               'uncorrected DW' )

    ########################### reading uncorrected RGB ########################
    if ( verbose ):

      print 'reading \'' + fileNameDtiRgb + '\''
    functors[ 'viewer-load-object' ]( fileNameDtiRgb, 'RGB' )
    functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'RGB' )

    functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                       'first_view',
                                                       'uncorrected RGB' )
    functors[ 'viewer-add-object-to-window' ]( 'RGB',
                                               'first_view',
                                               'uncorrected RGB' )

    ####################### waiting for corrected DW ###########################
    functors[ 'condition-wait-and-release' ]( \
                                           subjectName,
                                           'eddy-current-and-motion-processed' )

    ###################### reading geometric DW mean ###########################
    if ( verbose ):

      print 'reading \'' + fileNameGeometricMeanDW + '\''
    functors[ 'viewer-load-object' ]( fileNameGeometricMeanDW,
                                      'geometricMeanDW' )
    functors[ 'viewer-assign-referential-to-object' ]( \
                                                   'frameDW',
                                                   'geometricMeanDW' )

    functors[ 'viewer-assign-referential-to-window' ]( \
                                                   'frameDW',    
                                                   'first_view',
                                                   'geometric mean DW' )
    functors[ 'viewer-add-object-to-window' ]( 'geometricMeanDW',
                                               'first_view',
                                               'geometric mean DW' )

    ########################## reading corrected DW ############################
    if ( verbose ):

      print 'reading \'' + fileNameDWWoEddyCurrentAndMotion + '\''
    functors[ 'viewer-load-object' ]( fileNameDWWoEddyCurrentAndMotion,
                                      'DWWoEddyCurrentAndMotion' )
    functors[ 'viewer-assign-referential-to-object' ]( \
                                                    'frameDW',
                                                    'DWWoEddyCurrentAndMotion' )

    functors[ 'viewer-assign-referential-to-window' ]( \
                                                   'frameDW',    
                                                   'first_view',
                                                   'corrected DW' )
    functors[ 'viewer-add-object-to-window' ]( 'DWWoEddyCurrentAndMotion',
                                               'first_view',
                                               'corrected DW' )


    ######################### waiting for corrected RGB ########################
    functors[ 'condition-wait-and-release' ]( subjectName,
                                              'corrected-rgb-processed' )

    ########################### reading corrected RGB ##########################
    if ( verbose ):

      print 'reading \'' + fileNameDtiRgbWoEddyCurrentAndMotion + '\''
    functors[ 'viewer-load-object' ]( fileNameDtiRgbWoEddyCurrentAndMotion,
                                      'RGBWoEddyCurrentAndMotion' )
    functors[ 'viewer-assign-referential-to-object' ]( \
                                                   'frameDW',
                                                   'RGBWoEddyCurrentAndMotion' )

    functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                       'first_view',
                                                       'corrected RGB' )
    functors[ 'viewer-add-object-to-window' ]( 'RGBWoEddyCurrentAndMotion',
                                               'first_view',
                                               'corrected RGB' )
