from Viewer import *


class DwiEddyCurrentAndMotionCorrectionViewer( Viewer ):

  def __init__( self, minimumSize, parent ):

    Viewer.__init__( self, minimumSize, parent )
    
    ########################## First View ######################################

    self.createView( 'first_view' )

    self.addAxialWindow( 'first_view', 1, 0, 1, 1,
                         'uncorrected Average T2(b=0)',
                         'uncorrected Average T2(b=0)' )
    self.addAxialWindow( 'first_view', 1, 1, 1, 1,
                         'uncorrected DW',
                         'uncorrected DW' )
    self.addAxialWindow( 'first_view', 1, 2, 1, 1,
                         'uncorrected RGB',
                         'uncorrected RGB' )

    self.addAxialWindow( 'first_view', 2, 0, 1, 1,
                         'geometric mean DW',
                         'geometric mean DW' )
    self.addAxialWindow( 'first_view', 2, 1, 1, 1,
                         'corrected DW',
                         'corrected DW' )
    self.addAxialWindow( 'first_view', 2, 2, 1, 1,
                         'corrected RGB',
                         'corrected RGB' )

def createDwiEddyCurrentAndMotionCorrectionViewer( minimumSize, parent ):

  return DwiEddyCurrentAndMotionCorrectionViewer( minimumSize, parent )

