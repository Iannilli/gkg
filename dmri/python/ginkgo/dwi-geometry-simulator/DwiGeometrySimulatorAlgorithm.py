from Algorithm import *
from DwiGeometrySimulatorTask import *

###############################################################################

class DwiGeometrySimulatorAlgorithm( Algorithm ):

  def __init__( self, verbose ):

    Algorithm.__init__( self, 'DWI-Geometry-Simulator', verbose )

    ###########################################################################
    # Microscopist geometry model
    ###########################################################################

    # Elementary geometry model
    self.addParameter( ChoiceParameter( 'elementaryGeometry',
                                        0,
                                        ( 'box',
                                          'cylinder',
                                          'ellipsoid',
                                          'fiber',
                                          'sphere',
                                          'star' ) ) )

    ######## Box ###############################################################
    # Box model parameters
    self.addParameter( DoubleParameter( 'boxLowerPointX',
                                        -5.0, -1000.0, 1000.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'boxLowerPointY',
                                        -5.0, -1000.0, 1000.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'boxLowerPointZ',
                                        -5.0, -1000.0, 1000.0, 1.0 ) )

    self.addParameter( DoubleParameter( 'boxUpperPointX',
                                        +5.0, -1000.0, 1000.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'boxUpperPointY',
                                        +5.0, -1000.0, 1000.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'boxUpperPointZ',
                                        +5.0, -1000.0, 1000.0, 1.0 ) )

    self.addParameter( BooleanParameter( 'randomRotationOfBox', 0 ) )

    # Network of boxes
    self.addParameter( BooleanParameter( 'boxNetwork', 0 ) )

    self.addParameter( DoubleParameter( 'boxNetworkAxis1X',
                                        1.0, -1.0, 1.0, 0.1 ) )
    self.addParameter( DoubleParameter( 'boxNetworkAxis1Y',
                                        0.0, -1.0, 1.0, 0.1 ) )
    self.addParameter( DoubleParameter( 'boxNetworkAxis1Z',
                                        0.0, -1.0, 1.0, 0.1 ) )

    self.addParameter( DoubleParameter( 'boxNetworkAxis2X',
                                        0.0, -1.0, 1.0, 0.1 ) )
    self.addParameter( DoubleParameter( 'boxNetworkAxis2Y',
                                        1.0, -1.0, 1.0, 0.1 ) )
    self.addParameter( DoubleParameter( 'boxNetworkAxis2Z',
                                        0.0, -1.0, 1.0, 0.1 ) )

    self.addParameter( DoubleParameter( 'boxNetworkAxis3X',
                                        0.0, -1.0, 1.0, 0.1 ) )
    self.addParameter( DoubleParameter( 'boxNetworkAxis3Y',
                                        0.0, -1.0, 1.0, 0.1 ) )
    self.addParameter( DoubleParameter( 'boxNetworkAxis3Z',
                                        1.0, -1.0, 1.0, 0.1 ) )

    self.addParameter( DoubleParameter( 'boxNetworkSpacing1',
                                        20.0, 0.0, 1000.0, 0.1 ) )
    self.addParameter( DoubleParameter( 'boxNetworkSpacing2',
                                        20.0, 0.0, 1000.0, 0.1 ) )
    self.addParameter( DoubleParameter( 'boxNetworkSpacing3',
                                        20.0, 0.0, 1000.0, 0.1 ) )

    self.addParameter( IntegerParameter( 'boxNetworkCount1',
                                         10, 1, 1000, 1 ) )
    self.addParameter( IntegerParameter( 'boxNetworkCount2',
                                         10, 1, 1000, 1 ) )
    self.addParameter( IntegerParameter( 'boxNetworkCount3',
                                         10, 1, 1000, 1 ) )

    self.addParameter( BooleanParameter( 'randomRotationOfBoxes', 0 ) )


    ######## Cylinder ##########################################################
    # Cylinder model parameters
    self.addParameter( DoubleParameter( 'cylinderCenterX',
                                        0.0, -1000.0, 1000.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'cylinderCenterY',
                                        0.0, -1000.0, 1000.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'cylinderCenterZ',
                                        0.0, -1000.0, 1000.0, 1.0 ) )

    self.addParameter( DoubleParameter( 'cylinderAxisX',
                                        0.0, -10.0, 10.0, 0.1 ) )
    self.addParameter( DoubleParameter( 'cylinderAxisY',
                                        0.0, -10.0, 10.0, 0.1 ) )
    self.addParameter( DoubleParameter( 'cylinderAxisZ',
                                        1.0, -10.0, 10.0, 0.1 ) )

    self.addParameter( DoubleParameter( 'cylinderRadius1',
                                        3.5, 1.0, 100.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'cylinderRadius2',
                                        3.5, 1.0, 100.0, 1.0 ) )

    self.addParameter( DoubleParameter( 'cylinderLength',
                                        100.0, 0.0, 2000.0, 1.0 ) )

    self.addParameter( IntegerParameter( 'cylinderFacetCount',
                                         36, 4, 1000, 2 ) )

    self.addParameter( BooleanParameter( 'cylinderClosedShape', 0 ) )

    self.addParameter( BooleanParameter( 'randomRotationOfCylinder', 0 ) )

    # Network of cylinders
    self.addParameter( BooleanParameter( 'cylinderNetwork', 0 ) )

    self.addParameter( DoubleParameter( 'cylinderNetworkAxis1X',
                                        1.0, -10.0, 10.0, 0.1 ) )
    self.addParameter( DoubleParameter( 'cylinderNetworkAxis1Y',
                                        0.0, -10.0, 10.0, 0.1 ) )
    self.addParameter( DoubleParameter( 'cylinderNetworkAxis1Z',
                                        0.0, -10.0, 10.0, 0.1 ) )

    self.addParameter( DoubleParameter( 'cylinderNetworkAxis2X',
                                        0.0, -10.0, 10.0, 0.1 ) )
    self.addParameter( DoubleParameter( 'cylinderNetworkAxis2Y',
                                        1.0, -10.0, 10.0, 0.1 ) )
    self.addParameter( DoubleParameter( 'cylinderNetworkAxis2Z',
                                        0.0, -10.0, 10.0, 0.1 ) )

    self.addParameter( DoubleParameter( 'cylinderNetworkSpacing1',
                                        8.0, 0.0, 1000.0, 0.1 ) )
    self.addParameter( DoubleParameter( 'cylinderNetworkSpacing2',
                                        8.0, 0.0, 1000.0, 0.1 ) )

    self.addParameter( IntegerParameter( 'cylinderNetworkCount1',
                                         10, 1, 1000, 1 ) )
    self.addParameter( IntegerParameter( 'cylinderNetworkCount2',
                                         10, 1, 1000, 1 ) )

    self.addParameter( BooleanParameter( 'randomRotationOfCylinders', 0 ) )


    ######## Ellipsoid #########################################################
    # Ellipsoid model parameters
    self.addParameter( DoubleParameter( 'ellipsoidCenterX',
                                        0.0, -1000.0, 1000.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'ellipsoidCenterY',
                                        0.0, -1000.0, 1000.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'ellipsoidCenterZ',
                                        0.0, -1000.0, 1000.0, 1.0 ) )

    self.addParameter( DoubleParameter( 'ellipsoidEigenVector1X',
                                        1.0, -1.0, 1.0, 0.1  ) )
    self.addParameter( DoubleParameter( 'ellipsoidEigenVector1Y',
                                        0.0, -1.0, 1.0, 0.1  ) )
    self.addParameter( DoubleParameter( 'ellipsoidEigenVector1Z',
                                        0.0, -1.0, 1.0, 0.1  ) )

    self.addParameter( DoubleParameter( 'ellipsoidEigenVector2X',
                                        0.0, -1.0, 1.0, 0.1  ) )
    self.addParameter( DoubleParameter( 'ellipsoidEigenVector2Y',
                                        1.0, -1.0, 1.0, 0.1  ) )
    self.addParameter( DoubleParameter( 'ellipsoidEigenVector2Z',
                                        0.0, -1.0, 1.0, 0.1  ) )

    self.addParameter( DoubleParameter( 'ellipsoidEigenVector3X',
                                        0.0, -1.0, 1.0, 0.1  ) )
    self.addParameter( DoubleParameter( 'ellipsoidEigenVector3Y',
                                        0.0, -1.0, 1.0, 0.1  ) )
    self.addParameter( DoubleParameter( 'ellipsoidEigenVector3Z',
                                        1.0, -1.0, 1.0, 0.1  ) )

    self.addParameter( DoubleParameter( 'ellipsoidEigenValue1',
                                        5.0, 1.0, 100.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'ellipsoidEigenValue2',
                                        2.0, 1.0, 100.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'ellipsoidEigenValue3',
                                        1.0, 1.0, 100.0, 1.0 ) )

    self.addParameter( IntegerParameter( 'ellipsoidVextexCount',
                                         100, 4, 2000, 2 ) )

    self.addParameter( BooleanParameter( 'randomRotationOfEllipsoid', 0 ) )

    # Network of ellipsoids
    self.addParameter( BooleanParameter( 'ellipsoidNetwork', 0 ) )

    self.addParameter( DoubleParameter( 'ellipsoidNetworkAxis1X',
                                        1.0, -1.0, 1.0, 0.1 ) )
    self.addParameter( DoubleParameter( 'ellipsoidNetworkAxis1Y',
                                        0.0, -1.0, 1.0, 0.1 ) )
    self.addParameter( DoubleParameter( 'ellipsoidNetworkAxis1Z',
                                        0.0, -1.0, 1.0, 0.1 ) )

    self.addParameter( DoubleParameter( 'ellipsoidNetworkAxis2X',
                                        0.0, -1.0, 1.0, 0.1 ) )
    self.addParameter( DoubleParameter( 'ellipsoidNetworkAxis2Y',
                                        1.0, -1.0, 1.0, 0.1 ) )
    self.addParameter( DoubleParameter( 'ellipsoidNetworkAxis2Z',
                                        0.0, -1.0, 1.0, 0.1 ) )

    self.addParameter( DoubleParameter( 'ellipsoidNetworkAxis3X',
                                        0.0, -1.0, 1.0, 0.1 ) )
    self.addParameter( DoubleParameter( 'ellipsoidNetworkAxis3Y',
                                        0.0, -1.0, 1.0, 0.1 ) )
    self.addParameter( DoubleParameter( 'ellipsoidNetworkAxis3Z',
                                        1.0, -1.0, 1.0, 0.1 ) )

    self.addParameter( DoubleParameter( 'ellipsoidNetworkSpacing1',
                                        10.0, 0.0, 1000.0, 0.1 ) )
    self.addParameter( DoubleParameter( 'ellipsoidNetworkSpacing2',
                                        10.0, 0.0, 1000.0, 0.1 ) )
    self.addParameter( DoubleParameter( 'ellipsoidNetworkSpacing3',
                                        10.0, 0.0, 1000.0, 0.1 ) )

    self.addParameter( IntegerParameter( 'ellipsoidNetworkCount1',
                                         10, 1, 1000, 1 ) )
    self.addParameter( IntegerParameter( 'ellipsoidNetworkCount2',
                                         10, 1, 1000, 1 ) )
    self.addParameter( IntegerParameter( 'ellipsoidNetworkCount3',
                                         10, 1, 1000, 1 ) )

    self.addParameter( BooleanParameter( 'randomRotationOfEllipsoids', 0 ) )


    ######## Fiber #############################################################
    # Fiber model parameters
    self.addParameter( BooleanParameter( 'fiberManualChoice', 1 ) )

    self.addParameter( DoubleParameter( 'fiberPointsX',
                                        0.0, -1000.0, 1000.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'fiberPointsY',
                                        0.0, -1000.0, 1000.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'fiberPointsZ',
                                        0.0, -1000.0, 1000.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'fiberRadius',
                                        1.0, 0.001, 50.0, 1.0 ) )

    self.addParameter( ListOfParameterList( 'listOfPointsAndRadii',
                                            4,
                                            [ 'Points X',
                                              'Points Y',
                                              'Points Z',
                                              'Radius' ],
                                            [ 'double',
                                              'double',
                                              'double',
                                              'double' ] ) )

    self.addParameter( BooleanParameter( 'fiberInputFileChoice', 0 ) )
    self.addParameter( StringParameter( 'fiberInputFile', "" ) )

    self.addParameter( IntegerParameter( 'fiberVertexCountPerPlane',\
                                         36, 4, 100, 1 ) )

    self.addParameter( BooleanParameter( 'randomRotationOfFiber', 0 ) )

    # Network of fibers
    self.addParameter( BooleanParameter( 'fiberNetwork', 0 ) )

    self.addParameter( DoubleParameter( 'fiberNetworkAxis1X',
                                        1.0, -1.0, 1.0, 0.1  ) )
    self.addParameter( DoubleParameter( 'fiberNetworkAxis1Y',
                                        0.0, -1.0, 1.0, 0.1  ) )
    self.addParameter( DoubleParameter( 'fiberNetworkAxis1Z',
                                        0.0, -1.0, 1.0, 0.1  ) )

    self.addParameter( DoubleParameter( 'fiberNetworkAxis2X',
                                        0.0, -1.0, 1.0, 0.1  ) )
    self.addParameter( DoubleParameter( 'fiberNetworkAxis2Y',
                                        1.0, -1.0, 1.0, 0.1  ) )
    self.addParameter( DoubleParameter( 'fiberNetworkAxis2Z',
                                        0.0, -1.0, 1.0, 0.1  ) )

    self.addParameter( DoubleParameter( 'fiberNetworkSpacing1',
                                        5.0, 0.0, 1000.0, 0.1 ) )
    self.addParameter( DoubleParameter( 'fiberNetworkSpacing2',
                                        5.0, 0.0, 1000.0, 0.1 ) )

    self.addParameter( IntegerParameter( 'fiberNetworkCount1',
                                         2, 1, 1000, 1 ) )
    self.addParameter( IntegerParameter( 'fiberNetworkCount2',
                                         2, 1, 1000, 1 ) )

    self.addParameter( BooleanParameter( 'randomRotationOfFibers', 0 ) )


    ######## Sphere ############################################################
    # Sphere model parameters
    self.addParameter( DoubleParameter( 'sphereCenterX',
                                        0.0, -1000.0, 1000.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'sphereCenterY',
                                        0.0, -1000.0, 1000.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'sphereCenterZ',
                                        0.0, -1000.0, 1000.0, 1.0 ) )

    self.addParameter( DoubleParameter( 'sphereRadius',
                                        2.5, 1.0, 100.0, 1.0 ) )

    self.addParameter( IntegerParameter( 'sphereVextexCount',
                                         100, 4, 1000, 2 ) )

    # Network of spheres
    self.addParameter( BooleanParameter( 'sphereNetwork', 0 ) )

    self.addParameter( DoubleParameter( 'sphereNetworkAxis1X',
                                        1.0, -1.0, 1.0, 0.1  ) )
    self.addParameter( DoubleParameter( 'sphereNetworkAxis1Y',
                                        0.0, -1.0, 1.0, 0.1  ) )
    self.addParameter( DoubleParameter( 'sphereNetworkAxis1Z',
                                        0.0, -1.0, 1.0, 0.1  ) )

    self.addParameter( DoubleParameter( 'sphereNetworkAxis2X',
                                        0.0, -1.0, 1.0, 0.1  ) )
    self.addParameter( DoubleParameter( 'sphereNetworkAxis2Y',
                                        1.0, -1.0, 1.0, 0.1  ) )
    self.addParameter( DoubleParameter( 'sphereNetworkAxis2Z',
                                        0.0, -1.0, 1.0, 0.1  ) )

    self.addParameter( DoubleParameter( 'sphereNetworkAxis3X',
                                        0.0, -1.0, 1.0, 0.1  ) )
    self.addParameter( DoubleParameter( 'sphereNetworkAxis3Y',
                                        0.0, -1.0, 1.0, 0.1  ) )
    self.addParameter( DoubleParameter( 'sphereNetworkAxis3Z',
                                        1.0, -1.0, 1.0, 0.1  ) )

    self.addParameter( DoubleParameter( 'sphereNetworkSpacing1',
                                        6.0, 0.0, 1000.0, 0.1 ) )
    self.addParameter( DoubleParameter( 'sphereNetworkSpacing2',
                                        6.0, 0.0, 1000.0, 0.1 ) )
    self.addParameter( DoubleParameter( 'sphereNetworkSpacing3',
                                        6.0, 0.0, 1000.0, 0.1 ) )

    self.addParameter( IntegerParameter( 'sphereNetworkCount1',
                                         10, 1, 1000, 1 ) )
    self.addParameter( IntegerParameter( 'sphereNetworkCount2',
                                         10, 1, 1000, 1 ) )
    self.addParameter( IntegerParameter( 'sphereNetworkCount3',
                                         10, 1, 1000, 1 ) )

    ######## Star ##############################################################
    # Star model parameters
    self.addParameter( DoubleParameter( 'starCenterX',
                                        0.0, -1000.0, 1000.0, 1.0  ) )
    self.addParameter( DoubleParameter( 'starCenterY',
                                        0.0, -1000.0, 1000.0, 1.0  ) )
    self.addParameter( DoubleParameter( 'starCenterZ',
                                        0.0, -1000.0, 1000.0, 1.0  ) )

    self.addParameter( DoubleParameter( 'starRadius',
                                        3.0, 1.0, 100.0, 1.0 ) )
    self.addParameter( IntegerParameter( 'starPeakCount',
                                         12, 12, 256, 2 ) )

    self.addParameter( StringParameter(
                                       'starPeakMagnitudes',
                                       "10 10 10 10 10 10 10 10 10 10 10 10" ) )

    self.addParameter( StringParameter(
                                       'starPeakApertureAngles',
                                       "15 15 15 15 15 15 15 15 15 15 15 15" ) )

    self.addParameter( BooleanParameter( 'starSharpPeak', 0 ) )
    self.addParameter( BooleanParameter( 'starSymmetricPeak', 0 ) )
    self.addParameter( IntegerParameter( 'starVextexCount',
                                         1000, 10, 10000, 1 ) )

    self.addParameter( BooleanParameter( 'randomRotationOfStar', 0 ) )

    # Network of stars
    self.addParameter( BooleanParameter( 'starNetwork', 0 ) )

    self.addParameter( DoubleParameter( 'starNetworkAxis1X',
                                        1.0, -1.0, 1.0, 0.1  ) )
    self.addParameter( DoubleParameter( 'starNetworkAxis1Y',
                                        0.0, -1.0, 1.0, 0.1  ) )
    self.addParameter( DoubleParameter( 'starNetworkAxis1Z',
                                        0.0, -1.0, 1.0, 0.1  ) )

    self.addParameter( DoubleParameter( 'starNetworkAxis2X',
                                        0.0, -1.0, 1.0, 0.1  ) )
    self.addParameter( DoubleParameter( 'starNetworkAxis2Y',
                                        1.0, -1.0, 1.0, 0.1  ) )
    self.addParameter( DoubleParameter( 'starNetworkAxis2Z',
                                        0.0, -1.0, 1.0, 0.1  ) )

    self.addParameter( DoubleParameter( 'starNetworkAxis3X',
                                        0.0, -1.0, 1.0, 0.1  ) )
    self.addParameter( DoubleParameter( 'starNetworkAxis3Y',
                                        0.0, -1.0, 1.0, 0.1  ) )
    self.addParameter( DoubleParameter( 'starNetworkAxis3Z',
                                        1.0, -1.0, 1.0, 0.1  ) )

    self.addParameter( DoubleParameter( 'starNetworkSpacing1',
                                        20.0, 0.0, 1000.0, 0.1 ) )
    self.addParameter( DoubleParameter( 'starNetworkSpacing2',
                                        20.0, 0.0, 1000.0, 0.1 ) )
    self.addParameter( DoubleParameter( 'starNetworkSpacing3',
                                        20.0, 0.0, 1000.0, 0.1 ) )

    self.addParameter( IntegerParameter( 'starNetworkCount1',
                                         10, 1, 1000, 1 ) )
    self.addParameter( IntegerParameter( 'starNetworkCount2',
                                         10, 1, 1000, 1 ) )
    self.addParameter( IntegerParameter( 'starNetworkCount3',
                                         10, 1, 1000, 1 ) )

    self.addParameter( BooleanParameter( 'randomRotationOfStars', 0 ) )

    ######## End of elementary geometry ########################################

    self.addParameter( StringParameter( 'elementaryGeometryName',
                                        "" ) )

    self.addParameter( ListOfParameterList( 'SelectedElementaryGeometries',
                                            1,
                                            [ 'Elements' ],
                                            [ 'string' ] ) )


  def launch( self ):

    if ( self._verbose ):

      print '******** Running DWI geometry simulator'

    task = DwiGeometrySimulatorTask( self._application )
    task.launch( False )


  def view( self ):

    if ( self._verbose ):

      print '******** Viewing DWI geometry simulator'

    task = DwiGeometrySimulatorTask( self._application )
    task.launch( True )

