from AlgorithmGUI import *
from ResourceManager import *

class DwiGeometrySimulatorAlgorithmGUI( AlgorithmGUI ):

  def __init__( self, algorithm ):

    AlgorithmGUI.__init__( self,
                           algorithm,
                           ResourceManager().getUIFileName(
                                                   'dmri',
                                                   'DwiGeometrySimulator.ui' ) )

    self._geometryTransferDictionary = {}
    self._geometryGlobalDictionary = {}

    ############################################################################
    # Connecting microscopist geometry model
    ############################################################################

    # Selecting geometry element
    self.connectChoiceParameterToComboBox( 'elementaryGeometry',
                                           'comboBox_ElementaryGeometry' )

    # Connecting set of parameters corresponding to the choice of sequence
    self.connectComboBoxToStackedWidget(
                                       'comboBox_ElementaryGeometry',
                                       'stackedWidget_GeometryModelParameters' )


    ############################################################################
    # Elementary geometry :  Box
    ############################################################################

    # Box model parameters
    self.connectDoubleParameterToSpinBox( 'boxLowerPointX',
                                          'doubleSpinBox_BoxLowerPointX' )
    self.connectDoubleParameterToSpinBox( 'boxLowerPointY',
                                          'doubleSpinBox_BoxLowerPointY' )
    self.connectDoubleParameterToSpinBox( 'boxLowerPointZ',
                                          'doubleSpinBox_BoxLowerPointZ' )

    self.connectDoubleParameterToSpinBox( 'boxUpperPointX',
                                          'doubleSpinBox_BoxUpperPointX' )
    self.connectDoubleParameterToSpinBox( 'boxUpperPointY',
                                          'doubleSpinBox_BoxUpperPointY' )
    self.connectDoubleParameterToSpinBox( 'boxUpperPointZ',
                                          'doubleSpinBox_BoxUpperPointZ' )

    self.connectBooleanParameterToCheckBox( 'randomRotationOfBox',
                                            'checkBox_RandomRotationofBox' )

    # Network of boxes
    self.connectBooleanParameterToCheckBox( 'boxNetwork',
                                            'checkBox_BoxNetwork' )
    self.connectCheckBoxToCustomCallback( 'checkBox_BoxNetwork',
                                          self.callbackEnableDisableBoxNetwork )

    self.connectDoubleParameterToSpinBox( 'boxNetworkAxis1X',
                                          'doubleSpinBox_BoxNetworkAxis1X' )
    self.connectDoubleParameterToSpinBox( 'boxNetworkAxis1Y',
                                          'doubleSpinBox_BoxNetworkAxis1Y' )
    self.connectDoubleParameterToSpinBox( 'boxNetworkAxis1Z',
                                          'doubleSpinBox_BoxNetworkAxis1Z' )

    self.connectDoubleParameterToSpinBox( 'boxNetworkAxis2X',
                                          'doubleSpinBox_BoxNetworkAxis2X' )
    self.connectDoubleParameterToSpinBox( 'boxNetworkAxis2Y',
                                          'doubleSpinBox_BoxNetworkAxis2Y' )
    self.connectDoubleParameterToSpinBox( 'boxNetworkAxis2Z',
                                          'doubleSpinBox_BoxNetworkAxis2Z' )

    self.connectDoubleParameterToSpinBox( 'boxNetworkAxis3X',
                                          'doubleSpinBox_BoxNetworkAxis3X' )
    self.connectDoubleParameterToSpinBox( 'boxNetworkAxis3Y',
                                          'doubleSpinBox_BoxNetworkAxis3Y' )
    self.connectDoubleParameterToSpinBox( 'boxNetworkAxis3Z',
                                          'doubleSpinBox_BoxNetworkAxis3Z' )

    self.connectDoubleParameterToSpinBox( 'boxNetworkSpacing1',
                                          'doubleSpinBox_BoxNetworkSpacing1' )
    self.connectDoubleParameterToSpinBox( 'boxNetworkSpacing2',
                                          'doubleSpinBox_BoxNetworkSpacing2' )
    self.connectDoubleParameterToSpinBox( 'boxNetworkSpacing3',
                                          'doubleSpinBox_BoxNetworkSpacing3' )

    self.connectIntegerParameterToSpinBox( 'boxNetworkCount1',
                                           'spinBox_BoxNetworkCount1' )
    self.connectIntegerParameterToSpinBox( 'boxNetworkCount2',
                                           'spinBox_BoxNetworkCount2' )
    self.connectIntegerParameterToSpinBox( 'boxNetworkCount3',
                                           'spinBox_BoxNetworkCount3' )

    self.connectBooleanParameterToCheckBox( 'randomRotationOfBoxes',
                                            'checkBox_RandomRotationOfBoxes' )

    ############################################################################
    # Elementary geometry :  Cylinder
    ############################################################################

    # Cylinder model parameters
    self.connectDoubleParameterToSpinBox( 'cylinderCenterX',
                                          'doubleSpinBox_CylinderCenterX' )
    self.connectDoubleParameterToSpinBox( 'cylinderCenterY',
                                          'doubleSpinBox_CylinderCenterY' )
    self.connectDoubleParameterToSpinBox( 'cylinderCenterZ',
                                          'doubleSpinBox_CylinderCenterZ' )

    self.connectDoubleParameterToSpinBox( 'cylinderAxisX',
                                          'doubleSpinBox_CylinderAxisX' )
    self.connectDoubleParameterToSpinBox( 'cylinderAxisY',
                                          'doubleSpinBox_CylinderAxisY' )
    self.connectDoubleParameterToSpinBox( 'cylinderAxisZ',
                                          'doubleSpinBox_CylinderAxisZ' )

    self.connectDoubleParameterToSpinBox( 'cylinderRadius1',
                                          'doubleSpinBox_CylinderRadius1' )
    self.connectDoubleParameterToSpinBox( 'cylinderRadius2',
                                          'doubleSpinBox_CylinderRadius2' )

    self.connectDoubleParameterToSpinBox( 'cylinderLength',
                                          'doubleSpinBox_CylinderLength' )

    self.connectIntegerParameterToSpinBox( 'cylinderFacetCount',
                                           'spinBox_CylinderFacetCount' )
    self.connectBooleanParameterToCheckBox( 'cylinderClosedShape',
                                            'checkBox_CylinderClosedShape' )
    self.connectBooleanParameterToCheckBox( 'randomRotationOfCylinder',
                                            'checkBox_RandomRotationOfCylinder')


    # Network of cylinders
    self.connectBooleanParameterToCheckBox( 'cylinderNetwork',
                                            'checkBox_CylinderNetwork' )
    self.connectCheckBoxToCustomCallback(
                                     'checkBox_CylinderNetwork',
                                     self.callbackEnableDisableCylinderNetwork )

    self.connectDoubleParameterToSpinBox(
                                         'cylinderNetworkAxis1X',
                                         'doubleSpinBox_CylinderNetworkAxis1X' )
    self.connectDoubleParameterToSpinBox(
                                         'cylinderNetworkAxis1Y',
                                         'doubleSpinBox_CylinderNetworkAxis1Y' )
    self.connectDoubleParameterToSpinBox(
                                         'cylinderNetworkAxis1Z',
                                         'doubleSpinBox_CylinderNetworkAxis1Z' )

    self.connectDoubleParameterToSpinBox(
                                         'cylinderNetworkAxis2X',
                                         'doubleSpinBox_CylinderNetworkAxis2X' )
    self.connectDoubleParameterToSpinBox(
                                         'cylinderNetworkAxis2Y',
                                         'doubleSpinBox_CylinderNetworkAxis2Y' )
    self.connectDoubleParameterToSpinBox(
                                         'cylinderNetworkAxis2Z',
                                         'doubleSpinBox_CylinderNetworkAxis2Z' )

    self.connectDoubleParameterToSpinBox(
                                       'cylinderNetworkSpacing1',
                                       'doubleSpinBox_CylinderNetworkSpacing1' )
    self.connectDoubleParameterToSpinBox(
                                       'cylinderNetworkSpacing2',
                                       'doubleSpinBox_CylinderNetworkSpacing2' )

    self.connectIntegerParameterToSpinBox( 'cylinderNetworkCount1',
                                           'spinBox_CylinderNetworkCount1' )
    self.connectIntegerParameterToSpinBox( 'cylinderNetworkCount2',
                                           'spinBox_CylinderNetworkCount2' )

    self.connectBooleanParameterToCheckBox( 
                                          'randomRotationOfCylinders',
                                          'checkBox_RandomRotationOfCylinders' )

    ############################################################################
    # Elementary geometry :  Ellipsoid
    ############################################################################

    # Ellipsoid model parameters
    self.connectDoubleParameterToSpinBox( 'ellipsoidCenterX',
                                          'doubleSpinBox_EllipsoidCenterX' )
    self.connectDoubleParameterToSpinBox( 'ellipsoidCenterY',
                                          'doubleSpinBox_EllipsoidCenterY' )
    self.connectDoubleParameterToSpinBox( 'ellipsoidCenterZ',
                                          'doubleSpinBox_EllipsoidCenterZ' )

    self.connectDoubleParameterToSpinBox(
                                        'ellipsoidEigenVector1X',
                                        'doubleSpinBox_EllipsoidEigenVector1X' )
    self.connectDoubleParameterToSpinBox(
                                        'ellipsoidEigenVector1Y',
                                        'doubleSpinBox_EllipsoidEigenVector1Y' )
    self.connectDoubleParameterToSpinBox(
                                        'ellipsoidEigenVector1Z',
                                        'doubleSpinBox_EllipsoidEigenVector1Z' )

    self.connectDoubleParameterToSpinBox(
                                        'ellipsoidEigenVector2X',
                                        'doubleSpinBox_EllipsoidEigenVector2X' )
    self.connectDoubleParameterToSpinBox(
                                        'ellipsoidEigenVector2Y',
                                        'doubleSpinBox_EllipsoidEigenVector2Y' )
    self.connectDoubleParameterToSpinBox(
                                        'ellipsoidEigenVector2Z',
                                        'doubleSpinBox_EllipsoidEigenVector2Z' )

    self.connectDoubleParameterToSpinBox(
                                        'ellipsoidEigenVector3X',
                                        'doubleSpinBox_EllipsoidEigenVector3X' )
    self.connectDoubleParameterToSpinBox(
                                        'ellipsoidEigenVector3Y',
                                        'doubleSpinBox_EllipsoidEigenVector3Y' )
    self.connectDoubleParameterToSpinBox(
                                        'ellipsoidEigenVector3Z',
                                        'doubleSpinBox_EllipsoidEigenVector3Z' )

    self.connectDoubleParameterToSpinBox( 'ellipsoidEigenValue1',
                                          'doubleSpinBox_EllipsoidEigenValue1' )
    self.connectDoubleParameterToSpinBox( 'ellipsoidEigenValue2',
                                          'doubleSpinBox_EllipsoidEigenValue2' )
    self.connectDoubleParameterToSpinBox( 'ellipsoidEigenValue3',
                                          'doubleSpinBox_EllipsoidEigenValue3' )

    self.connectIntegerParameterToSpinBox( 'ellipsoidVextexCount',
                                           'spinBox_EllipsoidVextexCount' )

    self.connectBooleanParameterToCheckBox(
                                          'randomRotationOfEllipsoid',
                                          'checkBox_RandomRotationOfEllipsoid' )

    # Network of ellipsoid
    self.connectBooleanParameterToCheckBox( 'ellipsoidNetwork',
                                            'checkBox_EllipsoidNetwork' )
    self.connectCheckBoxToCustomCallback(
                                    'checkBox_EllipsoidNetwork',
                                    self.callbackEnableDisableEllipsoidNetwork )

    self.connectDoubleParameterToSpinBox(
                                        'ellipsoidNetworkAxis1X',
                                        'doubleSpinBox_EllipsoidNetworkAxis1X' )
    self.connectDoubleParameterToSpinBox(
                                        'ellipsoidNetworkAxis1Y',
                                        'doubleSpinBox_EllipsoidNetworkAxis1Y' )
    self.connectDoubleParameterToSpinBox(
                                        'ellipsoidNetworkAxis1Z',
                                        'doubleSpinBox_EllipsoidNetworkAxis1Z' )

    self.connectDoubleParameterToSpinBox(
                                        'ellipsoidNetworkAxis2X',
                                        'doubleSpinBox_EllipsoidNetworkAxis2X' )
    self.connectDoubleParameterToSpinBox(
                                        'ellipsoidNetworkAxis2Y',
                                        'doubleSpinBox_EllipsoidNetworkAxis2Y' )
    self.connectDoubleParameterToSpinBox(
                                        'ellipsoidNetworkAxis2Z',
                                        'doubleSpinBox_EllipsoidNetworkAxis2Z' )

    self.connectDoubleParameterToSpinBox(
                                        'ellipsoidNetworkAxis3X',
                                        'doubleSpinBox_EllipsoidNetworkAxis3X' )
    self.connectDoubleParameterToSpinBox(
                                        'ellipsoidNetworkAxis3Y',
                                        'doubleSpinBox_EllipsoidNetworkAxis3Y' )
    self.connectDoubleParameterToSpinBox(
                                        'ellipsoidNetworkAxis3Z',
                                        'doubleSpinBox_EllipsoidNetworkAxis3Z' )

    self.connectDoubleParameterToSpinBox(
                                      'ellipsoidNetworkSpacing1',
                                      'doubleSpinBox_EllipsoidNetworkSpacing1' )
    self.connectDoubleParameterToSpinBox(
                                      'ellipsoidNetworkSpacing2',
                                      'doubleSpinBox_EllipsoidNetworkSpacing2' )
    self.connectDoubleParameterToSpinBox(
                                      'ellipsoidNetworkSpacing3',
                                      'doubleSpinBox_EllipsoidNetworkSpacing3' )

    self.connectIntegerParameterToSpinBox( 'ellipsoidNetworkCount1',
                                           'spinBox_EllipsoidNetworkCount1' )
    self.connectIntegerParameterToSpinBox( 'ellipsoidNetworkCount2',
                                           'spinBox_EllipsoidNetworkCount2' )
    self.connectIntegerParameterToSpinBox( 'ellipsoidNetworkCount3',
                                           'spinBox_EllipsoidNetworkCount3' )

    self.connectBooleanParameterToCheckBox(
                                         'randomRotationOfEllipsoids',
                                         'checkBox_RandomRotationOfEllipsoids' )


    ############################################################################
    # Elementary geometry :  fiber
    ############################################################################

    ## Manage Qframes by radioButtons & initialisation of Qframes - Manual
    self.connectRadioButtonToParameterAndCustomCallback(
                                    'fiberManualChoice',
                                    'radioButton_FiberManualSwitch',
                                    self.callbackEnableDisableFiberManualEntry )
    self.callbackEnableDisableFiberManualEntry( 1 )

    self.connectDoubleParameterToPersistentSpinBox(
                                                  'fiberPointsX',
                                                  'doubleSpinBox_FiberPointsX' )
    self.connectDoubleParameterToPersistentSpinBox(
                                                  'fiberPointsY',
                                                  'doubleSpinBox_FiberPointsY' )
    self.connectDoubleParameterToPersistentSpinBox(
                                                  'fiberPointsZ',
                                                  'doubleSpinBox_FiberPointsZ' )
    self.connectDoubleParameterToPersistentSpinBox(
                                                   'fiberRadius',
                                                   'doubleSpinBox_FiberRadius' )

    self.connectListOfParameterToPushButtonAndTreeWidget(
                                        'listOfPointsAndRadii',
                                        [ 'fiberPointsX',
                                          'fiberPointsY',
                                          'fiberPointsZ',
                                          'fiberRadius' ],
                                        'pushButton_FiberAddPoint',
                                        'pushButton_FiberRemovePoint',
                                        'treeWidget_FiberListOfPointsAndRadii' )

    ## Manage Qframes by radioButtons & initialisation of Qframes - File input
    self.connectRadioButtonToParameterAndCustomCallback(
                                    'fiberInputFileChoice',
                                    'radioButton_FiberInputFileSwitch',
                                    self.callbackEnableDisableFiberFileLoading )
    self.callbackEnableDisableFiberFileLoading( 0 )

    self.connectStringParameterToLineEdit( 'fiberInputFile',
                                           'lineEdit_FiberInputFile' )
    self.connectFileBrowserToPushButtonAndLineEdit(
                                              'pushButton_FiberBrowseInputFile',
                                              'lineEdit_FiberInputFile' )

    self.connectIntegerParameterToSpinBox( 'fiberVertexCountPerPlane',
                                           'spinBox_FiberVertexCountPerPlane')

    self.connectBooleanParameterToCheckBox( 'randomRotationOfFiber',
                                            'checkBox_RandomRotationOfFiber' )

    # Network of fibers
    self.connectBooleanParameterToCheckBox( 'fiberNetwork',
                                            'checkBox_FiberNetwork' )
    self.connectCheckBoxToCustomCallback(
                                        'checkBox_FiberNetwork',
                                        self.callbackEnableDisableFiberNetwork )

    self.connectDoubleParameterToSpinBox( 'fiberNetworkAxis1X',
                                          'doubleSpinBox_FiberNetworkAxis1X' )
    self.connectDoubleParameterToSpinBox( 'fiberNetworkAxis1Y',
                                          'doubleSpinBox_FiberNetworkAxis1Y' )
    self.connectDoubleParameterToSpinBox( 'fiberNetworkAxis1Z',
                                          'doubleSpinBox_FiberNetworkAxis1Z' )

    self.connectDoubleParameterToSpinBox( 'fiberNetworkAxis2X',
                                          'doubleSpinBox_FiberNetworkAxis2X' )
    self.connectDoubleParameterToSpinBox( 'fiberNetworkAxis2Y',
                                          'doubleSpinBox_FiberNetworkAxis2Y' )
    self.connectDoubleParameterToSpinBox( 'fiberNetworkAxis2Z',
                                          'doubleSpinBox_FiberNetworkAxis2Z' )

    self.connectDoubleParameterToSpinBox( 'fiberNetworkSpacing1',
                                          'doubleSpinBox_FiberNetworkSpacing1' )
    self.connectDoubleParameterToSpinBox( 'fiberNetworkSpacing2',
                                          'doubleSpinBox_FiberNetworkSpacing2' )

    self.connectIntegerParameterToSpinBox( 'fiberNetworkCount1',
                                           'spinBox_FiberNetworkCount1' )
    self.connectIntegerParameterToSpinBox( 'fiberNetworkCount2',
                                           'spinBox_FiberNetworkCount2' )

    self.connectBooleanParameterToCheckBox( 'randomRotationOfFibers',
                                            'checkBox_RandomRotationOfFibers' )

    ############################################################################
    # Elementary geometry : Sphere
    ############################################################################

    # Sphere model parameters
    self.connectDoubleParameterToSpinBox( 'sphereCenterX',
                                          'doubleSpinBox_SphereCenterX' )
    self.connectDoubleParameterToSpinBox( 'sphereCenterY',
                                          'doubleSpinBox_SphereCenterY' )
    self.connectDoubleParameterToSpinBox( 'sphereCenterZ',
                                          'doubleSpinBox_SphereCenterZ' )

    self.connectDoubleParameterToSpinBox( 'sphereRadius',
                                          'doubleSpinBox_SphereRadius' )

    self.connectIntegerParameterToSpinBox( 'sphereVextexCount',
                                           'spinBox_SphereVextexCount' )

    self.connectBooleanParameterToCheckBox( 'sphereNetwork',
                                            'checkBox_SphereNetwork' )

    ## Network of spheres
    self.connectBooleanParameterToCheckBox( 'sphereNetwork',
                                            'checkBox_SphereNetwork' )
    self.connectCheckBoxToCustomCallback(
                                       'checkBox_SphereNetwork',
                                       self.callbackEnableDisableSphereNetwork )

    self.connectDoubleParameterToSpinBox( 'sphereNetworkAxis1X',
                                          'doubleSpinBox_SphereNetworkAxis1X' )
    self.connectDoubleParameterToSpinBox( 'sphereNetworkAxis1Y',
                                          'doubleSpinBox_SphereNetworkAxis1Y' )
    self.connectDoubleParameterToSpinBox( 'sphereNetworkAxis1Z',
                                          'doubleSpinBox_SphereNetworkAxis1Z' )

    self.connectDoubleParameterToSpinBox( 'sphereNetworkAxis2X',
                                          'doubleSpinBox_SphereNetworkAxis2X' )
    self.connectDoubleParameterToSpinBox( 'sphereNetworkAxis2Y',
                                          'doubleSpinBox_SphereNetworkAxis2Y' )
    self.connectDoubleParameterToSpinBox( 'sphereNetworkAxis2Z',
                                          'doubleSpinBox_SphereNetworkAxis2Z' )

    self.connectDoubleParameterToSpinBox( 'sphereNetworkAxis3X',
                                          'doubleSpinBox_SphereNetworkAxis3X' )
    self.connectDoubleParameterToSpinBox( 'sphereNetworkAxis3Y',
                                          'doubleSpinBox_SphereNetworkAxis3Y' )
    self.connectDoubleParameterToSpinBox( 'sphereNetworkAxis3Z',
                                          'doubleSpinBox_SphereNetworkAxis3Z' )

    self.connectDoubleParameterToSpinBox(
                                         'sphereNetworkSpacing1',
                                         'doubleSpinBox_SphereNetworkSpacing1' )
    self.connectDoubleParameterToSpinBox(
                                         'sphereNetworkSpacing2',
                                         'doubleSpinBox_SphereNetworkSpacing2' )
    self.connectDoubleParameterToSpinBox(
                                         'sphereNetworkSpacing3',
                                         'doubleSpinBox_SphereNetworkSpacing3' )

    self.connectIntegerParameterToSpinBox( 'sphereNetworkCount1',
                                           'spinBox_SphereNetworkCount1' )
    self.connectIntegerParameterToSpinBox( 'sphereNetworkCount2',
                                           'spinBox_SphereNetworkCount2' )
    self.connectIntegerParameterToSpinBox( 'sphereNetworkCount3',
                                           'spinBox_SphereNetworkCount3' )


    ############################################################################
    # Elementary geometry : Star
    ############################################################################

    # Star model parameters
    self.connectDoubleParameterToSpinBox( 'starCenterX',
                                          'doubleSpinBox_StarCenterX' )
    self.connectDoubleParameterToSpinBox( 'starCenterY',
                                          'doubleSpinBox_StarCenterY' )
    self.connectDoubleParameterToSpinBox( 'starCenterZ',
                                          'doubleSpinBox_StarCenterZ' )

    self.connectDoubleParameterToSpinBox( 'starRadius',
                                          'doubleSpinBox_StarRadius' )
    self.connectIntegerParameterToSpinBox( 'starPeakCount',
                                           'spinBox_StarPeakCount' )

    self.connectStringParameterToLineEdit( 'starPeakMagnitudes',
                                           'lineEdit_StarPeakMagnitudes' )
    self.connectStringParameterToLineEdit( 'starPeakApertureAngles',
                                           'lineEdit_StarPeakApertureAngles' )

    self.connectIntegerParameterToSpinBox( 'starVextexCount',
                                           'spinBox_StarVextexCount' )
    self.connectBooleanParameterToCheckBox( 'starSharpPeak',
                                            'checkBox_StarSharpPeak' )
    self.connectBooleanParameterToCheckBox( 'starSymmetricPeak',
                                            'checkBox_StarSymmetricPeak' )
    self.connectBooleanParameterToCheckBox( 'randomRotationOfStar',
                                            'checkBox_RandomRotationOfStar' )

    ## Network of stars
    self.connectBooleanParameterToCheckBox( 'starNetwork',
                                            'checkBox_StarNetwork' )
    self.connectCheckBoxToCustomCallback(
                                         'checkBox_StarNetwork',
                                         self.callbackEnableDisableStarNetwork )

    self.connectDoubleParameterToSpinBox( 'starNetworkAxis1X',
                                          'doubleSpinBox_StarNetworkAxis1X' )
    self.connectDoubleParameterToSpinBox( 'starNetworkAxis1Y',
                                          'doubleSpinBox_StarNetworkAxis1Y' )
    self.connectDoubleParameterToSpinBox( 'starNetworkAxis1Z',
                                          'doubleSpinBox_StarNetworkAxis1Z' )

    self.connectDoubleParameterToSpinBox( 'starNetworkAxis2X',
                                          'doubleSpinBox_StarNetworkAxis2X' )
    self.connectDoubleParameterToSpinBox( 'starNetworkAxis2Y',
                                          'doubleSpinBox_StarNetworkAxis2Y' )
    self.connectDoubleParameterToSpinBox( 'starNetworkAxis2Z',
                                          'doubleSpinBox_StarNetworkAxis2Z' )

    self.connectDoubleParameterToSpinBox( 'starNetworkAxis3X',
                                          'doubleSpinBox_StarNetworkAxis3X' )
    self.connectDoubleParameterToSpinBox( 'starNetworkAxis3Y',
                                          'doubleSpinBox_StarNetworkAxis3Y' )
    self.connectDoubleParameterToSpinBox( 'starNetworkAxis3Z',
                                          'doubleSpinBox_StarNetworkAxis3Z' )

    self.connectDoubleParameterToSpinBox( 'starNetworkSpacing1',
                                          'doubleSpinBox_StarNetworkSpacing1' )
    self.connectDoubleParameterToSpinBox( 'starNetworkSpacing2',
                                          'doubleSpinBox_StarNetworkSpacing2' )
    self.connectDoubleParameterToSpinBox( 'starNetworkSpacing3',
                                          'doubleSpinBox_StarNetworkSpacing3' )

    self.connectIntegerParameterToSpinBox( 'starNetworkCount1',
                                           'spinBox_StarNetworkCount1' )
    self.connectIntegerParameterToSpinBox( 'starNetworkCount2',
                                           'spinBox_StarNetworkCount2' )
    self.connectIntegerParameterToSpinBox( 'starNetworkCount3',
                                           'spinBox_StarNetworkCount3' )

    self.connectBooleanParameterToCheckBox( 'randomRotationOfStars',
                                            'checkBox_RandomRotationOfStars' )


    ######## End of elementary geometry ########################################

    ############################################################################
    # Manage adding or deleting geometry elements
    ###########################################################################

    self.connectStringParameterToLineEdit( 'elementaryGeometryName',
                                           'lineEdit_ElementaryGeometryName' )

    self.connectComboBoxToLineEdit( 'comboBox_ElementaryGeometry',
                                    'lineEdit_ElementaryGeometryName' )

    self.connectPushButtonToCustomCallback(
       'pushButton_AddElement',
       self.
       callbackAddOrReplaceElementaryGeometryToTransferDictionaryAndTreeWidget )

    self.connectListOfParameterToPushButtonAndTreeWidget(
                                     'SelectedElementaryGeometries',
                                     [ 'elementaryGeometryName' ],
                                     'pushButton_AddElement',
                                     'pushButton_DeleteElement',
                                     'treeWidget_SelectedElementaryGeometries' )

    self.connectPushButtonToCustomCallback(
                                         'pushButton_Run',
                                         self.callbackCreateGeometryDictionary )


  def connectComboBoxToLineEdit( self, comboBoxName, lineEditName ):

    comboBoxWidget = self._findChild( self._awin, comboBoxName )
    comboBoxWidget.activated.connect( self.callbackElementaryGeometryChoice )


  def callbackElementaryGeometryChoice( self, currentIndex ):

    comboBoxWidget = self._findChild( self._awin,
                                      'comboBox_ElementaryGeometry' )
    lineEditWidget = self._findChild( self._awin,
                                      'lineEdit_ElementaryGeometryName' )

    text = comboBoxWidget.itemText( currentIndex ) + "_"
    lineEditWidget.setText( text )


  ##############################################################################
  # 1st pack of functions:  trigger = 'Add element'
  ##############################################################################

  def callbackAddOrReplaceElementaryGeometryToTransferDictionaryAndTreeWidget(
                                                                         self ):

    print 'Geometry model in course of transfer: ', \
          self._algorithm.getParameter( 'elementaryGeometryName' ).readValue()

    # getting the elementary geometry parameters in the dictionary
    # "elementaryGeometryTransferDictionary"
    elementaryGeometryTransferDictionary = \
                                      self.collectElementaryGeometryParameters()

    # getting the elementary geometry name used as the key in the dictionary
    currentElementaryGeometryName = \
                                elementaryGeometryTransferDictionary.keys()[ 0 ]

    # writing the elementary geometry parameters in the dictionary
    # "_geometryTransferDictionary" following the key

    # check for overwriting
    if ( currentElementaryGeometryName in self._geometryTransferDictionary ):

      print 'Overwriting the geometry: ', currentElementaryGeometryName

      selectedElementaryGeometries = self._algorithm.getParameter(
                                                'SelectedElementaryGeometries' )
      elementaryGeometryCount = selectedElementaryGeometries.getEntryCount()

      for geometryRank in range( elementaryGeometryCount ):

        selectedElementaryGeometryName = selectedElementaryGeometries\
                         .getValueForEntryIndexAndLabelIndex( geometryRank , 0 )

        if ( selectedElementaryGeometryName == currentElementaryGeometryName ):

          selectedElementaryGeometries.removeEntry( geometryRank )

    self._geometryTransferDictionary[ currentElementaryGeometryName ] = \
           elementaryGeometryTransferDictionary[ currentElementaryGeometryName ]


  def collectElementaryGeometryParameters( self ):

    elementaryGeometryType = self._algorithm.getParameter(
                                              'elementaryGeometry' ).getChoice()
    elementaryGeometryDictionary = {}

    if ( elementaryGeometryType == 'box' ):

      elementaryGeometryDictionary = self.collectBoxGeometryParameters()

    elif ( elementaryGeometryType == 'cylinder' ):

      elementaryGeometryDictionary = self.collectCylinderGeometryParameters()

    elif ( elementaryGeometryType == 'ellipsoid' ):

      elementaryGeometryDictionary = self.collectEllipsoidGeometryParameters()

    elif ( elementaryGeometryType == 'fiber' ):

      elementaryGeometryDictionary = self.collectFiberGeometryParameters()

    elif ( elementaryGeometryType == 'sphere' ):

      elementaryGeometryDictionary = self.collectSphereGeometryParameters()

    elif ( elementaryGeometryType == 'star' ):

      elementaryGeometryDictionary = self.collectStarGeometryParameters()

    return elementaryGeometryDictionary


  ##############################################################################
  # 2nd pack of functions: trigger = 'Run'
  ##############################################################################

  def callbackCreateGeometryDictionary( self ):

    outputWorkDirectory = self._algorithm.getParameter(
                                              'outputWorkDirectory' ).getValue()
    geometryFileName = os.path.join( outputWorkDirectory, 'geometry.membrane' )
    self.writeGeometryDictionary( geometryFileName )


  def writeGeometryDictionary( self, geometryFileName ):

    if ( len( self._geometryTransferDictionary ) != 0 ):

      selectedElementaryGeometries = self._algorithm.getParameter(
                                                'SelectedElementaryGeometries' )

      elementaryGeometryCount = selectedElementaryGeometries.getEntryCount()

      for entryIndex in range( elementaryGeometryCount ):

        currentElementaryGeometryName = selectedElementaryGeometries\
                           .getValueForEntryIndexAndLabelIndex( entryIndex , 0 )

        print 'copying geometry: ', currentElementaryGeometryName

        # collecting the parameters for each selected geometry
        self._geometryGlobalDictionary[ currentElementaryGeometryName ] = \
               self._geometryTransferDictionary[ currentElementaryGeometryName ]

      self.writeGeometryDictionaryFile( geometryFileName,
                                        self._geometryGlobalDictionary )

    # the global dictionary is released to avoid crossing between several runs
    self._geometryGlobalDictionary = {}


  ##############################################################################
  # 3rd pack of functions: write/read files
  ##############################################################################

  def writeGeometryDictionaryFile( self, fileName, geometryDictionary ):

    print 'saving parameters in ', fileName

    of = open( fileName, 'w' )
    of.write( 'attributes = ' )
    of.write( repr( geometryDictionary ) )
    of.close()


  def loadFiberFile( self ):

    filename = self._algorithm.getParameter( 'fiberInputFile' ).readValue()

    print 'reading parameters in ', filename

    fiberSegmentCoordinates = list()
    fiberSegmentRadii = list()

    fiberCurrentSegmentCoordinates = list()

    inputFile = open( filename, 'r' )
    currentSegmentProperties = inputFile.readline()
    inputFile.close()

    currentSegmentProperties = str()
    currentSegmentSplitListSpace = list()
    currentSegmentSplitListComma = list()
    currentSegmentListString = list()

    currentSegmentSplitListComma = currentSegmentProperties.split( ',' )
    currentSegmentSplitListSpace = currentSegmentProperties.split( ' ' )

    if ( len( currentSegmentSplitListComma ) == 1 ):

      currentSegmentListString = currentSegmentSplitListSpace

    else:

      currentSegmentListString = currentSegmentSplitListComma

    fiberCurrentSegmentCoordinates.append(
                                        float( currentSegmentListString[ 0 ] ) )
    fiberCurrentSegmentCoordinates.append(
                                        float( currentSegmentListString[ 1 ] ) )
    fiberCurrentSegmentCoordinates.append(
                                        float( currentSegmentListString[ 2 ] ) )

    fiberSegmentCoordinates.append( fiberCurrentSegmentCoordinates )
    fiberSegmentRadii.append( float( currentSegmentListString[ 3 ] ) )

    fiberCurrentSegmentCoordinates = []
    currentSegmentListString = []

    fiberSegmentsProperties = { 'points': fiberSegmentCoordinates,
                                'radii': fiberSegmentRadii }

    return fiberSegmentsProperties


  ##############################################################################
  # 4th pack of functions: manage list of parameters
  ##############################################################################

  def loadStarPeakProperties( self, starPeakCount, starPeakProperties ):

    liststring = str()
    liststring = starPeakProperties

    splitListstringComma = liststring.split( ',' )
    splitListstringSpace = liststring.split( ' ' )
    if ( len( splitListstringComma ) == 1 ):

      splitListstring = splitListstringSpace

    else:

      splitListstring = splitListstringComma

    peaks = list()

    if ( len( splitListstring ) != starPeakCount ):

      print 'Error: size of list is inappropriated: '
      print starPeakCount + '<>' + len( splitListstring )

    else:

      for v in splitListstring:

        peaks.append( float( v ) )

    return peaks


  ##############################################################################
  # 5th pack of functions: manage enable/disable Widget
  ##############################################################################

  def callbackEnableDisableBoxNetwork( self, state ):

    self.enableDisableOneWidget( 'frame_BoxNetworkParameters',
                                 bool( state ) )


  def callbackEnableDisableCylinderNetwork( self, state ):

    self.enableDisableOneWidget( 'frame_CylinderNetworkParameters',
                                 bool( state ) )


  def callbackEnableDisableFiberNetwork( self, state ):

    self.enableDisableOneWidget( 'frame_FiberNetworkParameters',
                                 bool( state ) )


  def callbackEnableDisableEllipsoidNetwork( self, state ):

    self.enableDisableOneWidget( 'frame_EllipsoidNetworkParameters',
                                 bool( state ) )


  def callbackEnableDisableSphereNetwork( self, state ):

    self.enableDisableOneWidget( 'frame_SphereNetworkParameters',
                                 bool( state ) )


  def callbackEnableDisableStarNetwork( self, state ):

    self.enableDisableOneWidget( 'frame_StarNetworkParameters',
                                 bool( state ) )


  def enableDisableOneWidget( self, widgetName, state ):

    widget = self._findChild( self._awin, widgetName )
    widget.setEnabled( state )


  def connectRadioButtonToParameterAndCustomCallback( self,
                                                      parameterName,
                                                      radioButtonName,
                                                      customCallback ):

    self.connectBooleanParameterToRadioButton( parameterName,
                                               radioButtonName )
    radioButtonWidget = self._findChild( self._awin, radioButtonName )

    radioButtonWidget.toggled.connect( customCallback )


  def callbackEnableDisableFiberManualEntry( self, state ):

    self.enableDisableOneWidget( 'frame_FiberManualEntry',
                                 bool ( state ) )


  def callbackEnableDisableFiberFileLoading( self, state ):

    self.enableDisableOneWidget( 'frame_FiberFileLoading',
                                 bool ( state ) )


  def connectDoubleParameterToPersistentSpinBox( self,
                                                 parameterName,
                                                 widgetName ):

    widget = self._findChild( self._awin, widgetName )
    parameter = self._algorithm.getParameter( parameterName )

    widget.setMinimum( parameter.getLowerValue() )
    widget.setMaximum( parameter.getUpperValue() )
    widget.setSingleStep( parameter.getIncrementValue() )
    widget.setValue( parameter.getValue() )

    widget.valueChanged.connect( parameter.setValue )

    parameter.setWidget( '', None )


  ######## Elementary geometry parameters ######################################

  def collectBoxGeometryParameters( self ):

    # Getting the network type
    boxNetwork = self._algorithm.getParameter( 'boxNetwork' ).readValue()

    # Creating the elementary dictionary
    boxName = self._algorithm.getParameter(
                                          'elementaryGeometryName' ).readValue()
    boxParameters = {}
    boxParameters[ str( boxName ) ] = {}
    boxParameters[ str( boxName ) ][ str( 'type' ) ] = 'box'
    boxParameters[ str( boxName ) ][ str( 'parameters' ) ] = {}

    # Collecting the parameters and values
    boxLowerPoint = [
                  self._algorithm.getParameter( 'boxLowerPointX' ).readValue(),
                  self._algorithm.getParameter( 'boxLowerPointY' ).readValue(),
                  self._algorithm.getParameter( 'boxLowerPointZ' ).readValue() ]
    boxUpperPoint = [
                  self._algorithm.getParameter( 'boxUpperPointX' ).readValue(),
                  self._algorithm.getParameter( 'boxUpperPointY' ).readValue(),
                  self._algorithm.getParameter( 'boxUpperPointZ' ).readValue() ]

    randomRotationOfBox = self._algorithm.getParameter(
                                             'randomRotationOfBox' ).readValue()

    # Writing the elementary dictionary
    boxParameters[ str( boxName ) ][ str( 'parameters' ) ]\
                                   [ 'lower_point' ] = boxLowerPoint
    boxParameters[ str( boxName ) ][ str( 'parameters' ) ]\
                                   [ 'upper_point' ] = boxUpperPoint
    boxParameters[ str( boxName ) ][ str( 'parameters' ) ]\
                                   [ 'random_rotation' ] = randomRotationOfBox

    if ( boxNetwork == 2 ):

      # Updating only the type of the geometry
      boxParameters[ str( boxName ) ][ str( 'type' ) ] = 'network_of_boxes'

      # Collecting the newtwork parameters and values
      randomRotationOfBoxes = self._algorithm.getParameter(
                                           'randomRotationOfBoxes' ).readValue()
      boxNetworkAxis1 = [
                self._algorithm.getParameter( 'boxNetworkAxis1X' ).readValue(),
                self._algorithm.getParameter( 'boxNetworkAxis1Y' ).readValue(),
                self._algorithm.getParameter( 'boxNetworkAxis1Z' ).readValue() ]
      boxNetworkAxis2 = [
                self._algorithm.getParameter( 'boxNetworkAxis2X' ).readValue(),
                self._algorithm.getParameter( 'boxNetworkAxis2Y' ).readValue(),
                self._algorithm.getParameter( 'boxNetworkAxis2Z' ).readValue() ]
      boxNetworkAxis3 = [
                self._algorithm.getParameter( 'boxNetworkAxis3X' ).readValue(),
                self._algorithm.getParameter( 'boxNetworkAxis3Y' ).readValue(),
                self._algorithm.getParameter( 'boxNetworkAxis3Z' ).readValue() ]

      boxNetworkSpacing1 = self._algorithm.getParameter(
                                              'boxNetworkSpacing1' ).readValue()
      boxNetworkSpacing2 = self._algorithm.getParameter(
                                              'boxNetworkSpacing2' ).readValue()
      boxNetworkSpacing3 = self._algorithm.getParameter(
                                              'boxNetworkSpacing3' ).readValue()

      boxNetworkCount1 = self._algorithm.getParameter(
                                                'boxNetworkCount1' ).readValue()
      boxNetworkCount2 = self._algorithm.getParameter(
                                                'boxNetworkCount2' ).readValue()
      boxNetworkCount3 = self._algorithm.getParameter(
                                                'boxNetworkCount3' ).readValue()

      randomRotationOfBoxes = self._algorithm.getParameter(
                                           'randomRotationOfBoxes' ).readValue()

      # Writing the elementary dictionary
      boxParameters[ str( boxName ) ][ str( 'parameters' ) ]\
                   [ 'network_axis1' ] = boxNetworkAxis1
      boxParameters[ str( boxName ) ][ str( 'parameters' ) ]\
                   [ 'network_axis2' ] = boxNetworkAxis2
      boxParameters[ str( boxName ) ][ str( 'parameters' ) ]\
                   [ 'network_axis3' ] = boxNetworkAxis3

      boxParameters[ str( boxName ) ][ str( 'parameters' ) ]\
                   [ 'network_spacing1' ] = boxNetworkSpacing1
      boxParameters[ str( boxName ) ][ str( 'parameters' ) ]\
                   [ 'network_spacing2' ] = boxNetworkSpacing2
      boxParameters[ str( boxName ) ][ str( 'parameters' ) ]\
                   [ 'network_spacing3' ] = boxNetworkSpacing3

      boxParameters[ str( boxName ) ][ str( 'parameters' ) ]\
                   [ 'network_count1' ] = boxNetworkCount1
      boxParameters[ str( boxName ) ][ str( 'parameters' ) ]\
                   [ 'network_count2' ] = boxNetworkCount2
      boxParameters[ str( boxName ) ][ str( 'parameters' ) ]\
                   [ 'network_count3' ] = boxNetworkCount3
      boxParameters[ str( boxName ) ][ str( 'parameters' ) ]\
                   [ 'random_rotation' ] = randomRotationOfBoxes

    # Exporting the elementary dictionary
    return boxParameters

  
  def collectCylinderGeometryParameters( self ):

    # Getting the network type
    cylinderNetwork = self._algorithm.getParameter(
                                                 'cylinderNetwork' ).readValue()

    # Creating the elementary dictionary
    cylinderName = self._algorithm.getParameter(
                                          'elementaryGeometryName' ).readValue()

    cylinderParameters = {}
    cylinderParameters[ str( cylinderName ) ] = {}
    cylinderParameters[ str( cylinderName ) ][ str( 'type' ) ] = 'cylinder'
    cylinderParameters[ str( cylinderName ) ][ str( 'parameters' ) ] = {}

    # Collecting the parameters and values
    cylinderCenter = [
                 self._algorithm.getParameter( 'cylinderCenterX' ).readValue(),
                 self._algorithm.getParameter( 'cylinderCenterY' ).readValue(),
                 self._algorithm.getParameter( 'cylinderCenterZ' ).readValue() ]

    cylinderAxis = [
                   self._algorithm.getParameter( 'cylinderAxisX' ).readValue(),
                   self._algorithm.getParameter( 'cylinderAxisY' ).readValue(),
                   self._algorithm.getParameter( 'cylinderAxisZ' ).readValue() ]

    cylinderLength = self._algorithm.getParameter(
                                                  'cylinderLength' ).readValue()

    cylinderRadius1 = self._algorithm.getParameter(
                                                 'cylinderRadius1' ).readValue()
    cylinderRadius2 = self._algorithm.getParameter(
                                                 'cylinderRadius2' ).readValue()
    cylinderFacetCount = self._algorithm.getParameter(
                                              'cylinderFacetCount' ).readValue()
    cylinderClosedShape = self._algorithm.getParameter(
                                             'cylinderClosedShape' ).readValue()
    randomRotationOfCylinder = self._algorithm.getParameter(
                                        'randomRotationOfCylinder' ).readValue()

    # Writing the elementary dictionary
    cylinderParameters[ cylinderName ][ 'parameters' ]\
                      [ 'center' ] = cylinderCenter
    cylinderParameters[ cylinderName ][ 'parameters' ]\
                      [ 'axis' ] = cylinderAxis
    cylinderParameters[ cylinderName ][ 'parameters' ]\
                      [ 'length' ] = cylinderLength
    cylinderParameters[ cylinderName ][ 'parameters' ]\
                      [ 'radius1' ] = cylinderRadius1
    cylinderParameters[ cylinderName ][ 'parameters' ]\
                      [ 'radius2' ] = cylinderRadius2
    cylinderParameters[ cylinderName ][ 'parameters' ]\
                      [ 'facet_count' ] = cylinderFacetCount
    cylinderParameters[ cylinderName ][ 'parameters' ]\
                      [ 'closed' ] = cylinderClosedShape
    cylinderParameters[ cylinderName ][ 'parameters' ]\
                      [ 'random_rotation' ] = randomRotationOfCylinder

    if ( cylinderNetwork == 2 ):

      # Updating only the type of the geometry
      cylinderParameters[ str( cylinderName ) ][ str( 'type' ) ] = \
                                                          'network_of_cylinders'

      # Collecting the newtwork parameters and values
      cylinderNetworkAxis1 = [
           self._algorithm.getParameter( 'cylinderNetworkAxis1X' ).readValue(),
           self._algorithm.getParameter( 'cylinderNetworkAxis1Y' ).readValue(),
           self._algorithm.getParameter( 'cylinderNetworkAxis1Z' ).readValue() ]
      cylinderNetworkAxis2 = [
           self._algorithm.getParameter( 'cylinderNetworkAxis2X' ).readValue(),
           self._algorithm.getParameter( 'cylinderNetworkAxis2Y' ).readValue(),
           self._algorithm.getParameter( 'cylinderNetworkAxis2Z' ).readValue() ]

      cylinderNetworkSpacing1 = self._algorithm.getParameter(
                                         'cylinderNetworkSpacing1' ).readValue()
      cylinderNetworkSpacing2 = self._algorithm.getParameter(
                                         'cylinderNetworkSpacing2' ).readValue()

      cylinderNetworkCount1 = self._algorithm.getParameter(
                                           'cylinderNetworkCount1' ).readValue()
      cylinderNetworkCount2 = self._algorithm.getParameter(
                                           'cylinderNetworkCount2' ).readValue()
      randomRotationOfCylinders = self._algorithm.getParameter(
                                       'randomRotationOfCylinders' ).readValue()

      # Writing the elementary dictionary
      cylinderParameters[ str( cylinderName ) ][ str( 'parameters' ) ]\
                        [ 'network_axis1' ] = cylinderNetworkAxis1
      cylinderParameters[ str( cylinderName ) ][ str( 'parameters' ) ]\
                        [ 'network_axis2' ] = cylinderNetworkAxis2
      cylinderParameters[ str( cylinderName ) ][ str( 'parameters' ) ]\
                        [ 'network_spacing1' ] = cylinderNetworkSpacing1
      cylinderParameters[ str( cylinderName ) ][ str( 'parameters' ) ]\
                        [ 'network_spacing2' ] = cylinderNetworkSpacing2
      cylinderParameters[ str( cylinderName ) ][ str( 'parameters' ) ]\
                        [ 'network_count1' ] = cylinderNetworkCount1
      cylinderParameters[ str( cylinderName ) ][ str( 'parameters' ) ]\
                        [ 'network_count2' ] = cylinderNetworkCount2
      cylinderParameters[ str( cylinderName ) ][ str( 'parameters' ) ]\
                        [ 'random_rotation' ] = randomRotationOfCylinders

    # Exporting the elementary dictionary
    return cylinderParameters


  def collectEllipsoidGeometryParameters( self ):

    # Getting the network type
    ellipsoidNetwork = self._algorithm.getParameter(
                                                'ellipsoidNetwork' ).readValue()

    # Creating the elementary dictionary
    ellipsoidName = self._algorithm.getParameter(
                                          'elementaryGeometryName' ).readValue()
    ellipsoidParameters = {}
    ellipsoidParameters[ str( ellipsoidName ) ] = {}
    ellipsoidParameters[ str( ellipsoidName ) ][ str( 'type' ) ] = 'ellipsoid'
    ellipsoidParameters[ str( ellipsoidName ) ][ str( 'parameters' ) ] = {}

    # Collecting the parameters and values
    ellipsoidCenter = [
                self._algorithm.getParameter( 'ellipsoidCenterX' ).readValue(),
                self._algorithm.getParameter( 'ellipsoidCenterY' ).readValue(),
                self._algorithm.getParameter( 'ellipsoidCenterZ' ).readValue() ]

    ellipsoidEigenVector1 = [
          self._algorithm.getParameter( 'ellipsoidEigenVector1X' ).readValue(),
          self._algorithm.getParameter( 'ellipsoidEigenVector1Y' ).readValue(),
          self._algorithm.getParameter( 'ellipsoidEigenVector1Z' ).readValue() ]
    ellipsoidEigenVector2 = [
          self._algorithm.getParameter( 'ellipsoidEigenVector2X' ).readValue(),
          self._algorithm.getParameter( 'ellipsoidEigenVector2Y' ).readValue(),
          self._algorithm.getParameter( 'ellipsoidEigenVector2Z' ).readValue() ]
    ellipsoidEigenVector3 = [
          self._algorithm.getParameter( 'ellipsoidEigenVector3X' ).readValue(),
          self._algorithm.getParameter( 'ellipsoidEigenVector3Y' ).readValue(),
          self._algorithm.getParameter( 'ellipsoidEigenVector3Z' ).readValue() ]

    ellipsoidEigenValue1 = self._algorithm.getParameter(
                                            'ellipsoidEigenValue1' ).readValue()
    ellipsoidEigenValue2 = self._algorithm.getParameter(
                                            'ellipsoidEigenValue2' ).readValue()
    ellipsoidEigenValue3 = self._algorithm.getParameter(
                                            'ellipsoidEigenValue3' ).readValue()
    ellipsoidVextexCount = self._algorithm.getParameter(
                                            'ellipsoidVextexCount' ).readValue()
    randomRotationOfEllipsoid = self._algorithm.getParameter(
                                       'randomRotationOfEllipsoid' ).readValue()

    # Writing the elementary dictionary
    ellipsoidParameters[ str( ellipsoidName ) ][ str( 'parameters' ) ]\
                       [ 'center' ] = ellipsoidCenter
    ellipsoidParameters[ str( ellipsoidName ) ][ str( 'parameters' ) ]\
                       [ 'eigenvector1' ] = ellipsoidEigenVector1
    ellipsoidParameters[ str( ellipsoidName ) ][ str( 'parameters' ) ]\
                       [ 'eigenvector2' ] = ellipsoidEigenVector2
    ellipsoidParameters[ str( ellipsoidName ) ][ str( 'parameters' ) ]\
                       [ 'eigenvector3' ] = ellipsoidEigenVector3
    ellipsoidParameters[ str( ellipsoidName ) ][ str( 'parameters' ) ]\
                       [ 'radius1' ] = ellipsoidEigenValue1
    ellipsoidParameters[ str( ellipsoidName ) ][ str( 'parameters' ) ]\
                       [ 'radius2' ] = ellipsoidEigenValue2
    ellipsoidParameters[ str( ellipsoidName ) ][ str( 'parameters' ) ]\
                       [ 'radius3' ] = ellipsoidEigenValue3
    ellipsoidParameters[ str( ellipsoidName ) ][ str( 'parameters' ) ]\
                       [ 'sphere_vertex_count' ] = ellipsoidVextexCount
    ellipsoidParameters[ str( ellipsoidName ) ][ str( 'parameters' ) ]\
                       [ 'random_rotation' ] = randomRotationOfEllipsoid

    if ( ellipsoidNetwork == 2 ):

      # Updating only the type of the geometry
      ellipsoidParameters[ str( ellipsoidName ) ]\
                         [ str( 'type' ) ] = 'network_of_ellipsoids'

      # Collecting the newtwork parameters and values
      randomRotationOfEllipsoids = self._algorithm.getParameter(
                                      'randomRotationOfEllipsoids' ).readValue()
      ellipsoidNetworkAxis1 = [
          self._algorithm.getParameter( 'ellipsoidNetworkAxis1X' ).readValue(),
          self._algorithm.getParameter( 'ellipsoidNetworkAxis1Y' ).readValue(),
          self._algorithm.getParameter( 'ellipsoidNetworkAxis1Z' ).readValue() ]
      ellipsoidNetworkAxis2 = [
          self._algorithm.getParameter( 'ellipsoidNetworkAxis2X' ).readValue(),
          self._algorithm.getParameter( 'ellipsoidNetworkAxis2Y' ).readValue(),
          self._algorithm.getParameter( 'ellipsoidNetworkAxis2Z' ).readValue() ]
      ellipsoidNetworkAxis3 = [
          self._algorithm.getParameter( 'ellipsoidNetworkAxis3X' ).readValue(),
          self._algorithm.getParameter( 'ellipsoidNetworkAxis3Y' ).readValue(),
          self._algorithm.getParameter( 'ellipsoidNetworkAxis3Z' ).readValue() ]

      ellipsoidNetworkSpacing1 = self._algorithm.getParameter(
                                        'ellipsoidNetworkSpacing1' ).readValue()
      ellipsoidNetworkSpacing2 = self._algorithm.getParameter(
                                        'ellipsoidNetworkSpacing2' ).readValue()
      ellipsoidNetworkSpacing3 = self._algorithm.getParameter(
                                        'ellipsoidNetworkSpacing3' ).readValue()

      ellipsoidNetworkCount1 = self._algorithm.getParameter(
                                          'ellipsoidNetworkCount1' ).readValue()
      ellipsoidNetworkCount2 = self._algorithm.getParameter(
                                          'ellipsoidNetworkCount2' ).readValue()
      ellipsoidNetworkCount3 = self._algorithm.getParameter(
                                          'ellipsoidNetworkCount3' ).readValue()
      randomRotationOfEllipsoids = self._algorithm.getParameter(
                                      'randomRotationOfEllipsoids' ).readValue()

      # Writing the elementary dictionary
      ellipsoidParameters[ str( ellipsoidName ) ][ str( 'parameters' ) ]\
                         [ 'network_axis1' ] = ellipsoidNetworkAxis1
      ellipsoidParameters[ str( ellipsoidName ) ][ str( 'parameters' ) ]\
                         [ 'network_axis2' ] = ellipsoidNetworkAxis2
      ellipsoidParameters[ str( ellipsoidName ) ][ str( 'parameters' ) ]\
                         [ 'network_axis3' ] = ellipsoidNetworkAxis3

      ellipsoidParameters[ str( ellipsoidName ) ][ str( 'parameters' ) ]\
                         [ 'network_spacing1' ] = ellipsoidNetworkSpacing1
      ellipsoidParameters[ str( ellipsoidName ) ][ str( 'parameters' ) ]\
                         [ 'network_spacing2' ] = ellipsoidNetworkSpacing2
      ellipsoidParameters[ str( ellipsoidName ) ][ str( 'parameters' ) ]\
                         [ 'network_spacing3' ] = ellipsoidNetworkSpacing3

      ellipsoidParameters[ str( ellipsoidName ) ][ str( 'parameters' ) ]\
                         [ 'network_count1' ] = ellipsoidNetworkCount1
      ellipsoidParameters[ str( ellipsoidName ) ][ str( 'parameters' ) ]\
                         [ 'network_count2' ] = ellipsoidNetworkCount2
      ellipsoidParameters[ str( ellipsoidName ) ][ str( 'parameters' ) ]\
                         [ 'network_count3' ] = ellipsoidNetworkCount3
      ellipsoidParameters[ str( ellipsoidName ) ][ str( 'parameters' ) ]\
                         [ 'random_rotation' ] = randomRotationOfEllipsoids

    # Exporting the elementary dictionary
    return ellipsoidParameters


  def collectFiberGeometryParameters( self ):

    # Getting the network type
    fiberNetwork = self._algorithm.getParameter( 'fiberNetwork' ).readValue()

    # Creating the elementary dictionary
    fiberName = self._algorithm.getParameter(
                                          'elementaryGeometryName' ).readValue()
    fiberParameters = {}
    fiberParameters[ str( fiberName ) ] = {}
    fiberParameters[ str( fiberName ) ][ str( 'type' ) ] = 'fiber'
    fiberParameters[ str( fiberName ) ][ str( 'parameters' ) ] = {}

    listOfPoints = list()
    listOfRadii = list()

    # Collecting the parameters and values
    if ( self._algorithm.getParameter( 'fiberManualChoice' ).readValue() == 1 ):

      entryCount = self._algorithm.getParameter(
                                        'listOfPointsAndRadii' ).getEntryCount()
      currentPoint = list()
      currentCoordinate = 0.0
      for entryIndex in range( entryCount ):

        for labelIndex in range( 3 ):

          currentCoordinate = \
                   float( self._algorithm.getParameter( 'listOfPointsAndRadii' )
                                         .getValueForEntryIndexAndLabelIndex(
                                                      entryIndex, labelIndex ) )
          currentPoint.append( currentCoordinate )

        listOfPoints.append( currentPoint )
        currentPoint = []
        listOfRadii.append( float( self._algorithm
                                       .getParameter( 'listOfPointsAndRadii' )
                                       .getValueForEntryIndexAndLabelIndex(
                                                             entryIndex, 3 ) ) )

    elif ( self._algorithm.getParameter(
                                    'fiberInputFileChoice' ).readValue() == 1 ):

      fiberSegmentsProperties = self.loadFiberFile()

      listOfPoints = fiberSegmentsProperties.get( 'points' )
      listOfRadii = fiberSegmentsProperties.get( 'radii' )


    fiberVertexCountPerPlane = self._algorithm.getParameter(
                                        'fiberVertexCountPerPlane' ).readValue()
    randomRotationOfFiber = self._algorithm.getParameter(
                                           'randomRotationOfFiber' ).readValue()

    # Writing the elementary dictionary
    fiberParameters[ str( fiberName ) ][ str( 'parameters' ) ]\
                   [ 'points' ] = listOfPoints
    fiberParameters[ str( fiberName ) ][ str( 'parameters' ) ]\
                   [ 'radii' ] = listOfRadii
    fiberParameters[ str( fiberName ) ][ str( 'parameters' ) ]\
                   [ 'vertex_count_per_plane' ] = fiberVertexCountPerPlane
    fiberParameters[ str( fiberName ) ][ str( 'parameters' ) ]\
                   [ 'random_rotation' ] = randomRotationOfFiber

    if ( fiberNetwork == 2 ):

      # Updating only the type of the geometry
      fiberParameters[ str( fiberName ) ][ str( 'type' ) ] = 'network_of_fibers'

      # Collecting the newtwork parameters and values
      fiberNetworkAxis1 = [
              self._algorithm.getParameter( 'fiberNetworkAxis1X' ).readValue(),
              self._algorithm.getParameter( 'fiberNetworkAxis1Y' ).readValue(), 
              self._algorithm.getParameter( 'fiberNetworkAxis1Z' ).readValue() ]
      fiberNetworkAxis2 = [
              self._algorithm.getParameter( 'fiberNetworkAxis2X' ).readValue(),
              self._algorithm.getParameter( 'fiberNetworkAxis2Y' ).readValue(), 
              self._algorithm.getParameter( 'fiberNetworkAxis2Z' ).readValue() ]

      fiberNetworkSpacing1 = self._algorithm.getParameter(
                                            'fiberNetworkSpacing1' ).readValue()
      fiberNetworkSpacing2 = self._algorithm.getParameter(
                                            'fiberNetworkSpacing2' ).readValue()

      fiberNetworkCount1 = self._algorithm.getParameter(
                                              'fiberNetworkCount1' ).readValue()
      fiberNetworkCount2 = self._algorithm.getParameter(
                                              'fiberNetworkCount2' ).readValue()
      randomRotationOfFibers = self._algorithm.getParameter(
                                          'randomRotationOfFibers' ).readValue()

      # Writing the elementary dictionary
      fiberParameters[ str( fiberName ) ][ str( 'parameters' ) ]\
                     [ 'network_axis1' ] = fiberNetworkAxis1
      fiberParameters[ str( fiberName ) ][ str( 'parameters' ) ]\
                     [ 'network_axis2' ] = fiberNetworkAxis2

      fiberParameters[ str( fiberName ) ][ str( 'parameters' ) ]\
                     [ 'network_spacing1' ] = fiberNetworkSpacing1
      fiberParameters[ str( fiberName ) ][ str( 'parameters' ) ]\
                     [ 'network_spacing2' ] = fiberNetworkSpacing2

      fiberParameters[ str( fiberName ) ][ str( 'parameters' ) ]\
                     [ 'network_count1' ] = fiberNetworkCount1
      fiberParameters[ str( fiberName ) ][ str( 'parameters' ) ]\
                     [ 'network_count2' ] = fiberNetworkCount2
      fiberParameters[ str( fiberName ) ][ str( 'parameters' ) ]\
                     [ 'random_rotation' ] = randomRotationOfFibers

    # Exporting the elementary dictionary
    return fiberParameters


  def collectSphereGeometryParameters( self ):

    # Getting the network type
    sphereNetwork = self._algorithm.getParameter( 'sphereNetwork' ).readValue()

    # Creating the elementary dictionary
    sphereName = self._algorithm.getParameter(
                                          'elementaryGeometryName' ).readValue()
    sphereParameters = {}
    sphereParameters[ str( sphereName ) ] = {}
    sphereParameters[ str( sphereName ) ][ str( 'type' ) ] = 'sphere'
    sphereParameters[ str( sphereName ) ][ str( 'parameters' ) ] = {}

    # Collecting the parameters and values
    sphereCenter = [ 
                   self._algorithm.getParameter( 'sphereCenterX' ).readValue(),
                   self._algorithm.getParameter( 'sphereCenterY' ).readValue(),
                   self._algorithm.getParameter( 'sphereCenterZ' ).readValue() ]
    sphereRadius = self._algorithm.getParameter( 'sphereRadius' ).readValue()
    sphereVextexCount  = self._algorithm.getParameter(
                                               'sphereVextexCount' ).readValue()

    # Writing the elementary dictionary
    sphereParameters[ str( sphereName ) ][ str( 'parameters' ) ]\
                    [ 'center' ] = sphereCenter
    sphereParameters[ str( sphereName ) ][ str( 'parameters' ) ]\
                    [ 'radius' ] = sphereRadius
    sphereParameters[ str( sphereName ) ][ str( 'parameters' ) ]\
                    [ 'sphere_vertex_count' ] = sphereVextexCount

    if ( sphereNetwork == 2 ):

      # Updating only the type of the geometry
      sphereParameters[ str( sphereName ) ]\
                      [ str( 'type' ) ] ='network_of_spheres'

      # Collecting the newtwork parameters and values
      sphereNetworkAxis1 = [
             self._algorithm.getParameter( 'sphereNetworkAxis1X' ).readValue(),
             self._algorithm.getParameter( 'sphereNetworkAxis1Y' ).readValue(),
             self._algorithm.getParameter( 'sphereNetworkAxis1Z' ).readValue() ]
      sphereNetworkAxis2 = [
             self._algorithm.getParameter( 'sphereNetworkAxis2X' ).readValue(),
             self._algorithm.getParameter( 'sphereNetworkAxis2Y' ).readValue(),
             self._algorithm.getParameter( 'sphereNetworkAxis2Z' ).readValue() ]
      sphereNetworkAxis3 = [
             self._algorithm.getParameter( 'sphereNetworkAxis3X' ).readValue(),
             self._algorithm.getParameter( 'sphereNetworkAxis3Y' ).readValue(),
             self._algorithm.getParameter( 'sphereNetworkAxis3Z' ).readValue() ]

      sphereNetworkSpacing1 = self._algorithm.getParameter(
                                           'sphereNetworkSpacing1' ).readValue()
      sphereNetworkSpacing2 = self._algorithm.getParameter(
                                           'sphereNetworkSpacing2' ).readValue()
      sphereNetworkSpacing3 = self._algorithm.getParameter(
                                           'sphereNetworkSpacing3' ).readValue()

      sphereNetworkCount1 = self._algorithm.getParameter(
                                             'sphereNetworkCount1' ).readValue()
      sphereNetworkCount2 = self._algorithm.getParameter(
                                             'sphereNetworkCount2' ).readValue()
      sphereNetworkCount3 = self._algorithm.getParameter(
                                             'sphereNetworkCount3' ).readValue()

      # Writing the elementary dictionary
      sphereParameters[ str( sphereName ) ][ str( 'parameters' ) ]\
                      [ 'network_axis1' ] = sphereNetworkAxis1
      sphereParameters[ str( sphereName ) ][ str( 'parameters' ) ]\
                      [ 'network_axis2' ] = sphereNetworkAxis2
      sphereParameters[ str( sphereName ) ][ str( 'parameters' ) ]\
                      [ 'network_axis3' ] = sphereNetworkAxis3

      sphereParameters[ str( sphereName ) ][ str( 'parameters' ) ]\
                      [ 'network_spacing1' ] = sphereNetworkSpacing1
      sphereParameters[ str( sphereName ) ][ str( 'parameters' ) ]\
                      [ 'network_spacing2' ] = sphereNetworkSpacing2
      sphereParameters[ str( sphereName ) ][ str( 'parameters' ) ]\
                      [ 'network_spacing3' ] = sphereNetworkSpacing3

      sphereParameters[ str( sphereName ) ][ str( 'parameters' ) ]\
                      [ 'network_count1' ] = sphereNetworkCount1
      sphereParameters[ str( sphereName ) ][ str( 'parameters' ) ]\
                      [ 'network_count2' ] = sphereNetworkCount2
      sphereParameters[ str( sphereName ) ][ str( 'parameters' ) ]\
                      [ 'network_count3' ] = sphereNetworkCount3

    # Exporting the elementary dictionary
    return sphereParameters


  def collectStarGeometryParameters( self ):

    # Getting the network type
    starNetwork = self._algorithm.getParameter( 'starNetwork' ).readValue()

    # Creating the elementary dictionary
    starName = self._algorithm.getParameter(
                                          'elementaryGeometryName' ).readValue()
    starParameters = {}
    starParameters[ str( starName ) ] = {}
    starParameters[ str( starName ) ][ str( 'type' ) ] = 'star'
    starParameters[ str( starName ) ][ str( 'parameters' ) ] = {}

    # Collecting the parameters and values
    starCenter = [ self._algorithm.getParameter( 'starCenterX' ).readValue(),
                   self._algorithm.getParameter( 'starCenterY' ).readValue(),
                   self._algorithm.getParameter( 'starCenterZ' ).readValue() ]

    starRadius = self._algorithm.getParameter( 'starRadius' ).readValue()
    starPeakCount = self._algorithm.getParameter( 'starPeakCount' ).readValue()

    starPeakMagnitudes = self.loadStarPeakProperties(
               starPeakCount,
               self._algorithm.getParameter( 'starPeakMagnitudes' ).getValue() )
    starPeakApertureAngles = self.loadStarPeakProperties(
           starPeakCount,
           self._algorithm.getParameter( 'starPeakApertureAngles' ).getValue() )

    starVextexCount = self._algorithm.getParameter(
                                                 'starVextexCount' ).readValue()
    starSharpPeak = self._algorithm.getParameter( 'starSharpPeak' ).readValue()
    starSymmetricPeak = self._algorithm.getParameter(
                                               'starSymmetricPeak' ).readValue()
    randomRotationOfStar = self._algorithm.getParameter(
                                            'randomRotationOfStar' ).readValue()

    # Writing the elementary dictionary
    starParameters[ str( starName ) ][ str( 'parameters' ) ]\
                  [ 'center' ] = starCenter
    starParameters[ str( starName ) ][ str( 'parameters' ) ]\
                  [ 'radius' ] = starRadius
    starParameters[ str( starName ) ][ str( 'parameters' ) ]\
                  [ 'peak_count' ] = starPeakCount
    starParameters[ str( starName ) ][ str( 'parameters' ) ]\
                  [ 'peak_magnitudes' ] = starPeakMagnitudes
    starParameters[ str( starName ) ][ str( 'parameters' ) ]\
                  [ 'peak_aperture_angles_in_degrees' ] = starPeakApertureAngles
    starParameters[ str( starName ) ][ str( 'parameters' ) ]\
                  [ 'sphere_vertex_count' ] = starVextexCount
    starParameters[ str( starName ) ][ str( 'parameters' ) ]\
                  [ 'sharp_peak' ] = starSharpPeak
    starParameters[ str( starName ) ][ str( 'parameters' ) ]\
                  [ 'symmetric_peak' ] = starSymmetricPeak
    starParameters[ str( starName ) ][ str( 'parameters' ) ]\
                  [ 'random_rotation' ] = randomRotationOfStar

    if ( starNetwork == 2 ):

      # Updating only the type of the geometry
      starParameters[ str( starName ) ][ str( 'type' ) ] = 'network_of_stars'

      # Collecting the newtwork parameters and values
      starNetworkAxis1 = [
               self._algorithm.getParameter( 'starNetworkAxis1X' ).readValue(),
               self._algorithm.getParameter( 'starNetworkAxis1Y' ).readValue(),
               self._algorithm.getParameter( 'starNetworkAxis1Z' ).readValue() ]
      starNetworkAxis2 = [
               self._algorithm.getParameter( 'starNetworkAxis2X' ).readValue(),
               self._algorithm.getParameter( 'starNetworkAxis2Y' ).readValue(),
               self._algorithm.getParameter( 'starNetworkAxis2Z' ).readValue() ]
      starNetworkAxis3 = [
               self._algorithm.getParameter( 'starNetworkAxis3X' ).readValue(),
               self._algorithm.getParameter( 'starNetworkAxis3Y' ).readValue(),
               self._algorithm.getParameter( 'starNetworkAxis3Z' ).readValue() ]

      starNetworkSpacing1 = self._algorithm.getParameter(
                                             'starNetworkSpacing1' ).readValue()
      starNetworkSpacing2 = self._algorithm.getParameter(
                                             'starNetworkSpacing2' ).readValue()
      starNetworkSpacing3 = self._algorithm.getParameter(
                                             'starNetworkSpacing3' ).readValue()

      starNetworkCount1 = self._algorithm.getParameter(
                                               'starNetworkCount1' ).readValue()
      starNetworkCount2 = self._algorithm.getParameter(
                                               'starNetworkCount2' ).readValue()
      starNetworkCount3 = self._algorithm.getParameter(
                                               'starNetworkCount3' ).readValue()

      randomRotationOfStars = self._algorithm.getParameter(
                                           'randomRotationOfStars' ).readValue()

      # Writing the elementary dictionary
      starParameters[ str( starName ) ][ str( 'parameters' ) ]\
                    [ 'network_axis1' ] = starNetworkAxis1
      starParameters[ str( starName ) ][ str( 'parameters' ) ]\
                    [ 'network_axis2' ] = starNetworkAxis2
      starParameters[ str( starName ) ][ str( 'parameters' ) ]\
                    [ 'network_axis3' ] = starNetworkAxis3

      starParameters[ str( starName ) ][ str( 'parameters' ) ]\
                    [ 'network_spacing1' ] = starNetworkSpacing1
      starParameters[ str( starName ) ][ str( 'parameters' ) ]\
                    [ 'network_spacing2' ] = starNetworkSpacing2
      starParameters[ str( starName ) ][ str( 'parameters' ) ]\
                    [ 'network_spacing3' ] = starNetworkSpacing3

      starParameters[ str( starName ) ][ str( 'parameters' ) ]\
                    [ 'network_count1' ] = starNetworkCount1
      starParameters[ str( starName ) ][ str( 'parameters' ) ]\
                    [ 'network_count2' ] = starNetworkCount2
      starParameters[ str( starName ) ][ str( 'parameters' ) ]\
                    [ 'network_count3' ] = starNetworkCount3

      starParameters[ str( starName ) ][ str( 'parameters' ) ]\
                    [ 'random_rotation' ] = randomRotationOfStars

    # Exporting the elementary dictionary
    return starParameters

