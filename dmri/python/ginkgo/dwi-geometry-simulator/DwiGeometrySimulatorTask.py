from Task import *
import time

class DwiGeometrySimulatorTask( Task ):

  def __init__( self, application ):

    Task.__init__( self, application )


  ##############################################################################
  # execute()
  ##############################################################################

  def execute( self, functors, parameters, subjectName, verbose, viewOnly ):

    try:

      ####################### acquiring conditions #############################
      functors[ 'condition-acquire' ]( subjectName, 'geometry-processed' )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 0 )

      ############################ loading geometries ##########################

      outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()

      membraneFileName =  os.path.join( outputWorkDirectory, 
                                        'geometry.membrane' )
      membraneMeshFileName = os.path.join( outputWorkDirectory, 
                                           'geometry.mesh' )

      command = 'GkgExecuteCommand DwiGeometrySimulator' + \
                ' -geometry ' + membraneFileName + \
                ' -membraneMeshFileName ' + membraneMeshFileName + \
                ' -verbose true'

      executeCommand( self, subjectName, command, viewOnly )

      ####################### notifying display thread #########################
      functors[ 'condition-notify-and-release' ]( subjectName,
                                                  'geometry-processed' )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 100 )

    except:

      self._functors[ 'task-set-status-as-failed' ]( subjectName,
                                                     sys.exc_info() )


  ##############################################################################
  # display()
  ##############################################################################

  def display( self, functors, parameters, subjectName, verbose,
               isReportModeActive ):

    ########################### resetting the viewer #########################
    functors[ 'viewer-reset' ]()

    ####################### waiting for result ################################
    functors[ 'condition-wait-and-release' ]( subjectName,
                                              'geometry-processed' )

    ############################ loading geometries ############################
    outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()
    membraneMeshFileName = os.path.join( outputWorkDirectory, 
                                         'geometry.mesh' )

    functors[ 'viewer-load-object' ]( membraneMeshFileName , 'Geometry model' )
    functors[ 'viewer-set-diffuse' ]( 'Geometry model',
                                      [ 0.7, 0.8, 0.9, 1.0 ] )
    functors[ 'viewer-add-object-to-window' ]( 'Geometry model', 
                                               'dms_geometry_model_view',
                                               'Geometry Model' )

