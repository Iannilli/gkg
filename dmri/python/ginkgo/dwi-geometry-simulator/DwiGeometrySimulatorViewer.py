from Viewer import *


class DwiGeometrySimulatorViewer( Viewer ):

  def __init__( self, minimumSize, parent ):

    Viewer.__init__( self, minimumSize, parent )

    ######################### First View ######################################

    self.createView( 'dms_geometry_model_view' )

    self.add3DWindow( 'dms_geometry_model_view', 0, 0, 1, 1,
                      'Geometry Model',
                      'Geometry Model',
                      [ 0, 0, 0, 1 ],
                      True,
                      False,
                      True,
                      False )


def createDwiGeometrySimulatorViewer( minimumSize, parent ):

  return DwiGeometrySimulatorViewer( minimumSize, parent )

