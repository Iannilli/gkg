from Algorithm import *
from DwiLocalModelingTask import *


class DwiLocalModelingAlgorithm( Algorithm ):

  def __init__( self, verbose ):

    Algorithm.__init__( self, 'DWI-Local-Modeling', verbose, True )
    
    # Input data
    self.addParameter( StringParameter( 'fileNameT2', '' ) )
    self.addParameter( StringParameter( 'fileNameDw', '' ) )
    self.addParameter( StringParameter( 'fileNameMask', '' ) )
    self.addParameter( StringParameter( 'fileNameT1', '' ) )
    self.addParameter( StringParameter( 'fileNameTransformationDwToT1', '' ) )
    
    # Local model parameters
    self.addParameter( ChoiceParameter(
                            'odfType',
                            0, 
                            [ 'Diffusion Orientation Transform (DOT)',
                              'Diffusion Spectrum Imaging (DSI)',
                              'Q-Ball Imaging (QBI)',
                              'Spherical Deconvolution (SD)',
                              'Sharpening Deconvolution Transform (SDT)',
                              'analytical Q-Ball Imaging (aQBI)',
                              'solid angle analytical Q-Ball Imaging (sa-aQBI)',
                              'Diffusion Tensor Imaging (DTI)' ] ) )
                                          
    # DOT model type
    self.addParameter( IntegerParameter( 'dotMaximumSHOrder', 4, 2, 20, 2 ) )	
    self.addParameter( DoubleParameter( 'dotEffectiveDiffusionTime',
                                        25, 0, 500, 1 ) ) 
    self.addParameter( DoubleParameter( 'dotR0',
                                        12, 1, 50, 1 ) )	
    self.addParameter( BooleanParameter( 'dotOdfComputation', 2 ) )

    # DSI model type   
    self.addParameter( DoubleParameter( 'dsiMinimumR0', 1, 1, 50, 1 ) )
    self.addParameter( DoubleParameter( 'dsiMaximumR0', 15, 1, 50, 1 ) )
    self.addParameter( BooleanParameter( 'dsiFilteringDataBeforeFFT', 2 ) )
    self.addParameter( BooleanParameter( 'dsiMarginalOdf', 2 ) )

    # OBI model type
    self.addParameter( IntegerParameter( 'qbiEquatorPointCount',
                                         50, 1, 100, 1 ) )
    self.addParameter( DoubleParameter( 'qbiPhiFunctionAngle', 0, 0, 360, 5 ) )
    self.addParameter( DoubleParameter( 'qbiPhiFunctionMaximumAngle',
                                        0, 0, 360, 5 ) )
    self.addParameter( ChoiceParameter( 'qbiPhiFunctionType',
                                        0,
                                        [ 'gaussian', 
                                          'multiquadric', 
                                          'thinplate' ] ) )

    # SD model type
    self.addParameter( IntegerParameter( 'sdKernelVoxelCount',
                                         300, 1, 500, 1 ) )
    self.addParameter( IntegerParameter( 'sdMaximumSHOrder', 4, 2, 20, 2 ) )
    self.addParameter( DoubleParameter( 'sdKernelLowerFAThreshold',
                                        0.65, 0, 1, 0.05 ) )
    self.addParameter( DoubleParameter( 'sdKernelUpperFAThreshold',
                                        0.85, 0, 1, 0.05 ) )
    self.addParameter( StringParameter( 'sdFilterCoefficients',
                                        '1 1 1 0.5 0.1 0.02 0.002 0.0005 '+
                                        '0.0001 0.0001 0.00001 0.00001 ' +
                                        '0.00001 0.00001 0.00001 0.00001 ' +
                                        '0.00001', False,
                                        False ) ),
    self.addParameter( ChoiceParameter( 'sdKernelType',
                                        0,
                                        [ 'symmetric_tensor',
                                          'normal' ] ) )
    self.addParameter( BooleanParameter( 'sdUseCSD', 0 ) )
    
    # SDT model type
    self.addParameter( IntegerParameter( 'sdtKernelVoxelCount',
                                         300, 1, 500, 1 ) )
    self.addParameter( IntegerParameter( 'sdtMaximumSHOrder', 4, 2, 20, 2 ) )
    self.addParameter( DoubleParameter( 'sdtKernelLowerFAThreshold',
                                        0.65, 0, 1, 0.05 ) )
    self.addParameter( DoubleParameter( 'sdtKernelUpperFAThreshold',
                                        0.85, 0, 1, 0.05 ) )
    self.addParameter( DoubleParameter( 'sdtRegularizationLcurveFactor',
                                        0.006, 0, 1, 0.001 ) )
    self.addParameter( ChoiceParameter( 'sdtKernelType',
                                        0,
                                        [ 'symmetric_tensor',
                                          'normal' ] ) )
    self.addParameter( BooleanParameter( 'sdtUseCSD', 0 ) )
    
    # aQBI model type
    self.addParameter( IntegerParameter( 'aqbiMaximumSHOrder', 4, 2, 20, 2 ) ) 
    self.addParameter( DoubleParameter( 'aqbiRegularizationLcurveFactor',
                                        0.006, 0, 1, 0.001 ) )
    self.addParameter( DoubleParameter( 'aqbiLaplaceBeltramiSharpeningFactor',
                                        0.0, 0.0, 1.0, 0.05 ) )
    
    # sa-aQBI model type
    self.addParameter( IntegerParameter( 'saAqbiMaximumSHOrder', 4, 2, 20, 2 ) ) 
    self.addParameter( DoubleParameter( 'saAqbiRegularizationLcurveFactor',
                                        0.006, 0, 1, 0.001 ) )
    self.addParameter( DoubleParameter( 'saAqbiLaplaceBeltramiSharpeningFactor',
                                        0.0, 0.0, 1.0, 0.05 ) )
    
    # DTI model type
    self.addParameter( ChoiceParameter( 'dtiEstimatorType',
                                        0,
                                        ( 'linear_square',
                                          'robust_positive_definite' ) ) )
    # RGB scale
    self.addParameter( DoubleParameter( 'rgbScale', 1.0, 1, 100, 0.1 ) )	

    # output orientation count
    self.addParameter( IntegerParameter( 'outputOrientationCount',
                                          500, 1, 4000, 1 ) )	

    # compute ODF volume
    self.addParameter( BooleanParameter( 'computeOdfVolume', 0 ) )

    # View	    			
    self.addParameter( ChoiceParameter( 'viewType',
                                        0,
                                        [ 'DOT view',
                                          'DSI view',
                                          'QBI view',
                                          'SD view',
                                          'SDT view',
                                          'aQBI view',
                                          'sa-aQBI view',
                                          'DTI view' ] ) )
        
    	
  def launch( self ):
  
    if ( self._verbose ):
    
      print \
        'running DWI local modeling'

    defaultViewIndex = int( self._application.getAlgorithmGUI().getAlgorithm().\
                                      getParameter( 'odfType' ).getValue() ) + 1
    task = DwiLocalModelingTask( self._application, defaultViewIndex )
    task.launch( False )


  def view( self ):
  
    if ( self._verbose ):
    
      print \
        'viewing DWI local modeling'

    defaultViewIndex = int( self._application.getAlgorithmGUI().getAlgorithm().\
                                      getParameter( 'odfType' ).getValue() ) + 1
    task = DwiLocalModelingTask( self._application, defaultViewIndex )
    task.launch( True )

