from AlgorithmGUI import *
from ViewerCallbackManager import *
from ResourceManager import *


class DwiLocalModelingAlgorithmGUI( AlgorithmGUI ):

  def __init__( self, algorithm ):

    AlgorithmGUI.__init__( self,
                           algorithm,
                           ResourceManager().getUIFileName(
                                                       'dmri',
                                                       'DwiLocalModeling.ui' ) )

    ############################################################################
    # creating a viewer callback manager
    ############################################################################

    self._viewerCallbackManager = ViewerCallbackManager( self.getAlgorithm() )
			   
    ############################################################################
    # connecting data interface
    ############################################################################
    
    # Input T2 (b=0) file name and push button
    self.connectStringParameterToLineEdit( 'fileNameT2',
                                           'lineEdit_FileNameT2' )
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit(
                                                        'pushButton_FileNameT2',
                                                        'lineEdit_FileNameT2' )
     
    # Input DW file name and push button
    self.connectStringParameterToLineEdit( 'fileNameDw', 'lineEdit_FileNameDw' )
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit(
                                                        'pushButton_FileNameDw',
                                                        'lineEdit_FileNameDw' )
     
    # Input Mask file name and push button
    self.connectStringParameterToLineEdit( 'fileNameMask',
                                           'lineEdit_FileNameMask' )
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit(
                                                      'pushButton_FileNameMask',
                                                      'lineEdit_FileNameMask' )
                                                        
    # Input T1 file name and push button
    self.connectStringParameterToLineEdit( 'fileNameT1', 'lineEdit_FileNameT1' )
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit(
                                                        'pushButton_FileNameT1',
                                                        'lineEdit_FileNameT1' )
							    
    # Input DW => T1 3D Transform file name and push button
    self.connectStringParameterToLineEdit(
                                      'fileNameTransformationDwToT1',
                                      'lineEdit_FileNameTransformationDwToT1' )
    self.connectFileBrowserToPushButtonAndLineEdit(
                                      'pushButton_FileNameTransformationDwToT1',
                                      'lineEdit_FileNameTransformationDwToT1' )
    
    ############################################################################
    # Connecting local model parameters interface
    ############################################################################

    # Connecting ODF model type
    self.connectChoiceParameterToComboBox( 'odfType',
                                           'comboBox_OdfType' )
    self.connectComboBoxToStackedWidget( 'comboBox_OdfType',
                                         'stackedWidget_OdfType' )
    self.connectComboBoxToCustomCallback( 'comboBox_OdfType',
                                  self._viewerCallbackManager.setViewFromIndex )
    self.connectComboBoxToComboBox( 'comboBox_OdfType', 'comboBox_ViewType' )

    # Connecting the DOT model					
    self.connectIntegerParameterToSpinBox( 'dotMaximumSHOrder', 
                                           'spinBox_DotMaximumSHOrder' )
    self.connectDoubleParameterToSpinBox(
                                      'dotEffectiveDiffusionTime',
                                      'doubleSpinBox_DotEffectiveDiffusionTime')
    self.connectDoubleParameterToSpinBox( 'dotR0',
                                          'doubleSpinBox_DotR0' )
    self.connectBooleanParameterToCheckBox( 'dotOdfComputation',
                                            'checkBox_DotOdfComputation' )

    # Connecting DSI model type
    self.connectDoubleParameterToSpinBox( 'dsiMinimumR0',
                                          'doubleSpinBox_DsiMinimumR0' )
    self.connectDoubleParameterToSpinBox( 'dsiMaximumR0',
                                          'doubleSpinBox_DsiMaximumR0' )
    self.connectBooleanParameterToCheckBox(
                                          'dsiFilteringDataBeforeFFT',
                                          'checkBox_DsiFilteringDataBeforeFFT' )
    self.connectBooleanParameterToCheckBox( 'dsiMarginalOdf',
                                            'checkBox_DsiMarginalOdf' )

    # Connecting OBI model type
    self.connectIntegerParameterToSpinBox( 'qbiEquatorPointCount',
                                           'spinBox_QbiEquatorPointCount' )
    self.connectDoubleParameterToSpinBox( 'qbiPhiFunctionAngle',
                                          'doubleSpinBox_QbiPhiFunctionAngle' )
    self.connectDoubleParameterToSpinBox(
                                    'qbiPhiFunctionMaximumAngle',
                                    'doubleSpinBox_QbiPhiFunctionMaximumAngle' )
    self.connectChoiceParameterToComboBox( 'qbiPhiFunctionType',
                                           'comboBox_QbiPhiFunctionType' )
                                           
    # Connecting SD model type
    self.connectIntegerParameterToSpinBox( 'sdKernelVoxelCount',
                                           'spinBox_SdKernelVoxelCount' )
    self.connectIntegerParameterToSpinBox( 'sdMaximumSHOrder',
                                           'spinBox_SdMaximumSHOrder' )
    self.connectDoubleParameterToSpinBox(
                                       'sdKernelLowerFAThreshold',
                                       'doubleSpinBox_SdKernelLowerFAThreshold')
    self.connectDoubleParameterToSpinBox(
                                       'sdKernelUpperFAThreshold',
                                       'doubleSpinBox_SdKernelUpperFAThreshold')
    self.connectStringParameterToLineEdit( 'sdFilterCoefficients',
                                           'lineEdit_SdFilterCoefficients' )
    self.connectChoiceParameterToComboBox( 'sdKernelType',
                                           'comboBox_SdKernelType' )
    self.connectBooleanParameterToCheckBox( 'sdUseCSD',
                                            'checkBox_SdUseCSD' )

    # Connecting SDT model type
    self.connectIntegerParameterToSpinBox( 'sdtKernelVoxelCount',
                                           'spinBox_SdtKernelVoxelCount' )
    self.connectIntegerParameterToSpinBox( 'sdtMaximumSHOrder',
                                           'spinBox_SdtMaximumSHOrder' )
    self.connectDoubleParameterToSpinBox(
                                      'sdtKernelLowerFAThreshold',
                                      'doubleSpinBox_SdtKernelLowerFAThreshold')
    self.connectDoubleParameterToSpinBox(
                                      'sdtKernelUpperFAThreshold',
                                      'doubleSpinBox_SdtKernelUpperFAThreshold')
    self.connectDoubleParameterToSpinBox( 
                                 'sdtRegularizationLcurveFactor',
                                 'doubleSpinBox_SdtRegularizationLcurveFactor' )
    self.connectChoiceParameterToComboBox( 'sdtKernelType',
                                           'comboBox_SdtKernelType' )
    self.connectBooleanParameterToCheckBox( 'sdtUseCSD',
                                            'checkBox_SdtUseCSD' )
                                            
    # Connecting aQBI model type
    self.connectIntegerParameterToSpinBox( 'aqbiMaximumSHOrder',
                                           'spinBox_aQbiMaximumSHOrder' )
    self.connectDoubleParameterToSpinBox( 
                                'aqbiRegularizationLcurveFactor',
                                'doubleSpinBox_aQbiRegularizationLcurveFactor' )
    self.connectDoubleParameterToSpinBox( 
                           'aqbiLaplaceBeltramiSharpeningFactor',
                           'doubleSpinBox_aQbiLaplaceBeltramiSharpeningFactor' )

    # Connecting sa-aQBI model type
    self.connectIntegerParameterToSpinBox( 'saAqbiMaximumSHOrder',
                                           'spinBox_saAQbiMaximumSHOrder' )
    self.connectDoubleParameterToSpinBox( 
                              'saAqbiRegularizationLcurveFactor',
                              'doubleSpinBox_saAQbiRegularizationLcurveFactor' )
    self.connectDoubleParameterToSpinBox( 
                         'saAqbiLaplaceBeltramiSharpeningFactor',
                         'doubleSpinBox_saAQbiLaplaceBeltramiSharpeningFactor' )

    # Connecting DTI model type
    self.connectChoiceParameterToComboBox( 'dtiEstimatorType',
                                           'comboBox_DtiEstimatorType' )

    # Connecting the RGB scale
    self.connectDoubleParameterToSpinBox( 'rgbScale',
                                          'doubleSpinBox_RgbScale' )

    # Connecting the output orientation count
    self.connectIntegerParameterToSpinBox( 'outputOrientationCount',
                                           'spinBox_OutputOrientationCount' )

    # Connecting the compute ODF volume
    self.connectBooleanParameterToCheckBox( 'computeOdfVolume',
                                            'checkBox_ComputeOdfVolume' )

    ############################################################################
    # Connecting viewer interface
    ############################################################################
    
    # Connecting view type
    self.connectChoiceParameterToComboBox( 'viewType', 'comboBox_ViewType' )
    self.connectComboBoxToCustomCallback( 'comboBox_ViewType',
                                  self._viewerCallbackManager.setViewFromIndex )

