from Viewer import *


class DwiLocalModelingViewer( Viewer ):

  def __init__( self, minimumSize, parent ):

    Viewer.__init__( self, minimumSize, parent )
    
    ########################## DOT View ########################################

    self.createView( 'dot_view' )
    
    self.addAxialWindow( 'dot_view', 0, 0, 1, 1,
                         'T2(b=0)', 'T2(b=0)' )
    self.addAxialWindow( 'dot_view', 0, 1, 1, 1,
                         'DW', 'DW' )
    self.addAxialWindow( 'dot_view', 0, 2, 1, 1,
                         'Nematic order', 'Nematic order' )
    self.addAxialWindow( 'dot_view', 0, 3, 1, 1,
                         'GFA', 'GFA' )

  
    self.addAxialWindow( 'dot_view', 1, 0, 1, 1,
                         'mask', 'mask' )
    self.addAxialWindow( 'dot_view', 1, 1, 1, 1,
                         'T2 + mask', 'T2 + mask' )
    self.addAxialWindow( 'dot_view', 1, 2, 1, 1,
                         'Normalized entropy', 'Normalized entropy' )
    self.addAxialWindow( 'dot_view', 1, 3, 1, 1,
                         'Mean diffusivity', 'Mean diffusivity' )


    self.addAxialWindow( 'dot_view', 2, 0, 1, 1,
                         'T1 + RGB (ax)', 'T1 + RGB (ax)',
                         [ 0.0, 0.0, 0.0, 1.0 ], False )
    self.addSagittalWindow( 'dot_view', 2, 1, 1, 1,
                            'T1 + RGB (sag)', 'T1 + RGB (sag)',
                            [ 0.0, 0.0, 0.0, 1.0 ], False )
    self.addCoronalWindow( 'dot_view', 3, 0, 1, 1,
                           'T1 + RGB (cor)', 'T1 + RGB (cor)',
                           [ 0.0, 0.0, 0.0, 1.0 ], False )
    self.addAxialWindow( 'dot_view', 3, 1, 1, 1,
                        'T1 + mask', 'T1 + mask' )

    self.addAxialWindow( 'dot_view', 2, 2, 2, 2,
                         'ODF field', 'ODF field' )

    ########################## DSI View ########################################

    self.createView( 'dsi_view' )
    
    self.addAxialWindow( 'dsi_view', 0, 0, 1, 1,
                         'T2(b=0)', 'T2(b=0)' )
    self.addAxialWindow( 'dsi_view', 0, 1, 1, 1,
                         'DW', 'DW' )
    self.addAxialWindow( 'dsi_view', 0, 2, 1, 1,
                         'Mean squared displac.', 'Mean squared displac.' )
    self.addAxialWindow( 'dsi_view', 0, 3, 1, 1,
                         'GFA', 'GFA' )

  
    self.addAxialWindow( 'dsi_view', 1, 0, 1, 1,
                         'mask', 'mask' )
    self.addAxialWindow( 'dsi_view', 1, 1, 1, 1,
                         'T2 + mask', 'T2 + mask' )
    self.addAxialWindow( 'dsi_view', 1, 2, 1, 1,
                         'Return to orig. prob.', 'Return to orig. prob.' )
    self.addAxialWindow( 'dsi_view', 1, 3, 1, 1,
                         'Mean diffusivity', 'Mean diffusivity' )


    self.addAxialWindow( 'dsi_view', 2, 0, 1, 1,
                         'T1 + RGB (ax)', 'T1 + RGB (ax)',
                         isMuteToolButtonVisible = False )
    self.addSagittalWindow( 'dsi_view', 2, 1, 1, 1,
                            'T1 + RGB (sag)', 'T1 + RGB (sag)',
                            isMuteToolButtonVisible = False )
    self.addCoronalWindow( 'dsi_view', 3, 0, 1, 1,
                           'T1 + RGB (cor)', 'T1 + RGB (cor)',
                           isMuteToolButtonVisible = False )
    self.addAxialWindow( 'dsi_view', 3, 1, 1, 1,
                        'T1 + mask', 'T1 + mask' )

    self.addAxialWindow( 'dsi_view', 2, 2, 2, 2,
                         'ODF field', 'ODF field' )

    ########################## QBI View ########################################

    self.createView( 'qbi_view' )
    
    self.addAxialWindow( 'qbi_view', 0, 0, 1, 1,
                         'T2(b=0)', 'T2(b=0)' )
    self.addAxialWindow( 'qbi_view', 0, 1, 1, 1,
                         'DW', 'DW' )
    self.addAxialWindow( 'qbi_view', 0, 2, 1, 1,
                         'Nematic order', 'Nematic order' )
    self.addAxialWindow( 'qbi_view', 0, 3, 1, 1,
                         'GFA', 'GFA' )

  
    self.addAxialWindow( 'qbi_view', 1, 0, 1, 1,
                         'mask', 'mask' )
    self.addAxialWindow( 'qbi_view', 1, 1, 1, 1,
                         'T2 + mask', 'T2 + mask' )
    self.addAxialWindow( 'qbi_view', 1, 2, 1, 1,
                         'Normalized entropy', 'Normalized entropy' )
    self.addAxialWindow( 'qbi_view', 1, 3, 1, 1,
                         'Mean diffusivity', 'Mean diffusivity' )


    self.addAxialWindow( 'qbi_view', 2, 0, 1, 1,
                         'T1 + RGB (ax)', 'T1 + RGB (ax)',
                         isMuteToolButtonVisible = False )
    self.addSagittalWindow( 'qbi_view', 2, 1, 1, 1,
                            'T1 + RGB (sag)', 'T1 + RGB (sag)',
                            isMuteToolButtonVisible = False )
    self.addCoronalWindow( 'qbi_view', 3, 0, 1, 1,
                           'T1 + RGB (cor)', 'T1 + RGB (cor)',
                           isMuteToolButtonVisible = False )
    self.addAxialWindow( 'qbi_view', 3, 1, 1, 1,
                        'T1 + mask', 'T1 + mask' )

    self.addAxialWindow( 'qbi_view', 2, 2, 2, 2,
                         'ODF field', 'ODF field' )

    ########################## SD View #########################################

    self.createView( 'sd_view' )
    
    self.addAxialWindow( 'sd_view', 0, 0, 1, 1,
                         'T2(b=0)', 'T2(b=0)' )
    self.addAxialWindow( 'sd_view', 0, 1, 1, 1,
                         'DW', 'DW' )
    self.addAxialWindow( 'sd_view', 0, 2, 1, 1,
                         'DW SH coef.', 'DW SH coef.' )
    self.addAxialWindow( 'sd_view', 0, 3, 1, 1,
                         'GFA', 'GFA' )

  
    self.addAxialWindow( 'sd_view', 1, 0, 1, 1,
                         'mask', 'mask' )
    self.addAxialWindow( 'sd_view', 1, 1, 1, 1,
                         'T2 + mask', 'T2 + mask' )
    self.addAxialWindow( 'sd_view', 1, 2, 1, 1,
                         'ODF SH coef.', 'ODF SH coef.' )
    self.addAxialWindow( 'sd_view', 1, 3, 1, 1,
                         'Mean diffusivity', 'Mean diffusivity' )


    self.addAxialWindow( 'sd_view', 2, 0, 1, 1,
                         'T1 + RGB (ax)', 'T1 + RGB (ax)',
                         isMuteToolButtonVisible = False )
    self.addSagittalWindow( 'sd_view', 2, 1, 1, 1,
                            'T1 + RGB (sag)', 'T1 + RGB (sag)',
                            isMuteToolButtonVisible = False )
    self.addCoronalWindow( 'sd_view', 3, 0, 1, 1,
                           'T1 + RGB (cor)', 'T1 + RGB (cor)',
                           isMuteToolButtonVisible = False )
    self.addAxialWindow( 'sd_view', 3, 1, 1, 1,
                        'T1 + mask', 'T1 + mask' )

    self.addAxialWindow( 'sd_view', 2, 2, 2, 2,
                         'ODF field', 'ODF field' )

    ########################## SDT View ########################################

    self.createView( 'sdt_view' )
    
    self.addAxialWindow( 'sdt_view', 0, 0, 1, 1,
                         'T2(b=0)', 'T2(b=0)' )
    self.addAxialWindow( 'sdt_view', 0, 1, 1, 1,
                         'DW', 'DW' )
    self.addAxialWindow( 'sdt_view', 0, 2, 1, 1,
                         'DW SH coef.', 'DW SH coef.' )
    self.addAxialWindow( 'sdt_view', 0, 3, 1, 1,
                         'GFA', 'GFA' )

  
    self.addAxialWindow( 'sdt_view', 1, 0, 1, 1,
                         'mask', 'mask' )
    self.addAxialWindow( 'sdt_view', 1, 1, 1, 1,
                         'T2 + mask', 'T2 + mask' )
    self.addAxialWindow( 'sdt_view', 1, 2, 1, 1,
                         'ODF SH coef.', 'ODF SH coef.' )
    self.addAxialWindow( 'sdt_view', 1, 3, 1, 1,
                         'Mean diffusivity', 'Mean diffusivity' )


    self.addAxialWindow( 'sdt_view', 2, 0, 1, 1,
                         'T1 + RGB (ax)', 'T1 + RGB (ax)',
                         isMuteToolButtonVisible = False )
    self.addSagittalWindow( 'sdt_view', 2, 1, 1, 1,
                            'T1 + RGB (sag)', 'T1 + RGB (sag)',
                            isMuteToolButtonVisible = False )
    self.addCoronalWindow( 'sdt_view', 3, 0, 1, 1,
                           'T1 + RGB (cor)', 'T1 + RGB (cor)',
                           isMuteToolButtonVisible = False )
    self.addAxialWindow( 'sdt_view', 3, 1, 1, 1,
                        'T1 + mask', 'T1 + mask' )

    self.addAxialWindow( 'sdt_view', 2, 2, 2, 2,
                         'ODF field', 'ODF field' )

    ########################## aQBI View #######################################

    self.createView( 'aqbi_view' )
    
    self.addAxialWindow( 'aqbi_view', 0, 0, 1, 1,
                         'T2(b=0)', 'T2(b=0)' )
    self.addAxialWindow( 'aqbi_view', 0, 1, 1, 1,
                         'DW', 'DW' )
    self.addAxialWindow( 'aqbi_view', 0, 2, 1, 1,
                         'DW SH coef.', 'DW SH coef.' )
    self.addAxialWindow( 'aqbi_view', 0, 3, 1, 1,
                         'GFA', 'GFA' )

  
    self.addAxialWindow( 'aqbi_view', 1, 0, 1, 1,
                         'mask', 'mask' )
    self.addAxialWindow( 'aqbi_view', 1, 1, 1, 1,
                         'T2 + mask', 'T2 + mask' )
    self.addAxialWindow( 'aqbi_view', 1, 2, 1, 1,
                         'ODF SH coef.', 'ODF SH coef.' )
    self.addAxialWindow( 'aqbi_view', 1, 3, 1, 1,
                         'Mean diffusivity', 'Mean diffusivity' )


    self.addAxialWindow( 'aqbi_view', 2, 0, 1, 1,
                         'T1 + RGB (ax)', 'T1 + RGB (ax)',
                         isMuteToolButtonVisible = False )
    self.addSagittalWindow( 'aqbi_view', 2, 1, 1, 1,
                            'T1 + RGB (sag)', 'T1 + RGB (sag)',
                            isMuteToolButtonVisible = False )
    self.addCoronalWindow( 'aqbi_view', 3, 0, 1, 1,
                           'T1 + RGB (cor)', 'T1 + RGB (cor)',
                           isMuteToolButtonVisible = False )
    self.addAxialWindow( 'aqbi_view', 3, 1, 1, 1,
                        'T1 + mask', 'T1 + mask' )

    self.addAxialWindow( 'aqbi_view', 2, 2, 2, 2,
                         'ODF field', 'ODF field' )

    ########################## sa-aQBI View ####################################

    self.createView( 'sa-aqbi_view' )
    
    self.addAxialWindow( 'sa-aqbi_view', 0, 0, 1, 1,
                         'T2(b=0)', 'T2(b=0)' )
    self.addAxialWindow( 'sa-aqbi_view', 0, 1, 1, 1,
                         'DW', 'DW' )
    self.addAxialWindow( 'sa-aqbi_view', 0, 2, 1, 1,
                         'DW SH coef.', 'DW SH coef.' )
    self.addAxialWindow( 'sa-aqbi_view', 0, 3, 1, 1,
                         'GFA', 'GFA' )

  
    self.addAxialWindow( 'sa-aqbi_view', 1, 0, 1, 1,
                         'mask', 'mask' )
    self.addAxialWindow( 'sa-aqbi_view', 1, 1, 1, 1,
                         'T2 + mask', 'T2 + mask' )
    self.addAxialWindow( 'sa-aqbi_view', 1, 2, 1, 1,
                         'ODF SH coef.', 'ODF SH coef.' )
    self.addAxialWindow( 'sa-aqbi_view', 1, 3, 1, 1,
                         'Mean diffusivity', 'Mean diffusivity' )


    self.addAxialWindow( 'sa-aqbi_view', 2, 0, 1, 1,
                         'T1 + RGB (ax)', 'T1 + RGB (ax)',
                         isMuteToolButtonVisible = False )
    self.addSagittalWindow( 'sa-aqbi_view', 2, 1, 1, 1,
                            'T1 + RGB (sag)', 'T1 + RGB (sag)',
                            isMuteToolButtonVisible = False )
    self.addCoronalWindow( 'sa-aqbi_view', 3, 0, 1, 1,
                           'T1 + RGB (cor)', 'T1 + RGB (cor)',
                           isMuteToolButtonVisible = False )
    self.addAxialWindow( 'sa-aqbi_view', 3, 1, 1, 1,
                        'T1 + mask', 'T1 + mask' )

    self.addAxialWindow( 'sa-aqbi_view', 2, 2, 2, 2,
                         'ODF field', 'ODF field' )

    ########################## DTI View ########################################

    self.createView( 'dti_view' )
    
    self.addAxialWindow( 'dti_view', 0, 0, 1, 1,
                         'T2(b=0)', 'T2(b=0)' )
    self.addAxialWindow( 'dti_view', 0, 1, 1, 1,
                         'DW', 'DW' )
    self.addAxialWindow( 'dti_view', 0, 2, 1, 1,
                         'ADC', 'ADC' )
    self.addAxialWindow( 'dti_view', 0, 3, 1, 1,
                         'FA', 'FA' )

  
    self.addAxialWindow( 'dti_view', 1, 0, 1, 1,
                         'mask', 'mask' )
    self.addAxialWindow( 'dti_view', 1, 1, 1, 1,
                         'T2 + mask', 'T2 + mask' )
    self.addAxialWindow( 'dti_view', 1, 2, 1, 1,
                         'D(parallel)', 'D(parallel)' )
    self.addAxialWindow( 'dti_view', 1, 3, 1, 1,
                         'D(transverse)', 'D(transverse)' )


    self.addAxialWindow( 'dti_view', 2, 0, 1, 1,
                         'T1 + RGB (ax)', 'T1 + RGB (ax)',
                         isMuteToolButtonVisible = False )
    self.addSagittalWindow( 'dti_view', 2, 1, 1, 1,
                            'T1 + RGB (sag)', 'T1 + RGB (sag)',
                            isMuteToolButtonVisible = False )
    self.addCoronalWindow( 'dti_view', 3, 0, 1, 1,
                           'T1 + RGB (cor)', 'T1 + RGB (cor)',
                           isMuteToolButtonVisible = False )
    self.addAxialWindow( 'dti_view', 3, 1, 1, 1,
                        'T1 + mask', 'T1 + mask' )

    self.addAxialWindow( 'dti_view', 2, 2, 2, 2,
                         'ODF field', 'ODF field' )


def createDwiLocalModelingViewer( minimumSize, parent ):

  return DwiLocalModelingViewer( minimumSize, parent )

