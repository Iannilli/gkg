from Algorithm import *
from DwiLongWhiteMatterBundleLabelingTask import *


class DwiLongWhiteMatterBundleLabelingAlgorithm( Algorithm ):

  def __init__( self, verbose ):

    Algorithm.__init__( self,
                        'DWI-Long-White-Matter-Bundle-Labeling',
                        verbose,
                        True )

    ############################################################################
    # Input data
    ############################################################################
    self.addParameter( StringParameter( 'fileNameInputBundleMaps', '' ) )

    self.addParameter( StringParameter( \
                                'fileNameBundleMapsToAtlasTransformation', '' ) )

    ############################################################################
    # Labeling parameters
    ############################################################################

    self.addParameter( IntegerParameter( 'fiberResamplingPointCount', 
                                         21, 1, 1000, 1 ) )
    self.addParameter( DoubleParameter( 'densityMaskFiberResamplingStep', 
                                         0.1, 0.001, 10.0, 0.001 ) )
    self.addParameter( DoubleParameter( 'densityMaskResolution', 
                                         1.0, 0.001, 10, 0.001 ) )

    ############################################################################
    # Output options
    ############################################################################

    self.addParameter( StringParameter( 'outputDirectory', '' ) )
    self.addParameter( BooleanParameter( 'intermediate', 0 ) )
    self.addParameter( BooleanParameter( 'single', 2 ) )
    self.addParameter( BooleanParameter( 'computeDensityMasks', 2 ) )


  def launch( self ):

    if ( self._verbose ):

      print \
        'running DWI long WM bundle labeling task'

    task = DwiLongWhiteMatterBundleLabelingTask( self._application )
    task.launch( False )


  def view( self ):

    if ( self._verbose ):

      print \
        'viewing DWI long WM bundle labeling task'

    task = DwiLongWhiteMatterBundleLabelingTask( self._application )
    task.launch( True )

