from Algorithm import *
from DwiMonteCarloSimulatorTask import *

###############################################################################

class DwiMonteCarloSimulatorAlgorithm( Algorithm ):

  def __init__( self, verbose ):

    Algorithm.__init__( self, 'DWI-Monte-Carlo-Simulator', verbose )

    ###########################################################################
    # MCS scene and setting
    ###########################################################################

    # Global bounding box
    self.addParameter( DoubleParameter( 'globalBoxMinusX',
                                        -50.0, -1000.0, 1000.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'globalBoxPlusX',
                                        +50.0, -1000.0, 1000.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'globalBoxMinusY',
                                        -50.0, -1000.0, 1000.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'globalBoxPlusY',
                                        +50.0, -1000.0, 1000.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'globalBoxMinusZ',
                                        -50.0, -1000.0, 1000.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'globalBoxPlusZ',
                                        +50.0, -1000.0, 1000.0, 1.0 ) )

    # Octree cache size
    self.addParameter( IntegerParameter( 'octreeCacheSizeX',
                                         50, 1, 1000, 1 ) )
    self.addParameter( IntegerParameter( 'octreeCacheSizeY',
                                         50, 1, 1000, 1 ) )
    self.addParameter( IntegerParameter( 'octreeCacheSizeZ',
                                         50, 1, 1000, 1 ) )

    # Time Step, step count, and session count
    self.addParameter( DoubleParameter( 'simulationTimeStep',
                                        10.0, 0.01, 100.0, 1.0 ) )
    self.addParameter( IntegerParameter( 'simulationStepCount',
                                         10000, 100, 1000000, 10 ) )
    self.addParameter( IntegerParameter( 'simulationSessionCount',
                                         1, 1, 10000, 1 ) )


    ###########################################################################
    # Membranes
    ###########################################################################

    self.addParameter( StringParameter( 'membraneGeometryFileName', "" ) )
    self.addParameter( DoubleParameter( 'layerDiffusivity',
                                        0.0020, 0.0, 1.0, 0.001 ) )
    self.addParameter( DoubleParameter( 'layerThickness',
                                        0.0, 0.0, 1.0, 0.01 ) )
    self.addParameter( DoubleParameter( 'permeability',
                                        0.0, 0.0, 1.0, 0.01 ) )
    self.addParameter( BooleanParameter( 'biphasicDiffusionModel', 0 ) )


    ###########################################################################
    # Diffusion Properties
    ###########################################################################

    self.addParameter( DoubleParameter( 'globalDiffusivity',
                                        0.0020, 0.0, 1.0, 0.001 ) )
    self.addParameter( IntegerParameter( 'globalParticleCountPerSession',
                                         10000, 1, 1000000, 1 ) )
    self.addParameter( ChoiceParameter( 'initialParticlePositioning',
                                        0,
                                        ( 'gbox-uniform-random',
                                          'gbox-center',                             
                                          'intra-extra',
                                          'custom-box-uniform-random',
                                          'custom-position' ) ) )
    self.addParameter( DoubleParameter( 'intracellularFraction',
                                        0.5, 0.0, 1.0, 0.01 ) )
    # custom bounding box
    self.addParameter( DoubleParameter( 'customBoxMinusX',
                                        -50.0, -1000.00, 1000.00, 1.0 ) )
    self.addParameter( DoubleParameter( 'customBoxPlusX',
                                        +50.0, -1000.00, 1000.00, 1.0 ) )
    self.addParameter( DoubleParameter( 'customBoxMinusY',
                                        -50.0, -1000.00, 1000.00, 1.0 ) )
    self.addParameter( DoubleParameter( 'customBoxPlusY',
                                        +50.0, -1000.00, 1000.00, 1.0 ) )
    self.addParameter( DoubleParameter( 'customBoxMinusZ',
                                        -50.0, -1000.00, 1000.00, 1.0 ) )
    self.addParameter( DoubleParameter( 'customBoxPlusZ',
                                        +50.0, -1000.00, 1000.00, 1.0 ) )

    # custom position
    self.addParameter( DoubleParameter( 'customPositionX',
                                        0.0, -1000.00, 1000.00, 1.0 ) )
    self.addParameter( DoubleParameter( 'customPositionY',
                                        0.0, -1000.00, 1000.00, 1.0 ) )
    self.addParameter( DoubleParameter( 'customPositionZ',
                                        0.0, -1000.00, 1000.00, 1.0 ) )


    ###########################################################################
    # Rendering properties
    ###########################################################################

    # Particles renderings and trajectories in the volume of interest
    self.addParameter( DoubleParameter( 'localBoxMinusX',
                                        -50.0, -1000.00, 1000.00, 1.0 ) )
    self.addParameter( DoubleParameter( 'localBoxPlusX',
                                        +50.0, -1000.00, 1000.00, 1.0 ) )
    self.addParameter( DoubleParameter( 'localBoxMinusY',
                                        -50.0, -1000.00, 1000.00, 1.0 ) )
    self.addParameter( DoubleParameter( 'localBoxPlusY',
                                        +50.0, -1000.00, 1000.00, 1.0 ) )
    self.addParameter( DoubleParameter( 'localBoxMinusZ',
                                        -50.0, -1000.00, 1000.00, 1.0 ) )
    self.addParameter( DoubleParameter( 'localBoxPlusZ',
                                        +50.0, -1000.00, 1000.00, 1.0 ) )
    self.addParameter( IntegerParameter( 'localParticleCount',
                                         10000, 1, 1000000, 1 ) )
    self.addParameter( DoubleParameter( 'particleRadius',
                                        0.25, 0.05, 10.0, 0.1 ) )
    self.addParameter( IntegerParameter( 'particleVertexCount',
                                         30, 6, 2000, 1 ) )
    self.addParameter( IntegerParameter( 'temporalSubSamplingCount',
                                         100, 1, 100000, 50 ) )	


  def launch( self ):

    if ( self._verbose ):

      print '******** running DWI Monte-Carlo simulator'

    task = DwiMonteCarloSimulatorTask( self._application )
    task.launch( False )


  def view( self ):

    if ( self._verbose ):

      print '******** viewing DWI Monte-Carlo simulator'

    task = DwiMonteCarloSimulatorTask( self._application )
    task.launch( True )

