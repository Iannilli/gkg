from AlgorithmGUI import *
from ResourceManager import *

class DwiMonteCarloSimulatorAlgorithmGUI( AlgorithmGUI ):

  def __init__( self, algorithm ):

    AlgorithmGUI.__init__( self,
                           algorithm,
                           ResourceManager().getUIFileName(
                                               'dmri',
                                               'DwiMonteCarloSimulator.ui' ) )

    ############################################################################
    # Connecting MCS scene and setting
    ############################################################################

    # Global bounding box
    self.connectDoubleParameterToSpinBox( 'globalBoxMinusX',
                                          'doubleSpinBox_GlobalBoxMinusX' )
    self.connectDoubleParameterToSpinBox( 'globalBoxPlusX',
                                          'doubleSpinBox_GlobalBoxPlusX' )
    self.connectDoubleParameterToSpinBox( 'globalBoxMinusY',
                                          'doubleSpinBox_GlobalBoxMinusY' )
    self.connectDoubleParameterToSpinBox( 'globalBoxPlusY',
                                          'doubleSpinBox_GlobalBoxPlusY' )
    self.connectDoubleParameterToSpinBox( 'globalBoxMinusZ',
                                          'doubleSpinBox_GlobalBoxMinusZ' )
    self.connectDoubleParameterToSpinBox( 'globalBoxPlusZ',
                                          'doubleSpinBox_GlobalBoxPlusZ' )

    # Octree cache size
    self.connectIntegerParameterToSpinBox( 'octreeCacheSizeX',
                                           'spinBox_OctreeCacheSizeX' )
    self.connectIntegerParameterToSpinBox( 'octreeCacheSizeY',
                                           'spinBox_OctreeCacheSizeY' )
    self.connectIntegerParameterToSpinBox( 'octreeCacheSizeZ',
                                           'spinBox_OctreeCacheSizeZ' )

    # Time Step, step count, and session count
    self.connectDoubleParameterToSpinBox( 'simulationTimeStep',
                                          'doubleSpinBox_SimulationTimeStep' )
    self.connectIntegerParameterToSpinBox( 'simulationStepCount',
                                           'spinBox_SimulationStepCount' )
    self.connectIntegerParameterToSpinBox( 'simulationSessionCount',
                                           'spinBox_SimulationSessionCount' )


    ############################################################################
    # Connecting membrane interface
    ############################################################################

    self.connectStringParameterToLineEdit( 'membraneGeometryFileName',
                                           'lineEdit_MembraneGeometryFileName' )
    self.connectFileBrowserToPushButtonAndLineEdit(
                                        'pushButton_MembraneGeometryFileBrowse',
                                        'lineEdit_MembraneGeometryFileName' )

    self.connectDoubleParameterToSpinBox( 'layerDiffusivity',
                                          'doubleSpinBox_LayerDiffusivity' )
    self.connectDoubleParameterToSpinBox( 'layerThickness',
                                          'doubleSpinBox_LayerThickness' )
    self.connectDoubleParameterToSpinBox( 'permeability',
                                          'doubleSpinBox_Permeability' )
    self.connectBooleanParameterToCheckBox( 'biphasicDiffusionModel',
                                            'checkBox_BiphasicDiffusionModel' )


    ############################################################################
    # Connecting particle interface
    ############################################################################

    # Partilce properties
    self.connectDoubleParameterToSpinBox( 'globalDiffusivity',
                                          'doubleSpinBox_GlobalDiffusivity' )
    self.connectIntegerParameterToSpinBox(
                                       'globalParticleCountPerSession',
                                       'spinBox_GlobalParticleCountPerSession' )

    ######## Particle initial positioning
    self.connectChoiceParameterToComboBox(
                                         'initialParticlePositioning',
                                         'comboBox_InitialParticlePositioning' )
    self.connectCustomBoxToCustomCallback(
                             'comboBox_InitialParticlePositioning',
                             self.switchingInitialParticlePositioningAlgorithm )

    # intra-extra
    self.connectDoubleParameterToSpinBox( 'intracellularFraction',
                                          'doubleSpinBox_IntracellularFraction')

    # custom-box
    self.connectPushButtonToCustomCallback(
                                          'pushButton_SetCustomBoxToGlobalBox',
                                          self.callbackSetCustomBoxToGlobalBox )
    self.connectDoubleParameterToSpinBox( 'customBoxMinusX',
                                          'doubleSpinBox_CustomBoxMinusX' )
    self.connectDoubleParameterToSpinBox( 'customBoxPlusX',
                                          'doubleSpinBox_CustomBoxPlusX' )
    self.connectDoubleParameterToSpinBox( 'customBoxMinusY',
                                          'doubleSpinBox_CustomBoxMinusY' )
    self.connectDoubleParameterToSpinBox( 'customBoxPlusY',
                                          'doubleSpinBox_CustomBoxPlusY' )
    self.connectDoubleParameterToSpinBox( 'customBoxMinusZ',
                                          'doubleSpinBox_CustomBoxMinusZ' )
    self.connectDoubleParameterToSpinBox( 'customBoxPlusZ',
                                          'doubleSpinBox_CustomBoxPlusZ' )

    # custom position
    self.connectDoubleParameterToSpinBox( 'customPositionX',
                                          'doubleSpinBox_CustomPositionX' )
    self.connectDoubleParameterToSpinBox( 'customPositionY',
                                          'doubleSpinBox_CustomPositionY' )
    self.connectDoubleParameterToSpinBox( 'customPositionZ',
                                          'doubleSpinBox_CustomPositionZ' )

    # disable by default
    self.enableDisableOneWidget( 'frame_IntracellularFraction', 0 )
    self.enableDisableOneWidget( 'frame_CustomBox', 0 )
    self.enableDisableOneWidget( 'frame_CustomPosition', 0 )


    ############################################################################
    # Connecting output and rendering properties
    ############################################################################

    # Local volume-of-interest bounding box
    self.connectPushButtonToCustomCallback(
                                           'pushButton_SetLocalBoxToGlobalBox',
                                           self.callbackSetLocalBoxToGlobalBox )
    self.connectDoubleParameterToSpinBox( 'localBoxMinusX',
                                          'doubleSpinBox_LocalBoxMinusX' )
    self.connectDoubleParameterToSpinBox( 'localBoxPlusX',
                                          'doubleSpinBox_LocalBoxPlusX' )
    self.connectDoubleParameterToSpinBox( 'localBoxMinusY',
                                          'doubleSpinBox_LocalBoxMinusY' )
    self.connectDoubleParameterToSpinBox( 'localBoxPlusY',
                                          'doubleSpinBox_LocalBoxPlusY' )
    self.connectDoubleParameterToSpinBox( 'localBoxMinusZ',
                                          'doubleSpinBox_LocalBoxMinusZ' )
    self.connectDoubleParameterToSpinBox( 'localBoxPlusZ',
                                          'doubleSpinBox_LocalBoxPlusZ' )

    # Properties of particles renderings and trajectories
    self.connectIntegerParameterToSpinBox( 'localParticleCount',
                                           'spinBox_LocalParticleCount' )
    self.connectDoubleParameterToSpinBox( 'particleRadius',
                                          'doubleSpinBox_ParticleRadius' )
    self.connectIntegerParameterToSpinBox( 'particleVertexCount',
                                           'spinBox_ParticleVertexCount' )
    self.connectIntegerParameterToSpinBox( 'temporalSubSamplingCount',
                                           'spinBox_TemporalSubSamplingCount' )


  ############ Custom functions ################################################

  def connectCustomBoxToCustomCallback( self, comboBoxName, customCallback ):

    comboBoxWidget = self._findChild( self._awin, comboBoxName )
    comboBoxWidget.activated.connect( customCallback )


  def switchingInitialParticlePositioningAlgorithm( self, currentIndex ):

    comboBoxWidget = self._findChild( self._awin,
                                      'comboBox_InitialParticlePositioning' )
    positioningAlgorithm = comboBoxWidget.itemText( currentIndex )

    if ( positioningAlgorithm == 'gbox-uniform-random' ):

      self.enableDisableOneWidget( 'frame_IntracellularFraction', 0 )
      self.enableDisableOneWidget( 'frame_CustomBox', 0 )
      self.enableDisableOneWidget( 'frame_CustomPosition', 0 )

    if ( positioningAlgorithm == 'gbox-center' ):

      self.enableDisableOneWidget( 'frame_IntracellularFraction', 0 )
      self.enableDisableOneWidget( 'frame_CustomBox', 0 )
      self.enableDisableOneWidget( 'frame_CustomPosition', 0 )

    elif ( positioningAlgorithm == 'intra-extra' ):

      self.enableDisableOneWidget( 'frame_IntracellularFraction', 1 )
      self.enableDisableOneWidget( 'frame_CustomBox', 1 )
      self.enableDisableOneWidget( 'frame_CustomPosition', 0 )

    elif ( positioningAlgorithm == 'custom-box-uniform-random' ):

      self.enableDisableOneWidget( 'frame_IntracellularFraction', 0 )
      self.enableDisableOneWidget( 'frame_CustomBox', 1 )
      self.enableDisableOneWidget( 'frame_CustomPosition', 0 )

    elif ( positioningAlgorithm == 'custom-position' ):

      self.enableDisableOneWidget( 'frame_IntracellularFraction', 0 )
      self.enableDisableOneWidget( 'frame_CustomBox', 0 )
      self.enableDisableOneWidget( 'frame_CustomPosition', 1 )


  def enableDisableOneWidget( self, widgetName, state ):

    widget = self._findChild( self._awin, widgetName )
    widget.setEnabled( state )


  def callbackSetCustomBoxToGlobalBox( self ):

    value = self._algorithm.getParameter( 'globalBoxMinusX' ).getValue()
    self._algorithm.getParameter( 'customBoxMinusX' ).setValue( value )
    value = self._algorithm.getParameter( 'globalBoxMinusY' ).getValue()
    self._algorithm.getParameter( 'customBoxMinusY' ).setValue( value )
    value = self._algorithm.getParameter( 'globalBoxMinusZ' ).getValue()
    self._algorithm.getParameter( 'customBoxMinusZ' ).setValue( value )
    value = self._algorithm.getParameter( 'globalBoxPlusX' ).getValue()
    self._algorithm.getParameter( 'customBoxPlusX' ).setValue( value )
    value = self._algorithm.getParameter( 'globalBoxPlusY' ).getValue()
    self._algorithm.getParameter( 'customBoxPlusY' ).setValue( value )
    value = self._algorithm.getParameter( 'globalBoxPlusZ' ).getValue()
    self._algorithm.getParameter( 'customBoxPlusZ' ).setValue( value )


  def callbackSetLocalBoxToGlobalBox( self ):

    value = self._algorithm.getParameter( 'globalBoxMinusX' ).getValue()
    self._algorithm.getParameter( 'localBoxMinusX' ).setValue( value )
    value = self._algorithm.getParameter( 'globalBoxMinusY' ).getValue()
    self._algorithm.getParameter( 'localBoxMinusY' ).setValue( value )
    value = self._algorithm.getParameter( 'globalBoxMinusZ' ).getValue()
    self._algorithm.getParameter( 'localBoxMinusZ' ).setValue( value )
    value = self._algorithm.getParameter( 'globalBoxPlusX' ).getValue()
    self._algorithm.getParameter( 'localBoxPlusX' ).setValue( value )
    value = self._algorithm.getParameter( 'globalBoxPlusY' ).getValue()
    self._algorithm.getParameter( 'localBoxPlusY' ).setValue( value )
    value = self._algorithm.getParameter( 'globalBoxPlusZ' ).getValue()
    self._algorithm.getParameter( 'localBoxPlusZ' ).setValue( value )

