from Task import *
import time


class DwiMonteCarloSimulatorTask( Task ):

  def __init__( self, application ):

    Task.__init__( self, application )

  ##############################################################################
  # execute()
  ##############################################################################

  def execute( self, functors, parameters, subjectName, verbose, viewOnly ):

    try:
    
      ######## acquiring conditions ############################################
      functors[ 'condition-acquire' ]( subjectName, 'geometry-mesh-processed' )
      functors[ 'condition-acquire' ](
                                    subjectName,
                                    'membranes-and-initial-particle-processed' )
      functors[ 'condition-acquire' ]( subjectName, 'simulation-processed' )


      ######## updating progress bar ###########################################
      functors[ 'update-progress-bar' ]( subjectName, 0 )


      ######## collecting mcs scene and setting ################################
      # global bounding box
      globalBoxMinusX = parameters[ 'globalBoxMinusX' ].getValue()
      globalBoxPlusX = parameters[ 'globalBoxPlusX' ].getValue()
      globalBoxMinusY = parameters[ 'globalBoxMinusY' ].getValue()
      globalBoxPlusY = parameters[ 'globalBoxPlusY' ].getValue()
      globalBoxMinusZ = parameters[ 'globalBoxMinusZ' ].getValue()
      globalBoxPlusZ = parameters[ 'globalBoxPlusZ' ].getValue()

      # octree cache size
      octreeCacheSizeX = parameters[ 'octreeCacheSizeX' ].getValue()
      octreeCacheSizeY = parameters[ 'octreeCacheSizeY' ].getValue()
      octreeCacheSizeZ = parameters[ 'octreeCacheSizeZ' ].getValue()

      # time Step, step count, and session count
      simulationTimeStep = parameters[ 'simulationTimeStep' ].getValue()
      simulationStepCount = parameters[ 'simulationStepCount' ].getValue()
      simulationSessionCount = parameters[ 'simulationSessionCount' ].getValue()


      ######## collecting membrane geometry and properties #####################
      # membrane geometry model
      membraneGeometryFileName = parameters[
                                         'membraneGeometryFileName' ].getValue()

      # biphasic water diffusion model
      permeability = parameters[ 'permeability' ].getValue()
      layerDiffusivity = parameters[ 'layerDiffusivity' ].getValue()
      layerThickness = parameters[ 'layerThickness' ].getValue()
      biphasicDiffusionModel = str( bool( parameters[
                                       'biphasicDiffusionModel' ].getValue() ) )


      ######## collecting particles properties #################################
      globalDiffusivity = parameters[ 'globalDiffusivity' ].getValue()
      globalParticleCountPerSession = parameters[
                                    'globalParticleCountPerSession' ].getValue()
      initialParticlePositioning = parameters[
                                      'initialParticlePositioning' ].getChoice()
      intracellularFraction = parameters[ 'intracellularFraction' ].getValue()

      # custom bounding box for particle positioning
      customBoxMinusX = parameters[ 'customBoxMinusX' ].getValue()
      customBoxPlusX = parameters[ 'customBoxPlusX' ].getValue()
      customBoxMinusY = parameters[ 'customBoxMinusY' ].getValue()
      customBoxPlusY = parameters[ 'customBoxPlusY' ].getValue()
      customBoxMinusZ = parameters[ 'customBoxMinusZ' ].getValue()
      customBoxPlusZ = parameters[ 'customBoxPlusZ' ].getValue()


      ######## rendering properties ############################################
      # local bouding box
      localBoxMinusX = parameters[ 'localBoxMinusX' ].getValue()
      localBoxPlusX = parameters[ 'localBoxPlusX' ].getValue()
      localBoxMinusY = parameters[ 'localBoxMinusY' ].getValue()
      localBoxPlusY = parameters[ 'localBoxPlusY' ].getValue()
      localBoxMinusZ = parameters[ 'localBoxMinusZ' ].getValue()
      localBoxPlusZ = parameters[ 'localBoxPlusZ' ].getValue()

      # particle meshes
      localParticleCount = parameters[ 'localParticleCount' ].getValue()
      particleRadius = parameters[ 'particleRadius' ].getValue()
      particleVertexCount = parameters[ 'particleVertexCount' ].getValue()
      temporalSubSamplingCount = parameters[
                                         'temporalSubSamplingCount' ].getValue()


      ######## output directory ################################################
      outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()
      functors[ 'update-progress-bar' ]( subjectName, 5 )


      ######## creating membranes ##############################################
      membraneMeshFileName = os.path.join( outputWorkDirectory,
                                           'membranes.mesh' )

      command = 'GkgExecuteCommand DwiGeometrySimulator' \
                ' -geometry ' + membraneGeometryFileName + \
                ' -membraneMeshFileName ' + membraneMeshFileName + \
                ' -verbose true'
      executeCommand( self, subjectName, command, viewOnly )


      ######## notifying display thread ########################################
      functors[ 'condition-notify-and-release' ]( subjectName,
                                                  'geometry-mesh-processed' )


      ######## updating progress bar ###########################################
      functors[ 'update-progress-bar' ]( subjectName, 10 )


      ######## preparing the script for monte carlo simulation #################
      particleMapFileName = os.path.join( outputWorkDirectory,
                                          'particles' )

      globalBoundingBoxFileName = os.path.join( outputWorkDirectory,
                                                'gbox.mesh' )
      localBoundingBoxFileName = os.path.join( outputWorkDirectory,
                                               'lbox.mesh' )
      customBoundingBoxFileName = os.path.join( outputWorkDirectory,
                                                'cbox.mesh')
      particleTrajectoryFileName = os.path.join( outputWorkDirectory,
                                                 'particleTrajectories.mesh' )
      particleMeshFileName = os.path.join( outputWorkDirectory,
                                           'particleMeshes.mesh' )

      monteCarloCommand = \
        'GkgExecuteCommand DwiMonteCarloSimulator' + \
        ' -sessionCount ' + str( simulationSessionCount ) + \
        ' -farFromMembraneDiffusivity ' + str( globalDiffusivity ) + \
        ' -closeToMembraneDiffusivity ' + str( layerDiffusivity ) + \
        ' -gbox ' + str( globalBoxMinusX ) + ' ' + str( globalBoxPlusX ) + ' ' \
                  + str( globalBoxMinusY ) + ' ' + str( globalBoxPlusY ) + ' ' \
                  + str( globalBoxMinusZ ) + ' ' + str( globalBoxPlusZ ) + \
        ' -timeStep ' + str( simulationTimeStep ) + \
        ' -temporalSubSamplingCount ' + str( temporalSubSamplingCount ) + \
        ' -cacheSize ' + str( octreeCacheSizeX ) + ' ' \
                       + str( octreeCacheSizeY ) + ' ' \
                       + str( octreeCacheSizeZ ) + \
        ' -geometry ' + membraneGeometryFileName + \
        ' -layerThickness ' + str( layerThickness ) + \
        ' -permeability ' + str( permeability ) + \
        ' -radiusOfInfluence 0.0' + \
        ' -storeEvolvingMembrane false' + \
        ' -globalParticleCount ' + str( globalParticleCountPerSession ) + \
        ' -initialParticlePositioning ' + initialParticlePositioning + \
        ' -cbox ' + str( customBoxMinusX ) + ' ' + str( customBoxPlusX ) + ' ' \
                  + str( customBoxMinusY ) + ' ' + str( customBoxPlusY ) + ' ' \
                  + str( customBoxMinusZ ) + ' ' + str( customBoxPlusZ ) + \
        ' -intraCellularFraction ' + str( intracellularFraction ) + \
        ' -storeCloseToMembranePositionCount ' + biphasicDiffusionModel + \
        ' -o ' + particleMapFileName + \
        ' -particleFileCount 1' + \
        ' -lbox ' + str( localBoxMinusX ) + ' ' + str( localBoxPlusX ) + ' ' \
                  + str( localBoxMinusY ) + ' ' + str( localBoxPlusY ) + ' ' \
                  + str( localBoxMinusZ ) + ' ' + str( localBoxPlusZ ) + \
        ' -localParticleCount ' + str( localParticleCount ) + \
        ' -particleSphereRadius ' + str( particleRadius ) + \
        ' -particleSphereVertexCount ' + str( particleVertexCount ) + \
        ' -globalBoundingBoxFileName ' + globalBoundingBoxFileName + \
        ' -localBoundingBoxFileName ' + localBoundingBoxFileName + \
        ' -customBoundingBoxFileName ' + customBoundingBoxFileName + \
        ' -membraneMeshFileName ' + membraneMeshFileName + \
        ' -particleTrajectoryFileName ' + particleTrajectoryFileName + \
        ' -particleMeshFileName ' +  particleMeshFileName + \
        ' -verbose'


      ######## collecting particles' initial positions #########################
      particleStartingPositionMeshFileName = os.path.join (
                                              outputWorkDirectory,
                                              'particleStartingPositionMeshes' )
      command = monteCarloCommand + \
                ' -stepCount 0' + \
                ' -particleStartingPositionMeshFileName ' +  \
                                            particleStartingPositionMeshFileName
      executeCommand( self, subjectName, command, viewOnly )


      ######## notifying display thread ########################################
      functors[ 'condition-notify-and-release' ](
                                    subjectName,
                                    'membranes-and-initial-particle-processed' )


      ######## updating progress bar ###########################################
      functors[ 'update-progress-bar' ]( subjectName, 20 )


      ######## launching simulation [GkgDwiMonteCarloSimulator] ################
      command = monteCarloCommand + \
                ' -stepCount ' + str( simulationStepCount )
      executeCommand( self, subjectName, command, viewOnly )


      ######## notifying display thread ########################################
      functors[ 'condition-notify-and-release' ]( subjectName,
                                                  'simulation-processed' )

      ######## updating progress bar ###########################################
      functors[ 'update-progress-bar' ]( subjectName, 100 )

    except:

      self._functors[ 'task-set-status-as-failed' ]( subjectName,
                                                     sys.exc_info() )


  ##############################################################################
  # display()
  ##############################################################################

  def display( self, functors, parameters, subjectName, verbose,
               isReportModeActive ):

    ######## resetting the viewer ##############################################
    functors[ 'viewer-reset' ]()


    ######## output directory ##################################################
    outputWorkDirectory  =  parameters[ 'outputWorkDirectory' ].getValue()


    ######## loading membranes #################################################
    membraneMeshFileName = os.path.join( outputWorkDirectory,
                                         'membranes.mesh' )


    ######## waiting for result ################################################
    functors[ 'condition-wait-and-release' ]( subjectName,
                                              'geometry-mesh-processed' )


    ######## membranes rendering ###############################################
    # window 'Membranes'
    functors[ 'viewer-load-object' ]( membraneMeshFileName, 'membranes' )
    functors[ 'viewer-set-diffuse' ]( 'membranes',
                                      [ 0.6, 0.2, 0.25, 0.4 ] )
    functors[ 'viewer-add-object-to-window' ]( 'membranes',
                                               'dms_monte_carlo_view',
                                               'Membranes' )


    ######## waiting for result ################################################
    functors[ 'condition-wait-and-release' ](
                                    subjectName,
                                    'membranes-and-initial-particle-processed' )


    ######## membranes and initial particles ###################################
    globalBoundingBoxFileName = os.path.join( outputWorkDirectory, 'gbox.mesh' )
    localBoundingBoxFileName = os.path.join( outputWorkDirectory, 'lbox.mesh' )
    customBoundingBoxFileName = os.path.join( outputWorkDirectory, 'cbox.mesh')
    particleTrajectoryFileName = os.path.join( outputWorkDirectory,
                                               'particleTrajectories.mesh' )
    particleMeshFileName = os.path.join( outputWorkDirectory,
                                         'particleMeshes.mesh' )
    particleStartingPositionMeshFileName = os.path.join(
                                              outputWorkDirectory,
                                              'particleStartingPositionMeshes' )

    # window 'Membranes / Initial Particles'
    functors[ 'viewer-load-object' ]( globalBoundingBoxFileName,
                                      'globalBoundingBox' )
    functors[ 'viewer-add-object-to-window' ]( 'globalBoundingBox',
                                               'dms_monte_carlo_view',
                                               'Membranes / Initial Particles' )

    functors[ 'viewer-load-object' ]( localBoundingBoxFileName,
                                      'localBoundingBox' )
    functors[ 'viewer-add-object-to-window' ]( 'localBoundingBox',
                                               'dms_monte_carlo_view',
                                               'Membranes / Initial Particles' )
    functors[ 'viewer-add-object-to-window' ]( 'membranes',
                                               'dms_monte_carlo_view',
                                               'Membranes / Initial Particles' )

    functors[ 'viewer-load-object' ]( particleStartingPositionMeshFileName,
                                      'particleStartingPositionMeshes' )
    functors[ 'viewer-set-diffuse' ]( 'particleStartingPositionMeshes',
                                      [ 0.0, 1.0, 0.0, 1.0 ] )
    functors[ 'viewer-add-object-to-window' ]( 'particleStartingPositionMeshes',
                                               'dms_monte_carlo_view',
                                               'Membranes / Initial Particles' )


    ######## waiting for result ################################################
    functors[ 'condition-wait-and-release' ]( subjectName,
                                              'simulation-processed' )


    ######## membranes, particles and trajectories #############################
    # window 'Membranes / Trajectories'
    functors[ 'viewer-add-object-to-window' ]( 'globalBoundingBox',
                                               'dms_monte_carlo_view',
                                               'Membranes / Trajectories' )
    functors[ 'viewer-add-object-to-window' ]( 'localBoundingBox',
                                               'dms_monte_carlo_view',
                                               'Membranes / Trajectories' )
    functors[ 'viewer-add-object-to-window' ]( 'membranes',
                                               'dms_monte_carlo_view',
                                               'Membranes / Trajectories' )

    functors[ 'viewer-load-object' ]( particleTrajectoryFileName,
                                      'particleTrajectories' )
    functors[ 'viewer-set-diffuse' ]( 'particleTrajectories',
                                      [ 0.15, 0.7, 0.8, 0.4 ] )
    functors[ 'viewer-add-object-to-window' ]( 'particleTrajectories',
                                               'dms_monte_carlo_view',
                                               'Membranes / Trajectories' )

    # window 'Membranes / particles / Trajectories'
    functors[ 'viewer-add-object-to-window' ](
                                        'globalBoundingBox',
                                        'dms_monte_carlo_view',
                                        'Membranes / Particles / Trajectories' )
    functors[ 'viewer-add-object-to-window' ](
                                        'localBoundingBox',
                                        'dms_monte_carlo_view',
                                        'Membranes / Particles / Trajectories' )
    functors[ 'viewer-add-object-to-window' ](
                                        'membranes',
                                        'dms_monte_carlo_view',
                                        'Membranes / Particles / Trajectories' )

    functors[ 'viewer-load-object' ]( particleMeshFileName, 'particleMeshes' )
    functors[ 'viewer-set-diffuse' ]( 'particleMeshes', [ 0.2, 0.2, 0.8, 0.9 ] )
    functors[ 'viewer-add-object-to-window' ](
                                        'particleMeshes',
                                        'dms_monte_carlo_view',
                                        'Membranes / Particles / Trajectories' )
    functors[ 'viewer-add-object-to-window' ](
                                        'particleTrajectories',
                                        'dms_monte_carlo_view',
                                        'Membranes / Particles / Trajectories' )

