from Viewer import *


class DwiMonteCarloSimulatorViewer( Viewer ):

  def __init__( self, minimumSize, parent ):

    Viewer.__init__( self, minimumSize, parent )

    ########################## First View #####################################

    self.createView( 'dms_monte_carlo_view' )

    self.add3DWindow( 'dms_monte_carlo_view', 0, 0, 1, 1,
                      'Membranes',
                      'Membranes',
                      [ 0, 0, 0, 1 ] )
    self.add3DWindow( 'dms_monte_carlo_view', 0, 1, 1, 1,
                      'Membranes / Initial Particles',
                      'Membranes / Initial Particles',
                      [ 0, 0, 0, 1 ] )
    self.add3DWindow( 'dms_monte_carlo_view', 1, 0, 1, 1,
                      'Membranes / Trajectories',
                      'Membranes / Trajectories',
                      [ 0, 0, 0, 1 ] )
    self.add3DWindow( 'dms_monte_carlo_view', 1, 1, 1, 1,
                      'Membranes / Particles / Trajectories',
                      'Membranes / Particles / Trajectories',
                      [ 0, 0, 0, 1 ] )

def createDwiMonteCarloSimulatorViewer( minimumSize, parent ):

  return DwiMonteCarloSimulatorViewer( minimumSize, parent )

