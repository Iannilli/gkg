from Algorithm import *
from DwiMRISynthesisTask import *


class DwiMRISynthesisAlgorithm( Algorithm ):

  def __init__( self, verbose ):

    Algorithm.__init__( self, 'DWI-MRImage-Synthesis', verbose, True )

    ############################################################################
    #  Input data (Monte Carlo file)
    ############################################################################

    # Input file
    self.addParameter( StringParameter( 'fileNameMonteCarlo', "" ) )

    # Simulation session count
    self.addParameter( IntegerParameter( 'simulationSessionCount',
                                         1, 1, 100000, 1 ) )

    # Simulation starting session
    self.addParameter( IntegerParameter( 'simulationStartingSession',
                                         1, 1, 100000, 1 ) )


    ############################################################################
    #  MRI system capabilities
    ############################################################################

    # NMR/MRI pulse sequence selection
    self.addParameter( ChoiceParameter( 'sequenceChoice',
                                        0,
                                        ( 'Single PGSE',
                                          'Multiple PGSE',
                                          'Stimulated Echo',
                                          'Bipolar STE',
                                          'Bipolar Double STE',
                                          'Twice Refocused SE',
                                          'OGSE' ) ) )

    # Defining DW q-space sampling scheme
    self.addParameter( BooleanParameter(
                                       'dwDiscreteOrientationCountChoice', 1 ) )
    self.addParameter( IntegerParameter( 'dwDiscreteOrientationCount',
                                         60, 3, 8000, 5 ) )
    self.addParameter( BooleanParameter(
                                        'dwDiscreteOrientationFileChoice', 0 ) )
    self.addParameter( StringParameter( 'dwDiscreteOrientationFile', "" ) )


    # MRI time step
    self.addParameter( DoubleParameter( 'mriTimeStep',
                                        10.0, 1.0, 100.0, 1.0 ) )

    # Set of gradient amplitudes
    self.addParameter( StringParameter( 'gradientAmplitudeSet', '40', False ) )
    self.addParameter( IntegerParameter( 'indexGradientToShow',
                                         0, 0, 10000, 1 ) )


    ######## Single PGSE sequence
    self.addParameter( DoubleParameter( 'singlePGSEGradientSlewRate',
                                        200.0, 0.0, 1000000.0, 10.0 ) ) 
    self.addParameter( DoubleParameter( 'singlePGSEGradientTimeResolution',
                                        1.0, 0.0, 100.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'singlePGSELittleDelta',
                                        10.0, 0.001, 10000.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'singlePGSEBigDelta',
                                        20.0, 0.01, 10000.0, 1.0 ) )


    ######## Multiple PGSE sequence
    self.addParameter( DoubleParameter( 'multiplePGSEGradientSlewRate',
                                        200.0, 0.0, 1000000.0, 10.0 ) )
    self.addParameter( DoubleParameter( 'multiplePGSEGradientTimeResolution',
                                        1.0, 0.0, 10.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'multiplePGSELittleDelta',
                                        1.0, 0.001, 100.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'multiplePGSEBigDelta',
                                        16.0, 0.01, 1000.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'multiplePGSEMixingTime',
                                        0.0, 0.0, 50.0, 1.0e-3  ) )
    self.addParameter( IntegerParameter( 'multiplePGSEGradientPairCount',
                                         2.0, 1.0, 10.0, 1.0 ) )
    self.addParameter( StringParameter( 'multiplePGSEWavevectorsFileName',
                                        '' ) )


    ######## Stimulated echo sequence
    self.addParameter( DoubleParameter( 'stimulatedEchoGradientSlewRate',
                                        200.0, 0.0, 1000000.0, 10.0 ) )
    self.addParameter( DoubleParameter( 'stimulatedEchoGradientTimeResolution',
                                        1.0, 0.0, 10.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'stimulatedEchoLittleDelta',
                                        1.0, 0.001, 100.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'stimulatedEchoBigDelta',
                                        16.0, 0.01, 1000.0, 1.0 ) )


    ######## Bipolar Stimulated Echo sequence
    self.addParameter( DoubleParameter(
                                     'bipolarStimulatedEchoGradientSlewRate',
                                     200.0, 0.0, 1000000.0, 10.0 ) )
    self.addParameter( DoubleParameter(
                                  'bipolarStimulatedEchoGradientTimeResolution',
                                  1.0, 0.0, 10.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'bipolarStimulatedEchoLittleDelta',
                                        1.0, 0.001, 100.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'bipolarStimulatedEchoBigDelta',
                                        16.0, 0.01, 1000.0, 1.0 ) )


    ######## Bipolar Double Stimulated Echo sequence
    self.addParameter( DoubleParameter(
                               'bipolarDoubleStimulatedEchoGradientSlewRate',
                               200.0, 0.0, 1000000.0, 10.0 ) ) 
    self.addParameter( DoubleParameter(
                            'bipolarDoubleStimulatedEchoGradientTimeResolution',
                            1.0, 0.0, 10.0, 1.0 ) )
    self.addParameter( DoubleParameter(
                                       'bipolarDoubleStimulatedEchoLittleDelta',
                                       1.0, 0.001, 100.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'bipolarDoubleStimulatedEchoBigDelta',
                                        16.0, 0.01, 1000.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'bipolarDoubleStimulatedEchoMixingTime',
                                        0.0, 0.0, 50.0, 1.0e-3  ) )
    self.addParameter( StringParameter(
                        'bipolarDoubleStimulatedEchoWavevectorsFileName', "" ) )


    ######## Twice refocused spin echo sequence
    self.addParameter( DoubleParameter( 'twiceRefocusedSEGradientSlewRate',
                                        200.0, 0.0, 1000000.0, 10.0 ) )
    self.addParameter( DoubleParameter(
                                       'twiceRefocusedSEGradientTimeResolution',
                                       1.0, 0.0, 10.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'twiceRefocusedSELittleDelta',
                                        1.0, 0.001, 100.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'twiceRefocusedSEBigDelta',
                                        16.0, 0.01, 1000.0, 1.0 ) )


    ######## Oscillating gradient spin echo sequence
    self.addParameter( ChoiceParameter( 'ogseGradientWaveform',
                                         0,
                                         ('Sine',
                                          'Double-sine',
                                          'Cosine' ) ) )
    self.addParameter( DoubleParameter( 'ogseWaveformPeriod',
                                        0.1, 1.0e-6, 10.0, 1.0e-3 ) )
    self.addParameter( IntegerParameter( 'ogseWaveformLobeCount',
                                         1.0, 0.0, 1.0e6, 10.0 ) )


    ############################################################################
    # Output MR Image
    ############################################################################

    # MRI volume-of-interest box
    self.addParameter( DoubleParameter( 'mriBoxMinusX',
                                        -50.0, -1000.0, 1000.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'mriBoxPlusX',
                                        +50.0, -1000.0, 1000.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'mriBoxMinusY',
                                        -50.0, -1000.0, 1000.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'mriBoxPlusY',
                                        +50.0, -1000.0, 1000.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'mriBoxMinusZ',
                                        -50.0, -1000.0, 1000.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'mriBoxPlusZ',
                                        +50.0, -1000.0, 1000.0, 1.0 ) )

    # MRI matrix size
    self.addParameter( IntegerParameter( 'mriMatrixSizeX',
                                         5, 1, 2000, 1 ) )
    self.addParameter( IntegerParameter( 'mriMatrixSizeY',
                                         5, 1, 2000, 1 ) )
    self.addParameter( IntegerParameter( 'mriMatrixSizeZ',
                                         5, 1, 2000, 1 ) )

    # MRI Signal intensity
    self.addParameter( DoubleParameter( 'mriSignalS0',
                                        10000.0, 100.0, 10000.0, 100.0 ) )

    # Noise standard deviation
    self.addParameter( DoubleParameter( 'noiseStdDev',
                                        0.0, 0.0, 10000.0, 100.0 ) )

    # Close to membrane lifetime threshold
    self.addParameter( BooleanParameter( 'saveBiphasicDwiVolumes', 0 ) )
    self.addParameter( DoubleParameter( 'closeToMembraneLifetimeThreshold',
                                        0.5, 0.0, 1.0, 0.1 ) )


    ############################################################################
    # Output diffusion PDF
    ############################################################################

    # PDF Orientation count
    self.addParameter( BooleanParameter(
                                      'pdfDiscreteOrientationCountChoice', 1 ) )

    self.addParameter( IntegerParameter( 'pdfDiscreteOrientationCount',
                                         2000, 500, 4000, 5 ) )

    # PDF Orientation file
    self.addParameter( BooleanParameter(
                                       'pdfDiscreteOrientationFileChoice', 0 ) )
    self.addParameter( StringParameter( 'pdfDiscreteOrientationFile', "" ) )

    # PDF temporal subsampling count
    self.addParameter( IntegerParameter( 'pdfTemporalSubSamplingCount',
                                         100, 1, 10000, 5 ) )

    # PDF filter
    self.addParameter( DoubleParameter( 'pdfLowerLimit',
                                        0.0, 0.0, 1.0e38, 1.0 ) )
    self.addParameter( DoubleParameter( 'pdfUpperLimit',
                                        100000.0, 0.0, 1.0e38, 1.0 ) )
    self.addParameter( DoubleParameter( 'pdfAngularLimit',
                                        40.0, 0.0, 100.0, 10.0 ) )


  def launch( self ):

    if ( self._verbose ):

      print '******** running MR Image synthesis'

    task = DwiMRISynthesisTask( self._application )
    task.launch( False )


  def view( self ):

    if ( self._verbose ):

      print '******** viewing MR Image synthesis'

    task = DwiMRISynthesisTask( self._application )
    task.launch( True )

