from AlgorithmGUI import *
from ResourceManager import *

class DwiMRISynthesisAlgorithmGUI( AlgorithmGUI ):

  def __init__( self, algorithm ):

    AlgorithmGUI.__init__( self,
                           algorithm,
                           ResourceManager().getUIFileName(
                                                 'dmri',
                                                 'DwiMRISynthesis.ui' ) )


    ############################################################################
    # Connecting input data (Monte Carlo file)
    ############################################################################

    # Input file
    self.connectStringParameterToLineEdit( 'fileNameMonteCarlo',
                                           'lineEdit_FileNameMonteCarlo' )
    self.connectDirectoryBrowserToPushButtonAndLineEdit(
                                                'pushButton_FileNameMonteCarlo',
                                                'lineEdit_FileNameMonteCarlo' )

    # MCS session count
    self.connectIntegerParameterToSpinBox( 'simulationSessionCount',
                                           'spinBox_SimulationSessionCount' )

    # MCS starting session
    self.connectIntegerParameterToSpinBox( 'simulationStartingSession',
                                           'spinBox_SimulationStartingSession' )


    ############################################################################
    #  Connecting MRI system capabilities
    ############################################################################

    # NMR/MRI pulse sequence selection
    self.connectChoiceParameterToComboBox( 'sequenceChoice',
                                           'comboBox_Sequence' )

    # Defining q-space sampling scheme:
    # based on DW orientation count
    self.connectBooleanParameterToRadioButton(
                                      'dwDiscreteOrientationCountChoice',
                                      'radioButton_DwDiscreteOrientationCount' )
    self.connectRadioButtonToParameterAndCustomCallback(
                          'dwDiscreteOrientationCountChoice',
                          'radioButton_DwDiscreteOrientationCount',
                          self.callbackEnableDisableDwDiscreteOrientationCount )
    self.callbackEnableDisableDwDiscreteOrientationCount( 1 )

    self.connectIntegerParameterToSpinBox(
                                          'dwDiscreteOrientationCount',
                                          'spinBox_DwDiscreteOrientationCount' )

    # based on user-defined orientation file
    self.connectBooleanParameterToRadioButton(
                                       'dwDiscreteOrientationFileChoice',
                                       'radioButton_DwDiscreteOrientationFile' )
    self.connectRadioButtonToParameterAndCustomCallback(
                           'dwDiscreteOrientationFileChoice',
                           'radioButton_DwDiscreteOrientationFile',
                           self.callbackEnableDisableDwDiscreteOrientationFile )
    self.callbackEnableDisableDwDiscreteOrientationFile( 0 )

    self.connectStringParameterToLineEdit(
                                          'dwDiscreteOrientationFile',
                                          'lineEdit_DwDiscreteOrientationFile' )
    self.connectFileBrowserToPushButtonAndLineEdit(
                                   'pushButton_DwDiscreteOrientationFileBrowse',
                                   'lineEdit_DwDiscreteOrientationFile' )

    # MRI time step
    self.connectDoubleParameterToSpinBox( 'mriTimeStep',
                                          'doubleSpinBox_MRITimeStep' )


    # Set of gradient amplitudes
    self.connectStringParameterToLineEdit( 'gradientAmplitudeSet',
                                           'lineEdit_GradientAmplitudeSet' )
    self.connectIntegerParameterToSpinBox( 'indexGradientToShow',
                                           'spinBox_IndexGradientToShow' )

    # Connecting set of parameters corresponding to the choice of sequence
    self.connectComboBoxToStackedWidget( 'comboBox_Sequence',
                                         'stackedWidget_SequenceParameters' ) 


    ######## Single PGSE sequence
    self.connectDoubleParameterToSpinBox(
                                    'singlePGSEGradientSlewRate',
                                    'doubleSpinBox_SinglePGSEGradientSlewRate' )
    self.connectDoubleParameterToSpinBox(
                              'singlePGSEGradientTimeResolution',
                              'doubleSpinBox_SinglePGSEGradientTimeResolution' )
    self.connectDoubleParameterToSpinBox(
                                         'singlePGSELittleDelta',
                                         'doubleSpinBox_SinglePGSELittleDelta' )
    self.connectDoubleParameterToSpinBox( 'singlePGSEBigDelta',
                                          'doubleSpinBox_SinglePGSEBigDelta' ) 


    ######## Multiple PGSE sequence
    self.connectDoubleParameterToSpinBox(
                                  'multiplePGSEGradientSlewRate',
                                  'doubleSpinBox_MultiplePGSEGradientSlewRate' )
    self.connectDoubleParameterToSpinBox(
                            'multiplePGSEGradientTimeResolution',
                            'doubleSpinBox_MultiplePGSEGradientTimeResolution' )
    self.connectDoubleParameterToSpinBox(
                                       'multiplePGSELittleDelta',
                                       'doubleSpinBox_MultiplePGSELittleDelta' )
    self.connectDoubleParameterToSpinBox( 'multiplePGSEBigDelta',
                                          'doubleSpinBox_MultiplePGSEBigDelta' ) 

    self.connectDoubleParameterToSpinBox(
                                        'multiplePGSEMixingTime',
                                        'doubleSpinBox_MultiplePGSEMixingTime' )
    self.connectIntegerParameterToSpinBox(
                                       'multiplePGSEGradientPairCount',
                                       'spinBox_MultiplePGSEGradientPairCount' ) 

    # Wavevectors file name
    self.connectStringParameterToLineEdit(
                                    'multiplePGSEWavevectorsFileName',
                                    'lineEdit_MultiplePGSEWavevectorsFileName' )
    self.connectFileBrowserToPushButtonAndLineEdit( 
                             'pushButton_MultiplePGSEWavevectorsFileNameBrowse',
                             'lineEdit_MultiplePGSEWavevectorsFileName' )


    ######## Stimulated echo sequence
    self.connectDoubleParameterToSpinBox(
                                'stimulatedEchoGradientSlewRate',
                                'doubleSpinBox_StimulatedEchoGradientSlewRate' )
    self.connectDoubleParameterToSpinBox( 
                          'stimulatedEchoGradientTimeResolution',
                          'doubleSpinBox_StimulatedEchoGradientTimeResolution' )
    self.connectDoubleParameterToSpinBox(
                                      'stimulatedEchoLittleDelta',
                                      'doubleSpinBox_StimulatedEchoLittleDelta')
    self.connectDoubleParameterToSpinBox(
                                        'stimulatedEchoBigDelta',
                                        'doubleSpinBox_StimulatedEchoBigDelta' )


    ######## Bipolar stimulated echo sequence	
    self.connectDoubleParameterToSpinBox(
                         'bipolarStimulatedEchoGradientSlewRate',
                         'doubleSpinBox_BipolarStimulatedEchoGradientSlewRate' )
    self.connectDoubleParameterToSpinBox(
                   'bipolarStimulatedEchoGradientTimeResolution',
                   'doubleSpinBox_BipolarStimulatedEchoGradientTimeResolution' )
    self.connectDoubleParameterToSpinBox(
                              'bipolarStimulatedEchoLittleDelta',
                              'doubleSpinBox_BipolarStimulatedEchoLittleDelta' )
    self.connectDoubleParameterToSpinBox( 
                                 'bipolarStimulatedEchoBigDelta',
                                 'doubleSpinBox_BipolarStimulatedEchoBigDelta' )


    ######## Bipolar double stimulated echo sequence
    self.connectDoubleParameterToSpinBox(
                   'bipolarDoubleStimulatedEchoGradientSlewRate',
                   'doubleSpinBox_BipolarDoubleStimulatedEchoGradientSlewRate' )
    self.connectDoubleParameterToSpinBox(
             'bipolarDoubleStimulatedEchoGradientTimeResolution',
             'doubleSpinBox_BipolarDoubleStimulatedEchoGradientTimeResolution' )
    self.connectDoubleParameterToSpinBox( 
                        'bipolarDoubleStimulatedEchoLittleDelta',
                        'doubleSpinBox_BipolarDoubleStimulatedEchoLittleDelta' )
    self.connectDoubleParameterToSpinBox( 
                           'bipolarDoubleStimulatedEchoBigDelta',
                           'doubleSpinBox_BipolarDoubleStimulatedEchoBigDelta' )
    self.connectDoubleParameterToSpinBox( 
                         'bipolarDoubleStimulatedEchoMixingTime',
                         'doubleSpinBox_BipolarDoubleStimulatedEchoMixingTime' )

    # Wavevectors file name
    self.connectStringParameterToLineEdit( 
                     'bipolarDoubleStimulatedEchoWavevectorsFileName',
                     'lineEdit_BipolarDoubleStimulatedEchoWavevectorsFileName' )
    self.connectFileBrowserToPushButtonAndLineEdit( 
              'pushButton_BipolarDoubleStimulatedEchoWavevectorsFileNameBrowse',
              'lineEdit_BipolarDoubleStimulatedEchoWavevectorsFileName' )


    ######## Twice refocused spin echo sequence
    self.connectDoubleParameterToSpinBox(
                              'twiceRefocusedSEGradientSlewRate',
                              'doubleSpinBox_TwiceRefocusedSEGradientSlewRate' )
    self.connectDoubleParameterToSpinBox(
                        'twiceRefocusedSEGradientTimeResolution',
                        'doubleSpinBox_TwiceRefocusedSEGradientTimeResolution' )
    self.connectDoubleParameterToSpinBox(
                                   'twiceRefocusedSELittleDelta',
                                   'doubleSpinBox_TwiceRefocusedSELittleDelta' )
    self.connectDoubleParameterToSpinBox(
                                      'twiceRefocusedSEBigDelta',
                                      'doubleSpinBox_TwiceRefocusedSEBigDelta' )


    ######## Oscillating gradient spin echo sequence
    self.connectChoiceParameterToComboBox( 'ogseGradientWaveform',
                                           'comboBox_OGSEGradientWaveform' )
    self.connectDoubleParameterToSpinBox( 'ogseWaveformPeriod',
                                          'doubleSpinBox_OGSEWaveformPeriod' )
    self.connectIntegerParameterToSpinBox( 'ogseWaveformLobeCount',
                                           'spinBox_OGSEWaveformLobeCount' )


    ###########################################################################
    # Connecting output MR Image
    ###########################################################################

    # MRI volume-of-interest box
    self.connectDoubleParameterToSpinBox( 'mriBoxMinusX',
                                          'doubleSpinBox_MRIBoxMinusX' )
    self.connectDoubleParameterToSpinBox( 'mriBoxPlusX',
                                          'doubleSpinBox_MRIBoxPlusX' )
    self.connectDoubleParameterToSpinBox( 'mriBoxMinusY',
                                          'doubleSpinBox_MRIBoxMinusY' )
    self.connectDoubleParameterToSpinBox( 'mriBoxPlusY',
                                          'doubleSpinBox_MRIBoxPlusY' )
    self.connectDoubleParameterToSpinBox( 'mriBoxMinusZ',
                                          'doubleSpinBox_MRIBoxMinusZ' )
    self.connectDoubleParameterToSpinBox( 'mriBoxPlusZ',
                                          'doubleSpinBox_MRIBoxPlusZ' )

    # MRI matrix size
    self.connectIntegerParameterToSpinBox( 'mriMatrixSizeX',
                                           'spinBox_MRIMatrixSizeX' )
    self.connectIntegerParameterToSpinBox( 'mriMatrixSizeY',
                                           'spinBox_MRIMatrixSizeY' )
    self.connectIntegerParameterToSpinBox( 'mriMatrixSizeZ',
                                           'spinBox_MRIMatrixSizeZ' )

    # MRI Signal intensity
    self.connectDoubleParameterToSpinBox( 'mriSignalS0',
                                          'doubleSpinBox_MRISignalS0' )

    # Noise standard deviation
    self.connectDoubleParameterToSpinBox( 'noiseStdDev',
                                          'doubleSpinBox_NoiseStdDev' )

    # Enable collecting biphasic DW signal volumes
    self.connectCheckBoxToCustomCallback(
                           'checkBox_SaveBiphasicDwiVolumes',
                           self.callbackEnableDisableSaveBiphasicDwiVolumes )

    # Close to membrane lifetime threshold
    self.connectDoubleParameterToSpinBox(
                              'closeToMembraneLifetimeThreshold',
                              'doubleSpinBox_CloseToMembraneLifetimeThreshold' )


    ############################################################################
    #  Connecting output diffusion PDF
    ############################################################################
    # PDF Orientation count
    self.connectBooleanParameterToRadioButton(
                                     'pdfDiscreteOrientationCountChoice',
                                     'radioButton_PdfDiscreteOrientationCount' )
    self.connectRadioButtonToParameterAndCustomCallback(
                                 'pdfDiscreteOrientationCountChoice',
                                 'radioButton_PdfDiscreteOrientationCount',
                                 self.callbackEnableDisablePdfOrientationCount )
    self.callbackEnableDisablePdfOrientationCount( 1 )

    self.connectIntegerParameterToSpinBox(
                                         'pdfDiscreteOrientationCount',
                                         'spinBox_PdfDiscreteOrientationCount' )

    # PDF Orientation file
    self.connectBooleanParameterToRadioButton(
                                      'pdfDiscreteOrientationFileChoice',
                                      'radioButton_PdfDiscreteOrientationFile' )
    self.connectRadioButtonToParameterAndCustomCallback(
                                  'pdfDiscreteOrientationFileChoice',
                                  'radioButton_PdfDiscreteOrientationFile',
                                  self.callbackEnableDisablePdfOrientationFile )
    self.callbackEnableDisablePdfOrientationFile( 0 )

    self.connectStringParameterToLineEdit(
                                         'pdfDiscreteOrientationFile',
                                         'lineEdit_PdfDiscreteOrientationFile' )
    self.connectFileBrowserToPushButtonAndLineEdit(
                                  'pushButton_PdfDiscreteOrientationFileBrowse',
                                  'lineEdit_PdfDiscreteOrientationFile' )

    # PDF temporal subsampling count
    self.connectIntegerParameterToSpinBox(
                                         'pdfTemporalSubSamplingCount',
                                         'spinBox_PdfTemporalSubSamplingCount' )

    # Enable diffusion PDF filter
    self.connectCheckBoxToCustomCallback(
                                        'checkBox_PdfFilter',
                                        self.callbackEnableDisableGetPdfFilter )

    self.connectDoubleParameterToSpinBox( 'pdfLowerLimit',
                                          'doubleSpinBox_PdfLowerLimit' )
    self.connectDoubleParameterToSpinBox( 'pdfUpperLimit',
                                          'doubleSpinBox_PdfUpperLimit' )
    self.connectDoubleParameterToSpinBox( 'pdfAngularLimit',
                                          'doubleSpinBox_PdfAngularLimit' )	



    ############ 1st pack of functions: manage enable/disable wigdets #########

  def callbackEnableDisablePdfOrientationCount( self, state ):

    self.enableDisableOneWidget( 'spinBox_PdfDiscreteOrientationCount',
                                 bool ( state ) )

  def callbackEnableDisablePdfOrientationFile( self, state ):

    self.enableDisableOneWidget( 'lineEdit_PdfDiscreteOrientationFile',
                                 bool ( state ) )
    self.enableDisableOneWidget( 'pushButton_PdfDiscreteOrientationFileBrowse',
                                 bool ( state ) )

  def callbackEnableDisableDwDiscreteOrientationCount( self, state ):

    self.enableDisableOneWidget( 'spinBox_DwDiscreteOrientationCount',
                                 bool ( state ) )

  def callbackEnableDisableDwDiscreteOrientationFile( self, state ):

    self.enableDisableOneWidget( 'lineEdit_DwDiscreteOrientationFile',
                                 bool ( state ) )
    self.enableDisableOneWidget( 'pushButton_DwDiscreteOrientationFileBrowse',
                                 bool ( state ) )

  def enableDisableOneWidget( self, widgetName, state ):

    widget = self._findChild( self._awin, widgetName )
    widget.setEnabled( state )

  def connectRadioButtonToParameterAndCustomCallback( self,
                                                      parameterName,
                                                      radioButtonName,
                                                      customCallback ):

    self.connectBooleanParameterToRadioButton( parameterName,
                                               radioButtonName )
    radioButtonWidget = self._findChild( self._awin, radioButtonName )

    radioButtonWidget.toggled.connect( customCallback )

  def callbackEnableDisableSaveBiphasicDwiVolumes( self, state ):

    self.enableDisableOneWidget(
                               'doubleSpinBox_CloseToMembraneLifetimeThreshold',
                               bool( state ) )

  def callbackEnableDisableGetPdfFilter( self, state ):

    self.enableDisableOneWidget( 'frame_PdfFilter', bool( state ) )

