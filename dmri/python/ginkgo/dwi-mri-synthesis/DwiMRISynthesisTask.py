from Task import *
import time


class DwiMRISynthesisTask( Task ):

  def __init__( self, application ):

    Task.__init__( self, application )


  ##############################################################################
  # execute()
  ##############################################################################

  def execute( self, functors, parameters, subjectName, verbose, viewOnly ):

    try:
    
      ######## acquiring conditions ############################################
      functors[ 'condition-acquire' ]( subjectName, 'synthetizing-processed' )

      ######## updating progress bar ###########################################
      functors[ 'update-progress-bar' ]( subjectName, 0 )

      ######## Collecting Input data from MCS particle map #####################
      fileNameMonteCarlo = parameters[ 'fileNameMonteCarlo' ].getValue()
      simulationSessionCount = parameters[ 'simulationSessionCount' ].getValue()
      simulationStartingSession = parameters[
                                        'simulationStartingSession' ].getValue()

      ######## Collecting MRI system capabilities ##############################
      # NMR/MRI pulse sequence
      sequenceChoice = parameters[ 'sequenceChoice' ].getValue()

      # MRI time step
      mriTimeStep = parameters[ 'mriTimeStep' ].getValue()

      # Set of gradient amplitudes
      gradientAmplitudeSet = parameters[ 'gradientAmplitudeSet' ].getValue()
      splitGradientAmplitudeSet = list()
      splitGradientAmplitudeSet = gradientAmplitudeSet.split( ' ' )
      indexGradientToShow = parameters[ 'indexGradientToShow' ].getValue()

      # collecting DW q-space sampling scheme
      # DW orientation count
      dwDiscreteOrientationCountChoice = parameters[
                                 'dwDiscreteOrientationCountChoice' ].getValue()
      dwDiscreteOrientationFileChoice = parameters[
                                  'dwDiscreteOrientationFileChoice' ].getValue()

      if ( dwDiscreteOrientationCountChoice == 1 ):

        dwOrientationCount = parameters[
                                       'dwDiscreteOrientationCount' ].getValue()

        dwDiscreteOrientation = ' -dwOrientationCount ' + \
                                str( dwOrientationCount )
        
      elif ( dwDiscreteOrientationFileChoice == 1 ):
    
        dwOrientationFileName = parameters[
                                        'dwDiscreteOrientationFile' ].getValue()

        dwDiscreteOrientation = ' -dwOrientationSetFileName ' + \
                                dwOrientationFileName

        # Reading the number of orientations in the file
        inputFile = open( dwOrientationFileName, 'r' )
        dwOrientationCount = inputFile.readline()
        inputFile.close()
        dwOrientationCount = dwOrientationCount[
                                               0:len( dwOrientationCount ) - 1 ]

      else:
    
        print( 'Error: not a valid dwDiscreteOrientation' )

      # Sanity check
      if ( indexGradientToShow > dwOrientationCount ):
    
        print( 'Error: indexGradientToShow must be < dwOrientationCount !' )
        return


      ######## Collecting MR Image #############################################
      # MRI volume-of-interest box
      mriBoxMinusX = parameters[ 'mriBoxMinusX' ].getValue()
      mriBoxPlusX = parameters[ 'mriBoxPlusX' ].getValue()
      mriBoxMinusY = parameters[ 'mriBoxMinusY' ].getValue()
      mriBoxPlusY = parameters[ 'mriBoxPlusY' ].getValue()
      mriBoxMinusZ = parameters[ 'mriBoxMinusZ' ].getValue()
      mriBoxPlusZ = parameters[ 'mriBoxPlusZ' ].getValue()

      # MRI matrix size
      mriMatrixSizeX = parameters[ 'mriMatrixSizeX' ].getValue()
      mriMatrixSizeY = parameters[ 'mriMatrixSizeY' ].getValue()
      mriMatrixSizeZ = parameters[ 'mriMatrixSizeZ' ].getValue()

      # MRI Signal intensity
      mriSignalS0 = parameters[ 'mriSignalS0' ].getValue()

      # Noise standard deviation
      noiseStdDev = parameters[ 'noiseStdDev' ].getValue()


      ######## Output diffusion PDF ############################################
      pdfDiscreteOrientationCountChoice = parameters[
                                'pdfDiscreteOrientationCountChoice' ].getValue()
      pdfDiscreteOrientationFileChoice = parameters[
                                 'pdfDiscreteOrientationFileChoice' ].getValue()

      if ( pdfDiscreteOrientationCountChoice == 1 ):
    
        pdfDiscreteOrientation = \
                   ' -pdfOrientationCount ' + \
                   str( parameters[ 'pdfDiscreteOrientationCount' ].getValue() )
        
      elif ( pdfDiscreteOrientationFileChoice == 1 ):
    
        pdfDiscreteOrientation = \
                    ' -pdfOrientationSetFileName ' + \
                    str( parameters[ 'pdfDiscreteOrientationFile' ].getValue() )
        
      else:
    
        print( 'Error: not a valid PdfDiscreteOrientation' )

      # PDF temporal subsampling count
      pdfTemporalSubSamplingCount = parameters[
                                      'pdfTemporalSubSamplingCount' ].getValue()

      # PDF filter
      pdfLowerLimit = parameters[ 'pdfLowerLimit' ].getValue()
      pdfUpperLimit = parameters[ 'pdfUpperLimit' ].getValue()
      pdfAngularLimit = parameters[ 'pdfAngularLimit' ].getValue()


      # Output directory
      outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()

      ### Loading the appropriated parameters following the sequence choice ####

      # Initialization of stringParameters
      stringParameters = ''

      if ( sequenceChoice == 0 ):
 
        sequence = 'single_pgse_nmr_sequence'

        singlePGSEGradientSlewRate = parameters[
                                       'singlePGSEGradientSlewRate' ].getValue()
        singlePGSEGradientTimeResolution = parameters[
                                 'singlePGSEGradientTimeResolution' ].getValue()
        singlePGSELittleDelta = parameters[ 'singlePGSELittleDelta' ].getValue()
        singlePGSEBigDelta = parameters[ 'singlePGSEBigDelta' ].getValue()

        scalarParameters = str( singlePGSEGradientSlewRate ) + ' ' + \
                           str( singlePGSEGradientTimeResolution ) + ' ' + \
                           str( singlePGSELittleDelta ) + ' ' + \
                           str( singlePGSEBigDelta )

      elif ( sequenceChoice == 1 ):
 
        sequence = 'multiple_pgse_nmr_sequence'

        multiplePGSEGradientSlewRate = parameters[
                                     'multiplePGSEGradientSlewRate' ].getValue()
        multiplePGSEGradientTimeResolution = parameters[
                               'multiplePGSEGradientTimeResolution' ].getValue()
        multiplePGSELittleDelta = parameters[
                                          'multiplePGSELittleDelta' ].getValue()
        multiplePGSEBigDelta = parameters[ 'multiplePGSEBigDelta' ].getValue()
        multiplePGSEMixingTime = parameters[
                                           'multiplePGSEMixingTime' ].getValue()
        multiplePGSEGradientPairCount = parameters[
                                    'multiplePGSEGradientPairCount' ].getValue()
        multiplePGSEWavevectorsFileName = parameters[
                                  'multiplePGSEWavevectorsFileName' ].getValue()

        scalarParameters = str( multiplePGSEGradientSlewRate ) + ' ' + \
                           str( multiplePGSEGradientTimeResolution ) + ' ' + \
                           str( multiplePGSELittleDelta ) + ' ' + \
                           str( multiplePGSEBigDelta ) + ' ' + \
                           str( multiplePGSEMixingTime ) + ' ' + \
                           str( multiplePGSEGradientPairCount )

        stringParameters = ' -stringParameters ' + \
                           str( multiplePGSEWavevectorsFileName )

      elif ( sequenceChoice == 2 ):
 
        sequence = 'stimulated_echo_nmr_sequence'

        stimulatedEchoGradientSlewRate = parameters[
                                   'stimulatedEchoGradientSlewRate' ].getValue()
        stimulatedEchoGradientTimeResolution = parameters[
                             'stimulatedEchoGradientTimeResolution' ].getValue()
        stimulatedEchoLittleDelta = parameters[
                                        'stimulatedEchoLittleDelta' ].getValue()
        stimulatedEchoBigDelta = parameters[
                                           'stimulatedEchoBigDelta' ].getValue()

        scalarParameters = str( stimulatedEchoGradientSlewRate ) + ' ' + \
                           str( stimulatedEchoGradientTimeResolution ) + ' ' + \
                           str( stimulatedEchoLittleDelta ) + ' ' + \
                           str( stimulatedEchoBigDelta )

      elif ( sequenceChoice == 3 ):

        sequence = 'bipolar_ste_nmr_sequence'

        bipolarStimulatedEchoGradientSlewRate = parameters[
                            'bipolarStimulatedEchoGradientSlewRate' ].getValue()
        bipolarStimulatedEchoGradientTimeResolution = parameters[
                      'bipolarStimulatedEchoGradientTimeResolution' ].getValue()
        bipolarStimulatedEchoLittleDelta = parameters[
                                 'bipolarStimulatedEchoLittleDelta' ].getValue()
        bipolarStimulatedEchoBigDelta = parameters[
                                    'bipolarStimulatedEchoBigDelta' ].getValue()

        scalarParameters = str( bipolarStimulatedEchoGradientSlewRate )  \
                   + ' ' + str( bipolarStimulatedEchoGradientTimeResolution ) \
                   + ' ' + str( bipolarStimulatedEchoLittleDelta )  \
                   + ' ' + str( bipolarStimulatedEchoBigDelta )

      elif ( sequenceChoice == 4 ):
 
        sequence = 'bipolar_double_ste_nmr_sequence'

        bipolarDoubleStimulatedEchoGradientSlewRate = parameters[
                      'bipolarDoubleStimulatedEchoGradientSlewRate' ].getValue()
        bipolarDoubleStimulatedEchoGradientTimeResolution = parameters[
                'bipolarDoubleStimulatedEchoGradientTimeResolution' ].getValue()
        bipolarDoubleStimulatedEchoLittleDelta = parameters[
                           'bipolarDoubleStimulatedEchoLittleDelta' ].getValue()
        bipolarDoubleStimulatedEchoBigDelta = parameters[
                              'bipolarDoubleStimulatedEchoBigDelta' ].getValue()
        bipolarDoubleStimulatedEchoMixingTime = parameters[
                            'bipolarDoubleStimulatedEchoMixingTime' ].getValue()
        bipolarDoubleStimulatedEchoWavevectorsFileName = parameters[
                   'bipolarDoubleStimulatedEchoWavevectorsFileName' ].getValue()

        scalarParameters = \
              str( bipolarDoubleStimulatedEchoGradientSlewRate ) + ' ' + \
              str( bipolarDoubleStimulatedEchoGradientTimeResolution ) + ' ' + \
              str( bipolarDoubleStimulatedEchoLittleDelta ) + ' ' + \
              str( bipolarDoubleStimulatedEchoBigDelta ) + ' ' + \
              str( bipolarDoubleStimulatedEchoMixingTime )

        stringParameters = ' -stringParameters ' + \
                           str( bipolarDoubleStimulatedEchoWavevectorsFileName )

      elif ( sequenceChoice == 5 ):

        sequence = 'twice_refocused_se_nmr_sequence'

        twiceRefocusedSEGradientSlewRate = parameters[
                                 'twiceRefocusedSEGradientSlewRate' ].getValue()
        twiceRefocusedSEGradientTimeResolution = parameters[
                           'twiceRefocusedSEGradientTimeResolution' ].getValue()
        twiceRefocusedSELittleDelta = parameters[
                                      'twiceRefocusedSELittleDelta' ].getValue()
        twiceRefocusedSEBigDelta = parameters[
                                         'twiceRefocusedSEBigDelta' ].getValue()

        scalarParameters = \
                         str( twiceRefocusedSEGradientSlewRate ) + ' ' + \
                         str( twiceRefocusedSEGradientTimeResolution ) + ' ' + \
                         str( twiceRefocusedSELittleDelta ) + ' ' + \
                         str( twiceRefocusedSEBigDelta )

      elif ( sequenceChoice == 6 ):
 
        sequence = 'ogse_nmr_sequence'

        ogseWaveformPeriod = parameters[ 'ogseWaveformPeriod' ].getValue()
        ogseWaveformLobeCount = parameters[ 'ogseWaveformLobeCount' ].getValue()
        ogseGradientWaveform = parameters[ 'ogseGradientWaveform' ].getChoice()
      
        if ( ogseGradientWaveform == 'Sine' ):

          ogseGradientWaveform = 'sine'

        elif ( ogseGradientWaveform == 'Double-sine' ):

          ogseGradientWaveform = 'double-sine'

        elif ( ogseGradientWaveform == 'Cosine' ):

          ogseGradientWaveform = 'cosine'

        scalarParameters = str( ogseWaveformPeriod ) + ' ' + \
                           str( ogseWaveformLobeCount )

        stringParameters = ' -stringParameters ' + '' + \
                           str( ogseGradientWaveform )

      else:
 
        print( 'NMR Sequence Error: unkonwn sequence' )
        print( sequenceChoice )


      ######## preparing the command ###########################################
      particleMapFileName = os.path.join( fileNameMonteCarlo, 'particles' )

      t2FileName = os.path.join( outputWorkDirectory, 't2' )
      dwFileName = os.path.join( outputWorkDirectory, 'dw')
      maskFileName = os.path.join( outputWorkDirectory, 'mask' )
      meanDisplacementFileName = os.path.join( outputWorkDirectory,
                                               'meanDisplacement' )
      pdfFileName = os.path.join( outputWorkDirectory, 'pdf' )
      pdfMeshFileName = os.path.join( outputWorkDirectory, 'pdfMesh' )
      particleDistributionFileName = os.path.join( outputWorkDirectory,
                                                   'particleDistribution' )

      command = 'GkgExecuteCommand DwiMRImageSimulator' + \
                ' -sequence ' + sequence + \
                ' -gradientAmplitudes ' + gradientAmplitudeSet + \
                ' -scalarParameters ' + scalarParameters + \
                stringParameters + \
                dwDiscreteOrientation + \
                ' -startingSession ' + str( simulationStartingSession ) + \
                ' -sessionCount ' + str( simulationSessionCount ) + \
                ' -particleMapFileName ' + particleMapFileName + \
                ' -timeStep ' + str( mriTimeStep ) + \
                ' -temporalSubSamplingCount ' + \
                                          str( pdfTemporalSubSamplingCount ) + \
                pdfDiscreteOrientation + \
                ' -lowerLimit ' + str( pdfLowerLimit ) + \
                ' -upperLimit ' + str( pdfUpperLimit ) + \
                ' -apertureAngleLimit ' + str( pdfAngularLimit ) + \
                ' -mrbox ' \
                        + str( mriBoxMinusX ) + ' ' + str( mriBoxPlusX ) + ' ' \
                        + str( mriBoxMinusY ) + ' ' + str( mriBoxPlusY ) + ' ' \
                        + str( mriBoxMinusZ ) + ' ' + str( mriBoxPlusZ ) + \
                ' -mrVolumeSize ' + str( mriMatrixSizeX ) + ' ' \
                                  + str( mriMatrixSizeY ) + ' ' \
                                  + str( mriMatrixSizeZ ) + \
                ' -S0 ' + str( mriSignalS0 ) + \
                ' -noiseStdDev ' + str( noiseStdDev ) + \
                ' -t2 ' + t2FileName + \
                ' -dw ' + dwFileName + \
                ' -m ' + maskFileName + \
                ' -meanDisplacement ' + meanDisplacementFileName + \
                ' -pdf ' + pdfFileName + \
                ' -pdfMesh ' + pdfMeshFileName + \
                ' -particleDistribution ' + particleDistributionFileName + \
                ' -verbose'

      # colleting biphasic DW volumes if required
      if ( parameters[ 'saveBiphasicDwiVolumes' ].getValue() == 1 ):

        closeToMembraneLifetimeThreshold = parameters[
                                 'closeToMembraneLifetimeThreshold' ].getValue()

        dwCloseToMembraneFileName = os.path.join( outputWorkDirectory,
                                                  'dw_close' )
        dwFarFromMembrnaeFileName = os.path.join( outputWorkDirectory,
                                                  'dw_far' )
        command = command + \
                  ' -closeToMembraneLifeTimeFraction ' + \
                                     str( closeToMembraneLifetimeThreshold ) + \
                  ' -dwClose ' + str( dw_closeOutputFileName ) + \
                  ' -dwFar ' + str( dw_farOutputFileName )

      ######## updating progress bar ###########################################
      functors[ 'update-progress-bar' ]( subjectName, 35 )

      ######## synthetizing MR image ###########################################
      executeCommand( self, subjectName, command, viewOnly )

      ######## notifying display thread ########################################
      functors[ 'condition-notify-and-release' ]( subjectName,
                                                  'synthetizing-processed' )

      ######## updating progress bar ###########################################
      functors[ 'update-progress-bar' ]( subjectName, 90 )

    except:

      self._functors[ 'task-set-status-as-failed' ]( subjectName,
                                                     sys.exc_info() )


  ##############################################################################
  # display()
  ##############################################################################

  def display( self, functors, parameters, subjectName, verbose,
               isReportModeActive ):

    ######## resetting the viewer ##############################################
    functors[ 'viewer-reset' ]()

    ######## collecting information ############################################
    # getting DW orientation count
    if ( parameters[ 'dwDiscreteOrientationCountChoice' ].getValue() == 1 ):

      dwOrientationCount = parameters[ 'dwDiscreteOrientationCount' ].getValue()
       
    elif ( parameters[ 'dwDiscreteOrientationFileChoice' ].getValue() == 1 ):
    
      dwOrientationFileName = parameters[
                                        'dwDiscreteOrientationFile' ].getValue()

      inputFile = open( parameters[ 'dwDiscreteOrientationFile' ].getValue(),
                        'r' )
      dwOrientationCount = inputFile.readline()
      inputFile.close()
      dwOrientationCount = dwOrientationCount[ 0:len( dwOrientationCount ) - 1 ]

    else:
    
      print( 'Error: not a valid dwDiscreteOrientation' )

    # getting gradient index
    indexGradientToShow = parameters[ 'indexGradientToShow' ].getValue()

    ######## loading the results ###############################################
    # Output directory
    outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()

    # DWI volume
    dwFileName = os.path.join( outputWorkDirectory,
                               'dw_o' + str( dwOrientationCount ) +
                               '_gIndex' + str( indexGradientToShow ) +
                               '.ima' )

    # PDF mesh
    pdfMeshFileName = os.path.join( outputWorkDirectory, 'pdfMesh' )

    # Mean displacement
    meanDisplacementFileName = os.path.join( outputWorkDirectory,
                                             'meanDisplacement' )

    # Particle distribution
    particleDistributionFileName = os.path.join( outputWorkDirectory,
                                                 'particleDistribution' )

    ######## waiting for result ################################################
    functors[ 'condition-wait-and-release' ]( subjectName,
                                              'synthetizing-processed' )

    ######## display result(s) #################################################

    functors[ 'viewer-load-object' ]( dwFileName, 'DWI' )
    functors[ 'viewer-add-object-to-window' ]( 'DWI',
                                               'dms_mri_synthesis_view',
                                               'DWI' )

    functors[ 'viewer-load-object' ]( pdfMeshFileName ,'PDF' )
    functors[ 'viewer-add-object-to-window' ]( 'PDF',
                                               'dms_mri_synthesis_view',
                                               'PDF' )

    functors[ 'viewer-load-object' ]( meanDisplacementFileName,
                                      'Mean Displacement' )
    functors[ 'viewer-add-object-to-window' ]( 'Mean Displacement',
                                               'dms_mri_synthesis_view',
                                               'Mean Displacement' )

    functors[ 'viewer-load-object' ]( particleDistributionFileName,
                                      'Particle Distribution' )
    functors[ 'viewer-add-object-to-window' ]( 'Particle Distribution',
                                               'dms_mri_synthesis_view',
                                               'Particle Distribution' )

    functors[ 'update-progress-bar' ]( subjectName, 100 )

