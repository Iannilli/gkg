from Viewer import *


class DwiMRISynthesisViewer( Viewer ):

  def __init__( self, minimumSize, parent ):

    Viewer.__init__( self, minimumSize, parent )

    ########################## First View ######################################

    self.createView( 'dms_mri_synthesis_view' )

    self.add3DWindow( 'dms_mri_synthesis_view', 0, 0, 1, 1,
                      'DWI',
                      'DWI',
                      [ 0, 0, 0, 1 ] )
    self.add3DWindow( 'dms_mri_synthesis_view', 0, 1, 1, 1,
                      'PDF',
                      'PDF',
                      [ 0, 0, 0, 1 ] )
    self.add3DWindow( 'dms_mri_synthesis_view', 1, 0, 1, 1,
                      'Mean Displacement',
                      'Mean Displacement',
                      [ 0, 0, 0, 1 ] )
    self.add3DWindow( 'dms_mri_synthesis_view', 1, 1, 1, 1,
                      'Particle Distribution',
                      'Particle Distribution',
                      [ 0, 0, 0, 1 ] )

def createDwiMRISynthesisViewer( minimumSize, parent ):

  return DwiMRISynthesisViewer( minimumSize, parent )
