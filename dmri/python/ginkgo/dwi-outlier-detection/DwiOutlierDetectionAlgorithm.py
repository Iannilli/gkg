from Algorithm import *
from DwiOutlierDetectionTask import *


class DwiOutlierDetectionAlgorithm( Algorithm ):

  def __init__( self, verbose ):

    Algorithm.__init__( self, 'DWI-Outlier-Detection', verbose,
                        True )

    # diffusion-weighted input data
    self.addParameter( StringParameter( 'rawDwiDirectory', '' ) )
    self.addParameter( StringParameter( 'roughMaskDirectory', '' ) )

    # outlier detection
    self.addParameter( DoubleParameter( 'outlierFactor',
                                        3.0, 0.1, 10, 0.1 ) )
    self.addParameter( StringParameter( 'discardedOrientationList',
                                        '', False ) )


  def launch( self ):

    if ( self._verbose ):

      print \
        'running DWI outlier detection'

    task = DwiOutlierDetectionTask( self._application )
    task.launch( False )


  def view( self ):

    if ( self._verbose ):

      print \
        'viewing DWI outlier detection'

    task = DwiOutlierDetectionTask( self._application )
    task.launch( True )

