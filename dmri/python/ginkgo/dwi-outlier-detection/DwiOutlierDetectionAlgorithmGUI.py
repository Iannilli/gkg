from AlgorithmGUI import *
from ResourceManager import *


class DwiOutlierDetectionAlgorithmGUI( AlgorithmGUI ):

  def __init__( self, algorithm ):

    AlgorithmGUI.__init__( self,
                           algorithm,
                           ResourceManager().getUIFileName(
                                         'dmri',
                                         'DwiOutlierDetection.ui' ) )

    ############################################################################
    # connecting input DWI interface
    ############################################################################

    # raw DW data directory interface
    self.connectStringParameterToLineEdit( 'rawDwiDirectory',
                                           'lineEdit_RawDwiDirectory' )    
    self.connectDirectoryBrowserToPushButtonAndLineEdit(
                                                   'pushButton_RawDwiDirectory',
                                                   'lineEdit_RawDwiDirectory' )

    # rough mask directory interface
    self.connectStringParameterToLineEdit( 'roughMaskDirectory',
                                           'lineEdit_RoughMaskDirectory' )    
    self.connectDirectoryBrowserToPushButtonAndLineEdit(
                                                'pushButton_RoughMaskDirectory',
                                                'lineEdit_RoughMaskDirectory' )

    ############################################################################
    # connecting outlier filter interface
    ############################################################################

    self.connectDoubleParameterToSpinBox( 'outlierFactor',
                                          'doubleSpinBox_OutlierFactor' )
    self.connectStringParameterToLineEdit( 'discardedOrientationList',
                                           'lineEdit_DiscardedOrientationList' )

