from Task import *
import shutil

class DwiOutlierDetectionTask( Task ):

  def __init__( self, application ):

    Task.__init__( self, application )

    self.addFunctor( 'viewer-set-dw-information',
                     self.viewerSetDwInformation )


  def viewerSetDwInformation( self,
                              sliceAxis,
                              sizeX, sizeY, sizeZ,
                              resolutionX, resolutionY, resolutionZ ):

    if ( self._application.getBatchMode() == False ):

      self._application.getMainThreadActionManager().call(
        self._application.getViewer().setDwInformation,
        sliceAxis, sizeX, sizeY, sizeZ, resolutionX, resolutionY, resolutionZ )

  ##############################################################################
  # execute()
  ##############################################################################

  def execute( self, functors, parameters, subjectName, verbose, viewOnly ):

    try:

      ####################### acquiring conditions #############################
      functors[ 'condition-acquire' ]( subjectName, 'outlier-processed' )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 0 )

      ####################### collecting output work directory #################
      outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()
      if ( verbose ):
      
        print  'Output work directory :', outputWorkDirectory

      ####################### collecting raw DWI directory #####################
      rawDwiDirectory = parameters[ 'rawDwiDirectory' ].getValue()
      if ( verbose ):
      
        print  'Raw DWI directory :', rawDwiDirectory

      ####################### collecting rough mask directory ##################
      roughMaskDirectory = parameters[ 'roughMaskDirectory' ].getValue()
      if ( verbose ):
      
        print  'Rough mask directory :', roughMaskDirectory

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 5 )

      ####################### reading acq parameters ###########################
      fileNameAcquisitionParameters = os.path.join( rawDwiDirectory,
                                                   'acquisition_parameters.py' )
      if ( verbose ):

        print 'reading \'' + fileNameAcquisitionParameters + '\''

      globalVariables = dict()
      localVariables = dict()
      execfile( fileNameAcquisitionParameters, globalVariables, localVariables )
      sliceAxis = localVariables[ 'acquisitionParameters' ][ 'sliceAxis' ]
      sizeX = int( localVariables[ 'acquisitionParameters' ][ 'sizeX' ] )
      sizeY = int( localVariables[ 'acquisitionParameters' ][ 'sizeY' ] )
      sizeZ = int( localVariables[ 'acquisitionParameters' ][ 'sizeZ' ] )
      resolutionX = float( \
                    localVariables[ 'acquisitionParameters' ][ 'resolutionX' ] )
      resolutionY = float( \
                    localVariables[ 'acquisitionParameters' ][ 'resolutionY' ] )
      resolutionZ = float( \
                    localVariables[ 'acquisitionParameters' ][ 'resolutionZ' ] )

      ####################### sanity checks ####################################

      functors[ 'viewer-set-dw-information' ]( sliceAxis,
                                               sizeX,
                                               sizeY,
                                               sizeZ,
                                               resolutionX,
                                               resolutionY,
                                               resolutionZ )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 15 )


      ####################### filtering outliers ###############################
      outlierFactor = parameters[ 'outlierFactor' ].getValue()
    
      fileNameAverageT2 = os.path.join( rawDwiDirectory, 't2' )
      fileNameDW = os.path.join( rawDwiDirectory, 'dw' )
      fileNameMask = os.path.join( roughMaskDirectory, 'mask' )
      fileNameDWWoOutlier = os.path.join( outputWorkDirectory, 'dw_wo_outlier' )
      fileNameOutliers = os.path.join( outputWorkDirectory, 'outliers.py' )
      
      command = 'GkgExecuteCommand DwiOutlierFilter' + \
                ' -t2 ' + fileNameAverageT2 + \
                ' -dw ' + fileNameDW + \
                ' -m ' + fileNameMask + \
                ' -axis ' + sliceAxis + \
                ' -f ' + str( outlierFactor ) + \
                ' -o ' + fileNameDWWoOutlier + \
                ' -outliers ' + fileNameOutliers + \
                ' -verbose true'
      executeCommand( self, subjectName, command, viewOnly )

      ######################### copying T2(b=0) ################################
      fileNameAverageT2WoOutlier = os.path.join( outputWorkDirectory,
                                                 't2_wo_outlier' )
      command = 'GkgExecuteCommand Combiner' + \
                ' -i ' +  fileNameAverageT2 + \
                ' -o ' + fileNameAverageT2WoOutlier + \
                ' -num1 1 0 -den1 1 1 ' + \
                ' -verbose true'
      executeCommand( self, subjectName, command, viewOnly )

      ######################### discarding directions ##########################
      discardedOrientationList = parameters[ 'discardedOrientationList' \
                                                                    ].getValue()
      if ( len( discardedOrientationList ) > 0 ):
     
        fileNameDWWoOutlierAndDiscardedOrientations = \
                       os.path.join( outputWorkDirectory,
                                    'dw_wo_outlier_and_discarded_orientations' )

        command = 'GkgExecuteCommand DwiSubVolume' + \
                  ' -i ' + \
                  fileNameDWWoOutlier + \
                  ' -o ' + \
                  fileNameDWWoOutlierAndDiscardedOrientations + \
                  ' -ignoredIndices ' + \
                  discardedOrientationList + \
                  ' -verbose true'
        executeCommand( self, subjectName, command, viewOnly )

      
      ########################## creating wo outlier RGB map ##s#################
      if ( len( discardedOrientationList ) > 0 ):

        self.createRGB( fileNameAverageT2WoOutlier,
                        fileNameDWWoOutlierAndDiscardedOrientations,
                        fileNameMask,
                        outputWorkDirectory,
                        subjectName,
                        verbose,
                        viewOnly )

      else:

        self.createRGB( fileNameAverageT2WoOutlier,
                        fileNameDWWoOutlier,
                        fileNameMask,
                        outputWorkDirectory,
                        subjectName,
                        verbose,
                        viewOnly )

      ####################### notifying display thread #########################
      functors[ 'condition-notify-and-release' ]( subjectName,
                                                  'outlier-processed' )


      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 100 )

    except:

      self._functors[ 'task-set-status-as-failed' ]( subjectName,
                                                     sys.exc_info() )

  ##############################################################################
  # creating RGB map
  ##############################################################################

  def createRGB( self,
                 fileNameAverageT2,
                 fileNameDW,
                 fileNameMask,
                 outputWorkDirectory,
                 subjectName,
                 verbose,
                 viewOnly ):

    # building RGB map
    fileNameRGB = os.path.join( outputWorkDirectory, 'dti_rgb_wo_outlier' )
    command = 'GkgExecuteCommand DwiTensorField' + \
              ' -t2 ' + fileNameAverageT2 + \
              ' -dw ' + fileNameDW + \
              ' -m ' +  fileNameMask + \
              ' -f rgb ' + \
              ' -o ' + fileNameRGB + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )

    return fileNameRGB


  ##############################################################################
  # display()
  ##############################################################################

  def display( self, functors, parameters, subjectName, verbose,
               isReportModeActive ):

    ########################### resetting the viewer ###########################
    functors[ 'viewer-reset' ]()

    ####################### collecting raw DWI directory #####################
    rawDwiDirectory = parameters[ 'rawDwiDirectory' ].getValue()
    if ( verbose ):
    
      print  'Raw DWI directory :', rawDwiDirectory

    ####################### collecting rough mask directory ##################
    roughMaskDirectory = parameters[ 'roughMaskDirectory' ].getValue()
    if ( verbose ):
    
      print  'Rough mask directory :', roughMaskDirectory

    ####################### collecting output work directory ###################
    outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()
    if ( verbose ):
    
      print  'Output work directory :', outputWorkDirectory


    ####################### buidling file names ################################
    fileNameAverageT2 = os.path.join( rawDwiDirectory, 't2' )
    fileNameDW = os.path.join( rawDwiDirectory, 'dw' )
    fileNameDWWoOutlier = os.path.join( outputWorkDirectory, 'dw_wo_outlier' )
    fileNameOutliers = os.path.join( outputWorkDirectory, 'outliers.py' )

    ####################### reading acq parameters ###########################
    fileNameAcquisitionParameters = os.path.join( rawDwiDirectory,
                                                 'acquisition_parameters.py' )
    if ( verbose ):

      print 'reading \'' + fileNameAcquisitionParameters + '\''

    globalVariables = dict()
    localVariables = dict()
    execfile( fileNameAcquisitionParameters, globalVariables, localVariables )
    sizeZ = int( localVariables[ 'acquisitionParameters' ][ 'sizeZ' ] )
    qSpacePointCount = int( \
               localVariables[ 'acquisitionParameters' ][ 'qSpacePointCount' ] )

    ####################### displaying average T2, DW ##########################
    if ( verbose ):

      print 'reading \'' + fileNameAverageT2 + '\''

    functors[ 'viewer-create-referential' ]( 'frameDW' )

    functors[ 'viewer-load-object' ]( fileNameAverageT2, 'averageT2' )
    functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'averageT2' )

    functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                       'first_view',
                                                       'Average T2(b=0)' )
    functors[ 'viewer-add-object-to-window' ]( 'averageT2',
                                               'first_view',
                                               'Average T2(b=0)' )

    if ( verbose ):

      print 'reading \'' + fileNameDW + '\''
    functors[ 'viewer-load-object' ]( fileNameDW, 'DW' )
    functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'DW' )

    functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                       'first_view',
                                                       'DW' )
    functors[ 'viewer-add-object-to-window' ]( 'DW', 'first_view', 'DW' )


    ####################### displaying DW_wo_outlier and graph #################
    functors[ 'condition-wait-and-release' ]( subjectName, 'outlier-processed' )

    if ( verbose ):

      print 'reading \'' + fileNameDWWoOutlier + '\''
    functors[ 'viewer-load-object' ]( fileNameDWWoOutlier, 'DWWoOutlier' )
    functors[ 'viewer-assign-referential-to-object' ]( 'frameDW',
                                                       'DWWoOutlier' )

    functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                       'first_view',
                                                       'DW wo outlier' )
    functors[ 'viewer-add-object-to-window' ]( 'DWWoOutlier',
                                               'first_view',
                                               'DW wo outlier' )

    if ( verbose ):

      print 'reading \'' + fileNameOutliers + '\''
    globalVariables = dict()
    localVariables = dict()
    execfile( fileNameOutliers, globalVariables, localVariables )
    functors[ 'viewer-add-object-to-graph' ]( localVariables[ 'orientations' ],
                                              localVariables[ 'slices' ],
                                              'orientation in q-sapce',
                                              'slice position',
                                              0, qSpacePointCount,
                                              0, sizeZ,
                                              'first_view',
                                              'Outlier graph' )

