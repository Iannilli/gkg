from Viewer import *


class DwiOutlierDetectionViewer( Viewer ):

  def __init__( self, minimumSize, parent ):

    Viewer.__init__( self, minimumSize, parent )
    
    ########################## First View ######################################

    self.createView( 'first_view' )
    
    self.addAxialWindow( 'first_view', 0, 0, 1, 1,
                         'Average T2(b=0)', 'Average T2(b=0)' )  
    self.addAxialWindow( 'first_view', 0, 1, 1, 1,
                         'DW', 'DW' )
    self.addGraph( 'first_view', 1, 0, 1, 1,
                   'Outlier graph', 'Outlier graph',
                   'orientation', 'z position', self.pickCallBack )
    self.addAxialWindow( 'first_view', 1, 1, 1, 1,
                         'DW wo outlier', 'DW wo outlier' )

    self._sliceAxis = 'z'
    self._sizeX = 0
    self._sizeY = 0
    self._sizeZ = 0
    self._resolutionX = 1.0
    self._resolutionY = 1.0
    self._resolutionZ = 1.0

  def setDwInformation( self,
                        sliceAxis,
                        sizeX, sizeY, sizeZ,
                        resolutionX, resolutionY, resolutionZ ):
                              
    self._sliceAxis = sliceAxis
    self._sizeX = sizeX
    self._sizeY = sizeY
    self._sizeZ = sizeZ
    self._resolutionX = resolutionX
    self._resolutionY = resolutionY
    self._resolutionZ = resolutionZ

  def pickCallBack( self, event ):

    if isinstance( event.artist, Line2D ):
  
      xdata = event.artist.get_xdata()
      ydata = event.artist.get_ydata()
      index = event.ind
      print 'onpick1 line:', xdata[ index[ 0 ] ], ', ', ydata[ index[ 0 ] ]

      pos = [ 0.0, 0.0, 0.0, 0.0 ]
      if ( self._sliceAxis == 'x' ):
      
        x = ydata[ index[ 0 ] ]
        o = xdata[ index[ 0 ] ]
        pos = [ float( x * self._resolutionX ),
                float( self._sizeY * self._resolutionY / 2 ),
                float( self._sizeZ * self._resolutionZ / 2 ),
                float( o ) ]

      elif ( self._sliceAxis == 'y' ):
      
        y = ydata[ index[ 0 ] ]
        o = xdata[ index[ 0 ] ]
        pos = [ float( self._sizeX * self._resolutionX / 2 ),
                float( y * self._resolutionY ),
                float( self._sizeZ * self._resolutionZ / 2 ),
                float( o ) ]

      elif ( self._sliceAxis == 'z' ):

        z = ydata[ index[ 0 ] ]
        o = xdata[ index[ 0 ] ]
        pos = [ float( self._sizeX * self._resolutionX / 2 ),
                float( self._sizeY * self._resolutionY / 2 ),
                float( z * self._resolutionZ ),
                float( o ) ]

      print 'destination position : ', pos
      self.getWindow( 'first_view', 'DW' ).moveLinkedCursor( pos )


def createDwiOutlierDetectionViewer( minimumSize, parent ):

  return DwiOutlierDetectionViewer( minimumSize, parent )

