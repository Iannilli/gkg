from Algorithm import *
from DwiPosterTask import *


class DwiPosterAlgorithm( Algorithm ):

  def __init__( self, verbose ):

    Algorithm.__init__( self, 'DWI-Poster', verbose, True )

    # input data
    self.addParameter( StringParameter( 'directoryNames', '' ) )
    self.addParameter( StringParameter( 'dataFileOrDirectoryNames', '' ) )
    self.addParameter( StringParameter( 'fileNameHierarchy', '' ) )
    self.addParameter( StringParameter( 'subjectNameGiven', '', False ) )

    self.addParameter( IntegerParameter( 'columnCount', 1, 1, 100000, 1 ) )	
    self.addParameter( IntegerParameter( 'widthResolution', 
                                         800, 1, 100000, 1 ) )
    self.addParameter( IntegerParameter( 'heightResolution',
                                         400, 1, 100000, 1 ) )
    self.addParameter( IntegerParameter( 'backgroundColorR',
                                         0, 0, 255, 0 ) )
    self.addParameter( IntegerParameter( 'backgroundColorG',
                                         0, 0, 255, 0 ) )
    self.addParameter( IntegerParameter( 'backgroundColorB',
                                         0, 0, 255, 0 ) )



  def launch( self ):

    if ( self._verbose ):

      print 'running DWI poster'

    task = DwiPosterTask( self._application )
    task.launch( False )

  def view( self ):

    if ( self._verbose ):

      print 'viewing DWI poster'

    task = DwiPosterTask( self._application )
    task.launch( True )

