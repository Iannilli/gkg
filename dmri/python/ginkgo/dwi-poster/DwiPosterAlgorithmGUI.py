from AlgorithmGUI import *
from ResourceManager import *


class DwiPosterAlgorithmGUI( AlgorithmGUI ):

  def __init__( self, algorithm ):

    AlgorithmGUI.__init__( self,
                           algorithm,
                           ResourceManager().getUIFileName( 'dmri',
                                                            'DwiPoster.ui' ) )

    ###########################################################################
    # connecting input data
    ###########################################################################

    # directory names
    self.connectStringParameterToLineEdit( 'directoryNames',
                                           'lineEdit_DirectoryNames' )
    self.connectDirectoryBrowserToPushButtonAndLineEdit(
                                                   'pushButton_DirectoryNames',
                                                   'lineEdit_DirectoryNames' )
    # data file names
    self.connectStringParameterToLineEdit( \
                                         'dataFileOrDirectoryNames',
                                         'lineEdit_DataFileOrDirectoryNames' )
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit( \
                                         'pushButton_DataFileOrDirectoryNames',
                                         'lineEdit_DataFileOrDirectoryNames' )

    # subject name
    self.connectStringParameterToLineEdit( 'subjectNameGiven',
                                           'lineEdit_SubjectNameGiven' )

    # hierarchy file name
    self.connectStringParameterToLineEdit( 'fileNameHierarchy',
                                           'lineEdit_FileNameHierarchy' )
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit(
                                                 'pushButton_FileNameHierarchy',
                                                 'lineEdit_FileNameHierarchy' )
    # column count
    self.connectIntegerParameterToSpinBox( 'columnCount', 
                                           'spinBox_ColumnCount' )
    # width resolution
    self.connectIntegerParameterToSpinBox( 'widthResolution', 
                                           'spinBox_WidthResolution' )
    # height resolution
    self.connectIntegerParameterToSpinBox( 'heightResolution', 
                                           'spinBox_HeightResolution' )
    # background color
    self.connectIntegerParameterToSpinBox( 'backgroundColorR', 
                                           'spinBox_BackgroundColorR' )
    self.connectIntegerParameterToSpinBox( 'backgroundColorG', 
                                           'spinBox_BackgroundColorG' )
    self.connectIntegerParameterToSpinBox( 'backgroundColorB', 
                                           'spinBox_BackgroundColorB' )



