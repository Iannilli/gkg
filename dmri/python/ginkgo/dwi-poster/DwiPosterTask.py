from Task import *
import os

from PIL import Image

class DwiPosterTask( Task ):

  def __init__( self, application ):

    Task.__init__( self, application )

  ##############################################################################
  # execute()
  ##############################################################################

  def execute( self, functors, parameters, subjectName, verbose, viewOnly ):

    try:

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 0 )

      ####################### collecting output work directory #################
      outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()
      if ( verbose ):

        print  'Output work directory :', outputWorkDirectory

      ####################### collecting input data filename(s) ################
      directoryNames = parameters[ 'directoryNames' ].getValue().split()
      dataFileOrDirectoryNames = \
                     parameters[ 'dataFileOrDirectoryNames' ].getValue().split()
      subjectNameGiven = parameters[ 'subjectNameGiven' ].getValue()
      fileNameHierarchy = parameters[ 'fileNameHierarchy' ].getValue()

      functors[ 'update-progress-bar' ]( subjectName, 10 )
      #################### creating intermediate filename(s) #################
      poster = os.path.join( outputWorkDirectory, 'poster.png' )

      columnCount = parameters[ 'columnCount' ].getValue()
      widthResolution = parameters[ 'widthResolution' ].getValue()
      heightResolution = parameters[ 'heightResolution' ].getValue()
      backGroundColor255 = ( parameters[ 'backgroundColorR' ].getValue(),
                            parameters[ 'backgroundColorG' ].getValue(),
                            parameters[ 'backgroundColorB' ].getValue() )
      backGroundColor = [ \
                    float( parameters[ 'backgroundColorR' ].getValue() ) / 255.0,
                    float( parameters[ 'backgroundColorG' ].getValue() ) / 255.0,
                    float( parameters[ 'backgroundColorB' ].getValue() ) / 255.0 ]

      posterFileName = os.path.join( outputWorkDirectory, 'poster.jpg' )

      quaternionUp = [ 0, 0, 0, 1 ]
      quaternionLeft = [ 0.5, 0.5, 0.5, 0.5 ]
      quaternionFront = [ 0.707107, 0, 0, 0.707107 ]

      zoom = 1

      view_size = [ widthResolution, heightResolution ]

      functors[ 'viewer-set-scene' ]( 'first_view', 
                                      'data',
                                      0, 0, 
                                      [ widthResolution, heightResolution ],
                                      { 'background' : backGroundColor } )

      temporaryDirectory = os.path.join( outputWorkDirectory, 'tmp' )
      if not os.path.exists( temporaryDirectory ):

        os.mkdir( temporaryDirectory )

      if os.path.exists( posterFileName ):

        os.remove( posterFileName )

      poster = None
      snapshotFileNameUp = []
      snapshotFileNameLeft = []
      snapshotFileNameFront = []
      lineIndex = 0
      columnIndex = 0
      pictureCount = 0
      index = 0
      givenDirectory = ''
      for directoryName in directoryNames:

        if( dataFileOrDirectoryNames[ 0 ].find( directoryName ) != -1 ):

          givenDirectory = directoryName
          break

      pictureCount = len( directoryNames )
      lineCount = pictureCount / columnCount
      if( pictureCount % columnCount > 0 ):

        lineCount += 1

      if( fileNameHierarchy != '' ):

        functors[ 'viewer-load-object' ]( fileNameHierarchy , 'hierarchy ' )
      for directoryName in directoryNames:

          subject = os.path.split( directoryName )[ 1 ]

          if( subject == '' ):

            subject  = os.path.split( os.path.split( directoryName )[ 0 ] )[ 1 ]
          index = 0
          for fileName in dataFileOrDirectoryNames:

            bundleName = os.path.join( directoryName , 
                                 fileName.replace( subjectNameGiven, subject ) )
            b, bundleExtension = os.path.splitext( bundleName )

            if ( bundleExtension == '.bundles' ):

              functors[ 'viewer-load-object' ]( bundleName,
                                                'dataObject' + str( index ) )
              functors[ 'viewer-add-object-to-window' ]( \
                                                    'dataObject' + str( index ),
                                                    'first_view',
                                                    'data' )
              functors[ 'viewer-set-diffuse' ]( 'dataObject' + str( index ),
                                                [ 1.0, 1.0, 1.0, 0.4 ] )

              index += 1

          snapshotFileNameUp = os.path.join( temporaryDirectory,
                                             subject + '_up.jpg'  )
          snapshotFileNameFront = os.path.join( temporaryDirectory,
                                                subject + '_front.jpg' )
          snapshotFileNameLeft = os.path.join( temporaryDirectory,
                                               subject + '_left.jpg' )


          functors[ 'viewer-adjust-camera' ]( 'first_view', 'data',
                                              None, quaternionUp, zoom )
          functors[ 'viewer-snapshot' ]( 'first_view', 'data', 
                                         snapshotFileNameUp )
          functors[ 'viewer-adjust-camera' ]( 'first_view', 'data',
                                              None, quaternionFront, zoom )
          functors[ 'viewer-snapshot' ]( 'first_view', 'data', 
                                         snapshotFileNameFront )

          functors[ 'viewer-adjust-camera' ]( 'first_view', 'data',
                                              None, quaternionLeft, zoom )
          functors[ 'viewer-snapshot' ]( 'first_view', 'data', 
                                         snapshotFileNameLeft )

          imageUp = map( Image.open, [ snapshotFileNameUp ] )[ 0 ]
          imageFront = map( Image.open, [ snapshotFileNameFront ] )[ 0 ]
          imageLeft = map( Image.open, [ snapshotFileNameLeft ] )[ 0 ]
          if ( poster == None ):

            poster = Image.new( "RGBA",
                          ( imageUp.size[ 0 ] * columnCount,
                            imageUp.size[ 1 ]  * lineCount * 3 ),
                          backGroundColor255 )

          if ( columnIndex >= columnCount ):

            lineIndex += 1
            columnIndex = 0

          poster.paste( imageUp, ( columnIndex * imageUp.size[ 0 ],
                                   lineIndex * imageUp.size[ 1 ] ) )
          poster.paste( imageFront, ( columnIndex * imageFront.size[ 0 ],
                                      lineIndex * imageFront.size[ 1 ] + \
                                      ( lineCount * imageUp.size[ 1 ] ) ) )
          poster.paste( imageLeft, ( columnIndex * imageLeft.size[ 0 ],
                                     lineIndex * imageLeft.size[ 1 ] + \
                                     ( 2 * lineCount * imageUp.size[ 1 ] ) ) )
          columnIndex += 1

          for i in xrange( index ):

            functors[ 'viewer-delete-object' ]( 'dataObject' + str( i ) )
          
      poster.save( posterFileName )

      ########################## updating progress bar #########################
      functors[ 'update-progress-bar' ]( subjectName, 100 )

    except:

      self._functors[ 'task-set-status-as-failed' ]( subjectName,
                                                     sys.exc_info() )

  ##############################################################################
  # display()
  ##############################################################################

  def display( self, functors, parameters, subjectName, verbose,
               isReportModeActive ):

    ########################### resetting the viewer ###########################
    functors[ 'viewer-reset' ]()



