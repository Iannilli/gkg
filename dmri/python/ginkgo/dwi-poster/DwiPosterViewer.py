from Viewer import *


class DwiPosterViewer( Viewer ):

  def __init__( self, minimumSize, parent ):

    Viewer.__init__( self, minimumSize, parent )

    ########################## First View ######################################

    self.createView( 'first_view' )
    self.addSagittalWindow( 'first_view', 0, 0, 1, 1,
                      'data', 'data',
                      backgroundColor = [ 0, 0, 0, 0 ],
                      isMuteToolButtonVisible = False,
                      isColorMapToolButtonVisible = False,
                      isSnapshotToolButtonVisible = False,
                      isZoomToolButtonVisible = False )

def createDwiPosterViewer( minimumSize, parent ):

  return DwiPosterViewer( minimumSize, parent )

