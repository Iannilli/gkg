from Algorithm import *
from DwiQualityCheckReportingTask import *


class DwiQualityCheckReportingAlgorithm( Algorithm ):

  def __init__( self, verbose ):

    Algorithm.__init__( self, 'DWI-Quality-Check-Reporting', verbose,
                        True )

    # project and subject information
    self.addParameter( \
      StringParameter( 'projectName', '' ) )
    self.addParameter( \
      StringParameter( 'subjectName', '' ) )
    self.addParameter( \
      StringParameter( 'timeStep', '' ) )

    # input QC directories
    self.addParameter( \
      StringParameter( 'directoryNameDataImportAndQSpaceSampling', '' ) )
    self.addParameter( \
      StringParameter( 'directoryNameToAnatomyMatching', '' ) )
    self.addParameter( \
      StringParameter( 'directoryNameRoughMask', '' ) )
    self.addParameter( \
      StringParameter( 'directoryNameOutlierDetection', '' ) )
    self.addParameter( \
      StringParameter( 'directoryNameSusceptibilityArtifactCorrection', '' ) )
    self.addParameter( \
      StringParameter( 'directoryNameEddyCurrentAndMotion', '' ) )


  def launch( self ):

    if ( self._verbose ):

      print \
        'running DWI quality check reporting'

    task = DwiQualityCheckReportingTask( self._application )
    task.launch( False )


  def view( self ):

    if ( self._verbose ):

      print \
        'viewing DWI quality check reporting'

    task = DwiQualityCheckReportingTask( self._application )
    task.launch( True )

