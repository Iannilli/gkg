from AlgorithmGUI import *
from ResourceManager import *


class DwiQualityCheckReportingAlgorithmGUI( AlgorithmGUI ):

  def __init__( self, algorithm ):

    AlgorithmGUI.__init__( self,
                           algorithm,
                           ResourceManager().getUIFileName(
                                         'dmri',
                                         'DwiQualityCheckReporting.ui' ) )

    ############################################################################
    # connecting input DWI interface
    ############################################################################

    # project name
    self.connectStringParameterToLineEdit( \
                 'projectName',
                 'lineEdit_ProjectName' )
                 
    # subject name
    self.connectStringParameterToLineEdit( \
                 'subjectName',
                 'lineEdit_SubjectName' )

    # time step
    self.connectStringParameterToLineEdit( \
                 'timeStep',
                 'lineEdit_TimeStep' )

    # input data import and q-space sampling directory
    self.connectStringParameterToLineEdit( \
                 'directoryNameDataImportAndQSpaceSampling',
                 'lineEdit_DirectoryNameDataImportAndQSpaceSampling' )
    self.connectDirectoryBrowserToPushButtonAndLineEdit(
                 'pushButton_DirectoryNameDataImportAndQSpaceSampling',
                 'lineEdit_DirectoryNameDataImportAndQSpaceSampling' )

    # input to anatomy matching directory
    self.connectStringParameterToLineEdit( \
                 'directoryNameToAnatomyMatching',
                 'lineEdit_DirectoryNameToAnatomyMatching' )
    self.connectDirectoryBrowserToPushButtonAndLineEdit(
                 'pushButton_DirectoryNameToAnatomyMatching',
                 'lineEdit_DirectoryNameToAnatomyMatching' )

    # input rough mask directory
    self.connectStringParameterToLineEdit( \
                 'directoryNameRoughMask',
                 'lineEdit_DirectoryNameRoughMask' )
    self.connectDirectoryBrowserToPushButtonAndLineEdit(
                 'pushButton_DirectoryNameRoughMask',
                 'lineEdit_DirectoryNameRoughMask' )

    # input outlier detection directory
    self.connectStringParameterToLineEdit( \
                 'directoryNameOutlierDetection',
                 'lineEdit_DirectoryNameOutlierDetection' )
    self.connectDirectoryBrowserToPushButtonAndLineEdit(
                 'pushButton_DirectoryNameOutlierDetection',
                 'lineEdit_DirectoryNameOutlierDetection' )

    # input susceptibility artifact correction directory
    self.connectStringParameterToLineEdit( \
                 'directoryNameSusceptibilityArtifactCorrection',
                 'lineEdit_DirectoryNameSusceptibilityArtifactCorrection' )
    self.connectDirectoryBrowserToPushButtonAndLineEdit(
                 'pushButton_DirectoryNameSusceptibilityArtifactCorrection',
                 'lineEdit_DirectoryNameSusceptibilityArtifactCorrection' )

    # input eddy current and motion directory
    self.connectStringParameterToLineEdit( \
                 'directoryNameEddyCurrentAndMotion',
                 'lineEdit_DirectoryNameEddyCurrentAndMotion' )
    self.connectDirectoryBrowserToPushButtonAndLineEdit(
                 'pushButton_DirectoryNameEddyCurrentAndMotion',
                 'lineEdit_DirectoryNameEddyCurrentAndMotion' )


