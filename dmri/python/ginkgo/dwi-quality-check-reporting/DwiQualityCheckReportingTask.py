from Task import *
import shutil
import anatomist.api as anatomist
from datetime import date

from ResourceManager import *


class DwiQualityCheckReportingTask( Task ):

  def __init__( self, application ):

    Task.__init__( self, application )


  ##############################################################################
  # execute()
  ##############################################################################

  def execute( self, functors, parameters, subjectName, verbose, viewOnly ):

    try:

      ####################### acquiring conditions #############################
      functors[ 'condition-acquire' ]( subjectName,
                                       'quality-check-report-ready' )


      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 0 )


      ####################### collecting output work directory #################
      outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()
      if ( verbose ):
      
        print  'Output work directory :', outputWorkDirectory


      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 5 )


      ####################### collecting directories ###########################
      directoryNameDataImportAndQSpaceSampling = \
      parameters[ 'directoryNameDataImportAndQSpaceSampling' ].getValue()

      directoryNameRoughMask = \
      parameters[ 'directoryNameRoughMask' ].getValue()

      directoryNameOutlierDetection = \
      parameters[ 'directoryNameOutlierDetection' ].getValue()

      directoryNameSusceptibilityArtifactCorrection = \
      parameters[ 'directoryNameSusceptibilityArtifactCorrection' ].getValue()

      directoryNameEddyCurrentAndMotion = \
      parameters[ 'directoryNameEddyCurrentAndMotion' ].getValue()

      directoryNameToAnatomyMatching = \
      parameters[ 'directoryNameToAnatomyMatching' ].getValue()


      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 10 )


      outputDirectoryDataImportAndQSpaceSampling = os.path.join( \
                           outputWorkDirectory,
                           'DWI-Data-Import-And-QSpace-Sampling' )
      outputDirectoryOutlierDetection = os.path.join( \
                           outputWorkDirectory,
                           'DWI-Outlier-Detection' )
      outputDirectorySusceptibilityArtifactCorrection = os.path.join( \
                           outputWorkDirectory,
                           'DWI-Susceptibility-Artifact-Correction' )
      outputDirectoryEddyCurrentAndMotionCorrection = os.path.join( \
                           outputWorkDirectory,
                           'DWI-Eddy-Current-And-Motion-Correction' )
      outputDirectoryTensorFitting = os.path.join( \
                           outputWorkDirectory,
                           'DWI-Tensor-Fitting' )

      # snap uncorrected data ##################################################
      if ( len( directoryNameDataImportAndQSpaceSampling ) ):

        if not( os.path.exists( outputDirectoryDataImportAndQSpaceSampling ) ):
        
          os.mkdir( outputDirectoryDataImportAndQSpaceSampling )

        functors[ 'snapshot-single-volume' ]( \
          subjectName,
          os.path.join( directoryNameDataImportAndQSpaceSampling,
                        't2' ),
          'B-W LINEAR', None, None,
          [ 512, 512 ],
          os.path.join( outputDirectoryDataImportAndQSpaceSampling,
                        'snapshot-axial-t2.jpg' ),
          os.path.join( outputDirectoryDataImportAndQSpaceSampling,
                        'snapshot-coronal-t2.jpg' ),
          os.path.join( outputDirectoryDataImportAndQSpaceSampling,
                        'snapshot-sagittal-t2.jpg' ) )

        functors[ 'snapshot-single-volume' ]( \
          subjectName,
          os.path.join( directoryNameDataImportAndQSpaceSampling,
                        'dw' ),
          'B-W LINEAR', None, None,
          [ 512, 512 ],
          os.path.join( outputDirectoryDataImportAndQSpaceSampling,
                        'snapshot-axial-dw.jpg' ),
          os.path.join( outputDirectoryDataImportAndQSpaceSampling,
                        'snapshot-coronal-dw.jpg' ),
          os.path.join( outputDirectoryDataImportAndQSpaceSampling,
                        'snapshot-sagittal-dw.jpg' ) )

        functors[ 'snapshot-volume-fusion-and-odf-field' ]( \
          subjectName,
          os.path.join( directoryNameDataImportAndQSpaceSampling,
                        't2' ),
          'B-W LINEAR', None, None,
          os.path.join( directoryNameDataImportAndQSpaceSampling,
                        'dti_rgb' ),
          None, None, None,
          os.path.join( directoryNameDataImportAndQSpaceSampling,
                        'aqbi_odf_texture_map.texturemap' ),
          None,
          None,
          [ 1024, 1024 ],
          1.0,
          os.path.join( outputDirectoryDataImportAndQSpaceSampling,
                        'snapshot-axial-t2-rgb-odf.jpg' ),
          os.path.join( outputDirectoryDataImportAndQSpaceSampling,
                        'snapshot-coronal-t2-rgb-odf.jpg' ),
          os.path.join( outputDirectoryDataImportAndQSpaceSampling,
                        'snapshot-sagittal-t2-rgb-odf.jpg' ) )


      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 20 )


      # snap outlier corrected data ############################################
      if ( len( directoryNameDataImportAndQSpaceSampling ) and \
           len( directoryNameOutlierDetection ) ):

        if not( os.path.exists( outputDirectoryOutlierDetection ) ):
        
          os.mkdir( outputDirectoryOutlierDetection )

        self.snapOutliers( functors,
                           subjectName,
                           os.path.join( \
                             directoryNameDataImportAndQSpaceSampling,
                             'acquisition_parameters.py' ),
                           os.path.join( \
                             directoryNameOutlierDetection,
                             'outliers.py' ),
                           os.path.join( \
                             outputDirectoryOutlierDetection,
                             'snapshot-outliers.jpg' ),
                           viewOnly )


      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 30 )


      # snap susceptiblity corrected data ######################################
      if (  len( directoryNameDataImportAndQSpaceSampling ) and \
            len( directoryNameSusceptibilityArtifactCorrection ) ):

        if not( os.path.exists( \
                            outputDirectorySusceptibilityArtifactCorrection ) ):
        
          os.mkdir( outputDirectorySusceptibilityArtifactCorrection )

        functors[ 'snapshot-volume-fusion' ]( \
          subjectName, 
          os.path.join( directoryNameDataImportAndQSpaceSampling,
                        't2' ),
          'B-W LINEAR', None, None,
          os.path.join( directoryNameSusceptibilityArtifactCorrection,
                        'b0_magnitude_echo1' ),
          'RED TEMPERATURE', None, None,
          os.path.join( directoryNameSusceptibilityArtifactCorrection,
                        'b0_to_dw.trm' ),
          [ 512, 512 ],
          os.path.join( outputDirectorySusceptibilityArtifactCorrection,
                        'snapshot-axial-t2-b0-magnitude.jpg' ),
          os.path.join( outputDirectorySusceptibilityArtifactCorrection,
                        'snapshot-coronal-t2-b0-magnitude.jpg' ),
          os.path.join( outputDirectorySusceptibilityArtifactCorrection,
                        'snapshot-sagittal-t2-b0-magnitude.jpg' ) )

        functors[ 'snapshot-volume-fusion' ]( \
          subjectName, 
          os.path.join( directoryNameDataImportAndQSpaceSampling,
                        't2' ),
          'B-W LINEAR', None, None,
          os.path.join( directoryNameSusceptibilityArtifactCorrection,
                        'b0_phase_unwrapped' ),
          'RAINBOW', None, None,
          os.path.join( directoryNameSusceptibilityArtifactCorrection,
                        'b0_to_dw.trm' ),
          [ 512, 512 ],
          os.path.join( outputDirectorySusceptibilityArtifactCorrection,
                        'snapshot-axial-t2-b0-phase.jpg' ),
          os.path.join( outputDirectorySusceptibilityArtifactCorrection,
                        'snapshot-coronal-t2-b0-phase.jpg' ),
          os.path.join( outputDirectorySusceptibilityArtifactCorrection,
                        'snapshot-sagittal-t2-b0-phase.jpg' ) )

        if ( len( directoryNameToAnatomyMatching ) ):

          functors[ 'snapshot-volume-fusion' ]( \
            subjectName,
            os.path.join( directoryNameToAnatomyMatching,
                          't1' ),
            'B-W LINEAR', None, None,
            os.path.join( directoryNameDataImportAndQSpaceSampling,
                          'dti_rgb' ),
            None, None, None,
            os.path.join( directoryNameToAnatomyMatching,
                          'dw_to_t1.trm' ),
            [ 512, 512 ],
            os.path.join( outputDirectorySusceptibilityArtifactCorrection,
                          'snapshot-axial-t1-rgb-with-susceptibility.jpg' ),
            os.path.join( outputDirectorySusceptibilityArtifactCorrection,
                          'snapshot-coronal-t1-rgb-with-susceptibility.jpg' ),
            os.path.join( outputDirectorySusceptibilityArtifactCorrection,
                          'snapshot-sagittal-t1-rgb-with-susceptibility.jpg' ) )

          functors[ 'snapshot-volume-fusion' ]( \
            subjectName,
            os.path.join( directoryNameToAnatomyMatching,
                          't1' ),
            'B-W LINEAR', None, None,
            os.path.join( directoryNameSusceptibilityArtifactCorrection,
                          'dti_rgb_wo_susceptibility' ),
            None, None, None,
            os.path.join( directoryNameToAnatomyMatching,
                          'dw_to_t1.trm' ),
            [ 512, 512 ],
            os.path.join( outputDirectorySusceptibilityArtifactCorrection,
                          'snapshot-axial-t1-rgb-without-susceptibility.jpg' ),
            os.path.join( outputDirectorySusceptibilityArtifactCorrection,
                         'snapshot-coronal-t1-rgb-without-susceptibility.jpg' ),
            os.path.join( outputDirectorySusceptibilityArtifactCorrection,
                       'snapshot-sagittal-t1-rgb-without-susceptibility.jpg' ) )

        command = 'GkgExecuteCommand Transform3dComposer ' + \
                  ' -i ' + os.path.join( \
                                 directoryNameToAnatomyMatching,
                                 't1_to_dw.trm' ) + ' ' + \
                           os.path.join( \
                                 directoryNameSusceptibilityArtifactCorrection,
                                 'dw_to_b0.trm' ) + \
                  ' -o ' + os.path.join( \
                                outputDirectorySusceptibilityArtifactCorrection,
                                't1_to_b0.trm' ) + \
                  ' -verbose true'
        executeCommand( self, subjectName, command, viewOnly )

        command = 'GkgExecuteCommand Resampling3d ' + \
                  ' -reference ' + os.path.join( directoryNameToAnatomyMatching,
                                                 'Morphologist',
                                                 'brain_t1' ) + \
                  ' -template ' + os.path.join( \
                                  directoryNameSusceptibilityArtifactCorrection,
                                 'susceptibility_shift_map' ) + \
                  ' -transforms ' + os.path.join( \
                                outputDirectorySusceptibilityArtifactCorrection,
                                't1_to_b0.trm' ) + \
                  ' -output ' + os.path.join( \
                                outputDirectorySusceptibilityArtifactCorrection,
                                'mask_in_b0_frame.ima' ) + \
                  ' -order 0 ' + \
                  ' -verbose true'
        executeCommand( self, subjectName, command, viewOnly )

        self.snapHistogramOfVolume( \
          functors,
          subjectName,
          os.path.join( directoryNameSusceptibilityArtifactCorrection,
                        'susceptibility_shift_map' ),
          os.path.join( outputDirectorySusceptibilityArtifactCorrection,
                        'mask_in_b0_frame.ima' ),
          100, None, None,
          'displacement in pixel along phase axis',
          None,
          os.path.join( outputDirectorySusceptibilityArtifactCorrection,
                        'snapshot-histogram-displacement-in-pixel.jpg' ),
          viewOnly )


      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 40 )


      # snap eddy current or motion corrected data #############################
      if (  len( directoryNameEddyCurrentAndMotion ) and \
            len( directoryNameToAnatomyMatching ) ):

        if not( os.path.exists( \
                              outputDirectoryEddyCurrentAndMotionCorrection ) ):
        
          os.mkdir( outputDirectoryEddyCurrentAndMotionCorrection )

        self.snapMotionProfile( functors,
                                subjectName,
                                os.path.join( \
                                  directoryNameEddyCurrentAndMotion,
                                  'motion_profile.py'  ),
                                os.path.join( \
                                  outputDirectoryEddyCurrentAndMotionCorrection,
                                  'snapshot-motion-profile.jpg' ),
                                viewOnly )


      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 50 )


      # snap tensor quantitative maps and ODF ##################################
      if (  len( directoryNameEddyCurrentAndMotion ) and \
            len( directoryNameToAnatomyMatching ) ):

        if not( os.path.exists(  outputDirectoryTensorFitting ) ):
        
          os.mkdir( outputDirectoryTensorFitting )

        specy = ResourceManager().getCurrentSpecy( 'dmri' )
        
	if ( specy == 'Human' ):
           	
          command = 'GkgExecuteCommand Resampling3d ' + \
                    ' -reference ' + os.path.join( directoryNameToAnatomyMatching,
                                                   'Morphologist',
                                                   'brain_t1' ) + \
                    ' -template ' + os.path.join( \
                                             directoryNameEddyCurrentAndMotion,
                                             't2_wo_eddy_current_and_motion' ) + \
                    ' -transforms ' + \
                                  os.path.join( directoryNameToAnatomyMatching,
                                                   't1_to_dw.trm' ) + \
                    ' -output ' + os.path.join( outputDirectoryTensorFitting,
                                        	'mask.ima' ) + \
                    ' -order 0 ' + \
                    ' -verbose true'
          executeCommand( self, subjectName, command, viewOnly )

	  command = 'GkgExecuteCommand DwiTensorField ' + \
                    ' -t2 ' + os.path.join( directoryNameEddyCurrentAndMotion,
                                            't2_wo_eddy_current_and_motion' ) + \
                    ' -dw ' + os.path.join( directoryNameEddyCurrentAndMotion,
                                            'dw_wo_eddy_current_and_motion' ) + \
                    ' -m ' + os.path.join( outputDirectoryTensorFitting,
                                           'mask.ima' ) + \
                    ' -f adc fa lambda_parallel lambda_transverse rgb ' + \
                    ' -o ' + os.path.join( outputDirectoryTensorFitting,
                                           'dti_adc.ima' ) + ' ' + \
                             os.path.join( outputDirectoryTensorFitting,
                                           'dti_fa.ima' ) + ' ' + \
                             os.path.join( outputDirectoryTensorFitting,
                                           'dti_lambda_parallel.ima' ) + ' ' + \
                             os.path.join( outputDirectoryTensorFitting,
                                           'dti_lambda_transverse.ima' ) + ' ' + \
                             os.path.join( outputDirectoryTensorFitting,
                                           'dti_rgb.ima' ) + ' ' + \
                    ' -stringParameters robust_positive_definite ' + \
                    ' -verbose true'
          executeCommand( self, subjectName, command, viewOnly )


        elif ( specy == 'Mouse' ):

          shutil.copyfile( os.path.join( directoryNameRoughMask,
	                                 'mask.ima' ), 
	  	           os.path.join( outputDirectoryTensorFitting,
			                 'mask.ima' ) )
          shutil.copyfile( os.path.join( directoryNameRoughMask,
	                                 'mask.dim' ), 
	  	           os.path.join( outputDirectoryTensorFitting,
			                 'mask.dim' ) )	
          shutil.copyfile( os.path.join( directoryNameRoughMask,
	                                 'mask.ima.minf' ), 
	  	           os.path.join( outputDirectoryTensorFitting,
			                 'mask.ima.minf' ) )
	
	  command = 'GkgExecuteCommand DwiTensorField ' + \
                    ' -t2 ' + os.path.join( directoryNameEddyCurrentAndMotion,
                                            't2_wo_eddy_current_and_motion' ) + \
                    ' -dw ' + os.path.join( directoryNameEddyCurrentAndMotion,
                                            'dw_wo_eddy_current_and_motion' ) + \
                    ' -m ' + os.path.join( outputDirectoryTensorFitting,
                                           'mask.ima' ) + \
                    ' -f adc fa lambda_parallel lambda_transverse rgb ' + \
                    ' -o ' + os.path.join( outputDirectoryTensorFitting,
                                           'dti_adc.ima' ) + ' ' + \
                             os.path.join( outputDirectoryTensorFitting,
                                           'dti_fa.ima' ) + ' ' + \
                             os.path.join( outputDirectoryTensorFitting,
                                           'dti_lambda_parallel.ima' ) + ' ' + \
                             os.path.join( outputDirectoryTensorFitting,
                                           'dti_lambda_transverse.ima' ) + ' ' + \
                             os.path.join( outputDirectoryTensorFitting,
                                           'dti_rgb.ima' ) + ' ' + \
                    ' -stringParameters linear_square ' + \
                    ' -verbose true'
          executeCommand( self, subjectName, command, viewOnly )

        command = 'GkgExecuteCommand DwiOdfField ' + \
                  ' -t2 ' + os.path.join( directoryNameEddyCurrentAndMotion,
                                          't2_wo_eddy_current_and_motion' ) + \
                  ' -dw ' + os.path.join( directoryNameEddyCurrentAndMotion,
                                          'dw_wo_eddy_current_and_motion' ) + \
                  ' -m ' + os.path.join( outputDirectoryTensorFitting,
                                         'mask.ima' ) + \
                  ' -type aqball_odf_cartesian_field ' + \
                  ' -f odf_site_map odf_texture_map  ' + \
                  ' -o ' + os.path.join( outputDirectoryTensorFitting,
                                         'dti_odf_site_map.sitemap' ) + ' ' + \
                           os.path.join( outputDirectoryTensorFitting,
                                         'dti_odf_texture_map.texturemap' ) + \
                  ' -scalarParameters 2 0.006 0.0 ' + \
                  ' -verbose true'
        executeCommand( self, subjectName, command, viewOnly )

        functors[ 'snapshot-single-volume' ]( \
          subjectName, 
          os.path.join( outputDirectoryTensorFitting,
                        'dti_adc' ),
          'B-W LINEAR', 0.0, 1e-9,
          [ 512, 512 ],
          os.path.join( outputDirectoryTensorFitting,
                        'snapshot-axial-adc.jpg' ),
          os.path.join( outputDirectoryTensorFitting,
                        'snapshot-coronal-adc.jpg' ),
          os.path.join( outputDirectoryTensorFitting,
                        'snapshot-sagittal-adc.jpg' ) )

        self.snapHistogramOfVolume( \
          functors,
          subjectName,
          os.path.join( outputDirectoryTensorFitting,
                        'dti_adc' ),
          os.path.join( outputDirectoryTensorFitting,
                                         'mask.ima' ),
          100, 0.0, 2e-9,
          'ADC in m^2/s',
          None,
          os.path.join( outputDirectoryTensorFitting,
                        'snapshot-histogram-adc.jpg' ),
          viewOnly )

        functors[ 'snapshot-single-volume' ]( \
          subjectName, 
          os.path.join( outputDirectoryTensorFitting,
                        'dti_fa' ),
          'B-W LINEAR', None, None,
          [ 512, 512 ], 
          os.path.join( outputDirectoryTensorFitting,
                        'snapshot-axial-fa.jpg' ),
          os.path.join( outputDirectoryTensorFitting,
                        'snapshot-coronal-fa.jpg' ),
          os.path.join( outputDirectoryTensorFitting,
                        'snapshot-sagittal-fa.jpg' ) )

        self.snapHistogramOfVolume( \
          functors,
          subjectName,
          os.path.join( outputDirectoryTensorFitting,
                        'dti_fa' ),
          os.path.join( outputDirectoryTensorFitting,
                                         'mask.ima' ),
          100, None, None,
          'FA between 0 and 1',
          None,
          os.path.join( outputDirectoryTensorFitting,
                        'snapshot-histogram-fa.jpg' ),
          viewOnly )

        functors[ 'snapshot-single-volume' ]( \
          subjectName, 
          os.path.join( outputDirectoryTensorFitting,
                        'dti_lambda_parallel' ),
          'B-W LINEAR', 0.0, 1e-9,
          [ 512, 512 ],
          os.path.join( outputDirectoryTensorFitting,
                        'snapshot-axial-lambda-parallel.jpg' ),
          os.path.join( outputDirectoryTensorFitting,
                        'snapshot-coronal-lambda-parallel.jpg' ),
          os.path.join( outputDirectoryTensorFitting,
                        'snapshot-sagittal-lambda-parallel.jpg' ) )

        self.snapHistogramOfVolume( \
          functors,
          subjectName,
          os.path.join( outputDirectoryTensorFitting,
                        'dti_lambda_parallel' ),
          os.path.join( outputDirectoryTensorFitting,
                                         'mask.ima' ),
          100, 0.0, 2e-9,
          'parallel diffusivity in m^2/s',
          None,
          os.path.join( outputDirectoryTensorFitting,
                        'snapshot-histogram-lambda-parallel.jpg' ),
          viewOnly )

        functors[ 'snapshot-single-volume' ]( \
          subjectName, 
          os.path.join( outputDirectoryTensorFitting,
                        'dti_lambda_transverse' ),
          'B-W LINEAR', 0.0, 1e-9,
          [ 512, 512 ],
          os.path.join( outputDirectoryTensorFitting,
                        'snapshot-axial-lambda-transverse.jpg' ),
          os.path.join( outputDirectoryTensorFitting,
                        'snapshot-coronal-lambda-transverse.jpg' ),
          os.path.join( outputDirectoryTensorFitting,
                        'snapshot-sagittal-lambda-transverse.jpg' ) )

        self.snapHistogramOfVolume( \
          functors,
          subjectName,
          os.path.join( outputDirectoryTensorFitting,
                        'dti_lambda_transverse' ),
          os.path.join( outputDirectoryTensorFitting,
                                         'mask.ima' ),
          100, 0.0, 2e-9,
          'transverse diffusivity in m^2/s',
          None,
          os.path.join( outputDirectoryTensorFitting,
                        'snapshot-histogram-lambda-transverse.jpg' ),
          viewOnly )

        functors[ 'snapshot-single-volume' ]( \
          subjectName, 
          os.path.join( outputDirectoryTensorFitting,
                        'dti_rgb' ),
          None, None, None,
          [ 512, 512 ], 
          os.path.join( outputDirectoryTensorFitting,
                        'snapshot-axial-rgb.jpg' ),
          os.path.join( outputDirectoryTensorFitting,
                        'snapshot-coronal-rgb.jpg' ),
          os.path.join( outputDirectoryTensorFitting,
                        'snapshot-sagittal-rgb.jpg' ) )

        functors[ 'snapshot-volume-fusion-and-odf-field' ]( \
          subjectName,
          os.path.join( directoryNameEddyCurrentAndMotion,
                        't2_wo_eddy_current_and_motion' ),
          'B-W LINEAR', None, None,
          os.path.join( outputDirectoryTensorFitting,
                        'dti_rgb' ),
          None, None, None,
          os.path.join( outputDirectoryTensorFitting,
                        'dti_odf_texture_map.texturemap' ),
          None,
          None,
          [ 1024, 1024 ],
          1.0,
          os.path.join( outputDirectoryTensorFitting,
                        'snapshot-axial-t1-rgb-odf.jpg' ),
          os.path.join( outputDirectoryTensorFitting,
                        'snapshot-coronal-t1-rgb-odf.jpg' ),
          os.path.join( outputDirectoryTensorFitting,
                        'snapshot-sagittal-t1-rgb-odf.jpg' ) )


      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 80 )


      # writing latex file #####################################################

      fileNameLateX = os.path.join( outputWorkDirectory,
                                    'report.tex' )
      of = open( fileNameLateX, 'wt' )
      
      of.write( '\documentclass[a4paper, 11pt]{article}\n' )
      of.write( '\usepackage{fullpage} % changes the margin\n' )
      of.write( '\usepackage{graphicx}\n' )
      of.write( '\n' )
      of.write( '\n' )
      of.write( '\\begin{document}\n' )
      of.write( '\n' )
      of.write( '\n' )
      of.write( '\\noindent\n' )
      of.write( '\large\\textbf{DWI QC Reporting}             \hfill \\textbf{Project: ' + parameters[ 'projectName' ].getValue() + '} \\\\\n' )
      of.write( '\\normalsize performed by NeuroSpin platform \hfill Subject name: ' + parameters[ 'subjectName' ].getValue() + ' \\\\\n' )
      of.write( '                                             \hfill Time step: ' + parameters[ 'timeStep' ].getValue() + ' \\\\\n' )
      of.write( 'powered by Ginkgo' + '                  \hfill QC Date: ' + str( date.today() ) + '\n' )
      of.write( '\n' )
      of.write( '\n' )
      of.write( '\\vspace{2cm}\n' )
      of.write( '\n' )
      of.write( '\n' )

      if ( len( directoryNameDataImportAndQSpaceSampling ) ):

        of.write( '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n' )
        of.write( '\section*{T2-weighted ($b=0s/mm^2$) and diffusion-weighted raw data}\n' )
        of.write( '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n' )
        of.write( '\n' )
        of.write( '\\begin{tabular}{cccc}\n' )
        of.write( '\n' )
        of.write( '  \includegraphics[width=3.8cm]{' + os.path.join( outputDirectoryDataImportAndQSpaceSampling,
                                                                     'snapshot-axial-t2.jpg' ) + '} &\n' )
        of.write( '  \includegraphics[width=3.8cm]{' + os.path.join( outputDirectoryDataImportAndQSpaceSampling,
                                                                     'snapshot-coronal-t2.jpg' ) + '} &\n' )
        of.write( '  \includegraphics[width=3.8cm]{' + os.path.join( outputDirectoryDataImportAndQSpaceSampling,
                                                                     'snapshot-axial-dw.jpg' ) + '} &\n' )
        of.write( '  \includegraphics[width=3.8cm]{' + os.path.join( outputDirectoryDataImportAndQSpaceSampling,
                                                                     'snapshot-coronal-dw.jpg' ) + '} \\\\\n' )
        of.write( '       &\n' )
        of.write( '  \includegraphics[width=3.8cm]{' + os.path.join( outputDirectoryDataImportAndQSpaceSampling,
                                                                     'snapshot-sagittal-t2.jpg' ) + '} & &\n' )
        of.write( '  \includegraphics[width=3.8cm]{' + os.path.join( outputDirectoryDataImportAndQSpaceSampling,
                                                                     'snapshot-sagittal-dw.jpg' ) + '}\n' )
        of.write( '\n' )
        of.write( '\end{tabular}\n' )
        of.write( '\n' )
        of.write( '\n' )
 
        of.write( '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n' )
        of.write( '\section*{Raw orientation distribution function field}\n' )
        of.write( '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n' )
        of.write( '\n' )
        of.write( '\\begin{tabular}{cc}\n' )
        of.write( '\n' )
        of.write( '  \includegraphics[width=8.0cm]{' + os.path.join( outputDirectoryDataImportAndQSpaceSampling,
                                                                     'snapshot-axial-t2-rgb-odf.jpg' ) + '} &\n' )
        of.write( '  \includegraphics[width=8.0cm]{' + os.path.join( outputDirectoryDataImportAndQSpaceSampling,
                                                                     'snapshot-coronal-t2-rgb-odf.jpg' ) + '}\n' )
        of.write( '\n' )
        of.write( '\end{tabular}\n' )
        of.write( '\n' )
        of.write( '\\newpage\n' )
        of.write( '\n' )

      if ( len( directoryNameDataImportAndQSpaceSampling ) and \
           len( directoryNameOutlierDetection ) ):

        of.write( '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n' )
        of.write( '\section*{Outlier detection and correction}\n' )
        of.write( '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n' )
        of.write( '\n' )
        of.write( '\\begin{tabular}{cc}\n' )
        of.write( '\n' )
        of.write( '  \includegraphics[width=17.0cm]{' + os.path.join( \
                                                         outputDirectoryOutlierDetection,
                                                         'snapshot-outliers.jpg' ) + '}\n' )
        of.write( '\n' )
        of.write( '\end{tabular}\n' )
        of.write( '\n' )
        of.write( '\\newpage\n' )
        of.write( '\n' )
        of.write( '\n' )
        
      if (  len( directoryNameDataImportAndQSpaceSampling ) and \
            len( directoryNameSusceptibilityArtifactCorrection ) ):
        
        of.write( '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n' )
        of.write( '\section*{Susceptibility artifact correction}\n' )
        of.write( '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n' )
        of.write( '\n' )
        of.write( '\\begin{tabular}{cccc}\n' )
        of.write( '\n' )
        of.write( '  \includegraphics[width=3.8cm]{' + os.path.join( outputDirectorySusceptibilityArtifactCorrection,
                                                                     'snapshot-axial-t2-b0-magnitude.jpg' ) + '} &\n' )
        of.write( '  \includegraphics[width=3.8cm]{' + os.path.join( outputDirectorySusceptibilityArtifactCorrection,
                                                                     'snapshot-coronal-t2-b0-magnitude.jpg' ) + '} &\n' )
        of.write( '  \includegraphics[width=3.8cm]{' + os.path.join( outputDirectorySusceptibilityArtifactCorrection,
                                                                     'snapshot-axial-t2-b0-phase.jpg' ) + '} &\n' )
        of.write( '  \includegraphics[width=3.8cm]{' + os.path.join( outputDirectorySusceptibilityArtifactCorrection,
                                                                     'snapshot-coronal-t2-b0-phase.jpg' ) + '} \\\\\n' )
        of.write( '       &\n' )
        of.write( '  \includegraphics[width=3.8cm]{' + os.path.join( outputDirectorySusceptibilityArtifactCorrection,
                                                                     'snapshot-sagittal-t2-b0-magnitude.jpg' ) + '} & &\n' )
        of.write( '  \includegraphics[width=3.8cm]{' + os.path.join( outputDirectorySusceptibilityArtifactCorrection,
                                                                     'snapshot-sagittal-t2-b0-phase.jpg' ) + '}\n' )
        of.write( '\n' )
        of.write( '\end{tabular}\n' )
        of.write( '\n' )
        of.write( '\\noindent\n' )
        of.write( '\\begin{tabular}{cc}\n' )
        of.write( '\n' )
        of.write( '  \includegraphics[width=8.0cm]{' + os.path.join( outputDirectorySusceptibilityArtifactCorrection,
                                                                     'snapshot-sagittal-t1-rgb-with-susceptibility.jpg' ) + '} &\n' )
        of.write( '  \includegraphics[width=8.0cm]{' + os.path.join( outputDirectorySusceptibilityArtifactCorrection,
                                                                     'snapshot-sagittal-t1-rgb-without-susceptibility.jpg' ) + '}\n' )
        of.write( '\n' )
        of.write( '\end{tabular}\n' )
        of.write( '\n' )
        of.write( '\\noindent\n' )
        of.write( '\\begin{tabular}{c}\n' )
        of.write( '\n' )
        of.write( '  \includegraphics[width=8.5cm]{' + os.path.join( outputDirectorySusceptibilityArtifactCorrection,
                                                                      'snapshot-histogram-displacement-in-pixel.jpg' ) + '}\n' )
        of.write( '\n' )
        of.write( '\end{tabular}\n' )
        of.write( '\n' )
        of.write( '\\newpage\n' )
        of.write( '\n' )
        of.write( '\n' )
         
      if (  len( directoryNameEddyCurrentAndMotion ) and \
            len( directoryNameToAnatomyMatching ) ):

        of.write( '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n' )
        of.write( '\section*{Eddy current and motion correction}\n' )
        of.write( '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n' )
        of.write( '\n' )
        of.write( '\\begin{tabular}{cc}\n' )
        of.write( '\n' )
        of.write( '  \includegraphics[width=17.0cm]{' + os.path.join( \
                                                         outputDirectoryEddyCurrentAndMotionCorrection,
                                                         'snapshot-motion-profile.jpg' ) + '}\n' )
        of.write( '\n' )
        of.write( '\end{tabular}\n' )
        of.write( '\n' )
        of.write( '\\newpage\n' )
        of.write( '\n' )
        of.write( '\n' )

      if (  len( directoryNameEddyCurrentAndMotion ) and \
            len( directoryNameToAnatomyMatching ) ):

        of.write( '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n' )
        of.write( '\section*{Corrected apparent diffusion coefficient mapping}\n' )
        of.write( '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n' )
        of.write( '\n' )
        of.write( '\\begin{tabular}{cc}\n' )
        of.write( '\n' )
        of.write( '  \includegraphics[width=8.0cm]{' + os.path.join( outputDirectoryTensorFitting,
                                                                     'snapshot-axial-adc.jpg' ) + '} &\n' )
        of.write( '  \includegraphics[width=8.0cm]{' + os.path.join( outputDirectoryTensorFitting,
                                                                     'snapshot-coronal-adc.jpg' ) + '} \\\\\n' )
        of.write( '\n' )
        of.write( '  \includegraphics[width=8.0cm]{' + os.path.join( outputDirectoryTensorFitting,
                                                                     'snapshot-histogram-adc.jpg' ) + '} &\n' )
        of.write( '  \includegraphics[width=8.0cm]{' + os.path.join( outputDirectoryTensorFitting,
                                                                     'snapshot-sagittal-adc.jpg' ) + '}\n' )
        of.write( '\n' )
        of.write( '\end{tabular}\n' )
        of.write( '\n' )
        of.write( '\\newpage\n' )
        of.write( '\n' )
        of.write( '\n' )
        of.write( '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n' )
        of.write( '\section*{Corrected fractional anisotropy mapping}\n' )
        of.write( '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n' )
        of.write( '\n' )
        of.write( '\\begin{tabular}{cc}\n' )
        of.write( '\n' )
        of.write( '  \includegraphics[width=8.0cm]{' + os.path.join( outputDirectoryTensorFitting,
                                                                     'snapshot-axial-fa.jpg' ) + '} &\n' )
        of.write( '  \includegraphics[width=8.0cm]{' + os.path.join( outputDirectoryTensorFitting,
                                                                     'snapshot-coronal-fa.jpg' ) + '} \\\\\n' )
        of.write( '\n' )
        of.write( '  \includegraphics[width=8.0cm]{' + os.path.join( outputDirectoryTensorFitting,
                                                                     'snapshot-histogram-fa.jpg' ) + '} &\n' )
        of.write( '  \includegraphics[width=8.0cm]{' + os.path.join( outputDirectoryTensorFitting,
                                                                     'snapshot-sagittal-fa.jpg' ) + '}\n' )
        of.write( '\n' )
        of.write( '\end{tabular}\n' )
        of.write( '\n' )
        of.write( '\\newpage\n' )
        of.write( '\n' )
        of.write( '\n' )
        of.write( '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n' )
        of.write( '\section*{Corrected parallel diffusivity mapping}\n' )
        of.write( '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n' )
        of.write( '\n' )
        of.write( '\\begin{tabular}{cc}\n' )
        of.write( '\n' )
        of.write( '  \includegraphics[width=8.0cm]{' + os.path.join( outputDirectoryTensorFitting,
                                                                     'snapshot-axial-lambda-parallel.jpg' ) + '} &\n' )
        of.write( '  \includegraphics[width=8.0cm]{' + os.path.join( outputDirectoryTensorFitting,
                                                                     'snapshot-coronal-lambda-parallel.jpg' ) + '} \\\\\n' )
        of.write( '\n' )
        of.write( '  \includegraphics[width=8.0cm]{' + os.path.join( outputDirectoryTensorFitting,
                                                                     'snapshot-histogram-lambda-parallel.jpg' ) + '} &\n' )
        of.write( '  \includegraphics[width=8.0cm]{' + os.path.join( outputDirectoryTensorFitting,
                                                                     'snapshot-sagittal-lambda-parallel.jpg' ) + '}\n' )
        of.write( '\n' )
        of.write( '\end{tabular}\n' )
        of.write( '\n' )
        of.write( '\\newpage\n' )
        of.write( '\n' )
        of.write( '\n' )
        of.write( '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n' )
        of.write( '\section*{Corrected transverse diffusivity mapping}\n' )
        of.write( '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n' )
        of.write( '\n' )
        of.write( '\\begin{tabular}{cc}\n' )
        of.write( '\n' )
        of.write( '  \includegraphics[width=8.0cm]{' + os.path.join( outputDirectoryTensorFitting,
                                                                     'snapshot-axial-lambda-transverse.jpg' ) + '} &\n' )
        of.write( '  \includegraphics[width=8.0cm]{' + os.path.join( outputDirectoryTensorFitting,
                                                                     'snapshot-coronal-lambda-transverse.jpg' ) + '} \\\\\n' )
        of.write( '\n' )
        of.write( '  \includegraphics[width=8.0cm]{' + os.path.join( outputDirectoryTensorFitting,
                                                                     'snapshot-histogram-lambda-transverse.jpg' ) + '} &\n' )
        of.write( '  \includegraphics[width=8.0cm]{' + os.path.join( outputDirectoryTensorFitting,
                                                                     'snapshot-sagittal-lambda-transverse.jpg' ) + '}\n' )
        of.write( '\n' )
        of.write( '\end{tabular}\n' )
        of.write( '\n' )
        of.write( '\\newpage\n' )
        of.write( '\n' )
        of.write( '\n' )
        of.write( '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n' )
        of.write( '\section*{Corrected orientation distribution function mapping}\n' )
        of.write( '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n' )
        of.write( '\n' )
        of.write( '\\begin{tabular}{cc}\n' )
        of.write( '\n' )
        of.write( '  \includegraphics[width=8.0cm]{' + os.path.join( outputDirectoryTensorFitting,
                                                                     'snapshot-axial-rgb.jpg' ) + '} &\n' )
        of.write( '  \includegraphics[width=8.0cm]{' + os.path.join( outputDirectoryTensorFitting,
                                                                     'snapshot-coronal-rgb.jpg' ) + '} \\\\\n' )
        of.write( '\n' )
        of.write( '   &\n' )
        of.write( '  \includegraphics[width=8.0cm]{' + os.path.join( outputDirectoryTensorFitting,
                                                                     'snapshot-sagittal-rgb.jpg' ) + '}\n' )
        of.write( '\n' )
        of.write( '\end{tabular}\n' )
        of.write( '\n' )
        of.write( '\\noindent\n' )
        of.write( '\\begin{tabular}{cc}\n' )
        of.write( '\n' )
        of.write( '  \includegraphics[width=8.0cm]{' + os.path.join( outputDirectoryTensorFitting,
                                                                     'snapshot-axial-t1-rgb-odf.jpg' ) + '} &\n' )
        of.write( '  \includegraphics[width=8.0cm]{' + os.path.join( outputDirectoryTensorFitting,
                                                                     'snapshot-coronal-t1-rgb-odf.jpg' ) + '} \\\\\n' )
        of.write( '\n' )
        of.write( '   &\n' )
        of.write( '  \includegraphics[width=8.0cm]{' + os.path.join( outputDirectoryTensorFitting,
                                                                     'snapshot-sagittal-t1-rgb-odf.jpg' ) + '}\n' )
        of.write( '\n' )
        of.write( '\end{tabular}\n' )
        of.write( '\n' )
        of.write( '\\newpage\n' )
        of.write( '\n' )
        of.write( '\n' )
        of.write( '\end{document}\n' )

      of.close()
      
      command = 'pdflatex ' + \
                ' -output-directory ' + outputWorkDirectory + \
                ' ' + fileNameLateX
      executeCommand( self, subjectName, command, viewOnly )
      
      if ( os.path.exists( os.path.join( outputWorkDirectory, \
                                         'report.tex' ) ) ):

        os.remove( os.path.join( outputWorkDirectory, 'report.tex' ) )

      if ( os.path.exists( os.path.join( outputWorkDirectory, \
                                         'report.aux' ) ) ):

        os.remove( os.path.join( outputWorkDirectory, 'report.aux' ) )

      if ( os.path.exists( os.path.join( outputWorkDirectory, \
                                         'report.log' ) ) ):

        os.remove( os.path.join( outputWorkDirectory, 'report.log' ) )


      ####################### notifying display thread #########################
      functors[ 'condition-notify-and-release' ]( subjectName,
                                                  'quality-check-report-ready' )


      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 100 )


    except:

      self._functors[ 'task-set-status-as-failed' ]( subjectName,
                                                     sys.exc_info() )


  def snapOutliers( self,
                    functors,
                    subjectName,
                    fileNameAcquisitionParameters,
                    fileNameOutliers,
                    fileNameSnapshotOutliers,
                    viewOnly ):

    if ( not viewOnly ):
    
      globalVariables = dict()
      localVariables = dict()
      execfile( fileNameAcquisitionParameters, globalVariables, localVariables )
      sizeZ = int( localVariables[ 'acquisitionParameters' ][ 'sizeZ' ] )
      qSpacePointCount = int( \
             localVariables[ 'acquisitionParameters' ][ 'qSpacePointCount' ] )
 
      globalVariables = dict()
      localVariables = dict()
      execfile( fileNameOutliers, globalVariables, localVariables )
      orientations = localVariables[ 'orientations' ]
      slices = localVariables[ 'slices' ]

      functors[ 'snapshot-outliers' ]( subjectName,
                                       orientations, slices,
                                       qSpacePointCount, sizeZ,
                                       fileNameSnapshotOutliers )


  def snapHistogramOfVolume( self,
                             functors,
                             subjectName,
                             fileNameVolume,
                             fileNameMask,
                             binCount,
                             wantedLowerBoundary,
                             wantedUpperBoundary,
                             xLabel,
                             yLabel,
                             fileNameSnapshotHistogram,
                             viewOnly ):
                             
    pythonFile = fileNameSnapshotHistogram + '.py'
    if ( ( wantedLowerBoundary is not None ) and \
         ( wantedUpperBoundary is not None ) ):
         
      command = 'GkgExecuteCommand HistogramAnalyzer ' \
                ' -i ' + fileNameVolume + \
                ' -m ' + fileNameMask + \
                ' -n ' + \
                ' -o ' + pythonFile + \
                ' -matPlotLib ' + \
                ' -wantedLowerBoundary ' + str( wantedLowerBoundary ) + \
                ' -wantedUpperBoundary ' + str( wantedUpperBoundary ) + \
                ' -levelCount ' + str( binCount ) + \
                ' -verbose'
         
    else:
    
      command = 'GkgExecuteCommand HistogramAnalyzer ' \
                ' -i ' + fileNameVolume + \
                ' -m ' + fileNameMask + \
                ' -n ' + \
                ' -o ' + pythonFile + \
                ' -matPlotLib ' + \
                ' -levelCount ' + str( binCount ) + \
                ' -verbose'
    executeCommand( self, subjectName, command, viewOnly )

    globalVariables = dict()
    localVariables = dict()
    execfile( pythonFile, globalVariables, localVariables )
    xs = localVariables[ 'xs' ]
    hs = localVariables[ 'hs' ]
    
    functors[ 'snapshot-histogram' ]( subjectName,
                                      xs, hs,
                                      xLabel, yLabel,
                                      fileNameSnapshotHistogram )


  def snapMotionProfile( self,
                         functors,
                         subjectName,
                         fileNameMotionProfile,
                         fileNameSnapshotMotionProfile,
                         viewOnly ):
                             
    globalVariables = dict()
    localVariables = dict()
    execfile( fileNameMotionProfile, globalVariables, localVariables )
    translationX = localVariables[ 'translationX' ]
    translationY = localVariables[ 'translationY' ]
    translationZ = localVariables[ 'translationZ' ]

    rotationX = localVariables[ 'rotationX' ]
    rotationY = localVariables[ 'rotationY' ]
    rotationZ = localVariables[ 'rotationZ' ]

    scalingX = localVariables[ 'scalingX' ]
    scalingY = localVariables[ 'scalingY' ]
    scalingZ = localVariables[ 'scalingZ' ]

    shearingXY = localVariables[ 'shearingXY' ]
    shearingXZ = localVariables[ 'shearingXZ' ]
    shearingYZ = localVariables[ 'shearingYZ' ]

    
    functors[ 'snapshot-motion-profile' ]( subjectName,
                                           translationX,
                                           translationY,
                                           translationZ,
                                           rotationX,
                                           rotationY,
                                           rotationZ,
                                           scalingX,
                                           scalingY,
                                           scalingZ,
                                           shearingXY,
                                           shearingXZ,
                                           shearingYZ,
                                           fileNameSnapshotMotionProfile )


  ##############################################################################
  # display()
  ##############################################################################

  def display( self, functors, parameters, subjectName, verbose,
               isReportModeActive ):

    ########################### resetting the viewer ###########################
    functors[ 'viewer-reset' ]()

    ####################### collecting output work directory ###################
    outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()
    if ( verbose ):
    
      print  'Output work directory :', outputWorkDirectory


    ####################### waiting for result ################################
    functors[ 'condition-wait-and-release' ]( subjectName,
                                              'quality-check-report-ready' )

    ####################### displaying report #################################
    fileNamePdfReport = os.path.join( outputWorkDirectory, 'report.pdf' )
    functors[ 'viewer-add-pdf-to-pdfviewer' ]( 'first_view',
                                               'Quality check report',
                                               fileNamePdfReport )

