from Viewer import *


class DwiQualityCheckReportingViewer( Viewer ):

  def __init__( self, minimumSize, parent ):

    Viewer.__init__( self, minimumSize, parent )
    
    ########################## First View ######################################

    self.createView( 'first_view' )
    
    self.addPdfViewer( 'first_view', 0, 0, 1, 1,
                       'Quality check report', 'Quality check report' )


def createDwiQualityCheckReportingViewer( minimumSize, parent ):

  return DwiQualityCheckReportingViewer( minimumSize, parent )

