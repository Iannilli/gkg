from Algorithm import *
from DwiRoughMaskExtractionTask import *


class DwiRoughMaskExtractionAlgorithm( Algorithm ):

  def __init__( self, verbose ):

    Algorithm.__init__( self, 'DWI-Rough-Mask-Extraction', verbose,
                        True )

    # diffusion-weighted input data
    self.addParameter( StringParameter( 'rawDwiDirectory', '' ) )

    # rough mask from T2(b=0)
    self.addParameter( BooleanParameter( 'strategyRoughMaskFromT2', True ) ) 
    self.addParameter( DoubleParameter( 'noiseThresholdPercentage', \
                                        2, 1, 100, 0.5 ) )
    self.addParameter( DoubleParameter( 'maskClosingRadius', \
                                        0.0, 0.0, 100.0, 0.01 ) )
    self.addParameter( DoubleParameter( 'maskDilationRadius', \
                                        4.0, 0.0, 100.0, 0.01 ) )
                                        
    # morpho mask
    self.addParameter( BooleanParameter( 'strategyRoughMaskFromT1', False ) ) 
    self.addParameter( StringParameter( 'morphologistBrainMask', '' ) ) 
    self.addParameter( StringParameter( 'anatomy', '' ) )  
    self.addParameter( RegistrationParameter( \
                         'dwToT1RegistrationParameter' ,
                         { 'optimizerParametersTranslationX':
                             { 'defaultValue': 30 },
                           'optimizerParametersTranslationY':
                             { 'defaultValue': 30 },
                           'optimizerParametersTranslationZ':
                             { 'defaultValue': 30 },
                           'optimizerParametersRotationX':
                             { 'defaultValue': 5 },
                           'optimizerParametersRotationY':
                             { 'defaultValue': 5 },
                           'optimizerParametersRotationZ':
                             { 'defaultValue': 5 } } ) )

  def launch( self ):

    if ( self._verbose ):

      print \
        'running DWI rough mask extraction'

    task = DwiRoughMaskExtractionTask( self._application )
    task.launch( False )


  def view( self ):

    if ( self._verbose ):

      print \
        'viewing DWI rough mask extraction'

    task = DwiRoughMaskExtractionTask( self._application )
    task.launch( True )

