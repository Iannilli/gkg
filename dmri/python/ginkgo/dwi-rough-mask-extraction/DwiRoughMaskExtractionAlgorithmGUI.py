from AlgorithmGUI import *
from ResourceManager import *


class DwiRoughMaskExtractionAlgorithmGUI( AlgorithmGUI ):

  def __init__( self, algorithm ):

    AlgorithmGUI.__init__( self,
                           algorithm,
                           ResourceManager().getUIFileName(
                                         'dmri',
                                         'DwiRoughMaskExtraction.ui' ) )

    ############################################################################
    # connecting raw DWI directory interface
    ############################################################################

    # input DW data file name and push button
    self.connectStringParameterToLineEdit( 'rawDwiDirectory',
                                           'lineEdit_RawDwiDirectory' )    
    self.connectDirectoryBrowserToPushButtonAndLineEdit( \
                                                   'pushButton_RawDwiDirectory',
                                                   'lineEdit_RawDwiDirectory' )

    ############################################################################
    # connecting rough mask from T2(b=0) interface
    ############################################################################

    self.connectBooleanParameterToRadioButton( \
                                         'strategyRoughMaskFromT2',
                                         'radioButton_StrategyRoughMaskFromT2' )
    self.connectDoubleParameterToSpinBox( \
                                      'noiseThresholdPercentage',
                                      'doubleSpinBox_NoiseThresholdPercentage' )
    self.connectDoubleParameterToSpinBox( 'maskClosingRadius',
                                          'doubleSpinBox_MaskClosingRadius' )
    self.connectDoubleParameterToSpinBox( 'maskDilationRadius',
                                          'doubleSpinBox_MaskDilationRadius' )

    self._findChild( self._awin,
                     'radioButton_StrategyRoughMaskFromT2'
                    ).toggled.connect( self.changeMaskStrategy )       
                                          
    ############################################################################
    # connecting rough mask from T1 interface
    ############################################################################

    self.connectBooleanParameterToRadioButton( 
                                         'strategyRoughMaskFromT1',
                                         'radioButton_StrategyRoughMaskFromT1' )
 
    # input brain mask from Morphologist file name and push button
    self.connectStringParameterToLineEdit( 'morphologistBrainMask',
                                           'lineEdit_MorphologistBrainMask' )
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit( \
                                             'pushButton_MorphologistBrainMask',
                                             'lineEdit_MorphologistBrainMask' )

    self.connectStringParameterToLineEdit( 'anatomy',
                                           'lineEdit_Anatomy' )    
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit( \
                                                           'pushButton_Anatomy',
                                                           'lineEdit_Anatomy' )
    self._findChild( self._awin,
                     'lineEdit_Anatomy'
                    ).textChanged.connect(
                                     self.setDwToT1SubSamplingMaximumSizeValue )

    self._DwToT1RegistrationAdvancedParameterWidget = \
         self.connectPushButtonToAdvancedParameterWidget( \
         'pushButton_Settings',
         ResourceManager().getUIFileName( 'core','RegistrationSettings.ui' ),
         'dwToT1RegistrationParameter' )

    self.changeMaskStrategy( True )
    

  def changeMaskStrategy( self, value ):

    # rough mask from T2(b=0)
    doubleSpinBoxNoiseThresholdPercentage = self._findChild( \
                                     self._awin,
                                     'doubleSpinBox_NoiseThresholdPercentage' )
    doubleSpinBoxMaskClosingRadius = self._findChild( \
                                             self._awin,
                                             'doubleSpinBox_MaskClosingRadius' )
    doubleSpinBoxMaskDilationRadius = self._findChild( \
                                            self._awin,
                                            'doubleSpinBox_MaskDilationRadius' )
    labelNoiseThresholdPercentage = self._findChild( \
                                              self._awin,
                                              'label_NoiseThresholdPercentage' )
    labelMaskClosingRadius = self._findChild( self._awin,
                                              'label_MaskClosingRadius' )
    labelMaskDilationRadius = self._findChild( self._awin,
                                               'label_MaskDilationRadius' )    
    
    # rough mask from T1
    lineEditMorphologistBrainMask = self._findChild( self._awin,
                                     'lineEdit_MorphologistBrainMask' )
    pushButtonMorphologistBrainMask = self._findChild( \
                                            self._awin,
                                            'pushButton_MorphologistBrainMask' )
    lineEditAnatomy = self._findChild( self._awin,
                                       'lineEdit_Anatomy' )
    pushButtonAnatomy = self._findChild( self._awin,
                                         'pushButton_Anatomy' )
    pushButtonSettings = self._findChild( self._awin,
                                          'pushButton_Settings' )
    labelMorphologistBrainMask = self._findChild( \
                                                 self._awin,
                                                 'label_MorphologistBrainMask' )
    labelAnatomy = self._findChild( self._awin,
                                    'label_Anatomy' )
    labelSettings = self._findChild( self._awin,
                                     'label_Settings' )
                      

    if ( value ):

      doubleSpinBoxNoiseThresholdPercentage.setEnabled( True )
      doubleSpinBoxMaskClosingRadius.setEnabled( True )
      doubleSpinBoxMaskDilationRadius.setEnabled( True )
      labelNoiseThresholdPercentage.setEnabled( True )
      labelMaskClosingRadius.setEnabled( True )
      labelMaskDilationRadius.setEnabled( True )    
    
      lineEditMorphologistBrainMask.setEnabled( False )
      pushButtonMorphologistBrainMask.setEnabled( False )
      lineEditAnatomy.setEnabled( False )
      pushButtonAnatomy.setEnabled( False )
      pushButtonSettings.setEnabled( False )
      labelMorphologistBrainMask.setEnabled( False )
      labelAnatomy.setEnabled( False )
      labelSettings.setEnabled( False )
      
    else:
      
      doubleSpinBoxNoiseThresholdPercentage.setEnabled( False )
      doubleSpinBoxMaskClosingRadius.setEnabled( False )
      doubleSpinBoxMaskDilationRadius.setEnabled( False )
      labelNoiseThresholdPercentage.setEnabled( False )
      labelMaskClosingRadius.setEnabled( False )
      labelMaskDilationRadius.setEnabled( False )    
    
      lineEditMorphologistBrainMask.setEnabled( True )
      pushButtonMorphologistBrainMask.setEnabled( True )
      lineEditAnatomy.setEnabled( True )
      pushButtonAnatomy.setEnabled( True )
      pushButtonSettings.setEnabled( True )
      labelMorphologistBrainMask.setEnabled( True )
      labelAnatomy.setEnabled( True )
      labelSettings.setEnabled( True )

      
  def setDwToT1SubSamplingMaximumSizeValue( self, fileName ):

    if ( os.path.exists( fileName ) ):

      print 'Check sub-sampling maximum sizes during optimization value...'
      command = 'GkgVolumeInformation' + \
                ' -i ' + fileName + \
                ' -info sizeX ' + \
                ' -verbosePluginLoading false'
      fd = os.popen( str( command ) )
      maxValue = fd.read()[ : -1 ]
      ret = fd.close()

      command = 'GkgVolumeInformation' + \
                ' -i ' + fileName + \
                ' -info sizeY ' + \
                ' -verbosePluginLoading false'
      fd = os.popen( str( command ) )
      value = fd.read()[ : -1 ]
      ret = fd.close()

      if( value > maxValue ):

        value = maxValue
      
      command = 'GkgVolumeInformation' + \
                ' -i ' + fileName + \
                ' -info sizeZ ' + \
                ' -verbosePluginLoading false'
      fd = os.popen( str( command ) )
      value = fd.read()[ : -1 ]
      ret = fd.close()

      if( value > maxValue ):

        value = maxValue

      parameter = self._algorithm.getParameter( 'dwToT1RegistrationParameter' )
      parameterValue = parameter.getValue()
      registrationValue = parameterValue[ 'subSamplingMaximumSizes' ]
      subSamplingMaximumSizes = str( int( maxValue ) / 2 ) + ' ' + str( maxValue )
      if ( registrationValue != subSamplingMaximumSizes ):

        message = 'sub-sampling maximum sizes during optimization \n' + \
                  'should be maximum size value of T1 file, here ' + \
                  subSamplingMaximumSizes + '.\n' + \
                  'Set this parameter as ' + subSamplingMaximumSizes + ' ?'
        answer = QtGui.QMessageBox.question( None,
                                             'Abort',
                                             message,
                                             QtGui.QMessageBox.Yes | \
                                             QtGui.QMessageBox.No )

        if answer == QtGui.QMessageBox.Yes:

          parameterValue[ 'subSamplingMaximumSizes' ] = subSamplingMaximumSizes
          parameter.setValue( parameterValue )

      else:

        print 'No change in ' + \
              'sub-sampling maximum sizes during optimization value.'
