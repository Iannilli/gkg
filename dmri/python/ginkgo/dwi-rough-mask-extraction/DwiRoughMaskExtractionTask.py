from Task import *
import shutil

class DwiRoughMaskExtractionTask( Task ):

  def __init__( self, application ):

    Task.__init__( self, application )

    self.addFunctor( 'viewer-set-dw-information',
                     self.viewerSetDwInformation )


  def viewerSetDwInformation( self,
                              sliceAxis,
                              sizeX, sizeY, sizeZ,
                              resolutionX, resolutionY, resolutionZ ):

    if ( self._application.getBatchMode() == False ):

      self._application.getMainThreadActionManager().call(
        self._application.getViewer().setDwInformation,
        sliceAxis, sizeX, sizeY, sizeZ, resolutionX, resolutionY, resolutionZ )

  ##############################################################################
  # execute()
  ##############################################################################

  def execute( self, functors, parameters, subjectName, verbose, viewOnly ):

    try:

      ####################### acquiring conditions #############################
      functors[ 'condition-acquire' ]( subjectName, 'mask-processed' )

      if ( parameters[ 'strategyRoughMaskFromT1' ].getValue() ):

        functors[ 'condition-acquire' ]( subjectName,
                                         't1-to-dw-matching-processed' )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 0 )

      ####################### collecting output work directory #################
      outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()
      if ( verbose ):
      
        print  'Output work directory :', outputWorkDirectory

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 5 )


      ########################### mask using morphologist ######################
      if ( parameters[ 'strategyRoughMaskFromT1' ].getValue() ):
        
        fileNameT1ToDwTransform3d = os.path.join( outputWorkDirectory,
                                                  't1_to_dw.trm' )
        fileNameDwToT1Transform3d = os.path.join( outputWorkDirectory,
                                                  'dw_to_t1.trm' )
        dwToT1RegistrationParameters = parameters[ \
                                      'dwToT1RegistrationParameter' ].getValue()

        fileNameAnatomy = parameters[ 'anatomy' ].getValue()
        dwiDirectory = parameters[ 'rawDwiDirectory' ].getValue()
        fileNameAverageT2 = os.path.join( dwiDirectory, 't2.ima' )
        
        similarityMeasureName = str( parameters[ \
          'dwToT1RegistrationParameter' ].getChoice( 'similarityMeasureName' ) )
        optimizerName = str( parameters[ \
          'dwToT1RegistrationParameter' ].getChoice( 'optimizerName' ) )
        transform3DType = str( parameters[ \
                'dwToT1RegistrationParameter' ].getChoice( 'transform3DType' ) )
        referenceLowerThreshold = str( dwToT1RegistrationParameters[ \
                                                   'referenceLowerThreshold' ] )
        floatingLowerThreshold = str( dwToT1RegistrationParameters[ \
                                                    'floatingLowerThreshold' ] )
        resamplingOrder = str( dwToT1RegistrationParameters[ \
                                                           'resamplingOrder' ] )
        subSamplingMaximumSizes = str( dwToT1RegistrationParameters[ \
                                                   'subSamplingMaximumSizes' ] )
        maximumIterationCount = str( dwToT1RegistrationParameters[ \
                                                     'maximumIterationCount' ] )
        levelCount = dwToT1RegistrationParameters[ 'levelCount' ] 
        applySmoothing = dwToT1RegistrationParameters[ 'applySmoothing' ]
        initializeCoefficientsUsingCenterOfGravity = \
                                  dwToT1RegistrationParameters[ \
                                  'initializeCoefficientsUsingCenterOfGravity' ]
        initialParametersScalingX = dwToT1RegistrationParameters[ \
                                  'initialParametersScalingX' ]
        initialParametersScalingY = dwToT1RegistrationParameters[ \
                                  'initialParametersScalingY' ]
        initialParametersScalingZ = dwToT1RegistrationParameters[ \
                                  'initialParametersScalingZ' ]
        initialParametersShearingXY = dwToT1RegistrationParameters[ \
                                  'initialParametersShearingXY' ]
        initialParametersShearingXZ = dwToT1RegistrationParameters[ \
                                  'initialParametersShearingXZ' ]
        initialParametersShearingYZ = dwToT1RegistrationParameters[ \
                                  'initialParametersShearingYZ' ]
        initialParametersRotationX = dwToT1RegistrationParameters[ \
                                  'initialParametersRotationX' ]
        initialParametersRotationY = dwToT1RegistrationParameters[ \
                                  'initialParametersRotationY' ]
        initialParametersRotationZ = dwToT1RegistrationParameters[ \
                                  'initialParametersRotationZ' ]
        initialParametersTranslationX = dwToT1RegistrationParameters[ \
                                  'initialParametersTranslationX' ]
        initialParametersTranslationY = dwToT1RegistrationParameters[ \
                                  'initialParametersTranslationY' ]
        initialParametersTranslationZ = dwToT1RegistrationParameters[ \
                                  'initialParametersTranslationZ' ]
        stoppingCriterionError = dwToT1RegistrationParameters[ \
                                                      'stoppingCriterionError' ]
        stepSize = dwToT1RegistrationParameters[ 'stepSize' ]
        maximumTestGradient = dwToT1RegistrationParameters[ \
                                                      'maximumTestGradient' ]
        maximumTolerance = dwToT1RegistrationParameters[ 'maximumTolerance' ]
        optimizerParametersScalingX = dwToT1RegistrationParameters[ \
                                  'optimizerParametersScalingX' ]
        optimizerParametersScalingY = dwToT1RegistrationParameters[ \
                                  'optimizerParametersScalingY' ]
        optimizerParametersScalingZ = dwToT1RegistrationParameters[ \
                                  'optimizerParametersScalingZ' ]
        optimizerParametersShearingXY = dwToT1RegistrationParameters[ \
                                  'optimizerParametersShearingXY' ]
        optimizerParametersShearingXZ = dwToT1RegistrationParameters[ \
                                  'optimizerParametersShearingXZ' ]
        optimizerParametersShearingYZ = dwToT1RegistrationParameters[ \
                                  'optimizerParametersShearingYZ' ]
        optimizerParametersRotationX = dwToT1RegistrationParameters[ \
                                  'optimizerParametersRotationX' ]
        optimizerParametersRotationY = dwToT1RegistrationParameters[ \
                                  'optimizerParametersRotationY' ]
        optimizerParametersRotationZ = dwToT1RegistrationParameters[ \
                                  'optimizerParametersRotationZ' ]
        optimizerParametersTranslationX = dwToT1RegistrationParameters[ \
                                  'optimizerParametersTranslationX' ]
        optimizerParametersTranslationY = dwToT1RegistrationParameters[ \
                                  'optimizerParametersTranslationY' ]
        optimizerParametersTranslationZ = dwToT1RegistrationParameters[ \
                                  'optimizerParametersTranslationZ' ]

        command = 'GkgExecuteCommand Registration3d ' + \
                  ' -reference ' + fileNameAnatomy + \
                  ' -floating ' + fileNameAverageT2 + \
                  ' -o ' + fileNameDwToT1Transform3d + \
                  ' -oi ' + fileNameT1ToDwTransform3d  + \
                  ' -similarityMeasureName ' + similarityMeasureName + \
                  ' -optimizerName ' + optimizerName + \
                  ' -referenceLowerThreshold ' + referenceLowerThreshold + \
                  ' -floatingLowerThreshold ' + floatingLowerThreshold + \
                  ' -resamplingOrder ' + resamplingOrder + \
                  ' -subSamplingMaximumSizes ' + subSamplingMaximumSizes + \
                  ' -t ' + transform3DType

        if ( similarityMeasureName != 'correlation-coefficient' ):

          command += ' -similarityMeasureParameters ' + str( levelCount ) + \
                                             ' ' + str( int( applySmoothing ) )

        if not initializeCoefficientsUsingCenterOfGravity:

          command += ' -initialParameters '
                                         
          if ( transform3DType != 'rigid' ):

            command += str( initialParametersScalingX ) + ' ' + \
                       str( initialParametersScalingY ) + ' ' + \
                       str( initialParametersScalingZ ) + ' '
                       

          if ( transform3DType == 'affine' ):

            command += str( initialParametersShearingXY ) + ' ' + \
                       str( initialParametersShearingXZ ) + ' ' + \
                       str( initialParametersShearingYZ ) + ' '
 
          command += str( initialParametersRotationX ) + ' ' + \
                     str( initialParametersRotationY ) + ' ' + \
                     str( initialParametersRotationZ ) + ' ' + \
                     str( initialParametersTranslationX ) + ' ' + \
                     str( initialParametersTranslationY ) + ' ' + \
                     str( initialParametersTranslationZ ) + ' '

          command += ' -initializedUsingCentreOfGravity false'

        else:
        
          command += ' -initializedUsingCentreOfGravity true'
 
        command += ' -optimizerParameters ' + str( maximumIterationCount ) + ' '

        if ( optimizerName == 'nelder-mead' ):
                                    
          command += str( stoppingCriterionError ) + ' '
          if ( transform3DType != 'rigid' ):

            command += str( optimizerParametersScalingX ) + ' ' + \
                       str( optimizerParametersScalingY ) + ' ' + \
                       str( optimizerParametersScalingZ ) + ' '
                       
          if ( transform3DType == 'affine' ):

            command += str( optimizerParametersShearingXY ) + ' ' + \
                       str( optimizerParametersShearingXZ ) + ' ' + \
                       str( optimizerParametersShearingYZ ) + ' '
 
          command += str( optimizerParametersRotationX ) + ' ' + \
                     str( optimizerParametersRotationY ) + ' ' + \
                     str( optimizerParametersRotationZ ) + ' ' + \
                     str( optimizerParametersTranslationX ) + ' ' + \
                     str( optimizerParametersTranslationY ) + ' ' + \
                     str( optimizerParametersTranslationZ ) + ' '
 
        else:
                                    
          command += str( stepSize ) + ' ' + \
                     str( maximumTestGradient ) + ' ' + \
                     str( maximumTolerance ) 
 
        command += ' -verbose true'
        executeCommand( self, subjectName, command, viewOnly )
        
        ####################### notifying display thread #########################
        functors[ 'condition-notify-and-release' ]( \
                                                 subjectName,
                                                 't1-to-dw-matching-processed' )

        morphologistBrainMask = parameters[ 'morphologistBrainMask' \
                                                                    ].getValue()
        dilatedMorphologistBrainMask = os.path.join( \
                                         outputWorkDirectory,
                                         'dilated_morphologist_brain_mask.ima' )

        # processing volume resolution(s)
        command = 'GkgExecuteCommand VolumeInformation' + \
                  ' -i ' + morphologistBrainMask + \
                  ' -info resolutionX ' + \
                  '-verbosePluginLoading false'
        fd = os.popen( command )
        stringResolutionX = fd.read()
        ret = fd.close()
        resolutionX = float( stringResolutionX[ : -1 ] )

        command = 'GkgExecuteCommand VolumeInformation' + \
                  ' -i ' + morphologistBrainMask + \
                  ' -info resolutionY ' + \
                  '-verbosePluginLoading false'
        fd = os.popen( command )
        stringResolutionY1 = fd.read()
        ret = fd.close()
        resolutionY = float( stringResolutionY1[ : -1 ] )

        command = 'GkgExecuteCommand VolumeInformation' + \
                  ' -i ' + morphologistBrainMask + \
                  ' -info resolutionZ ' + \
                  '-verbosePluginLoading false'
        fd = os.popen( command )
        stringResolutionZ = fd.read()
        ret = fd.close()
        resolutionZ = float( stringResolutionZ[ : -1 ] )

        minimumResolution = resolutionX
        if ( resolutionY < minimumResolution ):
        
          minimumResolution = resolutionY

        if ( resolutionZ < minimumResolution ):
        
          minimumResolution = resolutionZ

        dilationRadius = 4.0 * minimumResolution

        command = 'GkgExecuteCommand MorphologicalOperation ' + \
                  ' -i ' + morphologistBrainMask + \
                  ' -o ' + dilatedMorphologistBrainMask + \
                  ' -op dilation ' + \
                  ' -r ' + str( dilationRadius ) + \
                  ' -m gt -t1 0 ' + \
                  ' -verbose true'                  
        executeCommand( self, subjectName, command, viewOnly )
        
        fileNameMask = os.path.join( outputWorkDirectory, 'mask' )
        command = 'GkgExecuteCommand Resampling3d ' + \
                  ' -reference ' + dilatedMorphologistBrainMask + \
                  ' -template ' + fileNameAverageT2 + \
                  ' -transforms ' + fileNameT1ToDwTransform3d + \
                  ' -output ' + fileNameMask + \
                  ' -order 0 ' + \
                  ' -verbose true'                  
        executeCommand( self, subjectName, command, viewOnly )        
                  


      ####################### creating mask from average T2 ####################
      else :
        
        rawDwiDirectory = parameters[ 'rawDwiDirectory' ].getValue()
        fileNameAverageT2 = os.path.join( rawDwiDirectory, 't2' )
        fileNameDW = os.path.join( rawDwiDirectory, 'dw' )
        noiseThresholdPercentage = \
                             parameters[ 'noiseThresholdPercentage' ].getValue()
        maskClosingRadius = parameters[ 'maskClosingRadius' ].getValue()
        maskDilationRadius = parameters[ 'maskDilationRadius' ].getValue()
        fileNameMask = self.createMask( functors,
                                        fileNameAverageT2,
                                        noiseThresholdPercentage,
                                        maskClosingRadius,
                                        maskDilationRadius,
                                        outputWorkDirectory,
                                        subjectName,
                                        verbose,
                                        viewOnly )
                                        
      ########################## creating raw RGB map ##########################
      self.createRGB( fileNameAverageT2,
                      fileNameDW,
                      fileNameMask,
                      outputWorkDirectory,
                      subjectName,
                      verbose,
                      viewOnly )      
      

      ####################### notifying display thread #########################
      functors[ 'condition-notify-and-release' ]( subjectName,
                                                  'mask-processed' )


      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 100 )

    except:

      self._functors[ 'task-set-status-as-failed' ]( subjectName,
                                                     sys.exc_info() )

  ##############################################################################
  # creating mask
  ##############################################################################

  def createMask( self,
                  functors,
                  fileNameAverageT2, 
                  noiseThresholdPercentage,
                  maskClosingRadius,
                  maskDilationRadius,
                  outputWorkDirectory, 
                  subjectName,
                  verbose,
                  viewOnly ):

    furtherOption = ''
    if ( maskClosingRadius > 0.0 ):
    
      furtherOption = ' -r ' + str( maskClosingRadius )

    # building mask
    fileNameMask = os.path.join( outputWorkDirectory, 'mask' )
    command = 'GkgExecuteCommand GetMask' + \
              ' -i ' + \
              fileNameAverageT2 + \
              ' -o ' + \
              fileNameMask + \
              ' -a 2 ' + \
              ' -thresholdRatio ' + str( noiseThresholdPercentage / 100.0 ) + \
              furtherOption + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )

    command = 'GkgExecuteCommand MorphologicalOperation' + \
              ' -i ' + \
              fileNameMask + \
              ' -o ' + \
              fileNameMask + \
              ' -op dilation ' + \
              ' -r ' + str( maskDilationRadius ) + \
              ' -m gt -t1 0' + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )
    
    return fileNameMask


  ##############################################################################
  # creating RGB map
  ##############################################################################

  def createRGB( self,
                 fileNameAverageT2,
                 fileNameDW,
                 fileNameMask,
                 outputWorkDirectory,
                 subjectName,
                 verbose,
                 viewOnly ):

    # building RGB map
    fileNameRGB = os.path.join( outputWorkDirectory, 'dti_rgb' )
    command = 'GkgExecuteCommand DwiTensorField' + \
              ' -t2 ' + fileNameAverageT2 + \
              ' -dw ' + fileNameDW + \
              ' -m ' + fileNameMask + \
              ' -f rgb ' + \
              ' -o ' + fileNameRGB + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )


  ##############################################################################
  # display()
  ##############################################################################

  def display( self, functors, parameters, subjectName, verbose,
               isReportModeActive ):

    ########################### resetting the viewer ###########################
    functors[ 'viewer-reset' ]()

    ####################### collecting output work directory ###################
    outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()
    rawDwiDirectory = parameters[ 'rawDwiDirectory' ].getValue()
    fileNameAverageT2 = os.path.join( rawDwiDirectory, 't2' )
    if ( verbose ):
    
      print  'Output work directory :', outputWorkDirectory

    if ( verbose ):

      print 'reading \'' + fileNameAverageT2 + '\''


    ####################### creating mask from average T2 ######################
    if ( parameters[ 'strategyRoughMaskFromT2' ].getValue() ):
    
      viewType = 'first_view'
      functors[ 'viewer-set-view' ]( viewType )  

      functors[ 'viewer-create-referential' ]( 'frameDW' )

      functors[ 'viewer-load-object' ]( fileNameAverageT2, 'averageT2' )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'averageT2' )

      functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                         'first_view',
                                                         'Average T2(b=0)' )
      functors[ 'viewer-add-object-to-window' ]( 'averageT2',
                                                 'first_view',
                                                 'Average T2(b=0)' )
                                                 
      noiseThresholdPercentage = \
                               parameters[ 'noiseThresholdPercentage' ].getValue()
      maskClosingRadius = parameters[ 'maskClosingRadius' ].getValue()
      maskDilationRadius = parameters[ 'maskDilationRadius' ].getValue()
      fileNameMask = self.createMask( functors,
                                      fileNameAverageT2,
                                      noiseThresholdPercentage,
                                      maskClosingRadius,
                                      maskDilationRadius,
                                      outputWorkDirectory,
                                      subjectName,
                                      verbose,
                                      True )

      functors[ 'condition-wait-and-release' ]( subjectName, 'mask-processed' )

      if ( verbose ):

        print 'reading \'' + fileNameMask + '\''
      functors[ 'viewer-load-object' ]( fileNameMask, 'mask' )
      functors[ 'viewer-set-colormap' ]( 'mask', 'BLUE-ufusion' )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'mask' )

      functors[ 'viewer-fusion-objects' ]( [ 'averageT2', 'mask' ],
                                             'fusionAverageT2AndMask',
                                             'Fusion2DMethod' )


      functors[ 'viewer-assign-referential-to-object' ]( \
                                                        'frameDW',
                                                        'fusionAverageT2AndMask' )
      functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                         'first_view',
                                                         'Average T2(b=0)+mask' )
      functors[ 'viewer-add-object-to-window' ]( 'fusionAverageT2AndMask',
                                                 'first_view',
                                                 'Average T2(b=0)+mask' )

    ####################### creating mask from morphologist  ###################
    else : 
    
      viewType = 'second_view'
      functors[ 'viewer-set-view' ]( viewType )        
      
      functors[ 'viewer-create-referential' ]( 'frameDW' )

      functors[ 'viewer-load-object' ]( fileNameAverageT2, 'AverageT2' )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW',
                                                         'AverageT2' )

      functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                         'second_view',
                                                         'Average T2(b=0)' )
      functors[ 'viewer-add-object-to-window' ]( 'AverageT2',
                                                 'second_view',
                                                 'Average T2(b=0)' )
      
      ####################### collecting output work directory ###################
      rawDwiDirectory = parameters[ 'rawDwiDirectory' ].getValue()
      outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()

      fileNameAnatomy = parameters[ 'anatomy' ].getValue()
      fileNameAverageT2 = os.path.join( rawDwiDirectory,
                                        't2.ima' )
      fileNameDwToT1Transform3d = os.path.join( outputWorkDirectory,
                                                'dw_to_t1.trm' )

      ############################# reading T2(b=0) ##############################
      #functors[ 'viewer-create-referential' ]( 'frameDW' )
      #functors[ 'viewer-load-object' ]( fileNameAverageT2, 'AverageT2' )
      #functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'AverageT2' )
      functors[ 'viewer-set-colormap' ]( 'AverageT2', 'RED TEMPERATURE' )

      ####################### reading and displaying T1 ##########################
      functors[ 'viewer-create-referential' ]( 'frameT1' )
      functors[ 'viewer-load-object' ]( fileNameAnatomy, 'T1' )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameT1', 'T1' )

      functors[ 'condition-wait-and-release' ]( subjectName,
                                                't1-to-dw-matching-processed' )
                                                
      functors[ 'viewer-load-transformation' ]( fileNameDwToT1Transform3d,
                                                'frameDW', 'frameT1' )

      functors[ 'viewer-fusion-objects' ]( [ 'T1', 'AverageT2' ],
                                           'fusionT1AndAverageT2',
                                           'Fusion2DMethod' )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameT1',
                                                         'fusionT1AndAverageT2' )
      functors[ 'viewer-assign-referential-to-window' ]( 'frameT1',    
                                                         'second_view',
                                                         'T1 + Average T2(b=0)' )
      functors[ 'viewer-add-object-to-window' ]( 'fusionT1AndAverageT2',
                                                 'second_view',
                                                 'T1 + Average T2(b=0)' )     
                                                 
      ####################### waiting for result ################################
      functors[ 'condition-wait-and-release' ]( subjectName, 'mask-processed' )
      
      ########################### display mask ###############################
      
      fileNameMask = os.path.join( outputWorkDirectory,
                                   'mask.ima' )
      functors[ 'viewer-load-object' ]( fileNameMask, 'mask' )
      functors[ 'viewer-set-colormap' ]( 'mask', 'BLUE-ufusion' )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'mask' )

      functors[ 'viewer-fusion-objects' ]( [ 'AverageT2', 'mask' ],
                                             'fusionAverageT2AndMask',
                                             'Fusion2DMethod' )


      functors[ 'viewer-assign-referential-to-object' ]( \
                                                        'frameDW',
                                                        'fusionAverageT2AndMask' )
      functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                         'second_view',
                                                         'Average T2(b=0)+mask' )
      functors[ 'viewer-add-object-to-window' ]( 'fusionAverageT2AndMask',
                                                 'second_view',
                                                 'Average T2(b=0)+mask' )

