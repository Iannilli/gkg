from AlgorithmGUI import *
from ResourceManager import *


class DwiShortWhiteMatterBundleLabelingAlgorithmGUI( AlgorithmGUI ):

  def __init__( self, algorithm ):

    AlgorithmGUI.__init__( self,
                           algorithm,
                           ResourceManager().getUIFileName(
                                       'dmri',
                                       'DwiShortWhiteMatterBundleLabeling.ui' ) )

    ###########################################################################
    # Connecting input data
    ###########################################################################

    # Input bundle directory
    self.connectStringParameterToLineEdit( 'fileNameInputBundleMaps',
                                           'lineEdit_FileNameInputBundleMaps' )
    self.connectFileListBrowserToPushButtonAndLineEdit( 
                                           'pushButton_FileNameInputBundleMaps',
                                           'lineEdit_FileNameInputBundleMaps' )

    # bundle map to atlas transformation
    self.connectStringParameterToLineEdit( 
                           'fileNameBundleMapsToAtlasTransformation',
                           'lineEdit_FileNameBundleMapsToAtlasTransformation' )
    self.connectFileListBrowserToPushButtonAndLineEdit(
                           'pushButton_FileNameBundleMapsToAtlasTransformation',
                           'lineEdit_FileNameBundleMapsToAtlasTransformation' )


    ###########################################################################
    # Connecting labeling parameters
    ###########################################################################

    self.connectIntegerParameterToSpinBox( 'fiberResamplingPointCount',
                                           'spinBox_FiberResamplingPointCount' )
    self.connectDoubleParameterToSpinBox( \
                                 'densityMaskFiberResamplingStep',
                                 'doubleSpinBox_DensityMaskFiberResamplingStep' )
    self.connectDoubleParameterToSpinBox( \
                                 'densityMaskResolution',
                                 'doubleSpinBox_DensityMaskResolution' )


    ############################################################################
    # Output options
    ############################################################################

    # remove temporary files
    self.connectBooleanParameterToCheckBox( 'intermediate',
                                            'checkBox_Intermediate' )
    self.connectBooleanParameterToCheckBox( 'single',
                                            'checkBox_Single' )
    self.connectBooleanParameterToCheckBox( 'computeDensityMasks',
                                            'checkBox_ComputeDensityMasks' )


