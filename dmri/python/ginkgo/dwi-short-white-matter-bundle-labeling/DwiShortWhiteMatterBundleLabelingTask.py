from Task import *
import os


class DwiShortWhiteMatterBundleLabelingTask( Task ):

  def __init__( self, application ):

    Task.__init__( self, application )

  ##############################################################################
  # execute()
  ##############################################################################

  def execute( self, functors, parameters, subjectName, verbose, viewOnly ):

    try:

      ####################### acquiring conditions #############################
      functors[ 'condition-acquire' ]( subjectName, 'labeling-processed' )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 0 )


      ####################### collecting output work directory #################
      outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()
      if ( verbose ):

        print  'Output work directory :', outputWorkDirectory

      ####################### collecting input data filename(s) ################
      fileNameInputBundleMaps = parameters[ 'fileNameInputBundleMaps'
                                                                    ].getValue()
      fileNameInputBundleMaps = fileNameInputBundleMaps.replace( ';', ' ' )
      fileNameBundleMapsToAtlasTransformation = parameters[ 
                          'fileNameBundleMapsToAtlasTransformation' ].getValue()
      fiberResamplingPointCount = parameters[ 'fiberResamplingPointCount'
                                                                    ].getValue()
      densityMaskFiberResamplingStep = parameters[ \
                                   'densityMaskFiberResamplingStep' ].getValue()
      densityMaskResolution = parameters[ \
                                            'densityMaskResolution' ].getValue()
      intermediate = parameters[ 'intermediate' ].getValue()
      single = parameters[ 'single' ].getValue()
      computeDensityMasks = parameters[ 'computeDensityMasks' ].getValue()

      specy = ResourceManager().getCurrentSpecy( 'dmri' )

      fileNameShortWhiteMatterBundleAtlas = \
                          os.path.join( ResourceManager().getDataDirectory(),
                                        'dmri',
                                        'dwi-short-white-matter-bundle-labeling',
                                        specy,
                                        'atlas.bundles'  )
      fileNameMaximumDistanceToBundleMapConfiguration = \
                          os.path.join( ResourceManager().getDataDirectory(),
                                        'dmri',
                                        'dwi-short-white-matter-bundle-labeling',
                                        specy,
                                        'maximum-distance-to-bundle-map.py'  )

      command = 'GkgExecuteCommand DwiFiberLabelling ' + \
                ' -i ' + fileNameInputBundleMaps + \
                ' -a ' + fileNameShortWhiteMatterBundleAtlas + \
                ' -t ' + fileNameBundleMapsToAtlasTransformation + \
                ' -d ' + fileNameMaximumDistanceToBundleMapConfiguration + \
                ' -fiberResamplingPointCount ' + \
                str( fiberResamplingPointCount ) + \
                ' -densityMaskFiberResamplingStep ' + \
                str( densityMaskFiberResamplingStep ) + \
                ' -densityMaskResolution ' + \
                str( densityMaskResolution ) + ' ' + \
                str( densityMaskResolution ) + ' ' + \
                str( densityMaskResolution ) + \
                ' -o ' + outputWorkDirectory + \
                ' -verbose true'
                
      if ( intermediate ):
      
        command += ' -intermediate true '

      else:
      
        command += ' -intermediate false '

      if ( single ):
      
        command += ' -single true '
        
      else:

        command += ' -single false '

      if ( computeDensityMasks ):
      
        command += ' -computeDensityMasks true '
        
      else:

        command += ' -computeDensityMasks false '

      executeCommand( self, subjectName, command, viewOnly )

 
      ###################### notifying display thread ##########################
      functors[ 'condition-notify-and-release' ]( subjectName,
                                                 'labeling-processed' )

      ########################## updating progress bar #########################
      functors[ 'update-progress-bar' ]( subjectName, 100 )

    except:

      self._functors[ 'task-set-status-as-failed' ]( subjectName,
                                                     sys.exc_info() )


  ##############################################################################
  # display()
  ##############################################################################

  def display( self, functors, parameters, subjectName, verbose,
               isReportModeActive ):

    ########################### resetting the viewer ###########################
    functors[ 'viewer-reset' ]()

    # display selected view
    functors[ 'viewer-set-view' ]( 'first_view' )


    ########################### collecting file names ##########################
    outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()

    # collecting the current specy
    specy = ResourceManager().getCurrentSpecy( 'dmri' )


    ####################### loading hierarchy file #############################
    hierarchyFileName = os.path.join( ResourceManager().getDataDirectory(),
                                      'dmri',
                                      'dwi-short-white-matter-bundle-labeling',
                                      specy,
                                      'atlas.hie'  )

    functors[ 'viewer-load-object' ]( hierarchyFileName ,
                                      'hierarchy' )

    functors[ 'viewer-add-object-to-window' ]( 'hierarchy',
                                               'first_view',
                                               'browser' )

    functors[ 'viewer-create-referential' ]( 'frameDW' )
    functors[ 'viewer-create-referential' ]( 'frameAtlas' )


    #################### loading short white matter bundle atlas ################
    fileNameShortWhiteMatterBundleAtlas = \
                          os.path.join( ResourceManager().getDataDirectory(),
                                        'dmri',
                                        'dwi-short-white-matter-bundle-labeling',
                                        specy,
                                        'atlas.bundles'  )

    functors[ 'viewer-load-object' ]( fileNameShortWhiteMatterBundleAtlas,
                                      'short-white-matter-bundle-atlas' )
    functors[ 'viewer-set-diffuse' ]( 'short-white-matter-bundle-atlas',
                                      [ 1.0, 1.0, 1.0, 0.5 ] )
    functors[ 'viewer-assign-referential-to-object' ]( \
                                              'frameAtlas',
                                              'short-white-matter-bundle-atlas' )
    functors[ 'viewer-assign-referential-to-window' ]( 'frameAtlas',
                                                       'first_view',
                                                       'atlas_window' )
    functors[ 'viewer-add-object-to-window' ]( \
                                              'short-white-matter-bundle-atlas',
                                              'first_view',
                                              'atlas_window',
                                              addWholeGraphObject = True )


    ##################### loading DW to atlas transformation ###################
    fileNameBundleMapsToAtlasTransformation = parameters[ 
                         'fileNameBundleMapsToAtlasTransformation' ].getValue()
    if ( os.path.exists( fileNameBundleMapsToAtlasTransformation ) ):

      functors[ 'viewer-load-transformation' ]( \
                                        fileNameBundleMapsToAtlasTransformation,
                                        'frameDW', 'frameAtlas' )


    ####################### waiting for result ################################
    functors[ 'condition-wait-and-release' ]( subjectName,
                                              'labeling-processed' )


    #################### loading short white matter segmented bundles ##########

    single = parameters[ 'single' ].getValue()

    if ( single == 2 ):
    
      fileNameShortWhiteMatterBundles = os.path.join( outputWorkDirectory,
                                               'white_matter_bundles.bundles' )

      if ( os.path.exists( fileNameShortWhiteMatterBundles ) ):

        functors[ 'viewer-load-object' ]( fileNameShortWhiteMatterBundles,
                                          'short-white-matter-bundles' )
        functors[ 'viewer-set-diffuse' ]( 'short-white-matter-bundles',
                                          [ 1.0, 1.0, 1.0, 0.2 ] )
        functors[ 'viewer-assign-referential-to-object' ]( \
                                              'frameDW',
                                              'short-white-matter-bundles' )
        functors[ 'viewer-assign-referential-to-window' ]( \
                                                      'frameDW',
                                                      'first_view',
                                                      'labeled_fibers_window' )
        functors[ 'viewer-add-object-to-window' ]( \
                                                'short-white-matter-bundles',
                                                'first_view',
                                                'labeled_fibers_window',
                                                 addWholeGraphObject = True )
    else:

      globalVariables = dict()
      localVariables = dict()
      execfile( fileNameShortWhiteMatterBundleAtlas,
                globalVariables,
                localVariables )
      # collecting the list of bundle names
      bundleNames = localVariables[ 'attributes' ][ 'labels' ]

      for bundleName in bundleNames:
      
        fileNameShortWhiteMatterBundle = os.path.join( outputWorkDirectory,
                                                      bundleName + '.bundles' )

        if ( os.path.exists( fileNameShortWhiteMatterBundle ) ):
                                                      
          functors[ 'viewer-load-object' ]( fileNameShortWhiteMatterBundle,
                                            bundleName )
          functors[ 'viewer-set-diffuse' ]( bundleName,
                                            [ 1.0, 1.0, 1.0, 0.2 ] )
          functors[ 'viewer-assign-referential-to-object' ]( \
                                                'frameDW',
                                                bundleName )
          functors[ 'viewer-assign-referential-to-window' ]( \
                                                      'frameDW',
                                                      'first_view',
                                                      'labeled_fibers_window' )
          functors[ 'viewer-add-object-to-window' ]( \
                                                bundleName,
                                                'first_view',
                                                'labeled_fibers_window',
                                                 addWholeGraphObject = True )

    functors[ 'synchronize3DWindows' ]( 'first_view', 'labeled_fibers_window' )

