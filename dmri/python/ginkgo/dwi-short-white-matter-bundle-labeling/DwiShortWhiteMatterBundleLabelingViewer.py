from Viewer import *


class DwiShortWhiteMatterBundleLabelingViewer( Viewer ):

  def __init__( self, minimumSize, parent ):

    Viewer.__init__( self, minimumSize, parent )

    ########################## First View ######################################

    self.createView( 'first_view' )

    view = self.getViewFromName( 'first_view' )

    # browser window
    self.addBrowserWindow( 'first_view', 0, 1, 2, 2, 'browser', None )

    # centroids window
    self.add3DWindow( 'first_view',
                      2, 1, 1, 1,
                      'atlas_window',
                      'Atlas',
                      [ 0.0, 0.0, 0.0, 1.0 ],
                      isMuteToolButtonVisible = True,
                      isColorMapToolButtonVisible = True,
                      isSnapshotToolButtonVisible = True,
                      isZoomToolButtonVisible = True,
                      isClippingSpinBoxVisible = True,
                      clippingOptions = [ 2, 0.01, 1000.0, 200.0, 0.1, 2 ],
                      windowsSynchronized3d = [ 'labeled_fibers_window' ] )

    # bundles window
    self.add3DWindow( 'first_view',
                       2, 2, 1, 1,
                      'labeled_fibers_window',
                      'Labeled fibers',
                      [ 0.0, 0.0, 0.0, 1.0 ],
                      isMuteToolButtonVisible = True,
                      isColorMapToolButtonVisible = True,
                      isSnapshotToolButtonVisible = True,
                      isZoomToolButtonVisible = True,
                      isClippingSpinBoxVisible = True,
                      clippingOptions = [ 2, 0.01, 1000.0, 200.0, 0.1, 2 ],
                      windowsSynchronized3d = [ 'atlas_window' ] )




def createDwiShortWhiteMatterBundleLabelingViewer( minimumSize, parent ):

  return DwiShortWhiteMatterBundleLabelingViewer( minimumSize, parent )
