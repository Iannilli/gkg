from Algorithm import *
from DwiSubjectRegistrationToTemplateTask import *


class DwiSubjectRegistrationToTemplateAlgorithm( Algorithm ):

  def __init__( self, verbose ):

    Algorithm.__init__( self, 'DWI-Subject-Registration-To-Template',
                        verbose, True )

    # input data
    self.addParameter( StringParameter( 'fileNameDij', '' ) )
    self.addParameter( IntegerParameter( 'sizeX', \
                                          128, 4, 2048, 1 ) )
    self.addParameter( IntegerParameter( 'sizeY', \
                                          128, 4, 2048, 1 ) )
    self.addParameter( IntegerParameter( 'sizeZ', \
                                          128, 4, 2048, 1 ) )
    self.addParameter( DoubleParameter( 'resolutionX', \
                                        1, 0.1, 100, 0.1 ) )
    self.addParameter( DoubleParameter( 'resolutionY', \
                                        1, 0.1, 100, 0.1 ) )
    self.addParameter( DoubleParameter( 'resolutionZ', \
                                        1, 0.1, 100, 0.1 ) )
    self.addParameter( StringParameter( 'fileNameTemplateDij', '' ) )
    self.addParameter( StringParameter( 'fileNameOriginTransformation', '' ) )
    self.addParameter( BooleanParameter( 'addOriginTransformation', 2 ) )

    self.addParameter( ChoiceParameter( 'template',
                                        0,
                                        [ 'CONNECT/Archi 18-40yo template',
                                          'custom template' ] ) )


  def launch( self ):

    if ( self._verbose ):

      print \
        'running Subject registration to template'

    task = DwiSubjectRegistrationToTemplateTask( self._application )
    task.launch( False )


  def view( self ):

    if ( self._verbose ):

      print \
        'viewing Subject registration to template'

    task = DwiSubjectRegistrationToTemplateTask( self._application )
    task.launch( True )

