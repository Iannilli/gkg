from AlgorithmGUI import *
from ResourceManager import *
from PathHandler import *


class DwiSubjectRegistrationToTemplateAlgorithmGUI( AlgorithmGUI ):

  def __init__( self, algorithm ):

    AlgorithmGUI.__init__( self,
                           algorithm,
                           ResourceManager().getUIFileName( \
                                       'dmri',
                                       'DwiSubjectRegistrationToTemplate.ui' ) )

    ############################################################################
    # connecting input data
    ############################################################################

    # Dij
    self.connectStringParameterToLineEdit( 'fileNameDij',
                                           'lineEdit_FileNameDij' )
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit(\
                                                       'pushButton_FileNameDij',
                                                       'lineEdit_FileNameDij' )

    # template size
    self.connectIntegerParameterToSpinBox( 'sizeX',
                                           'spinBox_SizeX' )
    self._findChild( self._awin, 
                     'spinBox_SizeX' ).lineEdit().setReadOnly( True )
    self.connectSpinBoxToCustomCallback( 'spinBox_SizeX',
                                          self.customSetValue )
    self.connectIntegerParameterToSpinBox( 'sizeY',
                                           'spinBox_SizeY' )
    self._findChild( self._awin, 
                     'spinBox_SizeY' ).lineEdit().setReadOnly( True )
    self.connectSpinBoxToCustomCallback( 'spinBox_SizeY',
                                          self.customSetValue )
    self.connectIntegerParameterToSpinBox( 'sizeZ',
                                           'spinBox_SizeZ' )
    self._findChild( self._awin, 
                     'spinBox_SizeZ' ).lineEdit().setReadOnly( True )
    self.connectSpinBoxToCustomCallback( 'spinBox_SizeZ',
                                          self.customSetValue )

    # template resolution
    self.connectDoubleParameterToSpinBox( 'resolutionX',
                                          'doubleSpinBox_ResolutionX' )
    self.connectDoubleParameterToSpinBox( 'resolutionY',
                                          'doubleSpinBox_ResolutionY' )
    self.connectDoubleParameterToSpinBox( 'resolutionZ',
                                          'doubleSpinBox_ResolutionZ' )

    # origin transformations
    self.connectStringParameterToLineEdit( \
                                      'fileNameOriginTransformation',
                                      'lineEdit_FileNameOriginTransformation' )
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit( \
                                      'pushButton_FileNameOriginTransformation',
                                      'lineEdit_FileNameOriginTransformation' )


    # custom template
    self.connectStringParameterToLineEdit( 'fileNameTemplateDij',
                                           'lineEdit_FileNameTemplateDij' )
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit(
                                            'pushButton_FileNameTemplateDij',
                                            'lineEdit_FileNameTemplateDij' )

    # add origin
    self.connectBooleanParameterToCheckBox( 'addOriginTransformation',
                                            'checkBox_AddOriginTransformation' )
    self.connectCheckBoxToCustomCallback( \
                                  'checkBox_AddOriginTransformation',
                                  self.checkBoxAddOriginTransformationCallBack )

    # template
    self.connectChoiceParameterToComboBox( 'template', 'comboBox_Template' )
    self.connectComboBoxToCustomCallback( 'comboBox_Template',
                                          self.comboBoxTemplateDijCallBack )
    self.checkBoxAddOriginTransformationCallBack( True )
    self.comboBoxTemplateDijCallBack( False )


  def customSetValue( self, value ):

    spinBoxSizeX = self._findChild( self._awin, 'spinBox_SizeX' )
    spinBoxSizeY = self._findChild( self._awin, 'spinBox_SizeY' )
    spinBoxSizeZ = self._findChild( self._awin, 'spinBox_SizeZ' )

    values = [ 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048 ]
    if( values.count( spinBoxSizeX.value() ) == 0  ):

      if( values.count( spinBoxSizeX.value() - 1 ) != 0 ):

        spinBoxSizeX.setValue( \
                        values[ values.index( spinBoxSizeX.value() - 1 ) + 1 ] )

      if( values.count( spinBoxSizeX.value() + 1 ) != 0 ):

        spinBoxSizeX.setValue( \
                        values[ values.index( spinBoxSizeX.value() + 1 ) - 1 ] )

    if( values.count( spinBoxSizeY.value() ) == 0  ):

      if( values.count( spinBoxSizeY.value() - 1 ) != 0 ):

        spinBoxSizeY.setValue( \
                        values[ values.index( spinBoxSizeY.value() - 1 ) + 1 ] )

      if( values.count( spinBoxSizeY.value() + 1 ) != 0 ):

        spinBoxSizeY.setValue( \
                        values[ values.index( spinBoxSizeY.value() + 1 ) - 1 ] )

    if( values.count( spinBoxSizeZ.value() ) == 0  ):

      if( values.count( spinBoxSizeZ.value() - 1 ) != 0 ):

        spinBoxSizeZ.setValue( \
                        values[ values.index( spinBoxSizeZ.value() - 1 ) + 1 ] )

      if( values.count( spinBoxSizeZ.value() + 1 ) != 0 ):

        spinBoxSizeZ.setValue( \
                        values[ values.index( spinBoxSizeZ.value() + 1 ) - 1 ] )
    
  def checkBoxAddOriginTransformationCallBack( self, value ):


    if( value ):

      self._findChild( self._awin,
                  'label_FileNameOriginTransformation' ).setEnabled( True )
      self._findChild( self._awin,
                  'lineEdit_FileNameOriginTransformation' ).setEnabled( True )
      self._findChild( self._awin,
                  'pushButton_FileNameOriginTransformation' ).setEnabled( True )

    else:

      self._findChild( self._awin,
                 'label_FileNameOriginTransformation' ).setEnabled( False )
      self._findChild( self._awin,
                 'lineEdit_FileNameOriginTransformation' ).setEnabled( False )
      self._findChild( self._awin,
                 'pushButton_FileNameOriginTransformation' ).setEnabled( False )


  def comboBoxTemplateDijCallBack( self, value ):

    if( value ):

      self._findChild( self._awin,
                       'label_FileNameTemplateDij' ).setEnabled( True )
      self._findChild( self._awin,
                       'lineEdit_FileNameTemplateDij' ).setEnabled( True )
      self._findChild( self._awin,
                       'pushButton_FileNameTemplateDij' ).setEnabled( True )

    else:

      self._findChild( self._awin,
                       'label_FileNameTemplateDij' ).setEnabled( False )
      self._findChild( self._awin,
                       'lineEdit_FileNameTemplateDij' ).setEnabled( False )
      self._findChild( self._awin,
                       'pushButton_FileNameTemplateDij' ).setEnabled( False )

