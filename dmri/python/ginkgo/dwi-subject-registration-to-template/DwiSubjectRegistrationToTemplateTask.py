from Task import *
from PathHandler import *
import os
import shutil

class DwiSubjectRegistrationToTemplateTask( Task ):

  def __init__( self, application ):

    Task.__init__( self, application )

  ##############################################################################
  # execute()
  ##############################################################################

  def execute( self, functors, parameters, subjectName, verbose, viewOnly ):

    try:

      ####################### acquiring conditions #############################
      functors[ 'condition-acquire' ]( subjectName, 'subject-dij-converted' )
      functors[ 'condition-acquire' ]( subjectName, 'registration-processed' )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 0 )

      ####################### collecting output work directory #################
      outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()
      if ( verbose ):

        print  'Output work directory :', outputWorkDirectory

      ####################### collecting input data filename(s) ################
      addOriginTransformation =  parameters[ \
                                          'addOriginTransformation' ].getValue()
      if( addOriginTransformation ):

        fileNameOriginTransformation = parameters[ \
                                     'fileNameOriginTransformation' ].getValue()

      fileNameTemplateDij = os.path.join( ResourceManager().getDataDirectory(),
                                         'dmri',
                                         'dwi-subject-registration-to-template',
                                         'connect_archi_template.nii.gz' )
      if ( parameters[ 'template' ].getChoice() == 'custom template' ):

        fileNameTemplateDij = parameters[ 'fileNameTemplateDij' ].getValue()

      sizeX = parameters[ 'sizeX' ].getValue()
      sizeY = parameters[ 'sizeY' ].getValue()
      sizeZ = parameters[ 'sizeZ' ].getValue()
      resolutionX = parameters[ 'resolutionX' ].getValue()
      resolutionY = parameters[ 'resolutionY' ].getValue()
      resolutionZ = parameters[ 'resolutionZ' ].getValue()

      fileNameDij = parameters[ 'fileNameDij' ].getValue()
      fileNameDijResampled = os.path.join( \
                outputWorkDirectory,
                'dti_dij_dtitk.nii.gz' )
      fileNameDijInGISFormat = os.path.join( \
                outputWorkDirectory,
                'dti_dij_gis.ima' )
      fileNameTemplateDijResampled = os.path.join( \
                outputWorkDirectory,
                'template_resampled.nii.gz' )
      fileNameMeanInitial = os.path.join( \
                outputWorkDirectory,
                'mean_initial.nii.gz' )
      fileNameMeanInitialTR = os.path.join( \
                outputWorkDirectory,
                'mean_initial_tr.nii.gz' )
      fileNameTemplateBrainMask = os.path.join( \
                outputWorkDirectory,
                'template_brain_mask.nii.gz' )
      fileNameSubjectNames = os.path.join( \
                outputWorkDirectory,
                'dti_dij_dtitk.txt' )
      fileNameAffineTransformation = os.path.join( \
                outputWorkDirectory,
                'dti_dij_dtitk.aff' )
      fileNameAffineTransformationInverse = os.path.join( \
                outputWorkDirectory,
                'dti_dij_dtitk_inv.aff' )
      fileNameAffineTransformationWithoutOrigin = os.path.join( \
                outputWorkDirectory,
                'dti_dij_dtitk_wo_origin.aff' )
      fileNameAffineTransformationWithoutOriginInverse = os.path.join( \
                outputWorkDirectory,
                'dti_dij_dtitk_inv_wo_origin.aff' )
      fileNameAffineResampled = os.path.join( \
                outputWorkDirectory,
                'dti_dij_dtitk_aff.nii.gz' )
      fileNameDiffeomorphicTransformation = os.path.join( \
                outputWorkDirectory,
                'dti_dij_dtitk_aff_diffeo.df.nii.gz' )
      fileNameDiffeomorphicTransformationInverse = os.path.join( \
                outputWorkDirectory,
                'dti_dij_dtitk_aff_diffeo.df_inv.nii.gz' )
      fileNameCombinedTransformation = os.path.join( \
                outputWorkDirectory,
                'dti_dij_dtitk_combinedTransformation.df.nii.gz' )
      fileNameCombinedTransformationWithoutOrigin = os.path.join( \
                outputWorkDirectory,
                'dti_dij_dtitk_combinedTransformation_wo_origin.df.nii.gz' )
      fileNameCombinedTransformationInverse = os.path.join( \
                outputWorkDirectory,
                'dti_dij_dtitk_combinedTransformation_inv.df.nii.gz' )
      fileNameCombinedTransformationWithoutOriginInverse = os.path.join( \
                outputWorkDirectory,
                'dti_dij_dtitk_combinedTransformation_wo_origin_inv.df.nii.gz' )

      ########################### create subject list ##########################
      fileSubjectName = open( fileNameSubjectNames, 'w' )
      fileSubjectName.write( fileNameDijResampled )
      fileSubjectName.close()

      ### changing current directory as DTI-TK generate files in the current dir
      os.chdir( outputWorkDirectory )

      ######################### updating progress bar ##########################
      functors[ 'update-progress-bar' ]( subjectName, 10 )

      command = 'TVResample ' + \
                ' -in ' + fileNameTemplateDij + ' ' + \
                ' -out ' + fileNameTemplateDijResampled + ' ' + \
                ' -align center ' + \
                ' -size ' + str( sizeX ) + ' ' + \
                            str( sizeY ) + ' ' + \
                            str( sizeZ ) + \
                ' -vsize ' + str( resolutionX ) + ' ' + \
                                  str( resolutionY ) + ' ' + \
                                  str( resolutionZ )
      executeCommand( self, subjectName, command, viewOnly )  

      command = 'TVResample ' + \
                ' -in ' + fileNameDij + ' ' + \
                ' -out ' + fileNameDijResampled + ' ' + \
                ' -align center ' + \
                ' -size ' + str( sizeX ) + ' ' + \
                            str( sizeY ) + ' ' + \
                            str( sizeZ ) + \
                ' -vsize ' + str( resolutionX ) + ' ' + \
                                  str( resolutionY ) + ' ' + \
                                  str( resolutionZ )
      executeCommand( self, subjectName, command, viewOnly )

      ###################### convertion for display ############################
      command = 'GkgExecuteCommand Nifti2GisConverter ' + \
                ' -i ' + fileNameDijResampled + \
                ' -o ' + fileNameDijInGISFormat + \
                ' -verbose true'
      executeCommand( self, subjectName, command, viewOnly )
      functors[ 'condition-notify-and-release' ]( subjectName,
                                                  'subject-dij-converted' )
      ######################### updating progress bar ##########################
      functors[ 'update-progress-bar' ]( subjectName, 20 )

      command = 'dti_template_bootstrap ' + \
                fileNameTemplateDijResampled + ' ' + \
                fileNameSubjectNames
      executeCommand( self, subjectName, command, viewOnly )

      ######################### updating progress bar ##########################
      functors[ 'update-progress-bar' ]( subjectName, 30 )

      command = 'dti_affine_reg ' + \
                fileNameMeanInitial + ' ' + \
                fileNameDijResampled + \
                ' EDS 4 4 4 0.01 1'
      executeCommand( self, subjectName, command, viewOnly )

      ######################### updating progress bar ##########################
      functors[ 'update-progress-bar' ]( subjectName, 40 )
      command = 'TVtool ' + \
                ' -in ' + fileNameMeanInitial + \
                ' -out ' + fileNameMeanInitialTR + \
                ' -tr '
      executeCommand( self, subjectName, command, viewOnly )

      ######################### updating progress bar ##########################
      functors[ 'update-progress-bar' ]( subjectName, 50 )

      command = 'BinaryThresholdImageFilter ' + \
                fileNameMeanInitialTR + ' ' + \
                fileNameTemplateBrainMask + \
                ' 0.01 100 1 0 '
      executeCommand( self, subjectName, command, viewOnly )

      ######################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 70 )
      command = 'dti_diffeomorphic_reg ' + \
                fileNameMeanInitial + ' ' + \
                fileNameAffineResampled + ' ' + \
                fileNameTemplateBrainMask + \
                ' 1 6 0.002'
      executeCommand( self, subjectName, command, viewOnly )

      ######################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 80 )

      ######################## add origin transformation #####################
      if ( addOriginTransformation and not viewOnly ):

        fileOriginTransformation = open( fileNameOriginTransformation, 'r' )
        lines = fileOriginTransformation.readlines()
        originTransformation = lines[ 0 ].split()
        fileOriginTransformation.close()

        fileAffineTransformation = open( fileNameAffineTransformation, 'r' )
        lines = fileAffineTransformation.readlines()
        fileAffineTransformation.close()

        #save transformation without moving back to original origin
        fileAffineTransformationWithoutOrigin = \
                         open( fileNameAffineTransformationWithoutOrigin, 'w' )
        fileAffineTransformationWithoutOrigin.write( lines[ 0 ] )
        fileAffineTransformationWithoutOrigin.write( lines[ 1 ] )
        fileAffineTransformationWithoutOrigin.write( lines[ 2 ] )
        fileAffineTransformationWithoutOrigin.write( lines[ 3 ] )
        fileAffineTransformationWithoutOrigin.write( lines[ 4 ] )
        fileAffineTransformationWithoutOrigin.write( lines[ 5 ] )
        fileAffineTransformationWithoutOrigin.close()

        fileAffineTransformation = open( fileNameAffineTransformation, 'w' )
        fileAffineTransformation.write( lines[ 0 ] )
        fileAffineTransformation.write( lines[ 1 ] )
        fileAffineTransformation.write( lines[ 2 ] )
        fileAffineTransformation.write( lines[ 3 ] )
        fileAffineTransformation.write( lines[ 4 ] )
        aff = lines[ 5 ].split()
        translationX = str( float( aff[ 0 ] ) -
                            float( originTransformation[ 0 ] ) )
        translationY = str( float( aff[ 1 ] ) -
                            float( originTransformation[ 1 ] ) )
        translationZ = str( float( aff[ 2 ] ) -
                            float( originTransformation[ 2 ] ) )

        fileAffineTransformation.write( '       ' + translationX + \
                                        '       ' + translationY + \
                                        '       ' + translationZ )
        fileAffineTransformation.close()

        command = 'affine3Dtool ' + \
                  ' -in ' + fileNameAffineTransformation + \
                  ' -invert ' + \
                  ' -out ' + fileNameAffineTransformationInverse
        executeCommand( self, subjectName, command, viewOnly )

        command = 'affine3Dtool ' + \
                  ' -in ' + fileNameAffineTransformationWithoutOrigin + \
                  ' -invert ' + \
                  ' -out ' + fileNameAffineTransformationWithoutOriginInverse
        executeCommand( self, subjectName, command, viewOnly )
        
      else:

      
        command = 'affine3Dtool ' + \
                  ' -in ' + fileNameAffineTransformation + \
                  ' -invert ' + \
                  ' -out ' + fileNameAffineTransformationInverse
        executeCommand( self, subjectName, command, viewOnly )


      command = 'dfToInverse ' + \
                ' -in ' + fileNameDiffeomorphicTransformation 
      executeCommand( self, subjectName, command, viewOnly )

      ######################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 90 )

      command = 'dfRightComposeAffine ' + \
                ' -aff ' + fileNameAffineTransformation + \
                ' -df ' + fileNameDiffeomorphicTransformation + \
                ' -out ' + fileNameCombinedTransformation
      executeCommand( self, subjectName, command, viewOnly )

      if ( addOriginTransformation and not viewOnly ):

        command = 'dfRightComposeAffine ' + \
                  ' -aff ' + fileNameAffineTransformationWithoutOrigin + \
                  ' -df ' + fileNameDiffeomorphicTransformation + \
                  ' -out ' + fileNameCombinedTransformationWithoutOrigin
        executeCommand( self, subjectName, command, viewOnly )

      command = 'dfLeftComposeAffine ' + \
                ' -df ' + fileNameDiffeomorphicTransformationInverse + \
                ' -aff ' + fileNameAffineTransformationInverse + \
                ' -out ' + fileNameCombinedTransformationInverse
      executeCommand( self, subjectName, command, viewOnly )

      if ( addOriginTransformation and not viewOnly ):

        command = 'dfLeftComposeAffine ' + \
                  ' -df ' + fileNameDiffeomorphicTransformationInverse + \
                  ' -aff ' + fileNameAffineTransformationWithoutOriginInverse +\
                  ' -out ' + fileNameCombinedTransformationWithoutOriginInverse
        executeCommand( self, subjectName, command, viewOnly )

      functors[ 'condition-notify-and-release' ]( subjectName,
                                                  'registration-processed' )

      ########################## updating progress bar #########################
      functors[ 'update-progress-bar' ]( subjectName, 100 )

    except:

      self._functors[ 'task-set-status-as-failed' ]( subjectName,
                                                     sys.exc_info() )

  ##############################################################################
  # display()
  ##############################################################################

  def display( self, functors, parameters, subjectName, verbose,
               isReportModeActive ):

    ####################### collecting output work directory ###################
    outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()
    if ( verbose ):

      print  'Output work directory :', outputWorkDirectory

    ####################### collecting input data filename(s) ##################
    fileNameDij = parameters[ 'fileNameDij' ].getValue()
    fileNameDijInGISFormat = os.path.join( outputWorkDirectory,
                                           'dti_dij_gis.ima' )

    fileNameTemplateDij = os.path.join( outputWorkDirectory,
                                        'template_resampled.nii.gz' )

    fileNameRegisteredDij = os.path.join( outputWorkDirectory,
                                          'dti_dij_dtitk_aff_diffeo.nii.gz' )

    ############################# loading Input Dij ############################
    functors[ 'condition-wait-and-release' ]( subjectName,
                                              'subject-dij-converted' )
    functors[ 'viewer-load-object' ]( fileNameDijInGISFormat, 'Dij' )
    functors[ 'viewer-add-object-to-window' ]( 'Dij', 'first_view', 'Dij' )

    ############################# loading template Dij #########################
    functors[ 'viewer-load-object' ]( fileNameTemplateDij, 'templateDij' )
    functors[ 'viewer-add-object-to-window' ]( 'templateDij',
                                               'first_view',
                                               'Template Dij' )
    functors[ 'viewer-set-colormap' ]( 'templateDij', 'B-W LINEAR', 0.0, 1.0 )

    ####################### waiting for result #################################
    functors[ 'condition-wait-and-release' ]( subjectName,
                                              'registration-processed' )

    ############################ loading registered Dij ########################
    functors[ 'viewer-load-object' ]( fileNameRegisteredDij, 'registeredDij' )
    functors[ 'viewer-add-object-to-window' ]( 'registeredDij',
                                               'first_view',
                                               'Registered Dij' )

    #################### loading Template+Registered Dij #######################
    functors[ 'viewer-set-colormap' ]( 'templateDij', 'RED TEMPERATURE' )
    functors[ 'viewer-fusion-objects' ]( [ 'registeredDij', 'templateDij'],
                                         'fusionRegisteredDijAndTemplateDij',
                                         'Fusion2DMethod' )
    functors[ 'viewer-add-object-to-window' ]( \
                                            'fusionRegisteredDijAndTemplateDij',
                                            'first_view',
                                            'Registered+Template Dij' )

