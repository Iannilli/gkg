from Viewer import *


class DwiSubjectRegistrationToTemplateViewer( Viewer ):

  def __init__( self, minimumSize, parent ):

    Viewer.__init__( self, minimumSize, parent )

    ########################## First View ######################################

    self.createView( 'first_view' )

    # first dti window
    self.addAxialWindow( 'first_view', 0, 0, 1, 1,
                         'Dij', 'Dij' )

    # template window
    self.addAxialWindow( 'first_view', 0, 1, 1, 1,
                         'Template Dij', 'Template Dij' )

    # output dti window
    self.addAxialWindow( 'first_view', 1, 0, 1, 1,
                         'Registered Dij', 'Registered Dij' )

    # template and output dti window
    self.addAxialWindow( 'first_view', 1, 1, 1, 1,
                         'Registered+Template Dij', 'Registered+Template Dij' )


def createDwiSubjectRegistrationToTemplateViewer( minimumSize, parent ):

  return DwiSubjectRegistrationToTemplateViewer( minimumSize, parent )

