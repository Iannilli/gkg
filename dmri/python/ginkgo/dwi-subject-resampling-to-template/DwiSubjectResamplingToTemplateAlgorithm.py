from Algorithm import *
from DwiSubjectResamplingToTemplateTask import *


class DwiSubjectResamplingToTemplateAlgorithm( Algorithm ):

  def __init__( self, verbose ):

    Algorithm.__init__( self, 'DWI-Subject-Resampling-To-Template',
                        verbose, True )

    # input data
    self.addParameter( ChoiceParameter( \
                                  'transformationDirection',
                                  0,
                                  [ 'from subject space to template space',
                                    'from template space to subject space' ] ) )
                                    
    self.addParameter( BooleanParameter( 'useTransformationWithoutOrigin', 0 ) )

    self.addParameter( StringParameter( 'fileNameInputData', '' ) )
    self.addParameter( StringParameter( 'directoryNameRegistration', '' ) )


  def launch( self ):

    if ( self._verbose ):

      print \
        'running subject resampling to template'

    task = DwiSubjectResamplingToTemplateTask( self._application )
    task.launch( False )


  def view( self ):

    if ( self._verbose ):

      print \
        'viewing subject resampling to template'

    task = DwiSubjectResamplingToTemplateTask( self._application )
    task.launch( True )

