from AlgorithmGUI import *
from ResourceManager import *


class DwiSubjectResamplingToTemplateAlgorithmGUI( AlgorithmGUI ):

  def __init__( self, algorithm ):

    AlgorithmGUI.__init__( self,
                           algorithm,
                           ResourceManager().getUIFileName( \
                                         'dmri',
                                         'DwiSubjectResamplingToTemplate.ui' ) )

    ###########################################################################
    # connecting input data
    ###########################################################################

    # resample direction
    self.connectChoiceParameterToComboBox( 'transformationDirection',
                                           'comboBox_TransformationDirection' )

    # add origin
    self.connectBooleanParameterToCheckBox( \
                                     'useTransformationWithoutOrigin',
                                     'checkBox_UseTransformationWithoutOrigin' )

    # input data
    self.connectFileOrDirectoryListBrowserToPushButtonAndLineEdit(\
                                                 'pushButton_FileNameInputData',
                                                 'lineEdit_FileNameInputData' )
    self.connectStringParameterToLineEdit( 'fileNameInputData',
                                           'lineEdit_FileNameInputData' )

    # subject registration to template directory
    self.connectDirectoryBrowserToPushButtonAndLineEdit( \
                                     'pushButton_DirectoryNameRegistration',
                                     'lineEdit_DirectoryNameRegistration' )
    self.connectStringParameterToLineEdit( \
                                     'directoryNameRegistration',
                                     'lineEdit_DirectoryNameRegistration' )
