from Task import *
from PathHandler import *
from ContainerInformationHandler import *
from VolumeInformationHandler import *
import os
import shutil


class DwiSubjectResamplingToTemplateTask( Task ):

  def __init__( self, application ):

    Task.__init__( self, application )

  ##############################################################################
  # execute()
  ##############################################################################

  def execute( self, functors, parameters, subjectName, verbose, viewOnly ):

    try:

      ####################### acquiring conditions #############################
      functors[ 'condition-acquire' ]( subjectName, 'resampling-processed' )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 0 )

      ####################### collecting output work directory #################
      outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()
      if( verbose ):

        print  'Output work directory :', outputWorkDirectory

      ####################### collecting input data filename(s) ################
      transformationDirection = \
                              parameters[ 'transformationDirection' ].getChoice()
      useTransformationWithoutOrigin = \
                       parameters[ 'useTransformationWithoutOrigin' ].getValue()
      fileNameInputData = parameters[ 'fileNameInputData' ].getValue()
      directoryNameRegistration = parameters[ \
                                        'directoryNameRegistration' ].getValue()

      fileNameIdentityTransformation = os.path.join( \
                                         ResourceManager().getDataDirectory(),
                                         'dmri',
                                         'dwi-subject-resampling-to-template',
                                         'identityTransformation.trm' )

      fileNameMeanInitial = os.path.join( \
                directoryNameRegistration,
                'mean_initial.nii.gz' )
      fileNameTemplate = os.path.join( \
                directoryNameRegistration,
                'dti_dij_gis.ima' )
      fileNameDij = os.path.join( \
                directoryNameRegistration,
                'dti_dij_dtitk.nii.gz' )
      fileNameAffineTransformation = os.path.join( \
                directoryNameRegistration,
                'dti_dij_dtitk.aff' )
      fileNameAffineTransformationInverse = os.path.join( \
                directoryNameRegistration,
                'dti_dij_dtitk_inv.aff' )
      fileNameAffineTransformationWithoutOrigin = os.path.join( \
                directoryNameRegistration,
                'dti_dij_dtitk_wo_origin.aff' )
      fileNameAffineTransformationWithoutOriginInverse = os.path.join( \
                directoryNameRegistration,
                'dti_dij_dtitk_inv_wo_origin.aff' )
      fileNameAffineResampled = os.path.join( \
                directoryNameRegistration,
                'dti_dij_dtitk_aff.nii.gz' )
      fileNameDiffeomorphicTransformation = os.path.join( \
                directoryNameRegistration,
                'dti_dij_dtitk_aff_diffeo.df.nii.gz' )
      fileNameDiffeomorphicTransformationInverse = os.path.join( \
                directoryNameRegistration,
                'dti_dij_dtitk_aff_diffeo.df_inv.nii.gz' )
      fileNameCombinedTransformation = os.path.join( \
                directoryNameRegistration,
                'dti_dij_dtitk_combinedTransformation.df.nii.gz' )
      fileNameCombinedTransformationWithoutOrigin = os.path.join( \
                directoryNameRegistration,
                'dti_dij_dtitk_combinedTransformation_wo_origin.df.nii.gz' )
      fileNameCombinedTransformationInverse = os.path.join( \
                directoryNameRegistration,
                'dti_dij_dtitk_combinedTransformation_inv.df.nii.gz' )
      fileNameCombinedTransformationWithoutOriginInverse = os.path.join( \
                directoryNameRegistration,
                'dti_dij_dtitk_combinedTransformation_wo_origin_inv.df.nii.gz' )


      ######################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 10 )

      fileNameAppliedCombinedTransformation = fileNameCombinedTransformation
      fileNameAppliedAffineTransformation = fileNameAffineTransformation
      fileNameAppliedDiffeomorphicTransformation = \
                                             fileNameDiffeomorphicTransformation
      if ( useTransformationWithoutOrigin ):
      
        fileNameAppliedCombinedTransformation = \
                                    fileNameCombinedTransformationWithoutOrigin
        fileNameAppliedAffineTransformation = \
                                    fileNameAffineTransformationWithoutOrigin
      
      fileNameTarget = fileNameMeanInitial
      fileNameReference = fileNameDij
      
      if ( transformationDirection == 'from template space to subject space' ):
      
        fileNameTarget = fileNameDij
        fileNameReference = fileNameMeanInitial
        fileNameAppliedCombinedTransformation = \
                                           fileNameCombinedTransformationInverse
        fileNameAppliedAffineTransformation = \
                                           fileNameAffineTransformationInverse
        fileNameAppliedDiffeomorphicTransformation = \
                                      fileNameDiffeomorphicTransformationInverse
        if ( useTransformationWithoutOrigin ):
      
          fileNameAppliedCombinedTransformation = \
                              fileNameCombinedTransformationWithoutOriginInverse
          fileNameAppliedAffineTransformation = \
                              fileNameAffineTransformationWithoutOriginInverse


      fileNameInputDataList = fileNameInputData.split( ';' )
      for f in fileNameInputDataList:

        containerType = getContainerType( f )
        
        if ( containerType == 'Volume' ):

          if ( isNiftiFile( fileNameTarget ) is False ):

            fileNameTargetInNiftiFormat = os.path.join( \
                            outputWorkDirectory,
                            os.path.basename( getNiftiPath( fileNameTarget ) ) )
            command = 'AimsFileConvert ' + \
                      ' -i ' + fileNameTarget + \
                      ' -o ' + fileNameTargetInNiftiFormat
            executeCommand( self, subjectName, command, viewOnly )
            fileNameTarget = fileNameTargetInNiftiFormat

          fileNameReference = fileNameDij
      
          if ( transformationDirection == \
                                       'from template space to subject space' ):
      
            fileNameTarget = fileNameDij
            fileNameReference = fileNameMeanInitial

          itemType = getItemTypeOfVolume( f )
          
          if ( itemType == 'gkg_RGBComponent' ):
          
            pathNameWithoutExtension = os.path.join( \
                                              outputWorkDirectory,
                                              getBaseNameWithoutExtension( f ) )
            command = 'AimsSplitRgb ' + \
                      ' -i ' + f + \
                      ' -o ' + pathNameWithoutExtension
            executeCommand( self, subjectName, command, viewOnly )

 
            for component in [ '_r', '_g', '_b' ]:

              fileNameInputDataInGisFormat = pathNameWithoutExtension + \
                                             component + \
                                             '.ima'
              fileNameInputDataInNiftiFormat = pathNameWithoutExtension + \
                                               component + \
                                               '.nii'
              command = 'AimsFileConvert ' + \
                        ' -i ' + fileNameInputDataInGisFormat + \
                        ' -o ' + fileNameInputDataInGisFormat + \
                        ' -t U8'
              executeCommand( self, subjectName, command, viewOnly )

              command = 'GkgExecuteCommand Resampling3d ' + \
                        ' -reference ' + fileNameInputDataInGisFormat + \
                        ' -output ' + fileNameInputDataInGisFormat + \
                        ' -template ' + fileNameTemplate + \
                        ' -transforms ' + fileNameIdentityTransformation + \
                        ' -order 0'
              executeCommand( self, subjectName, command, viewOnly )

              command = 'GkgExecuteCommand Gis2NiftiConverter ' + \
                        ' -i ' + fileNameInputDataInGisFormat + \
                        ' -o ' + fileNameInputDataInNiftiFormat + \
                        ' -verbose true -verbosePluginLoading false'
              executeCommand( self, subjectName, command, viewOnly )

              fileNameOutputDataInNiftiFormat = pathNameWithoutExtension + \
                                                '_resampled' + \
                                                component + \
                                                '.nii.gz'
              fileNameOutputDataInGisFormat = pathNameWithoutExtension + \
                                              '_resampled' + \
                                              component + \
                                              '.ima'
              command = 'deformationScalarVolume ' + \
                        ' -in ' + fileNameInputDataInNiftiFormat + \
                        ' -trans ' + fileNameAppliedCombinedTransformation + \
                        ' -target ' + fileNameTarget + \
                        ' -out ' + fileNameOutputDataInNiftiFormat
              executeCommand( self, subjectName, command, viewOnly )

              command = 'AimsFileConvert ' + \
                        ' -i ' + fileNameOutputDataInNiftiFormat + \
                        ' -o ' + fileNameOutputDataInGisFormat + \
                        ' -t U8'
              executeCommand( self, subjectName, command, viewOnly )

            command = 'AimsMerge2Rgb ' + \
                      ' -r ' + pathNameWithoutExtension + '_resampled_r.ima' + \
                      ' -g ' + pathNameWithoutExtension + '_resampled_g.ima' + \
                      ' -b ' + pathNameWithoutExtension + '_resampled_b.ima' + \
                      ' -o ' + pathNameWithoutExtension + '_resampled.ima'
            executeCommand( self, subjectName, command, viewOnly  )
          
          else:
          
            sizeT = getSizeTOfVolume( f )
            fileNameOutputDataInGisFormat = os.path.join( \
                                            outputWorkDirectory,
                                            getBaseNameWithoutExtension( f ) + \
                                            '_resampled.ima' )
            if ( sizeT > 1 ):

              tmpPath = os.path.join( outputWorkDirectory, 'tmp' )
              if not os.path.exists( tmpPath ):

                os.mkdir( tmpPath )

              fileNamesToCat = ''
              for t in xrange( sizeT ):

                tFileName = os.path.join( tmpPath, str( t ) )
                fileNamesToCat += tFileName + '_resampled.ima '
                command = 'GkgExecuteCommand SubVolume ' + \
                          ' -i ' + f + \
                          ' -o ' + tFileName + '.ima' + \
                          ' -t ' + str( t ) + \
                          ' -T ' + str( t ) + \
                          ' -verbose true'
                executeCommand( self, subjectName, command, viewOnly )

                command = 'GkgExecuteCommand Resampling3d ' + \
                          ' -reference ' + tFileName + '.ima' + \
                          ' -output ' + tFileName + '.ima' + \
                          ' -template ' + fileNameTemplate + \
                          ' -transforms ' + fileNameIdentityTransformation
                executeCommand( self, subjectName, command, viewOnly )

                command = 'AimsFileConvert ' + \
                          ' -i ' + tFileName + '.ima' + \
                          ' -o ' + tFileName + '.nii.gz '
                executeCommand( self, subjectName, command, viewOnly )

                command = 'deformationScalarVolume ' + \
                          ' -in ' + tFileName + '.nii.gz ' + \
                          ' -trans ' + fileNameAppliedCombinedTransformation + \
                          ' -target ' + fileNameTarget + \
                          ' -out ' + tFileName + '_resampled.nii.gz'
                executeCommand( self, subjectName, command, viewOnly )

                command = 'AimsFileConvert ' + \
                          ' -i ' + tFileName + '_resampled.nii.gz' + \
                          ' -o ' + tFileName + '_resampled.ima'
                executeCommand( self, subjectName, command, viewOnly )

              command = 'GkgExecuteCommand Cat ' + \
                        ' -i ' + fileNamesToCat + \
                        ' -o ' + fileNameOutputDataInGisFormat + \
                        ' -t t ' + \
                        ' -verbose true'

              executeCommand( self, subjectName, command, viewOnly )

              if ( os.path.exists( tmpPath ) and not viewOnly ):

                shutil.rmtree( tmpPath )

            else:

              fileNameInputDataInNiftiFormat = os.path.join( \
                                         outputWorkDirectory,
                                         os.path.basename( getNiftiPath( f ) ) )
              fileNameInputDataInGisFormat = os.path.join( \
                                           outputWorkDirectory,
                                           os.path.basename( getGisPath( f ) ) )
              if( isGisFile( f ) == False ):



                command = 'AimsFileConvert ' + \
                          ' -i ' + f + \
                          ' -o ' + fileNameInputDataInGisFormat 
                executeCommand( self, subjectName, command, viewOnly )
                f = fileNameInputDataInGisFormat

              command = 'GkgExecuteCommand Resampling3d ' + \
                        ' -reference ' + f + \
                        ' -output ' + fileNameInputDataInGisFormat + \
                        ' -template ' + fileNameTemplate + \
                        ' -transforms ' + fileNameIdentityTransformation
              executeCommand( self, subjectName, command, viewOnly )

              command = 'AimsFileConvert ' + \
                        ' -i ' + fileNameInputDataInGisFormat + \
                        ' -o ' + fileNameInputDataInNiftiFormat
              executeCommand( self, subjectName, command, viewOnly )

              fileNameOutputData = os.path.join( \
                                            outputWorkDirectory,
                                            getBaseNameWithoutExtension( f ) + \
                                            '_resampled.nii.gz' )
              command = 'deformationScalarVolume ' + \
                        ' -in ' + fileNameInputDataInNiftiFormat + \
                        ' -trans ' + fileNameAppliedCombinedTransformation + \
                        ' -target ' + fileNameTarget + \
                        ' -out ' + fileNameOutputData
              executeCommand( self, subjectName, command, viewOnly )

              command = 'AimsFileConvert ' + \
                        ' -i ' + fileNameOutputData + \
                        ' -o ' + fileNameOutputDataInGisFormat
              executeCommand( self, subjectName, command, viewOnly )
        
        elif ( containerType == 'BundleMap' ):

          fileNameTarget = fileNameMeanInitial
          fileNameReference = fileNameDij

          if ( transformationDirection == \
                                       'from template space to subject space' ):
      
            fileNameTarget = fileNameDij
            fileNameReference = fileNameMeanInitial

          fileNameInputDataList = fileNameInputData.split( ';' )
          fileNameBundleList = ''
          for f in fileNameInputDataList:

            fileNameInputBundleList += f + ' '
            fileNameInputBundleList += os.path.join( \
                                        outputWorkDirectory,
                                        getBaseNameWithoutExtension( f ) ) + ' '
 
          fileNameInputBundleMapForRendering = os.path.join( \
                                       outputWorkDirectory,
                                       'input_bundlemap_for_rendering.bundles' )
          fileNameOutputBundleMapForRendering = os.path.join( \
                                      outputWorkDirectory,
                                      'output_bundlemap_for_rendering.bundles' )
 
          command = 'GkgExecuteCommand DwiBundleDiffeomorphicResampling ' + \
                    ' -b ' + f + \
                    ' -reference ' + fileNameReference + \
                    ' -target ' + fileNameTarget + \
                    ' -at ' + fileNameAppliedAffineTransformation + \
                    ' -dt ' + fileNameAppliedDiffeomorphicTransformation + \
                    ' -o ' + pathNameWithoutExtension + '_resampled.bundles' + \
                    ' -ir ' + fileNameInputBundleMapForRendering + \
                    ' -or ' + fileNameOutputBundleMapForRendering + \
                    ' -verbose true'
 
          executeCommand( self, subjectName, command, viewOnly )
        
        else:
        
          message = '\'' + f + '\' container type is invalid'
          raise RuntimeError, message

      functors[ 'condition-notify-and-release' ]( subjectName,
                                                  'resampling-processed' )


      ########################## updating progress bar #########################
      functors[ 'update-progress-bar' ]( subjectName, 100 )

    except:

      self._functors[ 'task-set-status-as-failed' ]( subjectName,
                                                     sys.exc_info() )

  ##############################################################################
  # display()
  ##############################################################################

  def display( self, functors, parameters, subjectName, verbose,
               isReportModeActive ):

    ####################### collecting output work directory ###################
    outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()
    if ( verbose ):

      print  'Output work directory :', outputWorkDirectory

    ####################### collecting input data filename(s) ##################
    transformationDirection = \
                            parameters[ 'transformationDirection' ].getChoice()
    useTransformationWithoutOrigin = \
                     parameters[ 'useTransformationWithoutOrigin' ].getValue()
    fileNameInputData = parameters[ 'fileNameInputData' ].getValue()
    directoryNameRegistration = parameters[ \
                                      'directoryNameRegistration' ].getValue()


    fileNameDijAffDiffeo = os.path.join( directoryNameRegistration,
                                         'dti_dij_dtitk_aff_diffeo.nii.gz' )

    fileNameDij = os.path.join( directoryNameRegistration,
                                'dti_dij_dtitk.nii.gz' )


    fileNameTarget = fileNameDijAffDiffeo
    if ( transformationDirection == 'from template space to subject space' ):
      
      fileNameTarget = fileNameDij


    ######################## taking container and item type ####################

    fileNameInputDataList = fileNameInputData.split( ';' )
    fileNameFirstInputData = fileNameInputDataList[ 0 ]
    
    containerType = getContainerType( fileNameFirstInputData )
    itemType = None
    if ( containerType == 'Volume' ):
    
      itemType = getItemTypeOfVolume( fileNameFirstInputData )
      

    ######################## loading target into viewer ########################

    functors[ 'viewer-load-object' ]( fileNameTarget, 'target' )
    if ( ( containerType == 'Volume' ) and ( itemType != 'gkg_RGBComponent' ) ):
    
      functors[ 'viewer-set-colormap' ]( 'target', 'RED TEMPERATURE' )


    ######################## adding input data to viewer #######################

    if ( containerType == 'Volume' ):
      
      functors[ 'viewer-load-object' ]( fileNameFirstInputData,
                                        'firstInputData' )
      functors[ 'viewer-add-object-to-window' ]( 'firstInputData',
                                                 'first_view',
                                                 'Input data' )

    elif ( containerType == 'BundleMap' ):

      fileNameInputBundleMapForRendering = os.path.join( \
                                       outputWorkDirectory,
                                       'input_bundlemap_for_rendering.bundles' )
      functors[ 'viewer-load-object' ]( fileNameInputBundleMapForRendering,
                                        'inputBundleMapForRendering' )
      functors[ 'viewer-add-object-to-window' ]( 'inputBundleMapForRendering',
                                                 'first_view',
                                                 'Input data' )

    ######################## waiting for result ################################
    functors[ 'condition-wait-and-release' ]( subjectName,
                                              'resampling-processed' )

    ######################## adding output data to viewer ######################
    if ( containerType == 'Volume' ):
      
      fileNameOutputData = os.path.join( \
                                      outputWorkDirectory,
                                      getBaseNameWithoutExtension( \
                                                    fileNameFirstInputData ) + \
                                            '_resampled.ima' )
      functors[ 'viewer-load-object' ]( fileNameOutputData,
                                        'outputData' )
      functors[ 'viewer-fusion-objects' ]( [ 'outputData', 'target' ],
                                           'fusionOutputDataAndTarget',
                                           'Fusion2DMethod' )
      functors[ 'viewer-add-object-to-window' ]( 'fusionOutputDataAndTarget',
                                                 'first_view',
                                                 'Output data' )

    elif ( containerType == 'BundleMap' ):

      fileNameOutputBundleMapForRendering = os.path.join( \
                                      outputWorkDirectory,
                                      'output_bundlemap_for_rendering.bundles' )
      functors[ 'viewer-load-object' ]( fileNameOutputBundleMapForRendering,
                                        'outputBundleMapForRendering' )
      functors[ 'viewer-add-object-to-window' ]( 'outputBundleMapForRendering',
                                                 'first_view',
                                                 'Output data' )
      functors[ 'viewer-add-object-to-window' ]( 'target',
                                                 'first_view',
                                                 'Output data' )


