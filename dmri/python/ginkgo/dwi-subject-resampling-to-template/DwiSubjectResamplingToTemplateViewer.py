from Viewer import *


class DwiSubjectResamplingToTemplateViewer( Viewer ):

  def __init__( self, minimumSize, parent ):

    Viewer.__init__( self, minimumSize, parent )

    ########################## First View ######################################

    self.createView( 'first_view' )

    # input data window
    self.add3DWindow( 'first_view', 0, 0, 1, 1,
                      'Input data', 'Input data',
                      backgroundColor = [ 0, 0, 0, 1 ],
                      isMuteToolButtonVisible = True,
                      isColorMapToolButtonVisible = True,
                      isSnapshotToolButtonVisible = True,
                      isZoomToolButtonVisible = True,
                      isClippingSpinBoxVisible = True,
                      clippingOptions = [ 2, 0.01, 1000.0, 2.0, 0.1, 2 ] )

    # ouptut data with target window
    self.add3DWindow( 'first_view', 0, 1, 1, 1,
                      'Output data', 'Output data', 
                      backgroundColor = [ 0, 0, 0, 1 ],
                      isMuteToolButtonVisible = True,
                      isColorMapToolButtonVisible = True,
                      isSnapshotToolButtonVisible = True,
                      isZoomToolButtonVisible = True,
                      isClippingSpinBoxVisible = True,
                      clippingOptions = [ 2, 0.01, 1000.0, 2.0, 0.1, 2 ] )

def createDwiSubjectResamplingToTemplateViewer( minimumSize, parent ):

  return DwiSubjectResamplingToTemplateViewer( minimumSize, parent )

