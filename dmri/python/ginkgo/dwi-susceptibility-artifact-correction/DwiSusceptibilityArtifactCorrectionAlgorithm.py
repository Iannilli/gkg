from Algorithm import *
from DwiSusceptibilityArtifactCorrectionTask import *


class DwiSusceptibilityArtifactCorrectionAlgorithm( Algorithm ):

  def __init__( self, verbose ):

    Algorithm.__init__( self, 'DWI-Susceptibility-Artifact-Correction', verbose,
                        True )

    # diffusion-weighted input data
    self.addParameter( StringParameter( 'rawDwiDirectory', '' ) )
    self.addParameter( StringParameter( 'outlierFilteredDwiDirectory', '' ) )
    self.addParameter( StringParameter( 'roughMaskDirectory', '' ) )

    # Correction strategy
    self.addParameter( ChoiceParameter( 'correctionStrategy',
                                        0,
                                        [ 'Fieldmap-based correction',
                                          'Top-Up-based correction' ] ) )

    ############################################################################
    # parameters for fieldmap-based correction
    ############################################################################
    
    # Bruker field mapping
    self.addParameter( StringParameter( 'brukerFileNameFirstEchoB0Magnitude',
                                        '' ) )
    self.addParameter( StringParameter( 'brukerFileNameB0PhaseDifference',
                                        '' ) )
    self.addParameter( DoubleParameter( 'brukerDeltaTE',
                                        2.46, 0.0, 100.0, 0.01 ) )
    
    # GE field mapping
    self.addParameter( StringParameter( \
                            'geFileNameDoubleEchoB0MagnitudePhaseRealImaginary',
                            '' ) )
    self.addParameter( DoubleParameter( 'geDeltaTE',
                                        2.46, 0.0, 100.0, 0.01 ) )

    # Philips field mapping
    self.addParameter( StringParameter( 'philipsFileNameFirstEchoB0Magnitude',
                                        '' ) )
    self.addParameter( StringParameter( 'philipsFileNameB0PhaseDifference',
                                        '' ) )
    self.addParameter( DoubleParameter( 'philipsDeltaTE',
                                        2.46, 0.0, 100.0, 0.01 ) )

    # Siemens field mapping
    self.addParameter( StringParameter( 'siemensFileNameDoubleEchoB0Magnitude',
                                        '' ) )
    self.addParameter( StringParameter( 'siemensFileNameB0PhaseDifference',
                                        '' ) )
    self.addParameter( DoubleParameter( 'siemensDeltaTE',
                                        2.46, 0.0, 100.0, 0.01 ) )

    # Bruker DW-EPI acquisition parameters
    self.addParameter( DoubleParameter( 'brukerPartialFourierFactor',
                                        1.0, 0.0, 1.0, 0.001 ) )
    self.addParameter( DoubleParameter( 'brukerEchoSpacing',
                                        0.75, 0.0, 10.0, 0.01 ) )
    self.addParameter( IntegerParameter( 'brukerParallelAccelerationFactor',
                                          1, 1, 10, 1 ) )
    self.addParameter( BooleanParameter( 'brukerPhaseNegativeSign', 0 ) )

    # GE DW-EPI acquisition parameters
    self.addParameter( DoubleParameter( 'gePartialFourierFactor',
                                        1.0, 0.0, 1.0, 0.001 ) )
    self.addParameter( DoubleParameter( 'geEchoSpacing',
                                        0.75, 0.0, 10.0, 0.01 ) )
    self.addParameter( IntegerParameter( 'geParallelAccelerationFactor',
                                          1, 1, 10, 1 ) )
    self.addParameter( BooleanParameter( 'gePhaseNegativeSign', 0 ) )

    # Philips DW-EPI acquisition parameters
    self.addParameter( DoubleParameter( 'philipsPartialFourierFactor',
                                        1.0, 0.0, 1.0, 0.001 ) )
    self.addParameter( DoubleParameter( 'philipsEchoSpacing',
                                        0.75, 0.0, 10.0, 0.01 ) )
    self.addParameter( IntegerParameter( 'philipsEPIFactor',
                                         128, 0, 1024, 1 ) )
    self.addParameter( DoubleParameter( 'philipsWaterFatShiftPerPixel',
                                         0.0, 0.0, 1024.0, 0.01 ) )
    self.addParameter( DoubleParameter( 'philipsStaticB0Field',
                                        3.0, 0.0, 10.0, 0.01 ) )
    self.addParameter( IntegerParameter( 'philipsParallelAccelerationFactor',
                                          1, 1, 10, 1 ) )
    self.addParameter( BooleanParameter( 'philipsPhaseNegativeSign', 0 ) )

    # Siemens DW-EPI acquisition parameters
    self.addParameter( DoubleParameter( 'siemensPartialFourierFactor',
                                        1.0, 0.0, 1.0, 0.001 ) )
    self.addParameter( DoubleParameter( 'siemensEchoSpacing',
                                        0.75, 0.0, 10.0, 0.01 ) )
    self.addParameter( IntegerParameter( 'siemensParallelAccelerationFactor',
                                          1, 1, 10, 1 ) )
    self.addParameter( BooleanParameter( 'siemensPhaseNegativeSign', 0 ) )


    # B0 to DW Registration parameters
    self.addParameter( BooleanParameter( \
                                        'generateDwToB0Transformation', True ) )
    self.addParameter( BooleanParameter( 'importDwToB0Transformation', False ) )
    self.addParameter( StringParameter( 'fileNameDwToB0Transformation', '' ) )
    self.addParameter( RegistrationParameter( 'DwToB0RegistrationParameter',
                        { 'optimizerParametersTranslationX':
                            { 'defaultValue': 10 },
                          'optimizerParametersTranslationY':
                            { 'defaultValue': 10 },
                          'optimizerParametersTranslationZ':
                            { 'defaultValue': 10 },
                          'optimizerParametersRotationX':
                            { 'defaultValue': 10 },
                          'optimizerParametersRotationY':
                            { 'defaultValue': 10 },
                          'optimizerParametersRotationZ':
                            { 'defaultValue': 10 } } ) )



    ############################################################################
    # parameters for Top-Up-based correction
    ############################################################################

    # top-up references
    self.addParameter( StringParameter( 'blipUpReferenceFileName', '' ) )
    self.addParameter( StringParameter( 'blipDownReferenceFileName', '' ) )
    
    self.addParameter( StringParameter( 'blipUpParameters', '' ) )
    self.addParameter( StringParameter( 'blipDownParameters', '' ) )


    
  def launch( self ):

    if ( self._verbose ):

      print \
        'running DWI susceptibility artifact correction'

    task = DwiSusceptibilityArtifactCorrectionTask( self._application )
    task.launch( False )


  def view( self ):

    if ( self._verbose ):

      print \
        'viewing DWI susceptibility artifact correction'

    task = DwiSusceptibilityArtifactCorrectionTask( self._application )
    task.launch( True )

