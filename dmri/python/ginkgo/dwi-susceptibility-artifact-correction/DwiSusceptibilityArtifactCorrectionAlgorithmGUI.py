from AlgorithmGUI import *
from ResourceManager import *
from PathHandler import *


class DwiSusceptibilityArtifactCorrectionAlgorithmGUI( AlgorithmGUI ):

  def __init__( self, algorithm ):

    AlgorithmGUI.__init__( self,
                           algorithm,
                           ResourceManager().getUIFileName(
                                    'dmri',
                                    'DwiSusceptibilityArtifactCorrection.ui' ) )

    ############################################################################
    ############################################################################
    # connecting input directory interface
    ############################################################################
    ############################################################################

    # raw DW data directory interface
    self.connectStringParameterToLineEdit( 'rawDwiDirectory',
                                           'lineEdit_RawDwiDirectory' )    
    self.connectDirectoryBrowserToPushButtonAndLineEdit(
                                                   'pushButton_RawDwiDirectory',
                                                   'lineEdit_RawDwiDirectory' )
    self._findChild( self._awin,
                     'lineEdit_RawDwiDirectory'
                    ).textChanged.connect( self.setManufacturer )

    # outlier filtered DW data directory interface
    self.connectStringParameterToLineEdit( \
                                        'outlierFilteredDwiDirectory',
                                        'lineEdit_OutlierFilteredDwiDirectory' )
    self.connectDirectoryBrowserToPushButtonAndLineEdit(
                                       'pushButton_OutlierFilteredDwiDirectory',
                                       'lineEdit_OutlierFilteredDwiDirectory' )

    # rough mask directory interface
    self.connectStringParameterToLineEdit( 'roughMaskDirectory',
                                           'lineEdit_RoughMaskDirectory' )    
    self.connectDirectoryBrowserToPushButtonAndLineEdit(
                                                'pushButton_RoughMaskDirectory',
                                                'lineEdit_RoughMaskDirectory' )

    ############################################################################
    ############################################################################
    # connecting correction strategy interface
    ############################################################################
    ############################################################################

    # Connecting correction strategy
    self.connectChoiceParameterToComboBox( 'correctionStrategy',
                                           'comboBox_CorrectionStrategy' )

    self.connectComboBoxToStackedWidget( 'comboBox_CorrectionStrategy',
                                         'stackedWidget_CorrectionSettings' )

    ############################################################################
    ############################################################################
    # connecting field mapping interface
    ############################################################################
    ############################################################################

    ############################### Bruker #####################################
    # Bruker double echo B0 magnitude
    self.connectStringParameterToLineEdit(
                               'brukerFileNameFirstEchoB0Magnitude',
                               'lineEdit_BrukerFileNameFirstEchoB0Magnitude' )
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit(
                               'pushButton_BrukerFileNameFirstEchoB0Magnitude',
                               'lineEdit_BrukerFileNameFirstEchoB0Magnitude' )
    self._findChild( self._awin,
                     'lineEdit_BrukerFileNameFirstEchoB0Magnitude'
                    ).textChanged.connect(
                                     self.setDwToB0SubSamplingMaximumSizeValue )

    # Bruker B0 phase difference
    self.connectStringParameterToLineEdit(
                               'brukerFileNameB0PhaseDifference',
                               'lineEdit_BrukerFileNameB0PhaseDifference' )
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit(
                               'pushButton_BrukerFileNameB0PhaseDifference',
                               'lineEdit_BrukerFileNameB0PhaseDifferencee' )
    self._findChild( self._awin,
                     'lineEdit_BrukerFileNameB0PhaseDifference'
                    ).textChanged.connect(
                                     self.setDwToB0SubSamplingMaximumSizeValue )

    # Bruker delta echo times
    self.connectDoubleParameterToSpinBox( 'brukerDeltaTE',
                                          'doubleSpinBox_BrukerDeltaTE' )

    ############################### GE #########################################
    # GE double echo B0 magnitude/phase/real/imaginary
    self.connectStringParameterToLineEdit(
                 'geFileNameDoubleEchoB0MagnitudePhaseRealImaginary',
                 'lineEdit_GeFileNameDoubleEchoB0MagnitudePhaseRealImaginary' )
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit(
                 'pushButton_GeFileNameDoubleEchoB0MagnitudePhaseRealImaginary',
                 'lineEdit_GeFileNameDoubleEchoB0MagnitudePhaseRealImaginary' )
    self._findChild( self._awin,
                    'lineEdit_GeFileNameDoubleEchoB0MagnitudePhaseRealImaginary'
                    ).textChanged.connect(
                                     self.setDwToB0SubSamplingMaximumSizeValue )

    # GE delta echo times
    self.connectDoubleParameterToSpinBox( 'geDeltaTE',
                                          'doubleSpinBox_GeDeltaTE' )

    ############################### Philips ####################################
    # Philips first echo B0 magnitude
    self.connectStringParameterToLineEdit(
                               'philipsFileNameFirstEchoB0Magnitude',
                               'lineEdit_PhilipsFileNameFirstEchoB0Magnitude' )
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit(
                               'pushButton_PhilipsFileNameFirstEchoB0Magnitude',
                               'lineEdit_PhilipsFileNameFirstEchoB0Magnitude' )
    self._findChild( self._awin,
                     'lineEdit_PhilipsFileNameFirstEchoB0Magnitude'
                    ).textChanged.connect(
                                     self.setDwToB0SubSamplingMaximumSizeValue )

    # Philips B0 phase difference
    self.connectStringParameterToLineEdit(
                               'philipsFileNameB0PhaseDifference',
                               'lineEdit_PhilipsFileNameB0PhaseDifference' )
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit(
                               'pushButton_PhilipsFileNameB0PhaseDifference',
                               'lineEdit_PhilipsFileNameB0PhaseDifference' )
    self._findChild( self._awin,
                     'lineEdit_PhilipsFileNameB0PhaseDifference'
                    ).textChanged.connect(
                                     self.setDwToB0SubSamplingMaximumSizeValue )

    # Philips delta echo times
    self.connectDoubleParameterToSpinBox( 'philipsDeltaTE',
                                          'doubleSpinBox_PhilipsDeltaTE' )

    ############################### Siemens ####################################
    # Siemens double echo B0 magnitude
    self.connectStringParameterToLineEdit(
                              'siemensFileNameDoubleEchoB0Magnitude',
                              'lineEdit_SiemensFileNameDoubleEchoB0Magnitude' )
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit(
                              'pushButton_SiemensFileNameDoubleEchoB0Magnitude',
                              'lineEdit_SiemensFileNameDoubleEchoB0Magnitude' )
    self._findChild( self._awin,
                     'lineEdit_SiemensFileNameDoubleEchoB0Magnitude'
                    ).textChanged.connect(
                                     self.setDwToB0SubSamplingMaximumSizeValue )

    # Siemens B0 phase difference
    self.connectStringParameterToLineEdit(
                               'siemensFileNameB0PhaseDifference',
                               'lineEdit_SiemensFileNameB0PhaseDifference' )
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit(
                               'pushButton_SiemensFileNameB0PhaseDifference',
                               'lineEdit_SiemensFileNameB0PhaseDifference' )
    self._findChild( self._awin,
                     'lineEdit_SiemensFileNameB0PhaseDifference'
                    ).textChanged.connect(
                                     self.setDwToB0SubSamplingMaximumSizeValue )

    # Siemens delta echo times
    self.connectDoubleParameterToSpinBox( 'siemensDeltaTE',
                                          'doubleSpinBox_SiemensDeltaTE' )


    ############################################################################
    # connecting DW-EPI acquisition parameters interface
    ############################################################################

    ############################### Bruker #####################################
    # partial Fourier factor
    self.connectDoubleParameterToSpinBox(
                                    'brukerPartialFourierFactor',
                                    'doubleSpinBox_BrukerPartialFourierFactor' )

    # parallel acceleration factor
    self.connectIntegerParameterToSpinBox( \
                                    'brukerParallelAccelerationFactor',
                                    'spinBox_BrukerParallelAccelerationFactor' )
                                          
    # echo spacing
    self.connectDoubleParameterToSpinBox( 'brukerEchoSpacing',
                                          'doubleSpinBox_BrukerEchoSpacing' )

    # phase negative sign
    self.connectBooleanParameterToCheckBox( 'brukerPhaseNegativeSign',
                                            'checkBox_BrukerPhaseNegativeSign' )

    ############################### GE #########################################
    # partial Fourier factor
    self.connectDoubleParameterToSpinBox(
                                        'gePartialFourierFactor',
                                        'doubleSpinBox_GePartialFourierFactor' )

    # parallel acceleration factor
    self.connectIntegerParameterToSpinBox( \
                                        'geParallelAccelerationFactor',
                                        'spinBox_GeParallelAccelerationFactor' )
                                          
    # echo spacing
    self.connectDoubleParameterToSpinBox( 'geEchoSpacing',
                                          'doubleSpinBox_GeEchoSpacing' )

    # phase negative sign
    self.connectBooleanParameterToCheckBox( 'gePhaseNegativeSign',
                                            'checkBox_GePhaseNegativeSign' )

    ############################### Philips ####################################
    # partial Fourier factor
    self.connectDoubleParameterToSpinBox(
                                   'philipsPartialFourierFactor',
                                   'doubleSpinBox_PhilipsPartialFourierFactor' )

    # parallel acceleration factor
    self.connectIntegerParameterToSpinBox(
                                   'philipsParallelAccelerationFactor',
                                   'spinBox_PhilipsParallelAccelerationFactor' )
                                          
    # EPI factor (echo train length = EPI factor + 1 )
    self.connectIntegerParameterToSpinBox( 'philipsEPIFactor',
                                           'spinBox_PhilipsEPIFactor' )

    # water fat shift per pixel
    self.connectDoubleParameterToSpinBox( 
                                  'philipsWaterFatShiftPerPixel',
                                  'doubleSpinBox_PhilipsWaterFatShiftPerPixel' )

    # static B0 field
    self.connectDoubleParameterToSpinBox( 'philipsStaticB0Field',
                                          'doubleSpinBox_PhilipsStaticB0Field' )

    # phase negative sign
    self.connectBooleanParameterToCheckBox( 
                                           'philipsPhaseNegativeSign',
                                           'checkBox_PhilipsPhaseNegativeSign' )
                                            
    ############################### Siemens ####################################
    # partial Fourier factor
    self.connectDoubleParameterToSpinBox(
                                   'siemensPartialFourierFactor',
                                   'doubleSpinBox_SiemensPartialFourierFactor' )

    # parallel acceleration factor
    self.connectIntegerParameterToSpinBox( \
                                   'siemensParallelAccelerationFactor',
                                   'spinBox_SiemensParallelAccelerationFactor' )
                                          
    # echo spacing
    self.connectDoubleParameterToSpinBox( 'siemensEchoSpacing',
                                          'doubleSpinBox_SiemensEchoSpacing' )

    # phase negative sign
    self.connectBooleanParameterToCheckBox(
                                           'siemensPhaseNegativeSign',
                                           'checkBox_SiemensPhaseNegativeSign' )
                                            
                                            
    ############################################################################
    # connecting B0 to DW registration advanced parameters
    ############################################################################
    self.connectBooleanParameterToRadioButton( 'generateDwToB0Transformation',
                                    'radioButton_GenerateDwToB0Transformation' )
    self.connectBooleanParameterToRadioButton( 'importDwToB0Transformation',
                                    'radioButton_ImportDwToB0Transformation' )
    self._findChild( self._awin,
                     'radioButton_GenerateDwToB0Transformation'
                    ).toggled.connect(
                                     self.enableDwToB0ImportAndGenerateWidgets )

    self._DwToB0RegistrationAdvancedParameterWidget = \
         self.connectPushButtonToAdvancedParameterWidget( \
         'pushButton_DwToB0Registration',
         ResourceManager().getUIFileName( 'core','RegistrationSettings.ui' ),
         'DwToB0RegistrationParameter' )

    self.connectStringParameterToLineEdit( 'fileNameDwToB0Transformation',
                                       'lineEdit_FileNameDwToB0Transformation' )
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit(
                                      'pushButton_FileNameDwToB0Transformation',
                                      'lineEdit_FileNameDwToB0Transformation' )


    ############################################################################
    ############################################################################
    # connecting Top-Up interface
    ############################################################################
    ############################################################################

    # connecting blip-up reference
    self.connectStringParameterToLineEdit( \
                                        'blipUpReferenceFileName',
                                        'lineEdit_BlipUpReferenceFileName' )
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit(
                                       'pushButton_BlipUpReferenceFileName',
                                       'lineEdit_BlipUpReferenceFileName' )

    # connecting blip-down reference
    self.connectStringParameterToLineEdit( \
                                        'blipDownReferenceFileName',
                                        'lineEdit_BlipDownReferenceFileName' )
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit(
                                       'pushButton_BlipDownReferenceFileName',
                                       'lineEdit_BlipDownReferenceFileName' )

    # connecting blip-up parameters
    self.connectStringParameterToLineEdit( \
                                        'blipUpParameters',
                                        'lineEdit_BlipUpParameters' )

    # connecting blip-down parameters
    self.connectStringParameterToLineEdit( \
                                        'blipDownParameters',
                                        'lineEdit_BlipDownParameters' )
                                       


  def setManufacturer( self, rawDwiDirectory ):


    rawDwiDirectoryForOneSubject = getSingleSubjectPath( rawDwiDirectory )
    print rawDwiDirectoryForOneSubject
    fileNameAcquisitionParameters = os.path.join( \
                                           str( rawDwiDirectoryForOneSubject ),
                                           'acquisition_parameters.py' )
    if ( os.path.exists( fileNameAcquisitionParameters ) ):
    
      print 'reading \'' + fileNameAcquisitionParameters + '\''

      globalVariables = dict()
      localVariables = dict()
      execfile( fileNameAcquisitionParameters, globalVariables, localVariables )
      manufacturer = localVariables[ 'acquisitionParameters' ][ 'manufacturer' ]
      

      stackedWidgetFieldMapping = self._findChild( 
                                                  self._awin,
                                                  'stackedWidget_FieldMapping' )
      stackedWidgetDwEpiAcquisitionParameters = self._findChild( 
                                    self._awin,
                                    'stackedWidget_DwEpiAcquisitionParameters' )

      labelManufacturer = self._findChild( 
                                    self._awin,
                                    'label_Manufacturer' )                
      if ( manufacturer == 'Bruker BioSpin' ):


        labelManufacturer.setText( 'For Bruker BioSpin manufacturer ' \
                                   + '(set by raw DWI directory):' )
        stackedWidgetFieldMapping.setCurrentIndex( 0 )
        stackedWidgetDwEpiAcquisitionParameters.setCurrentIndex( 0 )

      elif ( manufacturer == 'GE HealthCare' ):

        labelManufacturer.setText( 'For GE HealthCare manufacturer ' \
                                   + '(set by raw DWI directory):' )
        stackedWidgetFieldMapping.setCurrentIndex( 1 )
        stackedWidgetDwEpiAcquisitionParameters.setCurrentIndex( 1 )
      
      elif ( manufacturer == 'Philips HealthCare' ):

        labelManufacturer.setText( 'For Philips HealthCare manufacturer ' \
                                   + '(set by raw DWI directory):' )
        stackedWidgetFieldMapping.setCurrentIndex( 2 )
        stackedWidgetDwEpiAcquisitionParameters.setCurrentIndex( 2 )
      
      elif ( manufacturer == 'Siemens HealthCare' ):

        labelManufacturer.setText( 'For Siemens HealthCare manufacturer ' \
                                   + '(set by raw DWI directory):' )
        stackedWidgetFieldMapping.setCurrentIndex( 3 )
        stackedWidgetDwEpiAcquisitionParameters.setCurrentIndex( 3 )



  def setDwToB0SubSamplingMaximumSizeValue( self, fileName ):

    if ( os.path.exists( fileName ) ):

      print 'Check sub-sampling maximum sizes during optimization value...'
      command = 'GkgVolumeInformation' + \
                ' -i ' + fileName + \
                ' -info sizeX ' + \
                ' -verbosePluginLoading false'
      fd = os.popen( str( command ) )
      maxValue = fd.read()[ : -1 ]
      ret = fd.close()

      command = 'GkgVolumeInformation' + \
                ' -i ' + fileName + \
                ' -info sizeY ' + \
                ' -verbosePluginLoading false'
      fd = os.popen( str( command ) )
      value = fd.read()[ : -1 ]
      ret = fd.close()

      if( value > maxValue ):

        value = maxValue
      
      command = 'GkgVolumeInformation' + \
                ' -i ' + fileName + \
                ' -info sizeZ ' + \
                ' -verbosePluginLoading false'
      fd = os.popen( str( command ) )
      value = fd.read()[ : -1 ]
      ret = fd.close()

      if( value > maxValue ):

        value = maxValue

      parameter = self._algorithm.getParameter( 'DwToB0RegistrationParameter' )
      parameterValue = parameter.getValue()
      registrationValue = parameterValue[ 'subSamplingMaximumSizes' ] 
      subSamplingMaximumSizes = str( maxValue )
      if ( registrationValue != subSamplingMaximumSizes ):

        message = 'Sub-sampling maximum sizes during optimization \n' + \
                  'should be maximum size value of B0 file, here ' + \
                  subSamplingMaximumSizes + '.\n' + \
                  'Set this parameter as ' + subSamplingMaximumSizes + ' ?'
        answer = QtGui.QMessageBox.question( None,
                                             'Abort',
                                             message,
                                             QtGui.QMessageBox.Yes | \
                                             QtGui.QMessageBox.No )

        if answer == QtGui.QMessageBox.Yes:

          parameterValue[ 'subSamplingMaximumSizes' ] = subSamplingMaximumSizes
          parameter.setValue( parameterValue )

      else:

        print 'No change in ' + \
              'sub-sampling maximum sizes during optimization value.'


  def enableDwToB0ImportAndGenerateWidgets( self, value ):

    pushButtonDwToB0Registration = self._findChild( self._awin,
                                     'pushButton_DwToB0Registration' )
    lineEditImportDwToB0Transformation = self._findChild( self._awin,
                                     'lineEdit_FileNameDwToB0Transformation' )
    pushButtonImportDwToB0Transformation = self._findChild( self._awin,
                                     'pushButton_FileNameDwToB0Transformation' )
    labelImportDwToB0Transformation = self._findChild( self._awin,
                                     'label_FileNameDwToB0Transformation' )

    if ( value ):

      pushButtonDwToB0Registration.setEnabled( True )
      lineEditImportDwToB0Transformation.setEnabled( False )
      pushButtonImportDwToB0Transformation.setEnabled( False )
      labelImportDwToB0Transformation.setEnabled( False )

    else:

      pushButtonDwToB0Registration.setEnabled( False )
      lineEditImportDwToB0Transformation.setEnabled( True )
      pushButtonImportDwToB0Transformation.setEnabled( True )
      labelImportDwToB0Transformation.setEnabled( True )


