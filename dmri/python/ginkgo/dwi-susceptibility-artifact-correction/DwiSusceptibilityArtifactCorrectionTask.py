from Task import *
import shutil


class DwiSusceptibilityArtifactCorrectionTask( Task ):

  def __init__( self, application ):

    Task.__init__( self, application )


  ##############################################################################
  # execute()
  ##############################################################################

  def execute( self, functors, parameters, subjectName, verbose, viewOnly ):

    try:

      ####################### acquiring conditions #############################
      functors[ 'condition-acquire' ]( subjectName, 'susceptibility-processed' )
      functors[ 'condition-acquire' ]( subjectName, 'corrected-rgb-processed' )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 0 )

      ####################### collecting output work directory #################
      outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()
      if ( verbose ):
      
        print  'Output work directory :', outputWorkDirectory

      ####################### collecting raw DWI directory #####################
      rawDwiDirectory = parameters[ 'rawDwiDirectory' ].getValue()
      if ( verbose ):
      
        print  'Raw DWI directory :', rawDwiDirectory

      ####################### collecting outlier filtered DWI directory ########
      outlierFilteredDwiDirectory = parameters[ \
                                      'outlierFilteredDwiDirectory' ].getValue()
      if ( verbose ):
      
        print  'Outlier filtered DWI directory :', outlierFilteredDwiDirectory

      ####################### collecting raw DWI directory #####################
      roughMaskDirectory = parameters[ 'roughMaskDirectory' ].getValue()
      if ( verbose ):
      
        print  'Rough mask directory :', roughMaskDirectory

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 5 )


      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 30 )

      ####################### reading acq parameters ###########################
      fileNameAcquisitionParameters = os.path.join( rawDwiDirectory,
                                                   'acquisition_parameters.py' )
      if ( verbose ):

        print 'reading \'' + fileNameAcquisitionParameters + '\''

      globalVariables = dict()
      localVariables = dict()
      execfile( fileNameAcquisitionParameters, globalVariables, localVariables )
      sliceAxis = localVariables[ 'acquisitionParameters' ][ 'sliceAxis' ]
      phaseAxis = localVariables[ 'acquisitionParameters' ][ 'phaseAxis' ]
      sizeX = int( localVariables[ 'acquisitionParameters' ][ 'sizeX' ] )
      sizeY = int( localVariables[ 'acquisitionParameters' ][ 'sizeY' ] )
      sizeZ = int( localVariables[ 'acquisitionParameters' ][ 'sizeZ' ] )
      resolutionX = float( \
                   localVariables[ 'acquisitionParameters' ][ 'resolutionX' ] )
      resolutionY = float( \
                   localVariables[ 'acquisitionParameters' ][ 'resolutionY' ] )
      resolutionZ = float( \
                   localVariables[ 'acquisitionParameters' ][ 'resolutionZ' ] )
      qSpacePointCount = int( \
               localVariables[ 'acquisitionParameters' ][ 'qSpacePointCount' ] )
      manufacturer = localVariables[ 'acquisitionParameters' ][ 'manufacturer' ]
      if ( verbose ):
      
        print 'manufacturer :', manufacturer


      ####################### collecting T2(b=0), DW and mask ##################
      fileNameAverageT2 = os.path.join( rawDwiDirectory, 't2' )
      fileNameMultipleT2 = os.path.join( rawDwiDirectory, 't2_multiple' )
      fileNameDW = os.path.join( rawDwiDirectory, 'dw' )
      if ( len( outlierFilteredDwiDirectory ) ):
      
        fileNameAverageT2 = os.path.join( outlierFilteredDwiDirectory,
                                          't2_wo_outlier' )

        if ( os.path.exists( os.path.join( outlierFilteredDwiDirectory,
                           'dw_wo_outlier_and_discarded_orientations.ima' ) ) ):
                           
          fileNameDW = os.path.join( outlierFilteredDwiDirectory,
                                    'dw_wo_outlier_and_discarded_orientations' )
        else:

          fileNameDW = os.path.join( outlierFilteredDwiDirectory,
                                     'dw_wo_outlier' )
      fileNameMask = os.path.join( roughMaskDirectory, 'mask' )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 40 )

      ####################### correcting susceptibility artifacts ##############
      fileNameAverageT2WoSusceptibility = ''
      fileNameDWWoSusceptibility = ''
      
      
      correctionStrategy = parameters[ 'correctionStrategy' ].getChoice()
      
      if ( correctionStrategy == 'Fieldmap-based correction' ):
      
        if ( manufacturer == 'Bruker BioSpin' ):
 
          fileNameAverageT2WoSusceptibility, \
          fileNameDWWoSusceptibility = self.correctBrukerSusceptibility( \
                                            parameters,
                                            functors,
                                            sliceAxis,
                                            phaseAxis,
                                            sizeX,
                                            sizeY,
                                            sizeZ,
                                            resolutionX,
                                            resolutionY,
                                            resolutionZ,
                                            fileNameAverageT2,
                                            fileNameDW,
                                            outputWorkDirectory,
                                            subjectName,
                                            verbose,
                                            viewOnly )

        elif ( manufacturer == 'GE HealthCare' ):

          fileNameAverageT2WoSusceptibility, \
          fileNameDWWoSusceptibility = self.correctGeSusceptibility( \
                                        parameters,
                                        functors,
                                        sliceAxis,
                                        phaseAxis,
                                        sizeX,
                                        sizeY,
                                        sizeZ,
                                        resolutionX,
                                        resolutionY,
                                        resolutionZ,
                                        fileNameAverageT2,
                                        fileNameDW,
                                        outputWorkDirectory,
                                        subjectName,
                                        verbose,
                                        viewOnly )
 
        elif ( manufacturer == 'Philips HealthCare' ):

          fileNameAverageT2WoSusceptibility, \
          fileNameDWWoSusceptibility= self.correctPhilipsSusceptibility( \
                                             parameters,
                                             functors,
                                             sliceAxis,
                                             phaseAxis,
                                             sizeX,
                                             sizeY,
                                             sizeZ,
                                             resolutionX,
                                             resolutionY,
                                             resolutionZ,
                                             fileNameAverageT2,
                                             fileNameDW,
                                             outputWorkDirectory,
                                             subjectName,
                                             verbose,
                                             viewOnly )
 
        elif ( manufacturer == 'Siemens HealthCare' ):

          fileNameAverageT2WoSusceptibility, \
          fileNameDWWoSusceptibility= self.correctSiemensSusceptibility( \
                                             parameters,
                                             functors,
                                             sliceAxis,
                                             phaseAxis,
                                             sizeX,
                                             sizeY,
                                             sizeZ,
                                             resolutionX,
                                             resolutionY,
                                             resolutionZ,
                                             fileNameAverageT2,
                                             fileNameDW,
                                             outputWorkDirectory,
                                             subjectName,
                                             verbose,
                                             viewOnly )
                                             
      else:

        fileNameAverageT2WoSusceptibility, \
        fileNameDWWoSusceptibility= self.correctSusceptibilityUsingTopUp( \
                                             parameters,
                                             functors,
                                             sliceAxis,
                                             phaseAxis,
                                             sizeX,
                                             sizeY,
                                             sizeZ,
                                             qSpacePointCount,
                                             resolutionX,
                                             resolutionY,
                                             resolutionZ,
                                             fileNameAverageT2,
                                             fileNameDW,
                                             outputWorkDirectory,
                                             subjectName,
                                             verbose,
                                             viewOnly )
                                             

      ####################### notifying display thread #########################
      functors[ 'condition-notify-and-release' ]( subjectName,
                                                  'susceptibility-processed' )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 90 )

      ########################### computing uncorrected RGB map ################
      self.createRGB( fileNameAverageT2WoSusceptibility,
                      fileNameDWWoSusceptibility,
                      fileNameMask,
                      outputWorkDirectory,
                      subjectName,
                      verbose,
                      viewOnly )
                 
      ####################### notifying display thread #########################
      functors[ 'condition-notify-and-release' ]( subjectName,
                                                  'corrected-rgb-processed' )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 100 )

    except:

      self._functors[ 'task-set-status-as-failed' ]( subjectName,
                                                     sys.exc_info() )


  ##############################################################################
  # Bruker correction
  ##############################################################################

  def correctBrukerSusceptibility( self, 
                                   parameters,
                                   functors,
                                   sliceAxis,
                                   phaseAxis,
                                   sizeX,
                                   sizeY,
                                   sizeZ,
                                   resolutionX,
                                   resolutionY,
                                   resolutionZ,
                                   fileNameAverageT2,
                                   fileNameDW,
                                   outputWorkDirectory,
                                   subjectName,
                                   verbose,
                                   viewOnly ):

    # collecting informations from parameters
    fileNameFirstEchoB0Magnitude = parameters[ \
                               'brukerFileNameFirstEchoB0Magnitude' ].getValue()
    fileNameB0PhaseDifference = parameters[ \
                                  'brukerFileNameB0PhaseDifference' ].getValue()

    epiPhaseSize = 0
    if ( phaseAxis == 'x' ):

       epiPhaseSize = sizeX
      
    elif ( phaseAxis == 'y' ):

       epiPhaseSize = sizeY
      
    elif ( phaseAxis == 'z' ):

       epiPhaseSize = sizeZ

    deltaTE = parameters[ 'brukerDeltaTE' ].getValue()
    partialFourierFactor = parameters[ 'brukerPartialFourierFactor' ].getValue()
    echoSpacing = parameters[ 'brukerEchoSpacing' ].getValue()
    parallelAccelerationFactor = parameters[ \
                                 'brukerParallelAccelerationFactor' ].getValue()
    phaseNegativeSignValue = parameters[ 'brukerPhaseNegativeSign' ].getValue()
    phaseNegativeSign = 1.0
    if ( phaseNegativeSignValue == 2 ):

      phaseNegativeSign = -1.0

    if ( verbose ):

      print 'susceptibility artifact correction'
      print '----------------------------------'
      print 'slice axis : ', sliceAxis
      print 'phase axis : ', phaseAxis
      print 'TE2 - TE1 : ', deltaTE, ' ms'
      print 'partial Fourier factor : ', partialFourierFactor
      print 'echo spacing : ', echoSpacing, ' ms'
      print 'TE2 - TE1 : ', deltaTE
      print 'phase sign : ', phaseNegativeSign

    # extracting first echo of magnitude image
    fileNameFirstEchoB0MagnitudeGis = os.path.join( outputWorkDirectory,
                                                    'b0_magnitude_echo1_gis' )
    command = 'GkgExecuteCommand Nifti2GisConverter ' + \
              ' -i ' + fileNameFirstEchoB0Magnitude + \
              ' -o ' + fileNameFirstEchoB0MagnitudeGis + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )

    fileNameFirstEchoB0Magnitude = os.path.join( outputWorkDirectory,
                                                 'b0_magnitude_echo1' )
    command = 'GkgExecuteCommand Combiner ' + \
              ' -i ' + fileNameFirstEchoB0MagnitudeGis + \
              ' -o ' + fileNameFirstEchoB0Magnitude + \
              ' -num1 1 0 -den1 2 1 -t int16_t ' + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )


    # converting phase map
    fileNameWrappedB0PhaseDifferenceGis = os.path.join( outputWorkDirectory,
                                                        'b0_phase_wrapped_gis' )
    command = 'GkgExecuteCommand Nifti2GisConverter ' + \
              ' -i ' + fileNameB0PhaseDifference + \
              ' -o ' + fileNameWrappedB0PhaseDifferenceGis + \
              ' -verbose true'

    executeCommand( self, subjectName, command, viewOnly )

    fileNameWrappedB0PhaseDifference = os.path.join( outputWorkDirectory,
                                                     'b0_phase_wrapped' )
    command = 'GkgExecuteCommand SubVolume ' + \
              ' -i ' + fileNameWrappedB0PhaseDifferenceGis + \
              ' -o ' + fileNameWrappedB0PhaseDifference + \
              ' -t 0 -T 0 ' + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )

    # unwrapping phase map
    fileNameUnwrappedB0PhaseDifference = os.path.join( outputWorkDirectory,
                                                       'b0_phase_unwrapped' )
    fileNameB0QualityMap = os.path.join( outputWorkDirectory,
                                         'b0_quality_map' )
    command = 'GkgExecuteCommand PhaseUnwrapper ' + \
              ' -i ' + fileNameWrappedB0PhaseDifference + \
              ' -o ' + fileNameUnwrappedB0PhaseDifference + \
              ' -q ' + fileNameB0QualityMap + \
              ' -offset 0.0 ' + \
              ' -scale 1.0 ' + \
              ' -plane ' + sliceAxis + \
              ' -cycleCount 2 ' + \
              ' -coarsestSize 3 ' + \
              ' -preSmoothingIterationCount 2 ' + \
              ' -postSmoothingIterationCount 2 ' + \
              ' -useCongruence true ' + \
              ' -qualityThreshold 0.9 ' + \
              ' -removeRamp false ' + \
              ' -removeBias false ' + \
              ' -zUnwrappingType 1 ' + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )

    # matching T2(b=0) and B0 magnitude echo1 volume(s)
    fileNameDwToB0Transform3d, \
    fileNameB0ToDwTransform3d = self.matchB0AndT2( parameters,
                                                   outputWorkDirectory,
                                                   fileNameFirstEchoB0Magnitude,
                                                   fileNameAverageT2,
                                                   subjectName,
                                                   verbose,
                                                   viewOnly )

    # resampling unwrapped phase difference map
    fileNameResampledUnwrappedB0PhaseDifference = os.path.join( \
                                                outputWorkDirectory,
                                                'b0_phase_unwrapped_resampled' )
    command = 'GkgExecuteCommand Resampling3d ' + \
              ' -reference ' + fileNameUnwrappedB0PhaseDifference + \
              ' -size ' + str( sizeX ) + ' ' + \
                          str( sizeY ) + ' ' + \
                          str( sizeZ ) + ' ' + \
              ' -resolution ' + str( resolutionX ) + ' ' + \
                                str( resolutionY ) + ' ' + \
                                str( resolutionZ ) + ' ' + \
              ' -transforms ' + fileNameB0ToDwTransform3d + \
              ' -output ' + fileNameResampledUnwrappedB0PhaseDifference + \
              ' -order 3 ' + \
              ' -background 0 ' + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )

    # computing phase to pixel factor
    phaseToPixelFactor = ( float( epiPhaseSize ) * partialFourierFactor / \
                           float( parallelAccelerationFactor ) ) * \
                         ( echoSpacing / ( 2 * 3.14159265359 * deltaTE ) ) * \
                         phaseNegativeSign
    if ( verbose ):

      print 'phase to pixel factor : ', phaseToPixelFactor
      
    # correcting geometrical distortions of T2 due to susceptibility effects
    fileNameAverageT2WoSusceptibility = os.path.join( outputWorkDirectory,
                                                      't2_wo_susceptibility' )
    fileNameSusceptibilityShiftMap = os.path.join( outputWorkDirectory,
                                                   'susceptibility_shift_map' )
    command = 'GkgExecuteCommand B0InhomogeneityCorrection ' + \
              ' -i ' + fileNameAverageT2 + \
              ' -p ' + fileNameResampledUnwrappedB0PhaseDifference + \
              ' -o ' + fileNameAverageT2WoSusceptibility + \
              ' -s ' + fileNameSusceptibilityShiftMap + \
              ' -phaseAxis ' + phaseAxis + \
              ' -parameters ' + str( phaseToPixelFactor ) + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )

    # correcting geometrical distortions of DW due to susceptibility effects
    fileNameDWWoSusceptibility = os.path.join( outputWorkDirectory,
                                               'dw_wo_susceptibility' )
    command = 'GkgExecuteCommand B0InhomogeneityCorrection ' + \
              ' -i ' + fileNameDW + \
              ' -p ' + fileNameResampledUnwrappedB0PhaseDifference + \
              ' -o ' + fileNameDWWoSusceptibility + \
              ' -phaseAxis ' + phaseAxis + \
              ' -parameters ' + str( phaseToPixelFactor ) + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )

    return fileNameAverageT2WoSusceptibility, fileNameDWWoSusceptibility


  ##############################################################################
  # GE HealtCare correction
  ##############################################################################

  def correctGeSusceptibility( self, 
                               parameters,
                               functors,
                               sliceAxis,
                               phaseAxis,
                               sizeX,
                               sizeY,
                               sizeZ,
                               resolutionX,
                               resolutionY,
                               resolutionZ,
                               fileNameAverageT2,
                               fileNameDW,
                               outputWorkDirectory,
                               subjectName,
                               verbose,
                               viewOnly ):

    # collecting informations from parameters
    fileNameDoubleEchoB0MagnitudePhaseRealImaginary = parameters[ \
                'geFileNameDoubleEchoB0MagnitudePhaseRealImaginary' ].getValue()
    
    epiPhaseSize = 0
    if ( phaseAxis == 'x' ):

       epiPhaseSize = sizeX
      
    elif ( phaseAxis == 'y' ):

       epiPhaseSize = sizeY
      
    elif ( phaseAxis == 'z' ):

       epiPhaseSize = sizeZ

    deltaTE = parameters[ 'geDeltaTE' ].getValue()
    partialFourierFactor = parameters[ 'gePartialFourierFactor' ].getValue()
    echoSpacing = parameters[ 'geEchoSpacing' ].getValue()
    parallelAccelerationFactor = parameters[ \
                                     'geParallelAccelerationFactor' ].getValue()
    phaseNegativeSignValue = parameters[ 'gePhaseNegativeSign' ].getValue()
    phaseNegativeSign = 1.0
    if ( phaseNegativeSignValue == 2 ):

      phaseNegativeSign = -1.0

    if ( verbose ):

      print 'susceptibility artifact correction'
      print '----------------------------------'
      print 'slice axis : ', sliceAxis
      print 'phase axis : ', phaseAxis
      print 'TE2 - TE1 : ', deltaTE, ' ms'
      print 'partial Fourier factor : ', partialFourierFactor
      print 'echo spacing : ', echoSpacing, ' ms'
      print 'TE2 - TE1 : ', deltaTE
      print 'phase sign : ', phaseNegativeSign

    # extracting first echo of magnitude image
    fileNameFirstEchoB0Magnitude = os.path.join( outputWorkDirectory,
                                                 'b0_magnitude_echo1' )
    command = 'GkgExecuteCommand SubVolume ' + \
              ' -i ' + fileNameDoubleEchoB0MagnitudePhaseRealImaginary + \
              ' -o ' + fileNameFirstEchoB0Magnitude + \
              ' -t 0 -T 0 ' + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )
        
    # extracting second echo of magnitude image
    fileNameSecondEchoB0Magnitude = os.path.join( outputWorkDirectory,
                                                  'b0_magnitude_echo2' )
    command = 'GkgExecuteCommand SubVolume ' + \
              ' -i ' + fileNameDoubleEchoB0MagnitudePhaseRealImaginary + \
              ' -o ' + fileNameSecondEchoB0Magnitude + \
              ' -t 4 -T 4 ' + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )       

    # converting phase map
    fileNameFirstEchoB0PhaseShort = os.path.join( outputWorkDirectory, \
                                                  'b0_phase_echo1_short' )
    command = 'GkgExecuteCommand SubVolume ' + \
              ' -i ' + fileNameDoubleEchoB0MagnitudePhaseRealImaginary + \
              ' -o ' + fileNameFirstEchoB0PhaseShort + \
              ' -t 1 -T 1 ' + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )

    fileNameSecondEchoB0PhaseShort = os.path.join( outputWorkDirectory, \
                                                   'b0_phase_echo2_short' )
    command = 'GkgExecuteCommand SubVolume ' + \
              ' -i ' + fileNameDoubleEchoB0MagnitudePhaseRealImaginary + \
              ' -o ' + fileNameSecondEchoB0PhaseShort + \
              ' -t 1 -T 1 ' + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )

    fileNameWrappedB0PhaseDifference = os.path.join( outputWorkDirectory,
                                                     'b0_phase_wrapped' )
    command = 'GkgExecuteCommand Combiner ' + \
              ' -i ' + fileNameSecondEchoB0PhaseShort + ' ' + \
                       fileNameFirstEchoB0PhaseShort + \
              ' -o ' + fileNameWrappedB0PhaseDifference + \
              ' -num1 1.0 -1.0 0.0 ' + \
              ' -den1 1000.0 1000.0 1000.0 ' + \
              ' -t float ' + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )

    # unwrapping phase map
    fileNameUnwrappedB0PhaseDifference = os.path.join( outputWorkDirectory,
                                                       'b0_phase_unwrapped' )
    fileNameB0QualityMap = os.path.join( outputWorkDirectory,
                                         'b0_quality_map' )
    command = 'GkgExecuteCommand PhaseUnwrapper ' + \
              ' -i ' + fileNameWrappedB0PhaseDifference + \
              ' -o ' + fileNameUnwrappedB0PhaseDifference + \
              ' -q ' + fileNameB0QualityMap + \
              ' -offset 0.0 ' + \
              ' -scale 1.0 ' + \
              ' -plane ' + sliceAxis + \
              ' -cycleCount 2 ' + \
              ' -coarsestSize 3 ' + \
              ' -preSmoothingIterationCount 2 ' + \
              ' -postSmoothingIterationCount 2 ' + \
              ' -useCongruence true ' + \
              ' -qualityThreshold 0.9 ' + \
              ' -removeRamp false ' + \
              ' -removeBias false ' + \
              ' -zUnwrappingType 1 ' + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )

    # matching T2(b=0) and B0 magnitude echo1 volume(s)
    fileNameDwToB0Transform3d, \
    fileNameB0ToDwTransform3d = self.matchB0AndT2( parameters,
                                                   outputWorkDirectory,
                                                   fileNameFirstEchoB0Magnitude,
                                                   fileNameAverageT2,
                                                   subjectName,
                                                   verbose,
                                                   viewOnly )

    # resampling unwrapped phase difference map
    fileNameResampledUnwrappedB0PhaseDifference = os.path.join( \
                                                outputWorkDirectory,
                                                'b0_phase_unwrapped_resampled' )
    command = 'GkgExecuteCommand Resampling3d ' + \
              ' -reference ' + fileNameUnwrappedB0PhaseDifference + \
              ' -size ' + str( sizeX ) + ' ' + \
                          str( sizeY ) + ' ' + \
                          str( sizeZ ) + ' ' + \
              ' -resolution ' + str( resolutionX ) + ' ' + \
                                str( resolutionY ) + ' ' + \
                                str( resolutionZ ) + ' ' + \
              ' -transforms ' + fileNameB0ToDwTransform3d + \
              ' -output ' + fileNameResampledUnwrappedB0PhaseDifference + \
              ' -order 3 ' + \
              ' -background 0 ' + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )

    # computing phase to pixel factor
    phaseToPixelFactor = ( float( epiPhaseSize ) * partialFourierFactor / \
                           float( parallelAccelerationFactor ) ) * \
                         ( echoSpacing / ( 2 * 3.14159265359 * deltaTE ) ) * \
                         phaseNegativeSign
    if ( verbose ):

      print 'phase to pixel factor : ', phaseToPixelFactor
      
    # correcting geometrical distortions of T2 due to susceptibility effects
    fileNameAverageT2WoSusceptibility = os.path.join( outputWorkDirectory,
                                                      't2_wo_susceptibility' )
    fileNameSusceptibilityShiftMap = os.path.join( outputWorkDirectory,
                                                   'susceptibility_shift_map' )
    command = 'GkgExecuteCommand B0InhomogeneityCorrection ' + \
              ' -i ' + fileNameAverageT2 + \
              ' -p ' + fileNameResampledUnwrappedB0PhaseDifference + \
              ' -o ' + fileNameAverageT2WoSusceptibility + \
              ' -s ' + fileNameSusceptibilityShiftMap + \
              ' -phaseAxis ' + phaseAxis + \
              ' -parameters ' + str( phaseToPixelFactor ) + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )

    # correcting geometrical distortions of DW due to susceptibility effects
    fileNameDWWoSusceptibility = os.path.join( outputWorkDirectory,
                                               'dw_wo_susceptibility' )
    command = 'GkgExecuteCommand B0InhomogeneityCorrection ' + \
              ' -i ' + fileNameDW + \
              ' -p ' + fileNameResampledUnwrappedB0PhaseDifference + \
              ' -o ' + fileNameDWWoSusceptibility + \
              ' -phaseAxis ' + phaseAxis + \
              ' -parameters ' + str( phaseToPixelFactor ) + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )

    return fileNameAverageT2WoSusceptibility, fileNameDWWoSusceptibility


  ##############################################################################
  # Philips HealthCare correction
  ##############################################################################

  def correctPhilipsSusceptibility( self, 
                                    parameters,
                                    functors,
                                    sliceAxis,
                                    phaseAxis,
                                    sizeX,
                                    sizeY,
                                    sizeZ,
                                    resolutionX,
                                    resolutionY,
                                    resolutionZ,
                                    fileNameAverageT2,
                                    fileNameDW,
                                    outputWorkDirectory,
                                    subjectName,
                                    verbose,
                                    viewOnly ):

    # collecting informations from parameters
    fileNameFirstEchoB0Magnitude = parameters[ \
                              'philipsFileNameFirstEchoB0Magnitude' ].getValue()
    fileNameB0PhaseDifference = parameters[ \
                                 'philipsFileNameB0PhaseDifference' ].getValue()
    
    epiPhaseSize = 0
    if ( phaseAxis == 'x' ):

       epiPhaseSize = sizeX
      
    elif ( phaseAxis == 'y' ):

       epiPhaseSize = sizeY
      
    elif ( phaseAxis == 'z' ):

       epiPhaseSize = sizeZ

    deltaTE = parameters[ 'philipsDeltaTE' ].getValue()
    partialFourierFactor = parameters[ \
                                      'philipsPartialFourierFactor' ].getValue()
    epiFactor = parameters[ 'philipsEPIFactor' ].getValue()
    waterFatShiftPerPixel = parameters[ \
                                     'philipsWaterFatShiftPerPixel' ].getValue()
    staticB0Field = parameters[ 'philipsStaticB0Field' ].getValue()
    echoTrainLength = epiFactor + 1
    echoSpacing = ( 1000.0 * float( waterFatShiftPerPixel ) ) / \
                  ( staticB0Field * 3.35 * 42.576 * float( echoTrainLength ) )
    parallelAccelerationFactor = parameters[ \
                                'philipsParallelAccelerationFactor' ].getValue()
    phaseNegativeSignValue = parameters[ 'philipsPhaseNegativeSign' ].getValue()
    phaseNegativeSign = 1.0
    if ( phaseNegativeSignValue == 2 ):

      phaseNegativeSign = -1.0

    if ( verbose ):

      print 'susceptibility artifact correction'
      print '----------------------------------'
      print 'slice axis : ', sliceAxis
      print 'phase axis : ', phaseAxis
      print 'TE2 - TE1 : ', deltaTE, ' ms'
      print 'partial Fourier factor : ', partialFourierFactor
      print 'echo spacing : ', echoSpacing, ' ms'
      print 'TE2 - TE1 : ', deltaTE
      print 'phase sign : ', phaseNegativeSign

    # extracting first echo of magnitude image
    fileNameFirstEchoB0MagnitudeBis = os.path.join( outputWorkDirectory,
                                                    'b0_magnitude_echo1' )
    command = 'GkgExecuteCommand SubVolume ' + \
              ' -i ' + fileNameFirstEchoB0Magnitude + \
              ' -o ' + fileNameFirstEchoB0MagnitudeBis + \
              ' -t 0 -T 0 ' + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )

    fileNameFirstEchoB0MagnitudeMask = os.path.join( outputWorkDirectory,
                                                     'b0_magnitude_echo1_mask' )
    command = 'GkgExecuteCommand GetMask ' + \
              ' -i ' + fileNameFirstEchoB0MagnitudeBis + \
              ' -o ' + fileNameFirstEchoB0MagnitudeMask + \
              ' -a 2 ' + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )

    # converting phase difference map
    fileNameWrappedB0PhaseDifference = os.path.join( outputWorkDirectory,
                                                     'b0_phase_wrapped' )
    command = 'GkgExecuteCommand PhilipsPhaseMapConverter ' + \
              ' -i ' + fileNameB0PhaseDifference + \
              ' -o ' + fileNameWrappedB0PhaseDifference + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )

    # unwrapping phase map
    fileNameUnwrappedB0PhaseDifference = os.path.join( outputWorkDirectory,
                                                       'b0_phase_unwrapped' )
    fileNameB0QualityMap = os.path.join( outputWorkDirectory,
                                         'b0_quality_map' )
    command = 'GkgExecuteCommand PhaseUnwrapper ' + \
              ' -i ' + fileNameWrappedB0PhaseDifference + \
              ' -o ' + fileNameUnwrappedB0PhaseDifference + \
              ' -q ' + fileNameB0QualityMap + \
              ' -offset 0.0 ' + \
              ' -scale 1.0 ' + \
              ' -plane ' + sliceAxis + \
              ' -cycleCount 2 ' + \
              ' -coarsestSize 3 ' + \
              ' -preSmoothingIterationCount 2 ' + \
              ' -postSmoothingIterationCount 2 ' + \
              ' -useCongruence true ' + \
              ' -qualityThreshold 0.9 ' + \
              ' -removeRamp true ' + \
              ' -removeBias false ' + \
              ' -zUnwrappingType 1 ' + \
              ' -m ' + fileNameFirstEchoB0MagnitudeMask + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )


    # matching T2(b=0) and B0 magnitude echo1 volume(s)
    fileNameDwToB0Transform3d, \
    fileNameB0ToDwTransform3d = self.matchB0AndT2( parameters,
                                                   outputWorkDirectory,
                                                   fileNameFirstEchoB0Magnitude,
                                                   fileNameAverageT2,
                                                   subjectName,
                                                   verbose,
                                                   viewOnly )

    # resampling unwrapped phase difference map
    fileNameResampledUnwrappedB0PhaseDifference = os.path.join( \
                                                outputWorkDirectory,
                                                'b0_phase_unwrapped_resampled' )
    command = 'GkgExecuteCommand Resampling3d ' + \
              ' -reference ' + fileNameUnwrappedB0PhaseDifference + \
              ' -size ' + str( sizeX ) + ' ' + \
                          str( sizeY ) + ' ' + \
                          str( sizeZ ) + ' ' + \
              ' -resolution ' + str( resolutionX ) + ' ' + \
                                str( resolutionY ) + ' ' + \
                                str( resolutionZ ) + ' ' + \
              ' -transforms ' + fileNameB0ToDwTransform3d + \
              ' -output ' + fileNameResampledUnwrappedB0PhaseDifference + \
              ' -order 3 ' + \
              ' -background 0 ' + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )

    # computing phase to pixel factor
    phaseToPixelFactor = ( float( epiPhaseSize ) * partialFourierFactor / \
                           float( parallelAccelerationFactor ) ) * \
                         ( echoSpacing / ( 2 * 3.14159265359 * deltaTE ) ) * \
                         phaseNegativeSign
    if ( verbose ):

      print 'phase to pixel factor : ', phaseToPixelFactor
      
    # correcting geometrical distortions of T2 due to susceptibility effects
    fileNameAverageT2WoSusceptibility = os.path.join( outputWorkDirectory,
                                                      't2_wo_susceptibility' )
    fileNameSusceptibilityShiftMap = os.path.join( outputWorkDirectory,
                                                   'susceptibility_shift_map' )
    command = 'GkgExecuteCommand B0InhomogeneityCorrection ' + \
              ' -i ' + fileNameAverageT2 + \
              ' -p ' + fileNameResampledUnwrappedB0PhaseDifference + \
              ' -o ' + fileNameAverageT2WoSusceptibility + \
              ' -s ' + fileNameSusceptibilityShiftMap + \
              ' -phaseAxis ' + phaseAxis + \
              ' -parameters ' + str( phaseToPixelFactor ) + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )

    # correcting geometrical distortions of DW due to susceptibility effects
    fileNameDWWoSusceptibility = os.path.join( outputWorkDirectory,
                                               'dw_wo_susceptibility' )
    command = 'GkgExecuteCommand B0InhomogeneityCorrection ' + \
              ' -i ' + fileNameDW + \
              ' -p ' + fileNameResampledUnwrappedB0PhaseDifference + \
              ' -o ' + fileNameDWWoSusceptibility + \
              ' -phaseAxis ' + phaseAxis + \
              ' -parameters ' + str( phaseToPixelFactor ) + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )

    return fileNameAverageT2WoSusceptibility, fileNameDWWoSusceptibility


  ##############################################################################
  # Siemens HealtCare correction
  ##############################################################################

  def correctSiemensSusceptibility( self, 
                                    parameters,
                                    functors,
                                    sliceAxis,
                                    phaseAxis,
                                    sizeX,
                                    sizeY,
                                    sizeZ,
                                    resolutionX,
                                    resolutionY,
                                    resolutionZ,
                                    fileNameAverageT2,
                                    fileNameDW,
                                    outputWorkDirectory,
                                    subjectName,
                                    verbose,
                                    viewOnly ):

    # collecting informations from parameters
    fileNameDoubleEchoB0Magnitude = parameters[ \
                             'siemensFileNameDoubleEchoB0Magnitude' ].getValue()
    fileNameB0PhaseDifference = parameters[ \
                                 'siemensFileNameB0PhaseDifference' ].getValue()
   
    epiPhaseSize = 0
    if ( phaseAxis == 'x' ):

       epiPhaseSize = sizeX
      
    elif ( phaseAxis == 'y' ):

       epiPhaseSize = sizeY
      
    elif ( phaseAxis == 'z' ):

       epiPhaseSize = sizeZ

    deltaTE = parameters[ 'siemensDeltaTE' ].getValue()
    partialFourierFactor = parameters[ \
                                      'siemensPartialFourierFactor' ].getValue()
    echoSpacing = parameters[ 'siemensEchoSpacing' ].getValue()
    parallelAccelerationFactor = parameters[ \
                                'siemensParallelAccelerationFactor' ].getValue()
    phaseNegativeSignValue = parameters[ 'siemensPhaseNegativeSign' ].getValue()
    phaseNegativeSign = 1.0
    if ( phaseNegativeSignValue == 2 ):

      phaseNegativeSign = -1.0

    if ( verbose ):

      print 'susceptibility artifact correction'
      print '----------------------------------'
      print 'slice axis : ', sliceAxis
      print 'phase axis : ', phaseAxis
      print 'TE2 - TE1 : ', deltaTE, ' ms'
      print 'partial Fourier factor : ', partialFourierFactor
      print 'echo spacing : ', echoSpacing, ' ms'
      print 'TE2 - TE1 : ', deltaTE
      print 'phase sign : ', phaseNegativeSign

    # extracting first echo of magnitude image
    fileNameFirstEchoB0Magnitude = os.path.join( outputWorkDirectory,
                                                 'b0_magnitude_echo1' )
    command = 'GkgExecuteCommand SubVolume ' + \
              ' -i ' + fileNameDoubleEchoB0Magnitude + \
              ' -o ' + fileNameFirstEchoB0Magnitude + \
              ' -t 0 -T 0 ' + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )    

    # extracting second echo of magnitude image 
    fileNameSecondEchoB0Magnitude = os.path.join( outputWorkDirectory,
                                                  'b0_magnitude_echo2' )
    command = 'GkgExecuteCommand SubVolume ' + \
              ' -i ' + fileNameDoubleEchoB0Magnitude + \
              ' -o ' + fileNameSecondEchoB0Magnitude + \
              ' -t 1 -T 1 ' + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )
        
    # converting phase difference map
    fileNameWrappedB0PhaseDifference = os.path.join( outputWorkDirectory,
                                                     'b0_phase_wrapped' )
    command = 'GkgExecuteCommand SiemensPhaseMapConverter ' + \
              ' -i ' + fileNameB0PhaseDifference + \
              ' -o ' + fileNameWrappedB0PhaseDifference + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )

    # unwrapping phase map
    fileNameUnwrappedB0PhaseDifference = os.path.join( outputWorkDirectory,
                                                       'b0_phase_unwrapped' )
    fileNameB0QualityMap = os.path.join( outputWorkDirectory,
                                         'b0_quality_map' )
    command = 'GkgExecuteCommand PhaseUnwrapper ' + \
              ' -i ' + fileNameWrappedB0PhaseDifference + \
              ' -o ' + fileNameUnwrappedB0PhaseDifference + \
              ' -q ' + fileNameB0QualityMap + \
              ' -offset 0.0 ' + \
              ' -scale 1.0 ' + \
              ' -plane ' + sliceAxis + \
              ' -cycleCount 2 ' + \
              ' -coarsestSize 3 ' + \
              ' -preSmoothingIterationCount 2 ' + \
              ' -postSmoothingIterationCount 2 ' + \
              ' -useCongruence true ' + \
              ' -qualityThreshold 0.9 ' + \
              ' -removeRamp false ' + \
              ' -removeBias false ' + \
              ' -zUnwrappingType 1 ' + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )

    # matching T2(b=0) and B0 magnitude echo1 volume(s)
    fileNameDwToB0Transform3d, \
    fileNameB0ToDwTransform3d = self.matchB0AndT2( parameters,
                                                   outputWorkDirectory,
                                                   fileNameFirstEchoB0Magnitude,
                                                   fileNameAverageT2,
                                                   subjectName,
                                                   verbose,
                                                   viewOnly )

    # resampling unwrapped phase difference map
    fileNameResampledUnwrappedB0PhaseDifference = os.path.join( \
                                                outputWorkDirectory,
                                                'b0_phase_unwrapped_resampled' )
    command = 'GkgExecuteCommand Resampling3d ' + \
              ' -reference ' + fileNameUnwrappedB0PhaseDifference + \
              ' -size ' + str( sizeX ) + ' ' + \
                          str( sizeY ) + ' ' + \
                          str( sizeZ ) + ' ' + \
              ' -resolution ' + str( resolutionX ) + ' ' + \
                                str( resolutionY ) + ' ' + \
                                str( resolutionZ ) + ' ' + \
              ' -transforms ' + fileNameB0ToDwTransform3d + \
              ' -output ' + fileNameResampledUnwrappedB0PhaseDifference + \
              ' -order 3 ' + \
              ' -background 0 ' + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )

    # computing phase to pixel factor
    phaseToPixelFactor = ( float( epiPhaseSize ) * partialFourierFactor / \
                           float( parallelAccelerationFactor ) ) * \
                         ( echoSpacing / ( 2 * 3.14159265359 * deltaTE ) ) * \
                         phaseNegativeSign
    if ( verbose ):

      print 'phase to pixel factor : ', phaseToPixelFactor
      
    # correcting geometrical distortions of T2 due to susceptibility effects
    fileNameAverageT2WoSusceptibility = os.path.join( outputWorkDirectory,
                                                      't2_wo_susceptibility' )
    fileNameSusceptibilityShiftMap = os.path.join( outputWorkDirectory,
                                                   'susceptibility_shift_map' )
    command = 'GkgExecuteCommand B0InhomogeneityCorrection ' + \
              ' -i ' + fileNameAverageT2 + \
              ' -p ' + fileNameResampledUnwrappedB0PhaseDifference + \
              ' -o ' + fileNameAverageT2WoSusceptibility + \
              ' -s ' + fileNameSusceptibilityShiftMap + \
              ' -phaseAxis ' + phaseAxis + \
              ' -parameters ' + str( phaseToPixelFactor ) + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )

    # correcting geometrical distortions of DW due to susceptibility effects
    fileNameDWWoSusceptibility = os.path.join( outputWorkDirectory,
                                               'dw_wo_susceptibility' )
    command = 'GkgExecuteCommand B0InhomogeneityCorrection ' + \
              ' -i ' + fileNameDW + \
              ' -p ' + fileNameResampledUnwrappedB0PhaseDifference + \
              ' -o ' + fileNameDWWoSusceptibility + \
              ' -phaseAxis ' + phaseAxis + \
              ' -parameters ' + str( phaseToPixelFactor ) + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )
    
    return fileNameAverageT2WoSusceptibility, fileNameDWWoSusceptibility


  ##############################################################################
  # macthing B0 and T2(b=0)
  ##############################################################################

  def matchB0AndT2( self,
                    parameters,
                    outputWorkDirectory,
                    fileNameB0MagnitudeEcho1,
                    fileNameAverageT2,
                    subjectName,
                    verbose,
                    viewOnly ):


    # matching T2(b=0) and B0 magnitude echo1 volume(s)
    fileNameB0ToDwTransform3d = os.path.join( outputWorkDirectory,
                                              'b0_to_dw.trm' )
    fileNameDwToB0Transform3d = os.path.join( outputWorkDirectory,
                                              'dw_to_b0.trm' )
    if ( parameters[ 'generateDwToB0Transformation' ].getValue() ):

      DwToB0RegistrationParameters = parameters[ \
                                      'DwToB0RegistrationParameter' ].getValue()

      similarityMeasureName = str( parameters[ \
          'DwToB0RegistrationParameter' ].getChoice( 'similarityMeasureName' ) )
      optimizerName = str( parameters[ \
         'DwToB0RegistrationParameter' ].getChoice( 'optimizerName' ) )
      transform3DType = str( parameters[ \
                'DwToB0RegistrationParameter' ].getChoice( 'transform3DType' ) )
      referenceLowerThreshold = \
                       DwToB0RegistrationParameters[ 'referenceLowerThreshold' ]
      floatingLowerThreshold = \
                       DwToB0RegistrationParameters[ 'floatingLowerThreshold' ]
      resamplingOrder = DwToB0RegistrationParameters[ 'resamplingOrder' ]
      subSamplingMaximumSizes = \
                       DwToB0RegistrationParameters[ 'subSamplingMaximumSizes' ]
      levelCount = DwToB0RegistrationParameters[ 'levelCount' ]
      applySmoothing = DwToB0RegistrationParameters[ 'applySmoothing' ]
      maximumIterationCount = + \
                       DwToB0RegistrationParameters[ 'maximumIterationCount' ]
      initializeCoefficientsUsingCenterOfGravity = \
                      DwToB0RegistrationParameters[ \
                                  'initializeCoefficientsUsingCenterOfGravity' ]
      initialParametersScalingX = DwToB0RegistrationParameters[ \
                                  'initialParametersScalingX' ]
      initialParametersScalingY = DwToB0RegistrationParameters[  \
                                  'initialParametersScalingY' ]
      initialParametersScalingZ = DwToB0RegistrationParameters[  \
                                  'initialParametersScalingZ' ]
      initialParametersShearingXY = DwToB0RegistrationParameters[  \
                                  'initialParametersShearingXY' ]
      initialParametersShearingXZ = DwToB0RegistrationParameters[  \
                                  'initialParametersShearingXZ' ]
      initialParametersShearingYZ = DwToB0RegistrationParameters[  \
                                  'initialParametersShearingYZ' ]
      initialParametersTranslationX = DwToB0RegistrationParameters[  \
                                  'initialParametersTranslationX' ]
      initialParametersTranslationY = DwToB0RegistrationParameters[  \
                                  'initialParametersTranslationY' ]
      initialParametersTranslationZ = DwToB0RegistrationParameters[  \
                                  'initialParametersTranslationZ' ]
      initialParametersRotationX = DwToB0RegistrationParameters[  \
                                  'initialParametersRotationX' ]
      initialParametersRotationY = DwToB0RegistrationParameters[  \
                                  'initialParametersRotationY' ]
      initialParametersRotationZ = DwToB0RegistrationParameters[  \
                                  'initialParametersRotationZ' ]
      stoppingCriterionError = DwToB0RegistrationParameters[ \
                                                      'stoppingCriterionError' ]
      stepSize = DwToB0RegistrationParameters[ 'stepSize' ]
      maximumTestGradient = DwToB0RegistrationParameters[ 'maximumTestGradient' ]
      maximumTolerance = DwToB0RegistrationParameters[ 'maximumTolerance' ]
      optimizerParametersScalingX = DwToB0RegistrationParameters[  \
                                  'optimizerParametersScalingX' ]
      optimizerParametersScalingY = DwToB0RegistrationParameters[  \
                                  'optimizerParametersScalingY' ]
      optimizerParametersScalingZ = DwToB0RegistrationParameters[  \
                                  'optimizerParametersScalingZ' ]
      optimizerParametersShearingXY = DwToB0RegistrationParameters[  \
                                  'optimizerParametersShearingXY' ]
      optimizerParametersShearingXZ = DwToB0RegistrationParameters[  \
                                  'optimizerParametersShearingXZ' ]
      optimizerParametersShearingYZ = DwToB0RegistrationParameters[  \
                                  'optimizerParametersShearingYZ' ]
      optimizerParametersTranslationX = DwToB0RegistrationParameters[  \
                                  'optimizerParametersTranslationX' ]
      optimizerParametersTranslationY = DwToB0RegistrationParameters[  \
                                  'optimizerParametersTranslationY' ]
      optimizerParametersTranslationZ = DwToB0RegistrationParameters[  \
                                  'optimizerParametersTranslationZ' ]
      optimizerParametersRotationX = DwToB0RegistrationParameters[  \
                                  'optimizerParametersRotationX' ]
      optimizerParametersRotationY = DwToB0RegistrationParameters[  \
                                  'optimizerParametersRotationY' ]
      optimizerParametersRotationZ = DwToB0RegistrationParameters[  \
                                  'optimizerParametersRotationZ' ]

      command = \
          'GkgExecuteCommand Registration3d ' + \
            ' -reference ' + fileNameB0MagnitudeEcho1 + \
            ' -floating ' + fileNameAverageT2 + \
            ' -o ' + fileNameDwToB0Transform3d + \
            ' -oi ' + fileNameB0ToDwTransform3d + \
            ' -similarityMeasureName ' + similarityMeasureName + \
            ' -optimizerName ' + optimizerName + \
            ' -referenceLowerThreshold ' + str( referenceLowerThreshold ) + \
            ' -floatingLowerThreshold ' + str( floatingLowerThreshold ) + \
            ' -resamplingOrder ' + str( resamplingOrder ) + \
            ' -subSamplingMaximumSizes ' + str( subSamplingMaximumSizes )

      if( similarityMeasureName != 'correlation-coefficient' ):

        command += ' -similarityMeasureParameters ' + \
                                                   str( levelCount ) + ' ' + \
                                                   str( int( applySmoothing ) )
      command += ' -t ' + transform3DType 
      if not ( initializeCoefficientsUsingCenterOfGravity ):

        command += ' -initialParameters '
        if( transform3DType != 'rigid' ):

          command += str( initialParametersScalingX ) + \
               ' ' + str( initialParametersScalingY ) + \
               ' ' + str( initialParametersScalingZ ) + ' '

        if( transform3DType == 'affine' ):

          command += str( initialParametersShearingXY ) + \
               ' ' + str( initialParametersShearingXZ ) + \
               ' ' + str( initialParametersShearingYZ ) + ' '


        command += str( initialParametersTranslationX ) + \
             ' ' + str( initialParametersTranslationY ) + \
             ' ' + str( initialParametersTranslationZ ) + \
             ' ' + str( initialParametersRotationX ) + \
             ' ' + str( initialParametersRotationY ) + \
             ' ' + str( initialParametersRotationZ ) + ' '

      else:
        
        command += ' -initializedUsingCentreOfGravity'

      command += ' -optimizerParameters ' + str( maximumIterationCount ) + ' ' 

      if ( optimizerName == 'nelder-mead' ):
                                    
        command += str( stoppingCriterionError ) + ' '   
        if ( transform3DType != 'rigid' ):

          command += str( optimizerParametersScalingX ) + ' ' + \
                     str( optimizerParametersScalingY ) + ' ' + \
                     str( optimizerParametersScalingZ ) + ' '
                       

        if ( transform3DType == 'affine' ):

          command += str( optimizerParametersShearingXY ) + ' ' + \
                     str( optimizerParametersShearingXZ ) + ' ' + \
                     str( optimizerParametersShearingYZ ) + ' '
 
        command += str( optimizerParametersRotationX ) + ' ' + \
                   str( optimizerParametersRotationY ) + ' ' + \
                   str( optimizerParametersRotationZ ) + ' ' + \
                   str( optimizerParametersTranslationX ) + ' ' + \
                   str( optimizerParametersTranslationY ) + ' ' + \
                   str( optimizerParametersTranslationZ ) + ' '
 
      else:
                                    
        command += str( stepSize ) + ' ' + \
                   str( maximumTestGradient ) + ' ' + \
                   str( maximumTolerance ) 
      command += ' -verbose true'


      executeCommand( self, subjectName, command, viewOnly )

    else:

      shutil.copyfile( parameters[ 'fileNameDwToB0Transformation' ].getValue(),
                                   fileNameDwToB0Transform3d ) 

      command = 'GkgExecuteCommand Transform3dInverter ' + \
                ' -i ' + fileNameDwToB0Transform3d + \
                ' -o ' + fileNameB0ToDwTransform3d
      executeCommand( self, subjectName, command, viewOnly )
      
    return fileNameDwToB0Transform3d, fileNameB0ToDwTransform3d


  ##############################################################################
  # FFD-based correction
  ##############################################################################

  def correctSusceptibilityUsingTopUp( self, 
                                       parameters,
                                       functors,
                                       sliceAxis,
                                       phaseAxis,
                                       sizeX,
                                       sizeY,
                                       sizeZ,
                                       qSpacePointCount,
                                       resolutionX,
                                       resolutionY,
                                       resolutionZ,
                                       fileNameAverageT2,
                                       fileNameDW,
                                       outputWorkDirectory,
                                       subjectName,
                                       verbose,
                                       viewOnly ):

    # computing transformation from blip-up & blip-down references

    blipUpReferenceFileName = parameters[ 'blipUpReferenceFileName' ] \
                                                                     .getValue()
    blipDownReferenceFileName = parameters[ 'blipDownReferenceFileName' ] \
                                                                     .getValue()

    blipUpParameters = parameters[ 'blipUpParameters' ].getValue()
    blipDownParameters = parameters[ 'blipDownParameters' ].getValue()

    # creating the acquisition parameters text file
    fileNameParameters = os.path.join( outputWorkDirectory,
                                       'top_up_acquisition_parameters.txt' )
    file = open( fileNameParameters, 'w' )
    file.write( blipUpParameters + '\n' )
    file.write( blipDownParameters + '\n' )
    file.close()

    # merging blip-up and down references
    fileNameBothReferences = os.path.join( outputWorkDirectory,
                                           'references' )
    command = 'fsl5.0-fslmerge ' + \
              ' -t ' + fileNameBothReferences + ' ' + \
              blipUpReferenceFileName + ' ' + \
              blipDownReferenceFileName
    executeCommand( self, subjectName, command, viewOnly )

    # computing top-up transformation
    fileNameTopUpTransformation = os.path.join( outputWorkDirectory,
                                                'top_up_transformation' )
    command = 'fsl5.0-topup ' + \
              ' --imain=' + fileNameBothReferences + ' ' + \
              ' --datain=' + fileNameParameters + ' ' + \
              ' --out=' + fileNameTopUpTransformation + ' ' +  \
              ' -v'
    executeCommand( self, subjectName, command, viewOnly )

    # converting average T2 into NIFTI
    fileNameAverageT2Nifti = os.path.join( outputWorkDirectory,
                                           't2_nifti' )
    command = 'GkgExecuteCommand Gis2NiftiConverter ' + \
              ' -i ' + fileNameAverageT2 + \
              ' -o ' + fileNameAverageT2Nifti + '.nii' \
              ' -verbose'
    executeCommand( self, subjectName, command, viewOnly )

    command = 'gzip -f ' + fileNameAverageT2Nifti + '.nii'
    executeCommand( self, subjectName, command, viewOnly )

    # converting DW into NIFTI
    fileNameDWNifti = os.path.join( outputWorkDirectory,
                                    'dw_nifti' )
    command = 'GkgExecuteCommand Gis2NiftiConverter ' + \
              ' -i ' + fileNameDW + \
              ' -o ' + fileNameDWNifti + '.nii' \
              ' -verbose'
    executeCommand( self, subjectName, command, viewOnly )

    command = 'gzip -f ' + fileNameDWNifti + '.nii'
    executeCommand( self, subjectName, command, viewOnly )

    # applying top-up to average T2 (b=0)
    fileNameAverageT2WoSusceptibilityNifti = os.path.join( \
                                                  outputWorkDirectory,
                                                  't2_wo_susceptibility_nifti' )
    command = \
      'fsl5.0-applytopup ' + \
      ' --imain=' + fileNameAverageT2Nifti + '.nii.gz' + \
      ' --datain=' + fileNameParameters + \
      ' --inindex=1 ' + \
      ' --topup=' + fileNameTopUpTransformation + \
      ' --method=jac ' + \
      ' --out=' + fileNameAverageT2WoSusceptibilityNifti + '.nii.gz'
    executeCommand( self, subjectName, command, viewOnly )
    
    fileNameAverageT2WoSusceptibility = os.path.join( outputWorkDirectory,
                                                     't2_wo_susceptibility' )
    command = 'AimsFileConvert ' + \
              ' -i ' + fileNameAverageT2WoSusceptibilityNifti + '.nii.gz' + \
              ' -o ' + fileNameAverageT2WoSusceptibility + '.ima'
    executeCommand( self, subjectName, command, viewOnly )

    command = 'rm -f ' + fileNameAverageT2WoSusceptibility + '.ima.minf'
    executeCommand( self, subjectName, command, viewOnly )

    command = 'GkgExecuteCommand Thresholder ' + \
              ' -i ' + fileNameAverageT2WoSusceptibility + \
              ' -o ' + fileNameAverageT2WoSusceptibility + \
              ' -m gt -t1 0.0' + \
              ' -verbose'
    executeCommand( self, subjectName, command, viewOnly )

    # applying top-up to DW
    fileNameDWWoSusceptibilityNifti = os.path.join( \
                                                  outputWorkDirectory,
                                                  'dw_wo_susceptibility_nifti' )
    command = \
      'fsl5.0-applytopup ' + \
      ' --imain=' + fileNameDWNifti + '.nii.gz' + \
      ' --datain=' + fileNameParameters + \
      ' --inindex=1 ' + \
      ' --topup=' + fileNameTopUpTransformation + \
      ' --method=jac ' + \
      ' --out=' + fileNameDWWoSusceptibilityNifti + '.nii.gz'
    executeCommand( self, subjectName, command, viewOnly )

    fileNameDWWoSusceptibility = os.path.join( outputWorkDirectory,
                                               'dw_wo_susceptibility' )
    command = 'AimsFileConvert ' + \
              ' -i ' + fileNameDWWoSusceptibilityNifti + '.nii.gz' + \
              ' -o ' + fileNameDWWoSusceptibility + '.ima'
    executeCommand( self, subjectName, command, viewOnly )

    command = 'GkgExecuteCommand Thresholder ' + \
              ' -i ' + fileNameDWWoSusceptibility + \
              ' -o ' + fileNameDWWoSusceptibility + \
              ' -m gt -t1 0.0' + \
              ' -verbose'
    executeCommand( self, subjectName, command, viewOnly )

    # copying back the q-space sampling that did not change
    command = 'cp -f ' + fileNameDW + '.ima.minf' + \
              '   '    + fileNameDWWoSusceptibility + '.ima.minf'
    executeCommand( self, subjectName, command, viewOnly )

    return fileNameAverageT2WoSusceptibility, fileNameDWWoSusceptibility


  ##############################################################################
  # creating RGB map
  ##############################################################################

  def createRGB( self,
                 fileNameAverageT2,
                 fileNameDW,
                 fileNameMask,
                 outputWorkDirectory,
                 subjectName,
                 verbose,
                 viewOnly ):

    # building RGB map
    fileNameRGB = os.path.join( outputWorkDirectory,
                                'dti_rgb_wo_susceptibility' )
    command = 'GkgExecuteCommand DwiTensorField' + \
              ' -t2 ' + fileNameAverageT2 + \
              ' -dw ' + fileNameDW + \
              ' -m ' +  fileNameMask + \
              ' -f rgb ' + \
              ' -o ' + fileNameRGB + \
              ' -verbose true'
    executeCommand( self, subjectName, command, viewOnly )


  ##############################################################################
  # display()
  ##############################################################################

  def display( self, functors, parameters, subjectName, verbose,
               isReportModeActive ):

    ########################### resetting the viewer ###########################
    functors[ 'viewer-reset' ]()

    ########################### resetting the viewer ###########################
    correctionStrategy = parameters[ 'correctionStrategy' ].getChoice()
    viewName = 'first_view'
    if ( correctionStrategy != 'Fieldmap-based correction' ):
    
      viewName = 'second_view'

    functors[ 'viewer-set-view' ]( viewName )

    ####################### collecting raw DWI directory #####################
    rawDwiDirectory = parameters[ 'rawDwiDirectory' ].getValue()
    if ( verbose ):
    
      print  'Raw DWI directory :', rawDwiDirectory

    fileNameAverageT2 = os.path.join( rawDwiDirectory, 't2' )
    fileNameDW = os.path.join( rawDwiDirectory, 'dw' )
    fileNameDtiRgb = os.path.join( rawDwiDirectory, 'dti_rgb' )

    ####################### collecting raw DWI directory #####################
    roughMaskDirectory = parameters[ 'roughMaskDirectory' ].getValue()
    if ( len( roughMaskDirectory ) ):
    
      fileNameDtiRgb = os.path.join( roughMaskDirectory, 'dti_rgb' )

    ####################### collecting outlier filtered DWI directory ########
    outlierFilteredDwiDirectory = parameters[ \
                                    'outlierFilteredDwiDirectory' ].getValue()
    if ( verbose ):
    
      print  'Outlier filtered DWI directory :', outlierFilteredDwiDirectory

    if ( len( outlierFilteredDwiDirectory ) ):
    
      fileNameAverageT2 = os.path.join( outlierFilteredDwiDirectory,
                                        't2_wo_outlier' )
      fileNameDW = os.path.join( outlierFilteredDwiDirectory,
                                 'dw_wo_outlier' )
      fileNameDtiRgb = os.path.join( outlierFilteredDwiDirectory,
                                     'dti_rgb_wo_outlier' )

    ########################### creating DW, B0 frames #########################
    functors[ 'viewer-create-referential' ]( 'frameDW' )

    if ( correctionStrategy == 'Fieldmap-based correction' ):
    
      functors[ 'viewer-create-referential' ]( 'frameB0' )

    ################################ reading average T2 ########################
    if ( verbose ):

      print 'reading \'' + fileNameAverageT2 + '\''
    functors[ 'viewer-load-object' ]( fileNameAverageT2, 'averageT2' )
    functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'averageT2' )

    functors[ 'viewer-assign-referential-to-window' ]( \
                                                 'frameDW',
                                                 viewName,
                                                 'uncorrected Average T2(b=0)' )
    functors[ 'viewer-add-object-to-window' ]( 'averageT2',
                                               viewName,
                                               'uncorrected Average T2(b=0)' )

    ################################ reading DW ################################
    if ( verbose ):

      print 'reading \'' + fileNameDW + '\''
    functors[ 'viewer-load-object' ]( fileNameDW, 'DW' )
    functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'DW' )

    functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                       viewName,
                                                       'uncorrected DW' )
    functors[ 'viewer-add-object-to-window' ]( 'DW',
                                               viewName,
                                               'uncorrected DW' )

    ########################### reading uncorrected RGB ########################
    if ( verbose ):

      print 'reading \'' + fileNameDtiRgb + '\''
    functors[ 'viewer-load-object' ]( fileNameDtiRgb, 'RGB' )
    functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'RGB' )

    functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                       viewName,
                                                       'uncorrected RGB' )
    functors[ 'viewer-add-object-to-window' ]( 'RGB',
                                               viewName,
                                               'uncorrected RGB' )

    ####################### waiting for corrected T2/DW ########################
    functors[ 'condition-wait-and-release' ]( subjectName,
                                              'susceptibility-processed' )

    outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()

    ###################### reading corrected average T2 ########################
    fileNameAverageT2WoSusceptibility = os.path.join( outputWorkDirectory,
                                                      't2_wo_susceptibility' )
    if ( verbose ):

      print 'reading \'' + fileNameAverageT2WoSusceptibility + '\''
    functors[ 'viewer-load-object' ]( fileNameAverageT2WoSusceptibility,
                                      'averageT2WoSusceptibility' )
    functors[ 'viewer-assign-referential-to-object' ]( \
                                                   'frameDW',
                                                   'averageT2WoSusceptibility' )

    functors[ 'viewer-assign-referential-to-window' ]( \
                                                   'frameDW',    
                                                   viewName,
                                                   'corrected Average T2(b=0)' )
    functors[ 'viewer-add-object-to-window' ]( 'averageT2WoSusceptibility',
                                               viewName,
                                               'corrected Average T2(b=0)' )

    ########################## reading corrected DW ############################
    fileNameDWWoSusceptibility = os.path.join( outputWorkDirectory,
                                               'dw_wo_susceptibility' )
    if ( verbose ):

      print 'reading \'' + fileNameDWWoSusceptibility + '\''
    functors[ 'viewer-load-object' ]( fileNameDWWoSusceptibility,
                                      'DWWoSusceptibility' )
    functors[ 'viewer-assign-referential-to-object' ]( 'frameDW',
                                                       'DWWoSusceptibility' )

    functors[ 'viewer-assign-referential-to-window' ]( \
                                                   'frameDW',    
                                                   viewName,
                                                   'corrected DW' )
    functors[ 'viewer-add-object-to-window' ]( 'DWWoSusceptibility',
                                               viewName,
                                               'corrected DW' )

    ######################### waiting for corrected RGB ########################
    functors[ 'condition-wait-and-release' ]( subjectName,
                                              'corrected-rgb-processed' )

    ########################### reading corrected RGB ##########################
    fileNameDtiRgbWoSusceptibility = os.path.join( outputWorkDirectory,
                                                   'dti_rgb_wo_susceptibility' )
    if ( verbose ):

      print 'reading \'' + fileNameDtiRgbWoSusceptibility + '\''
    functors[ 'viewer-load-object' ]( fileNameDtiRgbWoSusceptibility,
                                      'RGBWoSusceptibility' )
    functors[ 'viewer-assign-referential-to-object' ]( 'frameDW',
                                                       'RGBWoSusceptibility' )

    functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                       viewName,
                                                       'corrected RGB' )
    functors[ 'viewer-add-object-to-window' ]( 'RGBWoSusceptibility',
                                               viewName,
                                               'corrected RGB' )

    ################# merging uncorrected and corrected RGBs ###################
    functors[ 'viewer-fusion-objects' ]( \
                 [ 'RGB', 'RGBWoSusceptibility' ],
                 'fusionRGBAndRGBWoSusceptibility',
                 'Fusion2DMethod' )
    functors[ 'viewer-assign-referential-to-object' ]( \
                'frameDW',
                'fusionRGBAndRGBWoSusceptibility' )

    functors[ 'viewer-assign-referential-to-window' ]( \
                                                'frameDW',    
                                                viewName,
                                                'Uncorrected + Corrected RGBs' )
    functors[ 'viewer-add-object-to-window' ]( \
                 'fusionRGBAndRGBWoSusceptibility',
                 viewName,
                 'Uncorrected + Corrected RGBs' )

    if ( correctionStrategy == 'Fieldmap-based correction' ):
    
      ###################### loading B0 to DW transformation ###################
      fileNameB0ToDwTransform3d = os.path.join( outputWorkDirectory,
                                                'b0_to_dw.trm' )
      functors[ 'viewer-load-transformation' ]( fileNameB0ToDwTransform3d,
                                                'frameB0', 'frameDW' )
                                                
      ###################### reading first echo B0 magnitude ###################
      fileNameFirstEchoB0Magnitude = os.path.join( outputWorkDirectory,
                                                   'b0_magnitude_echo1' )
      functors[ 'viewer-load-object' ]( fileNameFirstEchoB0Magnitude, 
                                        'FirstEchoB0Magnitude' )
      functors[ 'viewer-assign-referential-to-object' ]( \
                                                        'frameB0',
                                                        'FirstEchoB0Magnitude' )
      functors[ 'viewer-set-colormap' ]( 'FirstEchoB0Magnitude', 
                                         'RED TEMPERATURE' )

      ################### reading unwrapped B0 phase difference ################
      fileNameUnwrappedB0PhaseDifference = os.path.join( outputWorkDirectory,
                                                         'b0_phase_unwrapped' )
      functors[ 'viewer-load-object' ]( fileNameUnwrappedB0PhaseDifference,
                                        'UnwrappedB0PhaseDifference' )
      functors[ 'viewer-assign-referential-to-object' ]( \
                                                  'frameB0',
                                                  'UnwrappedB0PhaseDifference' )
      functors[ 'viewer-set-colormap' ]( 'UnwrappedB0PhaseDifference', 
                                         'RAINBOW',
                                         -1.0, +1.0 )

      ############ merging first echo B0 magnitude and average T2(b=0) #########
      functors[ 'viewer-fusion-objects' ]( \
                     [ 'averageT2WoSusceptibility', 'FirstEchoB0Magnitude' ],
                       'fusionAverageT2WoSusceptibilityAndFirstEchoB0Magnitude',
                       'Fusion2DMethod' )
      functors[ 'viewer-assign-referential-to-object' ]( \
                      'frameDW',
                      'fusionAverageT2WoSusceptibilityAndFirstEchoB0Magnitude' )

      functors[ 'viewer-assign-referential-to-window' ]( \
                                                'frameDW',    
                                                viewName,
                                                'Average T2(b=0)+B0 magnitude' )
      functors[ 'viewer-add-object-to-window' ]( \
                'fusionAverageT2WoSusceptibilityAndFirstEchoB0Magnitude',
                viewName,
                'Average T2(b=0)+B0 magnitude' )

      ########## merging unwrapped phase difference and average T2(b=0) ########
      functors[ 'viewer-fusion-objects' ]( \
                 [ 'averageT2WoSusceptibility', 'UnwrappedB0PhaseDifference' ],
                 'fusionAverageT2WoSusceptibilityAndB0UnwrappedPhaseDifference',
                 'Fusion2DMethod' )
      functors[ 'viewer-assign-referential-to-object' ]( \
                'frameDW',
                'fusionAverageT2WoSusceptibilityAndB0UnwrappedPhaseDifference' )

      functors[ 'viewer-assign-referential-to-window' ]( \
                                                    'frameDW',    
                                                    viewName,
                                                    'Average T2(b=0)+B0 phase' )
      functors[ 'viewer-add-object-to-window' ]( \
                 'fusionAverageT2WoSusceptibilityAndB0UnwrappedPhaseDifference',
                 viewName,
                 'Average T2(b=0)+B0 phase' )

    else:
    
      ################### reading reference ####################################
      fileNameTopUpReference = os.path.join( outputWorkDirectory,
                                             'references.nii.gz' )
      functors[ 'viewer-load-object' ]( fileNameTopUpReference,
                                        'topUpReference' )
      functors[ 'viewer-assign-referential-to-object' ]( \
                                                  'frameDW',
                                                  'topUpReference' )
      functors[ 'viewer-add-object-to-window' ]( 'topUpReference',
                                                 viewName,
                                                 'Top-Up reference' )

      ################### reading FFD map ######################################
      fileNameFFDMap = os.path.join( outputWorkDirectory,
                                     'top_up_transformation_fieldcoef.nii.gz' )
      functors[ 'viewer-load-object' ]( fileNameFFDMap,
                                        'ffdMap' )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW',
                                                         'ffdMap' )
      functors[ 'viewer-set-colormap' ]( 'ffdMap', 
                                         'RAINBOW',
                                         -1.0, 1.0 )
      functors[ 'viewer-add-object-to-window' ]( 'ffdMap',
                                                 viewName,
                                                 'FFD' )
                                         
