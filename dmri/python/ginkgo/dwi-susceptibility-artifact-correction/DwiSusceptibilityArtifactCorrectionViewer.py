from Viewer import *


class DwiSusceptibilityArtifactCorrectionViewer( Viewer ):

  def __init__( self, minimumSize, parent ):

    Viewer.__init__( self, minimumSize, parent )
    
    ########################## First View ######################################

    self.createView( 'first_view' )

    
    self.addAxialWindow( 'first_view', 0, 0, 1, 1,
                         'uncorrected Average T2(b=0)',
                         'uncorrected Average T2(b=0)' )
    self.addAxialWindow( 'first_view', 0, 1, 1, 1,
                         'uncorrected DW',
                         'uncorrected DW' )
    self.addAxialWindow( 'first_view', 0, 2, 1, 1,
                         'uncorrected RGB',
                         'uncorrected RGB' )

    self.addAxialWindow( 'first_view', 1, 0, 1, 1,
                         'corrected Average T2(b=0)',
                         'corrected Average T2(b=0)' )
    self.addAxialWindow( 'first_view', 1, 1, 1, 1,
                         'corrected DW',
                         'corrected DW' )
    self.addAxialWindow( 'first_view', 1, 2, 1, 1,
                         'corrected RGB',
                         'corrected RGB' )

    self.addAxialWindow( 'first_view', 2, 0, 1, 1,
                         'Average T2(b=0)+B0 magnitude',
                         'Average T2(b=0)+B0 magnitude' )
    self.addAxialWindow( 'first_view', 2, 1, 1, 1,
                         'Average T2(b=0)+B0 phase',
                         'Average T2(b=0)+B0 phase' )
    self.addSagittalWindow( 'first_view', 2, 2, 1, 1,
                            'Uncorrected + Corrected RGBs',
                            'Uncorrected + Corrected RGBs' )

    ########################## First View ######################################

    self.createView( 'second_view' )

    
    self.addAxialWindow( 'second_view', 0, 0, 1, 1,
                         'uncorrected Average T2(b=0)',
                         'uncorrected Average T2(b=0)' )
    self.addAxialWindow( 'second_view', 0, 1, 1, 1,
                         'uncorrected DW',
                         'uncorrected DW' )
    self.addAxialWindow( 'second_view', 0, 2, 1, 1,
                         'uncorrected RGB',
                         'uncorrected RGB' )

    self.addAxialWindow( 'second_view', 1, 0, 1, 1,
                         'corrected Average T2(b=0)',
                         'corrected Average T2(b=0)' )
    self.addAxialWindow( 'second_view', 1, 1, 1, 1,
                         'corrected DW',
                         'corrected DW' )
    self.addAxialWindow( 'second_view', 1, 2, 1, 1,
                         'corrected RGB',
                         'corrected RGB' )

    self.addAxialWindow( 'second_view', 2, 0, 1, 1,
                         'Top-Up reference',
                         'Top-Up reference' )
    self.addAxialWindow( 'second_view', 2, 1, 1, 1,
                         'FFD',
                         'FFD' )
    self.addSagittalWindow( 'second_view', 2, 2, 1, 1,
                            'Uncorrected + Corrected RGBs',
                            'Uncorrected + Corrected RGBs' )


def createDwiSusceptibilityArtifactCorrectionViewer( minimumSize, parent ):

  return DwiSusceptibilityArtifactCorrectionViewer( minimumSize, parent )

