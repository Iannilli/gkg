from Algorithm import *
from DwiToAnatomyMatchingTask import *


class DwiToAnatomyMatchingAlgorithm( Algorithm ):

  def __init__( self, verbose ):

    Algorithm.__init__( self, 'DWI-To-Anatomy-Matching', verbose,
                        True )

    # diffusion-weighted input data
    self.addParameter( StringParameter( 'correctedDwiDirectory', '' ) )
    self.addParameter( StringParameter( 'roughMaskDirectory', '' ) )

    # T1-weighted data
    self.addParameter( StringParameter( 'fileNameT1', '' ) )
    self.addParameter( IntegerParameter( 't1LeftXCropping',
                                         0, 0, 1000, 1 ) )
    self.addParameter( IntegerParameter( 't1RightXCropping',
                                         0, 0, 1000, 1 ) )
    self.addParameter( IntegerParameter( 't1AnteriorYCropping',
                                         0, 0, 1000, 1 ) )
    self.addParameter( IntegerParameter( 't1PosteriorYCropping', 
                                         0, 0, 1000, 1 ) )
    self.addParameter( IntegerParameter( 't1HeadZCropping',
                                         0, 0, 1000, 1 ) )
    self.addParameter( IntegerParameter( 't1FootZCropping',
                                         0, 0, 1000, 1 ) )

    # T1 Normalization
    self.addParameter( BooleanParameter( 'computeNormalization',
                                         False ) )
    self.addParameter( BooleanParameter( 'useMorphologistAPCFile',
                                         False ) )
    self.addParameter( StringParameter( 'morphologistAPCFileName', '' ) )
    
    self.addParameter( ChoiceParameter(
                         'templateName',
                         0,
                         [ 't1-MNI152-1mm',
                           't1-MNI152-1mm-brain',
                           't1-MNI152-2mm-smoothed' ] ) )
    self.addParameter( StringParameter( 'templateFlipping', '' ) )
    self.addParameter( 
      AdvancedParameter(
        'normalization3dSettings',
        { 'similarityMeasureName':
                      { 'widget': 'comboBox_SimilarityMeasureName',
                        'widgetType': 'comboBox',
                        'defaultValue': 1,
                        'choices': [ 'correlation-coefficient',
                                     'mutual-information',
                                     'normalized-mutual-information',
                                     'correlation-ratio' ]
                      },
          'templateLowerThreshold':
                      { 'widget': 'doubleSpinBox_TemplateLowerThreshold',
                        'widgetType': 'spinBox',
                        'defaultValue': 0.0,
                        'minimumValue': 0.0,
                        'maximumValue': 10000,
                        'incrementValue': 0.01
                      },
          'floatingLowerThreshold':
                      { 'widget': 'doubleSpinBox_FloatingLowerThreshold',
                        'widgetType': 'spinBox',
                        'defaultValue': 0.0,
                        'minimumValue': 0.0,
                        'maximumValue': 10000,
                        'incrementValue': 0.01
                      },
          'resamplingOrder':
                      { 'widget': 'spinBox_ResamplingOrder',
                        'widgetType': 'spinBox',
                        'defaultValue': 1,
                        'minimumValue': 0,
                        'maximumValue': 7,
                        'incrementValue': 1
                      },
          'subSamplingMaximumSizes':
                      { 'widget': 'lineEdit_SubSamplingMaximumSizes',
                        'widgetType': 'lineEdit',
                        'defaultValue': '64 128'
                      },
          'levelCount':
                      { 'widget': 'spinBox_LevelCount',
                        'widgetType': 'spinBox',
                        'defaultValue': 32,
                        'minimumValue': 0,
                        'maximumValue': 1000,
                        'incrementValue': 1,
                        'dependency': [ 'similarityMeasureName' ],
                        'dependencyValues': [ { 0: False,
                                                1: True,
                                                2: True,
                                                3: True  } ]
                      },
          'applySmoothing':
                      { 'widget': 'checkBox_ApplySmoothing',
                        'widgetType': 'checkBox',
                        'defaultValue': 1,
                        'dependency': [ 'similarityMeasureName' ],
                        'dependencyValues': [ { 0: False,
                                                1: True,
                                                2: True,
                                                3: True  } ]
                      },
          'maximumIterationCount':
                      { 'widget': 'spinBox_MaximumIterationCount',
                        'widgetType': 'spinBox',
                        'defaultValue': 2000,
                        'minimumValue': 0,
                        'maximumValue': 100000,
                        'incrementValue': 1
                      },
          'initialParametersTranslationX':
                      { 'widget': 'doubleSpinBox_InitialParametersTranslationX',
                        'widgetType': 'spinBox',
                        'defaultValue': 0,
                        'minimumValue': -10000,
                        'maximumValue': 10000,
                        'incrementValue': 0.01
                      },
          'initialParametersTranslationY':
                      { 'widget': 'doubleSpinBox_InitialParametersTranslationY',
                        'widgetType': 'spinBox',
                        'defaultValue': 0,
                        'minimumValue': -10000,
                        'maximumValue': 10000,
                        'incrementValue': 0.01
                      },
          'initialParametersTranslationZ':
                      { 'widget': 'doubleSpinBox_InitialParametersTranslationZ',
                        'widgetType': 'spinBox',
                        'defaultValue': 0,
                        'minimumValue': -10000,
                        'maximumValue': 10000,
                        'incrementValue': 0.01
                      },
          'initialParametersRotationX':
                      { 'widget': 'doubleSpinBox_InitialParametersRotationX',
                        'widgetType': 'spinBox',
                        'defaultValue': 0,
                        'minimumValue': 0,
                        'maximumValue': 360,
                        'incrementValue': 0.01
                      },
          'initialParametersRotationY':
                      { 'widget': 'doubleSpinBox_InitialParametersRotationY',
                        'widgetType': 'spinBox',
                        'defaultValue': 0,
                        'minimumValue': 0,
                        'maximumValue': 360,
                        'incrementValue': 0.01
                      },
          'initialParametersRotationZ':
                      { 'widget': 'doubleSpinBox_InitialParametersRotationZ',
                        'widgetType': 'spinBox',
                        'defaultValue': 0,
                        'minimumValue': 0,
                        'maximumValue': 360,
                        'incrementValue': 0.01
                      },
          'initialParametersScalingX':
                      { 'widget': 'doubleSpinBox_InitialParametersScalingX',
                        'widgetType': 'spinBox',
                        'defaultValue': 1.0
                      },
          'initialParametersScalingY':
                      { 'widget': 'doubleSpinBox_InitialParametersScalingY',
                        'widgetType': 'spinBox',
                        'defaultValue': 1.0,
                        'minimumValue': 0.0,
                        'maximumValue': 10000,
                        'incrementValue': 0.01
                      },
          'initialParametersScalingZ':
                      { 'widget': 'doubleSpinBox_InitialParametersScalingZ',
                        'widgetType': 'spinBox',
                        'defaultValue': 1.0,
                        'minimumValue': 0.0,
                        'maximumValue': 10000,
                        'incrementValue': 0.01
                      },
          'initialParametersShearingXY':
                      { 'widget': 'doubleSpinBox_InitialParametersShearingXY',
                        'widgetType': 'spinBox',
                        'defaultValue': 0.0,
                        'minimumValue': 0.0,
                        'maximumValue': 10000,
                        'incrementValue': 0.01
                      },
          'initialParametersShearingXZ':
                      { 'widget': 'doubleSpinBox_InitialParametersShearingXZ',
                        'widgetType': 'spinBox',
                        'defaultValue': 0.0,
                        'minimumValue': 0.0,
                        'maximumValue': 10000,
                        'incrementValue': 0.01
                      },
          'initialParametersShearingYZ':
                      { 'widget': 'doubleSpinBox_InitialParametersShearingYZ',
                        'widgetType': 'spinBox',
                        'defaultValue': 0.0,
                        'minimumValue': 0.0,
                        'maximumValue': 10000,
                        'incrementValue': 0.01
                      },
          'optimizerName':
                      { 'widget': 'comboBox_OptimizerName',
                        'widgetType': 'comboBox',
                        'defaultValue': 0,
                        'choices': [ 'nelder-mead',
                                     'fletcher-reeves' ]
                      },
          'stoppingCriterionError':
                      { 'widget': 'doubleSpinBox_StoppingCriterionError',
                        'widgetType': 'spinBox',
                        'defaultValue': 0.01,
                        'minimumValue': 0.00001,
                        'maximumValue': 10000,
                        'incrementValue': 0.00001,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: True,
                                                1: False } ]
                      },
          'stepSize':
                      { 'widget': 'doubleSpinBox_StepSize',
                        'widgetType': 'spinBox',
                        'defaultValue': 1e-1,
                        'minimumValue': 1e-10,
                        'maximumValue': 100,
                        'incrementValue': 1e-10,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: False,
                                                1: True } ]
                      },
          'maximumTestGradient':
                      { 'widget': 'doubleSpinBox_MaximumTestGradient',
                        'widgetType': 'spinBox',
                        'defaultValue': 1000.0,
                        'minimumValue': 0.0,
                        'maximumValue': 1000000.0,
                        'incrementValue': 1.0,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: False,
                                                1: True } ]
                      },
          'maximumTolerance':
                      { 'widget': 'doubleSpinBox_MaximumTolerance',
                        'widgetType': 'spinBox',
                        'defaultValue': 1e-2,
                        'minimumValue': 1e-10,
                        'maximumValue': 100,
                        'incrementValue': 1e-10,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: False,
                                                1: True } ]
                      },
          'optimizerParametersTranslationX':
                      { 'widget': 'doubleSpinBox_OptimizerParametersTranslationX',
                        'widgetType': 'spinBox',
                        'defaultValue': 5,
                        'minimumValue': -10000,
                        'maximumValue': 10000,
                        'incrementValue': 0.01,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: True,
                                                1: False } ]
                      },
          'optimizerParametersTranslationY':
                      { 'widget': 'doubleSpinBox_OptimizerParametersTranslationY',
                        'widgetType': 'spinBox',
                        'defaultValue': 5,
                        'minimumValue': -10000,
                        'maximumValue': 10000,
                        'incrementValue': 0.01,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: True,
                                                1: False } ]
                      },
          'optimizerParametersTranslationZ':
                      { 'widget': 'doubleSpinBox_OptimizerParametersTranslationZ',
                        'widgetType': 'spinBox',
                        'defaultValue': 5,
                        'minimumValue': -10000,
                        'maximumValue': 10000,
                        'incrementValue': 0.01,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: True,
                                                1: False } ]
                      },
          'optimizerParametersRotationX':
                      { 'widget': 'doubleSpinBox_OptimizerParametersRotationX',
                        'widgetType': 'spinBox',
                        'defaultValue': 10,
                        'minimumValue': 0,
                        'maximumValue': 360,
                        'incrementValue': 0.01,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: True,
                                                1: False } ]
                      },
          'optimizerParametersRotationY':
                      { 'widget': 'doubleSpinBox_OptimizerParametersRotationY',
                        'widgetType': 'spinBox',
                        'defaultValue': 10,
                        'minimumValue': 0,
                        'maximumValue': 360,
                        'incrementValue': 0.01,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: True,
                                                1: False } ]
                      },
          'optimizerParametersRotationZ':
                      { 'widget': 'doubleSpinBox_OptimizerParametersRotationZ',
                        'widgetType': 'spinBox',
                        'defaultValue': 10,
                        'minimumValue': 0,
                        'maximumValue': 360,
                        'incrementValue': 0.01,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: True,
                                                1: False } ]
                      },
          'optimizerParametersScalingX':
                      { 'widget': 'doubleSpinBox_OptimizerParametersScalingX',
                        'widgetType': 'spinBox',
                        'defaultValue': 0.02,
                        'minimumValue': 0.0,
                        'maximumValue': 10000,
                        'incrementValue': 0.01,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: True,
                                                1: False } ]
                      },
          'optimizerParametersScalingY':
                      { 'widget': 'doubleSpinBox_OptimizerParametersScalingY',
                        'widgetType': 'spinBox',
                        'defaultValue': 0.02,
                        'minimumValue': 0.0,
                        'maximumValue': 10000,
                        'incrementValue': 0.01,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: True,
                                                1: False } ]
                      },
          'optimizerParametersScalingZ':
                      { 'widget': 'doubleSpinBox_OptimizerParametersScalingZ',
                        'widgetType': 'spinBox',
                        'defaultValue': 0.02,
                        'minimumValue': 0.0,
                        'maximumValue': 10000,
                        'incrementValue': 0.01,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: True,
                                                1: False } ]
                      },
          'optimizerParametersShearingXY':
                      { 'widget': 'doubleSpinBox_OptimizerParametersShearingXY',
                        'widgetType': 'spinBox',
                        'defaultValue': 0.01,
                        'minimumValue': 0.0,
                        'maximumValue': 10000,
                        'incrementValue': 0.01,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: True,
                                                1: False } ]
                      },
          'optimizerParametersShearingXZ':
                      { 'widget': 'doubleSpinBox_OptimizerParametersShearingXZ',
                        'widgetType': 'spinBox',
                        'defaultValue': 0.01,
                        'minimumValue': 0.0,
                        'maximumValue': 10000,
                        'incrementValue': 0.01,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: True,
                                                1: False } ]
                      },
          'optimizerParametersShearingYZ':
                      { 'widget': 'doubleSpinBox_OptimizerParametersShearingYZ',
                        'widgetType': 'spinBox',
                        'defaultValue': 0.01,
                        'minimumValue': 0.0,
                        'maximumValue': 10000,
                        'incrementValue': 0.01,
                        'dependency': [ 'optimizerName' ],
                        'dependencyValues': [ { 0: True,
                                                1: False } ]
                      }
        } ) )

    # Application of freesurfer recon_all pipeline
    self.addParameter( BooleanParameter( 'applyFreesurferReconAll', False ) )

    # DWI to T1 matching
    self.addParameter( BooleanParameter( \
                                        'generateDwToT1Transformation', True ) )
    self.addParameter( BooleanParameter( 'importDwToT1Transformation', False ) )
    self.addParameter( StringParameter( 'fileNameDwToT1Transformation', '' ) )
    self.addParameter( RegistrationParameter( \
                         'dwToT1RegistrationParameter' ,
                         { 'levelCount':
                             { 'defaultValue': 64 },
                           'initializeCoefficientsUsingCenterOfGravity':
                             { 'defaultValue': False },
                           'optimizerParametersTranslationX':
                             { 'defaultValue': 30 },
                           'optimizerParametersTranslationY':
                             { 'defaultValue': 30 },
                           'optimizerParametersTranslationZ':
                             { 'defaultValue': 30 },
                           'optimizerParametersRotationX':
                             { 'defaultValue': 5 },
                           'optimizerParametersRotationY':
                             { 'defaultValue': 5 },
                           'optimizerParametersRotationZ':
                             { 'defaultValue': 5 } } ) )

    # Putting DWI data into Talairach space
    self.addParameter( BooleanParameter( 'resampleDwiDataToTalairachAcpcSpace',
                                         False ) )
    self.addParameter( IntegerParameter( 'leftRightAdditionSliceCount',
                                         0, 0, 1000, 1 ) )
    self.addParameter( IntegerParameter( 'anteriorPosteriorAdditionSliceCount',
                                         0, 0, 1000, 1 ) )
    self.addParameter( IntegerParameter( 'headFootAdditionSliceCount',
                                         0, 0, 1000, 1 ) )


  def launch( self ):

    if ( self._verbose ):

      print \
        'running DWI to anatomy macthing'

    task = DwiToAnatomyMatchingTask( self._application )
    task.launch( False )


  def view( self ):

    if ( self._verbose ):

      print \
        'viewing DWI to anatomy macthing'

    task = DwiToAnatomyMatchingTask( self._application )
    task.launch( True )

