from AlgorithmGUI import *
from ResourceManager import *


class DwiToAnatomyMatchingAlgorithmGUI( AlgorithmGUI ):

  def __init__( self, algorithm ):

    AlgorithmGUI.__init__( self,
                           algorithm,
                           ResourceManager().getUIFileName(
                                                   'dmri',
                                                   'DwiToAnatomyMatching.ui' ) )

    ############################################################################
    # connecting input DWI interface
    ############################################################################

    # corrected DW data directory interface
    self.connectStringParameterToLineEdit( 'correctedDwiDirectory',
                                           'lineEdit_CorrectedDwiDirectory' )
    self.connectDirectoryBrowserToPushButtonAndLineEdit(
                                             'pushButton_CorrectedDwiDirectory',
                                             'lineEdit_CorrectedDwiDirectory' )
                                             
    # rough mask directory interface
    self.connectStringParameterToLineEdit( 'roughMaskDirectory',
                                           'lineEdit_RoughMaskDirectory' )    
    self.connectDirectoryBrowserToPushButtonAndLineEdit(
                                                'pushButton_RoughMaskDirectory',
                                                'lineEdit_RoughMaskDirectory' )

    ############################################################################
    # connecting T1 anatomy interface
    ############################################################################

    # input T1 data file name and push button
    self.connectStringParameterToLineEdit( 'fileNameT1',
                                           'lineEdit_FileNameT1' )    
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit(
                                                        'pushButton_FileNameT1',
                                                        'lineEdit_FileNameT1' )
    self.lineEditFileNameT1 = self._findChild( self._awin,
                                               'lineEdit_FileNameT1' )
    self.lineEditFileNameT1.textChanged.connect(
                                     self.setDwToT1SubSamplingMaximumSizeValue )
    #self._awin.connect( self._findChild( self._awin,
    #                    'lineEdit_FileNameT1' ),
    #                    QtCore.SIGNAL( 'fileNameOrDirectoryEdited(QString)' ),
    #                    self.setDwToT1SubSamplingMaximumSizeValue )


    # cropping interface
    self.connectIntegerParameterToSpinBox( 't1LeftXCropping',
                                           'spinBox_T1LeftXCropping' )
    self.connectIntegerParameterToSpinBox( 't1RightXCropping',
                                           'spinBox_T1RightXCropping' )
    self.connectIntegerParameterToSpinBox( 't1AnteriorYCropping',
                                           'spinBox_T1AnteriorYCropping' )
    self.connectIntegerParameterToSpinBox( 't1PosteriorYCropping',
                                           'spinBox_T1PosteriorYCropping' )
    self.connectIntegerParameterToSpinBox( 't1HeadZCropping',
                                           'spinBox_T1HeadZCropping' )
    self.connectIntegerParameterToSpinBox( 't1FootZCropping',
                                           'spinBox_T1FootZCropping' )

    ############################################################################
    # connecting T1 normalization interface
    ############################################################################

    # computing normalization or not
    self.connectBooleanParameterToCheckBox( 'computeNormalization',
                                            'checkBox_ComputeNormalization' )
                                            
    # use of custom Morphologist directory or not
    self.connectBooleanParameterToCheckBox( \
                                     'useMorphologistAPCFile',
                                     'checkBox_UseMorphologistAPCFile' )
    self._findChild( self._awin,
                        'checkBox_UseMorphologistAPCFile'
                   ).toggled.connect(
                        self.enableMorphologistAPCFile )

    # Morphologist APC filename interface
    self.connectStringParameterToLineEdit( \
                                    'morphologistAPCFileName',
                                    'lineEdit_MorphologistAPCFileName' )    
    self.connectFileBrowserToPushButtonAndLineEdit( \
                                           'pushButton_MorphologistAPCFileName',
                                           'lineEdit_MorphologistAPCFileName' )

    # template type
    self.connectChoiceParameterToComboBox( 'templateName',
                                           'comboBox_TemplateName' )
    self.connectStringParameterToLineEdit( \
                                    'templateFlipping',
                                    'lineEdit_TemplateFlipping' )    

    # normalization parameter(s)
    self._normalization3dSettingss = \
      self.connectPushButtonToAdvancedParameterWidget( \
        'pushButton_Normalization3dSettings',
        ResourceManager().getUIFileName( 'dmri',
                                         'Normalization3dSettings.ui' ),
        'normalization3dSettings' )

    ############################################################################
    # connecting Freesurfer application interface
    ############################################################################

    self.connectBooleanParameterToCheckBox( 'applyFreesurferReconAll',
                                            'checkBox_ApplyFreesurferReconAll' )


    ############################################################################
    # connecting T1 to DW registration advanced parameters
    ############################################################################
    self.connectBooleanParameterToRadioButton( 'generateDwToT1Transformation',
                                    'radioButton_GenerateDwToT1Transformation' )
    self.connectBooleanParameterToRadioButton( 'importDwToT1Transformation',
                                    'radioButton_ImportDwToT1Transformation' )

    self._findChild( self._awin,
                     'radioButton_GenerateDwToT1Transformation'
                    ).toggled.connect( 
                                     self.enableDwToT1ImportAndGenerateWidgets )

    self._DwToT1RegistrationAdvancedParameterWidget = \
         self.connectPushButtonToAdvancedParameterWidget( \
         'pushButton_DwToT1Registration',
         ResourceManager().getUIFileName( 'core','RegistrationSettings.ui' ),
         'dwToT1RegistrationParameter' )

    self.connectStringParameterToLineEdit( 'fileNameDwToT1Transformation',
                                       'lineEdit_FileNameDwToT1Transformation' )
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit(
                                      'pushButton_FileNameDwToT1Transformation',
                                      'lineEdit_FileNameDwToT1Transformation' )

    ############################################################################
    # connecting Talairach space interface
    ############################################################################

    # AC/PC transformation of DWI data
    self.connectBooleanParameterToCheckBox( \
                                'resampleDwiDataToTalairachAcpcSpace',
                                'checkBox_ResampleDwiDataToTalairachAcpcSpace' )    
    self.connectIntegerParameterToSpinBox( \
                                         'leftRightAdditionSliceCount',  
                                         'spinBox_LeftRightAdditionSliceCount' )
    self.connectIntegerParameterToSpinBox( \
                                 'anteriorPosteriorAdditionSliceCount',  
                                 'spinBox_AnteriorPosteriorAdditionSliceCount' )
    self.connectIntegerParameterToSpinBox( \
                                          'headFootAdditionSliceCount',  
                                          'spinBox_HeadFootAdditionSliceCount' )
    

  def setDwToT1SubSamplingMaximumSizeValue( self, fileName ):

    if ( os.path.exists( fileName ) ):

      print 'Check sub-sampling maximum sizes during optimization value...'
      command = 'GkgExecuteCommand VolumeInformation' + \
                ' -i ' + fileName + \
                ' -info sizeX ' + \
                ' -verbosePluginLoading false'
      fd = os.popen( str( command ) )
      maxValue = fd.read()[ : -1 ]
      ret = fd.close()

      command = 'GkgExecuteCommand VolumeInformation' + \
                ' -i ' + fileName + \
                ' -info sizeY ' + \
                ' -verbosePluginLoading false'
      fd = os.popen( str( command ) )
      value = fd.read()[ : -1 ]
      ret = fd.close()

      if( value > maxValue ):

        value = maxValue
      
      command = 'GkgExecuteCommand VolumeInformation' + \
                ' -i ' + fileName + \
                ' -info sizeZ ' + \
                ' -verbosePluginLoading false'
      fd = os.popen( str( command ) )
      value = fd.read()[ : -1 ]
      ret = fd.close()

      if( value > maxValue ):

        value = maxValue

      parameter = self._algorithm.getParameter( 'dwToT1RegistrationParameter' )
      parameterValue = parameter.getValue()
      registrationValue = parameterValue[ 'subSamplingMaximumSizes' ]
      subSamplingMaximumSizes = str( int( maxValue ) / 2 ) + ' ' + str( maxValue )
      if ( registrationValue != subSamplingMaximumSizes ):

        message = 'sub-sampling maximum sizes during optimization \n' + \
                  'should be maximum size value of T1 file, here ' + \
                  subSamplingMaximumSizes + '.\n' + \
                  'Set this parameter as ' + subSamplingMaximumSizes + ' ?'
        answer = QtGui.QMessageBox.question( None,
                                             'Abort',
                                             message,
                                             QtGui.QMessageBox.Yes | \
                                             QtGui.QMessageBox.No )

        if answer == QtGui.QMessageBox.Yes:

          parameterValue[ 'subSamplingMaximumSizes' ] = subSamplingMaximumSizes
          parameter.setValue( parameterValue )

      else:

        print 'No change in ' + \
              'sub-sampling maximum sizes during optimization value.'


  def enableDwToT1ImportAndGenerateWidgets( self, value ):

    pushButtonDwToT1Registration = self._findChild( self._awin,
                                     'pushButton_DwToT1Registration' )
    lineEditImportDwToT1Transformation = self._findChild( self._awin,
                                     'lineEdit_FileNameDwToT1Transformation' )
    pushButtonImportDwToT1Transformation = self._findChild( self._awin,
                                     'pushButton_FileNameDwToT1Transformation' )
    labelImportDwToT1Transformation = self._findChild( self._awin,
                                     'label_FileNameDwToT1Transformation' )

    if ( value ):

      pushButtonDwToT1Registration.setEnabled( True )
      lineEditImportDwToT1Transformation.setEnabled( False )
      pushButtonImportDwToT1Transformation.setEnabled( False )
      labelImportDwToT1Transformation.setEnabled( False )

    else:

      pushButtonDwToT1Registration.setEnabled( False )
      lineEditImportDwToT1Transformation.setEnabled( True )
      pushButtonImportDwToT1Transformation.setEnabled( True )
      labelImportDwToT1Transformation.setEnabled( True )


  def enableMorphologistAPCFile( self, value ):


    lineEditMorphologistAPCFileName = self._findChild( \
                                      self._awin,
                                     'lineEdit_MorphologistAPCFileName' )

    pushButtonMorphologistAPCFileName = self._findChild( \
                                      self._awin,
                                     'pushButton_MorphologistAPCFileName' )

    comboBoxTemplateName = self._findChild( self._awin,
                                            'comboBox_TemplateName' )

    pushButtonNormalization3dSettings = self._findChild( \
                                           self._awin,
                                          'pushButton_Normalization3dSettings' )
    if ( value ):

      lineEditMorphologistAPCFileName.setEnabled( True )
      pushButtonMorphologistAPCFileName.setEnabled( True )
      comboBoxTemplateName.setEnabled( False )
      pushButtonNormalization3dSettings.setEnabled( False )

    else:

      lineEditMorphologistAPCFileName.setEnabled( False )
      pushButtonMorphologistAPCFileName.setEnabled( False )
      comboBoxTemplateName.setEnabled( True )
      pushButtonNormalization3dSettings.setEnabled( True )
