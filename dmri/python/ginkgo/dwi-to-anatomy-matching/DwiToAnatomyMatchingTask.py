from Task import *
import shutil
import glob
from PyQt5 import Qt

class DwiToAnatomyMatchingTask( Task ):

  def __init__( self, application ):

    Task.__init__( self, application )


  ##############################################################################
  # execute()
  ##############################################################################

  def execute( self, functors, parameters, subjectName, verbose, viewOnly ):

    try:

      ####################### acquiring conditions #############################
      functors[ 'condition-acquire' ]( subjectName,
                                      't1-cropping-processed' )
      functors[ 'condition-acquire' ]( subjectName,
                                   't1-normalization3d-morphologist-processed' )
      functors[ 'condition-acquire' ]( subjectName,
                                      't1-to-dw-matching-processed' )
      functors[ 'condition-acquire' ]( subjectName, 'talairach-processed' )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 0 )

      ####################### collecting output work directory #################
      outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()
      if ( verbose ):
      
        print  'Output work directory :', outputWorkDirectory

      ####################### collecting corrected DWI directory ###############
      correctedDwiDirectory = parameters[ 'correctedDwiDirectory' ].getValue()
      if ( verbose ):
      
        print  'Corrected DWI directory :', correctedDwiDirectory
        
      extension = ''
      if ( os.path.exists( os.path.join( correctedDwiDirectory,
                                         'dw_wo_outlier.ima' ) ) ):
                                         
        extension = '_wo_outlier'
        
      elif ( os.path.exists( os.path.join( correctedDwiDirectory,
                                           'dw_wo_susceptibility.ima' ) ) ):
        
        extension = '_wo_susceptibility'
                             
      elif ( os.path.exists( os.path.join( correctedDwiDirectory,
                                      'dw_wo_eddy_current_and_motion.ima' ) ) ):
        
        extension = '_wo_eddy_current_and_motion'
 
 
      fileNameAverageT2 = os.path.join( correctedDwiDirectory,
                                        't2' + extension )
      fileNameDW = os.path.join( correctedDwiDirectory,
                                 'dw' + extension )

      ####################### collecting rough mask directory ##################
      roughMaskDirectory = parameters[ 'roughMaskDirectory' ].getValue()
      if ( verbose ):
      
        print  'Rough mask directory :', roughMaskDirectory

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 5 )


      ####################### cropping T1 ######################################
      fileNameT1 = parameters[ 'fileNameT1' ].getValue()
      t1LeftXCropping = parameters[ 't1LeftXCropping' ].getValue()
      t1RightXCropping = parameters[ 't1RightXCropping' ].getValue()
      t1AnteriorYCropping = parameters[ 't1AnteriorYCropping' ].getValue()
      t1PosteriorYCropping = parameters[ 't1PosteriorYCropping' ].getValue()
      t1HeadZCropping = parameters[ 't1HeadZCropping' ].getValue()
      t1FootZCropping = parameters[ 't1FootZCropping' ].getValue()
      
      fileNameOutT1 = os.path.join( outputWorkDirectory, 't1' )
      fileNameOutT1S16 = os.path.join( outputWorkDirectory, 't1_S16' )

      if ( ( t1LeftXCropping > 0 ) or \
           ( t1RightXCropping > 0 ) or \
           ( t1AnteriorYCropping > 0 ) or \
           ( t1PosteriorYCropping > 0 ) or \
           ( t1HeadZCropping > 0 ) or \
           ( t1FootZCropping > 0 ) ):

        
        fileNameT1SubVolume = os.path.join( outputWorkDirectory,
                                            't1_subvolume' )
        command = 'GkgExecuteCommand SubVolume' + \
                  ' -i ' +  fileNameT1 + \
                  ' -o ' + fileNameOutT1;

        if ( t1RightXCropping > t1LeftXCropping ):
        
          command += ' -x ' + str( t1RightXCropping ) + \
                     ' -X ' + str( t1LeftXCropping )
                     
        if ( t1PosteriorYCropping > t1AnteriorYCropping ):
        
          command += ' -y ' + str( t1AnteriorYCropping ) + \
                     ' -Y ' + str( t1PosteriorYCropping )

        if ( t1FootZCropping > t1HeadZCropping ):
        
          command += ' -z ' + str( t1HeadZCropping ) + \
                     ' -Z ' + str( t1FootZCropping )
                     
        command += ' -verbose true'
        executeCommand( self, subjectName, command, viewOnly )
        
      else:
      
        command = 'GkgExecuteCommand DiskFormatConverter' + \
                  ' -i ' + fileNameT1 + \
                  ' -o ' + fileNameOutT1 + \
                  ' -of gis' + \
                  ' -verbose true'
        executeCommand( self, subjectName, command, viewOnly )
        
      command = 'GkgExecuteCommand Combiner' + \
                ' -i ' + fileNameOutT1 + \
                ' -o ' + fileNameOutT1S16 + \
                ' -num1 1 0 -den1 1 1 ' + \
                ' -t int16_t' + \
                ' -verbose true'
      executeCommand( self, subjectName, command, viewOnly )
      

      ####################### notifying display thread #########################
      functors[ 'condition-notify-and-release' ]( subjectName,
                                                  't1-cropping-processed' )


      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 10 )




      specy = ResourceManager().getCurrentSpecy( 'dmri' )
      templateFlipping = parameters[ 'templateFlipping' ].getValue()

      if ( specy == 'Human' ):

        computeNormalization = \
                       parameters[ 'computeNormalization' ].getValue()

        if ( computeNormalization == 2 ):
        
          #-------------------------- Morphologist -----------------------------
          useMorphologistAPCFile = \
                               parameters[ 'useMorphologistAPCFile' ].getValue()

          fileNameT1ACPCCoordinates = None
          # in case Morphologist APC file is not available, we do
          # the normalization by ourselves
          if ( useMorphologistAPCFile == 0 ):

            ####################### normalizing T1 #############################
            templateName = parameters[ 'templateName' ].getChoice()

            outputNormalization3dDirectory = os.path.join( outputWorkDirectory,
                                                         'Normalization3d' )
            if not( os.path.exists( outputNormalization3dDirectory ) ):

              os.mkdir( outputNormalization3dDirectory )

            fileNameT1ToTemplate = os.path.join( outputNormalization3dDirectory,
                                                 't1_to_template.trm' )
            fileNameT1ToTalairachMNI = os.path.join( \
                                                 outputNormalization3dDirectory,
                                                 't1_to_talairachMNI.trm' )
            fileNameT1ToTalairachACPC = os.path.join( \
                                                 outputNormalization3dDirectory,
                                                 't1_to_talairachACPC.trm' )
            fileNameT1ACPCCoordinates = os.path.join( \
                                                 outputNormalization3dDirectory,
                                                 't1.APC' )

            fileNameTemplateToT1 = os.path.join( outputNormalization3dDirectory,
                                               'template_to_t1.trm' )
            fileNameTalairachMNIToT1 = os.path.join( \
                                                 outputNormalization3dDirectory,
                                                 'talarachMNI_to_t1.trm' )
            fileNameTalairachACPCToT1 = os.path.join( \
                                                 outputNormalization3dDirectory,
                                                 'talarachACPC_to_t1.trm' )
            normalization3dSettings = parameters[ \
                                          'normalization3dSettings' ].getValue()
            similarityMeasureNameSetting = str( parameters[ \
              'normalization3dSettings' ].getChoice( 'similarityMeasureName' ) )
            optimizerNameSetting = str( parameters[ \
              'normalization3dSettings' ].getChoice( 'optimizerName' ) )
            templateLowerThresholdSetting = normalization3dSettings[ \
                                                      'templateLowerThreshold' ]
            floatingLowerThresholdSetting = normalization3dSettings[ \
                                                      'floatingLowerThreshold' ]
            resamplingOrderSetting = normalization3dSettings[ \
                                                             'resamplingOrder' ]
            subSamplingMaximumSizesSetting = normalization3dSettings[ \
                                                     'subSamplingMaximumSizes' ]
            levelCountSetting = normalization3dSettings[ 'levelCount' ]
            applySmoothingSetting = normalization3dSettings[ 'applySmoothing' ]
            maximumIterationCountSetting = normalization3dSettings[ \
                                                       'maximumIterationCount' ]
            initialParametersScalingXSetting = normalization3dSettings[ \
                                                   'initialParametersScalingX' ]
            initialParametersScalingYSetting = normalization3dSettings[ \
                                                   'initialParametersScalingY' ]
            initialParametersScalingZSetting = normalization3dSettings[ \
                                                   'initialParametersScalingZ' ]
            initialParametersShearingXYSetting = normalization3dSettings[ \
                                                 'initialParametersShearingXY' ]
            initialParametersShearingXZSetting = normalization3dSettings[ \
                                                 'initialParametersShearingXZ' ]
            initialParametersShearingYZSetting = normalization3dSettings[ \
                                                 'initialParametersShearingYZ' ]
            initialParametersRotationXSetting = normalization3dSettings[ \
                                                  'initialParametersRotationX' ]
            initialParametersRotationYSetting = normalization3dSettings[ \
                                                  'initialParametersRotationY' ]
            initialParametersRotationZSetting = normalization3dSettings[ \
                                                  'initialParametersRotationZ' ]
            initialParametersTranslationXSetting = normalization3dSettings[ \
                                               'initialParametersTranslationX' ]
            initialParametersTranslationYSetting = normalization3dSettings[ \
                                               'initialParametersTranslationY' ]
            initialParametersTranslationZSetting = normalization3dSettings[ \
                                               'initialParametersTranslationZ' ]
            stoppingCriterionErrorSetting = normalization3dSettings[ \
                                               'stoppingCriterionError' ]
            stepSizeSetting = normalization3dSettings[ 'stepSize' ]
            maximumTestGradientSetting = normalization3dSettings[ \
                                                         'maximumTestGradient' ]
            maximumToleranceSetting = normalization3dSettings[ \
                                                            'maximumTolerance' ]
            optimizerParametersScalingXSetting = normalization3dSettings[ \
                                                 'optimizerParametersScalingX' ]
            optimizerParametersScalingYSetting = normalization3dSettings[ \
                                                 'optimizerParametersScalingY' ]
            optimizerParametersScalingZSetting = normalization3dSettings[ \
                                                 'optimizerParametersScalingZ' ]
            optimizerParametersShearingXYSetting = normalization3dSettings[ \
                                               'optimizerParametersShearingXY' ]
            optimizerParametersShearingXZSetting = normalization3dSettings[ \
                                               'optimizerParametersShearingXZ' ]
            optimizerParametersShearingYZSetting = normalization3dSettings[ \
                                               'optimizerParametersShearingYZ' ]
            optimizerParametersRotationXSetting = normalization3dSettings[ \
                                                'optimizerParametersRotationX' ]
            optimizerParametersRotationYSetting = normalization3dSettings[ \
                                                'optimizerParametersRotationY' ]
            optimizerParametersRotationZSetting = normalization3dSettings[ \
                                                'optimizerParametersRotationZ' ]
            optimizerParametersTranslationXSetting = normalization3dSettings[ \
                                             'optimizerParametersTranslationX' ]
            optimizerParametersTranslationYSetting = normalization3dSettings[ \
                                             'optimizerParametersTranslationY' ]
            optimizerParametersTranslationZSetting = normalization3dSettings[ \
                                             'optimizerParametersTranslationZ' ]


            command = 'GkgExecuteCommand Normalization3d ' + \
                      ' -specy human ' + \
                      ' -template ' + templateName + \
                      ' -floating ' + fileNameOutT1 + \
                      ' -toTemplate ' + fileNameT1ToTemplate + \
                      ' -toTalairachMNI ' + fileNameT1ToTalairachMNI +  \
                      ' -toTalairachACPC ' + fileNameT1ToTalairachACPC +  \
                      ' -fromTemplate ' + fileNameTemplateToT1 + \
                      ' -fromTalairachMNI ' + fileNameTalairachMNIToT1 +  \
                      ' -fromTalairachACPC ' + fileNameTalairachACPCToT1 +  \
                      ' -acpcCoordinates ' + fileNameT1ACPCCoordinates + \
                      ' -optimizerName ' + optimizerNameSetting + \
                      ' -similarityMeasureName ' + \
                                                similarityMeasureNameSetting + \
                      ' -templateLowerThreshold ' + \
                                        str( templateLowerThresholdSetting ) + \
                      ' -floatingLowerThreshold ' + \
                                        str( floatingLowerThresholdSetting ) + \
                      ' -resamplingOrder ' + str( resamplingOrderSetting ) + \
                      ' -subSamplingMaximumSizes ' + \
                                           str( subSamplingMaximumSizesSetting )

            if ( len( templateFlipping ) ):
            
              command += ' -flippings ' + templateFlipping

            if ( similarityMeasureNameSetting != 'correlation-coefficient' ):

              command += ' -similarityMeasureParameters ' + \
                         str( levelCountSetting ) + ' ' + \
                         str( int( applySmoothingSetting ) )

            command +=  ' -initialParameters ' + \
                                 str( initialParametersScalingXSetting ) + \
                           ' ' + str( initialParametersScalingYSetting ) + \
                           ' ' + str( initialParametersScalingZSetting ) + \
                           ' ' + str( initialParametersShearingXYSetting ) + \
                           ' ' + str( initialParametersShearingXZSetting ) + \
                           ' ' + str( initialParametersShearingYZSetting ) + \
                           ' ' + str( initialParametersRotationXSetting ) + \
                           ' ' + str( initialParametersRotationYSetting ) + \
                           ' ' + str( initialParametersRotationZSetting ) + \
                           ' ' + str( initialParametersTranslationXSetting ) + \
                           ' ' + str( initialParametersTranslationYSetting ) + \
                           ' ' + str( initialParametersTranslationZSetting )

            command += ' -optimizerParameters ' + \
                       str( maximumIterationCountSetting )
            if ( optimizerNameSetting == 'nelder-mead' ):

              command += ' ' + str( stoppingCriterionErrorSetting ) + \
                         ' ' + str( optimizerParametersScalingXSetting ) + \
                         ' ' + str( optimizerParametersScalingYSetting ) + \
                         ' ' + str( optimizerParametersScalingZSetting ) + \
                         ' ' + str( optimizerParametersShearingXYSetting ) + \
                         ' ' + str( optimizerParametersShearingXZSetting ) + \
                         ' ' + str( optimizerParametersShearingYZSetting ) + \
                         ' ' + str( optimizerParametersRotationXSetting ) + \
                         ' ' + str( optimizerParametersRotationYSetting ) + \
                         ' ' + str( optimizerParametersRotationZSetting ) + \
                         ' ' + str( optimizerParametersTranslationXSetting ) + \
                         ' ' + str( optimizerParametersTranslationYSetting ) + \
                         ' ' + str( optimizerParametersTranslationZSetting )

            else:

              command += ' ' + str( stepSizeSetting ) + \
                         ' ' + str( maximumTestGradientSetting ) + \
                         ' ' + str( maximumToleranceSetting )

            command += ' -verbose true'

            executeCommand( self, subjectName, command, viewOnly )
          
          # in case a Morphologist APC file is available, we use it to
          # get the AC/PC coordinates
          else:
        
            print 'using Morphologist *.APC file'

            fileNameSourceT1ACPCCoordinates = \
                          parameters[ 'morphologistAPCFileName' ].getValue()

            if not( os.path.exists( fileNameSourceT1ACPCCoordinates ) ):

              raise RuntimeError, fileNameSourceT1ACPCCoordinates + \
                  ' cannot be read'

            outputNormalization3dDirectory = os.path.join( outputWorkDirectory,
                                                          'Normalization3d' )
            if not( os.path.exists( outputNormalization3dDirectory ) ):

              os.mkdir( outputNormalization3dDirectory )

            fileNameT1ACPCCoordinates = os.path.join( \
                                                 outputNormalization3dDirectory,
                                                 't1.APC' )
            shutil.copyfile( fileNameSourceT1ACPCCoordinates,
                             fileNameT1ACPCCoordinates )


          #################### extracting unbiased anatomy, histogram analysis, 
          # and computing Voronoi mask #########################################

          outputMorphologistDirectory = os.path.join( outputWorkDirectory,
                                                      'Morphologist' )
          if not( os.path.exists( outputMorphologistDirectory ) ):

            os.mkdir( outputMorphologistDirectory )

          fileNameUnbiasedT1 = os.path.join( outputMorphologistDirectory,
                                             'nobias_t1' )
          fileNameBiasFieldT1 = os.path.join( outputMorphologistDirectory,
                                              'biasfield_t1' )
          fileNameWhiteRidgeT1 = os.path.join( outputMorphologistDirectory,
                                               'whiteridge_t1' )
          fileNameEdgesT1 = os.path.join( outputMorphologistDirectory,
                                          'edges_t1' )
          fileNameVarianceT1 = os.path.join( outputMorphologistDirectory,
                                             'variance_t1' )
          fileNameMeanCurvatureT1 = os.path.join( outputMorphologistDirectory,
                                                  'mean_curvature_t1' )
          fileNameHFilteredT1 = os.path.join( outputMorphologistDirectory,
                                              'hfiltered_t1' )

          command = 'VipT1BiasCorrection' +  \
                    ' -i ' + fileNameOutT1S16 + \
                    ' -o ' + fileNameUnbiasedT1 + '.ima' + \
                    ' -Fwrite no ' + \
                    ' -field ' + fileNameBiasFieldT1 + '.ima' + \
                    ' -Wwrite yes ' + \
                    ' -wridge ' + fileNameWhiteRidgeT1 + '.ima' + \
                    ' -Kregul 20.0' + \
                    ' -sampling 16.0' + \
                    ' -Kcrest 20.0' + \
                    ' -Grid 2' + \
                    ' -ZregulTuning 0.5' + \
                    ' -vp 75' + \
                    ' -e 3' + \
                    ' -eWrite yes' + \
                    ' -ename ' + fileNameEdgesT1 + '.ima' +  \
                    ' -vWrite yes' + \
                    ' -vname ' + fileNameVarianceT1 + '.ima' + \
                    ' -mWrite no' +  \
                    ' -mname ' + fileNameMeanCurvatureT1 + '.ima' + \
                    ' -hWrite yes' + \
                    ' -hname ' + fileNameHFilteredT1 + '.ima' +  \
                    ' -Last \'auto (AC/PC Points needed)\'' + \
                    ' -Points ' + fileNameT1ACPCCoordinates
          executeCommand( self, subjectName, command, viewOnly )

          fileNameHistogramAnalysis = os.path.join( outputMorphologistDirectory,
                                                    't1.han' )
          command = 'VipHistoAnalysis' + \
                    ' -i ' + fileNameUnbiasedT1 + '.ima' + \
                    ' -o ' + fileNameHistogramAnalysis + \
                    ' -Save y ' + \
                    ' -Mask ' + fileNameHFilteredT1 + '.ima' + \
                    ' -Ridge ' + fileNameWhiteRidgeT1 + '.ima' +  \
                    ' -mode i'
          executeCommand( self, subjectName, command, viewOnly )


          fileNameBrainT1 = os.path.join( outputMorphologistDirectory,
                                          'brain_t1' )
          command = 'VipGetBrain' + \
                    ' -i ' + fileNameUnbiasedT1 + '.ima' + \
                    ' -berosion 1.8' + \
                    ' -analyse r ' + \
                    ' -hname ' + fileNameHistogramAnalysis + \
                    ' -bname ' + fileNameBrainT1 + '.ima' + \
                    ' -First 0 ' + \
                    ' -Last 0 ' + \
                    ' -layer 0 ' + \
                    ' -Points ' + fileNameT1ACPCCoordinates + \
                    ' -m V ' + \
                    ' -Variancename ' + fileNameVarianceT1 + '.ima' +  \
                    ' -Edgesname ' + fileNameEdgesT1 + '.ima' +  \
                    ' -Ridge ' + fileNameWhiteRidgeT1 + '.ima'
          executeCommand( self, subjectName, command, viewOnly )


          fileNameVoronoiMask = os.path.join( outputMorphologistDirectory,
                                              'voronoi_t1' )
          fileNameClosedVoronoi = ResourceManager().getTemplatesPath( \
                                                           specy,
                                                           'closedvoronoi.ima' )
          command = 'VipSplitBrain' + \
                    ' -input ' + fileNameUnbiasedT1 + '.ima' + \
                    ' -brain ' + fileNameBrainT1 + '.ima' + \
                    ' -analyse r ' + \
                    ' -hname ' + fileNameHistogramAnalysis + \
                    ' -output ' + fileNameVoronoiMask + '.ima' + \
                    ' -mode \'Watershed (2011)\'' + \
                    ' -erosion 2.0 ' + \
                    ' -ccsize 500 ' + \
                    ' -Points ' + fileNameT1ACPCCoordinates + \
                    ' -walgo b ' + \
                    ' -Bary 0.6 ' + \
                    ' -template ' + fileNameClosedVoronoi + \
                    ' -TemplateUse y ' + \
                    ' -Ridge ' + fileNameWhiteRidgeT1
          executeCommand( self, subjectName, command, viewOnly )

          fileNameLeftAndRightHemisphereMask = os.path.join(
                                            outputMorphologistDirectory,
                                            'left_and_right_hemispheres_mask' )

          command = 'GkgExecuteCommand Binarizer' + \
                    ' -i ' + fileNameVoronoiMask + \
                    ' -o ' + fileNameLeftAndRightHemisphereMask + \
                    ' -m  be ' + \
                    ' -t1 1 ' + \
                    ' -t2 2 ' + \
                    ' -f 32767 ' + \
                    ' -verbose true'
          executeCommand( self, subjectName, command, viewOnly )

          fileNameTalairachTransform = os.path.join(
                            outputMorphologistDirectory,
                          'RawT1-t1_default_acquisition_TO_Talairach-ACPC.trm' )

          command = 'VipTalairachTransform' + \
                    ' -i ' + fileNameT1ACPCCoordinates + \
                    ' -o ' + fileNameTalairachTransform + \
                    ' -m ' + fileNameLeftAndRightHemisphereMask
          executeCommand( self, subjectName, command, viewOnly )
  
          for f in glob.glob( os.path.join( outputMorphologistDirectory,
                                            '*.minf' ) ):
            os.remove( f )
          
        #-------------------------- Freesurfer ---------------------------------
        applyFreesurferReconAll = \
                       parameters[ 'applyFreesurferReconAll' ].getValue()
                       
        if ( applyFreesurferReconAll == 2 ):
      
          outputFreesurferDirectory = os.path.join( outputWorkDirectory,
                                                    'Freesurfer' )
          if not( os.path.exists( outputFreesurferDirectory ) ):

            os.mkdir( outputFreesurferDirectory )

          #- NIFTI convertion - - - - - - - - - - - - - - - - - - - - - - - - - 

          outputFreesurfer01InputDirectory = os.path.join( \
                                                      outputFreesurferDirectory,
                                                      '01-Input' )
          if not( os.path.exists( outputFreesurfer01InputDirectory ) ):

            os.mkdir( outputFreesurfer01InputDirectory )

          fileNameNiftiT1S16 = os.path.join( outputFreesurfer01InputDirectory,
                                             't1_S16.nii' )
          command = 'GkgExecuteCommand Gis2NiftiConverter ' + \
                    ' -i ' + fileNameOutT1S16 + \
                    ' -o ' + fileNameNiftiT1S16 + \
                    ' -verbose' + \
                    ''
          executeCommand( self, subjectName, command, viewOnly )

          fileNameNiftiT1S16Minf = os.path.join( \
                                               outputFreesurfer01InputDirectory,
                                               't1_S16.nii.minf' )
          if not viewOnly:
 
            os.remove( fileNameNiftiT1S16Minf )

          #- recon-all - - - -- - - - - - - - - - - - - - - - - - - - - - - - - 

          outputFreesurfer02ReconAll = os.path.join( outputFreesurferDirectory,
                                                     '02-ReconAll' )
          if not( os.path.exists( outputFreesurfer02ReconAll ) ):

            os.mkdir( outputFreesurfer02ReconAll )

          command = 'recon-all -all' + \
                    ' -subjid ' + subjectName + \
                    ' -i ' + fileNameNiftiT1S16 + \
                    ' -sd ' + outputFreesurfer02ReconAll
          executeCommand( self, subjectName, command, viewOnly )

          #- resampling to icosahedron of order 7 mesh  - - - - - - - - - - - - 

          outputFreesurfer03ToIcoOrder7 = os.path.join( \
                                                      outputFreesurferDirectory,
                                                      '03-ToIcosahedronOrder7' )
          if not( os.path.exists( outputFreesurfer03ToIcoOrder7 ) ):

            os.mkdir( outputFreesurfer03ToIcoOrder7 )
          
          command = 'export SUBJECTS_DIR=' + outputFreesurfer02ReconAll + \
                    ';mri_surf2surf' + \
                    ' --sval-xyz ' + 'pial' + \
                    ' --srcsubject ' + subjectName + \
                    ' --trgsubject ico ' + \
                    ' --trgicoorder 7 ' + \
                    ' --tval ' + os.path.join( outputFreesurfer03ToIcoOrder7,
                                               'lh.pial' ) + \
                    ' --tval-xyz ' + \
                    ' --hemi ' + 'lh' + \
                    ' --s ' + subjectName
          executeCommand( self, subjectName, command, viewOnly )

          command = 'export SUBJECTS_DIR=' + outputFreesurfer02ReconAll + \
                    ';mri_surf2surf' + \
                    ' --sval-xyz ' + 'white' + \
                    ' --srcsubject ' + subjectName + \
                    ' --trgsubject ico ' + \
                    ' --trgicoorder 7 ' + \
                    ' --tval ' + os.path.join( outputFreesurfer03ToIcoOrder7,
                                               'lh.white' ) + \
                    ' --tval-xyz ' + \
                    ' --hemi ' + 'lh' + \
                    ' --s ' + subjectName
          executeCommand( self, subjectName, command, viewOnly )

          command = 'export SUBJECTS_DIR=' + outputFreesurfer02ReconAll + \
                    ';mri_surf2surf' + \
                    ' --sval-xyz ' + 'pial' + \
                    ' --srcsubject ' + subjectName + \
                    ' --trgsubject ico ' + \
                    ' --trgicoorder 7 ' + \
                    ' --tval ' + os.path.join( outputFreesurfer03ToIcoOrder7,
                                               'rh.pial' ) + \
                    ' --tval-xyz ' + \
                    ' --hemi ' + 'rh' + \
                    ' --s ' + subjectName
          executeCommand( self, subjectName, command, viewOnly )

          command = 'export SUBJECTS_DIR=' + outputFreesurfer02ReconAll + \
                    ';mri_surf2surf' + \
                    ' --sval-xyz ' + 'white' + \
                    ' --srcsubject ' + subjectName + \
                    ' --trgsubject ico ' + \
                    ' --trgicoorder 7 ' + \
                    ' --tval ' + os.path.join( outputFreesurfer03ToIcoOrder7,
                                               'rh.white' ) + \
                    ' --tval-xyz ' + \
                    ' --hemi ' + 'rh' + \
                    ' --s ' + subjectName
          executeCommand( self, subjectName, command, viewOnly )

          command = 'export SUBJECTS_DIR=' + outputFreesurfer02ReconAll + \
                    ';mri_surf2surf' + \
                    ' --srcsubject ' + subjectName + \
                    ' --trgsubject ico ' + \
                    ' --trgicoorder 7 ' + \
                    ' --tval ' + os.path.join( outputFreesurfer03ToIcoOrder7,
                                               'lh.aparc.a2009s.annot' ) + \
                   ' --sval-annot ' + os.path.join( outputFreesurfer02ReconAll,
                                                    subjectName,
                                                    'label',
                                                    'lh.aparc.a2009s.annot' )+ \
                   ' --hemi ' + 'lh' + \
                   ' --s ' + subjectName
          executeCommand( self, subjectName, command, viewOnly )

          command = 'export SUBJECTS_DIR=' + outputFreesurfer02ReconAll + \
                    ';mri_surf2surf' + \
                    ' --srcsubject ' + subjectName + \
                    ' --trgsubject ico ' + \
                    ' --trgicoorder 7 ' + \
                    ' --tval ' + os.path.join( outputFreesurfer03ToIcoOrder7,
                                               'rh.aparc.a2009s.annot' ) + \
                   ' --sval-annot ' + os.path.join( outputFreesurfer02ReconAll,
                                                    subjectName,
                                                    'label',
                                                    'rh.aparc.a2009s.annot' ) +\
                   ' --hemi ' + 'rh' + \
                   ' --s ' + subjectName
          executeCommand( self, subjectName, command, viewOnly )

          #- converting to GIFTI and NIFTI formats - - - - - - - - - - - - - - -

          outputFreesurfer04GiftiAndNifti = os.path.join( \
                                                      outputFreesurferDirectory,
                                                      '04-GiftiAndNifti' )
          if not( os.path.exists( outputFreesurfer04GiftiAndNifti ) ):

            os.mkdir( outputFreesurfer04GiftiAndNifti )

          command = 'mri_convert' + \
                    ' --resample_type nearest ' + \
                    ' --reslice_like ' + os.path.join( \
                                                     outputFreesurfer02ReconAll,
                                                     subjectName,
                                                     'mri',
                                                     'rawavg.mgz' ) + ' ' + \
                   os.path.join( outputFreesurfer02ReconAll,
                                 subjectName,
                                 'mri',
                                 'aseg.mgz' ) + ' ' + \
                   os.path.join( outputFreesurfer04GiftiAndNifti,
                                 'aseg.nii.gz' ) + ' ' + \
                   ' -odt int'
          executeCommand( self, subjectName, command, viewOnly )

          command = 'mri_convert' + \
                    ' --resample_type nearest ' + \
                    ' --reslice_like ' + os.path.join( \
                                                     outputFreesurfer02ReconAll,
                                                     subjectName,
                                                     'mri',
                                                     'rawavg.mgz' ) + ' ' + \
                   os.path.join( outputFreesurfer02ReconAll,
                                 subjectName,
                                 'mri',
                                 'aparc+aseg.mgz' ) + ' ' + \
                   os.path.join( outputFreesurfer04GiftiAndNifti,
                                 'aparc+aseg.nii.gz' ) + ' ' + \
                   ' -odt int'
          executeCommand( self, subjectName, command, viewOnly )

          command = 'mri_convert' + \
                    ' --resample_type nearest ' + \
                    ' --reslice_like ' + os.path.join( \
                                                     outputFreesurfer02ReconAll,
                                                     subjectName,
                                                     'mri',
                                                     'rawavg.mgz' ) + ' ' + \
                   os.path.join( outputFreesurfer02ReconAll,
                                 subjectName,
                                 'mri',
                                 'aparc.a2009s+aseg.mgz' ) + ' ' + \
                   os.path.join( outputFreesurfer04GiftiAndNifti,
                                 'aparc.a2009s+aseg.nii.gz' ) + ' ' + \
                   ' -odt int'
          executeCommand( self, subjectName, command, viewOnly )

          command = 'mri_convert' + \
                    ' --resample_type nearest ' + \
                    ' --reslice_like ' + os.path.join( \
                                                     outputFreesurfer02ReconAll,
                                                     subjectName,
                                                     'mri',
                                                     'rawavg.mgz' ) + ' ' + \
                   os.path.join( outputFreesurfer02ReconAll,
                                 subjectName,
                                 'mri',
                                 'wmparc.mgz' ) + ' ' + \
                   os.path.join( outputFreesurfer04GiftiAndNifti,
                                 'wmparc.nii.gz' ) + ' ' + \
                   ' -odt int'
          executeCommand( self, subjectName, command, viewOnly )

          command = 'mris_convert ' + \
                    os.path.join( outputFreesurfer03ToIcoOrder7,
                                  'lh.pial' ) + ' ' + \
                    os.path.join( outputFreesurfer04GiftiAndNifti,
                                  'lh.pial.gii' )
          executeCommand( self, subjectName, command, viewOnly )

          command = 'mris_convert ' + \
                    os.path.join( outputFreesurfer03ToIcoOrder7,
                                  'lh.white' ) + ' ' + \
                    os.path.join( outputFreesurfer04GiftiAndNifti,
                                  'lh.white.gii' )
          executeCommand( self, subjectName, command, viewOnly )

          command = 'mris_convert ' + \
                    os.path.join( outputFreesurfer03ToIcoOrder7,
                                  'rh.pial' ) + ' ' + \
                    os.path.join( outputFreesurfer04GiftiAndNifti,
                                  'rh.pial.gii' )
          executeCommand( self, subjectName, command, viewOnly )

          command = 'mris_convert ' + \
                    os.path.join( outputFreesurfer03ToIcoOrder7,
                                  'rh.white' ) + ' ' + \
                    os.path.join( outputFreesurfer04GiftiAndNifti,
                                  'rh.white.gii' )
          executeCommand( self, subjectName, command, viewOnly )

          command = 'mris_convert ' + \
                    ' --annot ' + os.path.join( \
                                             outputFreesurfer03ToIcoOrder7,
                                             'lh.aparc.a2009s.annot' ) + ' ' + \
                    os.path.join( outputFreesurfer03ToIcoOrder7,
                                  'lh.pial' ) + ' ' + \
                    os.path.join( outputFreesurfer04GiftiAndNifti,
                                  'lh.aparc.a2009s.annot.gii' )
          executeCommand( self, subjectName, command, viewOnly )

          command = 'mris_convert ' + \
                    ' --annot ' + os.path.join( \
                                             outputFreesurfer03ToIcoOrder7,
                                             'rh.aparc.a2009s.annot' ) + ' ' + \
                    os.path.join( outputFreesurfer03ToIcoOrder7,
                                  'rh.pial' ) + ' ' + \
                    os.path.join( outputFreesurfer04GiftiAndNifti,
                                  'rh.aparc.a2009s.annot.gii' )
          executeCommand( self, subjectName, command, viewOnly )

          #- results into subject native space and GIS/*.mesh/*.tex format - - -

          outputFreesurfer05ToNativeSpace = os.path.join( \
                                                      outputFreesurferDirectory,
                                                      '05-ToNativeSpace' )
          if not( os.path.exists( outputFreesurfer05ToNativeSpace ) ):

            os.mkdir( outputFreesurfer05ToNativeSpace )

          command = 'GkgExecuteCommand Nifti2GisConverter ' + \
                    ' -i ' + os.path.join( outputFreesurfer04GiftiAndNifti,
                                           'aseg.nii.gz' ) + ' ' + \
                    ' -o ' + os.path.join( outputFreesurfer05ToNativeSpace,
                                           'aseg.ima' ) + ' ' + \
                    ' -verbose'
          executeCommand( self, subjectName, command, viewOnly )

          command = 'GkgExecuteCommand Nifti2GisConverter ' + \
                    ' -i ' + os.path.join( outputFreesurfer04GiftiAndNifti,
                                           'aparc+aseg.nii.gz' ) + ' ' + \
                    ' -o ' + os.path.join( outputFreesurfer05ToNativeSpace,
                                           'aparc+aseg.ima' ) + ' ' + \
                    ' -verbose'
          executeCommand( self, subjectName, command, viewOnly )

          command = 'GkgExecuteCommand Nifti2GisConverter ' + \
                    ' -i ' + os.path.join( \
                                          outputFreesurfer04GiftiAndNifti,
                                          'aparc.a2009s+aseg.nii.gz' ) + ' ' + \
                    ' -o ' + os.path.join( outputFreesurfer05ToNativeSpace,
                                           'aparc.a2009s+aseg.ima' ) + ' ' + \
                    ' -verbose'
          executeCommand( self, subjectName, command, viewOnly )

          command = 'GkgExecuteCommand Nifti2GisConverter ' + \
                    ' -i ' + os.path.join( outputFreesurfer04GiftiAndNifti,
                                           'wmparc.nii.gz' ) + ' ' + \
                    ' -o ' + os.path.join( outputFreesurfer05ToNativeSpace,
                                           'wmparc.ima' ) + ' ' + \
                    ' -verbose'
          executeCommand( self, subjectName, command, viewOnly )

          command = 'AimsFileConvert ' + \
                    ' -i ' + os.path.join( outputFreesurfer04GiftiAndNifti,
                                           'lh.pial.gii' ) + ' ' + \
                    ' -o ' + os.path.join( outputFreesurfer05ToNativeSpace,
                                           'lh.pial.mesh' )
          executeCommand( self, subjectName, command, viewOnly )

          command = 'AimsFileConvert ' + \
                    ' -i ' + os.path.join( outputFreesurfer04GiftiAndNifti,
                                           'lh.white.gii' ) + ' ' + \
                    ' -o ' + os.path.join( outputFreesurfer05ToNativeSpace,
                                           'lh.white.mesh' )
          executeCommand( self, subjectName, command, viewOnly )

          command = 'AimsFileConvert ' + \
                    ' -i ' + os.path.join( outputFreesurfer04GiftiAndNifti,
                                           'rh.pial.gii' ) + ' ' + \
                    ' -o ' + os.path.join( outputFreesurfer05ToNativeSpace,
                                           'rh.pial.mesh' )
          executeCommand( self, subjectName, command, viewOnly )

          command = 'AimsFileConvert ' + \
                    ' -i ' + os.path.join( outputFreesurfer04GiftiAndNifti,
                                           'rh.white.gii' ) + ' ' + \
                    ' -o ' + os.path.join( outputFreesurfer05ToNativeSpace,
                                           'rh.white.mesh' )
          executeCommand( self, subjectName, command, viewOnly )

          command = 'AimsFileConvert ' + \
                    ' -i ' + os.path.join( \
                                          outputFreesurfer04GiftiAndNifti,
                                         'lh.aparc.a2009s.annot.gii' ) + ' ' + \
                    ' -o ' + os.path.join( outputFreesurfer05ToNativeSpace,
                                           'lh.aparc.a2009s.annot.tex' )
          executeCommand( self, subjectName, command, viewOnly )

          command = 'AimsFileConvert ' + \
                    ' -i ' + os.path.join( \
                                         outputFreesurfer04GiftiAndNifti,
                                         'rh.aparc.a2009s.annot.gii' ) + ' ' + \
                    ' -o ' + os.path.join( outputFreesurfer05ToNativeSpace,
                                           'rh.aparc.a2009s.annot.tex' )
          executeCommand( self, subjectName, command, viewOnly )

          if not viewOnly:
 
            os.remove( os.path.join( outputFreesurfer05ToNativeSpace,
                                     'lh.pial.mesh.minf' ) )
            os.remove( os.path.join( outputFreesurfer05ToNativeSpace,
                                     'lh.white.mesh.minf' ) )
            os.remove( os.path.join( outputFreesurfer05ToNativeSpace,
                                     'rh.pial.mesh.minf' ) )
            os.remove( os.path.join( outputFreesurfer05ToNativeSpace,
                                     'rh.white.mesh.minf' ) )
            os.remove( os.path.join( outputFreesurfer05ToNativeSpace,
                                     'lh.aparc.a2009s.annot.tex.minf' ) )
            os.remove( os.path.join( outputFreesurfer05ToNativeSpace,
                                     'rh.aparc.a2009s.annot.tex.minf' ) )

          command = 'GkgExecuteCommand GetTransform3dFromVolumeHeader ' + \
                    ' -i ' + os.path.join( outputFreesurfer05ToNativeSpace,
                                           'aseg.ima' ) + ' ' + \
                    ' -o ' + os.path.join( outputFreesurfer05ToNativeSpace,
                                           'transformation.trm' )+ ' ' + \
                    ' -verbose'
          executeCommand( self, subjectName, command, viewOnly )

          if not viewOnly:
 
            os.remove( os.path.join( outputFreesurfer05ToNativeSpace,
                                   'aseg.ima.minf' ) )
            os.remove( os.path.join( outputFreesurfer05ToNativeSpace,
                                     'aparc+aseg.ima.minf' ) )
            os.remove( os.path.join( outputFreesurfer05ToNativeSpace,
                                     'aparc.a2009s+aseg.ima.minf' ) )
            os.remove( os.path.join( outputFreesurfer05ToNativeSpace,
                                     'wmparc.ima.minf' ) )

          command = 'GkgExecuteCommand MeshMapTransform3d ' + \
                    ' -i ' + os.path.join( outputFreesurfer05ToNativeSpace,
                                           'lh.pial.mesh' ) + ' ' + \
                    ' -t ' + os.path.join( outputFreesurfer05ToNativeSpace,
                                           'transformation1.trm' )+ ' ' + \
                    ' -o ' + os.path.join( outputFreesurfer05ToNativeSpace,
                                           'lh.pial.mesh' ) + ' ' + \
                    ' -verbose'
          executeCommand( self, subjectName, command, viewOnly )

          command = 'GkgExecuteCommand MeshMapTransform3d ' + \
                    ' -i ' + os.path.join( outputFreesurfer05ToNativeSpace,
                                           'lh.white.mesh' ) + ' ' + \
                    ' -t ' + os.path.join( outputFreesurfer05ToNativeSpace,
                                           'transformation1.trm' )+ ' ' + \
                    ' -o ' + os.path.join( outputFreesurfer05ToNativeSpace,
                                           'lh.white.mesh' ) + ' ' + \
                    ' -verbose'
          executeCommand( self, subjectName, command, viewOnly )

          command = 'GkgExecuteCommand MeshMapTransform3d ' + \
                    ' -i ' + os.path.join( outputFreesurfer05ToNativeSpace,
                                           'rh.pial.mesh' ) + ' ' + \
                    ' -t ' + os.path.join( outputFreesurfer05ToNativeSpace,
                                           'transformation1.trm' )+ ' ' + \
                    ' -o ' + os.path.join( outputFreesurfer05ToNativeSpace,
                                           'rh.pial.mesh' ) + ' ' + \
                    ' -verbose'
          executeCommand( self, subjectName, command, viewOnly )

          command = 'GkgExecuteCommand MeshMapTransform3d ' + \
                    ' -i ' + os.path.join( outputFreesurfer05ToNativeSpace,
                                           'rh.white.mesh' ) + ' ' + \
                    ' -t ' + os.path.join( outputFreesurfer05ToNativeSpace,
                                           'transformation1.trm' )+ ' ' + \
                    ' -o ' + os.path.join( outputFreesurfer05ToNativeSpace,
                                           'rh.white.mesh' ) + ' ' + \
                    ' -verbose'
          executeCommand( self, subjectName, command, viewOnly )

          if not viewOnly:
 
            os.remove( os.path.join( outputFreesurfer05ToNativeSpace,
                                   'lh.pial.mesh.minf' ) )
            os.remove( os.path.join( outputFreesurfer05ToNativeSpace,
                                     'lh.white.mesh.minf' ) )
            os.remove( os.path.join( outputFreesurfer05ToNativeSpace,
                                     'rh.pial.mesh.minf' ) )
            os.remove( os.path.join( outputFreesurfer05ToNativeSpace,
                                     'rh.white.mesh.minf' ) )


      elif ( specy == 'Mouse' ):

        computeNormalization = \
                       parameters[ 'computeNormalization' ].getValue()

        if ( computeNormalization == 2 ):

          outputNormalization3dDirectory = os.path.join( outputWorkDirectory,
                                                       'Normalization3d' )
          if not( os.path.exists( outputNormalization3dDirectory ) ):

            os.mkdir( outputNormalization3dDirectory )
        
          ####################### normalizing T1 ###############################
          templateName = parameters[ 'templateName' ].getChoice()


          fileNameT1ToTemplate = os.path.join( outputNormalization3dDirectory,
                                               't1_to_template.trm' )
          fileNameTemplateToT1 = os.path.join( outputNormalization3dDirectory,
                                             'template_to_t1.trm' )
          normalization3dSettings = parameters[ \
                                        'normalization3dSettings' ].getValue()
          similarityMeasureNameSetting = str( parameters[ \
            'normalization3dSettings' ].getChoice( 'similarityMeasureName' ) )
          optimizerNameSetting = str( parameters[ \
            'normalization3dSettings' ].getChoice( 'optimizerName' ) )
          templateLowerThresholdSetting = normalization3dSettings[ \
                                                    'templateLowerThreshold' ]
          floatingLowerThresholdSetting = normalization3dSettings[ \
                                                    'floatingLowerThreshold' ]
          resamplingOrderSetting = normalization3dSettings[ \
                                                           'resamplingOrder' ]
          subSamplingMaximumSizesSetting = normalization3dSettings[ \
                                                   'subSamplingMaximumSizes' ]
          levelCountSetting = normalization3dSettings[ 'levelCount' ]
          applySmoothingSetting = normalization3dSettings[ 'applySmoothing' ]
          maximumIterationCountSetting = normalization3dSettings[ \
                                                     'maximumIterationCount' ]
          initialParametersScalingXSetting = normalization3dSettings[ \
                                                 'initialParametersScalingX' ]
          initialParametersScalingYSetting = normalization3dSettings[ \
                                                 'initialParametersScalingY' ]
          initialParametersScalingZSetting = normalization3dSettings[ \
                                                 'initialParametersScalingZ' ]
          initialParametersShearingXYSetting = normalization3dSettings[ \
                                               'initialParametersShearingXY' ]
          initialParametersShearingXZSetting = normalization3dSettings[ \
                                               'initialParametersShearingXZ' ]
          initialParametersShearingYZSetting = normalization3dSettings[ \
                                               'initialParametersShearingYZ' ]
          initialParametersRotationXSetting = normalization3dSettings[ \
                                                'initialParametersRotationX' ]
          initialParametersRotationYSetting = normalization3dSettings[ \
                                                'initialParametersRotationY' ]
          initialParametersRotationZSetting = normalization3dSettings[ \
                                                'initialParametersRotationZ' ]
          initialParametersTranslationXSetting = normalization3dSettings[ \
                                             'initialParametersTranslationX' ]
          initialParametersTranslationYSetting = normalization3dSettings[ \
                                             'initialParametersTranslationY' ]
          initialParametersTranslationZSetting = normalization3dSettings[ \
                                             'initialParametersTranslationZ' ]
          stoppingCriterionErrorSetting = normalization3dSettings[ \
                                             'stoppingCriterionError' ]
          stepSizeSetting = normalization3dSettings[ 'stepSize' ]
          maximumTestGradientSetting = normalization3dSettings[ \
                                                       'maximumTestGradient' ]
          maximumToleranceSetting = normalization3dSettings[ \
                                                          'maximumTolerance' ]
          optimizerParametersScalingXSetting = normalization3dSettings[ \
                                               'optimizerParametersScalingX' ]
          optimizerParametersScalingYSetting = normalization3dSettings[ \
                                               'optimizerParametersScalingY' ]
          optimizerParametersScalingZSetting = normalization3dSettings[ \
                                               'optimizerParametersScalingZ' ]
          optimizerParametersShearingXYSetting = normalization3dSettings[ \
                                             'optimizerParametersShearingXY' ]
          optimizerParametersShearingXZSetting = normalization3dSettings[ \
                                             'optimizerParametersShearingXZ' ]
          optimizerParametersShearingYZSetting = normalization3dSettings[ \
                                             'optimizerParametersShearingYZ' ]
          optimizerParametersRotationXSetting = normalization3dSettings[ \
                                              'optimizerParametersRotationX' ]
          optimizerParametersRotationYSetting = normalization3dSettings[ \
                                              'optimizerParametersRotationY' ]
          optimizerParametersRotationZSetting = normalization3dSettings[ \
                                              'optimizerParametersRotationZ' ]
          optimizerParametersTranslationXSetting = normalization3dSettings[ \
                                           'optimizerParametersTranslationX' ]
          optimizerParametersTranslationYSetting = normalization3dSettings[ \
                                           'optimizerParametersTranslationY' ]
          optimizerParametersTranslationZSetting = normalization3dSettings[ \
                                           'optimizerParametersTranslationZ' ]


          command = 'GkgExecuteCommand Normalization3d ' + \
                    ' -specy mouse ' + \
                    ' -template ' + templateName + \
                    ' -floating ' + fileNameOutT1 + \
                    ' -toTemplate ' + fileNameT1ToTemplate + \
                    ' -fromTemplate ' + fileNameTemplateToT1 + \
                    ' -optimizerName ' + optimizerNameSetting + \
                    ' -similarityMeasureName ' + \
                                              similarityMeasureNameSetting + \
                    ' -templateLowerThreshold ' + \
                                      str( templateLowerThresholdSetting ) + \
                    ' -floatingLowerThreshold ' + \
                                      str( floatingLowerThresholdSetting ) + \
                    ' -resamplingOrder ' + str( resamplingOrderSetting ) + \
                    ' -subSamplingMaximumSizes ' + \
                                         str( subSamplingMaximumSizesSetting )

          if ( len( templateFlipping ) ):
            
            command += ' -flippings ' + templateFlipping


          if ( similarityMeasureNameSetting != 'correlation-coefficient' ):

            command += ' -similarityMeasureParameters ' + \
                       str( levelCountSetting ) + ' ' + \
                       str( int( applySmoothingSetting ) )

          command +=  ' -initialParameters ' + \
                               str( initialParametersScalingXSetting ) + \
                         ' ' + str( initialParametersScalingYSetting ) + \
                         ' ' + str( initialParametersScalingZSetting ) + \
                         ' ' + str( initialParametersShearingXYSetting ) + \
                         ' ' + str( initialParametersShearingXZSetting ) + \
                         ' ' + str( initialParametersShearingYZSetting ) + \
                         ' ' + str( initialParametersRotationXSetting ) + \
                         ' ' + str( initialParametersRotationYSetting ) + \
                         ' ' + str( initialParametersRotationZSetting ) + \
                         ' ' + str( initialParametersTranslationXSetting ) + \
                         ' ' + str( initialParametersTranslationYSetting ) + \
                         ' ' + str( initialParametersTranslationZSetting )

          command += ' -optimizerParameters ' + \
                     str( maximumIterationCountSetting )
          if ( optimizerNameSetting == 'nelder-mead' ):

            command += ' ' + str( stoppingCriterionErrorSetting ) + \
                       ' ' + str( optimizerParametersScalingXSetting ) + \
                       ' ' + str( optimizerParametersScalingYSetting ) + \
                       ' ' + str( optimizerParametersScalingZSetting ) + \
                       ' ' + str( optimizerParametersShearingXYSetting ) + \
                       ' ' + str( optimizerParametersShearingXZSetting ) + \
                       ' ' + str( optimizerParametersShearingYZSetting ) + \
                       ' ' + str( optimizerParametersRotationXSetting ) + \
                       ' ' + str( optimizerParametersRotationYSetting ) + \
                       ' ' + str( optimizerParametersRotationZSetting ) + \
                       ' ' + str( optimizerParametersTranslationXSetting ) + \
                       ' ' + str( optimizerParametersTranslationYSetting ) + \
                       ' ' + str( optimizerParametersTranslationZSetting )

          else:

            command += ' ' + str( stepSizeSetting ) + \
                       ' ' + str( maximumTestGradientSetting ) + \
                       ' ' + str( maximumToleranceSetting )

          command += ' -verbose true'
	  
          executeCommand( self, subjectName, command, viewOnly )
	  
	  if ( not viewOnly ):
	  
	    os.remove( os.path.join( outputNormalization3dDirectory,
                                   str(templateName) + '.ima.minf' ) )
				   
	    if ( templateName=='t2-WHS-40um' ) :			   	    
	    
	      os.remove( os.path.join( outputNormalization3dDirectory,
                                       'label-WHS-40um.ima.minf' ) )
	
	    else:
	  
	      os.remove( os.path.join( outputNormalization3dDirectory,
                                     'label-Duke-43um.ima.minf' ) )			     	    
	  	  
          ####################### resampling labels ############################
          
	  if ( templateName=='t2-WHS-40um' ) :
          
	    command = 'GkgExecuteCommand Resampling3d ' + \
	              ' -reference ' + os.path.join( \
                                                  outputNormalization3dDirectory,
		                                  'label-WHS-40um.ima' ) + \
		      ' -template ' + fileNameOutT1 + \
		      ' -transforms ' + fileNameTemplateToT1 + \
		      ' -output ' + os.path.join( \
                                       outputNormalization3dDirectory,
                                       'label-WHS-40um-resampled-to-t1.ima' ) + \
		      ' -order ' + '0' + \
		      ' -verbose'
		    
	    executeCommand( self, subjectName, command, viewOnly )
	  
	    os.remove( os.path.join( outputNormalization3dDirectory,
                                   'label-WHS-40um-resampled-to-t1.ima.minf' ) )
				    
	  else:	
	  
	    command = 'GkgExecuteCommand Resampling3d ' + \
	              ' -reference ' + os.path.join( \
                                                 outputNormalization3dDirectory,
		                                 'label-Duke-43um.ima' ) + \
		      ' -template ' + fileNameOutT1 + \
		      ' -transforms ' + fileNameTemplateToT1 + \
		      ' -output ' + os.path.join( \
                                       outputNormalization3dDirectory,
                                       'label-Duke-43um-resampled-to-t1.ima' ) + \
		      ' -order ' + '0' + \
		      ' -verbose'
		    
	    executeCommand( self, subjectName, command, viewOnly )
	  
	    if ( not viewOnly ):
	      os.remove( os.path.join( \
	                           outputNormalization3dDirectory,
                                   'label-Duke-43um-resampled-to-t1.ima.minf' ) )		    

      ####################### notifying display thread #########################
      functors[ 'condition-notify-and-release' ]( \
                                   subjectName,
                                   't1-normalization3d-morphologist-processed' )


      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 20 )



      ####################### matching T1 and T2(b=0) ##########################


      fileNameT1ToDwTransform3d = os.path.join( outputWorkDirectory,
                                                't1_to_dw.trm' )
      fileNameDwToT1Transform3d = os.path.join( outputWorkDirectory,
                                                'dw_to_t1.trm' )

      if ( parameters[ 'generateDwToT1Transformation' ].getValue() ):

        dwToT1RegistrationParameters = parameters[ \
                                      'dwToT1RegistrationParameter' ].getValue()

        similarityMeasureName = str( parameters[ \
          'dwToT1RegistrationParameter' ].getChoice( 'similarityMeasureName' ) )
        optimizerName = str( parameters[ \
          'dwToT1RegistrationParameter' ].getChoice( 'optimizerName' ) )
        transform3DType = str( parameters[ \
                'dwToT1RegistrationParameter' ].getChoice( 'transform3DType' ) )
        referenceLowerThreshold = str( dwToT1RegistrationParameters[ \
                                                   'referenceLowerThreshold' ] )
        floatingLowerThreshold = str( dwToT1RegistrationParameters[ \
                                                    'floatingLowerThreshold' ] )
        resamplingOrder = str( dwToT1RegistrationParameters[ \
                                                           'resamplingOrder' ] )
        subSamplingMaximumSizes = str( dwToT1RegistrationParameters[ \
                                                   'subSamplingMaximumSizes' ] )
        maximumIterationCount = str( dwToT1RegistrationParameters[ \
                                                     'maximumIterationCount' ] )
        levelCount = dwToT1RegistrationParameters[ 'levelCount' ] 
        applySmoothing = dwToT1RegistrationParameters[ 'applySmoothing' ]
        initializeCoefficientsUsingCenterOfGravity = \
                                  dwToT1RegistrationParameters[ \
                                  'initializeCoefficientsUsingCenterOfGravity' ]
        initialParametersScalingX = dwToT1RegistrationParameters[ \
                                  'initialParametersScalingX' ]
        initialParametersScalingY = dwToT1RegistrationParameters[ \
                                  'initialParametersScalingY' ]
        initialParametersScalingZ = dwToT1RegistrationParameters[ \
                                  'initialParametersScalingZ' ]
        initialParametersShearingXY = dwToT1RegistrationParameters[ \
                                  'initialParametersShearingXY' ]
        initialParametersShearingXZ = dwToT1RegistrationParameters[ \
                                  'initialParametersShearingXZ' ]
        initialParametersShearingYZ = dwToT1RegistrationParameters[ \
                                  'initialParametersShearingYZ' ]
        initialParametersRotationX = dwToT1RegistrationParameters[ \
                                  'initialParametersRotationX' ]
        initialParametersRotationY = dwToT1RegistrationParameters[ \
                                  'initialParametersRotationY' ]
        initialParametersRotationZ = dwToT1RegistrationParameters[ \
                                  'initialParametersRotationZ' ]
        initialParametersTranslationX = dwToT1RegistrationParameters[ \
                                  'initialParametersTranslationX' ]
        initialParametersTranslationY = dwToT1RegistrationParameters[ \
                                  'initialParametersTranslationY' ]
        initialParametersTranslationZ = dwToT1RegistrationParameters[ \
                                  'initialParametersTranslationZ' ]
        stoppingCriterionError = dwToT1RegistrationParameters[ \
                                                      'stoppingCriterionError' ]
        stepSize = dwToT1RegistrationParameters[ 'stepSize' ]
        maximumTestGradient = dwToT1RegistrationParameters[ \
                                                      'maximumTestGradient' ]
        maximumTolerance = dwToT1RegistrationParameters[ 'maximumTolerance' ]
        optimizerParametersScalingX = dwToT1RegistrationParameters[ \
                                  'optimizerParametersScalingX' ]
        optimizerParametersScalingY = dwToT1RegistrationParameters[ \
                                  'optimizerParametersScalingY' ]
        optimizerParametersScalingZ = dwToT1RegistrationParameters[ \
                                  'optimizerParametersScalingZ' ]
        optimizerParametersShearingXY = dwToT1RegistrationParameters[ \
                                  'optimizerParametersShearingXY' ]
        optimizerParametersShearingXZ = dwToT1RegistrationParameters[ \
                                  'optimizerParametersShearingXZ' ]
        optimizerParametersShearingYZ = dwToT1RegistrationParameters[ \
                                  'optimizerParametersShearingYZ' ]
        optimizerParametersRotationX = dwToT1RegistrationParameters[ \
                                  'optimizerParametersRotationX' ]
        optimizerParametersRotationY = dwToT1RegistrationParameters[ \
                                  'optimizerParametersRotationY' ]
        optimizerParametersRotationZ = dwToT1RegistrationParameters[ \
                                  'optimizerParametersRotationZ' ]
        optimizerParametersTranslationX = dwToT1RegistrationParameters[ \
                                  'optimizerParametersTranslationX' ]
        optimizerParametersTranslationY = dwToT1RegistrationParameters[ \
                                  'optimizerParametersTranslationY' ]
        optimizerParametersTranslationZ = dwToT1RegistrationParameters[ \
                                  'optimizerParametersTranslationZ' ]

        command = 'GkgExecuteCommand Registration3d ' + \
                  ' -reference ' + fileNameOutT1 + \
                  ' -floating ' + fileNameAverageT2 + \
                  ' -o ' + fileNameDwToT1Transform3d + \
                  ' -oi ' + fileNameT1ToDwTransform3d  + \
                  ' -similarityMeasureName ' + similarityMeasureName + \
                  ' -optimizerName ' + optimizerName + \
                  ' -referenceLowerThreshold ' + referenceLowerThreshold + \
                  ' -floatingLowerThreshold ' + floatingLowerThreshold + \
                  ' -resamplingOrder ' + resamplingOrder + \
                  ' -subSamplingMaximumSizes ' + subSamplingMaximumSizes + \
                  ' -t ' + transform3DType

        if ( similarityMeasureName != 'correlation-coefficient' ):

          command += ' -similarityMeasureParameters ' + str( levelCount ) + \
                                             ' ' + str( int( applySmoothing ) )

        if not initializeCoefficientsUsingCenterOfGravity:

          command += ' -initialParameters '
                                         
          if ( transform3DType != 'rigid' ):

            command += str( initialParametersScalingX ) + ' ' + \
                       str( initialParametersScalingY ) + ' ' + \
                       str( initialParametersScalingZ ) + ' '
                       

          if ( transform3DType == 'affine' ):

            command += str( initialParametersShearingXY ) + ' ' + \
                       str( initialParametersShearingXZ ) + ' ' + \
                       str( initialParametersShearingYZ ) + ' '
 
          command += str( initialParametersRotationX ) + ' ' + \
                     str( initialParametersRotationY ) + ' ' + \
                     str( initialParametersRotationZ ) + ' ' + \
                     str( initialParametersTranslationX ) + ' ' + \
                     str( initialParametersTranslationY ) + ' ' + \
                     str( initialParametersTranslationZ ) + ' '
                     
          command += ' -initializedUsingCentreOfGravity false'

        else:
        
          command += ' -initializedUsingCentreOfGravity true'
 
        command += ' -optimizerParameters ' + str( maximumIterationCount ) + ' '

        if ( optimizerName == 'nelder-mead' ):
                                    
          command += str( stoppingCriterionError ) + ' '
          if ( transform3DType != 'rigid' ):

            command += str( optimizerParametersScalingX ) + ' ' + \
                       str( optimizerParametersScalingY ) + ' ' + \
                       str( optimizerParametersScalingZ ) + ' '
                       
          if ( transform3DType == 'affine' ):

            command += str( optimizerParametersShearingXY ) + ' ' + \
                       str( optimizerParametersShearingXZ ) + ' ' + \
                       str( optimizerParametersShearingYZ ) + ' '
 
          command += str( optimizerParametersRotationX ) + ' ' + \
                     str( optimizerParametersRotationY ) + ' ' + \
                     str( optimizerParametersRotationZ ) + ' ' + \
                     str( optimizerParametersTranslationX ) + ' ' + \
                     str( optimizerParametersTranslationY ) + ' ' + \
                     str( optimizerParametersTranslationZ ) + ' '
 
        else:
                                    
          command += str( stepSize ) + ' ' + \
                     str( maximumTestGradient ) + ' ' + \
                     str( maximumTolerance ) 
 
        command += ' -verbose true'

        executeCommand( self, subjectName, command, viewOnly )

      else:

        shutil.copyfile( parameters[ \
                            'fileNameDwToT1Transformation' ].getValue(),
                            fileNameDwToT1Transform3d )
        command = 'GkgExecuteCommand Transform3dInverter ' + \
                  ' -i ' + fileNameDwToT1Transform3d + \
                  ' -o ' + fileNameT1ToDwTransform3d + \
                  ' -verbose true'
        executeCommand( self, subjectName, command, viewOnly )



      fileNameT1ToTalairachTransform = os.path.join(
                          outputWorkDirectory,
                          'Morphologist',
                          'RawT1-t1_default_acquisition_TO_Talairach-ACPC.trm' )
      if ( os.path.exists( fileNameT1ToTalairachTransform ) ):
      
        fileNameDwToTalairachTransform = os.path.join( \
                                                    outputWorkDirectory,
                                                    'dw_to_Talairach-ACPC.trm' )
        command = 'GkgExecuteCommand Transform3dComposer ' + \
                  ' -i ' + fileNameDwToT1Transform3d + ' ' + \
                           fileNameT1ToTalairachTransform + \
                  ' -o ' + fileNameDwToTalairachTransform + \
                  ' -verbose '
        executeCommand( self, subjectName, command, viewOnly )

        fileNameTalairachToDwTransform = os.path.join( \
                                                    outputWorkDirectory,
                                                    'Talairach-ACPC_to_dw.trm' )
        command = 'GkgExecuteCommand Transform3dInverter ' + \
                  ' -i ' + fileNameDwToTalairachTransform + \
                  ' -o ' + fileNameTalairachToDwTransform + \
                  ' -verbose '
        executeCommand( self, subjectName, command, viewOnly )


      ####################### notifying display thread #########################
      functors[ 'condition-notify-and-release' ]( \
                                                 subjectName,
                                                 't1-to-dw-matching-processed' )


      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 50 )


      ################### adding borders to T2(b=0) and DW #####################

      leftRightAdditionSliceCount = parameters[ \
                                      'leftRightAdditionSliceCount' ].getValue()
      anteriorPosteriorAdditionSliceCount = parameters[ \
                              'anteriorPosteriorAdditionSliceCount' ].getValue()
      headFootAdditionSliceCount = parameters[ \
                                       'headFootAdditionSliceCount' ].getValue()

      fileNameAverageT2Extended = os.path.join( outputWorkDirectory,
                                                't2_extended' )
      command = 'GkgExecuteCommand AddBorder2Volume ' + \
                ' -i ' + fileNameAverageT2 + \
                ' -o ' + fileNameAverageT2Extended + \
                ' -bx ' + str( leftRightAdditionSliceCount ) + \
                ' -by ' + str( anteriorPosteriorAdditionSliceCount ) + \
                ' -bz ' + str( headFootAdditionSliceCount ) + \
                ' -verbose true'
      executeCommand( self, subjectName, command, viewOnly )

      fileNameDWExtended = os.path.join( outputWorkDirectory,
                                         'dw_extended' )
      fileNameDwToDwExtendedTransform3d = os.path.join( \
                                                       outputWorkDirectory,
                                                       'dw_to_dw_extended.trm' )
      fileNameDwExtendedToDwTransform3d = os.path.join( \
                                                       outputWorkDirectory,
                                                       'dw_extended_to_dw.trm' )
      command = 'GkgExecuteCommand AddBorder2Volume ' + \
                ' -i ' + fileNameDW + \
                ' -o ' + fileNameDWExtended + \
                ' -bx ' + str( leftRightAdditionSliceCount ) + \
                ' -by ' + str( anteriorPosteriorAdditionSliceCount ) + \
                ' -bz ' + str( headFootAdditionSliceCount ) + \
                ' -td ' + fileNameDwToDwExtendedTransform3d + \
                ' -ti ' + fileNameDwExtendedToDwTransform3d + \
                ' -verbose true'
      executeCommand( self, subjectName, command, viewOnly )

      ################### adding borders to rough mask #########################
      
      if ( len( roughMaskDirectory ) > 0 ):
      
        fileNameRoughMask = os.path.join( roughMaskDirectory,
                                          'mask' )
        fileNameRoughMaskExtended = os.path.join( outputWorkDirectory,
                                                'mask_extended' )
        command = 'GkgExecuteCommand AddBorder2Volume ' + \
                  ' -i ' + fileNameRoughMask + \
                  ' -o ' + fileNameRoughMaskExtended + \
                  ' -bx ' + str( leftRightAdditionSliceCount ) + \
                  ' -by ' + str( anteriorPosteriorAdditionSliceCount ) + \
                  ' -bz ' + str( headFootAdditionSliceCount ) + \
                  ' -verbose true'
        executeCommand( self, subjectName, command, viewOnly )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 60 )

      ##################### computing Talairach transform ######################
      resampleDwiDataToTalairachAcpcSpace = parameters[ \
                              'resampleDwiDataToTalairachAcpcSpace' ].getValue()
                              
      if ( resampleDwiDataToTalairachAcpcSpace == 2 ):

        fileNameT1ToTalairachTransform3d = os.path.join( outputWorkDirectory,
                                                       't1_to_talairach.trm' )
        fileNameTalairachToT1Transform3d = os.path.join( outputWorkDirectory,
                                                       'talairach_to_t1.trm' )

        command = 'AimsTalairachTransform ' + \
                  ' -apc ' + fileNameT1ACPCCoordinates + \
                  ' -o ' + fileNameT1ToTalairachTransform3d + \
                  ' -ns '
        executeCommand( self, subjectName, command, viewOnly )

        command = 'GkgExecuteCommand Transform3dInverter ' + \
                  ' -i ' + fileNameT1ToTalairachTransform3d + \
                  ' -o ' + fileNameTalairachToT1Transform3d + \
                  ' -verbose true'
        executeCommand( self, subjectName, command, viewOnly )


        ######################### updating progress bar ########################
        functors[ 'update-progress-bar' ]( subjectName, 65 )


        ################### reading T1 volume size and resolution ##############
        # processing volume size(s)
        command = 'GkgExecuteCommand VolumeInformation' + \
                  ' -i ' + fileNameOutT1 + \
                  ' -info sizeX ' + \
                  '-verbosePluginLoading false'
        fd = os.popen( command )
        stringSizeXOfT1 = fd.read()
        ret = fd.close()
        sizeXOfT1 = int( stringSizeXOfT1[ : -1 ] )

        command = 'GkgExecuteCommand VolumeInformation' + \
                  ' -i ' + fileNameOutT1 + \
                  ' -info sizeY ' + \
                  '-verbosePluginLoading false'
        fd = os.popen( command )
        stringSizeYOfT1 = fd.read()
        ret = fd.close()
        sizeYOfT1 = int( stringSizeYOfT1[ : -1 ] )

        command = 'GkgExecuteCommand VolumeInformation' + \
                  ' -i ' + fileNameOutT1 + \
                  ' -info sizeZ ' + \
                  '-verbosePluginLoading false'
        fd = os.popen( command )
        stringSizeZOfT1 = fd.read()
        ret = fd.close()
        sizeZOfT1 = int( stringSizeZOfT1[ : -1 ] )

        # processing volume resolution(s)
        command = 'GkgExecuteCommand VolumeInformation' + \
                  ' -i ' + fileNameOutT1 + \
                  ' -info resolutionX ' + \
                  '-verbosePluginLoading false'
        fd = os.popen( command )
        stringResolutionXOfT1 = fd.read()
        ret = fd.close()
        resolutionXOfT1 = float( stringResolutionXOfT1[ : -1 ] )

        command = 'GkgExecuteCommand VolumeInformation' + \
                  ' -i ' + fileNameOutT1 + \
                  ' -info resolutionY ' + \
                  '-verbosePluginLoading false'
        fd = os.popen( command )
        stringResolutionYOfT1 = fd.read()
        ret = fd.close()
        resolutionYOfT1 = float( stringResolutionYOfT1[ : -1 ] )

        command = 'GkgExecuteCommand VolumeInformation' + \
                  ' -i ' + fileNameOutT1 + \
                  ' -info resolutionZ ' + \
                  '-verbosePluginLoading false'
        fd = os.popen( command )
        stringResolutionZOfT1 = fd.read()
        ret = fd.close()
        resolutionZOfT1 = float( stringResolutionZOfT1[ : -1 ] )

        ################### reading T2 volume size and resolution ##############
        # processing volume resolution(s)
        command = 'GkgExecuteCommand VolumeInformation' + \
                  ' -i ' + fileNameAverageT2Extended + \
                  ' -info resolutionX ' + \
                  '-verbosePluginLoading false'
        fd = os.popen( command )
        stringResolutionXOfT2Extended = fd.read()
        ret = fd.close()
        resolutionXOfT2Extended = float( stringResolutionXOfT2Extended[: -1 ] )

        command = 'GkgExecuteCommand VolumeInformation' + \
                  ' -i ' + fileNameAverageT2Extended + \
                  ' -info resolutionY ' + \
                  '-verbosePluginLoading false'
        fd = os.popen( command )
        stringResolutionYOfT2Extended = fd.read()
        ret = fd.close()
        resolutionYOfT2Extended = float( stringResolutionYOfT2Extended[: -1 ] )

        command = 'GkgExecuteCommand VolumeInformation' + \
                  ' -i ' + fileNameAverageT2Extended + \
                  ' -info resolutionZ ' + \
                  '-verbosePluginLoading false'
        fd = os.popen( command )
        stringResolutionZOfT2Extended = fd.read()
        ret = fd.close()
        resolutionZOfT2Extended = float( stringResolutionZOfT2Extended[: -1 ] )



        sizeXOfT2Talairach = int( float( sizeXOfT1 ) * resolutionXOfT1 /
                                  resolutionXOfT2Extended ) + 1
        sizeYOfT2Talairach = int( float( sizeYOfT1 ) * resolutionYOfT1 /
                                  resolutionYOfT2Extended ) + 1
        sizeZOfT2Talairach = int( float( sizeZOfT1 ) * resolutionZOfT1 /
                                  resolutionZOfT2Extended ) + 1
        resolutionXOfT2Talairach = resolutionXOfT2Extended
        resolutionYOfT2Talairach = resolutionYOfT2Extended
        resolutionZOfT2Talairach = resolutionZOfT2Extended

        ################### creating T1 volume in Talairach ####################


        fileNameT1InTalairachSpace = os.path.join( outputWorkDirectory,
                                                   't1_talairach' )

        command = 'GkgExecuteCommand Resampling3d ' + \
                  ' -reference ' + fileNameOutT1 + \
                  ' -template ' + fileNameOutT1 + \
                  ' -transforms ' + fileNameT1ToTalairachTransform3d + \
                  ' -output ' + fileNameT1InTalairachSpace + \
                  ' -order 3 ' + \
                  ' -background 0 ' + \
                  ' -verbose true'
        executeCommand( self, subjectName, command, viewOnly )

        command = 'GkgExecuteCommand Thresholder ' + \
                  ' -i ' + fileNameT1InTalairachSpace + \
                  ' -o ' + fileNameT1InTalairachSpace + \
                  ' -m gt t1 0 ' + \
                  ' -verbose true'
        executeCommand( self, subjectName, command, viewOnly )

        ######################### updating progress bar ########################
        functors[ 'update-progress-bar' ]( subjectName, 70 )

        ############### composing 3D transform for T2(b=0) and DW ##############

        fileNameDwExtendedToTalairachTransform3d = os.path.join( \
                                                outputWorkDirectory,       
                                                'dw_extended_to_talairach.trm' )

        command = 'GkgExecuteCommand Transform3dComposer ' + \
                  ' -i ' + fileNameDwExtendedToDwTransform3d + ' ' + \
                           fileNameDwToT1Transform3d + ' ' + \
                           fileNameT1ToTalairachTransform3d + \
                  ' -o ' + fileNameDwExtendedToTalairachTransform3d + \
                  ' -verbose true'
        executeCommand( self, subjectName, command, viewOnly )

        ######################### updating progress bar ########################
        functors[ 'update-progress-bar' ]( subjectName, 75 )

        ######################### T2(b=0) in Talairach space ###################
        fileNameAverageT2InTalaraichSpace = os.path.join( outputWorkDirectory,
                                                         't2_talairach' )


        command = 'GkgExecuteCommand DwiResampling3d ' + \
                  ' -reference ' + fileNameAverageT2Extended + \
                  ' -size ' + str( sizeXOfT2Talairach ) + ' ' + \
                              str( sizeYOfT2Talairach ) + ' ' + \
                              str( sizeZOfT2Talairach ) + ' ' + \
                  ' -resolution ' + str( resolutionXOfT2Talairach ) + ' ' + \
                                    str( resolutionYOfT2Talairach ) + ' ' + \
                                    str( resolutionZOfT2Talairach ) + ' ' + \
                  ' -transforms ' + fileNameDwExtendedToTalairachTransform3d + \
                  ' -output ' + fileNameAverageT2InTalaraichSpace + \
                  ' -order 3 ' + \
                  ' -background 0 ' + \
                  ' -verbose true'
        executeCommand( self, subjectName, command, viewOnly )

        command = 'GkgExecuteCommand Thresholder ' + \
                  ' -i ' + fileNameAverageT2InTalaraichSpace + \
                  ' -o ' + fileNameAverageT2InTalaraichSpace + \
                  ' -m gt t1 0 ' + \
                  ' -verbose true'
        executeCommand( self, subjectName, command, viewOnly )

        ######################### updating progress bar ########################
        functors[ 'update-progress-bar' ]( subjectName, 80 )

        ######################### DW in Talairach space ########################

        fileNameDWInTalaraichSpace = os.path.join( outputWorkDirectory,
                                                   'dw_talairach' )

        command = 'GkgExecuteCommand DwiResampling3d ' + \
                  ' -reference ' + fileNameDWExtended + \
                  ' -size ' + str( sizeXOfT2Talairach ) + ' ' + \
                              str( sizeYOfT2Talairach ) + ' ' + \
                              str( sizeZOfT2Talairach ) + ' ' + \
                  ' -resolution ' + str( resolutionXOfT2Talairach ) + ' ' + \
                                    str( resolutionYOfT2Talairach ) + ' ' + \
                                    str( resolutionZOfT2Talairach ) + ' ' + \
                  ' -transforms ' + fileNameDwExtendedToTalairachTransform3d + \
                  ' -output ' + fileNameDWInTalaraichSpace + \
                  ' -order 3 ' + \
                  ' -background 0 ' + \
                  ' -verbose true'
        executeCommand( self, subjectName, command, viewOnly )

        command = 'GkgExecuteCommand Thresholder ' + \
                  ' -i ' + fileNameDWInTalaraichSpace + \
                  ' -o ' + fileNameDWInTalaraichSpace + \
                  ' -m gt t1 0 ' + \
                  ' -verbose true'
        executeCommand( self, subjectName, command, viewOnly )

        ######################### mask in Talairach space ######################
        if ( len( roughMaskDirectory ) > 0 ):

          fileNameRoughMaskInTalairachSpace = os.path.join( outputWorkDirectory,
                                                            'mask_talairach' )
          command = \
                 'GkgExecuteCommand DwiResampling3d ' + \
                 ' -reference ' + fileNameRoughMaskExtended + \
                 ' -size ' + str( sizeXOfT2Talairach ) + ' ' + \
                             str( sizeYOfT2Talairach ) + ' ' + \
                             str( sizeZOfT2Talairach ) + ' ' + \
                 ' -resolution ' + str( resolutionXOfT2Talairach ) + ' ' + \
                                   str( resolutionYOfT2Talairach ) + ' ' + \
                                   str( resolutionZOfT2Talairach ) + ' ' + \
                 ' -transforms ' + fileNameDwExtendedToTalairachTransform3d + \
                 ' -output ' + fileNameRoughMaskInTalairachSpace + \
                 ' -order 0 ' + \
                 ' -background 0 ' + \
                 ' -verbose true'
          executeCommand( self, subjectName, command, viewOnly )

        ######################### updating progress bar ########################
        functors[ 'update-progress-bar' ]( subjectName, 90 )

        ######################### RGB in Talairach space #######################
        if ( len( roughMaskDirectory ) > 0 ):
      
          self.createRGB( fileNameAverageT2InTalaraichSpace, 
                          fileNameDWInTalaraichSpace,
                          fileNameRoughMaskInTalairachSpace,
                          outputWorkDirectory,
                          subjectName,
                          verbose,
                          viewOnly )
                          
        else:
      
          self.createRGB( fileNameAverageT2InTalaraichSpace, 
                          fileNameDWInTalaraichSpace,
                          '',
                          outputWorkDirectory,
                          subjectName,
                          verbose,
                          viewOnly )

      ####################### notifying display thread #########################
      functors[ 'condition-notify-and-release' ]( subjectName,
                                                  'talairach-processed' )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 100 )

    except:

      self._functors[ 'task-set-status-as-failed' ]( subjectName,
                                                     sys.exc_info() )

  ##############################################################################
  # creating RGB map
  ##############################################################################

  def createRGB( self,
                 fileNameAverageT2,
                 fileNameDW,
                 fileNameMask,
                 outputWorkDirectory,
                 subjectName,
                 verbose,
                 viewOnly ):

    # building RGB map
    fileNameRGB = os.path.join( outputWorkDirectory, 'dti_rgb_talairach' )
    if ( len( fileNameMask ) > 0 ):
    
      command = 'GkgExecuteCommand DwiTensorField' + \
                ' -t2 ' + fileNameAverageT2 + \
                ' -dw ' + fileNameDW + \
                ' -m ' +  fileNameMask + \
                ' -f rgb ' + \
                ' -o ' + fileNameRGB + \
                ' -verbose true'
                
    else:
    
      command = 'GkgExecuteCommand DwiTensorField' + \
                ' -t2 ' + fileNameAverageT2 + \
                ' -dw ' + fileNameDW + \
                ' -f rgb ' + \
                ' -o ' + fileNameRGB + \
                ' -verbose true'

    executeCommand( self, subjectName, command, viewOnly )

    return fileNameRGB




  ##############################################################################
  # display()
  ##############################################################################

  def display( self, functors, parameters, subjectName, verbose,
               isReportModeActive ):

    ########################### resetting the viewer ###########################
    functors[ 'viewer-reset' ]()

    ################# collecting corrected DWI directory #######################
    correctedDwiDirectory = parameters[ 'correctedDwiDirectory' ].getValue()
    extension = ''
    if ( os.path.exists( os.path.join( correctedDwiDirectory,
                                       'dw_wo_outlier.ima' ) ) ):
                                       
      extension = '_wo_outlier'
      
    elif ( os.path.exists( os.path.join( correctedDwiDirectory,
                                         'dw_wo_susceptibility.ima' ) ) ):
      
      extension = '_wo_susceptibility'
                           
    elif ( os.path.exists( os.path.join( correctedDwiDirectory,
                                    'dw_wo_eddy_current_and_motion.ima' ) ) ):
      
      extension = '_wo_eddy_current_and_motion'

    fileNameAverageT2 = os.path.join( correctedDwiDirectory,
                                      't2' + extension )
    fileNameRGB = os.path.join( correctedDwiDirectory,
                                'dti_rgb' + extension )


    ####################### collecting output work directory ###################
    outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()

    fileNameOutT1 = os.path.join( outputWorkDirectory, 't1' )
    fileNameDwToT1Transform3d = os.path.join( outputWorkDirectory,
                                              'dw_to_t1.trm' )

    ############################# reading T2(b=0) ##############################
    functors[ 'viewer-create-referential' ]( 'frameDW' )
    functors[ 'viewer-load-object' ]( fileNameAverageT2, 'AverageT2' )
    functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'AverageT2' )
    functors[ 'viewer-set-colormap' ]( 'AverageT2', 'RED TEMPERATURE' )

    ############################# reading RGB map ##############################
    functors[ 'viewer-load-object' ]( fileNameRGB, 'RGB' )
    functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'RGB' )

    ####################### waiting for result #################################
    functors[ 'condition-wait-and-release' ]( subjectName,
                                              't1-cropping-processed' )

    ####################### reading and displaying T1 ##########################
    functors[ 'viewer-create-referential' ]( 'frameT1' )
    functors[ 'viewer-load-object' ]( fileNameOutT1, 'T1' )
    functors[ 'viewer-assign-referential-to-object' ]( 'frameT1', 'T1' )

    functors[ 'viewer-assign-referential-to-window' ]( 'frameT1',
                                                       'first_view',
                                                       'T1' )
    functors[ 'viewer-add-object-to-window' ]( 'T1',
                                               'first_view',
                                               'T1' )

    ####################### waiting for result #################################
    functors[ 'condition-wait-and-release' ]( \
                                   subjectName,
                                   't1-normalization3d-morphologist-processed' )

    ####################### waiting for result #################################
    functors[ 'condition-wait-and-release' ]( subjectName,
                                                't1-to-dw-matching-processed' )

    ######################## displaying T1 + T2(b=0) fusion ####################
    functors[ 'viewer-load-transformation' ]( fileNameDwToT1Transform3d,
                                              'frameDW', 'frameT1' )

    functors[ 'viewer-fusion-objects' ]( [ 'T1', 'AverageT2' ],
                                         'fusionT1AndAverageT2',
                                         'Fusion2DMethod' )
    functors[ 'viewer-assign-referential-to-object' ]( 'frameT1',
                                                       'fusionT1AndAverageT2' )
    functors[ 'viewer-assign-referential-to-window' ]( 'frameT1',    
                                                       'first_view',
                                                       'T1 + Average T2(b=0)' )
    functors[ 'viewer-add-object-to-window' ]( 'fusionT1AndAverageT2',
                                               'first_view',
                                               'T1 + Average T2(b=0)' )

    ######################## displaying T1 + RGB fusion ########################
    functors[ 'viewer-fusion-objects' ]( [ 'T1', 'RGB' ],
                                         'fusionT1AndRGB',
                                         'Fusion2DMethod' )
    functors[ 'viewer-assign-referential-to-object' ]( 'frameT1',
                                                       'fusionT1AndRGB' )
    functors[ 'viewer-assign-referential-to-window' ]( 'frameT1',    
                                                       'first_view',
                                                       'T1 + RGB' )
    functors[ 'viewer-add-object-to-window' ]( 'fusionT1AndRGB',
                                               'first_view',
                                               'T1 + RGB' )
                                               
    ####################### waiting for result #################################
    functors[ 'condition-wait-and-release' ]( subjectName,
                                              'talairach-processed' )

    ################## displaying T1 + RGB in Talairach fusion #################
    resampleDwiDataToTalairachAcpcSpace = parameters[ \
                              'resampleDwiDataToTalairachAcpcSpace' ].getValue()
                              
    if ( resampleDwiDataToTalairachAcpcSpace == 2 ):

      fileNameT1InTalairachSpace = os.path.join( outputWorkDirectory,
                                                 't1_talairach' )
      fileNameRGBInTalairachSpace = os.path.join( outputWorkDirectory,
                                                 'dti_rgb_talairach' )

      functors[ 'viewer-load-object' ]( fileNameT1InTalairachSpace,
                                        'T1InTalairach' )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameT1',
                                                         'T1InTalairach' )

      functors[ 'viewer-load-object' ]( fileNameRGBInTalairachSpace,
                                        'RGBInTalairach' )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameT1',
                                                         'RGBInTalairach' )


      functors[ 'viewer-fusion-objects' ]( [ 'T1InTalairach',
                                             'RGBInTalairach' ],
                                           'fusionT1AndRGBInTalairach',
                                           'Fusion2DMethod' )
      functors[ 'viewer-assign-referential-to-object' ]( \
                                                     'frameT1',
                                                     'fusionT1AndRGBInTalairach' )
      functors[ 'viewer-assign-referential-to-window' ]( \
                                                   'frameT1',
                                                   'first_view',
                                                   'T1 + RGB in Talairach space' )
      functors[ 'viewer-add-object-to-window' ]( 'fusionT1AndRGBInTalairach',
                                                 'first_view',
                                                 'T1 + RGB in Talairach space' )

