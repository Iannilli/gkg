from Viewer import *


class DwiToAnatomyMatchingViewer( Viewer ):

  def __init__( self, minimumSize, parent ):

    Viewer.__init__( self, minimumSize, parent )
    
    ########################## First View ######################################

    self.createView( 'first_view' )
    
    self.addAxialWindow( 'first_view', 0, 0, 1, 1,
                         'T1', 'T1' )
    self.addAxialWindow( 'first_view', 0, 1, 1, 1,
                         'T1 + Average T2(b=0)', 'T1 + Average T2(b=0)' )
    self.addAxialWindow( 'first_view', 1, 0, 1, 1,
                         'T1 + RGB',
                         'T1 + RGB' )
    self.addAxialWindow( 'first_view', 1, 1, 1, 1,
                         'T1 + RGB in Talairach space',
                         'T1 + RGB in Talairach space' )


def createDwiToAnatomyMatchingViewer( minimumSize, parent ):

  return DwiToAnatomyMatchingViewer( minimumSize, parent )

