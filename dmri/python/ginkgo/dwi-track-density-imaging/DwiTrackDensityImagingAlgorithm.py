from Algorithm import *
from DwiTrackDensityImagingTask import *


class DwiTrackDensityImagingAlgorithm( Algorithm ):

  def __init__( self, verbose ):

    Algorithm.__init__( self, 'DWI-Track-Density-Imaging', verbose, True )

    # input data
    self.addParameter( StringParameter( 'bundleMapDirectoryNames', '' ) )
    self.addParameter( StringParameter( 'scalarMapFileNames', '' ) )

    self.addParameter( DoubleParameter( 'resolutionX',
                                         1.0, 0.001, 100, 0.1 ) )
    self.addParameter( DoubleParameter( 'resolutionY',
                                         1.0, 0.001, 100, 0.1 ) )
    self.addParameter( DoubleParameter( 'resolutionZ',
                                         1.0, 0.001, 100, 0.1 ) )

    self.addParameter( IntegerParameter( 'threadCount', 1, 1, 100, 1 ) )	



  def launch( self ):

    if ( self._verbose ):

      print 'running DWI Track Density Imaging'

    task = DwiTrackDensityImagingTask( self._application )
    task.launch( False )

  def view( self ):

    if ( self._verbose ):

      print 'viewing DWI Track Density Imaging'

    task = DwiTrackDensityImagingTask( self._application )
    task.launch( True )

