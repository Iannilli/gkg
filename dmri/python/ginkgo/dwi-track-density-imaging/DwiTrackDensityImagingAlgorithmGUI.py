from AlgorithmGUI import *
from ResourceManager import *


class DwiTrackDensityImagingAlgorithmGUI( AlgorithmGUI ):

  def __init__( self, algorithm ):

    AlgorithmGUI.__init__( self,
                           algorithm,
                           ResourceManager().getUIFileName( \
                                                 'dmri',
                                                 'DwiTrackDensityImaging.ui' ) )

    ###########################################################################
    # connecting input data
    ###########################################################################

    # bundle map directory names
    self.connectStringParameterToLineEdit( \
                                          'bundleMapDirectoryNames',
                                          'lineEdit_BundleMapDirectoryNames' )
    self.connectDirectoryBrowserToPushButtonAndLineEdit( \
                                          'pushButton_BundleMapDirectoryNames',
                                          'lineEdit_BundleMapDirectoryNames' )
    # scalar map file names
    self.connectStringParameterToLineEdit( 'scalarMapFileNames',
                                           'lineEdit_ScalarMapFileNames' )
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit( \
                                               'pushButton_ScalarMapFileNames',
                                               'lineEdit_ScalarMapFileNames' )

    # output target resolution
    self.connectDoubleParameterToSpinBox( 'resolutionX', 
                                          'doubleSpinBox_ResolutionX' )
    self.connectDoubleParameterToSpinBox( 'resolutionY', 
                                          'doubleSpinBox_ResolutionY' )
    self.connectDoubleParameterToSpinBox( 'resolutionZ', 
                                          'doubleSpinBox_ResolutionZ' )


    # maximum number of thread(s)
    self.connectIntegerParameterToSpinBox( 'threadCount', 
                                           'spinBox_ThreadCount' )


