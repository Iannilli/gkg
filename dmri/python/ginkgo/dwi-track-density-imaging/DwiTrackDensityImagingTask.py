from Task import *
import os


class DwiTrackDensityImagingTask( Task ):

  def __init__( self, application ):

    Task.__init__( self, application )

  ##############################################################################
  # execute()
  ##############################################################################

  def execute( self, functors, parameters, subjectName, verbose, viewOnly ):

    try:

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 0 )

      functors[ 'condition-acquire' ]( subjectName,
                                       'track-density-imaging-processed' )

      ####################### collecting output work directory #################
      outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()
      if ( verbose ):

        print  'Output work directory :', outputWorkDirectory

      ####################### collecting input data filename(s) ################
      bundleMapDirectoryNamesList = \
                      parameters[ 'bundleMapDirectoryNames' ].getValue().split()
      scalarMapFileNamesList = \
                           parameters[ 'scalarMapFileNames' ].getValue().split()

      bundleMapDirectoryNames = ''
      scalarMapFileNames = ''

      for bundleMapDirectoryName in bundleMapDirectoryNamesList:

        bundleMapDirectoryNames += bundleMapDirectoryName + ' '

      for scalarMapFileName in scalarMapFileNamesList:

        scalarMapFileNames += scalarMapFileName + ' '

      ########################### collecting parameters ########################

      resolution = str( parameters[ 'resolutionX' ].getValue() ) + ' ' + \
                   str( parameters[ 'resolutionY' ].getValue() ) + ' ' + \
                   str( parameters[ 'resolutionZ' ].getValue() )
      threadCount = parameters[ 'threadCount' ].getValue()

      functors[ 'update-progress-bar' ]( subjectName, 10 )

      ######################## running super resolution ########################
      outputFileName = os.path.join( outputWorkDirectory,
                                     'trackDensityImaging' )

      command = 'GkgExecuteCommand DwiMultipleSubjectTrackDensityImaging '\
                ' -b ' + bundleMapDirectoryNames + \
                ' -i ' + scalarMapFileNames + \
                ' -resolution ' + resolution + \
                ' -o ' + outputFileName + \
                ' -thread ' + str( threadCount ) + \
                ' -format gis' + \
                ' -verbose true'

      executeCommand( self, subjectName, command, viewOnly )

      ########################## updating progress bar #########################
      functors[ 'update-progress-bar' ]( subjectName, 100 )

      functors[ 'condition-notify-and-release' ]( \
                                              subjectName,
                                             'track-density-imaging-processed' )

    except:

      self._functors[ 'task-set-status-as-failed' ]( subjectName,
                                                     sys.exc_info() )


  ##############################################################################
  # display()
  ##############################################################################

  def display( self, functors, parameters, subjectName, verbose,
               isReportModeActive ):

    ########################### resetting the viewer ###########################
    functors[ 'viewer-reset' ]()

    # display selected view
    functors[ 'viewer-set-view' ]( 'first_view' )

    ########################### collecting file names ##########################
    outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()
    outputFileName = os.path.join( outputWorkDirectory,
                                   'trackDensityImaging' )

    functors[ 'condition-wait-and-release' ]( \
                                             subjectName,
                                             'track-density-imaging-processed' )

    functors[ 'viewer-load-object' ]( outputFileName ,
                                      'outputFile' )

    functors[ 'viewer-add-object-to-window' ]( 'outputFile',
                                               'first_view',
                                               'track density imaging' )
