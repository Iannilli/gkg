from Viewer import *


class DwiTrackDensityImagingViewer( Viewer ):

  def __init__( self, minimumSize, parent ):

    Viewer.__init__( self, minimumSize, parent )

    ########################## First View ######################################

    self.createView( 'first_view' )
    self.addSagittalWindow( 'first_view', 0, 0, 1, 1,
                            'tract density imaging', 'tract density imaging',
                            backgroundColor = [ 0, 0, 0, 0 ],
                            isMuteToolButtonVisible = True,
                            isColorMapToolButtonVisible = True,
                            isSnapshotToolButtonVisible = True,
                            isZoomToolButtonVisible = True )

def createDwiTrackDensityImagingViewer( minimumSize, parent ):

  return DwiTrackDensityImagingViewer( minimumSize, parent )

