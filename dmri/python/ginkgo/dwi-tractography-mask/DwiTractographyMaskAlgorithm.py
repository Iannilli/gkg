from Algorithm import *
from DwiTractographyMaskTask import *


class DwiTractographyMaskAlgorithm( Algorithm ):

  def __init__( self, verbose ):

    Algorithm.__init__( self, 'DWI-Tractography-Mask', verbose, True )

    # input data
    self.addParameter( StringParameter( 'anatomyAndTalairachDirectory', '' ) )

    self.addParameter( BooleanParameter( 'addCerebellum', 0 ) )
    self.addParameter( BooleanParameter( 'addROIMask', 0 ) )
    self.addParameter( BooleanParameter( 'removeROIMask', 0 ) )
    self.addParameter( StringParameter( 'fileNameROIMaskToAdd', '' ) )
    self.addParameter( StringParameter( 'fileNameROIMaskToRemove', '' ) )
    self.addParameter( BooleanParameter( 'addCommissures', 0 ) )
    self.addParameter( StringParameter( 'fileNameCommissureCoordinates', '' ) )

    self.addParameter( BooleanParameter( 'removeTemporaryFiles', 2 ) )

  def launch( self ):

    if ( self._verbose ):

      print \
        'running DWI tractography mask'

    task = DwiTractographyMaskTask( self._application )
    task.launch( False )

  def view( self ):

    if ( self._verbose ):

      print \
        'viewing DWI tractography mask'

    task = DwiTractographyMaskTask( self._application )
    task.launch( True )

