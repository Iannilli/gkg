from AlgorithmGUI import *
from ResourceManager import *


class DwiTractographyMaskAlgorithmGUI( AlgorithmGUI ):

  def __init__( self, algorithm ):

    AlgorithmGUI.__init__( self,
                           algorithm,
                           ResourceManager().getUIFileName(
                                                 'dmri',
                                                 'DwiTractographyMask.ui' ) )

    ###########################################################################
    # connecting input data
    ###########################################################################

    # raw DW data directory interface
    self.connectStringParameterToLineEdit( \
                                        'anatomyAndTalairachDirectory',
                                        'lineEdit_AnatomyAndTalairachDirectory' )    
    self.connectDirectoryBrowserToPushButtonAndLineEdit(
                                      'pushButton_AnatomyAndTalairachDirectory',
                                      'lineEdit_AnatomyAndTalairachDirectory' )


    ###########################################################################
    # connecting mask options
    ###########################################################################

    # add cerebellum
    self.connectBooleanParameterToCheckBox( 'addCerebellum',
                                            'checkBox_AddCerebellum' )

    # add ROI
    self.connectBooleanParameterToCheckBox( 'addROIMask',
                                            'checkBox_AddROIMask' )
    self.connectCheckBoxToCustomCallback( 'checkBox_AddROIMask',
                                          self.disableOrEnableAddRoiMask )

    # deep nuclei mask
    self.connectStringParameterToLineEdit( 'fileNameROIMaskToAdd',
                                           'lineEdit_FileNameROIMaskToAdd' )
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit( \
                                            'pushButton_FileNameROIMaskToAdd',
                                            'lineEdit_FileNameROIMaskToAdd' )
    # remove ventricles
    self.connectBooleanParameterToCheckBox( 'removeROIMask',
                                            'checkBox_RemoveROIMask' )
    self.connectCheckBoxToCustomCallback( 'checkBox_RemoveROIMask',
                                          self.disableOrEnableRemoveRoiMask )
    # ventricles mask
    self.connectStringParameterToLineEdit( 'fileNameROIMaskToRemove',
                                           'lineEdit_FileNameROIMaskToRemove' )
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit( \
                                            'pushButton_FileNameROIMaskToRemove',
                                            'lineEdit_FileNameROIMaskToRemove' )

    # add commissure(s)
    self.connectBooleanParameterToCheckBox( 'addCommissures',
                                            'checkBox_AddCommissures' )
    self.connectCheckBoxToCustomCallback( \
                                     'checkBox_AddCommissures',
                                     self.disableOrEnableCommissureCoordinates )

    # commissures coordinates
    self.connectStringParameterToLineEdit( \
                                      'fileNameCommissureCoordinates',
                                      'lineEdit_FileNameCommissureCoordinates' )
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit( \
                                    'pushButton_FileNameCommissureCoordinates',
                                    'lineEdit_FileNameCommissureCoordinates' )

    # remove temporary files
    self.connectBooleanParameterToCheckBox( 'removeTemporaryFiles',
                                            'checkBox_RemoveTemporaryFiles' )

  def disableOrEnableAddRoiMask( self, state ):

    lineEditWidget = self._findChild( self._awin,
                                      'lineEdit_FileNameROIMaskToAdd' )
    pushButtonWidget = self._findChild( self._awin,
                                      'pushButton_FileNameROIMaskToAdd' )
    labelWidget = self._findChild( self._awin,
                                      'label_FileNameROIMaskToAdd' )

    if ( state == 2 ):

      lineEditWidget.setEnabled( True )
      pushButtonWidget.setEnabled( True )
      labelWidget.setEnabled( True )

    else:

      lineEditWidget.setEnabled( False )
      pushButtonWidget.setEnabled( False )
      labelWidget.setEnabled( False )


  def disableOrEnableRemoveRoiMask( self, state ):

    lineEditWidget = self._findChild( self._awin,
                                      'lineEdit_FileNameROIMaskToRemove' )
    pushButtonWidget = self._findChild( self._awin,
                                      'pushButton_FileNameROIMaskToRemove' )
    labelWidget = self._findChild( self._awin,
                                      'label_FileNameROIMaskToRemove' )

    if ( state == 2 ):

      lineEditWidget.setEnabled( True )
      pushButtonWidget.setEnabled( True )
      labelWidget.setEnabled( True )

    else:

      lineEditWidget.setEnabled( False )
      pushButtonWidget.setEnabled( False )
      labelWidget.setEnabled( False )


  def disableOrEnableCommissureCoordinates( self, state ):

    lineEditWidget = self._findChild( self._awin,
                                      'lineEdit_FileNameCommissureCoordinates' )
    pushButtonWidget = self._findChild( \
                                    self._awin,
                                    'pushButton_FileNameCommissureCoordinates' )
    labelWidget = self._findChild( \
                                    self._awin,
                                    'label_FileNameCommissureCoordinates' )

    if ( state == 2 ):

      lineEditWidget.setEnabled( True )
      pushButtonWidget.setEnabled( True )
      labelWidget.setEnabled( True )

    else:

      lineEditWidget.setEnabled( False )
      pushButtonWidget.setEnabled( False )
      labelWidget.setEnabled( False )

