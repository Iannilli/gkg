from Task import *
from ResourceManager import *
import os

class DwiTractographyMaskTask( Task ):

  def __init__( self, application ):

    Task.__init__( self, application )

  ##############################################################################
  # execute()
  ##############################################################################

  def execute( self, functors, parameters, subjectName, verbose, viewOnly ):

    try:

      ####################### acquiring conditions #############################
      functors[ 'condition-acquire' ]( subjectName, 'sulci-skeleton-processed' )
      functors[ 'condition-acquire' ]( subjectName,
                                      'tractography-mask-processed' )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 0 )

      ####################### collecting output work directory #################
      outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()
      if ( verbose ):

        print  'Output work directory :', outputWorkDirectory

      ####################### collecting input data filename(s) ################
      anatomyAndTalairachDirectory = parameters[ \
                                     'anatomyAndTalairachDirectory' ].getValue()

      addCerebellum = parameters[ 'addCerebellum' ].getValue()
      addROIMask = parameters[ 'addROIMask' ].getValue()
      removeROIMask = parameters[ 'removeROIMask' ].getValue()
      fileNameROIMaskToAdd = parameters[ 'fileNameROIMaskToAdd' ].getValue()
      fileNameROIMaskToRemove = parameters[ \
                                          'fileNameROIMaskToRemove' ].getValue()
      addCommissures = parameters[ 'addCommissures' ].getValue()
      fileNameCommissureCoordinates = parameters[ \
                                    'fileNameCommissureCoordinates' ].getValue()


      #################### colecting unbiased anatomy, histogram analysis, 
      # and Voronoi mask path(s)   #############################################

      specy = ResourceManager().getCurrentSpecy( 'dmri' )

      fileNameUnbiasedT1 = os.path.join( anatomyAndTalairachDirectory,
                                         'Morphologist',
                                         'nobias_t1' )
      fileNameHistogramAnalysis = os.path.join( anatomyAndTalairachDirectory,
                                                'Morphologist',
                                                't1.han' )
      fileNameVoronoiMask = os.path.join( anatomyAndTalairachDirectory,
                                          'Morphologist',
                                          'voronoi_t1' )


      #################### creating intermediate filename(s) #################
      fileNameWhiteMatterMask = os.path.join( outputWorkDirectory,
                                              'white_matter_mask.ima' )
      fileNameClosedVoronoiHemispheres = os.path.join( \
                                  outputWorkDirectory,
                                  'closed_fileNameVoronoiMask_hemispheres.ima' )
      fileNameVoronoiHemispheres = os.path.join( \
                                         outputWorkDirectory,
                                         'fileNameVoronoiMask_hemispheres.ima' )
      fileNameVoronoiCerebellum = os.path.join( \
                                          outputWorkDirectory,
                                          'fileNameVoronoiMask_cerebellum.ima' )
      fileNameVoronoiBrain = os.path.join( outputWorkDirectory,
                                           'fileNameVoronoiMask_brain.ima' )
      fileNameGWHemispheres = os.path.join( \
                                          outputWorkDirectory,
                                          'grey_white_hemispheres.ima' )
      fileNameSulciSkeleton = os.path.join( outputWorkDirectory,
                                            'sulci_fileNameSulciSkeleton.ima' )
      fileNameRootsVoronoi = os.path.join( outputWorkDirectory,
                                            'rootsvoronoi.ima' )
      fileNameVentricles = os.path.join( outputWorkDirectory,
                                         'ventricles.ima' )
      fileNameDilatedVentricles = os.path.join( outputWorkDirectory,
                                                'dilatedVentricles.ima' )
      fileNameFornix = os.path.join( outputWorkDirectory,
                                     'fornix.ima' )
      fileNameNormalizedROI = os.path.join( outputWorkDirectory,
                                               'normalized_ROI.ima' )
      fileNameROIMaskToRemoveS16 = os.path.join( outputWorkDirectory,
                                               'ROI_to_remove_S16.ima' )

      fileNameTractographyMask = os.path.join( outputWorkDirectory,
                                               'tractography_mask.ima' )
      fileNameTemporary = os.path.join( outputWorkDirectory,
                                        'temporary.ima' )

      if ( addCerebellum == 2 ):

        command = 'AimsReplaceLevel' + \
                  ' -i ' + fileNameVoronoiMask + \
                  ' -o ' + fileNameVoronoiHemispheres + \
                  ' -g 1 2 3 ' + \
                  ' -n 1 1 1 '
        executeCommand( self, subjectName, command, viewOnly )

      else:

        command = 'AimsReplaceLevel' + \
                  ' -i ' + fileNameVoronoiMask + \
                  ' -o ' + fileNameVoronoiHemispheres + \
                  ' -g 1 2 3 ' + \
                  ' -n 1 1 0 '
        executeCommand( self, subjectName, command, viewOnly )

      command = 'VipClosing' + \
                ' -i ' + fileNameVoronoiHemispheres + \
                ' -o ' + fileNameClosedVoronoiHemispheres + \
                ' -s 20'
      executeCommand( self, subjectName, command, viewOnly )

      ########################## updating progress bar #########################
      functors[ 'update-progress-bar' ]( subjectName, 10 )

      command = 'VipMask' + \
                ' -i ' + fileNameUnbiasedT1 + \
                ' -m ' + fileNameVoronoiHemispheres + \
                ' -o ' + fileNameWhiteMatterMask + \
                ' -l 1 '
      executeCommand( self, subjectName, command, viewOnly )

      # Normal case
      command = 'VipHomotopicSnake' + \
                ' -i ' + fileNameWhiteMatterMask + \
                ' -o ' + fileNameTractographyMask + \
                ' -h ' + fileNameHistogramAnalysis

      # In case big ventricules
  #    command = 'VipHomotopicSnake' + \
  #              ' -i ' + fileNameWhiteMatterMask + \
  #              ' -o ' + fileNameTractographyMask + \
  #              ' -h ' + fileNameHistogramAnalysis + ' -fc 10'
                
      executeCommand( self, subjectName, command, viewOnly )

      ########################## updating progress bar #########################
      functors[ 'update-progress-bar' ]( subjectName, 20 )

      command = 'VipSkeleton' + \
                ' -i ' + fileNameTractographyMask + \
                ' -so ' + fileNameSulciSkeleton + \
                ' -vo ' + fileNameRootsVoronoi + \
                ' -g ' + fileNameWhiteMatterMask
      executeCommand( self, subjectName, command, viewOnly )

      command = 'VipSingleThreshold' + \
                ' -i ' + fileNameSulciSkeleton + \
                ' -o ' + fileNameSulciSkeleton + \
                ' -t 1 ' + \
                ' -c b ' + \
                ' -m gt'
      executeCommand( self, subjectName, command, viewOnly )

      ########################## updating progress bar #########################
      functors[ 'update-progress-bar' ]( subjectName, 30 )

      ####################### notifying display thread #########################
      functors[ 'condition-notify-and-release' ]( subjectName,
                                                  'sulci-skeleton-processed' )

      command = 'AimsLinearComb' + \
                ' -i ' + fileNameSulciSkeleton + \
                ' -j ' + fileNameVoronoiHemispheres + \
                ' -o ' + fileNameTractographyMask + \
                ' -c -1'
      executeCommand( self, subjectName, command, viewOnly )

      command = 'VipSingleThreshold' + \
                ' -i ' + fileNameTractographyMask + \
                ' -o ' + fileNameTractographyMask + \
                ' -t 0 ' + \
                ' -c b ' + \
                ' -m lt'
      executeCommand( self, subjectName, command, viewOnly )

      ########################## updating progress bar #########################
      functors[ 'update-progress-bar' ]( subjectName, 40 )

      ############################ use roi mask ################################
      if ( addROIMask == 2  or removeROIMask == 2 ):

        if ( addROIMask == 2  and removeROIMask == 2 ):

          command = 'AimsLinearComb' + \
                    ' -i ' + fileNameROIMaskToAdd + \
                    ' -j ' + fileNameROIMaskToRemove + \
                    ' -o ' + fileNameNormalizedROI + \
                    ' -c 1'
          executeCommand( self, subjectName, command, viewOnly )

          command = 'AimsFileConvert' + \
                    ' -i ' + fileNameNormalizedROI + \
                    ' -o ' + fileNameNormalizedROI + \
                    ' -t S16'
          executeCommand( self, subjectName, command, viewOnly )

          command = 'VipSingleThreshold' + \
                    ' -i ' + fileNameROIMaskToAdd + \
                    ' -o ' + fileNameNormalizedROI + \
                    ' -t 0 ' + \
                    ' -c b ' + \
                    ' -m gt'
          executeCommand( self, subjectName, command, viewOnly )

        elif ( addROIMask == 2 ):


          command = 'AimsFileConvert' + \
                    ' -i ' + fileNameROIMaskToAdd + \
                    ' -o ' + fileNameNormalizedROI + \
                    ' -t S16'
          executeCommand( self, subjectName, command, viewOnly )

          command = 'VipSingleThreshold' + \
                    ' -i ' + fileNameNormalizedROI + \
                    ' -o ' + fileNameNormalizedROI + \
                    ' -t 0 ' + \
                    ' -c b ' + \
                    ' -m gt'
          executeCommand( self, subjectName, command, viewOnly )


        ########################### updating progress bar ######################
        if ( addROIMask == 2 ):

          functors[ 'update-progress-bar' ]( subjectName, 50 )

          command = 'AimsLinearComb' + \
                    ' -i ' + fileNameTractographyMask + \
                    ' -j ' + fileNameNormalizedROI + \
                    ' -o ' + fileNameTractographyMask + \
                    ' -c 1'
          executeCommand( self, subjectName, command, viewOnly )

          command = 'VipSingleThreshold' + \
                    ' -i ' + fileNameTractographyMask + \
                    ' -o ' + fileNameTractographyMask + \
                    ' -t 0 ' + \
                    ' -c b ' + \
                    ' -m gt'
          executeCommand( self, subjectName, command, viewOnly )

        ########################## updating progress bar #######################
        functors[ 'update-progress-bar' ]( subjectName, 60 )

        ############################ use roi mask ##############################
        if ( removeROIMask == 2 ):

          command = 'AimsFileConvert' + \
                    ' -i ' + fileNameROIMaskToRemove + \
                    ' -o ' + fileNameROIMaskToRemoveS16 + \
                    ' -t S16'
          executeCommand( self, subjectName, command, viewOnly )

          command = 'AimsLinearComb' + \
                    ' -i ' + fileNameTractographyMask + \
                    ' -j ' + fileNameROIMaskToRemoveS16 + \
                    ' -o ' + fileNameTractographyMask + \
                    ' -c -1'
          executeCommand( self, subjectName, command, viewOnly )

          command = 'VipSingleThreshold' + \
                    ' -i ' + fileNameTractographyMask + \
                    ' -o ' + fileNameTractographyMask + \
                    ' -t 255 ' + \
                    ' -c b ' + \
                    ' -m ge'
          executeCommand( self, subjectName, command, viewOnly )

        ######################## updating progress bar #########################
          functors[ 'update-progress-bar' ]( subjectName, 70 )

          command = 'AimsLinearComb' + \
                    ' -i ' + fileNameTractographyMask + \
                    ' -j ' + fileNameClosedVoronoiHemispheres + \
                    ' -o ' + fileNameTractographyMask + \
                    ' -c 1'
          executeCommand( self, subjectName, command, viewOnly )

          command = 'VipSingleThreshold' + \
                    ' -i ' + fileNameTractographyMask + \
                    ' -o ' + fileNameTractographyMask + \
                    ' -t 255 ' + \
                    ' -c b ' + \
                    ' -m gt'
          executeCommand( self, subjectName, command, viewOnly )

      ####################### use anatomy histogram ############################
      dilatedVentricleThreshold = 0
      ventricleThreshold = 0
      fornixThreshold = 0
      if not( viewOnly ):

        file = open( fileNameHistogramAnalysis, 'r' )
        lines = file.readlines()
        for line in lines:

          words = line.split( ': ' )
          if ( words[ 0 ] == 'gray' ):

            numberGrey = words[ 2 ].split()

          elif ( words[ 0 ] == 'white' ):

            numberWhite = words[ 2 ].split()

        dilatedVentricleThreshold = ( float( numberWhite[ 0 ] ) \
                             - float( numberGrey[ 0 ] ) ) * 0.6 + 70
        ventricleThreshold = ( float( numberWhite[ 0 ] ) \
                           - float( numberGrey[ 0 ] ) ) * 2
        file.close()
        fornixThreshold = ( float( numberWhite[ 0 ] ) \
                            - float( numberGrey[ 0 ] ) ) * - 2.79 \
                            + float( numberWhite[ 0 ] ) * 1.34

      command = 'VipSingleThreshold' + \
                ' -i ' + fileNameUnbiasedT1 + \
                ' -o ' + fileNameDilatedVentricles + \
                ' -t ' + str( dilatedVentricleThreshold ) + \
                ' -c b ' + \
                ' -m le'
      executeCommand( self, subjectName, command, viewOnly )

      command = 'VipErosion' + \
                ' -i ' + fileNameClosedVoronoiHemispheres + \
                ' -o ' + fileNameTemporary + \
                ' -s 15 '
      executeCommand( self, subjectName, command, viewOnly )

      command = 'AimsLinearComb' + \
                ' -i ' + fileNameTemporary + \
                ' -j ' + fileNameDilatedVentricles + \
                ' -o ' + fileNameDilatedVentricles + \
                ' -c 1'
      executeCommand( self, subjectName, command, viewOnly )

      command = 'AimsReplaceLevel' + \
                ' -i ' + fileNameDilatedVentricles + \
                ' -o ' + fileNameDilatedVentricles + \
                ' -g 255 510 ' + \
                ' -n 0 255 '
      executeCommand( self, subjectName, command, viewOnly )

      ########################## updating progress bar #########################
      functors[ 'update-progress-bar' ]( subjectName, 80 )

      ############################ use roi mask ################################

      command = 'VipClosing' + \
                ' -i ' + fileNameDilatedVentricles + \
                ' -o ' + fileNameDilatedVentricles + \
                ' -s 1.2'
      executeCommand( self, subjectName, command, viewOnly )

      command = 'VipErosion' + \
                ' -i ' + fileNameDilatedVentricles + \
                ' -o ' + fileNameTemporary + \
                ' -s 1.2 '
      executeCommand( self, subjectName, command, viewOnly )

      command = 'VipDilation' + \
                ' -i ' + fileNameTemporary + \
                ' -o ' + fileNameDilatedVentricles + \
                ' -s 4 '
      executeCommand( self, subjectName, command, viewOnly )

      command = 'AimsConnectComp' + \
                ' -i ' + fileNameDilatedVentricles + \
                ' -o ' + fileNameDilatedVentricles + \
                ' -n 1 ' + \
                ' -c 6'
      executeCommand( self, subjectName, command, viewOnly )

      command = 'VipDilation' + \
                ' -i ' + fileNameDilatedVentricles + \
                ' -o ' + fileNameDilatedVentricles + \
                ' -s 10 '
      executeCommand( self, subjectName, command, viewOnly )

      command = 'AimsLinearComb' + \
                ' -i ' + fileNameClosedVoronoiHemispheres + \
                ' -j ' + fileNameDilatedVentricles + \
                ' -o ' + fileNameDilatedVentricles + \
                ' -c 1'
      executeCommand( self, subjectName, command, viewOnly )

      ######################## updating progress bar #########################
      functors[ 'update-progress-bar' ]( subjectName, 85 )

      command = 'VipSingleThreshold' + \
                ' -i ' + fileNameDilatedVentricles + \
                ' -o ' + fileNameDilatedVentricles + \
                ' -t 255' + \
                ' -c b ' + \
                ' -m gt'
      executeCommand( self, subjectName, command, viewOnly )

      command = 'AimsReplaceLevel' + \
                ' -i ' + fileNameDilatedVentricles + \
                ' -o ' + fileNameDilatedVentricles + \
                ' -g 255 1 ' + \
                ' -n 2000 2000 '
      executeCommand( self, subjectName, command, viewOnly )

      command = 'AimsLinearComb' + \
                ' -i ' + fileNameUnbiasedT1 + \
                ' -j ' + fileNameDilatedVentricles + \
                ' -o ' + fileNameVentricles + \
                ' -c 1'
      executeCommand( self, subjectName, command, viewOnly )

      command = 'VipDoubleThreshold' + \
                ' -i ' + fileNameVentricles + \
                ' -o ' + fileNameVentricles + \
                ' -tl ' + str( 2000 ) + \
                ' -th ' + str( 2000 + ventricleThreshold ) + \
                ' -c b ' + \
                ' -m b'
      executeCommand( self, subjectName, command, viewOnly )

      command = 'VipClosing' + \
                ' -i ' + fileNameVentricles + \
                ' -o ' + fileNameVentricles + \
                ' -s 1.2'
      executeCommand( self, subjectName, command, viewOnly )

      command = 'VipDilation' + \
                ' -i ' + fileNameTemporary + \
                ' -o ' + fileNameTemporary + \
                ' -s 2.4 '
      executeCommand( self, subjectName, command, viewOnly )

      ######################## updating progress bar #########################
      functors[ 'update-progress-bar' ]( subjectName, 90 )

      command = 'AimsConnectComp' + \
                ' -i ' + fileNameTemporary + \
                ' -o ' + fileNameTemporary + \
                ' -n 1 ' + \
                ' -c 6'
      executeCommand( self, subjectName, command, viewOnly )

      command = 'VipDilation' + \
                ' -i ' + fileNameTemporary + \
                ' -o ' + fileNameDilatedVentricles + \
                ' -s 8 '
      executeCommand( self, subjectName, command, viewOnly )

      command = 'AimsLinearComb' + \
                ' -i ' + fileNameVentricles + \
                ' -j ' + fileNameDilatedVentricles + \
                ' -o ' + fileNameVentricles + \
                ' -c 1'
      executeCommand( self, subjectName, command, viewOnly )

      command = 'VipSingleThreshold' + \
                ' -i ' + fileNameVentricles + \
                ' -o ' + fileNameVentricles + \
                ' -t 255' + \
                ' -c b ' + \
                ' -m gt'
      executeCommand( self, subjectName, command, viewOnly )

      command = 'AimsLinearComb' + \
                ' -i ' + fileNameTractographyMask + \
                ' -j ' + fileNameVentricles + \
                ' -o ' + fileNameTractographyMask + \
                ' -c -1'
      executeCommand( self, subjectName, command, viewOnly )

      command = 'VipSingleThreshold' + \
                ' -i ' + fileNameTractographyMask + \
                ' -o ' + fileNameTractographyMask + \
                ' -t 0' + \
                ' -c b ' + \
                ' -m gt'
      executeCommand( self, subjectName, command, viewOnly )

      ######################## updating progress bar #########################
      functors[ 'update-progress-bar' ]( subjectName, 95 )

      command = 'AimsReplaceLevel' + \
                ' -i ' + fileNameDilatedVentricles + \
                ' -o ' + fileNameDilatedVentricles + \
                ' -g 255 1' + \
                ' -n 2000 2000 '
      executeCommand( self, subjectName, command, viewOnly )

      command = 'AimsLinearComb' + \
                ' -i ' + fileNameUnbiasedT1 + \
                ' -j ' + fileNameDilatedVentricles + \
                ' -o ' + fileNameDilatedVentricles + \
                ' -c 1'
      executeCommand( self, subjectName, command, viewOnly )

      command = 'VipSingleThreshold' + \
                ' -i ' + fileNameDilatedVentricles + \
                ' -o ' + fileNameFornix + \
                ' -t ' + str( 2000 + fornixThreshold ) + \
                ' -c b ' + \
                ' -m ge'
      executeCommand( self, subjectName, command, viewOnly )

      command = 'AimsConnectComp' + \
                ' -i ' + fileNameFornix + \
                ' -o ' + fileNameFornix + \
                ' -n 1 ' + \
                ' -c 6'
      executeCommand( self, subjectName, command, viewOnly )

      command = 'AimsLinearComb' + \
                ' -i ' + fileNameTractographyMask + \
                ' -j ' + fileNameFornix + \
                ' -o ' + fileNameTractographyMask + \
                ' -c 1'
      executeCommand( self, subjectName, command, viewOnly )

      command = 'VipSingleThreshold' + \
                ' -i ' + fileNameTractographyMask + \
                ' -o ' + fileNameTractographyMask + \
                ' -t 0' + \
                ' -c b ' + \
                ' -m gt'
      executeCommand( self, subjectName, command, viewOnly )

      ####################### add commissures ##################################
      if ( addCommissures == 2 ):

        command = 'ConnectAddACPC' + \
                  ' -i ' + fileNameTractographyMask + \
                  ' -o ' + fileNameTractographyMask + \
                  ' -c ' + fileNameCommissureCoordinates
        executeCommand( self, subjectName, command, viewOnly )

      functors[ 'condition-notify-and-release' ]( subjectName,
                                                 'tractography-mask-processed' )

      if ( parameters[ 'removeTemporaryFiles' ].getValue() ):

        if os.path.exists( fileNameWhiteMatterMask ):

          os.remove( fileNameWhiteMatterMask )

        if os.path.exists( fileNameWhiteMatterMask + '.minf' ):

          os.remove( fileNameWhiteMatterMask + '.minf' )

        if os.path.exists( fileNameWhiteMatterMask[ \
                              0: len( fileNameWhiteMatterMask ) - 3 ] + 'dim' ):

          os.remove( fileNameWhiteMatterMask[ \
                               0: len( fileNameWhiteMatterMask ) - 3 ] + 'dim' )

        if os.path.exists( fileNameClosedVoronoiHemispheres ):

          os.remove( fileNameClosedVoronoiHemispheres )

        if os.path.exists( fileNameClosedVoronoiHemispheres + '.minf' ):

          os.remove( fileNameClosedVoronoiHemispheres + '.minf' )

        if os.path.exists( fileNameClosedVoronoiHemispheres[ \
                     0: len( fileNameClosedVoronoiHemispheres ) - 3 ] + 'dim' ):

          os.remove( fileNameClosedVoronoiHemispheres[ \
                      0: len( fileNameClosedVoronoiHemispheres ) - 3 ] + 'dim' )

        if os.path.exists( fileNameVoronoiHemispheres ):

          os.remove( fileNameVoronoiHemispheres )

        if os.path.exists( fileNameVoronoiHemispheres + '.minf' ):

          os.remove( fileNameVoronoiHemispheres + '.minf' )

        if os.path.exists( fileNameVoronoiHemispheres[ \
                           0: len( fileNameVoronoiHemispheres ) - 3 ] + 'dim' ):

          os.remove( fileNameVoronoiHemispheres[ \
                            0: len( fileNameVoronoiHemispheres ) - 3 ] + 'dim' )

        if os.path.exists( fileNameVoronoiCerebellum ):

          os.remove( fileNameVoronoiCerebellum )

        if os.path.exists( fileNameVoronoiCerebellum + '.minf' ):

          os.remove( fileNameVoronoiCerebellum + '.minf' )

        if os.path.exists( fileNameVoronoiCerebellum[ \
                            0: len( fileNameVoronoiCerebellum ) - 3 ] + 'dim' ):

          os.remove( fileNameVoronoiCerebellum[ \
                             0: len( fileNameVoronoiCerebellum ) - 3 ] + 'dim' )

        if os.path.exists( fileNameVoronoiBrain ):

            os.remove( fileNameVoronoiBrain )

        if os.path.exists( fileNameVoronoiBrain + '.minf' ):

          os.remove( fileNameVoronoiBrain + '.minf' )

        if os.path.exists( fileNameVoronoiBrain[ \
                                 0: len( fileNameVoronoiBrain ) - 3 ] + 'dim' ):

          os.remove( fileNameVoronoiBrain[ \
                                  0: len( fileNameVoronoiBrain ) - 3 ] + 'dim' )

        if os.path.exists( fileNameGWHemispheres ):

          os.remove( fileNameGWHemispheres )

        if os.path.exists( fileNameGWHemispheres + '.minf' ):

          os.remove( fileNameGWHemispheres + '.minf' )

        if os.path.exists( fileNameGWHemispheres[ \
                                0: len( fileNameGWHemispheres ) - 3 ] + 'dim' ):

          os.remove( fileNameGWHemispheres[ \
                                 0: len( fileNameGWHemispheres ) - 3 ] + 'dim' )

        if os.path.exists( fileNameRootsVoronoi ):

          os.remove( fileNameRootsVoronoi )

        if os.path.exists( fileNameRootsVoronoi + '.minf' ):

          os.remove( fileNameRootsVoronoi + '.minf' )

        if os.path.exists( fileNameRootsVoronoi[ \
                              0: len( fileNameRootsVoronoi ) - 3 ] + 'dim' ):

          os.remove( fileNameRootsVoronoi[ \
                               0: len( fileNameRootsVoronoi ) - 3 ] + 'dim' )

        if os.path.exists( fileNameVentricles ):

          os.remove( fileNameVentricles )

        if os.path.exists( fileNameVentricles + '.minf' ):

          os.remove( fileNameVentricles + '.minf' )

        if os.path.exists( fileNameVentricles[ \
                                   0: len( fileNameVentricles ) - 3 ] + 'dim' ):

          os.remove( fileNameVentricles[ \
                                    0: len( fileNameVentricles ) - 3 ] + 'dim' )

        if os.path.exists( fileNameDilatedVentricles ):

          os.remove( fileNameDilatedVentricles )

        if os.path.exists( fileNameDilatedVentricles + '.minf' ):

          os.remove( fileNameDilatedVentricles + '.minf' )

        if os.path.exists( fileNameDilatedVentricles[ \
                            0: len( fileNameDilatedVentricles ) - 3 ] + 'dim' ):

          os.remove( fileNameDilatedVentricles[ \
                             0: len( fileNameDilatedVentricles ) - 3 ] + 'dim' )

        if os.path.exists( fileNameFornix ):

          os.remove( fileNameFornix )

        if os.path.exists( fileNameFornix + '.minf' ):

          os.remove( fileNameFornix + '.minf' )

        if os.path.exists( fileNameFornix[ \
                                       0: len( fileNameFornix ) - 3 ] + 'dim' ):

          os.remove( fileNameFornix[ 0: len( fileNameFornix ) - 3 ] + 'dim' )

        if os.path.exists( fileNameNormalizedROI ):

          os.remove( fileNameNormalizedROI )

        if os.path.exists( fileNameNormalizedROI + '.minf' ):

          os.remove( fileNameNormalizedROI + '.minf' )
        if os.path.exists( fileNameNormalizedROI[ \
                                0: len( fileNameNormalizedROI ) - 3 ] + 'dim' ):

          os.remove( fileNameNormalizedROI[ \
                                 0: len( fileNameNormalizedROI ) - 3 ] + 'dim' )

        if os.path.exists( fileNameROIMaskToRemoveS16 ):

          os.remove( fileNameROIMaskToRemoveS16 )

        if os.path.exists( fileNameROIMaskToRemoveS16 + '.minf' ):

          os.remove( fileNameROIMaskToRemoveS16 + '.minf' )
        if os.path.exists( fileNameROIMaskToRemoveS16[ \
                                0: len( fileNameROIMaskToRemoveS16 ) - 3 ] + 'dim' ):

          os.remove( fileNameROIMaskToRemoveS16[ \
                                 0: len( fileNameROIMaskToRemoveS16 ) - 3 ] + 'dim' )


        if os.path.exists( fileNameTemporary ):

          os.remove( fileNameTemporary )

        if os.path.exists( fileNameTemporary + '.minf' ):

          os.remove( fileNameTemporary + '.minf' )

        if os.path.exists( fileNameTemporary[ \
                                    0: len( fileNameTemporary ) - 3 ] + 'dim' ):

          os.remove( fileNameTemporary[ \
                                     0: len( fileNameTemporary ) - 3 ] + 'dim' )

      ########################## updating progress bar #########################
      functors[ 'update-progress-bar' ]( subjectName, 100 )

    except:

      self._functors[ 'task-set-status-as-failed' ]( subjectName,
                                                     sys.exc_info() )

  ##############################################################################
  # display()
  ##############################################################################

  def display( self, functors, parameters, subjectName, verbose,
               isReportModeActive ):

    ########################### resetting the viewer ###########################
    functors[ 'viewer-reset' ]()

    ####################### collecting output work directory ###################
    outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()
    if ( verbose ):

      print  'Output work directory :', outputWorkDirectory

    ####################### collecting input data filename(s) #################
    anatomyAndTalairachDirectory = parameters[ \
                                   'anatomyAndTalairachDirectory' ].getValue()

    fileNameUnbiasedT1 = os.path.join( anatomyAndTalairachDirectory,
                                       'Morphologist',
                                       'nobias_t1' )
    fileNameVoronoiMask = os.path.join( anatomyAndTalairachDirectory,
                                        'Morphologist',
                                        'voronoi_t1' )
    addROIMask = parameters[ 'addROIMask' ].getValue()
    fileNameROIMaskToAdd = parameters[ 'fileNameROIMaskToAdd' ].getValue()
    removeROIMask = parameters[ 'removeROIMask' ].getValue()
    fileNameROIMaskToRemove = parameters[ 'fileNameROIMaskToRemove' ].getValue()

    #################### creating intermediate filename(s) #################
    fileNameSulciSkeleton = os.path.join( \
                                         outputWorkDirectory,
                                         'sulci_fileNameSulciSkeleton.ima' )
    fileNameTractographyMask = os.path.join( \
                                         outputWorkDirectory,
                                         'tractography_mask.ima' )


    ####################### adding result to viewer ###########################
    if ( addROIMask == 2 or removeROIMask == 2 ):

      view = 'second_view'

    else:

      view = 'first_view'

    self.viewerSetView( view )

    functors[ 'viewer-create-referential' ]( 'frameT1' )

    functors[ 'viewer-load-object' ]( fileNameUnbiasedT1,
                                      'unbiasedT1' )
    functors[ 'viewer-assign-referential-to-object' ]( 'frameT1',
                                                       'unbiasedT1' )
    functors[ 'viewer-assign-referential-to-window' ]( 'frameT1',
                                                       view,
                                                       'unbiased T1(ax)' )
    functors[ 'viewer-assign-referential-to-window' ]( 'frameT1',
                                                       view,
                                                       'unbiased T1(sag)' )
    functors[ 'viewer-assign-referential-to-window' ]( 'frameT1',
                                                       view,
                                                       'unbiased T1(cor)' )
    functors[ 'viewer-add-object-to-window' ]( 'unbiasedT1',
                                               view,
                                               'unbiased T1(ax)' )
    functors[ 'viewer-add-object-to-window' ]( 'unbiasedT1',
                                               view,
                                               'unbiased T1(sag)' )
    functors[ 'viewer-add-object-to-window' ]( 'unbiasedT1',
                                               view,
                                               'unbiased T1(cor)' )

    if ( addROIMask == 2 ):

      functors[ 'viewer-load-object' ]( fileNameROIMaskToAdd,
                                        'ROIMaskAdded' )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameT1',
                                                         'ROIMaskAdded' )
      functors[ 'viewer-assign-referential-to-window' ]( 'frameT1',
                                                         view,
                                                         'Roi mask added' )
      functors[ 'viewer-add-object-to-window' ]( 'ROIMaskAdded',
                                                 view,
                                                 'Roi mask added' )

    if ( removeROIMask == 2 ):

      functors[ 'viewer-load-object' ]( fileNameROIMaskToRemove,
                                        'ROIMaskRemoved' )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameT1',
                                                         'ROIMaskRemoved' )
      functors[ 'viewer-assign-referential-to-window' ]( 'frameT1',
                                                         view,
                                                         'Roi mask removed' )
      functors[ 'viewer-add-object-to-window' ]( 'ROIMaskRemoved',
                                                 view,
                                                 'Roi mask removed' )

    functors[ 'viewer-load-object' ]( fileNameVoronoiMask,
                                      'voronoiMask' )
    functors[ 'viewer-assign-referential-to-object' ]( 'frameT1',
                                                       'voronoiMask' )
    functors[ 'viewer-assign-referential-to-window' ]( 'frameT1',
                                                       view,
                                                       'Voronoi mask' )
    functors[ 'viewer-add-object-to-window' ]( 'voronoiMask', view,
                                               'Voronoi mask' )

    ####################### waiting for result ################################
    functors[ 'condition-wait-and-release' ]( subjectName,
                                              'sulci-skeleton-processed' )

    ####################### adding result to viewer ###########################
    functors[ 'viewer-load-object' ]( fileNameSulciSkeleton,
                                      'sulciSkeleton' )
    functors[ 'viewer-assign-referential-to-object' ]( 'frameT1',
                                                       'sulciSkeleton' )
    functors[ 'viewer-assign-referential-to-window' ]( 'frameT1',
                                                       view,
                                                       'Sulci skeleton' )
    functors[ 'viewer-add-object-to-window' ]( 'sulciSkeleton',
                                               view,
                                               'Sulci skeleton' )

    ####################### waiting for result ################################
    functors[ 'condition-wait-and-release' ]( subjectName, 
                                              'tractography-mask-processed' )

    ####################### adding result to viewer ###########################
    functors[ 'viewer-load-object' ]( fileNameTractographyMask,
                                      'tractographyMask' )

    functors[ 'viewer-assign-referential-to-object' ]( 'frameT1',
                                                       'tractographyMask' )
    functors[ 'viewer-set-colormap' ]( 'tractographyMask', 'BLUE-lfusion' )
    functors[ 'viewer-assign-referential-to-window' ]( 'frameT1',
                                                       view,
                                                       'mask(ax)' )
    functors[ 'viewer-assign-referential-to-window' ]( 'frameT1',
                                                       view,
                                                       'mask(sag)' )
    functors[ 'viewer-assign-referential-to-window' ]( 'frameT1',
                                                       view,
                                                       'mask(cor)' )
    functors[ 'viewer-add-object-to-window' ]( 'tractographyMask',
                                               view,
                                               'mask(ax)' )
    functors[ 'viewer-add-object-to-window' ]( 'tractographyMask',
                                               view,
                                               'mask(sag)' )
    functors[ 'viewer-add-object-to-window' ]( 'tractographyMask',
                                               view,
                                               'mask(cor)' )

    if( len( fileNameUnbiasedT1 ) ):

      functors[ 'viewer-fusion-objects' ]( [ 'unbiasedT1', 'tractographyMask'],
                                             'fusionT1AndMask',
                                             'Fusion2DMethod' )
    else:

      functors[ 'viewer-load-object' ]( fileNameTractographyMask,
                                        'fusionT1AndMask' )

    functors[ 'viewer-assign-referential-to-object' ]( 'frameT1',
                                                       'fusionT1AndMask')

    functors[ 'viewer-assign-referential-to-window' ]( 'frameT1',
                                                       view,
                                                       'T1/mask fusion(ax)' )
    functors[ 'viewer-assign-referential-to-window' ]( 'frameT1',
                                                       view,
                                                       'T1/mask fusion(sag)' )
    functors[ 'viewer-assign-referential-to-window' ]( 'frameT1',
                                                       view,
                                                       'T1/mask fusion(cor)' )
    functors[ 'viewer-add-object-to-window' ]( 'fusionT1AndMask',
                                               view,
                                               'T1/mask fusion(ax)' )
    functors[ 'viewer-add-object-to-window' ]( 'fusionT1AndMask',
                                               view,
                                               'T1/mask fusion(sag)' )
    functors[ 'viewer-add-object-to-window' ]( 'fusionT1AndMask',
                                               view,
                                               'T1/mask fusion(cor)' )


