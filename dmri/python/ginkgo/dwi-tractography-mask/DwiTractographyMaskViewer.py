from Viewer import *


class DwiTractographyMaskViewer( Viewer ):

  def __init__( self, minimumSize, parent ):

    Viewer.__init__( self, minimumSize, parent )

    ########################## First View ######################################

    self.createView( 'first_view' )

    view = self.getViewFromName( 'first_view' )
    # unbiased T1 windows
    self.addAxialWindow( 'first_view', 0, 0, 1, 1,
                         'unbiased T1(ax)', 'unbiased T1(ax)',
                         [ 0.0, 0.0, 0.0, 1.0 ], False )
    self.addSagittalWindow( 'first_view', 0, 1, 1, 1,
                            'unbiased T1(sag)', 'unbiased T1(sag)',
                            [ 0.0, 0.0, 0.0, 1.0 ], False )
    self.addCoronalWindow( 'first_view', 1, 0, 1, 1,
                           'unbiased T1(cor)', 'unbiased T1(cor)',
                           [ 0.0, 0.0, 0.0, 1.0 ], False )

    # G-W/Voronoi/Deep nuclei windows
    self.addAxialWindow( 'first_view', 0, 3, 1, 1,
                         'Voronoi mask', 'Voronoi mask' )
    self.addAxialWindow( 'first_view', 0, 2, 1, 1,
                         'Sulci skeleton', 'Sulci skeleton' )
    # mask windows
    self.addAxialWindow( 'first_view', 2, 0, 1, 1,
                         'mask(ax)', 'mask(ax)',
                         [ 0.0, 0.0, 0.0, 1.0 ], False )
    self.addSagittalWindow( 'first_view', 2, 1, 1, 1,
                            'mask(sag)', 'mask(sag)',
                            [ 0.0, 0.0, 0.0, 1.0 ], False )
    self.addCoronalWindow( 'first_view', 3, 0, 1, 1,
                           'mask(cor)', 'mask(cor)',
                           [ 0.0, 0.0, 0.0, 1.0 ], False )

    # T1/mask fusion windows
    self.addAxialWindow( 'first_view', 2, 2, 1, 1,
                         'T1/mask fusion(ax)', 'T1/mask fusion(ax)',
                         [ 0.0, 0.0, 0.0, 1.0 ], False )
    self.addSagittalWindow( 'first_view', 2, 3, 1, 1,
                            'T1/mask fusion(sag)', 'T1/mask fusion(sag)',
                            [ 0.0, 0.0, 0.0, 1.0 ], False )
    self.addCoronalWindow( 'first_view', 3, 2, 1, 1,
                           'T1/mask fusion(cor)', 'T1/mask fusion(cor)',
                           [ 0.0, 0.0, 0.0, 1.0 ], False )

    ########################## Second View ######################################

    self.createView( 'second_view' )

    view = self.getViewFromName( 'second_view' )

    # unbiased T1 windows
    self.addAxialWindow( 'second_view', 0, 0, 1, 1,
                         'unbiased T1(ax)', 'unbiased T1(ax)',
                         [ 0.0, 0.0, 0.0, 1.0 ], False )
    self.addSagittalWindow( 'second_view', 0, 1, 1, 1,
                            'unbiased T1(sag)', 'unbiased T1(sag)',
                            [ 0.0, 0.0, 0.0, 1.0 ], False )
    self.addCoronalWindow( 'second_view', 1, 0, 1, 1,
                           'unbiased T1(cor)', 'unbiased T1(cor)',
                           [ 0.0, 0.0, 0.0, 1.0 ], False )

    # G-W/Voronoi/Deep nuclei windows
    self.addAxialWindow( 'second_view', 0, 3, 1, 1,
                         'Voronoi mask', 'Voronoi mask' )
    self.addAxialWindow( 'second_view', 0, 2, 1, 1,
                         'Sulci skeleton', 'Sulci skeleton' )
    self.addAxialWindow( 'second_view', 1, 2, 1, 1,
                         'Roi mask removed', 'Roi mask removed' )
    self.addAxialWindow( 'second_view', 1, 3, 1, 1,
                         'Roi mask added', 'Roi mask added' )

    # mask windows
    self.addAxialWindow( 'second_view', 2, 0, 1, 1,
                         'mask(ax)', 'mask(ax)',
                         [ 0.0, 0.0, 0.0, 1.0 ], False )
    self.addSagittalWindow( 'second_view', 2, 1, 1, 1,
                            'mask(sag)', 'mask(sag)',
                            [ 0.0, 0.0, 0.0, 1.0 ], False )
    self.addCoronalWindow( 'second_view', 3, 0, 1, 1,
                           'mask(cor)', 'mask(cor)',
                           [ 0.0, 0.0, 0.0, 1.0 ], False )

    # T1/mask fusion windows
    self.addAxialWindow( 'second_view', 2, 2, 1, 1,
                         'T1/mask fusion(ax)', 'T1/mask fusion(ax)',
                         [ 0.0, 0.0, 0.0, 1.0 ], False )
    self.addSagittalWindow( 'second_view', 2, 3, 1, 1,
                            'T1/mask fusion(sag)', 'T1/mask fusion(sag)',
                            [ 0.0, 0.0, 0.0, 1.0 ], False )
    self.addCoronalWindow( 'second_view', 3, 2, 1, 1,
                           'T1/mask fusion(cor)', 'T1/mask fusion(cor)',
                           [ 0.0, 0.0, 0.0, 1.0 ], False )

def createDwiTractographyMaskViewer( minimumSize, parent ):

  return DwiTractographyMaskViewer( minimumSize, parent )

