from Algorithm import *
from DwiTractographyTask import *


class DwiTractographyAlgorithm( Algorithm ):

  def __init__( self, verbose ):

    Algorithm.__init__( self, 'DWI-Tractography', verbose, True )

    ###########################################################################
    # Input data
    ###########################################################################

    self.addParameter( StringParameter( 'fileNameOdfTextureMap', '' ) )
    self.addParameter( StringParameter( 'fileNameOdfSiteMap', '' ) )
    self.addParameter( StringParameter( 'fileNameRgb', '' ) )
    self.addParameter( StringParameter( 'fileNameT1', '' ) )
    self.addParameter( StringParameter( 'fileNameTransformationDwToT1', '' ) )

    ###########################################################################
    # Tracking mask type interface
    ###########################################################################

    # Tracking mask
    self.addParameter( StringParameter( 'fileNameMask', '' ) )
    self.addParameter( StringParameter( 'fileNameTransformationMaskToDw', '' ) )

    # Tracking parameters
    self.addParameter( ChoiceParameter(
                                       'trackingType',
                                       0,
                                       [ 'streamline deterministic',
                                         'streamline regularized deterministic',
                                         'streamline probabilistic' ] ) )
                                         
    # Streamline Deterministic tracking type
    self.addParameter( IntegerParameter(
                                     'deterministicVoxelSamplerPointCount',
                                      1, 1, 1000, 1  ) )
    self.addParameter( DoubleParameter( 'deterministicForwardStep',
                                        0.2, 0.001, 10, 0.001 ) )
    self.addParameter( IntegerParameter( 'deterministicStoringIncrement',
                                          10, 1, 50, 1 ) )
    self.addParameter( DoubleParameter( 'deterministicApertureAngle',
                                         30, 0, 360, 10 ) )
    self.addParameter( DoubleParameter( 'deterministicMinimumFiberLength',
                                         0.01, 0.01, 500, 0.01 ) )
    self.addParameter( DoubleParameter( 'deterministicMaximumFiberLength',
                                         300.00, 0.01, 500, 0.01 ) )

    # Regularised deterministic type
    self.addParameter( IntegerParameter(
                             'regularizedDeterministicVoxelSamplerPointCount',
                             1, 1, 1000, 1 ) )
    self.addParameter( DoubleParameter(
                             'regularizedDeterministicForwardStep',
                             0.2, 0.001, 10, 0.001 ) )
    self.addParameter( IntegerParameter(
                             'regularizedDeterministicStoringIncrement',
                              10, 1, 50, 1 ) )
    self.addParameter( DoubleParameter(
                             'regularizedDeterministicApertureAngle',
                              30, 0, 360, 10 ) )
    self.addParameter( DoubleParameter(
                             'regularizedDeterministicMinimumFiberLength',
                              5.00, 0.01, 500, 0.01 ) )
    self.addParameter( DoubleParameter(
                             'regularizedDeterministicMaximumFiberLength',
                              300.00, 0.01, 500, 0.01 ) )
    self.addParameter( DoubleParameter(
                             'regularizedDeterministicLowerGFABoundary',
                             -1.0, -1.0, 1.0, 0.01 ) )
    self.addParameter( DoubleParameter(
                             'regularizedDeterministicUpperGFABoundary',
                             -1.0, -1.0, 1.0, 0.01 ) )

    # Probabilistic type
    self.addParameter( IntegerParameter( 'probabilisticVoxelSamplerPointCount',
                                         1, 1, 1000, 1  ) )
    self.addParameter( DoubleParameter( 'probabilisticForwardStep',
                                         0.2, 0.001, 10, 0.001 ) )
    self.addParameter( IntegerParameter( 'probabilisticStoringIncrement',
                                         10, 1, 50, 1 ) )
    self.addParameter( DoubleParameter( 'probabilisticApertureAngle',
                                        90, 0, 360, 10 ) )
    self.addParameter( DoubleParameter( 'probabilisticMinimumFiberLength',
                                        5.00, 0.01, 500, 0.01 ) )
    self.addParameter( DoubleParameter( 'probabilisticMaximumFiberLength',
                                        300.00, 0.01, 500, 0.01 ) )
    self.addParameter( DoubleParameter( 'probabilisticGibbsTemperature',
                                         1.0, 0.0, 100.0, 1.0 ) )
    self.addParameter( DoubleParameter( 'probabilisticLowerGFABoundary',
                                        -1.0, -1.0, 1.0, 0.01 ) )
    self.addParameter( DoubleParameter( 'probabilisticUpperGFABoundary',
                                        -1.0, -1.0, 1.0, 0.01 ) )

    self.addParameter( IntegerParameter( 'outputOrientationCount',
                                          500, 6, 4000, 1 ) )
    self.addParameter( IntegerParameter( 'stepCount', 1, 1, 1000, 1 ) )

    ###########################################################################
    # Output
    ###########################################################################

    self.addParameter( ChoiceParameter( 'bundleMapFormat',
                                        0,
                                        [ 'aimsbundlemap',
                                          'bundlemap',
                                          'vtkbundlemap',
                                          'trkbundlemap' ] ) )


  def launch( self ):

    if ( self._verbose ):

      print \
        'running DWI tractography'

    task = DwiTractographyTask( self._application )
    task.launch( False )


  def view( self ):

    if ( self._verbose ):

      print \
        'viewing DWI tractography'

    task = DwiTractographyTask( self._application )
    task.launch( True )
