from AlgorithmGUI import *
from ResourceManager import *


class DwiTractographyAlgorithmGUI( AlgorithmGUI ):

  def __init__( self, algorithm ):

    AlgorithmGUI.__init__( self,
                           algorithm,
                           ResourceManager().getUIFileName(
                                                        'dmri',
                                                        'DwiTractography.ui' ) )

    ############################################################################
    # Connecting data interface
    ############################################################################

    # Input ODF texture map and push button
    self.connectStringParameterToLineEdit( 'fileNameOdfTextureMap',
                                           'lineEdit_FileNameOdfTextureMap' )

    self.connectFileBrowserToPushButtonAndLineEdit(
                                             'pushButton_FileNameOdfTextureMap',
                                             'lineEdit_FileNameOdfTextureMap' )

    # Input ODF site map
    self.connectStringParameterToLineEdit( 'fileNameOdfSiteMap',
                                           'lineEdit_FileNameOdfSiteMap')
    self.connectFileBrowserToPushButtonAndLineEdit(
                                                'pushButton_FileNameOdfSiteMap',
                                                'lineEdit_FileNameOdfSiteMap' )

    # Input RGB and pusk button
    self.connectStringParameterToLineEdit( 'fileNameRgb',
                                           'lineEdit_FileNameRgb' )
    self.connectFileBrowserToPushButtonAndLineEdit( 'pushButton_FileNameRgb',
                                                    'lineEdit_FileNameRgb' )

    # Input T1 and push button
    self.connectStringParameterToLineEdit( 'fileNameT1', 'lineEdit_FileNameT1' )
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit( 
                                                    'pushButton_FileNameT1',
                                                    'lineEdit_FileNameT1' )

    # Input DWToT1Transform and push button
    self.connectStringParameterToLineEdit(
                                       'fileNameTransformationDwToT1',
                                       'lineEdit_FileNameTransformationDwToT1' )
    self.connectFileBrowserToPushButtonAndLineEdit(
                                      'pushButton_FileNameTransformationDwToT1',
                                      'lineEdit_FileNameTransformationDwToT1' )

    ############################################################################
    # Connecting Tracking mask data
    ############################################################################

    self.connectStringParameterToLineEdit( 'fileNameMask',
                                           'lineEdit_FileNameMask' )
    self.connectFileOrDirectoryBrowserToPushButtonAndLineEdit(
                                                    'pushButton_FileNameMask',
                                                    'lineEdit_FileNameMask' )

    self.connectStringParameterToLineEdit(
                                     'fileNameTransformationMaskToDw',
                                     'lineEdit_FileNameTransformationMaskToDw' )
    self.connectFileBrowserToPushButtonAndLineEdit(
                                    'pushButton_FileNameTransformationMaskToDw',
                                    'lineEdit_FileNameTransformationMaskToDw' )

    ############################################################################
    # Connecting tracking type parameters interface
    ############################################################################

    self.connectChoiceParameterToComboBox( 'trackingType',
                                           'comboBox_TrackingType' )
    self.connectComboBoxToStackedWidget( 'comboBox_TrackingType',
                                         'stackedWidget_TrackingType' )

    # Connecting deterministic tracking type
    self.connectIntegerParameterToSpinBox(
                                'deterministicVoxelSamplerPointCount',
                                'spinBox_DeterministicVoxelSamplerPointCount' )
    self.connectDoubleParameterToSpinBox(
                                      'deterministicForwardStep',
                                      'doubleSpinBox_DeterministicForwardStep' )
    self.connectIntegerParameterToSpinBox(
                                      'deterministicStoringIncrement',
                                      'spinBox_DeterministicStoringIncrement' )
    self.connectDoubleParameterToSpinBox(
                                 'deterministicApertureAngle',
                                 'doubleSpinBox_DeterministicApertureAngle' )
    self.connectDoubleParameterToSpinBox(
                               'deterministicMinimumFiberLength',
                               'doubleSpinBox_DeterministicMinimumFiberLength' )
    self.connectDoubleParameterToSpinBox(
                               'deterministicMaximumFiberLength',
                               'doubleSpinBox_DeterministicMaximumFiberLength' )

    # Connecting regularized deterministic tracking type
    self.connectIntegerParameterToSpinBox(
                     'regularizedDeterministicVoxelSamplerPointCount',
                     'spinBox_RegularizedDeterministicVoxelSamplerPointCount' )
    self.connectDoubleParameterToSpinBox(
                           'regularizedDeterministicForwardStep',
                           'doubleSpinBox_RegularizedDeterministicForwardStep' )
    self.connectIntegerParameterToSpinBox(
                           'regularizedDeterministicStoringIncrement',
                           'spinBox_RegularizedDeterministicStoringIncrement' )
    self.connectDoubleParameterToSpinBox(
                        'regularizedDeterministicApertureAngle',
                        'doubleSpinBox_RegularizedDeterministicApertureAngle' )
    self.connectDoubleParameterToSpinBox(
                    'regularizedDeterministicMinimumFiberLength',
                    'doubleSpinBox_RegularizedDeterministicMinimumFiberLength' )
    self.connectDoubleParameterToSpinBox(
                    'regularizedDeterministicMaximumFiberLength',
                    'doubleSpinBox_RegularizedDeterministicMaximumFiberLength' )
    self.connectDoubleParameterToSpinBox(
                      'regularizedDeterministicLowerGFABoundary',
                      'doubleSpinBox_RegularizedDeterministicLowerGFABoundary' )
    self.connectDoubleParameterToSpinBox(
                      'regularizedDeterministicUpperGFABoundary',
                      'doubleSpinBox_RegularizedDeterministicUpperGFABoundary' )

    # Connecting probabilistic tracking type
    self.connectIntegerParameterToSpinBox(
                                'probabilisticVoxelSamplerPointCount',
                                'spinBox_ProbabilisticVoxelSamplerPointCount' )
    self.connectDoubleParameterToSpinBox(
                                      'probabilisticForwardStep',
                                      'doubleSpinBox_ProbabilisticForwardStep' )
    self.connectIntegerParameterToSpinBox(
                                       'probabilisticStoringIncrement',
                                       'spinBox_ProbabilisticStoringIncrement' )
    self.connectDoubleParameterToSpinBox(
                                    'probabilisticApertureAngle',
                                    'doubleSpinBox_ProbabilisticApertureAngle' )
    self.connectDoubleParameterToSpinBox(
                               'probabilisticMinimumFiberLength',
                               'doubleSpinBox_ProbabilisticMinimumFiberLength' )
    self.connectDoubleParameterToSpinBox(
                               'probabilisticMaximumFiberLength',
                               'doubleSpinBox_ProbabilisticMaximumFiberLength' )
    self.connectDoubleParameterToSpinBox(
                               'probabilisticLowerGFABoundary',
                               'doubleSpinBox_ProbabilisticLowerGFABoundary' )
    self.connectDoubleParameterToSpinBox(
                               'probabilisticUpperGFABoundary',
                               'doubleSpinBox_ProbabilisticUpperGFABoundary' )
    self.connectDoubleParameterToSpinBox(
                                 'probabilisticGibbsTemperature',
                                 'doubleSpinBox_ProbabilisticGibbsTemperature' )

    # Connecting Output orientation count and Step count
    self.connectIntegerParameterToSpinBox( 'outputOrientationCount',
                                           'spinBox_OutputOrientationCount' )
    self.connectIntegerParameterToSpinBox( 'stepCount', 'spinBox_StepCount' )

    ############################################################################
    # Connecting output bundle map format interface
    ############################################################################

    self.connectChoiceParameterToComboBox( 'bundleMapFormat',
                                           'comboBox_BundleMapFormat' )

