from Task import *
import time


class DwiTractographyTask( Task ):

  def __init__( self, application ):
  
    Task.__init__( self, application )

    
  ##############################################################################
  # execute()
  ##############################################################################

  def execute( self, functors, parameters, subjectName, verbose, viewOnly ):

    try:

      ####################### acquiring conditions #############################
      functors[ 'condition-acquire' ]( subjectName, 'tractogram-processed' )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 0 )

      ########################### collecting file names ########################
      outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()

      fileNameOdfTextureMap = parameters[ 'fileNameOdfTextureMap' ].getValue()
      fileNameOdfSiteMap = parameters[ 'fileNameOdfSiteMap' ].getValue()
      fileNameRgb = parameters[ 'fileNameRgb' ].getValue()

      fileNameT1 = parameters[ 'fileNameT1' ].getValue()
      fileNameTransformationDwToT1 = \
                        parameters[ 'fileNameTransformationDwToT1' ].getValue()

      fileNameMask = parameters[ 'fileNameMask' ].getValue()
      fileNameTransformationMaskToDw = \
                       parameters[ 'fileNameTransformationMaskToDw' ].getValue()

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 5 )

      ##################### setting options according to tractography type #####
      tractographyType = parameters[ 'trackingType' ].getChoice()
      outputOrientationCount = parameters[ 'outputOrientationCount' ].getValue()
      stepCount = parameters[ 'stepCount' ].getValue()

      command = 'GkgExecuteCommand DwiTractography' + \
                ' -s ' + fileNameOdfSiteMap + \
                ' -o ' + fileNameOdfTextureMap + \
                ' -r ' + fileNameMask + \
                ' -m ' + fileNameMask

      if ( len( fileNameTransformationMaskToDw ) ):
        
        command += ' -tr ' + fileNameTransformationMaskToDw + \
                  ' -tm ' + fileNameTransformationMaskToDw

      tractographyName = ''
      stringOfParameters =''
      if ( tractographyType == 'streamline deterministic' ):

        tractographyName = 'streamline-deterministic'
        voxelSamplerPointCount = \
          parameters[ 'deterministicVoxelSamplerPointCount' ].getValue()
        forwardStep = \
          parameters[ 'deterministicForwardStep' ].getValue()
        storingIncrement = \
          parameters[ 'deterministicStoringIncrement' ].getValue()
        minimumFiberLength = \
          parameters[ 'deterministicMinimumFiberLength' ].getValue()
        maximumFiberLength = \
          parameters[ 'deterministicMaximumFiberLength' ].getValue()
        apertureAngle = \
          parameters[ 'deterministicApertureAngle' ].getValue()

        stringOfParameters = str( voxelSamplerPointCount ) + ' ' + \
                            str( forwardStep ) + ' ' + \
                            str( storingIncrement ) + ' ' + \
                            str( minimumFiberLength ) + ' ' + \
                            str( maximumFiberLength ) + ' ' + \
                            str( apertureAngle )

      elif ( tractographyType == 'streamline regularized deterministic' ):

        tractographyName = 'streamline-regularized-deterministic'
        voxelSamplerPointCount = \
          parameters[ 'regularizedDeterministicVoxelSamplerPointCount' ].\
                                                                      getValue()
        forwardStep = \
          parameters[ 'regularizedDeterministicForwardStep' ].getValue()
        storingIncrement = \
          parameters[ 'regularizedDeterministicStoringIncrement' ].getValue()
        minimumFiberLength = \
          parameters[ 'regularizedDeterministicMinimumFiberLength' ].getValue()
        maximumFiberLength = \
          parameters[ 'regularizedDeterministicMaximumFiberLength' ].getValue()
        apertureAngle = \
          parameters[ 'regularizedDeterministicApertureAngle' ].getValue()
        lowerGFABoundary = \
          parameters[ 'regularizedDeterministicLowerGFABoundary' ].getValue()
        upperGFABoundary = \
          parameters[ 'regularizedDeterministicUpperGFABoundary' ].getValue()

        stringOfParameters = str( voxelSamplerPointCount ) + ' ' + \
                            str( forwardStep ) + ' ' + \
                            str( storingIncrement ) + ' ' + \
                            str( minimumFiberLength ) + ' ' + \
                            str( maximumFiberLength ) + ' ' + \
                            str( apertureAngle ) + ' ' + \
                            str( lowerGFABoundary ) + ' ' + \
                            str( upperGFABoundary )

      elif ( tractographyType == 'streamline probabilistic' ):

        tractographyName = 'streamline-probabilistic'
        voxelSamplerPointCount = \
          parameters[ 'probabilisticVoxelSamplerPointCount' ].getValue()
        forwardStep = \
          parameters[ 'probabilisticForwardStep' ].getValue()
        storingIncrement = \
          parameters[ 'probabilisticStoringIncrement' ].getValue()
        minimumFiberLength = \
          parameters[ 'probabilisticMinimumFiberLength' ].getValue()
        maximumFiberLength = \
          parameters[ 'probabilisticMaximumFiberLength' ].getValue()
        apertureAngle = \
          parameters[ 'probabilisticApertureAngle' ].getValue()
        lowerGFABoundary = \
          parameters[ 'probabilisticLowerGFABoundary' ].getValue()
        upperGFABoundary = \
          parameters[ 'probabilisticUpperGFABoundary' ].getValue()
        gibbsTemperature = \
          parameters[ 'probabilisticGibbsTemperature' ].getValue()

        stringOfParameters = str( voxelSamplerPointCount ) + ' ' + \
                            str( forwardStep ) + ' ' + \
                            str( storingIncrement ) + ' ' + \
                            str( minimumFiberLength ) + ' ' + \
                            str( maximumFiberLength ) + ' ' + \
                            str( apertureAngle ) + ' ' + \
                            str( gibbsTemperature ) + ' ' + \
                            str( lowerGFABoundary ) + ' ' + \
                            str( upperGFABoundary )

      fileNameBundleMap = os.path.join( outputWorkDirectory,
                                        'tractography-' + tractographyName )
      bundleMapFormat = parameters[ 'bundleMapFormat' ].getChoice()
                                        
      command += ' -type ' + tractographyName + \
                  ' -b ' + fileNameBundleMap + \
                  ' -scalarParameters ' + stringOfParameters + \
                  ' -outputOrientationCount ' + \
                  str( outputOrientationCount ) + \
                  ' -stepCount ' + str( stepCount ) + \
                  ' -bundleMapFormat ' + 'aimsbundlemap' + \
                  ' -verbose true'

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 20 )

      ########################### GkgDwiOdfField ###############################
      executeCommand( self, subjectName, command, viewOnly )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 75 )

      ########################### processing subsampling rate for rendering ####
      randomSelectionFactor = ( 100.0 * float( storingIncrement ) ) / \
                              ( float( voxelSamplerPointCount ) * 10.0 )
      if ( randomSelectionFactor > 100.0 ):
      
        randomSelectionFactor = 100.0

      ########################### building bundle map for rendering ############
      fileNameBundleMapForRendering = ''

      if ( randomSelectionFactor < 100.0 ):

        fileNameBundleMapForRendering = fileNameBundleMap + '_for_rendering'

        if ( stepCount == 1 ):

          command = 'GkgExecuteCommand DwiBundleOperator ' + \
                    ' -i ' + fileNameBundleMap + \
                    ' -o ' + fileNameBundleMapForRendering + \
                    ' -op random-selection ' + \
                    ' -scalarParameters ' + str( randomSelectionFactor ) + \
                    ' -of ' + 'aimsbundlemap' + \
                    ' -verbose true'
          executeCommand( self, subjectName, command, viewOnly )

        else:

          outputFileNameBundleMaps = ''
          for step in range( 0, stepCount ):
      
            inputFileNameBundleMap = fileNameBundleMap + '_' + \
                                    str( step + 1 ) + '_' + \
                                    str( stepCount )
            outputFileNameBundleMap = inputFileNameBundleMap + \
                                      '_random_selection'
            outputFileNameBundleMaps += outputFileNameBundleMap + ' '
          
            command = 'GkgExecuteCommand DwiBundleOperator ' + \
                      ' -i ' + inputFileNameBundleMap + \
                      ' -o ' + outputFileNameBundleMap + \
                      ' -op random-selection ' + \
                      ' -scalarParameters ' + str( randomSelectionFactor ) + \
                      ' -of ' + 'aimsbundlemap' + \
                      ' -verbose true'
            executeCommand( self, subjectName, command, viewOnly )

          command = 'GkgExecuteCommand DwiBundleOperator ' + \
                    ' -i ' + outputFileNameBundleMaps + \
                    ' -o ' + fileNameBundleMapForRendering + \
                    ' -op fusion' + \
                    ' -of ' + 'aimsbundlemap' + \
                    ' -verbose true'
          executeCommand( self, subjectName, command, viewOnly )
            
      else:

        if ( stepCount == 1 ):

          fileNameBundleMapForRendering = fileNameBundleMap

        else:

          fileNameBundleMaps = ''
          for step in range( 0, stepCount ):
      
            fileNameBundleMaps += fileNameBundleMap + '_' + \
                                  str( step + 1 ) + '_' + \
                                  str( stepCount ) + ' '
          
          fileNameBundleMapForRendering = fileNameBundleMap + '_for_rendering'
          command = 'GkgExecuteCommand DwiBundleOperator ' + \
                    ' -i ' + fileNameBundleMaps + \
                    ' -o ' + fileNameBundleMapForRendering + \
                    ' -op fusion' + \
                    ' -of ' + 'aimsbundlemap' + \
                    ' -verbose true'
          executeCommand( self, subjectName, command, viewOnly )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 80 )

      ########################### converting to bundle map format ##############

      if ( bundleMapFormat != 'aimsbundlemap' ):
      
        if ( stepCount == 1 ):

          fileNameBundleMapWithAimsBundleMapExt = fileNameBundleMap + '.bundles'

          command = 'GkgExecuteCommand DwiBundleOperator ' + \
                    ' -i ' + fileNameBundleMapWithAimsBundleMapExt + \
                    ' -o ' + fileNameBundleMap + \
                    ' -op fusion' + \
                    ' -of ' + bundleMapFormat + \
                    ' -verbose true'
          executeCommand( self, subjectName, command, viewOnly )
            
        else:
        
          for step in range( 0, stepCount ):

            inputFileNameBundleMap = fileNameBundleMap + '_' + \
                                    str( step + 1 ) + '_' + \
                                    str( stepCount )
            inputFileNameBundleMapWithAimsBundleMapExt = \
                                    inputFileNameBundleMap + '.bundles'

            command = 'GkgExecuteCommand DwiBundleOperator ' + \
                      ' -i ' + inputFileNameBundleMapWithAimsBundleMapExt + \
                      ' -o ' + inputFileNameBundleMap + \
                      ' -op fusion' + \
                      ' -of ' + bundleMapFormat + \
                      ' -verbose true'
            executeCommand( self, subjectName, command, viewOnly )


      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 90 )

      ####################### notifying display thread #########################
      functors[ 'condition-notify-and-release' ]( subjectName,
                                                  'tractogram-processed' )

      ########################### updating progress bar ########################
      functors[ 'update-progress-bar' ]( subjectName, 100 )

    except:

      self._functors[ 'task-set-status-as-failed' ]( subjectName,
                                                     sys.exc_info() )

  ##############################################################################
  # display()
  ##############################################################################

  def display( self, functors, parameters, subjectName, verbose,
               isReportModeActive ):

    ########################### resetting the viewer ###########################
    functors[ 'viewer-reset' ]()

    ########################### collecting file names ##########################
    outputWorkDirectory = parameters[ 'outputWorkDirectory' ].getValue()

    fileNameOdfTextureMap = parameters[ 'fileNameOdfTextureMap' ].getValue()
    fileNameOdfSiteMap = parameters[ 'fileNameOdfSiteMap' ].getValue()
    fileNameRgb = parameters[ 'fileNameRgb' ].getValue()

    fileNameT1 = parameters[ 'fileNameT1' ].getValue()
    fileNameTransformationDwToT1 = \
                        parameters[ 'fileNameTransformationDwToT1' ].getValue()

    fileNameMask = parameters[ 'fileNameMask' ].getValue()
    fileNameTransformationMaskToDw = \
                       parameters[ 'fileNameTransformationMaskToDw' ].getValue()

    ############## setting options according to tractography type ##############
    tractographyType = parameters[ 'trackingType' ].getChoice()
    stepCount = parameters[ 'stepCount' ].getValue()
    voxelSamplerPointCount = 1
    storingIncrement = 1
    
    tractographyName = ''
    if ( tractographyType == 'streamline deterministic' ):

      tractographyName = 'streamline-deterministic'
      voxelSamplerPointCount = \
                 parameters[ 'deterministicVoxelSamplerPointCount' ].getValue()
      storingIncrement = \
                       parameters[ 'deterministicStoringIncrement' ].getValue()

    elif ( tractographyType == 'streamline regularized deterministic' ):

      tractographyName = 'streamline-regularized-deterministic'
      voxelSamplerPointCount = \
       parameters[ 'regularizedDeterministicVoxelSamplerPointCount' ].getValue()
      storingIncrement = \
       parameters[ 'regularizedDeterministicStoringIncrement' ].getValue()

    elif ( tractographyType == 'streamline probabilistic' ):

      tractographyName = 'streamline-probabilistic'
      voxelSamplerPointCount = \
                 parameters[ 'probabilisticVoxelSamplerPointCount' ].getValue()
      storingIncrement = \
                       parameters[ 'probabilisticStoringIncrement' ].getValue()

    fileNameBundleMap = os.path.join( outputWorkDirectory,
                                      'tractography-' + tractographyName )
    bundleMapFormat = parameters[ 'bundleMapFormat' ].getChoice()

    ########################### processing subsampling rate for rendering ######
    randomSelectionFactor = ( 100.0 * float( storingIncrement ) ) / \
                            ( float( voxelSamplerPointCount ) * 10.0 )
    if ( randomSelectionFactor > 100.0 ):
    
      randomSelectionFactor = 100.0

    ########################### building bundle map for rendering ##############
    fileNameBundleMapForRendering = ''

    if ( randomSelectionFactor < 100.0 ):

      fileNameBundleMapForRendering = fileNameBundleMap + '_for_rendering'
          
    else:

      if ( stepCount == 1 ):

        fileNameBundleMapForRendering = fileNameBundleMap

      else:

        fileNameBundleMapForRendering = fileNameBundleMap + '_for_rendering'

                                      
    ########################### loading RGB, T1, and mask ######################

    functors[ 'viewer-create-referential' ]( 'frameDW' )
    functors[ 'viewer-create-referential' ]( 'frameT1' )
    functors[ 'viewer-create-referential' ]( 'frameMask' )

    functors[ 'viewer-load-object' ]( fileNameMask, 'mask' )
    functors[ 'viewer-set-colormap' ]( 'mask', 'BLUE-ufusion' )

    if ( len( fileNameTransformationMaskToDw ) ):
    
      functors[ 'viewer-load-transformation' ]( fileNameTransformationMaskToDw,
                                                'frameMask', 'frameDW' )
    
      functors[ 'viewer-assign-referential-to-object' ]( 'frameMask', 'mask' )
      
    else:

      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'mask' )

    if ( len( fileNameRgb ) ):

      # loading RGB
      functors[ 'viewer-load-object' ]( fileNameRgb, 'RGB' )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'RGB' )

    if ( len( fileNameT1 ) ):
    
      # loading T1
      functors[ 'viewer-load-object' ]( fileNameT1, 'T1' )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameT1', 'T1' )

      if ( len( fileNameTransformationDwToT1 ) ):
      

        functors[ 'viewer-load-transformation' ]( fileNameTransformationDwToT1,
                                                  'frameDW', 'frameT1' )

      functors[ 'viewer-assign-referential-to-window' ]( 'frameT1',
                                                         'first_view',
                                                         'T1 + RGB (axial)' )
      functors[ 'viewer-assign-referential-to-window' ]( 'frameT1',
                                                         'first_view',
                                                         'T1 + RGB (sagittal)' )
      functors[ 'viewer-assign-referential-to-window' ]( 'frameT1',
                                                         'first_view',
                                                         'T1 + RGB (coronal)' )

    if ( len( fileNameT1 ) and len( fileNameRgb ) ):

      # T1 and RGB fusion
      functors[ 'viewer-fusion-objects' ]( [ 'T1', 'RGB'],
                                           'fusionT1AndRGB',
                                           'Fusion2DMethod' )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameT1',
                                                         'fusionT1AndRGB' )

    elif ( len( fileNameT1 ) ):

      functors[ 'viewer-load-object' ]( fileNameT1, 'fusionT1AndRGB' )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameT1',
                                                         'fusionT1AndRGB' )

    if ( len( fileNameT1 ) ):
                                              
      functors[ 'viewer-add-object-to-window' ]( 'fusionT1AndRGB',
                                                 'first_view',
                                                 'T1 + RGB (axial)' )
      functors[ 'viewer-add-object-to-window' ]( 'fusionT1AndRGB',
                                                 'first_view',
                                                 'T1 + RGB (sagittal)' )
      functors[ 'viewer-add-object-to-window' ]( 'fusionT1AndRGB',
                                                 'first_view',
                                                 'T1 + RGB (coronal)' )

    elif( len( fileNameRgb ) ):

      functors[ 'viewer-assign-referential-to-window' ]( 'frameDW',
                                                         'first_view',
                                                         'T1 + RGB' )
      functors[ 'viewer-add-object-to-window' ]( 'RGB',
                                                 'first_view',
                                                 'T1 + RGB (axial)' )
      functors[ 'viewer-add-object-to-window' ]( 'RGB',
                                                 'first_view',
                                                 'T1 + RGB (sagittal)' )
      functors[ 'viewer-add-object-to-window' ]( 'RGB',
                                                 'first_view',
                                                 'T1 + RGB (coronal)' )

    if ( len( fileNameT1 ) and len( fileNameMask ) ):

      # T1 and mask fusion
      functors[ 'viewer-fusion-objects' ]( [ 'T1', 'mask'],
                                           'fusionT1AndMask',
                                           'Fusion2DMethod' )
      functors[ 'viewer-assign-referential-to-object' ]( 'frameT1',
                                                         'fusionT1AndMask' )

      functors[ 'viewer-assign-referential-to-window' ]( 'frameT1',
                                                         'first_view',
                                                         'T1 + mask (axial)' )
      functors[ 'viewer-add-object-to-window' ]( 'fusionT1AndMask',
                                                 'first_view',
                                                 'T1 + mask (axial)' )

      functors[ 'viewer-assign-referential-to-window' ]( 'frameT1',
                                                         'first_view',
                                                         'T1 + mask (sagittal)')
      functors[ 'viewer-add-object-to-window' ]( 'fusionT1AndMask',
                                                 'first_view',
                                                 'T1 + mask (sagittal)' )

      functors[ 'viewer-assign-referential-to-window' ]( 'frameT1',
                                                         'first_view',
                                                         'T1 + mask (coronal)' )
      functors[ 'viewer-add-object-to-window' ]( 'fusionT1AndMask',
                                                 'first_view',
                                                 'T1 + mask (coronal)' )

    elif ( len( fileNameMask ) ):

      # mask
      functors[ 'viewer-assign-referential-to-window' ]( 'frameMask',
                                                         'first_view',
                                                         'T1 + mask (axial)' )
      functors[ 'viewer-add-object-to-window' ]( 'mask',
                                                 'first_view',
                                                 'T1 + mask (axial)' )

      functors[ 'viewer-assign-referential-to-window' ]( 'frameMask',
                                                         'first_view',
                                                         'T1 + mask (sagittal)')
      functors[ 'viewer-add-object-to-window' ]( 'mask',
                                                 'first_view',
                                                 'T1 + mask (sagittal)' )

      functors[ 'viewer-assign-referential-to-window' ]( 'frameMask',
                                                         'first_view',
                                                         'T1 + mask (coronal)' )
      functors[ 'viewer-add-object-to-window' ]( 'mask',
                                                 'first_view',
                                                 'T1 + mask (coronal)' )

    ####################### waiting for result ################################
    functors[ 'condition-wait-and-release' ]( subjectName,
                                              'tractogram-processed' )

    ########################### displaying tractogram ##########################
    fileNameBundleMapForRenderingWithAimsBundleMapExt = \
                                      fileNameBundleMapForRendering + '.bundles'

    functors[ 'viewer-load-object' ]( \
                              fileNameBundleMapForRenderingWithAimsBundleMapExt,
                              'tractogram' )
    functors[ 'viewer-set-diffuse' ]( 'tractogram', [ 0.40, 0.80, 1.0, 0.2 ] )
    functors[ 'viewer-assign-referential-to-object' ]( 'frameDW', 'tractogram' )
    
    if ( len( fileNameT1 ) ):
    
      functors[ 'viewer-assign-referential-to-window' ](
                                                       'frameT1',
                                                       'first_view',
                                                       'T1 + RGB + tractogram' )
      
    else:

      functors[ 'viewer-assign-referential-to-window' ](
                                                       'frameDW',
                                                       'first_view',
                                                       'T1 + RGB + tractogram' )

    functors[ 'viewer-add-object-to-window' ]( 'tractogram',
                                               'first_view',
                                               'T1 + RGB + tractogram' )

    functors[ 'viewer-adjust-camera']( 'first_view',
                                       'T1 + RGB + tractogram' )
                                      

