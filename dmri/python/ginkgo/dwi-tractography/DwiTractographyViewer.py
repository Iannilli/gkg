from Viewer import *


class DwiTractographyViewer( Viewer ):

  def __init__( self, minimumSize, parent ):

    Viewer.__init__( self, minimumSize, parent )
    
    ########################## First View ######################################

    self.createView( 'first_view' )
    
    self.addAxialWindow( 'first_view', 0, 0, 1, 1,
                         'T1 + RGB (axial)', 'T1 + RGB (axial)',
                         [ 0.0, 0.0, 0.0, 1.0 ], False )
    self.addCoronalWindow( 'first_view', 1, 0, 1, 1,
                         'T1 + RGB (sagittal)', 'T1 + RGB (sagittal)',
                         [ 0.0, 0.0, 0.0, 1.0 ], False )
    self.addSagittalWindow( 'first_view', 2, 0, 1, 1,
                         'T1 + RGB (coronal)', 'T1 + RGB (coronal)',
                         [ 0.0, 0.0, 0.0, 1.0 ], False )

    self.addAxialWindow( 'first_view', 0, 1, 1, 1,
                         'T1 + mask (axial)', 'T1 + mask (axial)',
                         [ 0.0, 0.0, 0.0, 1.0 ], False )
    self.addSagittalWindow( 'first_view', 0, 2, 1, 1,
                            'T1 + mask (sagittal)', 'T1 + mask (sagittal)',
                            [ 0.0, 0.0, 0.0, 1.0 ], False )
    self.addCoronalWindow( 'first_view', 0, 3, 1, 1,
                           'T1 + mask (coronal)', 'T1 + mask (coronal)',
                           [ 0.0, 0.0, 0.0, 1.0 ], False )

  
    self.add3DWindow( 'first_view', 1, 1, 3, 3,
                      'T1 + RGB + tractogram', 'T1 + RGB + tractogram',
                      backgroundColor = [ 0, 0, 0, 1 ],
                      isMuteToolButtonVisible = True,
                      isColorMapToolButtonVisible = True,
                      isSnapshotToolButtonVisible = True,
                      isZoomToolButtonVisible = True,
                      isClippingSpinBoxVisible = True,
                      clippingOptions = [ 2, 0.01, 1000.0, 2.0, 0.1, 2 ],
                      object2DName = 'fusionT1AndRGB' )

  def pickCallBack( self, event ):

    if isinstance( event.artist, Line2D ):
  
      xdata = event.artist.get_xdata()
      ydata = event.artist.get_ydata()
      index = event.ind
      print 'onpick1 line:', xdata[ index[ 0 ] ], ', ', ydata[ index[ 0 ] ]



def createDwiTractographyViewer( minimumSize, parent ):

  return DwiTractographyViewer( minimumSize, parent )

