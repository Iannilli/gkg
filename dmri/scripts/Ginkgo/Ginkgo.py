import os
import sys
from PyQt5 import QtCore, QtGui, QtWidgets, uic
from distutils.spawn import find_executable


#################################################################################
# BRAINVISA & ANATOMIST
################################################################################

print '========================================================================'
print 'BRAINVISA & ANATOMIST --------------------------------------------------'
anatomistExecutable = find_executable( 'anatomist' )
print 'ANATOMIST :', anatomistExecutable

brainvisaPath = os.path.dirname( os.path.dirname( anatomistExecutable ) )
os.environ[ 'BRAINVISA_SHARE' ] = os.path.join( brainvisaPath, 'share' )

pyBrainvisaPath = os.path.join( brainvisaPath, 'python' )

sys.path.insert( 0, pyBrainvisaPath )

print 'BRAINVISA_SHARE :', os.environ[ 'BRAINVISA_SHARE' ]
print 'PYANATOMIST_PATH :', pyBrainvisaPath


################################################################################
# PATH & LD_LIBRARY_PATH
################################################################################

print 'PATH & LD_LIBRARY_PATH -------------------------------------------------'

os.environ[ 'PATH' ] = os.path.join( brainvisaPath, 'bin' ) + ':' + \
                       os.environ[ 'PATH' ]
os.environ[ 'PATH' ] = os.path.join( os.sep, 'usr', 'bin' ) + ':' + \
                       os.environ[ 'PATH' ]

os.environ[ 'LD_LIBRARY_PATH' ] = os.path.join( brainvisaPath, 'lib' ) + ':' + \
                       os.environ[ 'LD_LIBRARY_PATH' ]
os.environ[ 'LD_LIBRARY_PATH' ] = os.path.join( os.sep, 'usr', 'lib' ) + ':' + \
                       os.environ[ 'LD_LIBRARY_PATH' ]

print 'PATH :', os.environ[ 'PATH' ]
print 'LD_LIBRARY_PATH :', os.environ[ 'LD_LIBRARY_PATH' ]


################################################################################
# GKG & PYGKG
################################################################################

print 'GKG & PYGKG ------------------------------------------------------------'
gkgSharePath = os.path.join( os.sep, 'usr', 'share', 'gkg' )

pyGkgPath = os.path.join( gkgSharePath, 'python', 'pygkg' )
pyGinkgoPath = os.path.join( gkgSharePath, 'python', 'ginkgo' )

sys.path.insert( 0, os.path.join( os.sep, 'usr', 'python' ) )
sys.path.insert( 0, pyGkgPath )
sys.path.insert( 0, pyGinkgoPath )
sys.path.insert( 0, os.path.join( pyGinkgoPath,
                                  'dwi-data-import-and-qspace-sampling' ) )
sys.path.insert( 0, os.path.join( pyGinkgoPath,
                                  'dwi-to-anatomy-matching' ) )
sys.path.insert( 0, os.path.join( pyGinkgoPath,
                                  'dwi-rough-mask-extraction' ) )
sys.path.insert( 0, os.path.join( pyGinkgoPath,
                                  'dwi-outlier-detection' ) )
sys.path.insert( 0, os.path.join( pyGinkgoPath,
                                  'dwi-susceptibility-artifact-correction' ) )
sys.path.insert( 0, os.path.join( pyGinkgoPath,
                                  'dwi-eddy-current-and-motion-correction' ) )
sys.path.insert( 0, os.path.join( pyGinkgoPath,
                                  'dwi-quality-check-reporting' ) )
sys.path.insert( 0, os.path.join( pyGinkgoPath,
                                  'dwi-local-modeling' ) )
sys.path.insert( 0, os.path.join( pyGinkgoPath,
                                  'dwi-tractography-mask' ) )
sys.path.insert( 0, os.path.join( pyGinkgoPath,
                                  'dwi-tractography' ) )
sys.path.insert( 0, os.path.join( pyGinkgoPath,
                                  'dwi-bundle-filtering' ) )
sys.path.insert( 0, os.path.join( pyGinkgoPath,
                                  'dwi-bundle-statistics' ) )
sys.path.insert( 0, os.path.join( pyGinkgoPath,
                                  'dwi-connectivity-matrix' ) )
sys.path.insert( 0, os.path.join( pyGinkgoPath,
                                  'dwi-intrasubject-fiber-clustering' ) )
sys.path.insert( 0, os.path.join( pyGinkgoPath,
                                  'dwi-intersubject-fiber-clustering' ) )
sys.path.insert( 0, os.path.join( pyGinkgoPath,
                                  'dwi-long-white-matter-bundle-labeling' ) )
sys.path.insert( 0, os.path.join( pyGinkgoPath,
                                  'dwi-short-white-matter-bundle-labeling' ) )
sys.path.insert( 0, os.path.join( pyGinkgoPath,
                                  'dwi-data-convertion-to-dtitk' ) )
sys.path.insert( 0, os.path.join( pyGinkgoPath,
                                  'dwi-dti-template-construction' ) )
sys.path.insert( 0, os.path.join( pyGinkgoPath,
                                  'dwi-subject-registration-to-template' ) )
sys.path.insert( 0, os.path.join( pyGinkgoPath,
                                  'dwi-subject-resampling-to-template' ) )
sys.path.insert( 0, os.path.join( pyGinkgoPath,
                                  'dwi-atlas-creation' ) )
sys.path.insert( 0, os.path.join( pyGinkgoPath,
                                  'dwi-poster' ) )
sys.path.insert( 0, os.path.join( pyGinkgoPath,
                                  'dwi-track-density-imaging' ) )
sys.path.insert( 0, os.path.join( pyGinkgoPath,
                                  'dwi-geometry-simulator' ) )
sys.path.insert( 0, os.path.join( pyGinkgoPath,
                                  'dwi-montecarlo-simulator' ) )
sys.path.insert( 0, os.path.join( pyGinkgoPath,
                                  'dwi-mri-synthesis' ) )
sys.path.insert( 0, os.path.join( pyGinkgoPath,
				  'dwi-block-data-import-and-qspace-sampling' ) )
sys.path.insert( 0, os.path.join( pyGinkgoPath,
				  'dwi-block-filtering' ) )
sys.path.insert( 0, os.path.join( pyGinkgoPath,
				  'dwi-block-dti-modeling' ) )
sys.path.insert( 0, os.path.join( pyGinkgoPath,
                                  'dwi-block-pdf-field' ) )
sys.path.insert( 0, os.path.join( pyGinkgoPath,
				  'dwi-block-rough-mask-extraction' ) )
sys.path.insert( 0, os.path.join( pyGinkgoPath,
				  'dwi-block-t2-multiple-registration' ) )
                                  
print 'GKG_SHARE_PATH :', gkgSharePath

                     
################################################################################
# DCMTK
################################################################################

print 'DCMTK ------------------------------------------------------------------'
dcmDictPath = os.path.join( gkgSharePath, 'dicom' )
os.environ[ 'DCMDICTPATH' ] = \
              os.path.join( dcmDictPath, 'siemens-private-dicom.dic' ) + ':' + \
              os.path.join( dcmDictPath, 'gehc-private-dicom.dic' ) + ':' + \
              os.path.join( dcmDictPath, 'public-dicom.dic' )
              
print 'DCMDICTPATH :', os.environ[ 'DCMDICTPATH' ]


################################################################################
# DTI-TK
################################################################################

print 'DTI-TK -----------------------------------------------------------------'
TVtoolExecutable = find_executable('TVtool')
dtitkRoot = os.path.dirname( os.path.dirname( TVtoolExecutable ) )
os.environ[ 'DTITK_ROOT' ] = dtitkRoot
                     
print 'DTITK_ROOT :', os.environ[ 'DTITK_ROOT' ]


print '========================================================================'

################################################################################
# importation of modules
################################################################################

from OptionParser import *
from StyleSheet import *
from GinkgoToolBox import *
from GinkgoPages import *



################################################################################
# main
################################################################################

pages = getPages()
batchMode, parameterFileName, distributionSettings = getArgs( pages )

# in case of batch mode
if ( batchMode == True ):

  ginkgoToolBox = GinkgoToolBox( pages,
                                 True,
                                 distributionSettings,
                                 verbose = True )
  ginkgoToolBox.launch( parameterFileName, True )

# in case of graphical user interface mode
else:

  runqt = True
  if QtWidgets.qApp.startingUp():

   qapp = QtWidgets.QApplication( sys.argv )

   #set custom style
   qapp.setStyleSheet( getPyGkgStyleSheet() )

  else:

    runqt = False

  ginkgoToolBox = GinkgoToolBox( pages,
                                 False,
                                 distributionSettings,
                                 verbose = True )
  ginkgoToolBox.launch( parameterFileName, False )
  ginkgoToolBox.hideSplashScreen()
  
  if runqt:
  
    qapp.exec_()
