specySettings = {
  '_subjectName': {'defaultValue' : '' }, 
  'fileNameBundleMaps': {'defaultValue' : '' }, 
  'outputWorkDirectory': {'defaultValue' : '' }, 
  'viewType': {'defaultValue' : 0,
               'choices' : ['Simple View',
                            'Multiple View'] }, 
  'processes': []}
