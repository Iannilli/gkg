specySettings = {
  'fileNameTractographyMaskToDijTransformation': {'defaultValue' : '' }, 
  'outputWorkDirectory': {'defaultValue' : '' }, 
  'fileNameRoughMask': {'defaultValue' : '' }, 
  'fileNameDij': {'defaultValue' : '' }, 
  '_subjectName': {'defaultValue' : '' }, 
  'removeTemporaryFiles': {'defaultValue' : 2 }, 
  'fileNameTractographyMask': {'defaultValue' : '' }}
