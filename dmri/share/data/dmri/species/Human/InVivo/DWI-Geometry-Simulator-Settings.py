specySettings = {
  'cylinderNetworkAxis2X': {'defaultValue' : 0.0,
                            'lowerValue' : -10.0,
                            'upperValue' : 10.0,
                            'incrementValue' : 0.1 }, 
  'cylinderNetworkAxis2Y': {'defaultValue' : 1.0,
                            'lowerValue' : -10.0,
                            'upperValue' : 10.0,
                            'incrementValue' : 0.1 }, 
  'cylinderNetworkAxis2Z': {'defaultValue' : 0.0,
                            'lowerValue' : -10.0,
                            'upperValue' : 10.0,
                            'incrementValue' : 0.1 }, 
  'fiberPointsZ': {'defaultValue' : 0.0,
                   'lowerValue' : -1000.0,
                   'upperValue' : 1000.0,
                   'incrementValue' : 1.0 }, 
  'starNetworkAxis1Z': {'defaultValue' : 0.0,
                        'lowerValue' : -1.0,
                        'upperValue' : 1.0,
                        'incrementValue' : 0.1 }, 
  'starNetworkAxis1Y': {'defaultValue' : 0.0,
                        'lowerValue' : -1.0,
                        'upperValue' : 1.0,
                        'incrementValue' : 0.1 }, 
  'fiberPointsX': {'defaultValue' : 0.0,
                   'lowerValue' : -1000.0,
                   'upperValue' : 1000.0,
                   'incrementValue' : 1.0 }, 
  'ellipsoidEigenVector1Y': {'defaultValue' : 0.0,
                             'lowerValue' : -1.0,
                             'upperValue' : 1.0,
                             'incrementValue' : 0.1 }, 
  'boxNetworkSpacing1': {'defaultValue' : 20.0,
                         'lowerValue' : 0.0,
                         'upperValue' : 1000.0,
                         'incrementValue' : 0.1 }, 
  'boxNetworkSpacing2': {'defaultValue' : 20.0,
                         'lowerValue' : 0.0,
                         'upperValue' : 1000.0,
                         'incrementValue' : 0.1 }, 
  'boxNetworkSpacing3': {'defaultValue' : 20.0,
                         'lowerValue' : 0.0,
                         'upperValue' : 1000.0,
                         'incrementValue' : 0.1 }, 
  'fiberNetworkAxis2Z': {'defaultValue' : 0.0,
                         'lowerValue' : -1.0,
                         'upperValue' : 1.0,
                         'incrementValue' : 0.1 }, 
  'fiberNetworkAxis2Y': {'defaultValue' : 1.0,
                         'lowerValue' : -1.0,
                         'upperValue' : 1.0,
                         'incrementValue' : 0.1 }, 
  'fiberNetworkAxis2X': {'defaultValue' : 0.0,
                         'lowerValue' : -1.0,
                         'upperValue' : 1.0,
                         'incrementValue' : 0.1 }, 
  'randomRotationOfFiber': {'defaultValue' : 0 }, 
  'randomRotationOfEllipsoid': {'defaultValue' : 0 }, 
  'fiberInputFileChoice': {'defaultValue' : 0 }, 
  'cylinderClosedShape': {'defaultValue' : 0 }, 
  'ellipsoidEigenValue1': {'defaultValue' : 5.0,
                           'lowerValue' : 1.0,
                           'upperValue' : 100.0,
                           'incrementValue' : 1.0 }, 
  'cylinderFacetCount': {'defaultValue' : 36,
                         'lowerValue' : 4,
                         'upperValue' : 1000,
                         'incrementValue' : 2 }, 
  'ellipsoidEigenValue3': {'defaultValue' : 1.0,
                           'lowerValue' : 1.0,
                           'upperValue' : 100.0,
                           'incrementValue' : 1.0 }, 
  'randomRotationOfBoxes': {'defaultValue' : 0 }, 
  'starNetworkAxis1X': {'defaultValue' : 1.0,
                        'lowerValue' : -1.0,
                        'upperValue' : 1.0,
                        'incrementValue' : 0.1 }, 
  'boxLowerPointZ': {'defaultValue' : -5.0,
                     'lowerValue' : -1000.0,
                     'upperValue' : 1000.0,
                     'incrementValue' : 1.0 }, 
  'boxLowerPointX': {'defaultValue' : -5.0,
                     'lowerValue' : -1000.0,
                     'upperValue' : 1000.0,
                     'incrementValue' : 1.0 }, 
  'boxLowerPointY': {'defaultValue' : -5.0,
                     'lowerValue' : -1000.0,
                     'upperValue' : 1000.0,
                     'incrementValue' : 1.0 }, 
  'ellipsoidNetworkAxis1Y': {'defaultValue' : 0.0,
                             'lowerValue' : -1.0,
                             'upperValue' : 1.0,
                             'incrementValue' : 0.1 }, 
  'randomRotationOfStars': {'defaultValue' : 0 }, 
  'ellipsoidEigenVector1X': {'defaultValue' : 1.0,
                             'lowerValue' : -1.0,
                             'upperValue' : 1.0,
                             'incrementValue' : 0.1 }, 
  'sphereNetworkCount2': {'defaultValue' : 10,
                          'lowerValue' : 1,
                          'upperValue' : 1000,
                          'incrementValue' : 1 }, 
  'sphereNetworkCount1': {'defaultValue' : 10,
                          'lowerValue' : 1,
                          'upperValue' : 1000,
                          'incrementValue' : 1 }, 
  'ellipsoidNetworkAxis1X': {'defaultValue' : 1.0,
                             'lowerValue' : -1.0,
                             'upperValue' : 1.0,
                             'incrementValue' : 0.1 }, 
  'starNetworkAxis2Z': {'defaultValue' : 0.0,
                        'lowerValue' : -1.0,
                        'upperValue' : 1.0,
                        'incrementValue' : 0.1 }, 
  'starNetworkAxis2X': {'defaultValue' : 0.0,
                        'lowerValue' : -1.0,
                        'upperValue' : 1.0,
                        'incrementValue' : 0.1 }, 
  'starNetworkAxis2Y': {'defaultValue' : 1.0,
                        'lowerValue' : -1.0,
                        'upperValue' : 1.0,
                        'incrementValue' : 0.1 }, 
  'ellipsoidCenterX': {'defaultValue' : 0.0,
                       'lowerValue' : -1000.0,
                       'upperValue' : 1000.0,
                       'incrementValue' : 1.0 }, 
  'ellipsoidCenterY': {'defaultValue' : 0.0,
                       'lowerValue' : -1000.0,
                       'upperValue' : 1000.0,
                       'incrementValue' : 1.0 }, 
  'ellipsoidCenterZ': {'defaultValue' : 0.0,
                       'lowerValue' : -1000.0,
                       'upperValue' : 1000.0,
                       'incrementValue' : 1.0 }, 
  'starNetwork': {'defaultValue' : 0 }, 
  'sphereRadius': {'defaultValue' : 2.5,
                   'lowerValue' : 1.0,
                   'upperValue' : 100.0,
                   'incrementValue' : 1.0 }, 
  'fiberPointsY': {'defaultValue' : 0.0,
                   'lowerValue' : -1000.0,
                   'upperValue' : 1000.0,
                   'incrementValue' : 1.0 }, 
  'boxNetworkAxis3Y': {'defaultValue' : 0.0,
                       'lowerValue' : -1.0,
                       'upperValue' : 1.0,
                       'incrementValue' : 0.1 }, 
  'boxNetworkAxis3X': {'defaultValue' : 0.0,
                       'lowerValue' : -1.0,
                       'upperValue' : 1.0,
                       'incrementValue' : 0.1 }, 
  'boxNetworkAxis3Z': {'defaultValue' : 1.0,
                       'lowerValue' : -1.0,
                       'upperValue' : 1.0,
                       'incrementValue' : 0.1 }, 
  'sphereCenterY': {'defaultValue' : 0.0,
                    'lowerValue' : -1000.0,
                    'upperValue' : 1000.0,
                    'incrementValue' : 1.0 }, 
  'sphereCenterX': {'defaultValue' : 0.0,
                    'lowerValue' : -1000.0,
                    'upperValue' : 1000.0,
                    'incrementValue' : 1.0 }, 
  'sphereCenterZ': {'defaultValue' : 0.0,
                    'lowerValue' : -1000.0,
                    'upperValue' : 1000.0,
                    'incrementValue' : 1.0 }, 
  'starPeakMagnitudes': {'defaultValue' : '10 10 10 10 10 10 10 10 10 10 10 10' }, 
  'ellipsoidEigenVector1Z': {'defaultValue' : 0.0,
                             'lowerValue' : -1.0,
                             'upperValue' : 1.0,
                             'incrementValue' : 0.1 }, 
  'cylinderCenterX': {'defaultValue' : 0.0,
                      'lowerValue' : -1000.0,
                      'upperValue' : 1000.0,
                      'incrementValue' : 1.0 }, 
  'cylinderCenterY': {'defaultValue' : 0.0,
                      'lowerValue' : -1000.0,
                      'upperValue' : 1000.0,
                      'incrementValue' : 1.0 }, 
  'cylinderCenterZ': {'defaultValue' : 0.0,
                      'lowerValue' : -1000.0,
                      'upperValue' : 1000.0,
                      'incrementValue' : 1.0 }, 
  'sphereNetworkSpacing3': {'defaultValue' : 6.0,
                            'lowerValue' : 0.0,
                            'upperValue' : 1000.0,
                            'incrementValue' : 0.1 }, 
  'sphereNetworkSpacing2': {'defaultValue' : 6.0,
                            'lowerValue' : 0.0,
                            'upperValue' : 1000.0,
                            'incrementValue' : 0.1 }, 
  'sphereNetworkSpacing1': {'defaultValue' : 6.0,
                            'lowerValue' : 0.0,
                            'upperValue' : 1000.0,
                            'incrementValue' : 0.1 }, 
  'ellipsoidNetworkAxis3Z': {'defaultValue' : 1.0,
                             'lowerValue' : -1.0,
                             'upperValue' : 1.0,
                             'incrementValue' : 0.1 }, 
  'ellipsoidEigenVector3Y': {'defaultValue' : 0.0,
                             'lowerValue' : -1.0,
                             'upperValue' : 1.0,
                             'incrementValue' : 0.1 }, 
  'ellipsoidEigenVector3X': {'defaultValue' : 0.0,
                             'lowerValue' : -1.0,
                             'upperValue' : 1.0,
                             'incrementValue' : 0.1 }, 
  'starSharpPeak': {'defaultValue' : 0 }, 
  'ellipsoidNetwork': {'defaultValue' : 0 }, 
  'ellipsoidEigenVector2X': {'defaultValue' : 0.0,
                             'lowerValue' : -1.0,
                             'upperValue' : 1.0,
                             'incrementValue' : 0.1 }, 
  'starNetworkSpacing2': {'defaultValue' : 20.0,
                          'lowerValue' : 0.0,
                          'upperValue' : 1000.0,
                          'incrementValue' : 0.1 }, 
  'ellipsoidNetworkAxis2Y': {'defaultValue' : 1.0,
                             'lowerValue' : -1.0,
                             'upperValue' : 1.0,
                             'incrementValue' : 0.1 }, 
  'ellipsoidVextexCount': {'defaultValue' : 100,
                           'lowerValue' : 4,
                           'upperValue' : 2000,
                           'incrementValue' : 2 }, 
  'starCenterY': {'defaultValue' : 0.0,
                  'lowerValue' : -1000.0,
                  'upperValue' : 1000.0,
                  'incrementValue' : 1.0 }, 
  'sphereNetworkAxis1X': {'defaultValue' : 1.0,
                          'lowerValue' : -1.0,
                          'upperValue' : 1.0,
                          'incrementValue' : 0.1 }, 
  'ellipsoidEigenVector2Z': {'defaultValue' : 0.0,
                             'lowerValue' : -1.0,
                             'upperValue' : 1.0,
                             'incrementValue' : 0.1 }, 
  'sphereNetworkAxis1Z': {'defaultValue' : 0.0,
                          'lowerValue' : -1.0,
                          'upperValue' : 1.0,
                          'incrementValue' : 0.1 }, 
  'ellipsoidNetworkCount2': {'defaultValue' : 10,
                             'lowerValue' : 1,
                             'upperValue' : 1000,
                             'incrementValue' : 1 }, 
  'ellipsoidNetworkCount3': {'defaultValue' : 10,
                             'lowerValue' : 1,
                             'upperValue' : 1000,
                             'incrementValue' : 1 }, 
  'ellipsoidNetworkCount1': {'defaultValue' : 10,
                             'lowerValue' : 1,
                             'upperValue' : 1000,
                             'incrementValue' : 1 }, 
  'sphereVextexCount': {'defaultValue' : 100,
                        'lowerValue' : 4,
                        'upperValue' : 1000,
                        'incrementValue' : 2 }, 
  'fiberInputFile': {'defaultValue' : '' }, 
  'SelectedElementaryGeometries': [], 
  'cylinderNetwork': {'defaultValue' : 0 }, 
  'ellipsoidNetworkAxis2X': {'defaultValue' : 0.0,
                             'lowerValue' : -1.0,
                             'upperValue' : 1.0,
                             'incrementValue' : 0.1 }, 
  'ellipsoidEigenVector2Y': {'defaultValue' : 1.0,
                             'lowerValue' : -1.0,
                             'upperValue' : 1.0,
                             'incrementValue' : 0.1 }, 
  'ellipsoidNetworkAxis2Z': {'defaultValue' : 0.0,
                             'lowerValue' : -1.0,
                             'upperValue' : 1.0,
                             'incrementValue' : 0.1 }, 
  'ellipsoidEigenValue2': {'defaultValue' : 2.0,
                           'lowerValue' : 1.0,
                           'upperValue' : 100.0,
                           'incrementValue' : 1.0 }, 
  'elementaryGeometry': {'defaultValue' : 0,
                         'choices' : ['box',
                                      'cylinder',
                                      'ellipsoid',
                                      'fiber',
                                      'sphere',
                                      'star'] }, 
  'cylinderNetworkCount1': {'defaultValue' : 10,
                            'lowerValue' : 1,
                            'upperValue' : 1000,
                            'incrementValue' : 1 }, 
  'randomRotationOfFibers': {'defaultValue' : 0 }, 
  'sphereNetworkAxis2Z': {'defaultValue' : 0.0,
                          'lowerValue' : -1.0,
                          'upperValue' : 1.0,
                          'incrementValue' : 0.1 }, 
  'cylinderAxisZ': {'defaultValue' : 1.0,
                    'lowerValue' : -10.0,
                    'upperValue' : 10.0,
                    'incrementValue' : 0.1 }, 
  'starNetworkCount1': {'defaultValue' : 10,
                        'lowerValue' : 1,
                        'upperValue' : 1000,
                        'incrementValue' : 1 }, 
  'cylinderAxisX': {'defaultValue' : 0.0,
                    'lowerValue' : -10.0,
                    'upperValue' : 10.0,
                    'incrementValue' : 0.1 }, 
  'cylinderAxisY': {'defaultValue' : 0.0,
                    'lowerValue' : -10.0,
                    'upperValue' : 10.0,
                    'incrementValue' : 0.1 }, 
  'fiberRadius': {'defaultValue' : 1.0,
                  'lowerValue' : 0.001,
                  'upperValue' : 50.0,
                  'incrementValue' : 1.0 }, 
  'fiberNetwork': {'defaultValue' : 0 }, 
  'cylinderNetworkCount2': {'defaultValue' : 10,
                            'lowerValue' : 1,
                            'upperValue' : 1000,
                            'incrementValue' : 1 }, 
  'starVextexCount': {'defaultValue' : 1000,
                      'lowerValue' : 10,
                      'upperValue' : 10000,
                      'incrementValue' : 1 }, 
  'ellipsoidEigenVector3Z': {'defaultValue' : 1.0,
                             'lowerValue' : -1.0,
                             'upperValue' : 1.0,
                             'incrementValue' : 0.1 }, 
  'randomRotationOfStar': {'defaultValue' : 0 }, 
  'starSymmetricPeak': {'defaultValue' : 0 }, 
  'cylinderLength': {'defaultValue' : 100.0,
                     'lowerValue' : 0.0,
                     'upperValue' : 2000.0,
                     'incrementValue' : 1.0 }, 
  'ellipsoidNetworkAxis3X': {'defaultValue' : 0.0,
                             'lowerValue' : -1.0,
                             'upperValue' : 1.0,
                             'incrementValue' : 0.1 }, 
  'starCenterZ': {'defaultValue' : 0.0,
                  'lowerValue' : -1000.0,
                  'upperValue' : 1000.0,
                  'incrementValue' : 1.0 }, 
  'starNetworkSpacing1': {'defaultValue' : 20.0,
                          'lowerValue' : 0.0,
                          'upperValue' : 1000.0,
                          'incrementValue' : 0.1 }, 
  'starCenterX': {'defaultValue' : 0.0,
                  'lowerValue' : -1000.0,
                  'upperValue' : 1000.0,
                  'incrementValue' : 1.0 }, 
  'starNetworkSpacing3': {'defaultValue' : 20.0,
                          'lowerValue' : 0.0,
                          'upperValue' : 1000.0,
                          'incrementValue' : 0.1 }, 
  'sphereNetworkCount3': {'defaultValue' : 10,
                          'lowerValue' : 1,
                          'upperValue' : 1000,
                          'incrementValue' : 1 }, 
  'ellipsoidNetworkSpacing2': {'defaultValue' : 10.0,
                               'lowerValue' : 0.0,
                               'upperValue' : 1000.0,
                               'incrementValue' : 0.1 }, 
  'ellipsoidNetworkSpacing3': {'defaultValue' : 10.0,
                               'lowerValue' : 0.0,
                               'upperValue' : 1000.0,
                               'incrementValue' : 0.1 }, 
  'sphereNetworkAxis3X': {'defaultValue' : 0.0,
                          'lowerValue' : -1.0,
                          'upperValue' : 1.0,
                          'incrementValue' : 0.1 }, 
  'ellipsoidNetworkSpacing1': {'defaultValue' : 10.0,
                               'lowerValue' : 0.0,
                               'upperValue' : 1000.0,
                               'incrementValue' : 0.1 }, 
  'fiberManualChoice': {'defaultValue' : 1 }, 
  'fiberVertexCountPerPlane': {'defaultValue' : 36,
                               'lowerValue' : 4,
                               'upperValue' : 100,
                               'incrementValue' : 1 }, 
  'ellipsoidNetworkAxis1Z': {'defaultValue' : 0.0,
                             'lowerValue' : -1.0,
                             'upperValue' : 1.0,
                             'incrementValue' : 0.1 }, 
  'randomRotationOfCylinders': {'defaultValue' : 0 }, 
  'fiberNetworkSpacing1': {'defaultValue' : 5.0,
                           'lowerValue' : 0.0,
                           'upperValue' : 1000.0,
                           'incrementValue' : 0.1 }, 
  'fiberNetworkSpacing2': {'defaultValue' : 5.0,
                           'lowerValue' : 0.0,
                           'upperValue' : 1000.0,
                           'incrementValue' : 0.1 }, 
  'boxNetwork': {'defaultValue' : 0 }, 
  'fiberNetworkCount1': {'defaultValue' : 2,
                         'lowerValue' : 1,
                         'upperValue' : 1000,
                         'incrementValue' : 1 }, 
  'fiberNetworkCount2': {'defaultValue' : 2,
                         'lowerValue' : 1,
                         'upperValue' : 1000,
                         'incrementValue' : 1 }, 
  'ellipsoidNetworkAxis3Y': {'defaultValue' : 0.0,
                             'lowerValue' : -1.0,
                             'upperValue' : 1.0,
                             'incrementValue' : 0.1 }, 
  'sphereNetworkAxis3Z': {'defaultValue' : 1.0,
                          'lowerValue' : -1.0,
                          'upperValue' : 1.0,
                          'incrementValue' : 0.1 }, 
  'elementaryGeometryName': {'defaultValue' : '' }, 
  'listOfPointsAndRadii': [], 
  'sphereNetworkAxis2Y': {'defaultValue' : 1.0,
                          'lowerValue' : -1.0,
                          'upperValue' : 1.0,
                          'incrementValue' : 0.1 }, 
  'sphereNetworkAxis2X': {'defaultValue' : 0.0,
                          'lowerValue' : -1.0,
                          'upperValue' : 1.0,
                          'incrementValue' : 0.1 }, 
  'starPeakCount': {'defaultValue' : 12,
                    'lowerValue' : 12,
                    'upperValue' : 256,
                    'incrementValue' : 2 }, 
  'boxUpperPointY': {'defaultValue' : 5.0,
                     'lowerValue' : -1000.0,
                     'upperValue' : 1000.0,
                     'incrementValue' : 1.0 }, 
  'boxUpperPointX': {'defaultValue' : 5.0,
                     'lowerValue' : -1000.0,
                     'upperValue' : 1000.0,
                     'incrementValue' : 1.0 }, 
  'boxUpperPointZ': {'defaultValue' : 5.0,
                     'lowerValue' : -1000.0,
                     'upperValue' : 1000.0,
                     'incrementValue' : 1.0 }, 
  'starNetworkAxis3Y': {'defaultValue' : 0.0,
                        'lowerValue' : -1.0,
                        'upperValue' : 1.0,
                        'incrementValue' : 0.1 }, 
  'cylinderRadius2': {'defaultValue' : 3.5,
                      'lowerValue' : 1.0,
                      'upperValue' : 100.0,
                      'incrementValue' : 1.0 }, 
  'cylinderRadius1': {'defaultValue' : 3.5,
                      'lowerValue' : 1.0,
                      'upperValue' : 100.0,
                      'incrementValue' : 1.0 }, 
  'starNetworkAxis3Z': {'defaultValue' : 1.0,
                        'lowerValue' : -1.0,
                        'upperValue' : 1.0,
                        'incrementValue' : 0.1 }, 
  'boxNetworkAxis2Z': {'defaultValue' : 0.0,
                       'lowerValue' : -1.0,
                       'upperValue' : 1.0,
                       'incrementValue' : 0.1 }, 
  'boxNetworkAxis2X': {'defaultValue' : 0.0,
                       'lowerValue' : -1.0,
                       'upperValue' : 1.0,
                       'incrementValue' : 0.1 }, 
  'boxNetworkAxis2Y': {'defaultValue' : 1.0,
                       'lowerValue' : -1.0,
                       'upperValue' : 1.0,
                       'incrementValue' : 0.1 }, 
  'boxNetworkCount1': {'defaultValue' : 10,
                       'lowerValue' : 1,
                       'upperValue' : 1000,
                       'incrementValue' : 1 }, 
  'boxNetworkCount2': {'defaultValue' : 10,
                       'lowerValue' : 1,
                       'upperValue' : 1000,
                       'incrementValue' : 1 }, 
  'boxNetworkCount3': {'defaultValue' : 10,
                       'lowerValue' : 1,
                       'upperValue' : 1000,
                       'incrementValue' : 1 }, 
  'cylinderNetworkSpacing2': {'defaultValue' : 8.0,
                              'lowerValue' : 0.0,
                              'upperValue' : 1000.0,
                              'incrementValue' : 0.1 }, 
  'cylinderNetworkSpacing1': {'defaultValue' : 8.0,
                              'lowerValue' : 0.0,
                              'upperValue' : 1000.0,
                              'incrementValue' : 0.1 }, 
  'outputWorkDirectory': {'defaultValue' : '' }, 
  'starRadius': {'defaultValue' : 3.0,
                 'lowerValue' : 1.0,
                 'upperValue' : 100.0,
                 'incrementValue' : 1.0 }, 
  'randomRotationOfEllipsoids': {'defaultValue' : 0 }, 
  'starNetworkAxis3X': {'defaultValue' : 0.0,
                        'lowerValue' : -1.0,
                        'upperValue' : 1.0,
                        'incrementValue' : 0.1 }, 
  'boxNetworkAxis1Z': {'defaultValue' : 0.0,
                       'lowerValue' : -1.0,
                       'upperValue' : 1.0,
                       'incrementValue' : 0.1 }, 
  'boxNetworkAxis1Y': {'defaultValue' : 0.0,
                       'lowerValue' : -1.0,
                       'upperValue' : 1.0,
                       'incrementValue' : 0.1 }, 
  'boxNetworkAxis1X': {'defaultValue' : 1.0,
                       'lowerValue' : -1.0,
                       'upperValue' : 1.0,
                       'incrementValue' : 0.1 }, 
  'randomRotationOfBox': {'defaultValue' : 0 }, 
  'starNetworkCount2': {'defaultValue' : 10,
                        'lowerValue' : 1,
                        'upperValue' : 1000,
                        'incrementValue' : 1 }, 
  'randomRotationOfCylinder': {'defaultValue' : 0 }, 
  'cylinderNetworkAxis1Y': {'defaultValue' : 0.0,
                            'lowerValue' : -10.0,
                            'upperValue' : 10.0,
                            'incrementValue' : 0.1 }, 
  'cylinderNetworkAxis1X': {'defaultValue' : 1.0,
                            'lowerValue' : -10.0,
                            'upperValue' : 10.0,
                            'incrementValue' : 0.1 }, 
  'sphereNetwork': {'defaultValue' : 0 }, 
  'cylinderNetworkAxis1Z': {'defaultValue' : 0.0,
                            'lowerValue' : -10.0,
                            'upperValue' : 10.0,
                            'incrementValue' : 0.1 }, 
  'starPeakApertureAngles': {'defaultValue' : '15 15 15 15 15 15 15 15 15 15 15 15' },
  'fiberNetworkAxis1Z': {'defaultValue' : 0.0,
                         'lowerValue' : -1.0,
                         'upperValue' : 1.0,
                         'incrementValue' : 0.1 }, 
  'fiberNetworkAxis1X': {'defaultValue' : 1.0,
                         'lowerValue' : -1.0,
                         'upperValue' : 1.0,
                         'incrementValue' : 0.1 }, 
  'fiberNetworkAxis1Y': {'defaultValue' : 0.0,
                         'lowerValue' : -1.0,
                         'upperValue' : 1.0,
                         'incrementValue' : 0.1 }, 
  'sphereNetworkAxis1Y': {'defaultValue' : 0.0,
                          'lowerValue' : -1.0,
                          'upperValue' : 1.0,
                          'incrementValue' : 0.1 }, 
  'sphereNetworkAxis3Y': {'defaultValue' : 0.0,
                          'lowerValue' : -1.0,
                          'upperValue' : 1.0,
                          'incrementValue' : 0.1 }, 
  'starNetworkCount3': {'defaultValue' : 10,
                        'lowerValue' : 1,
                        'upperValue' : 1000,
                        'incrementValue' : 1 }}
