specySettings = {
  'discardedOrientationList': {'defaultValue' : '' }, 
  'roughMaskDirectory': {'defaultValue' : '' }, 
  'rawDwiDirectory': {'defaultValue' : '' }, 
  'outputWorkDirectory': {'defaultValue' : '' }, 
  '_subjectName': {'defaultValue' : '' }, 
  'outlierFactor': {'defaultValue' : 3.0,
                    'lowerValue' : 0.1,
                    'upperValue' : 10,
                    'incrementValue' : 0.1}}
