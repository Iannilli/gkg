specySettings = {
  'removeROIMask': {'defaultValue' : 0 }, 
  'outputWorkDirectory': {'defaultValue' : '' }, 
  'addROIMask': {'defaultValue' : 0 },  
  'fileNameHistogramAnalysis': {'defaultValue' : '' }, 
  'fileNameUnbiasedT1': {'defaultValue' : '' }, 
  '_subjectName': {'defaultValue' : '' }, 
  'addCerebellum': {'defaultValue' : 0 }, 
  'addCommissures': {'defaultValue' : 0 }, 
  'fileNameCommissureCoordinates': {'defaultValue' : '' }, 
  'removeTemporaryFiles': {'defaultValue' : 2 },  
  'fileNameVoronoiMask': {'defaultValue' : '' }, 
  'fileNameROIMaskToAdd': {'defaultValue' : '' }, 
  'fileNameROIMaskToRemove': {'defaultValue' : '' }}
