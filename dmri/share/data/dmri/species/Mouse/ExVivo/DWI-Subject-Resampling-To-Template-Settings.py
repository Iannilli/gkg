specySettings = {
  'fileNameInputData': {'defaultValue': '' }, 
  'useTransformationWithoutOrigin': {'defaultValue': 0 }, 
  'outputWorkDirectory': {'defaultValue': '' }, 
  'transformationDirection': {'defaultValue': 0,
                              'choices' : ['from subject space to template space',
                                           'from template space to subject space'] },
  '_subjectName': {'defaultValue': '' }, 
  'directoryNameRegistration': {'defaultValue': '' }}
