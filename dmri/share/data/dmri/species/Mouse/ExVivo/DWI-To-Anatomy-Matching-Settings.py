specySettings = {
  'correctedDwiDirectory': {'defaultValue' : '' }, 
  't1PosteriorYCropping': {'defaultValue' : 0,
                           'lowerValue' : 0,
                           'upperValue' : 1000,
                           'incrementValue' : 1 }, 
  'anteriorPosteriorAdditionSliceCount': {'defaultValue' : 0,
                                          'lowerValue' : 0,
                                          'upperValue' : 1000,
                                          'incrementValue' : 1 }, 
  't1RightXCropping': {'defaultValue' : 0,
                       'lowerValue' : 0,
                       'upperValue' : 1000,
                       'incrementValue' : 1 }, 
  'roughMaskDirectory': {'defaultValue' : '' }, 
  'importDwToT1Transformation': {'defaultValue' : 0 }, 
  'fileNameDwToT1Transformation': {'defaultValue' : '' }, 
  'outputWorkDirectory': {'defaultValue' : '' }, 
  't1LeftXCropping': {'defaultValue' : 0,
                      'lowerValue' : 0,
                      'upperValue' : 1000,
                      'incrementValue' : 1 }, 
  'dwToT1RegistrationParameter': {
    'initializeCoefficientsUsingCenterOfGravity': True, 
    'subSamplingMaximumSizes': '64', 
    'optimizerParametersTranslationX': 30, 
    'referenceLowerThreshold': 0.0, 
    'resamplingOrder': 1, 
    'initialParametersShearingXZ': 0.0, 
    'initialParametersShearingXY': 0.0, 
    'optimizerParametersScalingX': 0.05, 
    'stepSize': 0.1, 
    'stoppingCriterionError': 0.01, 
    'optimizerParametersShearingYZ': 0.05, 
    'initialParametersScalingX': 1.0, 
    'initialParametersScalingY': 1.0, 
    'initialParametersScalingZ': 1.0, 
    'optimizerParametersTranslationY': 30, 
    'optimizerName': 0, 
    'optimizerParametersTranslationZ': 30, 
    'floatingLowerThreshold': 0.0, 
    'maximumTestGradient': 1000.0, 
    'optimizerParametersShearingXY': 0.05, 
    'optimizerParametersShearingXZ': 0.05, 
    'initialParametersRotationX': 0, 
    'initialParametersRotationY': 0, 
    'initialParametersRotationZ': 0, 
    'maximumIterationCount': 1000, 
    'applySmoothing': 1, 
    'optimizerParametersRotationX': 5, 
    'optimizerParametersRotationZ': 5, 
    'optimizerParametersScalingZ': 0.05, 
    'optimizerParametersScalingY': 0.05, 
    'initialParametersShearingYZ': 0.0, 
    'maximumTolerance': 0.01, 
    'initialParametersTranslationZ': 0, 
    'levelCount': 32, 
    'initialParametersTranslationX': 0, 
    'initialParametersTranslationY': 0, 
    'transform3DType': 0, 
    'similarityMeasureName': 1, 
    'optimizerParametersRotationY': 5},
  't1HeadZCropping': {'defaultValue' : 0,
                      'lowerValue' : 0,
                      'upperValue' : 1000,
                      'incrementValue' : 1 }, 
  '_subjectName': {'defaultValue' : '' }, 
  'fileNameACP': {'defaultValue' : '' }, 
  'leftRightAdditionSliceCount': {'defaultValue' : 0,
                                  'lowerValue' : 0,
                                  'upperValue' : 1000,
                                  'incrementValue' : 1 }, 
  't1AnteriorYCropping': {'defaultValue' : 0,
                          'lowerValue' : 0,
                          'upperValue' : 1000,
                          'incrementValue' : 1 }, 
  'generateDwToT1Transformation': {'defaultValue' : 1 }, 
  'fileNameT1': {'defaultValue' : '' }, 
  'headFootAdditionSliceCount': {'defaultValue' : 0,
                                 'lowerValue' : 0,
                                 'upperValue' : 1000,
                                 'incrementValue' : 1 }, 
  't1FootZCropping': {'defaultValue' : 0,
                      'lowerValue' : 0,
                      'upperValue' : 1000,
                      'incrementValue' : 1 },
  'applyFreesurferReconAll' : {'defaultValue' : 0},
  'computeNormalization' : {'defaultValue' : 0},
  'useCustomMorphologistDirectory' : {'defaultValue' : 0},
  'customMorphologistDirectory' : {'defaultValue' : ''},
  'templateName' : {'defaultValue' : 0,
                    'choices' : ['t2-WHS-40um',
		                 't2-Duke-43um' ]},
  'resampleDwiDataToTalairachAcpcSpace' : {'defaultValue' : 0},
  'normalization3dSettings' : {
    'subSamplingMaximumSizes': '64 128',
    'optimizerParametersTranslationX': 5,
    'optimizerParametersShearingYZ': 0.01,
    'resamplingOrder': 1,
    'initialParametersShearingXZ': 0.0,
    'initialParametersShearingXY': 0.0,
    'templateLowerThreshold': 0.0,
    'optimizerParametersScalingX': 0.02,
    'stoppingCriterionError': 0.01,
    'stepSize': 0.1,
    'initialParametersScalingX': 1.0,
    'initialParametersScalingY': 1.0,
    'initialParametersScalingZ': 1.0,
    'optimizerParametersTranslationY': 5,
    'optimizerName': 0,
    'optimizerParametersTranslationZ': 5,
    'floatingLowerThreshold': 0.0,
    'maximumTestGradient': 1000.0,
    'optimizerParametersShearingXY': 0.01,
    'optimizerParametersShearingXZ': 0.01,
    'initialParametersRotationX': 0,
    'initialParametersRotationY': 0,
    'initialParametersRotationZ': 0,
    'maximumIterationCount': 2000,
    'applySmoothing': 1,
    'optimizerParametersRotationX': 10,
    'optimizerParametersRotationZ': 10,
    'optimizerParametersScalingZ': 0.02,
    'optimizerParametersScalingY': 0.02,
    'initialParametersShearingYZ': 0.0,
    'maximumTolerance': 0.01,
    'initialParametersTranslationZ': 0,
    'levelCount': 32,
    'initialParametersTranslationX': 0,
    'initialParametersTranslationY': 0,
    'similarityMeasureName': 1,
    'optimizerParametersRotationY': 10},
  'templateFlipping' : {'defaultValue' : '' } }
