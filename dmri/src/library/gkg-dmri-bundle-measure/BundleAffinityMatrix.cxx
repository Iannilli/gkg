#include <gkg-dmri-bundle-measure/BundleAffinityMatrix.h>
#include <gkg-dmri-bundle-measure/BundleMeasure_i.h>
#include <gkg-core-io/Eraser.h>
#include <gkg-core-exception/Exception.h>
#include <cmath>
#include <iomanip>


gkg::BundleAffinityMatrix::BundleAffinityMatrix(
              gkg::RCPointer< gkg::FiberDistanceFunctor > fiberDistanceFunctor,
              float maximumDistance,
              float variance,
              int32_t resamplingPointCount )
                          : gkg::BundleMeasure< gkg::SparseMatrix >(),
                            _fiberDistanceFunctor( fiberDistanceFunctor ),
                            _maximumDistance( maximumDistance ),
                            _variance( variance ),
                            _resamplingPointCount( resamplingPointCount )
{
}


gkg::BundleAffinityMatrix::~BundleAffinityMatrix()
{
}


gkg::SparseMatrix gkg::BundleAffinityMatrix::get(
                         const gkg::BundleAffinityMatrix::Bundle& bundle ) const
{

  try
  {

    gkg::SparseMatrix affinityMatrix;
    this->get( bundle, affinityMatrix );

    return affinityMatrix;

  }
  GKG_CATCH( "gkg::SparseMatrix gkg::BundleAffinityMatrix::get( "
             "const gkg::BundleAffinityMatrix::Bundle& bundle ) const" );

}


void gkg::BundleAffinityMatrix::get(
                         const gkg::BundleAffinityMatrix::Bundle& bundle,
                         gkg::SparseMatrix& affinityMatrix  ) const
{

  try
  {

    int32_t fiberCount = bundle.getCurve3dCount();
    affinityMatrix.reallocate( fiberCount + 1, fiberCount + 1 );

    float fourTimesMaximumDistance = 4.0f * _maximumDistance;
    float minimumAffinity = ( float )std::exp( -( _maximumDistance *
                                                  _maximumDistance ) /
                                               _variance );

    int32_t f1 = 0;
    int32_t f2 = 0;

    float distance = 0.0f;
    float affinity = 0.0f;

    for ( f1 = 0; f1 < fiberCount; f1++ )
    {

      const LightCurve3d< float >& centroid1 = bundle.getCurve3d( f1 );
      float length1 = centroid1.getLength();

      for ( f2 = 0; f2 < f1; f2++ )
      {

        const LightCurve3d< float >& centroid2 = bundle.getCurve3d( f2 );
        float length2 = centroid2.getLength();

        if ( std::fabs( length1 - length2 ) < fourTimesMaximumDistance )
        {
 
          distance = _fiberDistanceFunctor->getDistance( centroid1, centroid2 );
          affinity = ( float )std::exp( -( distance * distance ) / _variance );

          if ( affinity >= minimumAffinity )
          {

            affinityMatrix( f1 + 1, f2 + 1 ) = affinity;

          }

        }

      }

    }

  }
  GKG_CATCH( "void gkg::BundleAffinityMatrix::get( "
             "const gkg::BundleAffinityMatrix::Bundle& bundle, "
             "gkg::SparseMatrix& affinityMatrix ) const" );

}


void gkg::BundleAffinityMatrix::get(
  const std::vector< std::vector< gkg::LightCurve3d< float > > >& lightCurve3ds,
  const std::vector< std::pair< int32_t, int32_t > >&
                                sparseMatrixIndexToSubjectIndexAndCentroidIndex,
   int32_t sparseMatrixSize,
   gkg::SparseMatrix& affinityMatrix,
   bool verbose ) const
{

  try
  {

    int32_t subjectIndex = 0;
    int32_t subjectCount = ( int32_t )lightCurve3ds.size();

    std::vector< std::vector< gkg::LightCurve3d< float > > >
      resampledLightCurve3ds( subjectCount );

    for ( subjectIndex = 0; subjectIndex < subjectCount; subjectIndex++ )
    {

      int32_t centroidCount = ( int32_t )lightCurve3ds[ subjectIndex ].size();

      resampledLightCurve3ds[ subjectIndex ].resize( centroidCount );

      int32_t centroidIndex = 0;
      for ( centroidIndex = 0; centroidIndex < centroidCount; centroidIndex++ )
      {

        resampledLightCurve3ds[ subjectIndex ][ centroidIndex ] =
          lightCurve3ds[ subjectIndex ][ centroidIndex ].
                                   getEquidistantCurve( _resamplingPointCount );

      }

    }

    affinityMatrix.reallocate( sparseMatrixSize + 1, sparseMatrixSize + 1 );

    float fourTimesMaximumDistance = 4.0f * _maximumDistance;
    float minimumAffinity = ( float )std::exp( -( _maximumDistance *
                                                  _maximumDistance ) /
                                               _variance );

    int32_t f1 = 0;
    int32_t f2 = 0;

    float distance = 0.0f;
    float affinity = 0.0f;

    if ( verbose )
    {

      std::cout << "[ " << std::setw( 5 ) << f1
                << " / " << std::setw( 5 ) << sparseMatrixSize
                << " ]" << std::flush;

    }

    for ( f1 = 0; f1 < sparseMatrixSize; f1++ )
    {


      if ( verbose )
      {

        std::cout << gkg::Eraser( 17 );
        std::cout << "[ " << std::setw( 5 ) << f1 + 1
                  << " / " << std::setw( 5 ) << sparseMatrixSize
                  << " ]" << std::flush;


      }

      const std::pair< int32_t, int32_t >&
        subjectIndexAndCentroidIndex1 = 
           sparseMatrixIndexToSubjectIndexAndCentroidIndex[ f1 ];

      const LightCurve3d< float >&
        centroid1 = resampledLightCurve3ds
                                       [ subjectIndexAndCentroidIndex1.first ]
                                       [ subjectIndexAndCentroidIndex1.second ];
      float length1 = centroid1.getLength();

      for ( f2 = 0; f2 < f1; f2++ )
      {


        const std::pair< int32_t, int32_t >&
          subjectIndexAndCentroidIndex2 = 
                          sparseMatrixIndexToSubjectIndexAndCentroidIndex[ f2 ];

        const LightCurve3d< float >&
          centroid2 = resampledLightCurve3ds
                                       [ subjectIndexAndCentroidIndex2.first ]
                                       [ subjectIndexAndCentroidIndex2.second ];

        float length2 = centroid2.getLength();

        if ( std::fabs( length1 - length2 ) < fourTimesMaximumDistance )
        {
 
          
          distance = _fiberDistanceFunctor->getDistance( centroid1,
                                                         centroid2 );
          affinity = ( float )std::exp( -( distance * distance ) / _variance );

          if ( affinity >= minimumAffinity )
          {

            affinityMatrix( f1 + 1, f2 + 1 ) = affinity;

          }

        }

      }

    }

    if ( verbose )
    {

      std::cout << gkg::Eraser( 17 );

    }

  }
  GKG_CATCH( "void gkg::BundleAffinityMatrix::get( "
             "const std::vector< std::vector< gkg::LightCurve3d< float > > >& "
             "lightCurve3ds, "
             "const std::vector< std::pair< int32_t, int32_t > >& "
             "sparseMatrixIndexToSubjectIndexAndCentroidIndex, "
             "int32_t sparseMatrixSize, "
             "gkg::SparseMatrix& affinityMatrix, "
             "bool verbose ) const" );

}
