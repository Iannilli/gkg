#ifndef _gkg_dmri_bundle_measure_BundleAffinityMatrix_h_
#define _gkg_dmri_bundle_measure_BundleAffinityMatrix_h_


#include <gkg-dmri-bundle-measure/BundleMeasure.h>
#include <gkg-dmri-fiber-distance/FiberDistanceFunctor.h>
#include <gkg-processing-numericalanalysis/SparseMatrix.h>
#include <gkg-core-pattern/RCPointer.h>
#include <vector>
#include <map>
#include <utility>


namespace gkg
{


class BundleAffinityMatrix : public BundleMeasure< SparseMatrix >
{

  public:

    typedef BundleMeasure< SparseMatrix >::Bundle Bundle;

    BundleAffinityMatrix(
                       RCPointer< FiberDistanceFunctor > fiberDistanceFunctor,
                       float maximumDistance,
                       float variance,
                       int32_t resamplingPointCount );
    virtual ~BundleAffinityMatrix();

    SparseMatrix get( const Bundle& bundle ) const;
    void get( const Bundle& bundle, SparseMatrix& affinityMatrix ) const;
    void get(
       const std::vector< std::vector< LightCurve3d< float > > >& lightCurve3ds,
       const std::vector< std::pair< int32_t, int32_t > >&
                                sparseMatrixIndexToSubjectIndexAndCentroidIndex,
       int32_t sparseMatrixSize,
       gkg::SparseMatrix& affinityMatrix,
       bool verbose  ) const;


  protected:

    RCPointer< FiberDistanceFunctor > _fiberDistanceFunctor;
    float _maximumDistance;
    float _variance;
    int32_t _resamplingPointCount;

};


}


#endif
