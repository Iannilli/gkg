#include <gkg-dmri-diffusion-kernel/DiffusionKernel.h>
#include <gkg-processing-container/Volume_i.h>
#include <gkg-dmri-pdf-dti/TensorCartesianField.h>
#include <gkg-processing-container/CartesianField_i.h>
#include <gkg-core-io/Eraser.h>
#include <gkg-core-exception/Exception.h>
#include <iostream>
#include <iomanip>
#include <cmath>


gkg::DiffusionKernel::DiffusionKernel(
                          gkg::RCPointer< gkg::Volume< float > > t2,
                          gkg::RCPointer< gkg::Volume< float > > dw,
                          gkg::RCPointer< gkg::Volume< int16_t > > mask,
                          int32_t voxelCount,
                          float fractionalAnisotropyLowerThreshold,
                          float fractionalAnisotropyUpperThreshold,
                          gkg::DiffusionKernel::Type type,
                          bool verbose )
                     : _voxelCount( voxelCount ),
                       _fractionalAnisotropyLowerThreshold(
                                          fractionalAnisotropyLowerThreshold ),
                       _fractionalAnisotropyUpperThreshold(
                                          fractionalAnisotropyUpperThreshold ),
                       _type( type )
{

  try
  {

    if ( _type == gkg::DiffusionKernel::SymmetricTensor )
    {

      estimateAverageSymmetricTensor( t2, dw, mask, verbose );

    }

  }
  GKG_CATCH( "gkg::DiffusionKernel::DiffusionKernel( "
             "gkg::RCPointer< gkg::Volume< float > > t2, "
             "gkg::RCPointer< gkg::Volume< float > > dw, "
             "gkg::RCPointer< gkg::Volume< int16_t > > mask, "
             "int32_t voxelCount, "
             "float fractionalAnisotropyLowerThreshold, "
             "float fractionalAnisotropyUpperThreshold, "
             "gkg::DiffusionKernel::Type type, "
             "bool verbose )" );

}


gkg::DiffusionKernel::DiffusionKernel( float eigenValue,
                                       float ratio )
                     : _voxelCount( 0 ),
                       _fractionalAnisotropyLowerThreshold( 0.0 ),
                       _fractionalAnisotropyUpperThreshold( 1.0 ),
                       _type( gkg::DiffusionKernel::SymmetricTensor )
{

  try
  {

    gkg::Vector coefficients( 6 );
    coefficients( 0 ) = eigenValue;  //1.44e-09 is a good value
    coefficients( 1 ) = 0.0;
    coefficients( 2 ) = 0.0;
    coefficients( 3 ) = eigenValue * ratio;
    coefficients( 4 ) = 0.0;
    coefficients( 5 ) = eigenValue * ratio;
    _tensor = gkg::Tensor( 0, coefficients );

  }
  GKG_CATCH( "gkg::DiffusionKernel::DiffusionKernel( "
             "float eigenValue, float ratio )" );

}


gkg::DiffusionKernel::DiffusionKernel( const gkg::DiffusionKernel& other )
                     : _voxelCount( other._voxelCount ),
                       _fractionalAnisotropyLowerThreshold(
                                   other._fractionalAnisotropyLowerThreshold ),
                       _fractionalAnisotropyUpperThreshold(
                                   other._fractionalAnisotropyUpperThreshold ),
                       _type( other._type ),
                       _tensor( other._tensor ),
                       _voxelUsedVolume( other._voxelUsedVolume )
{
}


gkg::DiffusionKernel::~DiffusionKernel()
{
}


gkg::DiffusionKernel& 
gkg::DiffusionKernel::operator=( const gkg::DiffusionKernel& other )
{

  try
  {

    _voxelCount = other._voxelCount;
    _fractionalAnisotropyLowerThreshold =
      other._fractionalAnisotropyLowerThreshold;
    _fractionalAnisotropyUpperThreshold =
      other._fractionalAnisotropyUpperThreshold;
    _type = other._type;
    _tensor = other._tensor;
    _voxelUsedVolume = other._voxelUsedVolume;

    return *this;

  }
  GKG_CATCH( "gkg::DiffusionKernel& "
             "gkg::DiffusionKernel::operator=( "
             "const gkg::DiffusionKernel& other )" );

}


int32_t gkg::DiffusionKernel::getVoxelCount() const
{

  return _voxelCount;

}


float gkg::DiffusionKernel::getFractionalAnisotropyLowerThreshold() const
{

  return _fractionalAnisotropyLowerThreshold;

}


float gkg::DiffusionKernel::getFractionalAnisotropyUpperThreshold() const
{

  return _fractionalAnisotropyUpperThreshold;

}



gkg::DiffusionKernel::Type gkg::DiffusionKernel::getType() const
{

  return _type;

}


std::string gkg::DiffusionKernel::getTypeName() const
{

  try
  {

    std::string typeName = "";

    if ( _type == gkg::DiffusionKernel::SymmetricTensor )
    {

      typeName = "symmetric-tensor";

    }

    return typeName;

  }
  GKG_CATCH( "std::string gkg::DiffusionKernel::getTypeName() const" );

}


const gkg::Tensor& gkg::DiffusionKernel::getTensor() const
{

  return _tensor;

}


const gkg::Volume< uint8_t >& gkg::DiffusionKernel::getVoxelUsedVolume() const
{

  return _voxelUsedVolume;

}



///////////////////// protected functions //////////////////////////////////////

// To be perfect, would need to sort FA values at every voxel of the mask
// and then use only the hightest _numberOfVoxels.
//
// Currently, simply taking the first _voxelCount voxel(s) 
// with _fractionalAnisotropyLowerThreshold < FA <
//                                          _fractionalAnisotropyUpperThreshold
////////////////////////////////////////////////////////////////////////////////
void gkg::DiffusionKernel::estimateAverageSymmetricTensor(
                             gkg::RCPointer< gkg::Volume< float > > t2,
                             gkg::RCPointer< gkg::Volume< float > > dw,
                             gkg::RCPointer< gkg::Volume< int16_t > > mask,
                             bool verbose )
{

  try
  {

    // sanity check
    int32_t sizeX = t2->getSizeX();
    int32_t sizeY = t2->getSizeY();
    int32_t sizeZ = t2->getSizeZ();

    if ( ( dw->getSizeX() != sizeX ) ||
         ( dw->getSizeY() != sizeY ) ||
         ( dw->getSizeZ() != sizeZ ) )
    {

      throw std::runtime_error( "incompatible T2 and DW size(s)" );

    }
    if ( ( mask->getSizeX() != sizeX ) ||
         ( mask->getSizeY() != sizeY ) ||
         ( mask->getSizeZ() != sizeZ ) )
    {

      throw std::runtime_error( "incompatible T2 and mask size(s)" );

    }

    _voxelUsedVolume.reallocate( sizeX, sizeY, sizeZ );
    _voxelUsedVolume.fill(0);

    double resolutionX = 1.0;
    double resolutionY = 1.0;
    double resolutionZ = 1.0;
    if ( t2->getHeader().hasAttribute( "resolutionX" ) )
    {

      t2->getHeader().getAttribute( "resolutionX" , resolutionX );
      _voxelUsedVolume.getHeader().addAttribute( "resolutionX" , resolutionX );

    }
    if( t2->getHeader().hasAttribute( "resolutionY" ) )
    {

      t2->getHeader().getAttribute( "resolutionY" , resolutionY );
      _voxelUsedVolume.getHeader().addAttribute( "resolutionY" , resolutionY );

    }
    if( t2->getHeader().hasAttribute( "resolutionZ" ) )
    {

      t2->getHeader().getAttribute( "resolutionZ" , resolutionZ );
      _voxelUsedVolume.getHeader().addAttribute( "resolutionZ" , resolutionZ );

    }

    if( verbose )
    {
      std::cout << "computing tensor cartesian field : " << std::flush;

    }

    gkg::TensorCartesianField*
      tensorCartesianField = new gkg::TensorCartesianField(
                               t2, dw, mask,
                               gkg::Tensor::LinearSquare,
                               verbose );

    if( verbose )
    {

      std::cout << "done" << std::endl;

    }

    if( verbose )
    {

      std::cout << "computing diffusion kernel with (faLow,faUp)=("
                << _fractionalAnisotropyLowerThreshold << ","
                << _fractionalAnisotropyUpperThreshold << ") range : "
                << std::flush;

    }

    // we start from the center of the volume and work our way to the outside
    int32_t maxI = std::max( sizeX / 2, 1 );
    int32_t maxJ = std::max( sizeY / 2, 1 );
    int32_t maxK = std::max( sizeZ / 2, 1 );

    int32_t count = 0;
    float lambda1 = 0.0, lambda2 = 0.0, lambda3 = 0.0, fa = 0.0;
    int32_t x,y,z;
    int32_t i, j, k;

    for ( k = 0; k < maxK; k++ )
    {

      if ( count > _voxelCount )
      {

       break;

      }

      for ( j = 0; j < maxJ; j++ )
      {

        if ( count > _voxelCount )
        {

         break;

        }

        for ( i = 0; i < maxI; i++)
        {

          if ( count > _voxelCount )
          {

            break;

          }

          x = sizeX / 2 + i;
          y = sizeY / 2 + j;
          z = sizeZ / 2 + k;

          if ( ( *mask )( x, y, z ) )
          {

            const gkg::Tensor*
              currentTensor = tensorCartesianField->getItem(x,y,z);

            if ( ( currentTensor->getFractionalAnisotropy() >=
                   _fractionalAnisotropyLowerThreshold ) &&
                 ( currentTensor->getFractionalAnisotropy() <=
                   _fractionalAnisotropyUpperThreshold ) )
            {

              lambda1 += currentTensor->getEigenValue( gkg::Tensor::Maximum );
              lambda2 += currentTensor->getEigenValue( gkg::Tensor::Middle );
              lambda3 += currentTensor->getEigenValue( gkg::Tensor::Minimum );
              fa += currentTensor->getFractionalAnisotropy();

              _voxelUsedVolume( x, y, z ) = 1U;
              ++ count;

            }

          }

          x = sizeX / 2 - i;
          y = sizeY / 2 - j;
          z = sizeZ / 2 - k;

          if ( ( *mask )( x, y, z ) )
          {

            const gkg::Tensor*
              currentTensor = tensorCartesianField->getItem(x,y,z);

            if ( ( currentTensor->getFractionalAnisotropy() >=
                   _fractionalAnisotropyLowerThreshold ) &&
                 ( currentTensor->getFractionalAnisotropy() <=
                   _fractionalAnisotropyUpperThreshold ) )
            {

              lambda1 += currentTensor->getEigenValue( gkg::Tensor::Maximum );
              lambda2 += currentTensor->getEigenValue( gkg::Tensor::Middle );
              lambda3 += currentTensor->getEigenValue( gkg::Tensor::Minimum );
              fa += currentTensor->getFractionalAnisotropy();

              _voxelUsedVolume( x, y, z ) = 1U;
              ++ count;

            }

          }

        }

      }

    }

    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }

    if ( !count )
    {

      throw std::runtime_error( "Diffusion kernel is null" );

    }
    else
    {

      lambda1 /= ( double )count;
      lambda2 /= ( double )count;
      lambda3 /= ( double )count;
      fa /= ( double )count;

    }

    gkg::Vector coefficients( 6 );
    coefficients( 0 ) = lambda1;
    coefficients( 1 ) = 0.0;
    coefficients( 2 ) = 0.0;
    coefficients( 3 ) = ( lambda2 + lambda3 ) / 2;
    coefficients( 4 ) = 0.0;
    coefficients( 5 ) = ( lambda2 + lambda3 ) / 2;
    _tensor = gkg::Tensor( 0, coefficients );

    if ( verbose )
    {

      std::cout << "average FA for top " << count - 1 << " voxel(s) is "
                << fa << std::endl;
      std::cout << "true average profile is "
                << lambda1 << "," << lambda2 << "," << lambda3 << " m^2/s"
                << std::endl;

    }

  }
  GKG_CATCH( "void gkg::DiffusionKernel::estimateAverageSymmetricTensor( "
             "gkg::RCPointer< gkg::Volume< float > > t2, "
             "gkg::RCPointer< gkg::Volume< float > > dw, "
             "gkg::RCPointer< gkg::Volume< int16_t > > mask, "
             "bool verbose )" );

}

