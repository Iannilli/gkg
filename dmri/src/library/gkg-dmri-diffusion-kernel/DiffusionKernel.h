#ifndef _gkg_dmri_diffusion_kernel_DiffusionKernel_h_
#define _gkg_dmri_diffusion_kernel_DiffusionKernel_h_


#include <gkg-core-cppext/StdInt.h>
#include <gkg-core-pattern/RCPointer.h>
#include <gkg-dmri-pdf-dti/Tensor.h>
#include <gkg-processing-container/Volume.h>


namespace gkg
{


class DiffusionKernel
{

  public:

    enum Type
    {

      SymmetricTensor

    };

    DiffusionKernel( RCPointer< Volume< float > > t2,
                     RCPointer< Volume< float > > dw,
                     RCPointer< Volume< int16_t > > mask,
                     int32_t voxelCount = 300,
                     float fractionalAnisotropyLowerThreshold = 0.65,
                     float fractionalAnisotropyUpperThreshold = 0.85, 
                     Type type = SymmetricTensor,
                     bool verbose = false );
    DiffusionKernel( float eigenValue = 1.0, float ratio = 1.0 );
    DiffusionKernel( const DiffusionKernel& other );
    virtual ~DiffusionKernel();

    DiffusionKernel& operator=( const DiffusionKernel& other );

    int32_t getVoxelCount() const;
    float getFractionalAnisotropyLowerThreshold() const;
    float getFractionalAnisotropyUpperThreshold() const;
    Type getType() const;
    std::string getTypeName() const;

    const Tensor& getTensor() const;

    const Volume< uint8_t >& getVoxelUsedVolume() const;

  protected:

    void estimateAverageSymmetricTensor( RCPointer< Volume< float > > t2,
                                         RCPointer< Volume< float > > dw,
                                         RCPointer< Volume< int16_t > > mask,
                                         bool verbose );

    int32_t _voxelCount;
    float _fractionalAnisotropyLowerThreshold;
    float _fractionalAnisotropyUpperThreshold;
    Type _type;
    Tensor _tensor;
    Volume< uint8_t > _voxelUsedVolume;

};


}


///////////////////////////////////////////////////////////////////////////////
// creating type name for DiffusionKernel
///////////////////////////////////////////////////////////////////////////////

RegisterBaseType( gkg::DiffusionKernel, gkg_DiffusionKernel );


#endif

