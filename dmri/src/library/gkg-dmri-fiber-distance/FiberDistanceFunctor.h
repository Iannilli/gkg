#ifndef _gkg_dmri_fiber_distance_FiberDistanceFunctor_h_
#define _gkg_dmri_fiber_distance_FiberDistanceFunctor_h_


#include <gkg-processing-container/LightCurve3d.h>


namespace gkg
{


class FiberDistanceFunctor
{

  public:

    virtual ~FiberDistanceFunctor();

    virtual float getDistance( const LightCurve3d< float >& fiber1,
                               const LightCurve3d< float >& fiber2 ) const = 0;

  protected:

    FiberDistanceFunctor();

};


}


#endif

