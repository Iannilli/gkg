#include <gkg-dmri-fiber-distance/NormalizedPairwiseCentroidDistanceFunctor.h>
#include <gkg-dmri-fiber-distance/FiberDistanceFunctorFactory.h>
#include <gkg-core-exception/Exception.h>
#include <algorithm>


gkg::NormalizedPairwiseCentroidDistanceFunctor::
                                      NormalizedPairwiseCentroidDistanceFunctor(
                                 const std::vector< double >& scalarParameters )
                                               : gkg::FiberDistanceFunctor()
{

  try
  {

    // sanity check(s)
    if ( scalarParameters.size() != 4U )
    {

      throw std::runtime_error(
        "bad scalar parameter count, 4 expected:\n"
        "- scalarParameters[ 0 ] -> normalization factor\n"
        "- scalarParameters[ 1 ] -> lower length threshold\n"
        "- scalarParameters[ 2 ] -> upper length threshold\n"
        "- scalarParameters[ 3 ] -> default normalized distance\n" );

    }
    _normalizationFactor = ( float )scalarParameters[ 0 ];
    _lowerLengthThreshold = ( float )scalarParameters[ 1 ];
    _upperLengthThreshold = ( float )scalarParameters[ 2 ];
    _defaultNormalizedDistance = ( float )scalarParameters[ 3 ];

  }
  GKG_CATCH( "gkg::NormalizedPairwiseCentroidDistanceFunctor::"
             "NormalizedPairwiseCentroidDistanceFunctor( "
             "const std::vector< double >& scalarParameters )" );

}


gkg::NormalizedPairwiseCentroidDistanceFunctor::
                                    ~NormalizedPairwiseCentroidDistanceFunctor()
{
}


float gkg::NormalizedPairwiseCentroidDistanceFunctor::getDistance(
                             const gkg::LightCurve3d< float >& centroid1,
                             const gkg::LightCurve3d< float >& centroid2 ) const
{

  try
  {

    // first computing Euclidean distance between the two centroids
    float distance = std::sqrt( centroid1.getMaximumDistance2( centroid2 ) );

    // then computing the minimum length of the two centroids
    float centroidLength1 = centroid1.getLength();
    float centroidLength2 = centroid2.getLength();

    float minimumCentroidLength = std::min( centroidLength1, centroidLength2 );

    float normalizedDistance = 0.0f;

    float actualNormalizationFactor = _normalizationFactor;
    if ( ( minimumCentroidLength >= ( _upperLengthThreshold / 3.0f ) ) &&
         ( minimumCentroidLength < ( _upperLengthThreshold * 3.0f / 5.0f ) ) )
    {

      actualNormalizationFactor *= 1.3f;

    }
    else
    {

      actualNormalizationFactor *= 1.6f;

    }

    normalizedDistance = distance - actualNormalizationFactor * 
                         ( minimumCentroidLength - _lowerLengthThreshold ) /
                         ( _upperLengthThreshold - _lowerLengthThreshold );

//    float c1 = _normalizationFactor * 0.08333333333335502f;
//    float c2 = _normalizationFactor * 0.15000000000001501f + c1;
// 
//    float normalizedDistance = 0.0f;
//    if ( minimumCentroidLength < 70.0f )
//    {
// 
//       normalizedDistance = distance - 
//                            _normalizationFactor *  
//                            ( ( 200.0f - minimumCentroidLength ) * 
//                               0.005555555555555f - 1.0f );
//       normalizedDistance = distance - 
//                            _normalizationFactor +
//                            ( 200.0f - minimumCentroidLength ) *
//                            0.005555555555555 * _normalizationFactor;
// 
//    }
//    else if ( minimumCentroidLength < 110.0f )
//    {
// 
//       normalizedDistance = distance -
//                            _normalizationFactor * 1.3f +
//                            c1 +
//                            ( 200.0f - minimumCentroidLength ) *
//                            0.005555555555555 * _normalizationFactor * 1.3f;
// 
//    }
//    else
//    {
//       normalizedDistance = distance -
//                            _normalizationFactor * 1.6 +
//                            c2 +
//                            ( 200.0f - minimumCentroidLength ) *
//                            0.005555555555555 * _normalizationFactor * 1.6f;
// 
//    }

   if ( normalizedDistance < 0.0f )
   {

      normalizedDistance =  _defaultNormalizedDistance;

   }

   return normalizedDistance;


  }
  GKG_CATCH( "float gkg::NormalizedPairwiseCentroidDistanceFunctor::"
             "getDistance( "
             "const gkg::LightCurve3d< float >& fiber1, "
             "const gkg::LightCurve3d< float >& fiber2 ) const" );

}



std::string gkg::NormalizedPairwiseCentroidDistanceFunctor::getStaticName()
{

  try
  {

    return "normalized-pairwise-centroid-distance";

  }
  GKG_CATCH( "std::string gkg::NormalizedPairwiseCentroidDistanceFunctor::"
             "getStaticName()" );

}


std::string 
gkg::NormalizedPairwiseCentroidDistanceFunctor::getScalarParameterHelp()
{

  try
  {

    return std::string(
      ".  <P1>: normalization factor (human:10mm)\n"
      ".  <P2>: lower length threshold (human:5mm)\n"
      ".  <P3>: upper length threshold (human:300mm)\n"
      ".  <P4>: default normalized distance (human: 0.1mm)\n" );

  }
  GKG_CATCH( "std::string "
             "gkg::NormalizedPairwiseCentroidDistanceFunctor::"
             "getStringParameterHelp()" );

}


static bool init_NormalizedPairwiseCentroidDistanceFunctor
                                                       __attribute__((unused)) =
  gkg::FiberDistanceFunctorFactory::getInstance().registerFiberDistanceFunctor(
    gkg::NormalizedPairwiseCentroidDistanceFunctor::getStaticName(),
    &gkg::NormalizedPairwiseCentroidDistanceFunctor::createInstance,
    gkg::NormalizedPairwiseCentroidDistanceFunctor::getScalarParameterHelp() );
