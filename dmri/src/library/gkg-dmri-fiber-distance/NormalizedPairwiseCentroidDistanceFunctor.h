#ifndef _gkg_dmri_fiber_distance_NormalizedPairwiseCentroidDistanceFunctor_h_
#define _gkg_dmri_fiber_distance_NormalizedPairwiseCentroidDistanceFunctor_h_


#include <gkg-dmri-fiber-distance/FiberDistanceFunctor.h>
#include <gkg-core-pattern/Creator.h>
#include <vector>


namespace gkg
{


class NormalizedPairwiseCentroidDistanceFunctor :
                  public FiberDistanceFunctor,
                  public Creator1Arg< NormalizedPairwiseCentroidDistanceFunctor,
                                      FiberDistanceFunctor,
                                      const std::vector< double >& >
{

  public:

    virtual ~NormalizedPairwiseCentroidDistanceFunctor();

    float getDistance( const LightCurve3d< float >& centroid1,
                       const LightCurve3d< float >& centroid2 ) const;

    static std::string getStaticName();
    static std::string getScalarParameterHelp();

  protected:

    friend struct Creator1Arg< NormalizedPairwiseCentroidDistanceFunctor,
                               FiberDistanceFunctor,
                               const std::vector< double >& >;

    NormalizedPairwiseCentroidDistanceFunctor(
                                const std::vector< double >& scalarParameters );

    float _normalizationFactor;
    float _lowerLengthThreshold;
    float _upperLengthThreshold;
    float _defaultNormalizedDistance;

};


}


#endif
