#ifndef _gkg_dmri_io_DefaultAsciiItemWriterOfParticle_i_h_
#define _gkg_dmri_io_DefaultAsciiItemWriterOfParticle_i_h_


#include <gkg-core-io/DefaultAsciiItemWriter_i.h>
#include <gkg-core-io/StdVectorAsciiItemWriter.h>
#include <gkg-processing-io/DefaultAsciiItemWriterOfLightCurve3d_i.h>
#include <gkg-dmri-simulator-particle/Particle.h>


//
// class DefaultAsciiItemWriter< Particle >
//


namespace gkg
{


template <>
inline
DefaultAsciiItemWriter< Particle >::DefaultAsciiItemWriter()
                                   : ItemWriter< Particle >()
{
}


template <>
inline
DefaultAsciiItemWriter< Particle >::~DefaultAsciiItemWriter()
{
}


template <>
inline
void DefaultAsciiItemWriter< Particle >::write( std::ostream & os,
                                                const Particle* pItem,
                                                size_t count ) const
{

  ItemWriter< LightCurve3d< float > >& itemW_LightCurve3d =
    DefaultAsciiItemWriter< LightCurve3d< float > >::getInstance();

  ItemWriter< std::vector< int32_t > >& itemW_vector_of_int32_t =
    StdVectorAsciiItemWriter< int32_t >::getInstance();

  size_t i;
  for ( i = 0; i < count; i++ )
  {

    itemW_LightCurve3d.write( os, pItem[ i ].getTrajectory() );
    itemW_vector_of_int32_t.write( os, pItem[ i ].getStates() );

  }

}


template <>
inline
void DefaultAsciiItemWriter< Particle >::write( largefile_ofstream & os,
                                                const Particle* pItem,
                                                size_t count ) const
{

  ItemWriter< LightCurve3d< float > >& itemW_LightCurve3d =
    DefaultAsciiItemWriter< LightCurve3d< float > >::getInstance();

  ItemWriter< std::vector< int32_t > >& itemW_vector_of_int32_t =
    StdVectorAsciiItemWriter< int32_t >::getInstance();

  size_t i;
  for ( i = 0; i < count; i++ )
  {

    itemW_LightCurve3d.write( os, pItem[ i ].getTrajectory() );
    itemW_vector_of_int32_t.write( os, pItem[ i ].getStates() );

  }

}


template <>
inline
void DefaultAsciiItemWriter< Particle >::write( largefile_fstream & fs,
                                                const Particle* pItem,
                                                size_t count ) const
{

  ItemWriter< LightCurve3d< float > >& itemW_LightCurve3d =
    DefaultAsciiItemWriter< LightCurve3d< float > >::getInstance();

  ItemWriter< std::vector< int32_t > >& itemW_vector_of_int32_t =
    StdVectorAsciiItemWriter< int32_t >::getInstance();

  size_t i;
  for ( i = 0; i < count; i++ )
  {

    itemW_LightCurve3d.write( fs, pItem[ i ].getTrajectory() );
    itemW_vector_of_int32_t.write( fs, pItem[ i ].getStates() );

  }

}


}


#endif
