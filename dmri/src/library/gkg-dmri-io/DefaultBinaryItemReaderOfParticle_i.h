#ifndef _gkg_dmri_io_DefaultBinaryItemReaderOfParticle_i_h_
#define _gkg_dmri_io_DefaultBinaryItemReaderOfParticle_i_h_


#include <gkg-core-io/DefaultBinaryItemReader_i.h>
#include <gkg-core-io/StdVectorBinaryItemReader.h>
#include <gkg-processing-io/DefaultBinaryItemReaderOfLightCurve3d_i.h>
#include <gkg-dmri-simulator-particle/Particle.h>


//
// class DefaultBinaryItemReader< Particle >
//


namespace gkg
{


template <>
inline
DefaultBinaryItemReader< gkg::Particle >::DefaultBinaryItemReader()
                                         : ItemReader< Particle >()
{
}


template <>
inline
DefaultBinaryItemReader< Particle >::~DefaultBinaryItemReader()
{
}


template <>
inline
void DefaultBinaryItemReader< Particle >::read( std::istream & is,
                                                Particle* pItem,
                                                size_t count ) const
{

  ItemReader< LightCurve3d< float > >& itemR_LightCurve3d =
    DefaultBinaryItemReader< LightCurve3d< float > >::getInstance();

  ItemReader< std::vector< int32_t > >& itemR_vector_of_int32_t =
    StdVectorBinaryItemReader< int32_t >::getInstance();
  
  size_t i;
  for ( i = 0; i < count; i++ )
  {

    LightCurve3d< float > trajectory;
    itemR_LightCurve3d.read( is, trajectory );

    std::vector< int32_t > states;
    itemR_vector_of_int32_t.read( is, states );

    pItem[ i ].setTrajectoryAndStates( trajectory, states );

  }

}


template <>
inline
void DefaultBinaryItemReader< Particle >::read( largefile_ifstream & is,
                                                Particle* pItem,
                                                size_t count ) const
{

  ItemReader< LightCurve3d< float > >& itemR_LightCurve3d =
    DefaultBinaryItemReader< LightCurve3d< float > >::getInstance();

  ItemReader< std::vector< int32_t > >& itemR_vector_of_int32_t =
    StdVectorBinaryItemReader< int32_t >::getInstance();
  
  size_t i;
  for ( i = 0; i < count; i++ )
  {

    LightCurve3d< float > trajectory;
    itemR_LightCurve3d.read( is, trajectory );

    std::vector< int32_t > states;
    itemR_vector_of_int32_t.read( is, states );

    pItem[ i ].setTrajectoryAndStates( trajectory, states );

  }

}


template <>
inline
void DefaultBinaryItemReader< Particle >::read( largefile_fstream & fs,
                                                Particle* pItem,
                                                size_t count ) const
{

  ItemReader< LightCurve3d< float > >& itemR_LightCurve3d =
    DefaultBinaryItemReader< LightCurve3d< float > >::getInstance();

  ItemReader< std::vector< int32_t > >& itemR_vector_of_int32_t =
    StdVectorBinaryItemReader< int32_t >::getInstance();
  
  size_t i;
  for ( i = 0; i < count; i++ )
  {

    LightCurve3d< float > trajectory;
    itemR_LightCurve3d.read( fs, trajectory );

    std::vector< int32_t > states;
    itemR_vector_of_int32_t.read( fs, states );

    pItem[ i ].setTrajectoryAndStates( trajectory, states );

  }

}


}


#endif
