#ifndef _gkg_dmri_io_DefaultBinaryItemWriterOfParticle_i_h_
#define _gkg_dmri_io_DefaultBinaryItemWriterOfParticle_i_h_


#include <gkg-core-io/DefaultBinaryItemWriter_i.h>
#include <gkg-core-io/StdVectorBinaryItemWriter.h>
#include <gkg-processing-io/DefaultBinaryItemWriterOfLightCurve3d_i.h>
#include <gkg-dmri-simulator-particle/Particle.h>


//
// class DefaultBinaryItemWriter< Particle >
//


namespace gkg
{


template <>
inline
DefaultBinaryItemWriter< Particle >::DefaultBinaryItemWriter()
                                    : ItemWriter< Particle >()
{
}


template <>
inline
DefaultBinaryItemWriter< Particle >::~DefaultBinaryItemWriter()
{
}


template <>
inline
void DefaultBinaryItemWriter< Particle >::write( std::ostream & os,
                                                 const Particle* pItem,
                                                 size_t count ) const
{

  ItemWriter< LightCurve3d< float > >& itemW_LightCurve3d =
    DefaultBinaryItemWriter< LightCurve3d< float > >::getInstance();

  ItemWriter< std::vector< int32_t > >& itemW_vector_of_int32_t =
    StdVectorBinaryItemWriter< int32_t >::getInstance();

  size_t i;
  for ( i = 0; i < count; i++ )
  {

    itemW_LightCurve3d.write( os, pItem[ i ].getTrajectory() );
    itemW_vector_of_int32_t.write( os, pItem[ i ].getStates() );

  }

}


template <>
inline
void DefaultBinaryItemWriter< Particle >::write( largefile_ofstream & os,
                                                 const Particle* pItem,
                                                 size_t count ) const
{

  ItemWriter< LightCurve3d< float > >& itemW_LightCurve3d =
    DefaultBinaryItemWriter< LightCurve3d< float > >::getInstance();

  ItemWriter< std::vector< int32_t > >& itemW_vector_of_int32_t =
    StdVectorBinaryItemWriter< int32_t >::getInstance();

  size_t i;
  for ( i = 0; i < count; i++ )
  {

    itemW_LightCurve3d.write( os, pItem[ i ].getTrajectory() );
    itemW_vector_of_int32_t.write( os, pItem[ i ].getStates() );

  }

}


template <>
inline
void DefaultBinaryItemWriter< Particle >::write( largefile_fstream & fs,
                                                 const Particle* pItem,
                                                 size_t count ) const
{

  ItemWriter< LightCurve3d< float > >& itemW_LightCurve3d =
    DefaultBinaryItemWriter< LightCurve3d< float > >::getInstance();

  ItemWriter< std::vector< int32_t > >& itemW_vector_of_int32_t =
    StdVectorBinaryItemWriter< int32_t >::getInstance();

  size_t i;
  for ( i = 0; i < count; i++ )
  {

    itemW_LightCurve3d.write( fs, pItem[ i ].getTrajectory() );
    itemW_vector_of_int32_t.write( fs, pItem[ i ].getStates() );

  }

}


}


#endif
