#include <gkg-core-io/DefaultItemIOFactory.h>
#include <gkg-dmri-io/DefaultAsciiItemWriterOfParticle_i.h>
#include <gkg-dmri-io/DefaultBinaryItemWriterOfParticle_i.h>
#include <gkg-dmri-io/DefaultBSwapItemWriterOfParticle_i.h>
#include <gkg-dmri-io/DefaultAsciiItemReaderOfParticle_i.h>
#include <gkg-dmri-io/DefaultBinaryItemReaderOfParticle_i.h>
#include <gkg-dmri-io/DefaultBSwapItemReaderOfParticle_i.h>


//
// class DefaultItemIOFactory< Particle >
//


namespace gkg
{


template <>
DefaultItemIOFactory< Particle >::DefaultItemIOFactory()
{
}


template <>
DefaultItemIOFactory< Particle >::~DefaultItemIOFactory()
{
}


template <>
gkg::ItemWriter< Particle >& 
gkg::DefaultItemIOFactory< Particle >::getWriter( bool ascii,
                                                  bool bswap ) const
{

  if ( ascii )
  {

    return DefaultAsciiItemWriter< Particle >::getInstance();

  }
  else if ( bswap )
  {

    return DefaultBSwapItemWriter< Particle >::getInstance();

  }
  else
  {

    return DefaultBinaryItemWriter< Particle >::getInstance();

  }

}


template <>
gkg::ItemReader< Particle >& 
gkg::DefaultItemIOFactory< Particle >::getReader( bool ascii,
                                                  bool bswap ) const
{

  if ( ascii )
  {

    return DefaultAsciiItemReader< Particle >::getInstance();

  }
  else if ( bswap )
  {

    return DefaultBSwapItemReader< Particle >::getInstance();

  }
  else
  {

    return DefaultBinaryItemReader< Particle >::getInstance();

  }

}


}


template class gkg::DefaultItemIOFactory< gkg::Particle >;


// forcing method instanciation under gcc-3
#if defined( __GNUC__ ) && (__GNUC__-0 < 3)

gkg::ItemReader< gkg::Particle >& reader_particle =
gkg::DefaultItemIOFactory< gkg::Particle >::getInstance().getReader( false,
                                                                     false );
gkg::ItemWriter< gkg::Particle >& writer_particle =
gkg::DefaultItemIOFactory< gkg::Particle >::getInstance().getWriter( false,
                                                                     false );

#endif




