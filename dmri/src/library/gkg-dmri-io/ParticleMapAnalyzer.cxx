#include <gkg-dmri-io/ParticleMapAnalyzer.h>
#include <gkg-dmri-io/ParticleMapDiskFormat.h>
#include <gkg-dmri-simulator-particle/ParticleMap.h>
#include <gkg-core-exception/Exception.h>


gkg::ParticleMapAnalyzer::ParticleMapAnalyzer()
                         : gkg::BaseDiskFormatAnalyzer()
{
}


gkg::ParticleMapAnalyzer::~ParticleMapAnalyzer()
{
}


gkg::DiskFormat& gkg::ParticleMapAnalyzer::getDiskFormat() const
{

  return gkg::ParticleMapDiskFormat::getInstance();

}


void gkg::ParticleMapAnalyzer::analyze(
                                     const std::string& name,
                                     std::string& format,
                                     gkg::AnalyzedObject& analyzedObject ) const
{

  try
  {

    gkg::ParticleMap object;
    getDiskFormat().readHeader( name, object );

    std::string objectType;
    object.getHeader().getAttribute( "object_type", objectType ); 
    analyzedObject.getHeader().addAttribute( "object_type", objectType );

    format = getDiskFormat().getName();

  }
  GKG_CATCH( "void gkg::ParticleMapAnalyzer::analyze( "
             "const std::string& name, "
             "std::string& format, "
             "gkg::AnalyzedObject& analyzedObject ) const" );

}


//
// registrating standard ParticleMap analyzer
//

static bool initialize()
{

  try
  {

    gkg::DiskFormatAnalyzer::getInstance().registerAnalyzer(
         gkg::ParticleMapDiskFormat::getInstance().getName(),
         &gkg::ParticleMapAnalyzer::getInstance() );

    return true;

  }
  GKG_CATCH( "Particle Map analyzer registration: " )

}

static bool initialized __attribute__((unused)) = initialize();
