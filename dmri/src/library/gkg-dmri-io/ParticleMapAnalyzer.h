#ifndef _gkg_dmri_io_ParticleMapAnalyzer_h_
#define _gkg_dmri_io_ParticleMapAnalyzer_h_


#include <gkg-core-pattern/Singleton.h>
#include <gkg-core-io/DiskFormatAnalyzer.h>


namespace gkg
{


class ParticleMapAnalyzer : public BaseDiskFormatAnalyzer,
                            public Singleton< ParticleMapAnalyzer >
{

  public:

    ~ParticleMapAnalyzer();

    DiskFormat& getDiskFormat() const;
    void analyze( const std::string& name,
                  std::string& format,
                  gkg::AnalyzedObject& analyzedObject ) const;


  private:

    friend class Singleton< ParticleMapAnalyzer >;

    ParticleMapAnalyzer();

};


}


#endif

