#include <gkg-dmri-io/ParticleMapDiskFormat.h>
#include <gkg-dmri-simulator-particle/ParticleMap.h>
#include <gkg-dmri-io/DefaultAsciiItemReaderOfParticle_i.h>
#include <gkg-dmri-io/DefaultAsciiItemWriterOfParticle_i.h>
#include <gkg-dmri-io/DefaultBinaryItemReaderOfParticle_i.h>
#include <gkg-dmri-io/DefaultBinaryItemWriterOfParticle_i.h>
#include <gkg-dmri-io/DefaultBSwapItemReaderOfParticle_i.h>
#include <gkg-dmri-io/DefaultBSwapItemWriterOfParticle_i.h>
#include <gkg-communication-sysinfo/ConfigurationInfo.h>
#include <gkg-communication-sysinfo/Directory.h>
#include <gkg-core-io/TypedDiskFormat_i.h>
#include <gkg-core-io/ByteOrder.h>
#include <gkg-core-io/IOMode.h>
#include <gkg-core-io/DefaultItemIOFactory_i.h>
#include <gkg-processing-container/SiteMap_i.h>
#include <gkg-core-io/TypeOf.h>
#include <gkg-core-io/StringConverter.h>
#include <gkg-core-exception/FileNotFoundException.h>
#include <gkg-core-exception/ObjectTypeException.h>
#include <gkg-core-io/DiskFormatFactory_i.h>
#include <gkg-core-exception/Exception.h>
#include <fstream>


gkg::ParticleMapDiskFormat::ParticleMapDiskFormat()
                           : gkg::TypedDiskFormat< gkg::ParticleMap >()
{

  this->_extensions.insert( ".particlemap" );
  this->_extensions.insert( getHeaderExtension() );

}


gkg::ParticleMapDiskFormat::~ParticleMapDiskFormat()
{
}


std::string gkg::ParticleMapDiskFormat::getName() const
{

  return "particlemap";

}


void gkg::ParticleMapDiskFormat::readHeader( const std::string& name,
                                             gkg::HeaderedObject& object ) const
{

  try
  {


    // first we add all ParticleMap specific attributes
    object.addSemantic( "__particlemap__", "byte_order",
                       gkg::Semantic( gkg::TypeOf< std::string >::getName() ) );
    object.addSemantic( "__particlemap__", "io_mode",
                       gkg::Semantic( gkg::TypeOf< std::string >::getName() ) );


    // reading meta-information
    try
    {

      this->gkg::DiskFormat::readHeader( name, object );

    }
    catch ( gkg::FileNotFoundException& e )
    {

      // std::cerr << e.what() << ", "
      //           << "meta-information not present"
      //           << std::endl;

    }
    catch ( std::exception& e )
    {

      throw std::runtime_error( e.what() );

    }


    // checking if it is a particle map
    std::string objectType;
    object.getHeader().getAttribute( "object_type", objectType );
    if ( objectType != "ParticleMap" )
    {

      throw gkg::ObjectTypeException( this->getNameWithoutExtension( name ) +
                                      getHeaderExtension() );

    }


    // checking that item count exists and is valid
    int32_t itemCount = 0;
    if ( object.getHeader().hasAttribute( "item_count" ) )
    {

      object.getHeader().getAttribute( "item_count", itemCount );
      if ( itemCount < 0 )
      {

        throw std::runtime_error( "invalid item count" );

      }

    }
    else
    {

       throw std::runtime_error( "item count not present" );

    }


    // checking that file count exists and is valid
    int32_t fileCount = 0;
    if ( object.getHeader().hasAttribute( "file_count" ) )
    {

      object.getHeader().getAttribute( "file_count", fileCount );
      if ( fileCount < 0 )
      {

        throw std::runtime_error( "invalid item count" );

      }

    }
    else
    {

       throw std::runtime_error( "file count not present" );

    }

    // checking that time step exists and is valid
    if ( object.getHeader().hasAttribute( "time_step" ) )
    {

      double timeStep = 0.0 ;
      object.getHeader().getAttribute( "time_step", timeStep );
      if ( timeStep < 0.0 )
      {

        throw std::runtime_error( "invalid time step" );

      }

    }
    else
    {

       throw std::runtime_error( "time step not present" );

    }

    // updating observer(s)
    object.getHeader().setChanged();
    object.getHeader().notifyObservers();

  }
  GKG_CATCH( "void gkg::ParticleMapDiskFormat::readHeader( "
             "const std::string& name, "
             "gkg::HeaderedObject& object ) const" );

}


void gkg::ParticleMapDiskFormat::writeHeader(
                                       const std::string& name,
                                       const gkg::HeaderedObject& object ) const
{

  // writing the meta-information header
  try
  {

    this->gkg::DiskFormat::writeHeader( name, object );

  }
  GKG_CATCH( "void gkg::ParticleMapDiskFormat::writeHeader( "
             "const std::string& name, "
             "const gkg::HeaderedObject& object ) const" );

}


void gkg::ParticleMapDiskFormat::read( const std::string& name,
                                       gkg::ParticleMap& object ) const
{

  try
  {

    // reading meta-information
    readHeader( name, object );


    // getting byte order
    bool bswap = false;
    if ( object.getHeader().hasAttribute( "byte_order" ) )
    {

      std::string byteOrderName;
      object.getHeader().getAttribute( "byte_order", byteOrderName );

      bswap = 
        ( gkg::ByteOrder::getInstance().getTypeFromName( byteOrderName ) !=
          gkg::ByteOrder::getInstance().getCurrentType() ? true : false );

    }


    // getting IO mode
    bool ascii = false;
    if ( object.getHeader().hasAttribute( "io_mode" ) )
    {

      std::string ioModeName;
      object.getHeader().getAttribute( "io_mode", ioModeName );
      if ( gkg::IOMode::getInstance().getTypeFromName( ioModeName ) ==
           gkg::IOMode::Ascii )
      {

        ascii = true;

      }

    }


    // getting the reference to the adequate particle item reader
    gkg::ItemReader< gkg::Particle >& itemR_Particle =
      gkg::DefaultItemIOFactory< gkg::Particle >::getInstance().getReader(
                                                                        ascii,
                                                                        bswap );

    // collecting the item count
    int32_t itemCount = 0;
    object.getHeader().getAttribute( "item_count", itemCount );

    if ( object.getHeader().hasAttribute( "time_step" ) )
    {

      //collecting the time step
      double timeStep = 0.0 ;
      object.getHeader().getAttribute( "time_step", timeStep );
      object._timeStep = ( float )( timeStep );

    }

    // collecting the file count
    object.getHeader().getAttribute( "file_count", object._fileCount );

    // creating the directory
    std::string directoryName = this->getNameWithoutExtension( name ) +
                                ".particlemap";
    gkg::Directory directory( directoryName );
    if ( !directory.isValid() )
    {

      throw std::runtime_error( "not a valid directory" );

    }


    // computing the number if item count according to the file index
    int32_t itemCountPerFile = ( itemCount / object._fileCount ) +
                               ( itemCount % object._fileCount ? 1 : 0 );
    int32_t itemCountInLastFile = itemCount - ( object._fileCount - 1 ) *
                                  itemCountPerFile;

    // resizing the particle vector according to the item count
    std::vector< gkg::RCPointer< gkg::Particle > >&
      particles = object._particles;
    particles.resize( itemCount );


    // looping over file(s)
    int32_t f = 0;
    int32_t globalItemCount = 0;
    for ( f = 0; f < object._fileCount; f++ )
    {

      int32_t localItemCountInFile = ( ( f != ( object._fileCount - 1 ) ) ?
                                       itemCountPerFile : itemCountInLastFile );
      std::string fileName = this->getNameWithoutExtension( name ) +
                             ".particlemap" + gkg::getDirectorySeparator() +
                             gkg::StringConverter::toString( f + 1 );
      std::ifstream is( fileName.c_str() );

      int32_t p = 0;
      gkg::Particle* particle = 0;
      for ( p = 0; p < localItemCountInFile; p++ )
      {

        particle = new gkg::Particle;
        itemR_Particle.read( is, *particle );
        particles[ globalItemCount ].reset( particle );

        ++ globalItemCount;

      }

      is.close();

    }
    object.updateHeader();

  }
  GKG_CATCH( "void gkg::ParticleMapDiskFormat::read( "
             "const std::string& name, "
             "gkg::ParticleMap& object ) const" );

}


void gkg::ParticleMapDiskFormat::write( const std::string& name,
                                        gkg::ParticleMap& object,
                                        bool ascii ) const
{

  try
  {

    // setting IO mode
    std::string ioModeName =
      gkg::IOMode::getInstance().getNameFromType( ascii ? gkg::IOMode::Ascii :
                                                          gkg::IOMode::Binary );
    if ( object.getHeader().hasAttribute( "io_mode" ) )
    {

      object.getHeader()[ "io_mode" ] = ioModeName;
      
    }
    else
    {

      object.getHeader().addAttribute( "io_mode", ioModeName );

    }

    // setting byte order
    std::string byteOrderName = gkg::ByteOrder::getInstance().getCurrentName();
    if ( object.getHeader().hasAttribute( "byte_order" ) )
    {

      object.getHeader()[ "byte_order" ] = byteOrderName;

    }
    else
    {

      object.getHeader().addAttribute( "byte_order", byteOrderName );

    }

    // collecting the time step
    float timeStep = object.getTimeStep();

    // setting the time step
    if ( object.getHeader().hasAttribute( "time_step" ) )
    {

      object.getHeader()[ "time_step" ] = ( double )( timeStep );

    }
    else
    {

      object.getHeader().addAttribute( "time_step", ( double )( timeStep ) );

    }

    // writing header(s) meta-information
    writeHeader( name, object );

    // collecting the item count
    int32_t itemCount = object.getParticleCount();

    // collecting the file count
    int32_t fileCount = object.getFileCount();

    // creating the directory
    std::string directoryName = this->getNameWithoutExtension( name ) +
                                ".particlemap";
    gkg::Directory directory( directoryName );
    if ( !directory.isValid() )
    {

      directory.mkdir();

    }

    // looping over file(s)
    int32_t itemCountPerFile = ( itemCount / fileCount ) +
                               ( itemCount % fileCount ? 1 : 0 );
    int32_t itemCountInLastFile = itemCount - ( fileCount - 1 ) *
                                  itemCountPerFile;

    // getting the reference to the adequate particle item writer
    gkg::ItemWriter< gkg::Particle >& itemW_Particle =
      gkg::DefaultItemIOFactory< gkg::Particle >::getInstance().getWriter(
                                                                        ascii,
                                                                        false );

    // looping over file(s)
    int32_t f = 0;
    int32_t globalItemCount = 0;
    for ( f = 0; f < fileCount; f++ )
    {

      int32_t localItemCountInFile = ( ( f != ( fileCount - 1 ) ) ?
                                       itemCountPerFile : itemCountInLastFile );
      std::string fileName = this->getNameWithoutExtension( name ) +
                             ".particlemap" + gkg::getDirectorySeparator() +
                             gkg::StringConverter::toString( f + 1 );
      std::ofstream os( fileName.c_str() );

      int32_t p = 0;
      for ( p = 0; p < localItemCountInFile; p++ )
      {

        itemW_Particle.write( os, *object.getParticle( globalItemCount ) );
        ++ globalItemCount;

      }

      os.close();

    }

  }
  GKG_CATCH( "void gkg::ParticleMapDiskFormat::write( "
             "const std::string& name, "
             "gkg::ParticleMap& object, "
             "bool ascii ) const" );

}


bool gkg::ParticleMapDiskFormat::hasReader() const
{

  return true;

}


bool gkg::ParticleMapDiskFormat::hasWriter() const
{

  return true;

}


std::string gkg::ParticleMapDiskFormat::getHeaderExtension() const
{

  try
  {

    return std::string( ".particlemap" ) + 
           gkg::ConfigurationInfo::getInstance().getHeaderExtensionName();

  }
  GKG_CATCH( "std::string "
             "gkg::ParticleMapDiskFormat::getHeaderExtension() const" );

}


//
// registrating ParticleMap disk format for "ParticleMap" object factory
//

static bool initialize()
{

  try
  {

    gkg::DiskFormatFactory< gkg::ParticleMap  
                          >::getInstance().registerDiskFormat(
         gkg::ParticleMapDiskFormat::getInstance().getName(),
         &gkg::ParticleMapDiskFormat::getInstance() );

    return true;

  }
  GKG_CATCH( "ParticleMap disk format registration: " )

}


static bool initialized __attribute__((unused)) = initialize();
