#ifndef _gkg_dmri_io_ParticleMapDiskFormat_h_
#define _gkg_dmri_io_ParticleMapDiskFormat_h_


#include <gkg-core-io/TypedDiskFormat.h>
#include <gkg-core-pattern/Singleton.h>


namespace gkg
{


class ParticleMap;


class ParticleMapDiskFormat : public TypedDiskFormat< ParticleMap >,
                              public Singleton< ParticleMapDiskFormat >
{

  public:

    ~ParticleMapDiskFormat();

    std::string getName() const;

    void readHeader( const std::string& name,
                     HeaderedObject& object ) const;
    void writeHeader( const std::string& name,
                      const HeaderedObject& object ) const;

    void read( const std::string& name,
               ParticleMap& object ) const;
    void write( const std::string& name,
                ParticleMap& object,
                bool ascii ) const;

    bool hasReader() const;
    bool hasWriter() const;

    std::string getHeaderExtension() const;

  protected:

    friend class Singleton< ParticleMapDiskFormat >;

    ParticleMapDiskFormat();

};


}


#endif

