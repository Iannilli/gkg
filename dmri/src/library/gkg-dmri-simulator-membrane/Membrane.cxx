#include <gkg-dmri-simulator-membrane/Membrane.h>
#include <gkg-processing-container/MeshMap_i.h>
#include <gkg-processing-mesh/SceneModeler.h>
#include <gkg-core-exception/Exception.h>


gkg::Membrane::Membrane( 
                      gkg::RCPointer< gkg::SceneModeler > sceneModeler,
                      gkg::RCPointer< gkg::MeshMap< int32_t, float, 3U > > mesh,
                      gkg::RCPointer< gkg::VertexEvolutionFunction > 
                        vertexEvolutionFunction,
                      gkg::RCPointer< gkg::ParticleToMembraneInteraction >
                        particleToMembraneInteraction,
                      gkg::RCPointer< gkg::MotionModel > motionModel,
                      gkg::RCPointer< gkg::InteractionLayer > interactionLayer,
                      float permeability,
                      float radiusOfInfluence,
                      bool storeVertexTrajectories )
              : gkg::EvolvingMesh( sceneModeler,
                                   mesh,
                                   vertexEvolutionFunction,
                                   radiusOfInfluence,
                                   storeVertexTrajectories ),
                _particleToMembraneInteraction( particleToMembraneInteraction ),
                _motionModel( motionModel ),
                _interactionLayer( interactionLayer ),
		_permeability( permeability )
{
}


gkg::Membrane::~Membrane()
{
}

    
gkg::RCPointer< gkg::ParticleToMembraneInteraction > 
gkg::Membrane::getParticleToMembraneInteraction() const
{

  try
  {

    return _particleToMembraneInteraction;

  }
  GKG_CATCH( "gkg::RCPointer< gkg::ParticleToMembraneInteraction > "
             "gkg::Membrane::getParticleToMembraneInteraction() const" );

}


gkg::RCPointer< gkg::MotionModel > gkg::Membrane::getMotionModel() const
{

  try
  {

    return _motionModel;

  }
  GKG_CATCH( "gkg::RCPointer< gkg::MotionModel > "
             "gkg::Membrane::getMotionModel() const" );

}


gkg::RCPointer< gkg::InteractionLayer > 
gkg::Membrane::getInteractionLayer() const
{

  try
  {

    return _interactionLayer;

  }
  GKG_CATCH( "gkg::RCPointer< gkg::InteractionLayer > "
             "gkg::Membrane::getInteractionLayer() const" );

}


float gkg::Membrane::getPermeability() const
{

  try
  {

    return _permeability;

  }
  GKG_CATCH( "float gkg::Membrane::getPermeability() const" );

}
