#ifndef _gkg_dmri_simulator_membrane_Membrane_h_
#define _gkg_dmri_simulator_membrane_Membrane_h_


#include <gkg-processing-mesh/EvolvingMesh.h>
#include <gkg-dmri-simulator-particle-membrane-interaction/ParticleToMembraneInteraction.h>
#include <gkg-dmri-simulator-motion/MotionModel.h>
#include <gkg-dmri-simulator-particle-membrane-interaction/InteractionLayer.h>


namespace gkg
{


class SceneModeler;


class Membrane : public EvolvingMesh
{

  public:

    Membrane( RCPointer< SceneModeler > sceneModeler,
              RCPointer< MeshMap< int32_t, float, 3U > > mesh,
              RCPointer< VertexEvolutionFunction > vertexEvolutionFunction,
              RCPointer< ParticleToMembraneInteraction >
                                                  particleToMembraneInteraction,
              RCPointer< MotionModel > motionModel,
              RCPointer< InteractionLayer > interactionLayer,
              float permeability,
              float radiusOfInfluence,
              bool storeVertexTrajectories );
    virtual ~Membrane();

    RCPointer< ParticleToMembraneInteraction >
                                       getParticleToMembraneInteraction() const;
    RCPointer< MotionModel > getMotionModel() const;
    RCPointer< InteractionLayer > getInteractionLayer() const;
    float getPermeability() const;

  protected:

    RCPointer< ParticleToMembraneInteraction > _particleToMembraneInteraction;
    RCPointer< MotionModel > _motionModel;
    RCPointer< InteractionLayer > _interactionLayer;
    float _permeability;

};


}


#endif

