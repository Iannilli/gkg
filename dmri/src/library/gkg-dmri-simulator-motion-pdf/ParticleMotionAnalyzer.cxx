#include <gkg-dmri-simulator-motion-pdf/ParticleMotionAnalyzer.h>
#include <gkg-processing-algobase/Math.h>
#include <gkg-processing-container/MeshMap_i.h>
#include <gkg-processing-container/Volume_i.h>
#include <gkg-processing-mesh/ConvexHull_i.h>
#include <gkg-processing-mesh/MeshScaler_i.h>
#include <gkg-processing-mesh/MeshTransformer_i.h>
#include <gkg-processing-mesh/MeshAccumulator_i.h>
#include <gkg-processing-transform/Translation3d.h>
#include <gkg-communication-sysinfo/File.h>
#include <gkg-core-io/Writer_i.h>
#include <gkg-core-exception/Exception.h>
#include <algorithm>
#include <cmath>


#define ARC_TO_DEGREE 180 / M_PI


gkg::ParticleMotionAnalyzer::ParticleMotionAnalyzer(
                                   const gkg::BoundingBox< float >& boundingBox,
                                   const gkg::Vector3d< int32_t >& size,
                                   int32_t stepCount )
                            : gkg::RCObject(),
                              _boundingBox( boundingBox ),
                              _size( size ),
                              _stepCount( stepCount ),
                              _bresenhamAlgorithm( boundingBox, size ),
                              _volumeAllocated( false ),
                              _particleCountVolumeAllocated( false )
{

  try
  {

    // processing the resolution
    _resolution.x = ( double )( ( boundingBox.getUpperX() -
                                  boundingBox.getLowerX() ) / size.x );
    _resolution.y = ( double )( ( boundingBox.getUpperY() -
                                  boundingBox.getLowerY() ) / size.y );
    _resolution.z = ( double )( ( boundingBox.getUpperZ() -
                                  boundingBox.getLowerZ() ) / size.z );

  }
  GKG_CATCH( "gkg::ParticleMotionAnalyzer::ParticleMotionAnalyzer( "
             "const gkg::BoundingBox< float >& boundingBox, "
             "const gkg::Vector3d< int32_t >& size, "
             "int32_t stepCount, "
             "int32_t temporalSubSamplingCount )" );

}


gkg::ParticleMotionAnalyzer::~ParticleMotionAnalyzer()
{
}


gkg::Vector3d< int32_t > gkg::ParticleMotionAnalyzer::getCacheVoxel(
                                     const gkg::Vector3d< float >& point ) const
{

  try
  {

    gkg::Vector3d< int32_t > voxel;
    _bresenhamAlgorithm.getCacheVoxel( point, voxel );

    return voxel;

  }
  GKG_CATCH( "gkg::Vector3d< int32_t > "
             "gkg::ParticleMotionAnalyzer::getCacheVoxel( "
             "const gkg::Vector3d< float >& point ) const" );

}


void gkg::ParticleMotionAnalyzer::setPdfOrientationSet(
                          gkg::RCPointer< gkg::OrientationSet > orientationSet )
{

  try
  {

    _orientationSet = orientationSet;

  }
  GKG_CATCH( "void gkg::ParticleMotionAnalyzer::setPdfOrientationSet( "
             "gkg::RCPointer< gkg::OrientationSet > orientationSet )" );

}


void gkg::ParticleMotionAnalyzer::updateVolumes(
                                            const gkg::ParticleMap& particleMap,
                                            float lowerLimit,
                                            float upperLimit,
                                            float apertureAngleLimit )
{

  try
  {

    // allocating volumes
    if ( !_volumeAllocated )
    {

      // sanity check
      if ( _orientationSet.isNull() )
      {

        throw std::runtime_error( "undefined PDF orientation set" );

      }

      int32_t orientationCount = _orientationSet->getCount();

      _meanDisplacementVolume.reallocate( _size, orientationCount );
      _meanDisplacementVolume.fill( 0.0 );

      _pdfVolume.reallocate( _size, orientationCount );
      _pdfVolume.fill( 0 );

      _particleCountPerOrientation.reallocate( _size, orientationCount );
      _particleCountPerOrientation.fill( 0 );

      _volumeAllocated = true;

    }

    // looping over particles voxel(s)
    gkg::Vector3d< float > position;
    gkg::Vector3d< int32_t > voxel;
    gkg::Vector3d< float > displacement;
    float distance = 0.0;
    float vectorAngle = 0.0;
    int32_t nearestOrientationIndex;
    int32_t nearestSymmetricalOrientationIndex;

    int32_t particleCount = particleMap.getParticleCount();
    int32_t p = 0;
    for ( p = 0; p < particleCount; p++ )
    {

      // getting the position at the given step count
      position = particleMap.getParticle( p )->getPositionAt( _stepCount );

      displacement = position -
                     particleMap.getParticle( p )->getStartingPosition();
      distance = displacement.getNorm();

      // getting orientation indices
      nearestOrientationIndex =
        _orientationSet->getNearestOrientationIndex( displacement.normalize() );

      nearestSymmetricalOrientationIndex =
        _orientationSet->getNearestSymmetricalOrientationIndex(
                                                      nearestOrientationIndex );

      // processing vector angle if required
      if ( ( apertureAngleLimit >= 0.0 ) &&
           ( apertureAngleLimit <= 90.0 ) )
      {

        gkg::Vector3d< float > orientation =
          _orientationSet->getOrientation( nearestOrientationIndex );

        vectorAngle = gkg::getVectorAngles( displacement, orientation ) *
                      ARC_TO_DEGREE;

      }

      // processing volumes
      _bresenhamAlgorithm.getCacheVoxel( position, voxel );

      _meanDisplacementVolume( voxel,
                               nearestOrientationIndex ) += distance;
      _meanDisplacementVolume( voxel,
                               nearestSymmetricalOrientationIndex ) += distance;

      if ( ( distance >= lowerLimit ) &&
           ( distance <= upperLimit ) &&
           ( vectorAngle <= apertureAngleLimit ) )
      {

        ++ _pdfVolume( voxel, nearestOrientationIndex );
        ++ _pdfVolume( voxel, nearestSymmetricalOrientationIndex );

      }

      ++ _particleCountPerOrientation( voxel,
                                       nearestOrientationIndex );
      ++ _particleCountPerOrientation( voxel,
                                       nearestSymmetricalOrientationIndex );

    }

  }
  GKG_CATCH( "void gkg::ParticleMotionAnalyzer::updateVolumes( "
             "const gkg::ParticleMap& particleMap, "
             "float lowerLimit, "
             "float UpperLimit, "
             "float apertureAngleLimit )" );

}


void gkg::ParticleMotionAnalyzer::updateParticleCountVolume(
                                            const gkg::ParticleMap& particleMap,
                                            int32_t temporalSubSamplingCount )
{

  try
  {

    // collecting particle count
    int32_t particleCount = particleMap.getParticleCount();

    // collecting subsampled trajectories
    gkg::Curve3dMap< float > subSampledTrajectories;

    getParticleTrajectories( particleMap,
                             _boundingBox,
                             particleCount,
                             temporalSubSamplingCount,
                             subSampledTrajectories );

    // collecting the time point count
    int32_t rank = subSampledTrajectories.getCurve3d( 0 ).getPointCount();

    // allocating particle count volume if required
    if ( !_particleCountVolumeAllocated )
    {

      _particleCountVolume.reallocate( _size, rank );
      _particleCountVolume.setResolution( _resolution );
      _particleCountVolume.fill( 0 );

      _particleCountVolumeAllocated = true;

    }

    // updating particle count volume
    gkg::Vector3d< int32_t > voxel;
    int32_t p, r;
    for ( p = 0; p < particleCount; p++ )
    {

      const gkg::LightCurve3d< float >& 
        trajectory = subSampledTrajectories.getCurve3d( p );

      for ( r = 0; r < rank; r++ )
      {

        voxel = getCacheVoxel( trajectory.getPoint( r ) );

        ++ _particleCountVolume( voxel, r );

      }

    }

  }
  GKG_CATCH( "void gkg::ParticleMotionAnalyzer::updateParticleCountVolume( "
             "const gkg::ParticleMap& particleMap, "
             "int32_t temporalSubSamplingCount )" );

}


void gkg::ParticleMotionAnalyzer::getMeanDisplacementVolume(
                                                       Volume< float >& volume )
{

  try
  {

    // allocating the output volume
    int32_t orientationCount = _orientationSet->getCount();
    volume.reallocate( _size, orientationCount );
    volume.setResolution( _resolution );
    volume.fill( 0.0 );

    // looping over voxel(s) and orientations
    gkg::Vector3d< int32_t > voxel;
    int32_t o = 0;
    for ( voxel.z = 0; voxel.z < _size.z; voxel.z++ )
    {

      for ( voxel.y = 0; voxel.y < _size.y; voxel.y++ )
      {

        for ( voxel.x = 0; voxel.x < _size.x; voxel.x++ )
        {

          for ( o = 0; o < orientationCount; o++ )
          {

            volume( voxel, o ) = _meanDisplacementVolume( voxel, o ) /
                              ( float )_particleCountPerOrientation( voxel, o );

          }

        }

      }

    }

  }
  GKG_CATCH( "void gkg::ParticleMotionAnalyzer::getMeanDisplacementVolume( "
             "Volume< float >& volume )" );

}


void gkg::ParticleMotionAnalyzer::getDiffusionPdfVolume(
                                                     gkg::Volume< float >& pdf )
{

  try
  {

    // allocating and preparing the output volume
    int32_t orientationCount = _orientationSet->getCount();
    pdf.reallocate( _size, orientationCount );
    pdf.setResolution( _resolution );
    pdf.fill( 0.0 );

    // looping over voxel and orientation
    gkg::Vector3d< int32_t > voxel;
    int32_t totalParticleCount = 0;
    int32_t o = 0;
    for ( voxel.z = 0; voxel.z < _size.z; voxel.z++ )
    {

      for ( voxel.y = 0; voxel.y < _size.y; voxel.y++ )
      {

        for ( voxel.x = 0; voxel.x < _size.x; voxel.x++ )
        {

          // reset total particle count
          totalParticleCount = 0;

          for ( o = 0; o < orientationCount; o++ )
          {

            // getting total particle count
            totalParticleCount += _particleCountPerOrientation( voxel, o );

          }
          totalParticleCount /= 2;

          for ( o = 0; o < orientationCount; o++ )
          {

            // processing pdf
            pdf( voxel, o ) = ( float )_pdfVolume( voxel, o ) /
                              ( float )totalParticleCount;

          }

        }

      }

    }

  }
  GKG_CATCH( "void gkg::ParticleMotionAnalyzer::getDiffusionPdfVolumesAndMesh( "
             "std::string pdfMeshesFileName, "
             "gkg::Volume< float >& pdf, "
             "gkg::MeshMap< int32_t, float, 3U >& pdfMeshes )" );

}


void gkg::ParticleMotionAnalyzer::getDiffusionPdfMeshes(
                                 std::string pdfMeshesFileName,
                                 gkg::MeshMap< int32_t, float ,3U >& pdfMeshes )
{

  try
  {

    // getting the orientation count
    int32_t orientationCount = _orientationSet->getCount();

    // processing convex hull of orientation set
    gkg::MeshMap< int32_t, float, 3U > orientationMeshMap;
    ConvexHull::getInstance().addConvexHull( _orientationSet->getOrientations(),
                                             0,
                                             orientationMeshMap );

    int32_t vertexCount = orientationMeshMap.vertices.getSiteCount( 0 );
    int32_t polygonCount = orientationMeshMap.polygons.getPolygonCount( 0 );

    // allocating textures and palettes
    std::vector< float > red( vertexCount );
    std::vector< float > green( vertexCount );
    std::vector< float > blue( vertexCount );
    int32_t v = 0;
    for ( v = 0; v < vertexCount; v++ )
    {

      red[ v ] = std::fabs( _orientationSet->getOrientation( v ).x ) * 255.0;
      green[ v ] = std::fabs( _orientationSet->getOrientation( v ).y ) * 255.0;
      blue[ v ] = std::fabs( _orientationSet->getOrientation( v ).z ) * 255.0;

      if ( red[ v ] > 255.0 )
      {

        red[ v ] = 255.0;

      }
      if ( green[ v ] > 255.0 )
      {

        green[ v ] = 255.0;

      }
      if ( blue[ v ] > 255.0 )
      {

        blue[ v ] = 255.0;

      }

    }

    gkg::TextureMap< float > redTextureMap;
    gkg::TextureMap< float > greenTextureMap;
    gkg::TextureMap< float > blueTextureMap;
    std::vector< std::string > palettes( 3 );
    palettes[ 0 ] = "multitex-geom-red-mask";
    palettes[ 1 ] = "multitex-geom-green-mask";
    palettes[ 2 ] = "multitex-geom-blue-mask";
    redTextureMap.getHeader().addAttribute( "palette", palettes );

    // preparing mesh accumulator
    gkg::MeshAccumulator< int32_t, float, 3U > meshAccumulator;
    int32_t siteCount = _size.x * _size.y * _size.z;
    meshAccumulator.reserve( 0,
                             vertexCount * siteCount,
                             polygonCount * siteCount );

    // preparing translation & mesh scaler and transformer
    gkg::Translation3d< float > translation;
    gkg::MeshScaler< int32_t, float, 3U > meshScaler;
    gkg::MeshTransformer< int32_t, float, 3U > meshTransformer;

    // getting the minimum resolution
    float scalingFactor = 0.5 * ( float )( std::min( _resolution.x,
                                           std::min( _resolution.y,
                                                     _resolution.z ) ) );

    // looping over voxel and orientation
    std::vector< float > scaling( orientationCount );
    std::vector< float > pdf;
    gkg::Vector3d< int32_t > voxel;
    float maxPdf = 0.0;
    int32_t totalParticleCount = 0;
    int32_t o = 0;
    for ( voxel.z = 0; voxel.z < _size.z; voxel.z++ )
    {

      for ( voxel.y = 0; voxel.y < _size.y; voxel.y++ )
      {

        for ( voxel.x = 0; voxel.x < _size.x; voxel.x++ )
        {

          // reset pdf and total particle count
          pdf.clear();
          pdf.resize( orientationCount );
          maxPdf = 0.0;
          totalParticleCount = 0;

          for ( o = 0; o < orientationCount; o++ )
          {

            // getting total particle count
            totalParticleCount += _particleCountPerOrientation( voxel, o );

          }
          totalParticleCount /= 2;

          for ( o = 0; o < orientationCount; o++ )
          {

            // processing pdf
            pdf[ o ] = ( float )_pdfVolume( voxel, o ) /
                       ( float )totalParticleCount;
            maxPdf = std::max( maxPdf, pdf[ o ] );

          }
          for ( o = 0; o < orientationCount; o++ )
          {

            // processing scaling
            scaling[ o ] = scalingFactor * pdf[ o ] / maxPdf ;

          }

          // processing 3D transform
          translation.setDirectTranslation(
                                    ( float )voxel.x * ( float )_resolution.x,
                                    ( float )voxel.y * ( float )_resolution.y,
                                    ( float )voxel.z * ( float )_resolution.z );

          // processing local scaled mesh map
          gkg::MeshMap< int32_t, float, 3U > localMeshMap( orientationMeshMap );
          meshScaler.scale( localMeshMap, 0, scaling,
                            localMeshMap );       
          meshTransformer.transform( localMeshMap, translation,
                                     localMeshMap );

          // accumulating local mesh map
          meshAccumulator.add( localMeshMap );

          // adding red/green/blue texture(s)
          redTextureMap.addTextures( red );
          greenTextureMap.addTextures( green );
          blueTextureMap.addTextures( blue );

        }

      }

    }

    // processing output mesh map
    pdfMeshes.add( meshAccumulator );

    // exporting the texture maps to the disk
    std::vector< std::string > textureNames( 3 );
    textureNames[ 0 ] = "red";
    textureNames[ 1 ] = "green";
    textureNames[ 2 ] = "blue";

    std::vector< std::string > textureFileNames( 3 );
    textureFileNames[ 0 ] = gkg::File( pdfMeshesFileName ).getBaseName() +
                            ".red";
    textureFileNames[ 1 ] = gkg::File( pdfMeshesFileName ).getBaseName() +
                            ".green";
    textureFileNames[ 2 ] = gkg::File( pdfMeshesFileName ).getBaseName() +
                            ".blue";

    // adding attributes
    pdfMeshes.getHeader().addAttribute( "texture_names", textureNames );
    pdfMeshes.getHeader().addAttribute( "texture_filenames", textureFileNames );

    // writing the data
    gkg::Writer::getInstance().write( pdfMeshesFileName + ".red",
                                      redTextureMap,
                                      false,
                                      "aimsmesh" );
    gkg::Writer::getInstance().write( pdfMeshesFileName + ".green",
                                      greenTextureMap,
                                      false,
                                      "aimsmesh" );
    gkg::Writer::getInstance().write( pdfMeshesFileName + ".blue",
                                      blueTextureMap,
                                      false,
                                      "aimsmesh" );

  }
  GKG_CATCH( "void gkg::ParticleMotionAnalyzer::getDiffusionPdfMeshes( "
             "std::string pdfMeshesFileName, "
             "gkg::MeshMap< int32_t, float ,3U >& pdfMeshes )" );

}


void gkg::ParticleMotionAnalyzer::getParticleDistributionVolume(
                                                gkg::Volume< int32_t >& volume )
{

  try
  {

    volume.setResolution( _resolution );
    volume = _particleCountVolume;  

  }
  GKG_CATCH( "void "
             "gkg::ParticleMotionAnalyzer::getParticleDistributionVolume( "
             "gkg::Volume< int32_t >& volume )" );

}


void gkg::ParticleMotionAnalyzer::getBoundingBoxOutline(
                                  const gkg::BoundingBox< float >& boundingBox,
                                  gkg::Curve3dMap< float >& boundingBoxOutline )
{

  try
  {

    // allocating 3d curves
    std::vector< gkg::LightCurve3d< float > > curve3ds( 6 );

    // collecting the lower and upper points of the bounding box
    gkg::Vector3d< float > lowerPoint( boundingBox.getLowerX(),
                                       boundingBox.getLowerY(),
                                       boundingBox.getLowerZ() );
    gkg::Vector3d< float > upperPoint( boundingBox.getUpperX(),
                                       boundingBox.getUpperY(),
                                       boundingBox.getUpperZ() );

    // allocating a vector of vertices for the lower and upper planes
    std::vector< gkg::Vector3d< float > > plane( 5 );

    // processing the lower plane
    plane[ 0 ] = gkg::Vector3d< float >( lowerPoint.x,
                                         lowerPoint.y,
                                         lowerPoint.z );
    plane[ 1 ] = gkg::Vector3d< float >( upperPoint.x,
                                         lowerPoint.y,
                                         lowerPoint.z );
    plane[ 2 ] = gkg::Vector3d< float >( upperPoint.x,
                                         upperPoint.y,
                                         lowerPoint.z );
    plane[ 3 ] = gkg::Vector3d< float >( lowerPoint.x,
                                         upperPoint.y,
                                         lowerPoint.z );
    plane[ 4 ] = gkg::Vector3d< float >( lowerPoint.x,
                                         lowerPoint.y,
                                         lowerPoint.z );
    curve3ds[ 0 ] = gkg::LightCurve3d< float >( plane );

    // processing the upper plane
    plane[ 0 ] = gkg::Vector3d< float >( lowerPoint.x,
                                         lowerPoint.y,
                                         upperPoint.z );
    plane[ 1 ] = gkg::Vector3d< float >( upperPoint.x,
                                         lowerPoint.y,
                                         upperPoint.z );
    plane[ 2 ] = gkg::Vector3d< float >( upperPoint.x,
                                         upperPoint.y,
                                         upperPoint.z );
    plane[ 3 ] = gkg::Vector3d< float >( lowerPoint.x,
                                         upperPoint.y,
                                         upperPoint.z );
    plane[ 4 ] = gkg::Vector3d< float >( lowerPoint.x,
                                         lowerPoint.y,
                                         upperPoint.z );
    curve3ds[ 1 ] = gkg::LightCurve3d< float >( plane );

    // allocating a vector for the lower and upper segments
    std::vector< gkg::Vector3d< float > > segment( 2 );

    // collecting the lower vertices to the upper vertices
    // segment 1
    segment[ 0 ] = gkg::Vector3d< float >( lowerPoint.x,
                                           lowerPoint.y,
                                           lowerPoint.z );
    segment[ 1 ] = gkg::Vector3d< float >( lowerPoint.x,
                                           lowerPoint.y,
                                           upperPoint.z );
    curve3ds[ 2 ] = gkg::LightCurve3d< float >( segment );

    // segment 2
    segment[ 0 ] = gkg::Vector3d< float >( lowerPoint.x,
                                           upperPoint.y,
                                           lowerPoint.z );
    segment[ 1 ] = gkg::Vector3d< float >( lowerPoint.x,
                                           upperPoint.y,
                                           upperPoint.z );
    curve3ds[ 3 ] = gkg::LightCurve3d< float >( segment );

    // segment 3
    segment[ 0 ] = gkg::Vector3d< float >( upperPoint.x,
                                           lowerPoint.y,
                                           lowerPoint.z );
    segment[ 1 ] = gkg::Vector3d< float >( upperPoint.x,
                                           lowerPoint.y,
                                           upperPoint.z );
    curve3ds[ 4 ] = gkg::LightCurve3d< float >( segment );

    // segment 4
    segment[ 0 ] = gkg::Vector3d< float >( upperPoint.x,
                                           upperPoint.y,
                                           lowerPoint.z );
    segment[ 1 ] = gkg::Vector3d< float >( upperPoint.x,
                                           upperPoint.y,
                                           upperPoint.z );
    curve3ds[ 5 ] = gkg::LightCurve3d< float >( segment );

    // connecting the vertices
    boundingBoxOutline.addCurve3ds( curve3ds );

  }
  GKG_CATCH( "void gkg::ParticleMotionAnalyzer::getBoundingBoxOutline( "
             "const gkg::BoundingBox< float >& boundingBox, "
             "gkg::Curve3dMap< float >& boundingBoxOutline )" );

}


void gkg::ParticleMotionAnalyzer::getParticleTrajectories(
                                   const gkg::ParticleMap& particleMap,
                                   const gkg::BoundingBox< float >& boundingBox,
                                   int32_t wantedParticleCount,
                                   int32_t temporalSubSamplingCount,
                                   gkg::Curve3dMap< float >& trajectories )
{

  try
  {

    // sanity check
    int32_t globalParticleCount = particleMap.getParticleCount();
    if ( wantedParticleCount > globalParticleCount )
    {

      std::cerr << "fewer particles than wanted " << std::flush;

    }

    // allocating a vector of integers corresponding to the indices of particles
    // contained inside the bounding box
    std::vector< int32_t > insideBBoxParticleIndices;

    // looping over particles
    int32_t p;
    for ( p = 0; p < globalParticleCount; p++ )
    {

      if ( ( boundingBox.contains( 
               particleMap.getParticle( p )->getStartingPosition() ) ) ||
           ( boundingBox.contains(
               particleMap.getParticle( p )->getEndingPosition() ) ) )
      {

        insideBBoxParticleIndices.push_back( p );

      }

    }

    // sanity check
    int32_t insideBBoxParticleCount =
      ( int32_t )insideBBoxParticleIndices.size();
    if ( wantedParticleCount > insideBBoxParticleCount )
    {

      std::cerr << "fewer particles inside the bounding box than wanted"
                << std::flush;

    }
    int32_t realParticleCount = std::min( wantedParticleCount,
                                          insideBBoxParticleCount );

    // getting 3D curves for the wanted particles
    std::vector< gkg::LightCurve3d< float > > curve3ds( realParticleCount );
    gkg::LightCurve3d< float > subSampledTrajectory;
    int32_t particleIndex;
    int32_t s = 0;
    for ( p = 0; p < realParticleCount; p++ )
    {

      particleIndex = insideBBoxParticleIndices[ p ];
      const gkg::LightCurve3d< float >& 
        trajectory = particleMap.getParticle( particleIndex )->getTrajectory();
      for ( s = 0; s < ( _stepCount + 1 ); s++ )
      {

        if ( ( s % temporalSubSamplingCount == 0 ) ||
             ( s == _stepCount ) )
        {

          subSampledTrajectory.addPoint( trajectory.getPoint( s ) );

        }

      }
      curve3ds[ p ] = subSampledTrajectory;

      subSampledTrajectory.clear();

    }
    trajectories.addCurve3ds( curve3ds );

  }
  GKG_CATCH( "void gkg::ParticleMotionAnalyzer::getParticleTrajectories( "
             "const gkg::ParticleMap& particleMap, "
             "const gkg::BoundingBox< float >& boundingBox, "
             "int32_t wantedParticleCount, "
             "int32_t temporalSubSamplingCount, "
             "gkg::Curve3dMap< float >& trajectories )" );

}


void gkg::ParticleMotionAnalyzer::getParticleMeshes(
                            const gkg::ParticleMap& particleMap,
                            const gkg::BoundingBox< float >& boundingBox,
                            int32_t wantedParticleCount,
                            float sphereRadius,
                            int32_t sphereVertexCount,
                            int32_t temporalSubSamplingCount,
                            gkg::MeshMap< int32_t, float, 3U >& particleMeshes )
{

  try
  {

    // collecting all the trajectories
    gkg::Curve3dMap< float > trajectories;

    getParticleTrajectories( particleMap,
                             boundingBox,
                             wantedParticleCount,
                             temporalSubSamplingCount,
                             trajectories );

    int32_t realParticleCount = trajectories.getCurve3dCount();
    int32_t rank = trajectories.getCurve3d( 0 ).getPointCount();

    // building the mesh of a sphere
    gkg::OrientationSet orientationSet(
              gkg::ElectrostaticOrientationSet( sphereVertexCount / 2
                                               ).getSymmetricalOrientations() );

    std::vector< gkg::MeshMap< int32_t, float, 3U > > sphereMeshMap( rank );    
    int32_t r = 0;
    for ( r = 0; r < rank; r++ )
    {

      gkg::ConvexHull::getInstance().addConvexHull( 
                                               orientationSet.getOrientations(),
                                               r,
                                               sphereMeshMap[ r ] );

    }
    int32_t sphereVertexCount = 
      sphereMeshMap[ 0 ].vertices.getSiteCount( 0 );
    int32_t spherePolygonCount = 
      sphereMeshMap[ 0 ].polygons.getPolygonCount( 0 );

    // preparing mesh accumulator
    gkg::MeshAccumulator< int32_t, float, 3U > meshAccumulator;
    for ( r = 0; r < rank; r++ )
    {

      meshAccumulator.reserve( r,
                               sphereVertexCount * realParticleCount,
                               spherePolygonCount * realParticleCount );

    }

    // preparing translation
    gkg::Translation3d< float > translation;

    // preparing mesh scaler and transformer
    gkg::MeshScaler< int32_t, float, 3U > meshScaler;
    gkg::MeshTransformer< int32_t, float, 3U > meshTransformer;

    // creating the scale vector
    std::vector< float > scaling( sphereVertexCount, sphereRadius );

    // adding sphere meshes
    int32_t p = 0;
    for ( r = 0; r < rank; r++ )
    {

      for ( p = 0; p < realParticleCount; p++ )
      {

        // collecting the position of the particle p at step s
        const gkg::Vector3d< float >&
          position = trajectories.getCurve3d( p ).getPoint( r );

        // processing 3D transform
        translation.setDirectTranslation( position.x,
                                          position.y,
                                          position.z );

        // processing local scaled mesh map
        gkg::MeshMap< int32_t, float, 3U > localMeshMap( sphereMeshMap[ r ] );
        meshScaler.scale( localMeshMap, r, scaling, localMeshMap );
        meshTransformer.transform( localMeshMap, translation, localMeshMap );

        // accumulating local mesh map
        meshAccumulator.add( localMeshMap );

      }

    }

    particleMeshes.add( meshAccumulator );

  }
  GKG_CATCH( "void gkg::ParticleMotionAnalyzer::getParticleMeshes( "
             "const gkg::ParticleMap& particleMap, "
             "const gkg::BoundingBox< float >& boundingBox, "
             "int32_t wantedParticleCount, "
             "float sphereRadius, "
             "int32_t sphereVertexCount, "
             "int32_t temporalSubSamplingCount, "
             "gkg::MeshMap< int32_t, float, 3U >& particleMeshes )" );

}

