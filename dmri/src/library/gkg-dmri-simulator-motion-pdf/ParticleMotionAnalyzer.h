#ifndef _gkg_dmri_simulator_motion_pdf_ParticleMotionAnalyzer_h
#define _gkg_dmri_simulator_motion_pdf_ParticleMotionAnalyzer_h

#include <gkg-core-pattern/RCPointer.h>
#include <gkg-dmri-simulator-particle/ParticleMap.h>
#include <gkg-processing-mesh/BresenhamAlgorithm.h>
#include <gkg-processing-container/BoundingBox.h>
#include <gkg-processing-container/Volume.h>
#include <gkg-processing-container/Curve3dMap.h>
#include <gkg-processing-container/MeshMap.h>
#include <gkg-processing-coordinates/ElectrostaticOrientationSet.h>


namespace gkg
{


class ParticleMotionAnalyzer : public RCObject
{

  public:

    ParticleMotionAnalyzer( const BoundingBox< float >& boundingBox,
                            const Vector3d< int32_t >& size,
                            int32_t stepCount );
    virtual ~ParticleMotionAnalyzer();

    Vector3d< int32_t > getCacheVoxel( const Vector3d< float >& point ) const;
    void setPdfOrientationSet( RCPointer< OrientationSet > orientationSet );

    // method to update volume information
    void updateVolumes( const ParticleMap& particleMap,
                        float lowerLimit,
                        float upperLimit,
                        float apertureAngleLimit );

    void updateParticleCountVolume( const ParticleMap& particleMap,
                                    int32_t temporalSubSamplingCount );

    // methods to get diffusion functions & renderings
    void getMeanDisplacementVolume( Volume< float >& volume );
    void getDiffusionPdfVolume( Volume< float >& pdf );
    void getDiffusionPdfMeshes( std::string pdfMeshesFileName,
                                MeshMap< int32_t, float ,3U >& pdfMeshes );

    // methods to get particle distribution evolution
    void getParticleDistributionVolume( Volume< int32_t >& volume );

    // methods to get bounding box outline
    void getBoundingBoxOutline( const BoundingBox< float >& boundingBox,
                                Curve3dMap< float >& boundingBoxOutline );

    // methods to get partilce renderings
    void getParticleTrajectories( const ParticleMap& particleMap,
                                  const BoundingBox< float >& boundingBox,
                                  int32_t wantedParticleCount,
                                  int32_t temporalSubSamplingCount,
                                  Curve3dMap< float >& trajectories );

    void getParticleMeshes( const ParticleMap& particleMap,
                            const BoundingBox< float >& boundingBox,
                            int32_t wantedParticleCount,
                            float sphereRadius,
                            int32_t sphereVertexCount,
                            int32_t temporalSubSamplingCount,
                            MeshMap< int32_t, float, 3U >& particleMeshes );

  protected:

    BoundingBox< float > _boundingBox;
    Vector3d< int32_t > _size;
    Vector3d< double > _resolution;
    int32_t _stepCount;
    RCPointer< OrientationSet > _orientationSet;
    BresenhamAlgorithm _bresenhamAlgorithm;
    bool _volumeAllocated;
    bool _particleCountVolumeAllocated;

    Volume< float > _meanDisplacementVolume;
    Volume< int32_t > _pdfVolume;
    Volume< int32_t > _particleCountPerOrientation;
    Volume< int32_t > _particleCountVolume;

};


}


#endif
