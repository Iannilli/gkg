#ifndef _gkg_dmri_simulator_motion_MotionModel_h_
#define _gkg_dmri_simulator_motion_MotionModel_h_


#include <gkg-core-pattern/RCPointer.h>
#include <gkg-processing-coordinates/Vector3d.h>


namespace gkg
{


class MotionModel : public RCObject
{

  public:

    virtual ~MotionModel();

    virtual float getDisplacement( float timeStep ) const = 0;
    virtual void getNewPosition( float displacement,
                                 const Vector3d< float >& currentPosition,
                                 Vector3d< float >& newPosition ) const = 0;

  protected:

    MotionModel();

};


}


#endif
