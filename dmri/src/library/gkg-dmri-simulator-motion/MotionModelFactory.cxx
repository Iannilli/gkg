#include <gkg-dmri-simulator-motion/MotionModelFactory.h>
#include <gkg-dmri-simulator-motion/RandomWalkModel.h>
#include <gkg-core-exception/Exception.h>


gkg::MotionModelFactory::MotionModelFactory()
{
}


gkg::MotionModelFactory::~MotionModelFactory()
{
}


gkg::RCPointer< gkg::MotionModel >
gkg::MotionModelFactory::getRandomWalkModel( float mobility )
{

  try
  {

    gkg::RCPointer< gkg::MotionModel >
      randomWalkModel( new gkg::RandomWalkModel( mobility ) );

    return randomWalkModel;

  }
  GKG_CATCH( "gkg::RCPointer< gkg::MotionModel > "
             "gkg::MotionModelFactory::getRandomWalkModel( "
             "float mobility, "
             "int32_t discreteOrientationCount )" );

}

