#ifndef _gkg_dmri_simulator_motion_MotionModelFactory_h_
#define _gkg_dmri_simulator_motion_MotionModelFactory_h_


#include <gkg-core-pattern/Singleton.h>
#include <gkg-dmri-simulator-motion/MotionModel.h>


namespace gkg
{


class MotionModelFactory : public Singleton< MotionModelFactory >
{

  public:

    ~MotionModelFactory();

    RCPointer< MotionModel > getRandomWalkModel( float mobility );

  protected:

    friend class Singleton< MotionModelFactory >;

    MotionModelFactory();

};


}


#endif

