#include <gkg-dmri-simulator-motion/RandomWalkModel.h>
#include <gkg-processing-numericalanalysis/NumericalAnalysisSelector.h>
#include <gkg-processing-numericalanalysis/RandomGenerator.h>
#include <gkg-core-exception/Exception.h>


gkg::RandomWalkModel::RandomWalkModel( float mobility )
                     : gkg::MotionModel(),
                       _mobility( mobility ),
                       _sixTimesMobility( 6.0 * ( double )_mobility )
{
}


gkg::RandomWalkModel::~RandomWalkModel()
{
}


float gkg::RandomWalkModel::getDisplacement( float timeStep ) const
{

  try
  {

    return ( float )std::sqrt( _sixTimesMobility * ( double )timeStep );

  }
  GKG_CATCH( "float gkg::RandomWalkModel::getDisplacement( "
             "float timeStep ) const" );

}


void gkg::RandomWalkModel::getNewPosition(
                                  float displacement,
                                  const gkg::Vector3d< float >& currentPosition,
                                  gkg::Vector3d< float >& newPosition ) const
{

  try
  {

    // getting numerical analysis implementation factory
    static gkg::NumericalAnalysisImplementationFactory*
      factory = gkg::NumericalAnalysisSelector::getInstance().
                                                     getImplementationFactory();
    static gkg::RandomGenerator randomGenerator( gkg::RandomGenerator::Taus );

    gkg::Vector3d< float > direction( ( float )factory->getUniformRandomNumber(
                                                randomGenerator, -1.0, +1.0 ),
                                      ( float )factory->getUniformRandomNumber(
                                                randomGenerator, -1.0, +1.0 ),
                                      ( float )factory->getUniformRandomNumber(
                                                randomGenerator, -1.0, +1.0 ) );
    direction.normalize();

    newPosition = currentPosition + direction * displacement;

  }
  GKG_CATCH( "void gkg::RandomWalkModel::getNewPosition( "
             "float displacement, "
             "const gkg::Vector3d< float >& currentPosition, "
             "gkg::Vector3d< float >& newPosition )" );

}

