#ifndef _gkg_dmri_simulator_motion_RandomWalkModel_h_
#define _gkg_dmri_simulator_motion_RandomWalkModel_h_


#include <gkg-dmri-simulator-motion/MotionModel.h>


namespace gkg
{


class RandomWalkModel : public MotionModel
{

  public:

    RandomWalkModel( float mobility );
    virtual ~RandomWalkModel();

    float getDisplacement( float timeStep ) const;
    void getNewPosition( float displacement,
                         const Vector3d< float >& currentPosition,
                         Vector3d< float >& newPosition ) const;

  protected:

    float _mobility;
    double _sixTimesMobility;

};


}


#endif
