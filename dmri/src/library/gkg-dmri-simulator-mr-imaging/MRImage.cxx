#include <gkg-dmri-simulator-mr-imaging/MRImage.h>
#include <gkg-processing-container/Volume_i.h>
#include <gkg-core-exception/Exception.h>


gkg::MRImage::MRImage( const gkg::BoundingBox< float >& boundingBox,
                       const gkg::Vector3d< int32_t >& size,
                       gkg::RCPointer< gkg::NMRSequence > nmrSequence,
                       gkg::RCPointer< gkg::NoiseModel > noiseModel,
                       float S0,
                       float timeStep,
                       float threshold )
             : gkg::RCObject(),
               _boundingBox( boundingBox ),
               _size( size ),
               _nmrSequence( nmrSequence ),
               _noiseModel( noiseModel ),
               _S0( S0 ),
               _timeStep( timeStep ),
               _threshold( threshold ),
               _bresenhamAlgorithm( boundingBox, size ),
               _dwVolumesAllocated( false ),
               _spinCountVolumesAllocated( false )
{

  try
  {

    // processing the resolution
    _resolution.x = ( double )( ( boundingBox.getUpperX() -
                                  boundingBox.getLowerX() ) / size.x );
    _resolution.y = ( double )( ( boundingBox.getUpperY() -
                                  boundingBox.getLowerY() ) / size.y );
    _resolution.z = ( double )( ( boundingBox.getUpperZ() -
                                  boundingBox.getLowerZ() ) / size.z );

    // processing the iteration count at the echo time
    _iterationCount = ( int32_t )( _nmrSequence->getEchoTimeInUs() /
                                   _timeStep ) + 1;

  }
  GKG_CATCH( "gkg::MRImage::MRImage( "
             "const gkg::BoundingBox< float >& boundingBox, "
             "const gkg::Vector3d< int32_t >& size, "
             "gkg::RCPointer< gkg::NMRSequence > nmrSequence, "
             "gkg::RCPointer< gkg::NoiseModel > noiseModel, "
             "float S0, "
             "float timeStep, "
             "float threshold )" );

}


gkg::MRImage::~MRImage()
{
}


const gkg::BoundingBox< float >& gkg::MRImage::getBoundingBox() const
{

  try
  {

    return _boundingBox;

  }
  GKG_CATCH( "const gkg::BoundingBox< float >& "
             "gkg::MRImage::getBoundingBox() const" );

}


const gkg::Vector3d< int32_t >& gkg::MRImage::getSize() const
{

  try
  {

    return _size;

  }
  GKG_CATCH( "const gkg::Vector3d< int32_t >& gkg::MRImage::getSize() const" );

}


gkg::RCPointer< gkg::NMRSequence > gkg::MRImage::getNMRSequence() const
{

  try
  {

    return _nmrSequence;

  }
  GKG_CATCH( "gkg::RCPointer< gkg::NMRSequence > "
             "gkg::MRImage::getNMRSequence() const" );

}


float gkg::MRImage::getTimeStep() const
{

  try
  {

    return _timeStep;

  }
  GKG_CATCH( "float gkg::MRImage::getTimeStep() const" );

}


int32_t gkg::MRImage::getIterationCount() const
{

  try
  {

    return _iterationCount;

  }
  GKG_CATCH( "int32_t gkg::MRImage::getIterationCount() const" );

}


const gkg::Vector3d< double >& gkg::MRImage::getResolution() const
{

  try
  {

    return _resolution;

  }
  GKG_CATCH( "const gkg::Vector3d< double >& "
             "gkg::MRImage::getResolution() const" );

}


gkg::Vector3d< int32_t >
gkg::MRImage::getCacheVoxel( const gkg::Vector3d< float >& point ) const
{

  try
  {

    gkg::Vector3d< int32_t > voxel;
    _bresenhamAlgorithm.getCacheVoxel( point, voxel );

    return voxel;

  }
  GKG_CATCH( "gkg::Vector3d< int32_t > "
             "gkg::MRImage::getCacheVoxel( "
             "const gkg::Vector3d< float >& point ) const" );

}


void gkg::MRImage::addSpins( const gkg::SpinCache& spinCache )
{

  try
  {

    // looping over b-value and update DW volumes
    int32_t g = 0;
    for ( g = 0; g < _nmrSequence->getGradientAmplitudeCount(); g++ )
    {

      updateDWVolumes( spinCache, g );

    }

    // collecting spin count volumes
    updateSpinCountVolume( spinCache );

  }
  GKG_CATCH( "void gkg::MRImage::addSpin( gkg::RCPointer< gkg::Spin > spin )" );

}


void gkg::MRImage::updateDWVolumes( const gkg::SpinCache& spinCache,
                                    int32_t gradientAmplitudeIndex )
{

  try
  {

    // collecting the orientation count
    int32_t orientationCount = _nmrSequence->getOrientationSet()->getCount();

    // allocating volume(s)
    if ( !_dwVolumesAllocated )
    {

      // collecting the gradient amplidute count
      int32_t gradientAmplitudeCount =
                                      _nmrSequence->getGradientAmplitudeCount();

      _dwVolumes.resize( gradientAmplitudeCount );

      int32_t g = 0;
      for ( g = 0; g < gradientAmplitudeCount; g++ )
      {

        _dwVolumes[ g ].reallocate( _size, orientationCount );
        _dwVolumes[ g ].fill( 0.0 );

      }

      if ( spinCache.areStatesStored() )
      {

        _closeToMembraneDwVolumes.resize( gradientAmplitudeCount );
        _farFromMembraneDwVolumes.resize( gradientAmplitudeCount );

        for ( g = 0; g < gradientAmplitudeCount; g++ )
        {

          _closeToMembraneDwVolumes[ g ].reallocate( _size, orientationCount );
          _closeToMembraneDwVolumes[ g ].fill( 0.0 );
          _farFromMembraneDwVolumes[ g ].reallocate( _size, orientationCount );
          _farFromMembraneDwVolumes[ g ].fill( 0.0 );

        }

      }
      _dwVolumesAllocated = true;

    }

    // looping over voxel(s) and orientation(s) for the volume(s)
    gkg::Vector3d< int32_t > voxel;
    int32_t o;
    for ( voxel.z = 0; voxel.z < _size.z; voxel.z++ )
    {

      for ( voxel.y = 0; voxel.y < _size.y; voxel.y++ )
      {

        for ( voxel.x = 0; voxel.x < _size.x; voxel.x++ )
        {

          // obtaining the set of spins from the spin cache
          std::set< gkg::RCPointer< gkg::Spin > >* 
            spins = spinCache.getSpins( voxel );

          // if the set of spins exists and is not empty
          if ( spins != 0 )
          {

            // looping over spins
            float spinPhase = 0.0;
            std::set< gkg::RCPointer< gkg::Spin > >::const_iterator 
              s = spins->begin(),
              se = spins->end();
            while ( s != se )
            {

              for ( o = 0; o < orientationCount; o++ )
              {
              
                // getting the accumulated phase
                spinPhase = 
                  ( *s )->getAccumulatedPhase( gradientAmplitudeIndex, o );

                // updating the complex signal volume
                _dwVolumes[ gradientAmplitudeIndex ]( voxel, o ) +=
                  std::polar( 1.0f, spinPhase );

              }

              if ( spinCache.areStatesStored() )
              {

                float stateFraction =
                  ( *s )->getParticle()->getStateFractionAt( _iterationCount );

                if ( stateFraction >= _threshold )
                {

                  ////// case: the spin belongs to the close-to-membrane volume
                  for ( o = 0; o < orientationCount; o++ )
                  {

                    // getting the accumulated phase
                    spinPhase = 
                      ( *s )->getAccumulatedPhase( gradientAmplitudeIndex, o );

                    // updating the close-to-membrane complex signal volume
                    _closeToMembraneDwVolumes[ gradientAmplitudeIndex ]
                                             ( voxel, o ) +=
                      std::polar( 1.0f, spinPhase );

                  }

                }
                else
                {

                  ////// case: the spin belongs to the far-from-membrane volume
                  for ( o = 0; o < orientationCount; o++ )
                  {

                    // getting the accumulated phase
                    spinPhase =
                      ( *s )->getAccumulatedPhase( gradientAmplitudeIndex, o );

                    // updating the far-from-membrane complex signal volume
                    _farFromMembraneDwVolumes[ gradientAmplitudeIndex ]
                                             ( voxel, o ) +=
                      std::polar( 1.0f, spinPhase );
  
                  }

                }

              }
              ++ s;

            }

          }

        }

      }

    }

  }
  GKG_CATCH( "void gkg::MRImage::updateDWVolumes( "
             "const gkg::SpinCache& spinCache, "
             "int32_t gradientAmplitudeIndex )" );

}


void gkg::MRImage::updateSpinCountVolume( const gkg::SpinCache& spinCache )
{

  try
  {

    if ( !_spinCountVolumesAllocated )
    {

      // allocating spin count volumes
      _spinCountVolume.reallocate( _size );
      _spinCountVolume.fill( 0 );

      if ( spinCache.areStatesStored() )
      {

        _closeToMembraneSpinCountVolume.reallocate( _size );
        _closeToMembraneSpinCountVolume.fill( 0 );
        _farFromMembraneSpinCountVolume.reallocate( _size );
        _farFromMembraneSpinCountVolume.fill( 0 );

      }
      _spinCountVolumesAllocated = true;

    }

    // looping over voxel(s) for the volume(s)
    gkg::Vector3d< int32_t > voxel;
    for ( voxel.z = 0; voxel.z < _size.z; voxel.z++ )
    {

      for ( voxel.y = 0; voxel.y < _size.y; voxel.y++ )
      {

        for ( voxel.x = 0; voxel.x < _size.x; voxel.x++ )
        {

          // obtaining the set of spins from the spin cache
          std::set< gkg::RCPointer< gkg::Spin > >* 
            spins = spinCache.getSpins( voxel );

          // if the set of spins exists and is not empty
          if ( spins != 0 )
          {

            // looping over spins
            std::set< gkg::RCPointer< gkg::Spin > >::const_iterator 
              s = spins->begin(),
              se = spins->end();
            while ( s != se )
            {

              // updating the spin count
              ++ _spinCountVolume( voxel );

              if ( spinCache.areStatesStored() )
              {

                float stateFraction =
                  ( *s )->getParticle()->getStateFractionAt( _iterationCount );

                if ( stateFraction >= _threshold )
                {

                  ////// case: the spin belongs to the close-to-membrane volume
                  ++ _closeToMembraneSpinCountVolume( voxel );

                }
                else
                {

                  ////// case: the spin belongs to the far-from-membrane volume
                  ++ _farFromMembraneSpinCountVolume( voxel );

                }

              }
              ++ s;

            }

          }

        }

      }

    }

  }
  GKG_CATCH( "void gkg::MRImage::updateSpinCountVolume( "
             "const gkg::SpinCache& spinCache )" );

}


void gkg::MRImage::getT2Volume( gkg::Volume< float >& volume,
                                bool addNoise )
{

  try
  {

    // allocating and preparing the output volume
    volume.reallocate( _size );
    volume.setResolution ( _resolution );
    volume.fill( 0 );

    // looping over voxel(s) of the volume
    float noise = 0.0;

    gkg::Vector3d< int32_t > voxel;
    for ( voxel.z = 0; voxel.z < _size.z; voxel.z++ )
    {

      for ( voxel.y = 0; voxel.y < _size.y; voxel.y++ )
      {

        for ( voxel.x = 0; voxel.x < _size.x; voxel.x++ )
        {

          if ( addNoise )
          {

            noise = std::abs( _noiseModel->getNoise() );

          }
          volume( voxel ) = _S0 + noise;

        }

      }

    }

  }
  GKG_CATCH( "void gkg::MRImage::getT2Volume( "
             "gkg::Volume< float >& volume, "
             "bool addNoise )" );

}


void gkg::MRImage::getDWVolume( int32_t gradientAmplitudeIndex,
                                gkg::Volume< float >& volume,
                                bool addNoise )
{

  try
  {

    // collecting the orientation count
    int32_t orientationCount = _nmrSequence->getOrientationSet()->getCount();

    // allocating and preparing the output volume
    volume.reallocate( _size, orientationCount );
    volume.setResolution ( _resolution );
    volume.fill( 0 );

    // looping over voxel(s) of the volume
    std::complex< float > value;
    float signal = 0.0;
    float noise = 0.0;

    gkg::Vector3d< int32_t > voxel;
    int32_t spinCount;
    int32_t o;
    for ( voxel.z = 0; voxel.z < _size.z; voxel.z++ )
    {

      for ( voxel.y = 0; voxel.y < _size.y; voxel.y++ )
      {

        for ( voxel.x = 0; voxel.x < _size.x; voxel.x++ )
        {

          // collecting the spin count
          spinCount = _spinCountVolume( voxel );

          for ( o = 0; o < orientationCount; o++ )
          {

            // collecting the magnitude signal value
            value = _dwVolumes[ gradientAmplitudeIndex ]( voxel, o );
            signal = _S0 * std::abs( value ) / ( float )spinCount;

            if ( addNoise )
            {

              noise = std::abs( _noiseModel->getNoise() );

            }
            volume( voxel, o ) = signal + noise;

          }

        }

      }

    }

  }
  GKG_CATCH( "void gkg::MRImage::getDWVolume( "
             "int32_t gradientAmplitudeIndex, "
             "gkg::Volume< int16_t >& volume, "
             "bool addNoise )" );

}


void gkg::MRImage::getCloseToAndFarFromMembraneCompartmentDWVolume(
                                  int32_t gradientAmplitudeIndex,
                                  gkg::Volume< float >& closeToMembraneVolume,
                                  gkg::Volume< float >& farFromMembraneVolume,
                                  bool addNoise )
{

  try
  {

    // collecting the orientation count
    int32_t orientationCount = _nmrSequence->getOrientationSet()->getCount();

    // allocating and preparing the output volumes
    closeToMembraneVolume.reallocate( _size, orientationCount );
    closeToMembraneVolume.setResolution ( _resolution );
    closeToMembraneVolume.fill( 0 );

    farFromMembraneVolume.reallocate( _size, orientationCount );
    farFromMembraneVolume.setResolution ( _resolution );
    farFromMembraneVolume.fill( 0 );

    // looping over voxel(s) of the volumes
    std::complex< float > value1, value2;
    float signal1, signal2;
    float noise = 0.0;

    gkg::Vector3d< int32_t > voxel;
    int32_t spinCount1, spinCount2;
    int32_t o;
    for ( voxel.z = 0; voxel.z < _size.z; voxel.z++ )
    {

      for ( voxel.y = 0; voxel.y < _size.y; voxel.y++ )
      {

        for ( voxel.x = 0; voxel.x < _size.x; voxel.x++ )
        {

          // collecting the spin counts
          spinCount1 = _closeToMembraneSpinCountVolume( voxel );
          spinCount2 = _farFromMembraneSpinCountVolume( voxel );

          for ( o = 0; o < orientationCount; o++ )
          {

            // collecting the complex signal values
            value1 = _closeToMembraneDwVolumes[ gradientAmplitudeIndex ]
                                              ( voxel, o );
            value2 = _farFromMembraneDwVolumes[ gradientAmplitudeIndex ]
                                              ( voxel, o );

            // collecting the magnitude signal values
            signal1 = _S0 * ( std::abs( value1 ) / ( float )spinCount1 );
            signal2 = _S0 * ( std::abs( value2 ) / ( float )spinCount2 );

            if ( addNoise )
            {

              noise = std::abs( _noiseModel->getNoise() );

            }
            closeToMembraneVolume( voxel, o ) = signal1 + noise;
            farFromMembraneVolume( voxel, o ) = signal2 + noise;

          }

        }

      }

    }

  }
  GKG_CATCH( "void "
             "gkg::MRImage::getCloseToAndFarFromMembraneCompartmentDWVolume( "
             "int32_t gradientAmplitudeIndex, "
             "gkg::Volume< float >& closeToMembraneVolume, "
             "gkg::Volume< float >& farFromMembraneVolume, "
             "bool addNoise )" );

}


void gkg::MRImage::getMaskVolume( gkg::Volume< int16_t >& volume )
{

  try
  {

    // allocating and preparing the output DW volume
    volume.reallocate( _size );
    volume.setResolution ( _resolution );
    volume.fill( 1 );

  }
  GKG_CATCH( "void gkg::MRImage::getMaskVolume( "
             "gkg::Volume< int16_t >& volume ) " );

}

