#ifndef _gkg_dmri_simulator_mr_imaging_MRImage_h_
#define _gkg_dmri_simulator_mr_imaging_MRImage_h_

#include <gkg-processing-container/BoundingBox.h>
#include <gkg-processing-container/Volume.h>
#include <gkg-dmri-simulator-pulse-sequence/NMRSequence.h>
#include <gkg-dmri-simulator-noise/NoiseModel.h>
#include <gkg-dmri-simulator-spin/Spin.h>
#include <gkg-dmri-simulator-spin/SpinCache.h>
#include <gkg-processing-mesh/BresenhamAlgorithm.h>


namespace gkg
{


class MRImage : public RCObject
{

  public:

    MRImage( const BoundingBox< float >& boundingBox,
             const Vector3d< int32_t >& size,
             RCPointer< NMRSequence > nmrSequence,
             RCPointer< NoiseModel > noiseModel,
             float S0,
             float timeStep,
             float threshold );
    virtual ~MRImage();

    const BoundingBox< float >& getBoundingBox() const;
    const Vector3d< int32_t >& getSize() const;
    RCPointer< NMRSequence > getNMRSequence() const;
    float getTimeStep() const;
    int32_t getIterationCount() const;

    const Vector3d< double >& getResolution() const;
    Vector3d< int32_t > getCacheVoxel( const Vector3d< float >& point ) const;

    // methods to add spins
    void addSpins( const SpinCache& spinCache );

    // methods to get image volumes
    void getT2Volume( Volume< float >& volume,
                      bool addNoise = true );

    void getDWVolume( int32_t gradientAmplitudeIndex,
                      Volume< float >& volume,
                      bool addNoise = true );

    void getCloseToAndFarFromMembraneCompartmentDWVolume(
                                  int32_t gradientAmplitudeIndex,
                                  Volume< float >& closeToMembraneVolume,
                                  Volume< float >& farFromMembraneVolume,
                                  bool addNoise = true );

    void getMaskVolume( Volume< int16_t >& volume );

  protected:

    void updateDWVolumes( const SpinCache& spinCache,
                          int32_t gradientAmplitudeIndex );
    void updateSpinCountVolume( const SpinCache& spinCache );

    BoundingBox< float > _boundingBox;
    Vector3d< int32_t > _size;
    Vector3d< double > _resolution;
    RCPointer< NMRSequence > _nmrSequence;
    RCPointer< NoiseModel > _noiseModel;
    float _S0;
    float _timeStep;
    float _threshold;
    int32_t _iterationCount;
    BresenhamAlgorithm _bresenhamAlgorithm;
    bool _dwVolumesAllocated;
    bool _spinCountVolumesAllocated;

    std::vector< Volume< std::complex< float > > > _dwVolumes;
    std::vector< Volume< std::complex< float > > > _closeToMembraneDwVolumes;
    std::vector< Volume< std::complex< float > > > _farFromMembraneDwVolumes;
    Volume< int32_t > _spinCountVolume;
    Volume< int32_t > _closeToMembraneSpinCountVolume;
    Volume< int32_t > _farFromMembraneSpinCountVolume;

};


}


#endif
