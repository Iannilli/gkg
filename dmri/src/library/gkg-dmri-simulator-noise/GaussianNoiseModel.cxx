#include <gkg-dmri-simulator-noise/GaussianNoiseModel.h>
#include <gkg-processing-numericalanalysis/NumericalAnalysisSelector.h>
#include <gkg-processing-numericalanalysis/RandomGenerator.h>
#include <gkg-core-exception/Exception.h>


gkg::GaussianNoiseModel::GaussianNoiseModel( float sigma )
                        : gkg::NoiseModel(),
                          _sigma( sigma )
{
}


gkg::GaussianNoiseModel::~GaussianNoiseModel()
{
}


std::complex< float >
gkg::GaussianNoiseModel::getNoise() const
{

  try
  {

    static gkg::NumericalAnalysisImplementationFactory* 
      factory = gkg::NumericalAnalysisSelector::
                                     getInstance().getImplementationFactory();
    static RandomGenerator randomGenerator( gkg::RandomGenerator::Taus );

    return std::complex< float >(
       ( float )factory->getGaussianRandomNumber( randomGenerator,
                                                  0.0,
                                                  ( double )_sigma ),
       ( float )factory->getGaussianRandomNumber( randomGenerator,
                                                  0.0,
                                                  ( double )_sigma ) );

  }
  GKG_CATCH( "std::complex< float > "
             "gkg::GaussianNoiseModel::getNoise() const" );

}

