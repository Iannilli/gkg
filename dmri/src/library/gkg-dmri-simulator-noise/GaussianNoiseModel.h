#ifndef _gkg_dmri_simulator_noise_GaussianNoiseModel_h_
#define _gkg_dmri_simulator_noise_GaussianNoiseModel_h_


#include <gkg-core-pattern/RCPointer.h>
#include <gkg-dmri-simulator-noise/NoiseModel.h>


namespace gkg
{


class GaussianNoiseModel : public NoiseModel
{

  public:

    GaussianNoiseModel( float sigma );
    virtual ~GaussianNoiseModel();

    std::complex< float > getNoise() const;

  protected:

    float _sigma;

};


}


#endif
