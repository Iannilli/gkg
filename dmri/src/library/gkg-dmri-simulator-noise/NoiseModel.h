#ifndef _gkg_dmri_simulator_noise_NoiseModel_h_
#define _gkg_dmri_simulator_noise_NoiseModel_h_


#include <gkg-core-pattern/RCPointer.h>
#include <complex>


namespace gkg
{


class NoiseModel : public RCObject
{

  public:

    virtual ~NoiseModel();

    virtual std::complex< float > getNoise() const = 0;

  protected:

    NoiseModel();

};


}


#endif
