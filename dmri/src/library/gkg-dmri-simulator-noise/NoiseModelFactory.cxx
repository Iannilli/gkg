#include <gkg-dmri-simulator-noise/NoiseModelFactory.h>
#include <gkg-dmri-simulator-noise/GaussianNoiseModel.h>
#include <gkg-core-exception/Exception.h>


gkg::NoiseModelFactory::NoiseModelFactory()
{
}


gkg::NoiseModelFactory::~NoiseModelFactory()
{
}


gkg::RCPointer< gkg::NoiseModel >
gkg::NoiseModelFactory::getGaussianNoiseModel( float sigma )
{

  try
  {

    gkg::RCPointer< gkg::NoiseModel >
      gaussianNoiseModel( new gkg::GaussianNoiseModel( sigma ) );

    return gaussianNoiseModel;

  }
  GKG_CATCH( "gkg::RCPointer< gkg::NoiseModel > "
             "gkg::NoiseModelFactory::getGaussianNoiseModel( float sigma )" );

}

