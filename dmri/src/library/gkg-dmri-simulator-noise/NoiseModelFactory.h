#ifndef _gkg_dmri_simulator_motion_NoiseModelFactory_h_
#define _gkg_dmri_simulator_motion_NoiseModelFactory_h_


#include <gkg-core-pattern/Singleton.h>
#include <gkg-dmri-simulator-noise/NoiseModel.h>


namespace gkg
{


class NoiseModelFactory : public Singleton< NoiseModelFactory >
{

  public:

    ~NoiseModelFactory();

    RCPointer< NoiseModel > getGaussianNoiseModel( float sigma );

  protected:

    friend class Singleton< NoiseModelFactory >;

    NoiseModelFactory();

};


}


#endif

