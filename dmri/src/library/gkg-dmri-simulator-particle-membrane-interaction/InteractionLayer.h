#ifndef _gkg_dmri_simulator_particle_membrane_interaction_InteractionLayer_h_
#define _gkg_dmri_simulator_particle_membrane_interaction_InteractionLayer_h_


#include <gkg-core-pattern/RCPointer.h>
#include <gkg-processing-coordinates/Vector3d.h>


namespace gkg
{


class InteractionLayer : public RCObject
{

  public:

    InteractionLayer();
    virtual ~InteractionLayer();

    virtual bool hasInteraction( const Vector3d< float >& point,
                                 float distance ) const = 0;

};


}


#endif



