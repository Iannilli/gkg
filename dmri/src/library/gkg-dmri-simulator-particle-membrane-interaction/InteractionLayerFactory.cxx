#include <gkg-dmri-simulator-particle-membrane-interaction/InteractionLayerFactory.h>
#include <gkg-dmri-simulator-particle-membrane-interaction/ThickInteractionLayer.h>
#include <gkg-core-exception/Exception.h>


gkg::InteractionLayerFactory::InteractionLayerFactory()
{
}


gkg::InteractionLayerFactory::~InteractionLayerFactory()
{
}


gkg::RCPointer< gkg::InteractionLayer >
gkg::InteractionLayerFactory::getThickInteractionLayer( float thickness )
{

  try
  {

    gkg::RCPointer< gkg::InteractionLayer >
      thickInteractionLayer( new gkg::ThickInteractionLayer( thickness ) );

    return thickInteractionLayer;

  }
  GKG_CATCH( "gkg::RCPointer< gkg::InteractionLayer > "
             "gkg::InteractionLayerFactory::getThickInteractionLayer( "
             "float thickness )" );

}

