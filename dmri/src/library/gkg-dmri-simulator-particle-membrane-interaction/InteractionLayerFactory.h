#ifndef _gkg_dmri_simulator_particle_membrane_interaction_InteractionLayerFactory_h
#define _gkg_dmri_simulator_particle_membrane_interaction_InteractionLayerFactory_h_


#include <gkg-core-pattern/Singleton.h>
#include <gkg-dmri-simulator-particle-membrane-interaction/InteractionLayer.h>


namespace gkg
{


class InteractionLayerFactory : public Singleton< InteractionLayerFactory >
{

  public:

    ~InteractionLayerFactory();

    RCPointer< InteractionLayer > getThickInteractionLayer( float thickness );

  protected:

    friend class Singleton< InteractionLayerFactory >;

    InteractionLayerFactory();

};


}


#endif

