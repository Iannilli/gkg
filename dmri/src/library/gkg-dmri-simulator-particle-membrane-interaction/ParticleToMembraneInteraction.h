#ifndef _gkg_dmri_simulator_particle_membrane_interaction_ParticleToMembraneInteraction_h_
#define _gkg_dmri_simulator_particle_membrane_interaction_ParticleToMembraneInteraction_h_


#include <gkg-core-pattern/RCPointer.h>
#include <gkg-processing-coordinates/Vector3d.h>


namespace gkg
{


class Intersection;


class ParticleToMembraneInteraction : public RCObject
{

  public:

    virtual ~ParticleToMembraneInteraction();

    virtual void modifyPosition(
                          const Intersection& intersection,
                          const Vector3d< float >& currentPosition,
                          Vector3d< float >& theoreticalNewPosition ) const = 0;

  protected:

    ParticleToMembraneInteraction();

};


}


#endif
