#include <gkg-dmri-simulator-particle-membrane-interaction/ParticleToMembraneInteractionFactory.h>
#include <gkg-dmri-simulator-particle-membrane-interaction/TotalReflectionInteraction.h>
#include <gkg-core-exception/Exception.h>


gkg::ParticleToMembraneInteractionFactory::
                                          ParticleToMembraneInteractionFactory()
{
}


gkg::ParticleToMembraneInteractionFactory::
                                         ~ParticleToMembraneInteractionFactory()
{
}


gkg::RCPointer< gkg::ParticleToMembraneInteraction >
gkg::ParticleToMembraneInteractionFactory::getTotalReflectionInteraction()
{

  try
  {

    gkg::RCPointer< gkg::ParticleToMembraneInteraction >
      totalReflectionInteraction( new gkg::TotalReflectionInteraction() );

    return totalReflectionInteraction;

  }
  GKG_CATCH( "gkg::RCPointer< gkg::ParticleToMembraneInteraction > "
             "gkg::ParticleToMembraneInteractionFactory:: "
             "getTotalReflectionInteraction()" );

}

