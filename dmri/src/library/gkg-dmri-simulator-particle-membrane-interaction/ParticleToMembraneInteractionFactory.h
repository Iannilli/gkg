#ifndef _gkg_dmri_simulator_particle_membrane_interaction_ParticleToMembraneInteractionFactory_h_
#define _gkg_dmri_simulator_particle_membrane_interaction_ParticleToMembraneInteractionFactory_h_


#include <gkg-core-pattern/Singleton.h>
#include <gkg-dmri-simulator-particle-membrane-interaction/ParticleToMembraneInteraction.h>


namespace gkg
{


class ParticleToMembraneInteractionFactory :
                        public Singleton< ParticleToMembraneInteractionFactory >
{

  public:

    ~ParticleToMembraneInteractionFactory();

    RCPointer< ParticleToMembraneInteraction > getTotalReflectionInteraction();

  protected:

    friend class Singleton< ParticleToMembraneInteractionFactory >;

    ParticleToMembraneInteractionFactory();

};


}


#endif

