#include <gkg-dmri-simulator-particle-membrane-interaction/ThickInteractionLayer.h>
#include <gkg-core-exception/Exception.h>


gkg::ThickInteractionLayer::ThickInteractionLayer( float thickness )
                           : gkg::InteractionLayer(),
                             _thickness( thickness )
{
}


gkg::ThickInteractionLayer::~ThickInteractionLayer()
{
}


float gkg::ThickInteractionLayer::getThickness() const
{

  return _thickness;

}


bool gkg::ThickInteractionLayer::hasInteraction(
                                        const gkg::Vector3d< float >& /*point*/,
                                        float distance ) const
{

  try
  {

    return ( distance <= ( _thickness / 2.0 ) );

  }
  GKG_CATCH( "bool gkg::ThickInteractionLayer::hasInteraction( "
             "const gkg::Vector3d< float >& point, "
             "float distance ) const" );

}
