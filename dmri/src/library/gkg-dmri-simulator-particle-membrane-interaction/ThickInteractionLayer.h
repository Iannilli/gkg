#ifndef _gkg_dmri_simulator_particle_membrane_interaction_ThickInteractionLayer_h_
#define _gkg_dmri_simulator_particle_membrane_interaction_ThickInteractionLayer_h_


#include <gkg-dmri-simulator-particle-membrane-interaction/InteractionLayer.h>


namespace gkg
{


class ThickInteractionLayer : public InteractionLayer
{

  public:

    ThickInteractionLayer( float thickness );
    virtual ~ThickInteractionLayer();

    float getThickness() const;
    bool hasInteraction( const Vector3d< float >& point,
                         float distance ) const;

  protected:

    float _thickness;

};


}


#endif



