#include <gkg-dmri-simulator-particle-membrane-interaction/TotalReflectionInteraction.h>
#include <gkg-dmri-simulator-membrane/Membrane.h>
#include <gkg-processing-mesh/IntersectionSet.h>
#include <gkg-processing-numericalanalysis/NumericalAnalysisSelector.h>
#include <gkg-processing-numericalanalysis/RandomGenerator.h>
#include <gkg-core-exception/Exception.h>



gkg::TotalReflectionInteraction::TotalReflectionInteraction( 
                                                        /*float permeability*/ )
                                : gkg::ParticleToMembraneInteraction()
{
}


gkg::TotalReflectionInteraction::~TotalReflectionInteraction()
{
}


void gkg::TotalReflectionInteraction::modifyPosition(
                          const gkg::Intersection& intersection,
                          const gkg::Vector3d< float >& /*currentPosition*/,
                          gkg::Vector3d< float >& theoreticalNewPosition ) const
{

  try
  {

    float permeability = static_cast< gkg::Membrane* >(
                           intersection.evolvingMesh.get() )->getPermeability();

    // first, determine whether the particle is going to permeate through or
    // to be reflected by the interacting membrane
    gkg::NumericalAnalysisImplementationFactory*
      factory = gkg::NumericalAnalysisSelector::getInstance()
                                                    .getImplementationFactory();
    static gkg::RandomGenerator randomGenerator( gkg::RandomGenerator::Taus );
    double randomNumber = ( double )factory->getUniformRandomNumber(
                                                        randomGenerator, 0, 1 );

    if ( randomNumber > permeability )
    {

      // collecting the current vertex positions
      const gkg::Vector3d< float >&
        v1 = intersection.evolvingMesh->getCurrentVertex(
                                          intersection.polygon->indices[ 0 ] );
      const gkg::Vector3d< float >&
        v2 = intersection.evolvingMesh->getCurrentVertex(
                                          intersection.polygon->indices[ 1 ] );
      const gkg::Vector3d< float >&
        v3 = intersection.evolvingMesh->getCurrentVertex(
                                          intersection.polygon->indices[ 2 ] );

      // allocating the normal vector
      gkg::Vector3d< float > normal = ( v2 - v1 ).cross( v3 - v1 );
      if ( normal.getNorm2() == 0 )
      {

        throw std::runtime_error( "normal is nul vector" );

      }
      normal.normalize();

      //////////////////////////////////////////////////////////////////////////
      //  Total reflection method: finding the symmetric position regarding to 
      //  the original theoretical position and the plane defined by the polygon
      //////////////////////////////////////////////////////////////////////////
      //  plane: A x + B y + C z = k, contains a point p( px, py, pz )
      //  original theoretical position: t( tx, ty, tz )
      //  updated theoretical position: r( rx, ry, rz )
      //    
      //        rx = tx + 2 * A * h
      //        ry = ty + 2 * B * h
      //        rz = tz + 2 * C * h
      //
      //        h = ( k - ( A tx + B ty + C tz ) ) / ( A^2 + B^2 + C^2 );
      //  where k = A px + B py + C pz
      //////////////////////////////////////////////////////////////////////////

      float h = normal.dot( intersection.point - theoreticalNewPosition );
      theoreticalNewPosition += normal * 2.0f * h;

    }

  }
  GKG_CATCH( "void gkg::TotalReflectionInteraction:modifyPosition( "
             "const gkg::Vector3d< float >& currentPosition, "
             "gkg::Vector3d< float >& theoreticalNewPosition ) const" );

}

