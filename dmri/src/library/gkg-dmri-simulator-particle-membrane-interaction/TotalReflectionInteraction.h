#ifndef _gkg_dmri_simulator_particle_membrane_interaction_TotalReflectionInteraction_h_
#define _gkg_dmri_simulator_particle_membrane_interaction_TotalReflectionInteraction_h_


#include <gkg-dmri-simulator-particle-membrane-interaction/ParticleToMembraneInteraction.h>
#include <gkg-processing-container/Polygon.h>


namespace gkg
{


class TotalReflectionInteraction : public ParticleToMembraneInteraction
{

  public:

    TotalReflectionInteraction();
    virtual ~TotalReflectionInteraction();

    void modifyPosition( const Intersection& intersection,
                         const Vector3d< float >& currentPosition,
                         Vector3d< float >& theoreticalNewPosition ) const;

  protected:

    PolygonNormalFunctor< int32_t, float, 3U > _polygonNormalFunctor;

};


}


#endif

