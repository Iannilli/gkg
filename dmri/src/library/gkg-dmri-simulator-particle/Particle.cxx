#include <gkg-dmri-simulator-particle/Particle.h>
#include <gkg-dmri-simulator-particle-membrane-interaction/ParticleToMembraneInteraction.h>
#include <gkg-processing-mesh/IntersectionSet.h>
#include <gkg-dmri-simulator-tissue-modeler/TissueModeler.h>
#include <gkg-core-exception/Exception.h>


#define EPSILON 1e-6


gkg::Particle::Particle()
              : gkg::RCObject()
{

  try
  {

    _tissueModeler.reset( 0 );

  }
  GKG_CATCH( "gkg::Particle::Particle()" );

}


gkg::Particle::Particle(
                   gkg::RCPointer< gkg::TissueModeler > tissueModeler,
                   bool storeStates )
              : gkg::RCObject(),
                _tissueModeler( tissueModeler ),
                _storeStates( storeStates )
{
}


gkg::Particle::Particle( const gkg::LightCurve3d< float >& trajectory,
                         std::vector< int32_t > states )
              : gkg::RCObject(),
                _trajectory( trajectory ),
                _states( states )
{

  try
  {

    _tissueModeler.reset( 0 );

  }
  GKG_CATCH( "gkg::Particle::Particle( "
             "gkg::Curve3d< float > trajectory, "
             "std::vector< int32_t > states )" );

}


gkg::Particle::~Particle()
{
}


void gkg::Particle::initialize( const gkg::Vector3d< float >& position )
{

  try
  {

    // clearing the trajectory and state information
    clear();

    // setting the current position
    _currentPosition = position;
    _trajectory.addPoint( _currentPosition );

    // setting the cache voxel
    _cacheVoxel.x = -1;
    _cacheVoxel.y = -1;
    _cacheVoxel.z = -1;

    // allocating the states if required
    if ( _storeStates )
    {

      _states.resize( _tissueModeler->getStepCount() + 1 );

      // processing the initial states
      float distance = 1e38;
      gkg::RCPointer< gkg::EvolvingMesh > closestEvolvingMesh =
        _tissueModeler->getClosestEvolvingMesh( _currentPosition, distance );
      if ( !closestEvolvingMesh.isNull() )
      {

        _states[ 0 ] = 1;

      }

    }

  }
  GKG_CATCH( "void gkg::Particle::initialize( "
             "const gkg::Vector3d< float >& position )" );

}


void gkg::Particle::setTrajectoryAndStates(
                                   const gkg::LightCurve3d< float >& trajectory,
                                   const std::vector< int32_t >& states )
{

  try
  {

    if ( !_tissueModeler.isNull() )
    {

      throw std::runtime_error(
                          "can be used only outside a Monte-Carlo simulation" );


    }
    _trajectory = trajectory;
    _states = states;

  }
  GKG_CATCH( "void gkg::Particle:setTrajectoryAndStates( "
             "const gkg::LightCurve3d< float >& trajectory, "
             "const std::vector< int32_t >& states )" );

}


void gkg::Particle::clear()
{

  try
  {

    _trajectory.clear();
    _states.clear();

  }
  GKG_CATCH( "void gkg::particle::clear()" );

}


gkg::RCPointer< gkg::TissueModeler > gkg::Particle::getTissueModeler() const
{

  try
  {

    return _tissueModeler;  

  }
  GKG_CATCH( "gkg::RCPointer< gkg::TissueModeler > "
             "gkg::Particle::getTissueModeler() const" );

}


const gkg::Vector3d< float >& gkg::Particle::getStartingPosition() const
{

  try
  {

    return _trajectory.getPoint( 0 );

  }
  GKG_CATCH( "const gkg::Vector3d< float >& "
             "gkg::Particle::getStartingPosition() const" );

}


const gkg::Vector3d< float >& gkg::Particle::getCurrentPosition() const
{

  try
  {

    if ( _tissueModeler.isNull() )
    {

      throw std::runtime_error(
                         "cannot be used outside of a Monte-Carlo simulation" );

    }

    return _currentPosition;

  }
  GKG_CATCH( "const gkg::Vector3d< float >& "
             "gkg::Particle::getCurrentPosition() const" );

}


const gkg::Vector3d< float >& gkg::Particle::getEndingPosition() const
{

  try
  {

    return _trajectory.getPoint( _tissueModeler->getStepCount() + 1 );

  }
  GKG_CATCH( "const gkg::Vector3d< float >& "
             "gkg::Particle::getEndingPosition() const" );

}


const gkg::LightCurve3d< float >& gkg::Particle::getTrajectory() const
{

  try
  {

    return _trajectory;

  }
  GKG_CATCH( "const gkg::LightCurve3d< float >& "
             "gkg::Particle::getTrajectory() const" );

}


gkg::Vector3d< float > gkg::Particle::getPositionAt( int32_t iteration )
{

  try
  {

    if ( ( iteration < 0 ) || ( iteration > _trajectory.getPointCount() ) )
    {

      throw std::runtime_error( "iteration index cannot be negative or "
                                "greater than the point count of the "
                                "trajectory." );

    }
    return _trajectory.getPoint( iteration );

  }
  GKG_CATCH( "gkg::Vector3d< float > "
             "gkg::Particle::getPositionAt( int32_t iteration )" );

}


float gkg::Particle::getTimeFractionIn(
                                   const gkg::BoundingBox< float >& boundingBox,
                                   int32_t iterationCount )
{

  try
  {

    float fraction = 0.0;
    float factor = 1.0 / ( float )iterationCount;

    int32_t i = 0;
    for ( i = 0; i < iterationCount; i++ )
    {

      if ( boundingBox.contains( getPositionAt( i ) ) )
      {

        fraction += factor;

      }

    }

    return fraction;

  }
  GKG_CATCH( "float gkg::Particle::getTimeFractionIn( "
             "const gkg::BoundingBox< float >& boundingBox, "
             "int32_t iterationCount )" );

}


void gkg::Particle::move( int32_t iteration )
{

  try
  {

    if ( _tissueModeler.isNull() )
    {

      throw std::runtime_error(
                         "cannot be used outside of a Monte-Carlo simulation" );

    }

    bool isCloseToAMembrane = false;

    // getting the average free displacement based on the global diffusivity
    float displacement = _tissueModeler->getMotionModel()->getDisplacement(
                                                 _tissueModeler->getTimeStep() );

    if ( _storeStates || _tissueModeler->hasMultipleCompartments() )
    {

      // getting the closest membrane
      float distance = 1e38;
      gkg::RCPointer< gkg::EvolvingMesh > closestEvolvingMesh =
        _tissueModeler->getClosestEvolvingMesh( _currentPosition, distance );

      gkg::Membrane* closestMembrane = getMembrane( closestEvolvingMesh );

      if ( closestMembrane &&
           closestMembrane->getInteractionLayer()->hasInteraction(
                                                               _currentPosition,
                                                               distance ) )
      {

        // getting the average free displacement based on the diffusivity of
        // the interacting membrane
        displacement = closestMembrane->getMotionModel()->getDisplacement(
                                                 _tissueModeler->getTimeStep() );
        isCloseToAMembrane = true;

      }

    }

    // getting new theoretical position
    gkg::Vector3d< float > temporaryCurrentPosition = _currentPosition;
    gkg::Vector3d< float > temporaryTheoreticalPosition;
    _tissueModeler->getMotionModel()
                  ->getNewPosition( displacement,
                                    temporaryCurrentPosition,
                                    temporaryTheoreticalPosition );

    if ( _tissueModeler->getBoundingBox().
                                        contains( temporaryCurrentPosition ) ||
         _tissueModeler->getBoundingBox().
                                        contains( temporaryTheoreticalPosition ) )
    {

      // processing the global distance and ray direction
      float globalDistance = ( temporaryTheoreticalPosition -
                               temporaryCurrentPosition ).getNorm();
      gkg::Vector3d< float >
        currentRayDirection = ( temporaryTheoreticalPosition - 
                                temporaryCurrentPosition ).normalize();

      // looping over the putative intersections while limiting the total
      // motion to the global distance
      float accumulatedDistance = 0.0;
      gkg::Vector3d< float > newRayDirection = currentRayDirection;
      gkg::Intersection intersection, lastIntersection;
      int32_t index, intersectionCount;

      do
      {

        gkg::IntersectionSet intersectionSet( *_tissueModeler,
                                              temporaryCurrentPosition,
                                              temporaryTheoreticalPosition );
        intersectionCount = intersectionSet.getCount();

        if ( intersectionCount == 0 )
        {

          // case: no intersection
          accumulatedDistance += ( temporaryTheoreticalPosition -
                                   temporaryCurrentPosition ).getNorm();

        }
        else
        {

          // case: existing intersection(s)
          isCloseToAMembrane = true;

          // First: checking the first intersection of the new 
          // intersection set.
          // The intersection should be excluded if it is the same as 
          // the last intersection of the previous intersection set
          intersection = intersectionSet.getIntersection( 0 );

          if ( ( getMembrane( intersection.evolvingMesh ) ==
                 getMembrane( lastIntersection.evolvingMesh ) ) &&
               ( intersection.polygon->indices[ 0 ] ==
                 lastIntersection.polygon->indices[ 0 ] ) &&
               ( intersection.polygon->indices[ 1 ] ==
                 lastIntersection.polygon->indices[ 1 ] ) &&
               ( intersection.polygon->indices[ 2 ] ==
                 lastIntersection.polygon->indices[ 2 ] ) )
          {

            intersectionSet.eraseIntersection( 0 );
            intersectionCount = intersectionSet.getCount();

            if ( intersectionCount == 0 )
            {

              accumulatedDistance += ( temporaryTheoreticalPosition -
                                       temporaryCurrentPosition ).getNorm();

            }
          
          }

          // Second: processing intersection(s)
          index = 0;
          while ( ( index < intersectionCount ) &&
                  ( std::abs( newRayDirection.dot(
                              currentRayDirection ) - 1.0 ) < EPSILON ) )
          {

            intersection = intersectionSet.getIntersection( index );

            // updating the accumulated distance
            if ( ( intersection.point -
                   temporaryCurrentPosition ).getNorm2() != 0 )
            {

              accumulatedDistance += ( intersection.point -
                                       temporaryCurrentPosition ).getNorm();

            }

            // updating the theoretical position according to the kind of
            // interaction ( permeation, reflexion, ...)
            getMembrane( intersection.evolvingMesh )
              ->getParticleToMembraneInteraction()
              ->modifyPosition( intersection,
                                temporaryCurrentPosition,
                                temporaryTheoreticalPosition );

            // updating the ray direction
            if ( ( temporaryTheoreticalPosition -
                   intersection.point ).getNorm2() != 0 )
            {

              newRayDirection = ( temporaryTheoreticalPosition -
                                  intersection.point ).normalize();

            }
            else
            {

              // Temporary theoretical position approximates to the
              // intersecting point, which means the accumulated distance
              // approximates to the global distance.

              accumulatedDistance = globalDistance;
              break;              

            }

            // updating current position
            temporaryCurrentPosition = intersection.point;

            ++ index;

          }

          // while the ray direction changes, updating to the new direction
          currentRayDirection = newRayDirection;

          // getting the vertices of the last intersection
          lastIntersection = intersection;

        }

      }
      while ( ( accumulatedDistance < globalDistance ) && intersectionCount );

    }

    // updating the current position
    _currentPosition = temporaryTheoreticalPosition;

    // adding current position to trajectory
    _trajectory.addPoint( _currentPosition );

    // updating the accumulated close-to-membrane count
    if ( _storeStates && isCloseToAMembrane )
    {

      _states[ iteration + 1 ] = 1;

    }

  }
  GKG_CATCH( "void gkg::Particle::move( int32_t iteration )" );

}


void gkg::Particle::setCacheVoxel( const gkg::Vector3d< int32_t >& cacheVoxel )
{

  try
  {

    if ( _tissueModeler.isNull() )
    {

      throw std::runtime_error(
                         "cannot be used outside of a Monte-Carlo simulation" );

    }
    _cacheVoxel = cacheVoxel;

  }
  GKG_CATCH( "void gkg::Particle::setCacheVoxel( "
             "const gkg::Vector3d< int32_t >& cacheVoxel )" );

}


const gkg::Vector3d< int32_t >& gkg::Particle::getCacheVoxel() const
{

  try
  {

    if ( _tissueModeler.isNull() )
    {

      throw std::runtime_error(
                         "cannot be used outside of a Monte-Carlo simulation" );

    }
    return _cacheVoxel;

  }
  GKG_CATCH( "const gkg::Vector3d< int32_t >& "
             "gkg::Particle::getCacheVoxel() const" );

}


bool gkg::Particle::areStatesStored() const
{

  try
  {

    return ( !_states.empty() );

  }
  GKG_CATCH( "bool gkg::Particle::areStatesStored() const" );

}


const std::vector< int32_t >& gkg::Particle::getStates() const
{

  try
  {

    return _states;

  }
  GKG_CATCH( "const std::vector< int32_t >& "
             "gkg::Particle::getStates() const" );

}


float gkg::Particle::getStateFractionAt( int32_t iteration ) const
{

  try
  {

    float fraction = 0.0;
    float fractionFactor = 1.0 / ( float )( iteration + 1 );
    int32_t i = 0;
    for ( i = 0; i <= iteration; i++ )
    {

      if ( _states[ i ] == 1 )
      {

        fraction += fractionFactor;

      }

    }

    return fraction;

  }
  GKG_CATCH( "float gkg::Particle::getStateFractionAt( "
             "int32_t iteration ) const" );

}


gkg::Membrane* gkg::Particle::getMembrane(
                        const gkg::RCPointer< gkg::EvolvingMesh >& evolvingMesh )
{

  try
  {

    return static_cast< gkg::Membrane* >( evolvingMesh.get() );

  }
  GKG_CATCH( "gkg::Membrane* gkg::Particle::getMembrane( "
             "const gkg::RCPointer< gkg::EvolvingMesh >& evolvingMesh )t" );

}

#undef EPSILON

