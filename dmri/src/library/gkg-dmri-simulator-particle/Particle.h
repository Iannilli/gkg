#ifndef _gkg_dmri_simulator_particle_Particle_h_
#define _gkg_dmri_simulator_particle_Particle_h_


#include <gkg-core-pattern/RCPointer.h>
#include <gkg-processing-container/BoundingBox.h>
#include <gkg-processing-container/LightCurve3d.h>
#include <gkg-processing-mesh/EvolvingMesh.h>


namespace gkg
{


class TissueModeler;
class Membrane;


class Particle : public RCObject
{

  public:
  
    Particle();
    Particle( RCPointer< TissueModeler > tissueModeler,
              bool storeStates );
    Particle( const LightCurve3d< float >& trajectory,
              std::vector< int32_t > states );
    virtual ~Particle();

    void initialize( const Vector3d< float >& position );
    void setTrajectoryAndStates( const LightCurve3d< float >& trajectory,
                                 const std::vector< int32_t >& states );
    void clear();

    RCPointer< TissueModeler > getTissueModeler() const;
    const Vector3d< float >& getStartingPosition() const;
    const Vector3d< float >& getCurrentPosition() const;
    const Vector3d< float >& getEndingPosition() const;
    const LightCurve3d< float >& getTrajectory() const;
    Vector3d< float > getPositionAt( int32_t iteration );
    float getTimeFractionIn( const BoundingBox< float >& boundingBox,
                             int32_t iteractionCount );

    bool areStatesStored() const;
    const std::vector< int32_t >& getStates() const;
    float getStateFractionAt( int32_t iteration ) const;

    void move( int32_t iteration );

    void setCacheVoxel( const Vector3d< int32_t >& cacheVoxel );
    const Vector3d< int32_t >& getCacheVoxel() const;

  protected:

    static Membrane* 
      getMembrane( const RCPointer< EvolvingMesh >& evolvingMesh );

    RCPointer< TissueModeler > _tissueModeler;
    bool _storeStates;

    LightCurve3d< float > _trajectory;
    std::vector< int32_t > _states;

    Vector3d< float > _currentPosition;
    Vector3d< int32_t > _cacheVoxel;

};


}


#endif
