#include <gkg-dmri-simulator-particle/ParticleCache.h>
#include <gkg-dmri-simulator-tissue-modeler/TissueModeler.h>
#include <gkg-dmri-simulator-particle/Particle.h>
#include <gkg-processing-container/Volume_i.h>
#include <gkg-core-exception/Exception.h>


gkg::ParticleCache::ParticleCache( 
                           gkg::RCPointer< gkg::TissueModeler > tissueModeler,
                           const gkg::Vector3d< int32_t >& size )
                   : _tissueModeler( tissueModeler ),
                     _lut( size.x, size.y, size.z )
{

  try
  {

    // computing and storing the voxel resolution(s)
    float spanX = _tissueModeler->getBoundingBox().getUpperX() -
                   _tissueModeler->getBoundingBox().getLowerX();
    float spanY = _tissueModeler->getBoundingBox().getUpperY() -
                   _tissueModeler->getBoundingBox().getLowerY();
    float spanZ = _tissueModeler->getBoundingBox().getUpperZ() -
                   _tissueModeler->getBoundingBox().getLowerZ();

    gkg::Vector3d< double > resolution( ( double )( spanX / size.x ),
                                        ( double )( spanY / size.y ),
                                        ( double )( spanZ / size.z ) );

    _lut.setResolution ( resolution );
    _lut.fill( 0 );

  }
  GKG_CATCH( "gkg::ParticleCache::ParticleCache( "
             "gkg::RCPointer< gkg::TissueModeler > tissueModeler, "
             "const gkg::Vector3d< int32_t >& size )" );

}


gkg::ParticleCache::~ParticleCache()
{
}


std::set< gkg::RCPointer< gkg::Particle > >*
gkg::ParticleCache::getParticles( const gkg::Vector3d< int32_t >& voxel ) const
{

  try
  {

    return _lut( voxel );

  }
  GKG_CATCH( "const std::set< gkg::Particle* >& "
             "gkg::ParticleCache::getParticles( "
             "const gkg::Vector3d< int32_t >& voxel ) const" );

}


std::set< gkg::RCPointer< gkg::Particle > >*
gkg::ParticleCache::getParticles( const gkg::Vector3d< float >& point ) const
{

  try
  {

    gkg::Vector3d< int32_t > voxel;
    _tissueModeler->getCacheVoxel( point, voxel );

    return getParticles( voxel );

  }
  GKG_CATCH( "const std::set< gkg::Particle* >& "
             "gkg::ParticleCache::getParticles( "
             "const gkg::Vector3d< float >& point ) const" );

}


void gkg::ParticleCache::update( gkg::RCPointer< gkg::Particle > particle )
{

  try
  {

    gkg::Vector3d< int32_t > voxel;
    _tissueModeler->getCacheVoxel( particle->getCurrentPosition(), voxel );

    // first removing the particle from the old voxel in the lut
    if ( ( particle->getCacheVoxel().x != -1 ) &&
         ( !std::operator==( voxel, particle->getCacheVoxel() ) ) )
    {

      std::set< gkg::RCPointer< gkg::Particle > >*
        oldParticleSet = _lut( particle->getCacheVoxel() );
      std::set< gkg::RCPointer< gkg::Particle > >::const_iterator
        p = oldParticleSet->begin(),
        pe = oldParticleSet->end();
      while ( p != pe )
      {

        if ( ( *p ) == particle )
        {

          oldParticleSet->erase( p );
          break;

        }
        ++ p;

      }

    }

    // adding the particle to the current voxel of the lut
    std::set< gkg::RCPointer< gkg::Particle > >*
      particleSet = getParticles( voxel );
    if ( particleSet == 0 )
    {

      // no particle were already stored, so need to create the 
      // set and add the current particle to that set
      std::set< gkg::RCPointer< gkg::Particle > >*
        newParticleSet = new std::set< gkg::RCPointer< gkg::Particle > >();
      newParticleSet->insert( particle );
      _lut( voxel ) = newParticleSet;
      particle->setCacheVoxel( voxel );

    }
    else
    {

      // checking if the current particle is already present
      // and in that case, do nothing
      bool isStillNotPresent = true;
      std::set< gkg::RCPointer< gkg::Particle > >::const_iterator
        p = particleSet->begin(),
        pe = particleSet->end();
      while ( p != pe )
      {

        if ( ( *p ) == particle )
        {

          isStillNotPresent = false;

        }
        ++ p;

      }

      // if the current particle is not present, add it to the 
      // set
      if ( isStillNotPresent )
      {

        particleSet->insert( particle );
        particle->setCacheVoxel( voxel );

      }

    }

  }
  GKG_CATCH( "void gkg::ParticleCache::update( "
             "gkg::RCPointer< gkg::Particle > particle )" );

}


void gkg::ParticleCache::update(
               const std::vector< gkg::RCPointer< gkg::Particle > >& particles )
{

  try
  {

    int32_t p = 0;
    int32_t particleCount = ( int32_t )particles.size();
    for ( p = 0; p < particleCount; p++ )
    {

      this->update( particles[ p ] );

    }

  }
  GKG_CATCH( "void gkg::ParticleCache::update( "
             "const std::vector< gkg::RCPointer< gkg::Particle > >& "
             "particles )" );

}
