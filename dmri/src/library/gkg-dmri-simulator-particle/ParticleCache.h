#ifndef _gkg_dmri_simulator_particle_ParticleCache_h_
#define _gkg_dmri_simulator_particle_ParticleCache_h_

#include <gkg-core-pattern/RCPointer.h>
#include <gkg-processing-coordinates/Vector3d.h>
#include <gkg-processing-container/Volume.h>
#include <set>
#include <vector>


namespace gkg
{


class TissueModeler;
class Particle;


class ParticleCache
{

  public:

    ParticleCache( RCPointer< TissueModeler > tissueModeler,
                   const Vector3d< int32_t >& size );
    virtual ~ParticleCache();

    std::set< RCPointer< Particle > >* 
      getParticles( const Vector3d< int32_t >& voxel ) const;
    std::set< RCPointer< Particle > >*
      getParticles( const Vector3d< float >& point ) const;

    void update( RCPointer< Particle > particle );
    void update( const std::vector< RCPointer< Particle > >& particles );

  protected:

    RCPointer< TissueModeler > _tissueModeler;
    Volume< std::set< RCPointer< Particle > >* > _lut;

};


}


#endif
