#include <gkg-dmri-simulator-particle/ParticleMap.h>
#include <gkg-core-object/BaseObject_i.h>
#include <gkg-core-object/GenericObject_i.h>
#include <gkg-core-exception/Exception.h>


gkg::ParticleMap::ParticleMap()
                 : gkg::HeaderedObject(),
                   _fileCount( 0 )
{

  try
  {

    // adding a generic syntax set
    _syntaxSet[ "__generic__" ][ "object_type" ] =
      gkg::Semantic( gkg::TypeOf< std::string >::getName(), true );
    _syntaxSet[ "__generic__" ][ "item_count" ] =
      gkg::Semantic( gkg::TypeOf< int32_t >::getName(), true );
    _syntaxSet[ "__generic__" ][ "file_count" ] =
      gkg::Semantic( gkg::TypeOf< int32_t >::getName(), true );

    // adding attributes to header
    _header.addAttribute( "object_type", std::string( "ParticleMap" ) );
    this->updateHeader();

  }
  GKG_CATCH( "gkg::ParticleMap::ParticleMap()" );

}


gkg::ParticleMap::ParticleMap(
                          const std::vector< RCPointer< Particle > >& particles,
                          int32_t fileCount,
                          float timeStep )
                 : gkg::HeaderedObject(),
                   _particles( particles ),
                   _fileCount( fileCount ),
                   _timeStep( timeStep )


{

  try
  {

    // adding a generic syntax set
    _syntaxSet[ "__generic__" ][ "object_type" ] =
      gkg::Semantic( gkg::TypeOf< std::string >::getName(), true );
    _syntaxSet[ "__generic__" ][ "item_count" ] =
      gkg::Semantic( gkg::TypeOf< int32_t >::getName(), true );
    _syntaxSet[ "__generic__" ][ "file_count" ] =
      gkg::Semantic( gkg::TypeOf< int32_t >::getName(), true );

    // adding attributes to header
    _header.addAttribute( "object_type", std::string( "ParticleMap" ) );
    this->updateHeader();

  }
  GKG_CATCH( "gkg::ParticleMap::ParticleMap( "
             "const std::vector< RCPointer< Particle > >& particles, "
             "int32_t fileCount, "
             "float timeStep )" );

}


gkg::ParticleMap::~ParticleMap()
{
}


void gkg::ParticleMap::setParticles(
               const std::vector< gkg::RCPointer< gkg::Particle > >& particles )
{

  try
  {

    _particles = particles;
    this->updateHeader();

  }
  GKG_CATCH( "void gkg::ParticleMap::setParticles( "
             "const std::vector< gkg::RCPointer< gkg::Particle > >& "
             "particles )" );

}


const gkg::RCPointer< gkg::Particle >&
gkg::ParticleMap::getParticle( int32_t index ) const
{

  try
  {

    return _particles[ index ];

  }
  GKG_CATCH( "const gkg::RCPointer< gkg::Particle >& "
             "gkg::ParticleMap::getParticle( int32_t index ) const" );

}


int32_t gkg::ParticleMap::getParticleCount() const
{

  try
  {

    return ( int32_t )_particles.size();

  }
  GKG_CATCH( "int32_t gkg::ParticleMap::getParticleCount() const" );

}

float gkg::ParticleMap::getTimeStep() const
{

  try
  {

    return _timeStep;

  }
  GKG_CATCH( "float gkg::ParticleMap::getTimeStep() const" );

}


void gkg::ParticleMap::setFileCount( int32_t fileCount )
{

  try
  {

    if ( fileCount < 1 )
    {

      throw std::runtime_error( "file count must be greater or equal to 1" );

    }

    _fileCount = fileCount;
    this->updateHeader();

  }
  GKG_CATCH( "void gkg::ParticleMap::setFileCount( int32_t fileCount )" );

}


int32_t gkg::ParticleMap::getFileCount() const
{

  try
  {

    return _fileCount;

  }
  GKG_CATCH( "int32_t gkg::ParticleMap::getFileCount() const" );

}


void gkg::ParticleMap::updateHeader()
{

  try
  {

    _header.addAttribute( "item_count", ( int32_t )_particles.size() );
    _header.addAttribute( "file_count", _fileCount );


  }
  GKG_CATCH( "void gkg::ParticleMap::updateHeader()" );

}


