#ifndef _gkg_dmri_simulator_particle_ParticleMap_h_
#define _gkg_dmri_simulator_particle_ParticleMap_h_


#include <gkg-core-object/HeaderedObject.h>
#include <gkg-core-pattern/RCPointer.h>
#include <gkg-dmri-simulator-particle/Particle.h>
#include <vector>


namespace gkg
{


class ParticleMap : public HeaderedObject
{

  public:

    ParticleMap();
    ParticleMap( const std::vector< RCPointer< Particle > >& particles,
                 int32_t fileCount,
                 float timeStep );
    virtual ~ParticleMap();

    void setParticles( const std::vector< RCPointer< Particle > >& particles );
    const RCPointer< Particle >& getParticle( int32_t index ) const;
    int32_t getParticleCount() const;
    float getTimeStep() const;
    void setFileCount( int32_t fileCount );
    int32_t getFileCount() const;


  protected:

    friend class ParticleMapDiskFormat;

    void updateHeader();

    std::vector< RCPointer< Particle > > _particles;
    int32_t _fileCount;
    float _timeStep;

};


}


#endif
