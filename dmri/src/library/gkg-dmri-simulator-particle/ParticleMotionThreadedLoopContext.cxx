#include <gkg-dmri-simulator-particle/ParticleMotionThreadedLoopContext.h>
#include <gkg-core-exception/Exception.h>


gkg::ParticleMotionThreadedLoopContext::ParticleMotionThreadedLoopContext(
                                            const gkg::ParticleMap& particleMap,
                                            const int32_t& step )
                                       : gkg::LoopContext(),
                                         _particleMap( particleMap ),
                                         _step( step )
{
}


gkg::ParticleMotionThreadedLoopContext::~ParticleMotionThreadedLoopContext( )
{
}


void gkg::ParticleMotionThreadedLoopContext::doIt( int32_t startIndex,
                                                   int32_t count )
{

  try
  {

    int32_t p;
    for ( p = startIndex; p < startIndex + count; p++ )
    {

      _particleMap.getParticle( p )->move( _step );

    }

  }
  GKG_CATCH( "void gkg::ParticleMotionThreadedLoopContext::doIt( "
             "int32_t startIndex, "
             "int32_t count )" );

}
