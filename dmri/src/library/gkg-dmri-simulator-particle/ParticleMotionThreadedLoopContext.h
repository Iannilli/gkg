#ifndef _gkg_dmri_simulator_particle_ParticleMotionThreadedLoopContext_i_h_
#define _gkg_dmri_simulator_particle_ParticleMotionThreadedLoopContext_i_h_

#include <gkg-communication-thread/LoopContext.h>
#include <gkg-dmri-simulator-particle/ParticleMap.h>
#include <gkg-core-pattern/RCPointer.h>
#include <vector>


namespace gkg
{


class ParticleMotionThreadedLoopContext : public LoopContext
{

  public:

    ParticleMotionThreadedLoopContext( const ParticleMap& particleMap,
                                       const int32_t& step );
    virtual ~ParticleMotionThreadedLoopContext();

    void doIt( int32_t startIndex, int32_t count );

  private:

    const ParticleMap& _particleMap;
    const int32_t& _step;

};


}


#endif
