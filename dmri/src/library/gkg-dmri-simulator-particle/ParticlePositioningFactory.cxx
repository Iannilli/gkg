#include <gkg-dmri-simulator-particle/ParticlePositioningFactory.h>
#include <gkg-processing-numericalanalysis/NumericalAnalysisSelector.h>
#include <gkg-processing-numericalanalysis/RandomGenerator.h>
#include <gkg-core-exception/Exception.h>


gkg::ParticlePositioningFactory::ParticlePositioningFactory()
{
}


gkg::ParticlePositioningFactory::~ParticlePositioningFactory()
{
}


void gkg::ParticlePositioningFactory::getBoxUniformRandomPositions(
                              gkg::BoundingBox< float > boundingBox,
                              int32_t particleCount,
                              std::vector< gkg::Vector3d< float > >& positions )
{

  try
  {

    // resizing the positions
    positions.resize( particleCount );

    // creating randomizer
    static gkg::NumericalAnalysisImplementationFactory*
      factory = gkg::NumericalAnalysisSelector::getInstance().
                                                     getImplementationFactory();
    static gkg::RandomGenerator randomGenerator( gkg::RandomGenerator::Taus );

    // getting positions
    gkg::Vector3d< float > position;
    int32_t p = 0;
    for ( p = 0; p < particleCount; p++ )
    {

      position.x = ( float )factory->getUniformRandomNumber(
                                                      randomGenerator,
                                                      boundingBox.getLowerX(),
                                                      boundingBox.getUpperX() );
      position.y = ( float )factory->getUniformRandomNumber(
                                                      randomGenerator,
                                                      boundingBox.getLowerY(),
                                                      boundingBox.getUpperY() );
      position.z = ( float )factory->getUniformRandomNumber(
                                                      randomGenerator,
                                                      boundingBox.getLowerZ(),
                                                      boundingBox.getUpperZ() );

      positions[ p ] = position;

    }

  }
  GKG_CATCH( "void "
             "gkg::ParticlePositioningFactory::getBoxUniformRandomPositions( "
             "gkg::BoundingBox< float > boundingBox, "
             "int32_t particleCount, "
             "std::vector< gkg::Vector3d< float > >& positions )" );

}


void gkg::ParticlePositioningFactory::getBoxCenterPosition(
                              gkg::BoundingBox< float > boundingBox,
                              int32_t particleCount,
                              std::vector< gkg::Vector3d< float > >& positions )
{

  try
  {

    gkg::Vector3d< float > center( ( boundingBox.getLowerX() +
                                     boundingBox.getUpperX() ) / 2.0,
                                   ( boundingBox.getLowerY() +
                                     boundingBox.getUpperY() ) / 2.0,
                                   ( boundingBox.getLowerZ() +
                                     boundingBox.getUpperZ() ) / 2.0 );

    positions.resize( particleCount, center );

  }
  GKG_CATCH( "void "
             "gkg::ParticlePositioningFactory::getBoxCenterPosition( "
             "gkg::BoundingBox< float > boundingBox, "
             "int32_t particleCount, "
             "std::vector< gkg::Vector3d< float > >& positions )" );

}


void gkg::ParticlePositioningFactory::getSpecifiedPosition(
                              gkg::Vector3d< float > point,
                              int32_t particleCount,
                              std::vector< gkg::Vector3d< float > >& positions )
{

  try
  {

    positions.resize( particleCount, point );

  }
  GKG_CATCH( "void "
             "gkg::ParticlePositioningFactory::getSpecifiedPosition( "
             "gkg::Vector3d< float > point, "
             "int32_t particleCount, "
             "std::vector< gkg::Vector3d< float > >& positions )" );

}


void gkg::ParticlePositioningFactory::getIntraExtraMembranePositions(
                             gkg::RCPointer< gkg::TissueModeler > tissueModeler,
                             gkg::BoundingBox< float > boundingBox,
                             float intraCellularFraction,
                             int32_t particleCount,
                             std::vector< gkg::Vector3d< float > >& positions )
{

  try
  {

    // sanity check
    if ( tissueModeler->getMembraneCount() == 0 )
    {

      throw std::runtime_error( "No membrane exists." );

    }

    // resizing the positions
    positions.resize( particleCount );

    // creating randomizer
    static gkg::NumericalAnalysisImplementationFactory*
      factory = gkg::NumericalAnalysisSelector::getInstance().
                                                     getImplementationFactory();
    static gkg::RandomGenerator randomGenerator( gkg::RandomGenerator::Taus );

    // getting positions
    //gkg::BoundingBox< float > boundingBox = tissueModeler->getBoundingBox();
    gkg::Vector3d< float > position;
    int32_t p = 0;
    for ( p = 0; p < particleCount; p++ )
    {

      double randomNumber = ( double )factory->getUniformRandomNumber(
                                                        randomGenerator, 0, 1 );

      if ( randomNumber < intraCellularFraction )
      {

        // case: position must be inside any membranes
        bool isInsideAMembrane = false;
        while ( !isInsideAMembrane )
        {

          position.x = ( float )factory->getUniformRandomNumber(
                                                      randomGenerator,
                                                      boundingBox.getLowerX(),
                                                      boundingBox.getUpperX() );
          position.y = ( float )factory->getUniformRandomNumber(
                                                      randomGenerator,
                                                      boundingBox.getLowerY(),
                                                      boundingBox.getUpperY() );
          position.z = ( float )factory->getUniformRandomNumber(
                                                      randomGenerator,
                                                      boundingBox.getLowerZ(),
                                                      boundingBox.getUpperZ() );

          isInsideAMembrane = tissueModeler->isInsideAEvolvingMesh( position );

        }

      }
      else
      {

        // case: position must be outside any membrane
        bool isInsideAMembrane = true;
        while ( isInsideAMembrane )
        {

          position.x = ( float )factory->getUniformRandomNumber(
                                                      randomGenerator,
                                                      boundingBox.getLowerX(),
                                                      boundingBox.getUpperX() );
          position.y = ( float )factory->getUniformRandomNumber(
                                                      randomGenerator,
                                                      boundingBox.getLowerY(),
                                                      boundingBox.getUpperY() );
          position.z = ( float )factory->getUniformRandomNumber(
                                                      randomGenerator,
                                                      boundingBox.getLowerZ(),
                                                      boundingBox.getUpperZ() );

          isInsideAMembrane = tissueModeler->isInsideAEvolvingMesh( position );

        }

      }

      positions[ p ] = position;

    }

  }
  GKG_CATCH( "void "
             "gkg::ParticlePositioningFactory::getIntraExtraMembranePositions( "
             "gkg::RCPointer< gkg::TissueModeler > tissueModeler, "
             "float intraCellularFraction, "
             "int32_t particleCount, "
             "std::vector< gkg::Vector3d< float > >& positions )" );

}

