#ifndef _gkg_dmri_simulator_particle_ParticlePositioningFactory_h_
#define _gkg_dmri_simulator_particle_ParticlePositioningFactory_h_


#include <gkg-core-pattern/Singleton.h>
#include <gkg-dmri-simulator-tissue-modeler/TissueModeler.h>
#include <set>
#include <string>
#include <vector>


namespace gkg
{


class ParticlePositioningFactory :
                                  public Singleton< ParticlePositioningFactory >
{

  public:

    ~ParticlePositioningFactory();

    void getBoxUniformRandomPositions(
                                  BoundingBox< float > boundingBox,
                                  int32_t particleCount,
                                  std::vector< Vector3d< float > >& positions );

    void getBoxCenterPosition( BoundingBox< float > boundingBox,
                               int32_t particleCount,
                               std::vector< Vector3d< float > >& positions );

    void getSpecifiedPosition( Vector3d< float > point,
                               int32_t particleCount,
                               std::vector< Vector3d< float > >& positions );

    void getIntraExtraMembranePositions(
                                  RCPointer< TissueModeler > tissueModeler,
                                  BoundingBox< float > boundingBox,
                                  float intraCellularFraction,
                                  int32_t particleCount,
                                  std::vector< Vector3d< float > >& positions );

  protected:

    friend class Singleton< ParticlePositioningFactory >;

    ParticlePositioningFactory();

};


}

#endif

