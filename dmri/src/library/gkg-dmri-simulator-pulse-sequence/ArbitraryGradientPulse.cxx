#include <gkg-dmri-simulator-pulse-sequence/ArbitraryGradientPulse.h>
#include <gkg-core-exception/Exception.h>


gkg::ArbitraryGradientPulse::ArbitraryGradientPulse(
                                              int32_t axis,
                                              const std::vector< float >& shape,
                                              const float& startingTime,
                                              const float& timeStep )
                            : gkg::GradientPulse( axis ),
                              _shape( shape ),
                              _startingTime( startingTime ),
                              _timeStep( timeStep )
{

  try
  {

    if ( _timeStep <= 0 )
    {

      throw std::runtime_error( "negative time step" );

    }


    if ( _shape.empty() )
    {

      throw std::runtime_error( "empty gradient shape" );

    }


    _endingTime = _startingTime + _timeStep * ( float )this->getStepCount();
    _totalArea = 0.0f;
    _totalSquareArea = 0.0f;
    std::vector< float >::const_iterator v = _shape.begin(),
                                         ve = _shape.end();
    float tmp = 0.0f;
    while ( v != ve )
    {

      tmp = *v * _timeStep;
      _totalArea += tmp;
      _totalSquareArea += *v * tmp;

      ++ v;

    }

  }
  GKG_CATCH( "gkg::ArbitraryGradientPulse::ArbitraryGradientPulse( "
             "int32_t axis, "
             "const std::vector< float >& shape, "
             "const float& timeStep )" );


}


gkg::ArbitraryGradientPulse::~ArbitraryGradientPulse()
{
}


const std::vector< float >& gkg::ArbitraryGradientPulse::getShape() const
{


  return _shape;

}


int32_t gkg::ArbitraryGradientPulse::getStepCount() const
{

  try
  {

    return ( int32_t )_shape.size();

  }
  GKG_CATCH( "int32_t gkg::ArbitraryGradientPulse::getStepCount() const" );

}


const float& gkg::ArbitraryGradientPulse::getStartingTime() const
{

  return _startingTime;

}


const float& gkg::ArbitraryGradientPulse::getEndingTime() const
{

  return _endingTime;

}


const float& gkg::ArbitraryGradientPulse::getTimeStep() const
{

  return _timeStep;

}


const float& gkg::ArbitraryGradientPulse::getTotalArea() const
{

  return _totalArea;

}


const float& gkg::ArbitraryGradientPulse::getTotalSquareArea() const
{

  return _totalSquareArea;

}


float gkg::ArbitraryGradientPulse::getMaximumValue() const
{

  try
  {

    float maximum = *_shape.begin();
    std::vector< float >::const_iterator v = _shape.begin(),
                                         ve = _shape.end();
    ++ v;
    while ( v != ve )
    {


      if ( *v > maximum )
      {

        maximum = *v;

      }
      ++ v;

    }

    return maximum;

  }
  GKG_CATCH( "float gkg::ArbitraryGradientPulse::getMaximumValue() const" );

}


float gkg::ArbitraryGradientPulse::getValueAt( float time,
                                               float scaling ) const
{

  try
  {

    float value = 0.0f;
    if ( ( time >= _startingTime ) && ( time < _endingTime ) )
    {

      int32_t position = ( int32_t )( ( time - _startingTime ) / _timeStep );
      value = _shape[ position ];

    }
    return value * scaling;

  }
  GKG_CATCH( "float gkg::ArbitraryGradientPulse::getValueAt( "
             "float time, "
             "float scaling ) const" );

}


float gkg::ArbitraryGradientPulse::getIntegralValueAt( float time,
                                                       float scaling ) const
{

  try
  {

    float value = 0.0f;
    if ( time < _startingTime )
    {

      value = 0.0f;

    }
    else if ( time >= _endingTime )
    {

      value = _totalArea;

    }
    else if ( ( time >= _startingTime ) && ( time < _endingTime ) )
    {

      int32_t position = ( int32_t )( ( time - _startingTime ) / _timeStep );
      int32_t p = 0;
      for ( p = 0; p < position; p++ )
      {

        value += _shape[ p ] * _timeStep;

      }

      value += _shape[ p ] * ( time - ( float )position * _timeStep );

    }
    return value * scaling;

  }
  GKG_CATCH( "float gkg::ArbitraryGradientPulse::getIntegralValueAt( "
             "float time, "
             "float scaling ) const" );

}


float 
gkg::ArbitraryGradientPulse::getIntegralSquareValueAt( float time,
                                                       float scaling ) const
{

  try
  {

    float value = 0.0f;
    if ( time < _startingTime )
    {

      value = 0.0f;

    }
    else if ( time >= _endingTime )
    {

      value = _totalSquareArea;

    }
    else if ( ( time >= _startingTime ) && ( time < _endingTime ) )
    {

      int32_t position = ( int32_t )( ( time - _startingTime ) / _timeStep );
      int32_t p = 0;
      for ( p = 0; p < position; p++ )
      {

        value += _shape[ p ] * _shape[ p ] * _timeStep;

      }

      value += _shape[ p ] * _shape[ p ] * 
               ( time - ( float )position * _timeStep );

    }

    return value * scaling * scaling;

  }
  GKG_CATCH( "float gkg::ArbitraryGradientPulse::getIntegralSquareValueAt( "
             "float time, "
             "float scaling ) const" );


}

