#ifndef _gkg_dmri_simulator_pulse_sequence_ArbitraryGradientPulse_h_
#define _gkg_dmri_simulator_pulse_sequence_ArbitraryGradientPulse_h_


#include <gkg-dmri-simulator-pulse-sequence/GradientPulse.h>
#include <vector>


namespace gkg
{


// gradient magnitudes are provided in mT/m
// times are provided in us


class ArbitraryGradientPulse : public GradientPulse
{

  public:


    ArbitraryGradientPulse( int32_t axis,
                            const std::vector< float >& shape,
                            const float& startingTime,
                            const float& timeStep );
    virtual ~ArbitraryGradientPulse();

    const std::vector< float >& getShape() const;
    int32_t getStepCount() const;
    const float& getStartingTime() const;
    const float& getEndingTime() const;
    const float& getTimeStep() const;
    const float& getTotalArea() const;
    const float& getTotalSquareArea() const;

    float getMaximumValue() const;
    
    virtual float getValueAt( float time,
                              float scaling ) const;
    virtual float getIntegralValueAt( float time,
                                      float scaling ) const;
    virtual float getIntegralSquareValueAt( float time,
                                            float scaling ) const;

  protected:

    std::vector< float > _shape;
    float _startingTime;
    float _endingTime;
    float _timeStep;
    float _totalArea;
    float _totalSquareArea;

};


}


#endif
