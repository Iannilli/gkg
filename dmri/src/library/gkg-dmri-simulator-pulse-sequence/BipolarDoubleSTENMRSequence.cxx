#include <gkg-dmri-simulator-pulse-sequence/BipolarDoubleSTENMRSequence.h>
#include <gkg-dmri-simulator-pulse-sequence/RadioFrequencyPulse.h>
#include <gkg-dmri-simulator-pulse-sequence/DiffusionGradientPulse.h>
#include <gkg-core-exception/Exception.h>
#include <cmath>


#define TWOPI_GAM 26748.0
#define GAM 26748.0/ (2*M_PI)

#define MT_PER_M_TO_G_PER_CM              0.1
#define T_PER_M_PER_S_TO_G_PER_CM_PER_US  0.0001
#define MS_TO_US                          1000.0

#define RUP_GRD( A )  ( ( ( int32_t )gradientTimeResolution -       \
                          ( ( int32_t )( A ) %                      \
                            ( int32_t )gradientTimeResolution ) ) % \
                          ( int32_t )gradientTimeResolution +       \
                        ( int32_t )( A ) )


gkg::BipolarDoubleSTENMRSequence::BipolarDoubleSTENMRSequence(
                             const std::set< float >& gradientAmplitudes,
                             const std::vector< float >& scalarParameters,
                             const std::vector< std::string >& stringParameters,
                             bool verbose )
                                 : gkg::NMRSequence( gradientAmplitudes,
                                                     verbose )
{

  try
  {

    // collecting sequence scalar parameters
    float maximumSlewRate = scalarParameters[ 0 ];
    float gradientTimeResolution = scalarParameters[ 1 ];
    float littleDeltaInMs = scalarParameters[ 2 ];
    float bigDeltaInMs = scalarParameters[ 3 ];
    float mixingTimeInMs = scalarParameters[ 4 ];

    // collecting sequence string parameters
    std::string wavevectorFileName = stringParameters[ 0 ];
    gkg::OrientationSet wavevectors( wavevectorFileName );

    if ( wavevectors.getCount() != 2 )
    {

      throw std::runtime_error( "Invalid wavevector count" );

    }

    //
    // processing the diffusion gradient parameters
    //
    float maximumGradientAmplitude = this->_gradientAmplitudes.back();
    float maximumGradientAmplitudeInGaussPerCm = maximumGradientAmplitude *
                                                 MT_PER_M_TO_G_PER_CM;
    float maximumSlewRateInGaussPerCmPerUs = maximumSlewRate *
                                             T_PER_M_PER_S_TO_G_PER_CM_PER_US;

    float halfLittleDeltaInUs = 0.5 * littleDeltaInMs * MS_TO_US;
    float bigDeltaInUs = bigDeltaInMs * MS_TO_US;
    float rampWidthInUs = 0.0;
    float plateauWidthInUs = getDiffusionPulsePlateauWidthInUs(
                                           maximumGradientAmplitudeInGaussPerCm,
                                           maximumSlewRateInGaussPerCmPerUs,
                                           halfLittleDeltaInUs,
                                           gradientTimeResolution,
                                           rampWidthInUs );

    _effectiveDiffusionTimeInMs = bigDeltaInMs - littleDeltaInMs / 3.0;
    _maximumQInMeterInverse = GAM * 1e-2 * littleDeltaInMs *
                              maximumGradientAmplitude;
    float mixingTimeInUs = mixingTimeInMs * MS_TO_US;

    //
    // creating and adding pulses to the NMR sequence
    //

    ///////////// creating RF + Gslice + Gphase + Gread pulse sequences ////////
    gkg::RCPointer< gkg::PulseSequence >
      rfMagnitudePulseSequence( new gkg::PulseSequence( RF_MAGNITUDE_AXIS ) );

    gkg::RCPointer< gkg::PulseSequence >
      rfPhasePulseSequence( new gkg::PulseSequence( RF_PHASE_AXIS ) );

    gkg::RCPointer< gkg::PulseSequence >
      xGradientPulseSequence( new gkg::PulseSequence( GRADIENT_X_AXIS ) );

    gkg::RCPointer< gkg::PulseSequence >
      yGradientPulseSequence( new gkg::PulseSequence( GRADIENT_Y_AXIS ) );

    gkg::RCPointer< gkg::PulseSequence >
      zGradientPulseSequence( new gkg::PulseSequence( GRADIENT_Z_AXIS ) );

    /////////////////////////// focusing on RF pulses //////////////////////////

    // allocating excitation and refocusing pulses
    std::vector< gkg::RCPointer< gkg::Pulse > > excitationRFPulses( 5 );
    std::vector< gkg::RCPointer< gkg::Pulse > > refocusingRFPulses;

    float sliceSelectionGradientTime = 3.0 * MS_TO_US;
    float sliceSelectionRephasingTime = 0.75 * MS_TO_US;
    float refocusingGradientTime = 6.0 * MS_TO_US;
    float halfEchoTime =
                    sliceSelectionGradientTime + refocusingGradientTime +
                    2.0 * ( sliceSelectionRephasingTime + halfLittleDeltaInUs );

    // allocating the timings
    float excitationStartingTime;
    float excitationCenterTime;
    float excitationEndingTime;
    float refocusingStartingTime;
    float refocusingCenterTime;
    float refocusingEndingTime;

    // allocating DW gradient pulses
    gkg::RCPointer< gkg::Pulse > leftDWGradientPulsesOnXAxis;
    gkg::RCPointer< gkg::Pulse > leftDWGradientPulsesOnYAxis;
    gkg::RCPointer< gkg::Pulse > leftDWGradientPulsesOnZAxis;
    gkg::RCPointer< gkg::Pulse > rightDWGradientPulsesOnXAxis;
    gkg::RCPointer< gkg::Pulse > rightDWGradientPulsesOnYAxis;
    gkg::RCPointer< gkg::Pulse > rightDWGradientPulsesOnZAxis;

    if ( mixingTimeInUs >= ( halfLittleDeltaInUs +
                             refocusingGradientTime +
                             sliceSelectionRephasingTime ) )
    {

      ////// Bipolar double STE formation 1.

      //// building radiofrequency pulses
      refocusingRFPulses.resize( 4 );

      excitationStartingTime = -1.5 * MS_TO_US;
      excitationCenterTime = 0.0 * MS_TO_US;
      excitationEndingTime = 1.5 * MS_TO_US;
      excitationRFPulses[ 0 ].reset( new gkg::RadioFrequencyPulse(
                                           RF_MAGNITUDE_AXIS,
                                           gkg::RadioFrequencyPulse::Excitation,
                                           excitationStartingTime,
                                           excitationCenterTime,
                                           excitationEndingTime ) );

      refocusingCenterTime = excitationCenterTime +
                             0.5 * halfEchoTime;
      refocusingStartingTime = refocusingCenterTime -
                               0.5 * sliceSelectionRephasingTime;
      refocusingEndingTime = refocusingCenterTime +
                             0.5 * sliceSelectionRephasingTime;
      refocusingRFPulses[ 0 ].reset( new gkg::RadioFrequencyPulse(
                                           RF_MAGNITUDE_AXIS,
                                           gkg::RadioFrequencyPulse::Refocusing,
                                           refocusingStartingTime,
                                           refocusingCenterTime,
                                           refocusingEndingTime ) );

      excitationStartingTime += halfEchoTime;
      excitationCenterTime += halfEchoTime;
      excitationEndingTime += halfEchoTime;
      excitationRFPulses[ 1 ].reset( new gkg::RadioFrequencyPulse(
                                           RF_MAGNITUDE_AXIS,
                                           gkg::RadioFrequencyPulse::Excitation,
                                           excitationStartingTime,
                                           excitationCenterTime,
                                           excitationEndingTime ) );

      excitationStartingTime += ( bigDeltaInUs - halfEchoTime );
      excitationCenterTime += ( bigDeltaInUs - halfEchoTime );
      excitationEndingTime += ( bigDeltaInUs - halfEchoTime );
      excitationRFPulses[ 2 ].reset( new gkg::RadioFrequencyPulse(
                                           RF_MAGNITUDE_AXIS,
                                           gkg::RadioFrequencyPulse::Excitation,
                                           excitationStartingTime,
                                           excitationCenterTime,
                                           excitationEndingTime ) );

      refocusingStartingTime += bigDeltaInUs;
      refocusingCenterTime += bigDeltaInUs;
      refocusingEndingTime += bigDeltaInUs;
      refocusingRFPulses[ 1 ].reset( new gkg::RadioFrequencyPulse(
                                           RF_MAGNITUDE_AXIS,
                                           gkg::RadioFrequencyPulse::Refocusing,
                                           refocusingStartingTime,
                                           refocusingCenterTime,
                                           refocusingEndingTime ) );

      refocusingStartingTime += mixingTimeInUs;
      refocusingCenterTime += mixingTimeInUs;
      refocusingEndingTime += mixingTimeInUs;
      refocusingRFPulses[ 2 ].reset( new gkg::RadioFrequencyPulse(
                                           RF_MAGNITUDE_AXIS,
                                           gkg::RadioFrequencyPulse::Refocusing,
                                           refocusingStartingTime,
                                           refocusingCenterTime,
                                           refocusingEndingTime ) );

      excitationStartingTime = refocusingEndingTime +
                               halfLittleDeltaInUs +
                               sliceSelectionRephasingTime;
      excitationCenterTime = excitationStartingTime +
                             0.5 * sliceSelectionGradientTime;
      excitationEndingTime = excitationCenterTime +
                             0.5 * sliceSelectionGradientTime;
      excitationRFPulses[ 3 ].reset( new gkg::RadioFrequencyPulse(
                                           RF_MAGNITUDE_AXIS,
                                           gkg::RadioFrequencyPulse::Excitation,
                                           excitationStartingTime,
                                           excitationCenterTime,
                                           excitationEndingTime ) );

      excitationStartingTime += ( bigDeltaInUs - halfEchoTime );
      excitationCenterTime += ( bigDeltaInUs - halfEchoTime );
      excitationEndingTime += ( bigDeltaInUs - halfEchoTime );
      excitationRFPulses[ 4 ].reset( new gkg::RadioFrequencyPulse(
                                           RF_MAGNITUDE_AXIS,
                                           gkg::RadioFrequencyPulse::Excitation,
                                           excitationStartingTime,
                                           excitationCenterTime,
                                           excitationEndingTime ) );

      refocusingStartingTime += bigDeltaInUs;
      refocusingCenterTime += bigDeltaInUs;
      refocusingEndingTime += bigDeltaInUs;
      refocusingRFPulses[ 3 ].reset( new gkg::RadioFrequencyPulse(
                                           RF_MAGNITUDE_AXIS,
                                           gkg::RadioFrequencyPulse::Refocusing,
                                           refocusingStartingTime,
                                           refocusingCenterTime,
                                           refocusingEndingTime ) );

      //// adding radiofrequency pulses to the pulse sequence;
      rfMagnitudePulseSequence->addPulses( excitationRFPulses );
      rfMagnitudePulseSequence->addPulses( refocusingRFPulses );

      //// adding gradient pulses
      // allocating the wavevector
      gkg::Vector3d< float > wavevector = wavevectors.getOrientation( 0 );

      // building gradient pulses
      float dwLeftPulseStartingTime = 0.5 * sliceSelectionGradientTime + 
                                      sliceSelectionRephasingTime;
      float dwPulseStartingRampDuration = rampWidthInUs;
      float dwPulsePlateauDuration = plateauWidthInUs;
      float dwPulseEndingRampDuration = rampWidthInUs;
      float dwPulseMagnitude = maximumGradientAmplitude;

      // 1st pulsed gradient pair
      leftDWGradientPulsesOnXAxis.reset( new gkg::DiffusionGradientPulse(
                                           GRADIENT_X_AXIS,
                                           dwLeftPulseStartingTime,
                                           dwPulseStartingRampDuration,
                                           dwPulsePlateauDuration,
                                           dwPulseEndingRampDuration,
                                           -dwPulseMagnitude * wavevector.x ) );
      leftDWGradientPulsesOnYAxis.reset( new gkg::DiffusionGradientPulse(
                                           GRADIENT_Y_AXIS,
                                           dwLeftPulseStartingTime,
                                           dwPulseStartingRampDuration,
                                           dwPulsePlateauDuration,
                                           dwPulseEndingRampDuration,
                                           -dwPulseMagnitude * wavevector.y ) );
      leftDWGradientPulsesOnZAxis.reset( new gkg::DiffusionGradientPulse(
                                           GRADIENT_Z_AXIS,
                                           dwLeftPulseStartingTime,
                                           dwPulseStartingRampDuration,
                                           dwPulsePlateauDuration,
                                           dwPulseEndingRampDuration,
                                           -dwPulseMagnitude * wavevector.z ) );

      float dwRightPulseStartingTime = dwLeftPulseStartingTime +
                                       halfLittleDeltaInUs +
                                       refocusingGradientTime;
    
      rightDWGradientPulsesOnXAxis.reset( new gkg::DiffusionGradientPulse(
                                           GRADIENT_X_AXIS,
                                           dwRightPulseStartingTime,
                                           dwPulseStartingRampDuration,
                                           dwPulsePlateauDuration,
                                           dwPulseEndingRampDuration,
                                           dwPulseMagnitude * wavevector.x ) );
      rightDWGradientPulsesOnYAxis.reset( new gkg::DiffusionGradientPulse(
                                           GRADIENT_Y_AXIS,
                                           dwRightPulseStartingTime,
                                           dwPulseStartingRampDuration,
                                           dwPulsePlateauDuration,
                                           dwPulseEndingRampDuration,
                                           dwPulseMagnitude * wavevector.y ) );
      rightDWGradientPulsesOnZAxis.reset( new gkg::DiffusionGradientPulse(
                                           GRADIENT_Z_AXIS,
                                           dwRightPulseStartingTime,
                                           dwPulseStartingRampDuration,
                                           dwPulsePlateauDuration,
                                           dwPulseEndingRampDuration,
                                           dwPulseMagnitude * wavevector.z ) );

      // adding 1st pulsed gradient pair to the gradient pulse sequences
      xGradientPulseSequence->addPulse( leftDWGradientPulsesOnXAxis );
      yGradientPulseSequence->addPulse( leftDWGradientPulsesOnYAxis );
      zGradientPulseSequence->addPulse( leftDWGradientPulsesOnZAxis );
      xGradientPulseSequence->addPulse( rightDWGradientPulsesOnXAxis );
      yGradientPulseSequence->addPulse( rightDWGradientPulsesOnYAxis );
      zGradientPulseSequence->addPulse( rightDWGradientPulsesOnZAxis );

      // 2nd pulsed gradient pair
      dwLeftPulseStartingTime += bigDeltaInUs;
      dwRightPulseStartingTime += bigDeltaInUs;

      leftDWGradientPulsesOnXAxis.reset( new gkg::DiffusionGradientPulse(
                                           GRADIENT_X_AXIS,
                                           dwLeftPulseStartingTime,
                                           dwPulseStartingRampDuration,
                                           dwPulsePlateauDuration,
                                           dwPulseEndingRampDuration,
                                           -dwPulseMagnitude * wavevector.x ) );
      leftDWGradientPulsesOnYAxis.reset( new gkg::DiffusionGradientPulse(
                                           GRADIENT_Y_AXIS,
                                           dwLeftPulseStartingTime,
                                           dwPulseStartingRampDuration,
                                           dwPulsePlateauDuration,
                                           dwPulseEndingRampDuration,
                                           -dwPulseMagnitude * wavevector.y ) );
      leftDWGradientPulsesOnZAxis.reset( new gkg::DiffusionGradientPulse(
                                           GRADIENT_Z_AXIS,
                                           dwLeftPulseStartingTime,
                                           dwPulseStartingRampDuration,
                                           dwPulsePlateauDuration,
                                           dwPulseEndingRampDuration,
                                           -dwPulseMagnitude * wavevector.z ) );
    
      rightDWGradientPulsesOnXAxis.reset( new gkg::DiffusionGradientPulse(
                                           GRADIENT_X_AXIS,
                                           dwRightPulseStartingTime,
                                           dwPulseStartingRampDuration,
                                           dwPulsePlateauDuration,
                                           dwPulseEndingRampDuration,
                                           dwPulseMagnitude * wavevector.x ) );
      rightDWGradientPulsesOnYAxis.reset( new gkg::DiffusionGradientPulse(
                                           GRADIENT_Y_AXIS,
                                           dwRightPulseStartingTime,
                                           dwPulseStartingRampDuration,
                                           dwPulsePlateauDuration,
                                           dwPulseEndingRampDuration,
                                           dwPulseMagnitude * wavevector.y ) );
      rightDWGradientPulsesOnZAxis.reset( new gkg::DiffusionGradientPulse(
                                           GRADIENT_Z_AXIS,
                                           dwRightPulseStartingTime,
                                           dwPulseStartingRampDuration,
                                           dwPulsePlateauDuration,
                                           dwPulseEndingRampDuration,
                                           dwPulseMagnitude * wavevector.z ) );

      // adding 2nd pulsed gradient pair to the gradient pulse sequences
      xGradientPulseSequence->addPulse( leftDWGradientPulsesOnXAxis );
      yGradientPulseSequence->addPulse( leftDWGradientPulsesOnYAxis );
      zGradientPulseSequence->addPulse( leftDWGradientPulsesOnZAxis );
      xGradientPulseSequence->addPulse( rightDWGradientPulsesOnXAxis );
      yGradientPulseSequence->addPulse( rightDWGradientPulsesOnYAxis );
      zGradientPulseSequence->addPulse( rightDWGradientPulsesOnZAxis );

      // 3rd pulsed gradient pair

      wavevector = wavevectors.getOrientation( 1 );

      dwLeftPulseStartingTime += mixingTimeInUs;
      dwRightPulseStartingTime += mixingTimeInUs;

      leftDWGradientPulsesOnXAxis.reset( new gkg::DiffusionGradientPulse(
                                           GRADIENT_X_AXIS,
                                           dwLeftPulseStartingTime,
                                           dwPulseStartingRampDuration,
                                           dwPulsePlateauDuration,
                                           dwPulseEndingRampDuration,
                                           dwPulseMagnitude * wavevector.x ) );
      leftDWGradientPulsesOnYAxis.reset( new gkg::DiffusionGradientPulse(
                                           GRADIENT_Y_AXIS,
                                           dwLeftPulseStartingTime,
                                           dwPulseStartingRampDuration,
                                           dwPulsePlateauDuration,
                                           dwPulseEndingRampDuration,
                                           dwPulseMagnitude * wavevector.y ) );
      leftDWGradientPulsesOnZAxis.reset( new gkg::DiffusionGradientPulse(
                                           GRADIENT_Z_AXIS,
                                           dwLeftPulseStartingTime,
                                           dwPulseStartingRampDuration,
                                           dwPulsePlateauDuration,
                                           dwPulseEndingRampDuration,
                                           dwPulseMagnitude * wavevector.z ) );
    
      rightDWGradientPulsesOnXAxis.reset( new gkg::DiffusionGradientPulse(
                                           GRADIENT_X_AXIS,
                                           dwRightPulseStartingTime,
                                           dwPulseStartingRampDuration,
                                           dwPulsePlateauDuration,
                                           dwPulseEndingRampDuration,
                                           -dwPulseMagnitude * wavevector.x ) );
      rightDWGradientPulsesOnYAxis.reset( new gkg::DiffusionGradientPulse(
                                           GRADIENT_Y_AXIS,
                                           dwRightPulseStartingTime,
                                           dwPulseStartingRampDuration,
                                           dwPulsePlateauDuration,
                                           dwPulseEndingRampDuration,
                                           -dwPulseMagnitude * wavevector.y ) );
      rightDWGradientPulsesOnZAxis.reset( new gkg::DiffusionGradientPulse(
                                           GRADIENT_Z_AXIS,
                                           dwRightPulseStartingTime,
                                           dwPulseStartingRampDuration,
                                           dwPulsePlateauDuration,
                                           dwPulseEndingRampDuration,
                                           -dwPulseMagnitude * wavevector.z ) );

      // adding 3rd pulsed gradient pair to the gradient pulse sequences
      xGradientPulseSequence->addPulse( leftDWGradientPulsesOnXAxis );
      yGradientPulseSequence->addPulse( leftDWGradientPulsesOnYAxis );
      zGradientPulseSequence->addPulse( leftDWGradientPulsesOnZAxis );
      xGradientPulseSequence->addPulse( rightDWGradientPulsesOnXAxis );
      yGradientPulseSequence->addPulse( rightDWGradientPulsesOnYAxis );
      zGradientPulseSequence->addPulse( rightDWGradientPulsesOnZAxis );

      // 4th pulsed gradient pair
      dwLeftPulseStartingTime += bigDeltaInUs;
      dwRightPulseStartingTime += bigDeltaInUs;

      leftDWGradientPulsesOnXAxis.reset( new gkg::DiffusionGradientPulse(
                                           GRADIENT_X_AXIS,
                                           dwLeftPulseStartingTime,
                                           dwPulseStartingRampDuration,
                                           dwPulsePlateauDuration,
                                           dwPulseEndingRampDuration,
                                           dwPulseMagnitude * wavevector.x ) );
      leftDWGradientPulsesOnYAxis.reset( new gkg::DiffusionGradientPulse(
                                           GRADIENT_Y_AXIS,
                                           dwLeftPulseStartingTime,
                                           dwPulseStartingRampDuration,
                                           dwPulsePlateauDuration,
                                           dwPulseEndingRampDuration,
                                           dwPulseMagnitude * wavevector.y ) );
      leftDWGradientPulsesOnZAxis.reset( new gkg::DiffusionGradientPulse(
                                           GRADIENT_Z_AXIS,
                                           dwLeftPulseStartingTime,
                                           dwPulseStartingRampDuration,
                                           dwPulsePlateauDuration,
                                           dwPulseEndingRampDuration,
                                           dwPulseMagnitude * wavevector.z ) );
    
      rightDWGradientPulsesOnXAxis.reset( new gkg::DiffusionGradientPulse(
                                           GRADIENT_X_AXIS,
                                           dwRightPulseStartingTime,
                                           dwPulseStartingRampDuration,
                                           dwPulsePlateauDuration,
                                           dwPulseEndingRampDuration,
                                           -dwPulseMagnitude * wavevector.x ) );
      rightDWGradientPulsesOnYAxis.reset( new gkg::DiffusionGradientPulse(
                                           GRADIENT_Y_AXIS,
                                           dwRightPulseStartingTime,
                                           dwPulseStartingRampDuration,
                                           dwPulsePlateauDuration,
                                           dwPulseEndingRampDuration,
                                           -dwPulseMagnitude * wavevector.y ) );
      rightDWGradientPulsesOnZAxis.reset( new gkg::DiffusionGradientPulse(
                                           GRADIENT_Z_AXIS,
                                           dwRightPulseStartingTime,
                                           dwPulseStartingRampDuration,
                                           dwPulsePlateauDuration,
                                           dwPulseEndingRampDuration,
                                           -dwPulseMagnitude * wavevector.z ) );

      // adding 4th pulsed gradient pair to the gradient pulse sequences
      xGradientPulseSequence->addPulse( leftDWGradientPulsesOnXAxis );
      yGradientPulseSequence->addPulse( leftDWGradientPulsesOnYAxis );
      zGradientPulseSequence->addPulse( leftDWGradientPulsesOnZAxis );
      xGradientPulseSequence->addPulse( rightDWGradientPulsesOnXAxis );
      yGradientPulseSequence->addPulse( rightDWGradientPulsesOnYAxis );
      zGradientPulseSequence->addPulse( rightDWGradientPulsesOnZAxis );

    }
    else
    {

      ////// Bipolar double STE formation 2.

      //// building radiofrequency pulses
      refocusingRFPulses.resize( 3 );

      excitationStartingTime = -1.5 * MS_TO_US;
      excitationCenterTime = 0.0 * MS_TO_US;
      excitationEndingTime = 1.5 * MS_TO_US;
      excitationRFPulses[ 0 ].reset( new gkg::RadioFrequencyPulse(
                                           RF_MAGNITUDE_AXIS,
                                           gkg::RadioFrequencyPulse::Excitation,
                                           excitationStartingTime,
                                           excitationCenterTime,
                                           excitationEndingTime ) );

      refocusingStartingTime = excitationStartingTime + 0.5 * halfEchoTime;
      refocusingCenterTime = excitationCenterTime + 0.5 * halfEchoTime;
      refocusingEndingTime = excitationEndingTime + 0.5 * halfEchoTime;
      refocusingRFPulses[ 0 ].reset( new gkg::RadioFrequencyPulse(
                                           RF_MAGNITUDE_AXIS,
                                           gkg::RadioFrequencyPulse::Refocusing,
                                           refocusingStartingTime,
                                           refocusingCenterTime,
                                           refocusingEndingTime ) );

      excitationStartingTime += halfEchoTime;
      excitationCenterTime += halfEchoTime;
      excitationEndingTime += halfEchoTime;
      excitationRFPulses[ 1 ].reset( new gkg::RadioFrequencyPulse(
                                           RF_MAGNITUDE_AXIS,
                                           gkg::RadioFrequencyPulse::Excitation,
                                           excitationStartingTime,
                                           excitationCenterTime,
                                           excitationEndingTime ) );

      excitationStartingTime += ( bigDeltaInUs - halfEchoTime );
      excitationCenterTime += ( bigDeltaInUs - halfEchoTime );
      excitationEndingTime += ( bigDeltaInUs - halfEchoTime );
      excitationRFPulses[ 2 ].reset( new gkg::RadioFrequencyPulse(
                                           RF_MAGNITUDE_AXIS,
                                           gkg::RadioFrequencyPulse::Excitation,
                                           excitationStartingTime,
                                           excitationCenterTime,
                                           excitationEndingTime ) );

      refocusingStartingTime += bigDeltaInUs;
      refocusingCenterTime += bigDeltaInUs;
      refocusingEndingTime += bigDeltaInUs;
      refocusingRFPulses[ 1 ].reset( new gkg::RadioFrequencyPulse(
                                           RF_MAGNITUDE_AXIS,
                                           gkg::RadioFrequencyPulse::Refocusing,
                                           refocusingStartingTime,
                                           refocusingCenterTime,
                                           refocusingEndingTime ) );

      excitationStartingTime += halfEchoTime;
      excitationCenterTime += halfEchoTime;
      excitationEndingTime += halfEchoTime;
      excitationRFPulses[ 3 ].reset( new gkg::RadioFrequencyPulse(
                                           RF_MAGNITUDE_AXIS,
                                           gkg::RadioFrequencyPulse::Excitation,
                                           excitationStartingTime,
                                           excitationCenterTime,
                                           excitationEndingTime ) );

      excitationStartingTime += ( bigDeltaInUs - halfEchoTime );
      excitationCenterTime += ( bigDeltaInUs - halfEchoTime );
      excitationEndingTime += ( bigDeltaInUs - halfEchoTime );
      excitationRFPulses[ 4 ].reset( new gkg::RadioFrequencyPulse(
                                           RF_MAGNITUDE_AXIS,
                                           gkg::RadioFrequencyPulse::Excitation,
                                           excitationStartingTime,
                                           excitationCenterTime,
                                           excitationEndingTime ) );

      refocusingStartingTime += bigDeltaInUs;
      refocusingCenterTime += bigDeltaInUs;
      refocusingEndingTime += bigDeltaInUs;
      refocusingRFPulses[ 2 ].reset( new gkg::RadioFrequencyPulse(
                                           RF_MAGNITUDE_AXIS,
                                           gkg::RadioFrequencyPulse::Refocusing,
                                           refocusingStartingTime,
                                           refocusingCenterTime,
                                           refocusingEndingTime ) );

      //// adding radiofrequency pulses to the pulse sequence;
      rfMagnitudePulseSequence->addPulses( excitationRFPulses );
      rfMagnitudePulseSequence->addPulses( refocusingRFPulses );

      //// adding gradient pulses

      // building gradient pulses
      float dwLeftPulseStartingTime = 0.5 * sliceSelectionGradientTime + 
                                      sliceSelectionRephasingTime;
      float dwPulseStartingRampDuration = rampWidthInUs;
      float dwPulsePlateauDuration = plateauWidthInUs;
      float dwPulseEndingRampDuration = rampWidthInUs;
      float dwPulseMagnitude = maximumGradientAmplitude;

      // 1st pulsed gradient pair
      leftDWGradientPulsesOnXAxis.reset( new gkg::DiffusionGradientPulse(
                                               GRADIENT_X_AXIS,
                                               dwLeftPulseStartingTime,
                                               dwPulseStartingRampDuration,
                                               dwPulsePlateauDuration,
                                               dwPulseEndingRampDuration,
                                               -0.5 * dwPulseMagnitude ) );
      leftDWGradientPulsesOnYAxis.reset( new gkg::DiffusionGradientPulse(
                                               GRADIENT_Y_AXIS,
                                               dwLeftPulseStartingTime,
                                               dwPulseStartingRampDuration,
                                               dwPulsePlateauDuration,
                                               dwPulseEndingRampDuration,
                                               -0.5 * dwPulseMagnitude ) );
      leftDWGradientPulsesOnZAxis.reset( new gkg::DiffusionGradientPulse(
                                               GRADIENT_Z_AXIS,
                                               dwLeftPulseStartingTime,
                                               dwPulseStartingRampDuration,
                                               dwPulsePlateauDuration,
                                               dwPulseEndingRampDuration,
                                               -0.5 * dwPulseMagnitude ) );

      float dwRightPulseStartingTime = dwLeftPulseStartingTime +
                                       halfLittleDeltaInUs +
                                       refocusingGradientTime;
    
      rightDWGradientPulsesOnXAxis.reset( new gkg::DiffusionGradientPulse(
                                                GRADIENT_X_AXIS,
                                                dwRightPulseStartingTime,
                                                dwPulseStartingRampDuration,
                                                dwPulsePlateauDuration,
                                                dwPulseEndingRampDuration,
                                                0.5 * dwPulseMagnitude ) );
      rightDWGradientPulsesOnYAxis.reset( new gkg::DiffusionGradientPulse(
                                                GRADIENT_Y_AXIS,
                                                dwRightPulseStartingTime,
                                                dwPulseStartingRampDuration,
                                                dwPulsePlateauDuration,
                                                dwPulseEndingRampDuration,
                                                0.5 * dwPulseMagnitude ) );
      rightDWGradientPulsesOnZAxis.reset( new gkg::DiffusionGradientPulse(
                                                GRADIENT_Z_AXIS,
                                                dwRightPulseStartingTime,
                                                dwPulseStartingRampDuration,
                                                dwPulsePlateauDuration,
                                                dwPulseEndingRampDuration,
                                                0.5 * dwPulseMagnitude ) );

      // adding 1st pulsed gradient pair to the gradient pulse sequences
      xGradientPulseSequence->addPulse( leftDWGradientPulsesOnXAxis );
      yGradientPulseSequence->addPulse( leftDWGradientPulsesOnYAxis );
      zGradientPulseSequence->addPulse( leftDWGradientPulsesOnZAxis );
      xGradientPulseSequence->addPulse( rightDWGradientPulsesOnXAxis );
      yGradientPulseSequence->addPulse( rightDWGradientPulsesOnYAxis );
      zGradientPulseSequence->addPulse( rightDWGradientPulsesOnZAxis );

      // 2nd pulsed gradient pair
      dwLeftPulseStartingTime += bigDeltaInUs;
      dwRightPulseStartingTime += bigDeltaInUs;

      leftDWGradientPulsesOnXAxis.reset( new gkg::DiffusionGradientPulse(
                                               GRADIENT_X_AXIS,
                                               dwLeftPulseStartingTime,
                                               dwPulseStartingRampDuration,
                                               dwPulsePlateauDuration,
                                               dwPulseEndingRampDuration,
                                               -dwPulseMagnitude ) );
      leftDWGradientPulsesOnYAxis.reset( new gkg::DiffusionGradientPulse(
                                               GRADIENT_Y_AXIS,
                                               dwLeftPulseStartingTime,
                                               dwPulseStartingRampDuration,
                                               dwPulsePlateauDuration,
                                               dwPulseEndingRampDuration,
                                               -dwPulseMagnitude ) );
      leftDWGradientPulsesOnZAxis.reset( new gkg::DiffusionGradientPulse(
                                               GRADIENT_Z_AXIS,
                                               dwLeftPulseStartingTime,
                                               dwPulseStartingRampDuration,
                                               dwPulsePlateauDuration,
                                               dwPulseEndingRampDuration,
                                               -dwPulseMagnitude ) );
    
      rightDWGradientPulsesOnXAxis.reset( new gkg::DiffusionGradientPulse(
                                                GRADIENT_X_AXIS,
                                                dwRightPulseStartingTime,
                                                dwPulseStartingRampDuration,
                                                dwPulsePlateauDuration,
                                                dwPulseEndingRampDuration,
                                                dwPulseMagnitude ) );
      rightDWGradientPulsesOnYAxis.reset( new gkg::DiffusionGradientPulse(
                                                GRADIENT_Y_AXIS,
                                                dwRightPulseStartingTime,
                                                dwPulseStartingRampDuration,
                                                dwPulsePlateauDuration,
                                                dwPulseEndingRampDuration,
                                                dwPulseMagnitude ) );
      rightDWGradientPulsesOnZAxis.reset( new gkg::DiffusionGradientPulse(
                                                GRADIENT_Z_AXIS,
                                                dwRightPulseStartingTime,
                                                dwPulseStartingRampDuration,
                                                dwPulsePlateauDuration,
                                                dwPulseEndingRampDuration,
                                                dwPulseMagnitude ) );

      // adding 2nd pulsed gradient pair to the gradient pulse sequences
      xGradientPulseSequence->addPulse( leftDWGradientPulsesOnXAxis );
      yGradientPulseSequence->addPulse( leftDWGradientPulsesOnYAxis );
      zGradientPulseSequence->addPulse( leftDWGradientPulsesOnZAxis );
      xGradientPulseSequence->addPulse( rightDWGradientPulsesOnXAxis );
      yGradientPulseSequence->addPulse( rightDWGradientPulsesOnYAxis );
      zGradientPulseSequence->addPulse( rightDWGradientPulsesOnZAxis );

      // 3rd pulsed gradient pair
      dwLeftPulseStartingTime += bigDeltaInUs;
      dwRightPulseStartingTime += bigDeltaInUs;

      leftDWGradientPulsesOnXAxis.reset( new gkg::DiffusionGradientPulse(
                                               GRADIENT_X_AXIS,
                                               dwLeftPulseStartingTime,
                                               dwPulseStartingRampDuration,
                                               dwPulsePlateauDuration,
                                               dwPulseEndingRampDuration,
                                               -0.5 * dwPulseMagnitude ) );
      leftDWGradientPulsesOnYAxis.reset( new gkg::DiffusionGradientPulse(
                                               GRADIENT_Y_AXIS,
                                               dwLeftPulseStartingTime,
                                               dwPulseStartingRampDuration,
                                               dwPulsePlateauDuration,
                                               dwPulseEndingRampDuration,
                                               -0.5 * dwPulseMagnitude ) );
      leftDWGradientPulsesOnZAxis.reset( new gkg::DiffusionGradientPulse(
                                               GRADIENT_Z_AXIS,
                                               dwLeftPulseStartingTime,
                                               dwPulseStartingRampDuration,
                                               dwPulsePlateauDuration,
                                               dwPulseEndingRampDuration,
                                               -0.5 * dwPulseMagnitude ) );
    
      rightDWGradientPulsesOnXAxis.reset( new gkg::DiffusionGradientPulse(
                                                GRADIENT_X_AXIS,
                                                dwRightPulseStartingTime,
                                                dwPulseStartingRampDuration,
                                                dwPulsePlateauDuration,
                                                dwPulseEndingRampDuration,
                                                0.5 * dwPulseMagnitude ) );
      rightDWGradientPulsesOnYAxis.reset( new gkg::DiffusionGradientPulse(
                                                GRADIENT_Y_AXIS,
                                                dwRightPulseStartingTime,
                                                dwPulseStartingRampDuration,
                                                dwPulsePlateauDuration,
                                                dwPulseEndingRampDuration,
                                                0.5 * dwPulseMagnitude ) );
      rightDWGradientPulsesOnZAxis.reset( new gkg::DiffusionGradientPulse(
                                                GRADIENT_Z_AXIS,
                                                dwRightPulseStartingTime,
                                                dwPulseStartingRampDuration,
                                                dwPulsePlateauDuration,
                                                dwPulseEndingRampDuration,
                                                0.5 * dwPulseMagnitude ) );

      // adding 3rd pulsed gradient pair to the gradient pulse sequences
      xGradientPulseSequence->addPulse( leftDWGradientPulsesOnXAxis );
      yGradientPulseSequence->addPulse( leftDWGradientPulsesOnYAxis );
      zGradientPulseSequence->addPulse( leftDWGradientPulsesOnZAxis );
      xGradientPulseSequence->addPulse( rightDWGradientPulsesOnXAxis );
      yGradientPulseSequence->addPulse( rightDWGradientPulsesOnYAxis );
      zGradientPulseSequence->addPulse( rightDWGradientPulsesOnZAxis );

    }

    ///////////////////////// adding pulse sequences ///////////////////////////
    this->addPulseSequence( rfMagnitudePulseSequence );
    this->addPulseSequence( rfPhasePulseSequence );
    this->addPulseSequence( xGradientPulseSequence );
    this->addPulseSequence( yGradientPulseSequence );
    this->addPulseSequence( zGradientPulseSequence );

    // finally setting echo time
    this->setEchoTimeInUs( 0.5 * sliceSelectionGradientTime +
                           2.0 * ( sliceSelectionRephasingTime + bigDeltaInUs +
                                   halfLittleDeltaInUs ) +
                           mixingTimeInUs +
                           refocusingGradientTime );

  }
  GKG_CATCH( "gkg::BipolarDoubleSTENMRSequence:: "
             "BipolarDoubleSTENMRSequence( "
             "const std::set< float >& gradientAmplitudes, "
             "const std::vector< float >& scalarParameters, "
             "const std::vector< std::string >& stringParameters, "
             "bool verbose )" );

}


gkg::BipolarDoubleSTENMRSequence::~BipolarDoubleSTENMRSequence()
{
}


void gkg::BipolarDoubleSTENMRSequence::checkOrInitializeDefaultParameters(
                                  std::vector< float >& scalarParameters,
                                  std::vector< std::string >& stringParameters )
{

  try
  {

    if ( scalarParameters.size() != 5U )
    {

      throw std::runtime_error( "invalid scalar parameter count" );

    }
    if ( !stringParameters.empty() != 1U )
    {

      throw std::runtime_error( "invalid string parameter count" );

    }

  }
  GKG_CATCH( "void gkg::BipolarDoubleSTENMRSequence:: "
             "checkOrInitializeDefaultParameters( "
             "std::vector< float >& scalarParameters, "
             "std::vector< std::string >& stringParameters )" );

}


float gkg::BipolarDoubleSTENMRSequence::getDiffusionPulsePlateauWidthInUs(
                                      float maximuGradientAmplitudeInGaussPerCm,
                                      float maximumSlewRateInGaussPerCmPerUs,
                                      float littleDeltaInUs,
                                      float gradientTimeResolution,
                                      float& rampWidthInUs )
{

  try
  {

    float plateauWidthInUs = 0.0;

    // processing the ramp width
    rampWidthInUs = ( float )RUP_GRD( ( int32_t )
                                      ( maximuGradientAmplitudeInGaussPerCm /
                                        maximumSlewRateInGaussPerCmPerUs ) );

    plateauWidthInUs = littleDeltaInUs - 2.0 * rampWidthInUs;
    // put plateau width on GRAD_UPDATE boundaries
    plateauWidthInUs = ( float )RUP_GRD( plateauWidthInUs );

    if ( plateauWidthInUs <= 0.0 )
    {

      throw std::runtime_error( "Invalid gradient plateau width." );

    }

    return plateauWidthInUs;

  }
  GKG_CATCH( "float "
             "gkg::BipolarDoubleSTENMRSequence:: "
             "getDiffusionPulsePlateauWidthInUs( "
             "float maximuGradientAmplitudeInGaussPerCm, "
             "float maximumSlewRateInGaussPerCmPerUs, "
             "float littleDeltaInUs, "
             "float gradientTimeResolution, "
             "float& rampWidthInUs )" );

}


#undef TWOPI_GAM
#undef GAM

#undef MT_PER_M_TO_G_PER_CM
#undef T_PER_M_PER_S_TO_G_PER_CM_PER_US
#undef MS_TO_US

#undef RUP_GRD

