#ifndef _gkg_dmri_simulator_pulse_sequence_BipolarDoubleSTENMRSequence_h_
#define _gkg_dmri_simulator_pulse_sequence_BipolarDoubleSTENMRSequence_h_


#include <gkg-dmri-simulator-pulse-sequence/NMRSequence.h>


// ==================== Bipolar Double STE sequence diagram ====================
//
// reference: N. Shemesh et al., J. Chem. Phys. 133, 044705 (2010).
//
// =============================================================================


namespace gkg
{


class BipolarDoubleSTENMRSequence : public NMRSequence
{

  public:

    // gradient amplitudes should be given in mT/m
    // maximum slew rate should be given in T/m/s
    // little delta and big delta should be given in ms
    // gradient time resolution should be given in us
    BipolarDoubleSTENMRSequence(
                             const std::set< float >& gradientAmplitudes,
                             const std::vector< float >& scalarParameters,
                             const std::vector< std::string >& stringParameters,
                             bool verbose );
    virtual ~BipolarDoubleSTENMRSequence();

    static void checkOrInitializeDefaultParameters(
                                 std::vector< float >& scalarParameters,
                                 std::vector< std::string >& stringParameters );

  protected:

    float getDiffusionPulsePlateauWidthInUs(
                                      float maximuGradientAmplitudeInGaussPerCm,
                                      float maximumSlewRateInGaussPerCmPerUs,
                                      float littleDeltaInUs,
                                      float gradientTimeResolution,
                                      float& rampWidthInUs );

};


}


#endif
