#include <gkg-dmri-simulator-pulse-sequence/BipolarSTENMRSequence.h>
#include <gkg-dmri-simulator-pulse-sequence/RadioFrequencyPulse.h>
#include <gkg-dmri-simulator-pulse-sequence/DiffusionGradientPulse.h>
#include <gkg-core-exception/Exception.h>
#include <cmath>


#define TWOPI_GAM 26748.0
#define GAM 26748.0/ (2*M_PI)

#define MT_PER_M_TO_G_PER_CM              0.1
#define T_PER_M_PER_S_TO_G_PER_CM_PER_US  0.0001
#define MS_TO_US                          1000.0

#define RUP_GRD( A )  ( ( ( int32_t )gradientTimeResolution -       \
                          ( ( int32_t )( A ) %                      \
                            ( int32_t )gradientTimeResolution ) ) % \
                          ( int32_t )gradientTimeResolution +       \
                        ( int32_t )( A ) )


gkg::BipolarSTENMRSequence::BipolarSTENMRSequence(
                         const std::set< float >& gradientAmplitudes,
                         const std::vector< float >& scalarParameters,
                         const std::vector< std::string >& /*stringParameters*/,
                         bool verbose )
                           : gkg::NMRSequence( gradientAmplitudes, verbose )
{

  try
  {

    // collecting sequence scalar parameters
    float maximumSlewRate = scalarParameters[ 0 ];
    float gradientTimeResolution = scalarParameters[ 1 ];
    float littleDeltaInMs = scalarParameters[ 2 ];
    float bigDeltaInMs = scalarParameters[ 3 ];

    //
    // processing the diffusion gradient parameters
    //
    float maximumGradientAmplitude = this->_gradientAmplitudes.back();
    float maximumGradientAmplitudeInGaussPerCm = maximumGradientAmplitude *
                                                 MT_PER_M_TO_G_PER_CM;
    float maximumSlewRateInGaussPerCmPerUs = maximumSlewRate *
                                             T_PER_M_PER_S_TO_G_PER_CM_PER_US;

    float halfLittleDeltaInUs = 0.5 * littleDeltaInMs * MS_TO_US;
    float bigDeltaInUs = bigDeltaInMs * MS_TO_US;
    float rampWidthInUs = 0.0;
    float plateauWidthInUs = getDiffusionPulsePlateauWidthInUs(
                                           maximumGradientAmplitudeInGaussPerCm,
                                           maximumSlewRateInGaussPerCmPerUs,
                                           halfLittleDeltaInUs,
                                           gradientTimeResolution,
                                           rampWidthInUs );

    _effectiveDiffusionTimeInMs = bigDeltaInMs - littleDeltaInMs / 3.0;
    _maximumQInMeterInverse = GAM * 1e-2 * littleDeltaInMs *
                              maximumGradientAmplitude;

    //
    // processing the mixing time
    //
    // assuming excitation and refocusing pulses are 3.0 and 6.0 ms respectively
    // the slice selection rephasing gradient duration = 0.75 ms
    float sliceSelectionRephasingTimeInUs = 0.75 * MS_TO_US;
    float mixingTimeInUs = bigDeltaInUs -
                           2.0 * halfLittleDeltaInUs -
                           9.0 * MS_TO_US -
                           2.0 * sliceSelectionRephasingTimeInUs;
    float _mixingTimeInMs = mixingTimeInUs / MS_TO_US;

    //
    // sanity check
    //
    if ( _mixingTimeInMs < 3.75 )
    {

      throw std::runtime_error( "Invalid delta/DELTA combination." );

    }

    //
    // creating and adding pulses to the NMR sequence
    //

    ///////////// creating RF + Gslice + Gphase + Gread pulse sequences ////////
    gkg::RCPointer< gkg::PulseSequence >
      rfMagnitudePulseSequence( new gkg::PulseSequence( RF_MAGNITUDE_AXIS ) );

    gkg::RCPointer< gkg::PulseSequence >
      rfPhasePulseSequence( new gkg::PulseSequence( RF_PHASE_AXIS ) );

    gkg::RCPointer< gkg::PulseSequence >
      xGradientPulseSequence( new gkg::PulseSequence( GRADIENT_X_AXIS ) );

    gkg::RCPointer< gkg::PulseSequence >
      yGradientPulseSequence( new gkg::PulseSequence( GRADIENT_Y_AXIS ) );

    gkg::RCPointer< gkg::PulseSequence >
      zGradientPulseSequence( new gkg::PulseSequence( GRADIENT_Z_AXIS ) );

    /////////////////////////// focusing on RF pulses //////////////////////////

    // adding the 1st excitation pulse
    float firstExcitationStartingTime = -1.5 * MS_TO_US;
    float firstExcitationCentreTime = 0.0 * MS_TO_US;
    float firstExcitationEndingTime = 1.5 * MS_TO_US;
    gkg::RCPointer< gkg::Pulse >
      firstExcitationRFPulse( new gkg::RadioFrequencyPulse(
                                           RF_MAGNITUDE_AXIS,
                                           gkg::RadioFrequencyPulse::Excitation,
                                           firstExcitationStartingTime,
                                           firstExcitationCentreTime,
                          	           firstExcitationEndingTime ) );

    // adding the 1st refocusing pulse
    float firstRefocusingStartingTime = firstExcitationEndingTime +
                                        sliceSelectionRephasingTimeInUs +
                                        halfLittleDeltaInUs;
    float firstRefocusingCentreTime = firstRefocusingStartingTime +
                                      3.0 * MS_TO_US;
    float firstRefocusingEndingTime = firstRefocusingCentreTime +
                                      3.0 * MS_TO_US;
    gkg::RCPointer< gkg::Pulse >
      firstRefocusingRFPulse( new gkg::RadioFrequencyPulse(
                                           RF_MAGNITUDE_AXIS,
                                           gkg::RadioFrequencyPulse::Refocusing,
                                           firstRefocusingStartingTime,
                                           firstRefocusingCentreTime,
                                           firstRefocusingEndingTime ) );

    // adding the 2nd excitation pulse
    float secondExcitationStartingTime = firstRefocusingEndingTime +
                                         halfLittleDeltaInUs +
                                         sliceSelectionRephasingTimeInUs;
    float secondExcitationCentreTime = secondExcitationStartingTime +
                                       1.5 * MS_TO_US;
    float secondExcitationEndingTime = secondExcitationCentreTime +
                                       1.5 * MS_TO_US;
    gkg::RCPointer< gkg::Pulse >
      secondExcitationRFPulse( new gkg::RadioFrequencyPulse(
                                           RF_MAGNITUDE_AXIS,
                                           gkg::RadioFrequencyPulse::Excitation,
                                           secondExcitationStartingTime,
                                           secondExcitationCentreTime,
                          	           secondExcitationEndingTime ) );

    // adding the 3rd excitation pulse
    float thirdExcitationStartingTime = secondExcitationStartingTime +
                                        mixingTimeInUs;
    float thirdExcitationCentreTime = thirdExcitationStartingTime +
                                      1.5 * MS_TO_US;
    float thirdExcitationEndingTime = thirdExcitationCentreTime +
                                      1.5 * MS_TO_US;
    gkg::RCPointer< gkg::Pulse >
      thirdExcitationRFPulse( new gkg::RadioFrequencyPulse(
                                           RF_MAGNITUDE_AXIS,
                                           gkg::RadioFrequencyPulse::Excitation,
                                           thirdExcitationStartingTime,
                                           thirdExcitationCentreTime,
                          	           thirdExcitationEndingTime ) );

    // adding the 2nd refocusing pulse
    float secondRefocusingStartingTime = thirdExcitationEndingTime +
                                         sliceSelectionRephasingTimeInUs +
                                         halfLittleDeltaInUs;
    float secondRefocusingCentreTime = secondRefocusingStartingTime +
                                       3.0 * MS_TO_US;
    float secondRefocusingEndingTime = secondRefocusingCentreTime +
                                       3.0 * MS_TO_US;
    gkg::RCPointer< gkg::Pulse >
      secondRefocusingRFPulse( new gkg::RadioFrequencyPulse(
                                           RF_MAGNITUDE_AXIS,
                                           gkg::RadioFrequencyPulse::Refocusing,
                                           secondRefocusingStartingTime,
                                           secondRefocusingCentreTime,
                                           secondRefocusingEndingTime ) );

    // adding RF pulses to the RF pulse sequences
    rfMagnitudePulseSequence->addPulse( firstExcitationRFPulse );
    rfMagnitudePulseSequence->addPulse( secondExcitationRFPulse );
    rfMagnitudePulseSequence->addPulse( thirdExcitationRFPulse );

    rfMagnitudePulseSequence->addPulse( firstRefocusingRFPulse );
    rfMagnitudePulseSequence->addPulse( secondRefocusingRFPulse );

    /////////////////////// focusing on gradient pulses ////////////////////////

    ////// allocating DW gradient pulses
    gkg::RCPointer< gkg::Pulse > leftDWGradientPulsesOnXAxis;
    gkg::RCPointer< gkg::Pulse > leftDWGradientPulsesOnYAxis;
    gkg::RCPointer< gkg::Pulse > leftDWGradientPulsesOnZAxis;
    gkg::RCPointer< gkg::Pulse > rightDWGradientPulsesOnXAxis;
    gkg::RCPointer< gkg::Pulse > rightDWGradientPulsesOnYAxis;
    gkg::RCPointer< gkg::Pulse > rightDWGradientPulsesOnZAxis;

    ////// left diffusion-sensitization gradient pair
    float dwLeftPulseStartingTime = firstExcitationEndingTime + 
                                    sliceSelectionRephasingTimeInUs;
    float dwPulseStartingRampDuration = rampWidthInUs;
    float dwPulsePlateauDuration = plateauWidthInUs;
    float dwPulseEndingRampDuration = rampWidthInUs;
    float dwPulseMagnitude = maximumGradientAmplitude;

    // 1st pulse
    leftDWGradientPulsesOnXAxis.reset( new gkg::DiffusionGradientPulse(
                                             GRADIENT_X_AXIS,
                                             dwLeftPulseStartingTime,
                                             dwPulseStartingRampDuration,
                                             dwPulsePlateauDuration,
                                             dwPulseEndingRampDuration,
                                             -dwPulseMagnitude ) );
    leftDWGradientPulsesOnYAxis.reset( new gkg::DiffusionGradientPulse(
                                             GRADIENT_Y_AXIS,
                                             dwLeftPulseStartingTime,
                                             dwPulseStartingRampDuration,
                                             dwPulsePlateauDuration,
                                             dwPulseEndingRampDuration,
                                             -dwPulseMagnitude ) );
    leftDWGradientPulsesOnZAxis.reset( new gkg::DiffusionGradientPulse(
                                             GRADIENT_Z_AXIS,
                                             dwLeftPulseStartingTime,
                                             dwPulseStartingRampDuration,
                                             dwPulsePlateauDuration,
                                             dwPulseEndingRampDuration,
                                             -dwPulseMagnitude ) );

    // 2nd pulse
    float dwRightPulseStartingTime = dwLeftPulseStartingTime +
                                     halfLittleDeltaInUs + 6.0 * MS_TO_US;
    
    rightDWGradientPulsesOnXAxis.reset( new gkg::DiffusionGradientPulse(
                                              GRADIENT_X_AXIS,
                                              dwRightPulseStartingTime,
                                              dwPulseStartingRampDuration,
                                              dwPulsePlateauDuration,
                                              dwPulseEndingRampDuration,
                                              dwPulseMagnitude ) );
    rightDWGradientPulsesOnYAxis.reset( new gkg::DiffusionGradientPulse(
                                              GRADIENT_Y_AXIS,
                                              dwRightPulseStartingTime,
                                              dwPulseStartingRampDuration,
                                              dwPulsePlateauDuration,
                                              dwPulseEndingRampDuration,
                                              dwPulseMagnitude ) );
    rightDWGradientPulsesOnZAxis.reset( new gkg::DiffusionGradientPulse(
                                              GRADIENT_Z_AXIS,
                                              dwRightPulseStartingTime,
                                              dwPulseStartingRampDuration,
                                              dwPulsePlateauDuration,
                                              dwPulseEndingRampDuration,
                                              dwPulseMagnitude ) );

    // adding left DW gradient pair to the gradient pulse sequences
    xGradientPulseSequence->addPulse( leftDWGradientPulsesOnXAxis );
    yGradientPulseSequence->addPulse( leftDWGradientPulsesOnYAxis );
    zGradientPulseSequence->addPulse( leftDWGradientPulsesOnZAxis );
    xGradientPulseSequence->addPulse( rightDWGradientPulsesOnXAxis );
    yGradientPulseSequence->addPulse( rightDWGradientPulsesOnYAxis );
    zGradientPulseSequence->addPulse( rightDWGradientPulsesOnZAxis );

    ////// right diffusion-sensitization gradient pair
    dwLeftPulseStartingTime += bigDeltaInUs;
    dwRightPulseStartingTime += bigDeltaInUs;

    // 1st pulse
    leftDWGradientPulsesOnXAxis.reset( new gkg::DiffusionGradientPulse(
                                             GRADIENT_X_AXIS,
                                             dwLeftPulseStartingTime,
                                             dwPulseStartingRampDuration,
                                             dwPulsePlateauDuration,
                                             dwPulseEndingRampDuration,
                                             -dwPulseMagnitude ) );
    leftDWGradientPulsesOnYAxis.reset( new gkg::DiffusionGradientPulse(
                                             GRADIENT_Y_AXIS,
                                             dwLeftPulseStartingTime,
                                             dwPulseStartingRampDuration,
                                             dwPulsePlateauDuration,
                                             dwPulseEndingRampDuration,
                                             -dwPulseMagnitude ) );
    leftDWGradientPulsesOnZAxis.reset( new gkg::DiffusionGradientPulse(
                                             GRADIENT_Z_AXIS,
                                             dwLeftPulseStartingTime,
                                             dwPulseStartingRampDuration,
                                             dwPulsePlateauDuration,
                                             dwPulseEndingRampDuration,
                                             -dwPulseMagnitude ) );

    // 2nd pulse
    rightDWGradientPulsesOnXAxis.reset( new gkg::DiffusionGradientPulse(
                                              GRADIENT_X_AXIS,
                                              dwRightPulseStartingTime,
                                              dwPulseStartingRampDuration,
                                              dwPulsePlateauDuration,
                                              dwPulseEndingRampDuration,
                                              dwPulseMagnitude ) );
    rightDWGradientPulsesOnYAxis.reset( new gkg::DiffusionGradientPulse(
                                              GRADIENT_Y_AXIS,
                                              dwRightPulseStartingTime,
                                              dwPulseStartingRampDuration,
                                              dwPulsePlateauDuration,
                                              dwPulseEndingRampDuration,
                                              dwPulseMagnitude ) );
    rightDWGradientPulsesOnZAxis.reset( new gkg::DiffusionGradientPulse(
                                              GRADIENT_Z_AXIS,
                                              dwRightPulseStartingTime,
                                              dwPulseStartingRampDuration,
                                              dwPulsePlateauDuration,
                                              dwPulseEndingRampDuration,
                                              dwPulseMagnitude ) );

    // adding left DW gradient pair to the gradient pulse sequences
    xGradientPulseSequence->addPulse( leftDWGradientPulsesOnXAxis );
    yGradientPulseSequence->addPulse( leftDWGradientPulsesOnYAxis );
    zGradientPulseSequence->addPulse( leftDWGradientPulsesOnZAxis );
    xGradientPulseSequence->addPulse( rightDWGradientPulsesOnXAxis );
    yGradientPulseSequence->addPulse( rightDWGradientPulsesOnYAxis );
    zGradientPulseSequence->addPulse( rightDWGradientPulsesOnZAxis );


    ///////////////////////// adding pulse sequences ///////////////////////////
    this->addPulseSequence( rfMagnitudePulseSequence );
    this->addPulseSequence( rfPhasePulseSequence );
    this->addPulseSequence( xGradientPulseSequence );
    this->addPulseSequence( yGradientPulseSequence );
    this->addPulseSequence( zGradientPulseSequence );

    // finally setting echo time
    this->setEchoTimeInUs( 2.0 * secondExcitationCentreTime + mixingTimeInUs );

  }
  GKG_CATCH( "gkg::BipolarSTENMRSequence::BipolarSTENMRSequence( "
             "const std::set< float >& gradientAmplitudes, "
             "const std::vector< float >& scalarParameters, "
             "const std::vector< std::string >& /*stringParameters*/, "
             "bool verbose )" );

}


gkg::BipolarSTENMRSequence::~BipolarSTENMRSequence()
{
}


void gkg::BipolarSTENMRSequence::checkOrInitializeDefaultParameters(
                                  std::vector< float >& scalarParameters,
                                  std::vector< std::string >& stringParameters )
{

  try
  {

    if ( scalarParameters.size() != 4U )
    {

      throw std::runtime_error( "invalid scalar parameter count" );

    }
    if ( !stringParameters.empty() )
    {

      throw std::runtime_error( "invalid string parameter count" );

    }

  }
  GKG_CATCH( "void gkg::BipolarSTENMRSequence:: "
             "checkOrInitializeDefaultParameters( "
             "std::vector< float >& scalarParameters, "
             "std::vector< std::string >& stringParameters )" );

}


float gkg::BipolarSTENMRSequence::getDiffusionPulsePlateauWidthInUs(
                                      float maximuGradientAmplitudeInGaussPerCm,
                                      float maximumSlewRateInGaussPerCmPerUs,
                                      float littleDeltaInUs,
                                      float gradientTimeResolution,
                                      float& rampWidthInUs )
{

  try
  {

    float plateauWidthInUs = 0.0;

    // processing the ramp width
    rampWidthInUs = ( float )RUP_GRD( ( int32_t )
                                      ( maximuGradientAmplitudeInGaussPerCm /
                                        maximumSlewRateInGaussPerCmPerUs ) );

    plateauWidthInUs = littleDeltaInUs - 2.0 * rampWidthInUs;
    // put plateau width on GRAD_UPDATE boundaries
    plateauWidthInUs = ( float )RUP_GRD( plateauWidthInUs );

    if ( plateauWidthInUs <= 0.0 )
    {

      throw std::runtime_error( "Invalid gradient plateau width." );

    }

    return plateauWidthInUs;

  }
  GKG_CATCH( "float "
             "gkg::BipolarSTENMRSequence:: "
             "getDiffusionPulsePlateauWidthInUs( "
             "float maximuGradientAmplitudeInGaussPerCm, "
             "float maximumSlewRateInGaussPerCmPerUs, "
             "float littleDeltaInUs, "
             "float gradientTimeResolution, "
             "float& rampWidthInUs )" );

}


#undef TWOPI_GAM
#undef GAM

#undef MT_PER_M_TO_G_PER_CM
#undef T_PER_M_PER_S_TO_G_PER_CM_PER_US
#undef MS_TO_US

#undef RUP_GRD

