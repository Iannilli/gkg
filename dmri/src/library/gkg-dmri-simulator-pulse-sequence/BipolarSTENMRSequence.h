#ifndef _gkg_dmri_simulator_pulse_sequence_BipolarSTENMRSequence_h_
#define _gkg_dmri_simulator_pulse_sequence_BipolarSTENMRSequence_h_


#include <gkg-dmri-simulator-pulse-sequence/NMRSequence.h>


/* ====== Bipolar STE sequence diagram ( with trapezoid gradient pulses ) ======

 reference: N. Shemesh et al., J. Chem. Phys. 133, 044705 (2010).


                TE/2                TM                 TE/2
   <-----------------------------><-----><----------------------------->

                 180                                   180
                  |                                     |           Amp.
  90              |    +----+   90      90              |    +----+ <--
   |   delta/2    |   /|    |\   |       |              |   /|    |\    
   | <----------> |  / |    | \  |       |              |  / |    | \   
  -+-+--+----+--+-+-+--+----+--+-+-------+-+--+----+--+-+-+--+----+--+-+-
      \ |    | /                            \ |    | /
       \|    |/                              \|    |/
        +----+                                +----+
     <------------------------------------->
                      DELTA

 =============================================================================*/


namespace gkg
{


class BipolarSTENMRSequence : public NMRSequence
{

  public:

    // gradient amplitudes should be given in mT/m
    // maximum slew rate should be given in T/m/s
    // little delta and big delta should be given in ms
    // gradient time resolution should be given in us
    BipolarSTENMRSequence( const std::set< float >& gradientAmplitudes,
                           const std::vector< float >& scalarParameters,
                           const std::vector< std::string >& stringParameters,
                           bool verbose );
    virtual ~BipolarSTENMRSequence();

    static void checkOrInitializeDefaultParameters(
                                 std::vector< float >& scalarParameters,
                                 std::vector< std::string >& stringParameters );

  protected:

    float getDiffusionPulsePlateauWidthInUs(
                                      float maximuGradientAmplitudeInGaussPerCm,
                                      float maximumSlewRateInGaussPerCmPerUs,
                                      float littleDeltaInUs,
                                      float gradientTimeResolution,
                                      float& rampWidthInUs );

};


}


#endif
