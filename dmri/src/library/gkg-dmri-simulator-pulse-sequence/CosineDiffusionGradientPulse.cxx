#include <gkg-dmri-simulator-pulse-sequence/CosineDiffusionGradientPulse.h>
#include <gkg-core-exception/Exception.h>


gkg::CosineDiffusionGradientPulse::CosineDiffusionGradientPulse(
                                                             int32_t axis,
                                                             float startingTime,
                                                             float period,
                                                             int32_t lobeCount,
                                                             float magnitude )
                                  : gkg::CosineGradientPulse( axis,
                                                              startingTime,
                                                              period,
                                                              lobeCount,
                                                              magnitude )
{
}


gkg::CosineDiffusionGradientPulse::~CosineDiffusionGradientPulse()
{
}


float gkg::CosineDiffusionGradientPulse::getValueAt( float time,
                                                     float scaling ) const
{

  try
  {

    return this->gkg::CosineGradientPulse::getValueAt( time, scaling ) *
           scaling;

  }
  GKG_CATCH( "float gkg::CosineDiffusionGradientPulse::getValueAt( "
             "float time, "
             "float scaling ) const" );

}


float gkg::CosineDiffusionGradientPulse::getSquareValueAt( float time,
                                                           float scaling ) const
{

  try
  {

    return this->gkg::CosineGradientPulse::getSquareValueAt( time, scaling ) *
           scaling * scaling;

  }
  GKG_CATCH( "float gkg::CosineDiffusionGradientPulse::getSquareValueAt( "
             "float time, "
             "float scaling ) const" );

}


float gkg::CosineDiffusionGradientPulse::getIntegralValueAt( 
                                                           float time,
                                                           float scaling ) const
{

  try
  {

    return this->gkg::CosineGradientPulse::getIntegralValueAt( time,
                                                               scaling ) *
           scaling;

  }
  GKG_CATCH( "float gkg::CosineDiffusionGradientPulse::getIntegralValueAt( "
             "float time, "
             "float scaling ) const" );

}


float gkg::CosineDiffusionGradientPulse::getIntegralSquareValueAt(
                                                           float time,
                                                           float scaling ) const
{

  try
  {

    return this->gkg::CosineGradientPulse::getIntegralSquareValueAt( time,
                                                                     scaling ) *
           scaling * scaling;

  }
  GKG_CATCH( "float gkg::CosineDiffusionGradientPulse::getIntegralSquareValueAt( "
             "float time, "
             "float scaling ) const" );

}

