#ifndef _gkg_dmri_simulator_pulse_sequence_CosineDiffusionGradientPulse_h_
#define _gkg_dmri_simulator_pulse_sequence_CosineDiffusionGradientPulse_h_


#include <gkg-dmri-simulator-pulse-sequence/CosineGradientPulse.h>


namespace gkg
{


// gradient magnitudes are provided in mT/m


class CosineDiffusionGradientPulse : public CosineGradientPulse
{

  public:

    CosineDiffusionGradientPulse( int32_t axis,
                                  float startingTime,
                                  float period,
                                  int32_t lobeCount,
                                  float magnitude );
    virtual ~CosineDiffusionGradientPulse();

    virtual float getValueAt( float time,
                              float scaling ) const;
    virtual float getSquareValueAt( float time,
                                    float scaling ) const;
    virtual float getIntegralValueAt( float time,
                                      float scaling ) const;
    virtual float getIntegralSquareValueAt( float time,
                                            float scaling ) const;

};


}


#endif

