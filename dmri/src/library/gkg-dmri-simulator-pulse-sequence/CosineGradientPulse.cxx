#include <gkg-dmri-simulator-pulse-sequence/CosineGradientPulse.h>
#include <gkg-dmri-simulator-pulse-sequence/NMRDefines.h>
#include <gkg-core-exception/Exception.h>
#include <cmath>


gkg::CosineGradientPulse::CosineGradientPulse( int32_t axis,
                                               float startingTime,
                                               float period,
                                               int32_t lobeCount,
                                               float magnitude )
                         : gkg::GradientPulse( axis ),
                           _period( period ),
                           _lobeCount( lobeCount ),
                           _magnitude( magnitude ),
                           _angularFrequency( 2.0 * M_PI / period )
{

  try
  {

    _pulseType = GRADIENT_PULSE_TYPE;

    float time = startingTime;
    addBreakPoint( time );
    time += ( _period / 4.0 );
    addBreakPoint( time );
    time += ( _period * ( ( float )_lobeCount - 0.5 ) );
    addBreakPoint( time );
    time += ( _period / 4.0 );
    addBreakPoint( time );

  }
  GKG_CATCH( "gkg::CosineGradientPulse::CosineGradientPulse( "
             "int32_t axis, "
             "float startingTime, "
             "float period, "
             "int32_t lobeCount, "
             "float magnitude )" );

}


gkg::CosineGradientPulse::~CosineGradientPulse()
{
}


float gkg::CosineGradientPulse::getPeriod() const
{

  try
  {

    return _period;

  }
  GKG_CATCH( "float gkg::CosineGradientPulse::getPeriod() const" );

}


float gkg::CosineGradientPulse::getAngularFrequency() const
{

  try
  {

    return _angularFrequency;

  }
  GKG_CATCH( "float gkg::CosineGradientPulse::getAngularFrequency() const" );

}


int32_t gkg::CosineGradientPulse::getLobeCount() const
{

  try
  {

    return _lobeCount;

  }
  GKG_CATCH( "int32_t gkg::CosineGradientPulse::getLobeCount() const" );

}


float gkg::CosineGradientPulse::getMagnitude() const
{

  try
  {

    return _magnitude;

  }
  GKG_CATCH( "float gkg::CosineGradientPulse::getMagnitude() const" );

}


float gkg::CosineGradientPulse::getValueAt( float time,
                                            float /*scaling*/ ) const
{

  try
  {

    float value = 0.0;

    if ( ( time > _breakPoints[ 0 ] ) &&
         ( time <= _breakPoints[ 1 ] ) )
    {

      float r = _angularFrequency * ( time - _breakPoints[ 0 ] );
      value = _magnitude * std::sin( 2.0 * r );

    }
    else if ( ( time > _breakPoints[ 1 ] ) &&
              ( time <= _breakPoints[ 2 ] ) )
    {

      float r = _angularFrequency * ( time - _breakPoints[ 1 ] );
      value = -_magnitude * std::sin( r );

    }
    else if ( ( time > _breakPoints[ 2 ] ) &&
              ( time <= _breakPoints[ 3 ] ) )
    {

      float r = _angularFrequency * ( time - _breakPoints[ 2 ] );
      value = _magnitude * std::sin( 2.0 * r );

    }

    return value;

  }
  GKG_CATCH( "float gkg::CosineGradientPulse::getValueAt( "
             "float time, "
             "float /*scaling*/ ) const" );

}


float gkg::CosineGradientPulse::getSquareValueAt( float time,
                                                  float /*scaling*/ ) const
{

  try
  {

    float value = 0.0;

    if ( ( time > _breakPoints[ 0 ] ) &&
         ( time <= _breakPoints[ 1 ] ) )
    {

      float r = _angularFrequency * ( time - _breakPoints[ 0 ] );
      value = _magnitude * std::sin( 2.0 * r );

    }
    else if ( ( time > _breakPoints[ 1 ] ) &&
              ( time <= _breakPoints[ 2 ] ) )
    {

      float r = _angularFrequency * ( time - _breakPoints[ 1 ] );
      value = -_magnitude * std::sin( r );

    }
    else if ( ( time > _breakPoints[ 2 ] ) &&
              ( time <= _breakPoints[ 3 ] ) )
    {

      float r = _angularFrequency * ( time - _breakPoints[ 2 ] );
      value = _magnitude * std::sin( 2.0 * r );

    }

    return value * value;

  }
  GKG_CATCH( "float gkg::CosineGradientPulse::getSquareValueAt( "
             "float time, "
             "float /*scaling*/ ) const" );

}


float gkg::CosineGradientPulse::getIntegralValueAt( float time,
                                                    float /*scaling*/ ) const
{

  try
  {

    float value = 0.0;

    if ( ( time > _breakPoints[ 0 ] ) &&
         ( time <= _breakPoints[ 1 ] ) )
    {

      float r = _angularFrequency * ( time - _breakPoints[ 0 ] );
      value = _magnitude *
              ( 1.0 - std::cos( 2.0 * r ) ) / ( 2.0 * _angularFrequency );

    }
    else if ( ( time > _breakPoints[ 1 ] ) &&
              ( time <= _breakPoints[ 2 ] ) )
    {

      // form _breakPoints[ 0 ] to _breakPoints[ 1 ]
      value += ( 1.0 / _angularFrequency );

      // from _breakPoints[ 1 ] to time
      float r = _angularFrequency * ( time - _breakPoints[ 1 ] );
      value += ( ( std::cos( r ) - 1.0 ) / _angularFrequency );

      value *= _magnitude;

    }
    else if ( ( time > _breakPoints[ 2 ] ) &&
              ( time <= _breakPoints[ 3 ] ) )
    {

      // from _breakPoints[ 0 ] to _breakPoints[ 2 ]
      value += ( -1.0 / _angularFrequency );

      // from _breakPoints[ 2 ] to time
      float r = _angularFrequency * ( time - _breakPoints[ 2 ] );
      value += ( 1.0 - std::cos( 2.0 * r ) ) / ( 2.0 * _angularFrequency );

      value *= _magnitude;

    }

    return value;

  }
  GKG_CATCH( "float gkg::CosineGradientPulse::getIntegralValueAt( "
             "float time, "
             "float /*scaling*/ ) const" );

}


float gkg::CosineGradientPulse::getIntegralSquareValueAt(
                                                       float time,
                                                       float /*scaling*/ ) const
{

  try
  {

    float value = 0.0;

    if ( ( time > _breakPoints[ 0 ] ) &&
         ( time <= _breakPoints[ 1 ] ) )
    {

      float t = time - _breakPoints[ 0 ];
      value = _magnitude * _magnitude *
              ( ( t / 2.0 ) -
                std::sin( 4.0 * _angularFrequency * t ) /
                ( 8.0 * _angularFrequency ) );

    }
    else if ( ( time > _breakPoints[ 1 ] ) &&
              ( time <= _breakPoints[ 2 ] ) )
    {

      // from _breakPoints[ 0 ] to _breakPoints[ 1 ]
      value += ( _period / 8.0 );

      // from _breakPoints[ 1 ] to time
      float t = time - _breakPoints[ 1 ];
      float r = _angularFrequency * t;
      value += ( t / 2.0 - std::sin( 2.0 * r ) / ( 4.0 * _angularFrequency ) );

      value *= ( _magnitude * _magnitude );

    }
    else if ( ( time > _breakPoints[ 2 ] ) &&
              ( time <= _breakPoints[ 3 ] ) )
    {

      // from _breakPoints[ 0 ] to _breakPoints[ 2 ]
      value += ( ( _period / 8.0 ) * ( 4.0 * ( float )_lobeCount - 1.0 ) );

      // from _breakPoints[ 2 ] to time
      float t = time - _breakPoints[ 2 ];
      value += ( ( t / 2.0 ) -
                 std::sin( 4.0 * _angularFrequency * t ) /
                 ( 8.0 * _angularFrequency ) );

      value *= ( _magnitude * _magnitude );

    }
    else if ( time > _breakPoints[ 3 ] )
    {

      value = _magnitude * _magnitude * _period * ( ( float )_lobeCount ) / 2.0;

    }

    return value;

  }
  GKG_CATCH( "float gkg::CosineGradientPulse::getIntegralSquareValueAt( "
             "float time, "
             "float /*scaling*/ ) const" );

}

