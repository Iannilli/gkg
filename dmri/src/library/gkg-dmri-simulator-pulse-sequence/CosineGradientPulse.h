#ifndef _gkg_dmri_simulator_pulse_sequence_CosineGradientPulse_h_
#define _gkg_dmri_simulator_pulse_sequence_CosineGradientPulse_h_


#include <gkg-dmri-simulator-pulse-sequence/GradientPulse.h>


namespace gkg
{


// cosine wave:
// g( t ) = G * sin( 2.0 * w * t )     0.0 <= t < ( p / 4.0 )
// g( t ) = G * cos( w * t )           ( p / 4.0 ) <= t < ( T - p / 4.0 )
// g( t ) = G * sin( 2.0 * w * t )     ( T - p / 4.0 ) <= t <= T
//
// G : magnitude ( mT/m )
// w : angular frequency
// p : period
// T : waveform duration = ( p / 4.0 ) + ( p * lobeCount ) + ( p / 4.0 )


class CosineGradientPulse : public GradientPulse
{

  public:

    CosineGradientPulse( int32_t axis,
                         float startingTime,
                         float period,
                         int32_t lobeCount,
                         float magnitude );
    virtual ~CosineGradientPulse();

    float getPeriod() const;
    int32_t getLobeCount() const;
    float getMagnitude() const; 
    float getAngularFrequency() const;   

    virtual float getValueAt( float time,
                              float /*scaling*/ ) const;
    virtual float getSquareValueAt( float time,
                                    float /*scaling*/ ) const;
    virtual float getIntegralValueAt( float time,
                                      float /*scaling*/ ) const;
    virtual float getIntegralSquareValueAt( float time,
                                            float /*scaling*/ ) const;

  protected:

    float _period;
    int32_t _lobeCount;
    float _magnitude;
    float _angularFrequency;

};


}


#endif
