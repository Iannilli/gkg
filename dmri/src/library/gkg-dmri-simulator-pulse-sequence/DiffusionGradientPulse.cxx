#include <gkg-dmri-simulator-pulse-sequence/DiffusionGradientPulse.h>
#include <gkg-core-exception/Exception.h>



gkg::DiffusionGradientPulse::DiffusionGradientPulse( int32_t axis,
			                             float startingTime,
			                             float startingRampDuration,
			                             float plateauDuration,
			                             float endingRampDuration,
			                             float magnitude )
                            : gkg::TrapezoidGradientPulse( axis,
                                                           startingTime,
                                                           startingRampDuration,
                                                           plateauDuration,
                                                           endingRampDuration,
                                                           magnitude )
{
}


gkg::DiffusionGradientPulse::~DiffusionGradientPulse()
{
}


float gkg::DiffusionGradientPulse::getValueAt( float time,
                                               float scaling ) const
{

  try
  {

    return this->gkg::TrapezoidGradientPulse::getValueAt( time, scaling ) *
           scaling;

  }
  GKG_CATCH( "float gkg::DiffusionGradientPulse::getValueAt( "
             "float time, "
             "float scaling ) const" );

}


float gkg::DiffusionGradientPulse::getIntegralValueAt( float time,
                                                       float scaling ) const
{

  try
  {

    return this->gkg::TrapezoidGradientPulse::getIntegralValueAt( time,
                                                                  scaling ) *
           scaling;

  }
  GKG_CATCH( "float gkg::DiffusionGradientPulse::getIntegralValueAt( "
             "float time, "
             "float scaling ) const" );

}


float gkg::DiffusionGradientPulse::getIntegralSquareValueAt(
                                                       float time,
                                                       float scaling ) const
{

  try
  {

    return this->gkg::TrapezoidGradientPulse::getIntegralSquareValueAt(
                                                                  time,
                                                                  scaling ) *
           scaling * scaling;

  }
  GKG_CATCH( "float gkg::DiffusionGradientPulse::getIntegralSquareValueAt( "
             "float time, "
             "float scaling ) const" );

}

