#ifndef _gkg_dmri_simulator_pulse_sequence_DiffusionGradientPulse_h_
#define _gkg_dmri_simulator_pulse_sequence_DiffusionGradientPulse_h_


#include <gkg-dmri-simulator-pulse-sequence/TrapezoidGradientPulse.h>


namespace gkg
{


// gradient magnitudes are provided in mT/m


class DiffusionGradientPulse : public TrapezoidGradientPulse
{

  public:

    DiffusionGradientPulse( int32_t axis,
                            float startingTime,
                            float startingRampDuration,
                            float plateauDuration,
                            float endingRampDuration,
                            float magnitude );
    virtual ~DiffusionGradientPulse();

    virtual float getValueAt( float time,
                              float scaling ) const;
    virtual float getIntegralValueAt( float time,
                                      float scaling ) const;
    virtual float getIntegralSquareValueAt( float time,
                                            float scaling ) const;

};


}


#endif

