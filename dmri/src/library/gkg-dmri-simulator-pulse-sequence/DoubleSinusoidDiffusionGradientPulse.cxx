#include <gkg-dmri-simulator-pulse-sequence/DoubleSinusoidDiffusionGradientPulse.h>
#include <gkg-core-exception/Exception.h>


gkg::DoubleSinusoidDiffusionGradientPulse::DoubleSinusoidDiffusionGradientPulse(
                                                             int32_t axis,
                                                             float startingTime,
                                                             float period,
                                                             int32_t lobeCount,
                                                             float magnitude )
                                          : gkg::DoubleSinusoidGradientPulse(
                                                                   axis,
                                                                   startingTime,
                                                                   period,
                                                                   lobeCount,
                                                                   magnitude )
{
}


gkg::DoubleSinusoidDiffusionGradientPulse::~DoubleSinusoidDiffusionGradientPulse()
{
}


float gkg::DoubleSinusoidDiffusionGradientPulse::getValueAt(
                                                           float time,
                                                           float scaling ) const
{

  try
  {

    return this->gkg::DoubleSinusoidGradientPulse::getValueAt( time, scaling ) *
           scaling;

  }
  GKG_CATCH( "float gkg::DoubleSinusoidDiffusionGradientPulse::getValueAt( "
             "float time, "
             "float scaling ) const" );

}


float gkg::DoubleSinusoidDiffusionGradientPulse::getSquareValueAt(
                                                           float time,
                                                           float scaling ) const
{

  try
  {

    return this->gkg::DoubleSinusoidGradientPulse::getSquareValueAt( time,
                                                                     scaling ) *
           scaling * scaling;

  }
  GKG_CATCH( "float gkg::DoubleSinusoidDiffusionGradientPulse:: "
             "getSquareValueAt( "
             "float time, "
             "float scaling ) const" );

}


float gkg::DoubleSinusoidDiffusionGradientPulse::getIntegralValueAt( 
                                                           float time,
                                                           float scaling ) const
{

  try
  {

    return this->gkg::DoubleSinusoidGradientPulse::getIntegralValueAt(
                                                                    time,
                                                                    scaling ) *
           scaling;

  }
  GKG_CATCH( "float gkg::DoubleSinusoidDiffusionGradientPulse:: "
             "getIntegralValueAt( "
             "float time, "
             "float scaling ) const" );

}


float gkg::DoubleSinusoidDiffusionGradientPulse::getIntegralSquareValueAt(
                                                           float time,
                                                           float scaling ) const
{

  try
  {

    return this->gkg::DoubleSinusoidGradientPulse::getIntegralSquareValueAt(
                                                                    time,
                                                                    scaling ) *
           scaling * scaling;

  }
  GKG_CATCH( "float gkg::DoubleSinusoidDiffusionGradientPulse:: "
             "getIntegralSquareValueAt( "
             "float time, "
             "float scaling ) const" );

}

