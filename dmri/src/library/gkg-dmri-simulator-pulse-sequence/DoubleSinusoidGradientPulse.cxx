#include <gkg-dmri-simulator-pulse-sequence/DoubleSinusoidGradientPulse.h>
#include <gkg-dmri-simulator-pulse-sequence/NMRDefines.h>
#include <gkg-core-exception/Exception.h>
#include <cmath>


gkg::DoubleSinusoidGradientPulse::DoubleSinusoidGradientPulse(
                                              int32_t axis,
                                              float startingTime,
                                              float period,
                                              int32_t lobeCount,
                                              float magnitude )
                         : gkg::GradientPulse( axis ),
                           _period( period ),
                           _lobeCount( lobeCount ),
                           _magnitude( magnitude ),
                           _angularFrequency( 2.0 * M_PI / period )
{

  try
  {

    _pulseType = GRADIENT_PULSE_TYPE;

    float time = startingTime;
    addBreakPoint( time );
    time += ( _period * ( float )_lobeCount );
    addBreakPoint( time );

  }
  GKG_CATCH( "gkg::DoubleSinusoidGradientPulse::DoubleSinusoidGradientPulse( "
             "int32_t axis, "
             "float startingTime, "
             "float period, "
             "int32_t lobeCount, "
             "float magnitude )" );

}


gkg::DoubleSinusoidGradientPulse::~DoubleSinusoidGradientPulse()
{
}


float gkg::DoubleSinusoidGradientPulse::getPeriod() const
{

  try
  {

    return _period;

  }
  GKG_CATCH( "float gkg::DoubleSinusoidGradientPulse::getPeriod() const" );

}


float gkg::DoubleSinusoidGradientPulse::getAngularFrequency() const
{

  try
  {

    return _angularFrequency;

  }
  GKG_CATCH( "float "
             "gkg::DoubleSinusoidGradientPulse::getAngularFrequency() const" );

}


int32_t gkg::DoubleSinusoidGradientPulse::getLobeCount() const
{

  try
  {

    return _lobeCount;

  }
  GKG_CATCH( "int32_t gkg::DoubleSinusoidGradientPulse::getLobeCount() const" );

}


float gkg::DoubleSinusoidGradientPulse::getMagnitude() const
{

  try
  {

    return _magnitude;

  }
  GKG_CATCH( "float gkg::DoubleSinusoidGradientPulse::getMagnitude() const" );

}


float gkg::DoubleSinusoidGradientPulse::getValueAt( float time,
                                                    float /*scaling*/ ) const
{

  try
  {

    float value = 0.0;

    if ( ( time > _breakPoints[ 0 ] ) &&
         ( time <= _breakPoints[ 1 ] ) )
    {

      float r = _angularFrequency * ( time - _breakPoints[ 0 ] );

      float sign = +1.0;
      if ( std::sin( r / 2.0 ) < 0 )
      {

        sign *= -1.0;

      }

      value = sign * _magnitude * std::sin( r );

    }

    return value;

  }
  GKG_CATCH( "float gkg::DoubleSinusoidGradientPulse::getValueAt( "
             "float time, "
             "float /*scaling*/ ) const" );

}


float gkg::DoubleSinusoidGradientPulse::getSquareValueAt(
                                                       float time,
                                                       float /*scaling*/ ) const
{

  try
  {

    float value = 0.0;

    if ( ( time > _breakPoints[ 0 ] ) &&
	 ( time <= _breakPoints[ 1 ] ) )
    {
       
      float r = _angularFrequency * ( time - _breakPoints[ 0 ] );
      value = _magnitude * std::sin( r );
   
    }    

    return value * value;

  }
  GKG_CATCH( "float gkg::DoubleSinusoidGradientPulse::getSquareValueAt( "
             "float time, "
             "float /*scaling*/ ) const" );

}


float gkg::DoubleSinusoidGradientPulse::getIntegralValueAt(
                                                       float time,
                                                       float /*scaling*/ ) const
{

  try
  {

    float value = 0.0;

    if ( ( time > _breakPoints[ 0 ] ) &&
         ( time <= _breakPoints[ 1 ] ) )
    {

      float s = time - _breakPoints[ 0 ];
      float t = _period * ( ( s / _period ) - std::floor( s / _period ) );
      float r = _angularFrequency * t;

      float sign = +1.0;
      if ( std::sin( _angularFrequency * s / 2.0 ) < 0 )
      {

        sign *= -1.0;

      }
      value = sign * _magnitude * ( 1.0 - std::cos( r ) ) / _angularFrequency;

    }

    return value;

  }
  GKG_CATCH( "float gkg::DoubleSinusoidGradientPulse::getIntegralValueAt( "
             "float time, "
             "float /*scaling*/ ) const" );

}


float gkg::DoubleSinusoidGradientPulse::getIntegralSquareValueAt(
                                                       float time,
                                                       float /*scaling*/ ) const
{

  try
  {

    float value = 0.0;

    if ( ( time > _breakPoints[ 0 ] ) &&
         ( time <= _breakPoints[ 1 ] ) )
    {

      float t = time - _breakPoints[ 0 ];
      float r = _angularFrequency * t;
      value = _magnitude * _magnitude *
              ( t / 2.0 - std::sin( 2.0 * r ) / ( 4.0 * _angularFrequency ) );

    }
    else if ( time > _breakPoints[ 1 ] )
    {

      float t = _breakPoints[ 1 ] - _breakPoints[ 0 ];
      float r = _angularFrequency * t;
      value = _magnitude * _magnitude *
              ( t / 2.0 - std::sin( 2.0 * r ) / ( 4.0 * _angularFrequency ) );

    }

    return value;

  }
  GKG_CATCH( "float "
             "gkg::DoubleSinusoidGradientPulse::getIntegralSquareValueAt( "
             "float time, "
             "float /*scaling*/ ) const" );

}

