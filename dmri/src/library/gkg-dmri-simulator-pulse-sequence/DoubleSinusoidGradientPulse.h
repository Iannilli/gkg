#ifndef _gkg_dmri_simulator_pulse_sequence_DoubleSinusoidGradientPulse_h_
#define _gkg_dmri_simulatort_pulse_sequence_DoubleSinusoidGradientPulse_h_


#include <gkg-dmri-simulator-pulse-sequence/GradientPulse.h>


namespace gkg
{


// double-sine wave:
// g( t ) = G * sin( w * t ) * sgn( sin( w * t / 2.0 ) );
//
// G : magnitude ( mT/m )
// w : angular frequency
// sgn : signum function


class DoubleSinusoidGradientPulse : public GradientPulse
{

  public:

    DoubleSinusoidGradientPulse( int32_t axis,
                                 float startingTime,
                                 float period,
                                 int32_t lobeCount,
                                 float magnitude );
    virtual ~DoubleSinusoidGradientPulse();

    float getPeriod() const;
    int32_t getLobeCount() const;
    float getMagnitude() const;    
    float getAngularFrequency() const;

    virtual float getValueAt( float time,
                              float /*scaling*/ ) const;
    virtual float getSquareValueAt( float time,
                                    float /*scaling*/ ) const;
    virtual float getIntegralValueAt( float time,
                                      float /*scaling*/ ) const;
    virtual float getIntegralSquareValueAt( float time,
                                            float /*scaling*/ ) const;

  protected:

    float _period;
    int32_t _lobeCount;
    float _magnitude;
    float _angularFrequency;

};


}


#endif

