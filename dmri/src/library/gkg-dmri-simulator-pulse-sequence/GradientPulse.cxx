#include <gkg-dmri-simulator-pulse-sequence/GradientPulse.h>


gkg::GradientPulse::GradientPulse( int32_t axis )
                   : gkg::Pulse( GRADIENT_PULSE_TYPE, axis )
{
}


gkg::GradientPulse::~GradientPulse()
{
}

