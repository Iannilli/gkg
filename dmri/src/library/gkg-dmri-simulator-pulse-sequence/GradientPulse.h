#ifndef _gkg_dmri_simulator_pulse_sequence_GradientPulse_h_
#define _gkg_dmri_simulator_pulse_sequence_GradientPulse_h_


#include <gkg-dmri-simulator-pulse-sequence/Pulse.h>


namespace gkg
{


class GradientPulse : public Pulse
{

  public:

    virtual ~GradientPulse();


  protected:

    GradientPulse( int32_t axis );

};


}


#endif

