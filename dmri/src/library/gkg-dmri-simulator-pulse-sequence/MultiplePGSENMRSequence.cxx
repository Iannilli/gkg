#include <gkg-dmri-simulator-pulse-sequence/MultiplePGSENMRSequence.h>
#include <gkg-dmri-simulator-pulse-sequence/RadioFrequencyPulse.h>
#include <gkg-dmri-simulator-pulse-sequence/DiffusionGradientPulse.h>
#include <gkg-core-exception/Exception.h>
#include <cmath>


#define TWOPI_GAM 26748.0
#define GAM 26748.0/ (2*M_PI)

#define MT_PER_M_TO_G_PER_CM              0.1
#define T_PER_M_PER_S_TO_G_PER_CM_PER_US  0.0001
#define MS_TO_US                          1000.0

#define RUP_GRD( A )  ( ( ( int32_t )gradientTimeResolution -       \
                          ( ( int32_t )( A ) %                      \
                            ( int32_t )gradientTimeResolution ) ) % \
                          ( int32_t )gradientTimeResolution +       \
                        ( int32_t )( A ) )


gkg::MultiplePGSENMRSequence::MultiplePGSENMRSequence(
                             const std::set< float >& gradientAmplitudes,
                             const std::vector< float >& scalarParameters,
                             const std::vector< std::string >& stringParameters,
                             bool verbose )
                             : gkg::NMRSequence( gradientAmplitudes, verbose )
{

  try
  {

    // collecting sequence scalar parameters
    float maximumSlewRate = scalarParameters[ 0 ];
    float gradientTimeResolution = scalarParameters[ 1 ];
    float littleDeltaInMs = scalarParameters[ 2 ];
    float bigDeltaInMs = scalarParameters[ 3 ];
    float mixingTimeInMs = scalarParameters[ 4 ];
    int32_t gradientPairCount = ( int32_t )scalarParameters[ 5 ];

    // collecting sequence string parameters
    std::string wavevectorFileName = stringParameters[ 0 ];
    gkg::OrientationSet wavevectors( wavevectorFileName );

    //
    // Sanity check
    //
    if ( bigDeltaInMs < ( littleDeltaInMs + 6.0 ) )
    {

      // assume that the refocusing RF pulse duration is 6 ms
      throw std::runtime_error( "Invalid delta/DELTA combination" );

    }
    if ( ( mixingTimeInMs != 0.0 ) &&
         ( mixingTimeInMs < littleDeltaInMs ) )
    {

      throw std::runtime_error( "Invalid mixing time" );

    }
    if ( gradientPairCount <= 1 )
    {

      throw std::runtime_error( "Invalid gradient pair count" );

    }

    //
    // processing the diffusion gradient parameters
    //
    float maximumGradientAmplitude = this->_gradientAmplitudes.back();
    float maximumGradientAmplitudeInGaussPerCm = maximumGradientAmplitude *
                                                 MT_PER_M_TO_G_PER_CM;
    float maximumSlewRateInGaussPerCmPerUs = maximumSlewRate *
                                             T_PER_M_PER_S_TO_G_PER_CM_PER_US;

    float littleDeltaInUs = littleDeltaInMs * MS_TO_US;
    float bigDeltaInUs = bigDeltaInMs * MS_TO_US;
    float mixingTimeInUs = mixingTimeInMs * MS_TO_US;
    float timeSeparationInUs = bigDeltaInUs - littleDeltaInUs;
    float rampWidthInUs = 0.0;
    float plateauWidthInUs = getDiffusionPulsePlateauWidthInUs(
                                           maximumGradientAmplitudeInGaussPerCm,
                                           maximumSlewRateInGaussPerCmPerUs,
                                           littleDeltaInUs,
                                           gradientTimeResolution,
                                           rampWidthInUs );

    _effectiveDiffusionTimeInMs = bigDeltaInMs - littleDeltaInMs / 3.0;
    _maximumQInMeterInverse = GAM * 1e-2 * littleDeltaInMs *
                              maximumGradientAmplitude;

    //
    // creating and adding pulses to the NMR sequence
    //

    ///////////// creating RF + Gslice + Gphase + Gread pulse sequences ////////
    gkg::RCPointer< gkg::PulseSequence >
      rfMagnitudePulseSequence( new gkg::PulseSequence( RF_MAGNITUDE_AXIS ) );

    gkg::RCPointer< gkg::PulseSequence >
      rfPhasePulseSequence( new gkg::PulseSequence( RF_PHASE_AXIS ) );

    gkg::RCPointer< gkg::PulseSequence >
      xGradientPulseSequence( new gkg::PulseSequence( GRADIENT_X_AXIS ) );

    gkg::RCPointer< gkg::PulseSequence >
      yGradientPulseSequence( new gkg::PulseSequence( GRADIENT_Y_AXIS ) );

    gkg::RCPointer< gkg::PulseSequence >
      zGradientPulseSequence( new gkg::PulseSequence( GRADIENT_Z_AXIS ) );

    ///////////////////////// focusing on RF pulses //////////////////////////

    //// adding excitation pulse;
    // we consider the excitation pulse and slice selecton gradient will
    // last 3ms + 0.75ms for its rephasing gradient
    float excitationStartingTime = -1.5 * MS_TO_US;
    float excitationCentreTime = 0.0 * MS_TO_US;
    float excitationEndingTime = 1.5 * MS_TO_US;

    gkg::RCPointer< gkg::Pulse >
      excitationRFPulse( new gkg::RadioFrequencyPulse(
                                           RF_MAGNITUDE_AXIS,
                                           gkg::RadioFrequencyPulse::Excitation,
                                           excitationStartingTime,
                                           excitationCentreTime,
                          	           excitationEndingTime ) );

    //// adding refocusing pulses ( duration = 6.0 ms );
    float sliceSelectionRephasingTime = 0.75 * MS_TO_US;
    float timeDelayOfRefocusingRFInUs = ( timeSeparationInUs -
                                          6.0 * MS_TO_US ) / 2;
    float refocusingStartingTime = excitationEndingTime + 
                                   sliceSelectionRephasingTime +
                                   littleDeltaInUs +
                                   timeDelayOfRefocusingRFInUs;
    float refocusingCentreTime = refocusingStartingTime + 3.0 * MS_TO_US;
    float refocusingEndingTime = refocusingCentreTime + 3.0 * MS_TO_US;

    // getting the time interval between two refocusing pulses
    float refocusingPulseTimeInterval = 6.0 * MS_TO_US +
                                        2.0 * timeDelayOfRefocusingRFInUs +
                                        mixingTimeInUs +
                                        littleDeltaInUs;

    // allocating the refocusing pulses
    std::vector< gkg::RCPointer< gkg::Pulse > >
                                        refocusingRFPulses( gradientPairCount );

    // creating the refocusing pulses
    int32_t p = 0;
    for ( p = 0; p < gradientPairCount; p++ )
    {

      refocusingRFPulses[ p ].reset( new gkg::RadioFrequencyPulse(
                                           RF_MAGNITUDE_AXIS,
                                           gkg::RadioFrequencyPulse::Refocusing,
                                           refocusingStartingTime,
                                           refocusingCentreTime,
                                           refocusingEndingTime ) );

      // updating the time
      refocusingStartingTime += refocusingPulseTimeInterval;
      refocusingCentreTime += refocusingPulseTimeInterval;
      refocusingEndingTime += refocusingPulseTimeInterval;

    }

    // addingRF pulses to the RF pulse sequences
    rfMagnitudePulseSequence->addPulse( excitationRFPulse );
    rfMagnitudePulseSequence->addPulses( refocusingRFPulses );

    /////////////////////// focusing on gradient pulses ////////////////////////

    float dwLeftPulseStartingTime = excitationEndingTime +
                                    sliceSelectionRephasingTime;
    float dwRightPulseStartingTime = dwLeftPulseStartingTime + bigDeltaInUs;
    float dwPulseStartingRampDuration = rampWidthInUs;
    float dwPulsePlateauDuration = plateauWidthInUs;
    float dwPulseEndingRampDuration = rampWidthInUs;
    float dwPulseMagnitude = maximumGradientAmplitude;
    float dwGradientPulseTimeInterval = bigDeltaInUs + mixingTimeInUs;

    if ( mixingTimeInMs >= littleDeltaInMs )
    {

      ////// Multiple PGSE of form 1:

      // allocating DW gradient pulses
      std::vector< gkg::RCPointer< gkg::Pulse > >
                               leftDWGradientPulsesOnXAxis( gradientPairCount );
      std::vector< gkg::RCPointer< gkg::Pulse > >
                               leftDWGradientPulsesOnYAxis( gradientPairCount );
      std::vector< gkg::RCPointer< gkg::Pulse > >
                               leftDWGradientPulsesOnZAxis( gradientPairCount );
      std::vector< gkg::RCPointer< gkg::Pulse > >
                              rightDWGradientPulsesOnXAxis( gradientPairCount );
      std::vector< gkg::RCPointer< gkg::Pulse > >
                              rightDWGradientPulsesOnYAxis( gradientPairCount );
      std::vector< gkg::RCPointer< gkg::Pulse > >
                              rightDWGradientPulsesOnZAxis( gradientPairCount );

      gkg::Vector3d< float > wavevector;
      for ( p = 0; p < gradientPairCount; p++ )
      {

        // getting the wavevector
        wavevector= wavevectors.getOrientation( p );

        // left diffusion-sensitization
        leftDWGradientPulsesOnXAxis[ p ].reset(
                new DiffusionGradientPulse( GRADIENT_X_AXIS,
                                            dwLeftPulseStartingTime,
                                            dwPulseStartingRampDuration,
                                            dwPulsePlateauDuration,
                                            dwPulseEndingRampDuration,
                                            dwPulseMagnitude * wavevector.x ) );
        leftDWGradientPulsesOnYAxis[ p ].reset(
                new DiffusionGradientPulse( GRADIENT_Y_AXIS,
                                            dwLeftPulseStartingTime,
                                            dwPulseStartingRampDuration,
                                            dwPulsePlateauDuration,
                                            dwPulseEndingRampDuration,
                                            dwPulseMagnitude * wavevector.y ) );
        leftDWGradientPulsesOnZAxis[ p ].reset(
                new DiffusionGradientPulse( GRADIENT_Z_AXIS,
                                            dwLeftPulseStartingTime,
                                            dwPulseStartingRampDuration,
                                            dwPulsePlateauDuration,
                                            dwPulseEndingRampDuration,
                                            dwPulseMagnitude * wavevector.z ) );

        // right diffusion-sensitization
        rightDWGradientPulsesOnXAxis[ p ].reset(
                new DiffusionGradientPulse( GRADIENT_X_AXIS,
                                            dwRightPulseStartingTime,
                                            dwPulseStartingRampDuration,
                                            dwPulsePlateauDuration,
                                            dwPulseEndingRampDuration,
                                            dwPulseMagnitude * wavevector.x ) );
        rightDWGradientPulsesOnYAxis[ p ].reset(
                new DiffusionGradientPulse( GRADIENT_Y_AXIS,
                                            dwRightPulseStartingTime,
                                            dwPulseStartingRampDuration,
                                            dwPulsePlateauDuration,
                                            dwPulseEndingRampDuration,
                                            dwPulseMagnitude * wavevector.y ) );
        rightDWGradientPulsesOnZAxis[ p ].reset(
                new DiffusionGradientPulse( GRADIENT_Z_AXIS,
                                            dwRightPulseStartingTime,
                                            dwPulseStartingRampDuration,
                                            dwPulsePlateauDuration,
                                            dwPulseEndingRampDuration,
                                            dwPulseMagnitude * wavevector.z ) );

        // updating the time
        dwLeftPulseStartingTime += dwGradientPulseTimeInterval;
        dwRightPulseStartingTime += dwGradientPulseTimeInterval;

      }

      // adding DW gradient pulses to the gradient pulse sequences
      xGradientPulseSequence->addPulses( leftDWGradientPulsesOnXAxis );
      yGradientPulseSequence->addPulses( leftDWGradientPulsesOnYAxis );
      zGradientPulseSequence->addPulses( leftDWGradientPulsesOnZAxis );
      xGradientPulseSequence->addPulses( rightDWGradientPulsesOnXAxis );
      yGradientPulseSequence->addPulses( rightDWGradientPulsesOnYAxis );
      zGradientPulseSequence->addPulses( rightDWGradientPulsesOnZAxis );

    }
    else
    {

      ////// Multiple PGSE of form 2:

      // allocating DW gradient pulses
      int32_t gradientPulseCount = gradientPairCount + 1;
      std::vector< gkg::RCPointer< gkg::Pulse > >
                                  dwGradientPulsesOnXAxis( gradientPulseCount );
      std::vector< gkg::RCPointer< gkg::Pulse > >
                                  dwGradientPulsesOnYAxis( gradientPulseCount );
      std::vector< gkg::RCPointer< gkg::Pulse > >
                                  dwGradientPulsesOnZAxis( gradientPulseCount );

      float dwPulseStartingTime = dwLeftPulseStartingTime;
      for ( p = 0; p < gradientPulseCount; p++ )
      {

        if ( ( p == 0 ) || ( p == ( gradientPulseCount - 1 ) ) )
        {

          // adding the first and the last dw gradient pulses
          dwGradientPulsesOnXAxis[ p ].reset( new DiffusionGradientPulse(
                                                    GRADIENT_X_AXIS,
                                                    dwPulseStartingTime,
                                                    dwPulseStartingRampDuration,
                                                    dwPulsePlateauDuration,
                                                    dwPulseEndingRampDuration,
                                                    dwPulseMagnitude / 2.0 ) );
          dwGradientPulsesOnYAxis[ p ].reset( new DiffusionGradientPulse(
                                                    GRADIENT_Y_AXIS,
                                                    dwPulseStartingTime,
                                                    dwPulseStartingRampDuration,
                                                    dwPulsePlateauDuration,
                                                    dwPulseEndingRampDuration,
                                                    dwPulseMagnitude / 2.0 ) );
          dwGradientPulsesOnZAxis[ p ].reset( new DiffusionGradientPulse(
                                                    GRADIENT_Z_AXIS,
                                                    dwPulseStartingTime,
                                                    dwPulseStartingRampDuration,
                                                    dwPulsePlateauDuration,
                                                    dwPulseEndingRampDuration,
                                                    dwPulseMagnitude / 2.0 ) );

        }
        else
        {

          // adding the superposed dw gradient pulses
          dwGradientPulsesOnXAxis[ p ].reset( new DiffusionGradientPulse(
                                                    GRADIENT_X_AXIS,
                                                    dwPulseStartingTime,
                                                    dwPulseStartingRampDuration,
                                                    dwPulsePlateauDuration,
                                                    dwPulseEndingRampDuration,
                                                    dwPulseMagnitude ) );
          dwGradientPulsesOnYAxis[ p ].reset( new DiffusionGradientPulse(
                                                    GRADIENT_Y_AXIS,
                                                    dwPulseStartingTime,
                                                    dwPulseStartingRampDuration,
                                                    dwPulsePlateauDuration,
                                                    dwPulseEndingRampDuration,
                                                    dwPulseMagnitude ) );
          dwGradientPulsesOnZAxis[ p ].reset( new DiffusionGradientPulse(
                                                    GRADIENT_Z_AXIS,
                                                    dwPulseStartingTime,
                                                    dwPulseStartingRampDuration,
                                                    dwPulsePlateauDuration,
                                                    dwPulseEndingRampDuration,
                                                    dwPulseMagnitude ) );

        }

        // updating the time
        dwPulseStartingTime += dwGradientPulseTimeInterval;

      }

      // adding DW gradient pulses to the gradient pulse sequences
      xGradientPulseSequence->addPulses( dwGradientPulsesOnXAxis );
      yGradientPulseSequence->addPulses( dwGradientPulsesOnYAxis );
      zGradientPulseSequence->addPulses( dwGradientPulsesOnZAxis );
      
    }

    ///////////////////////// adding pulse sequences ///////////////////////////
    this->addPulseSequence( rfMagnitudePulseSequence );
    this->addPulseSequence( rfPhasePulseSequence );
    this->addPulseSequence( xGradientPulseSequence );
    this->addPulseSequence( yGradientPulseSequence );
    this->addPulseSequence( zGradientPulseSequence );

    // finally setting echo time
    this->setEchoTimeInUs( excitationEndingTime + sliceSelectionRephasingTime +
                           gradientPairCount * bigDeltaInUs +
                           ( gradientPairCount - 1 ) * mixingTimeInUs +
                           littleDeltaInUs +
                           0.75 * MS_TO_US );

  }
  GKG_CATCH( "gkg::MultiplePGSENMRSequence::MultiplePGSENMRSequence( "
             "const std::set< float >& gradientAmplitudes, "
             "const std::vector< float >& scalarParameters, "
             "const std::vector< std::string >& stringParameters, "
             "bool verbose )" );

}


gkg::MultiplePGSENMRSequence::~MultiplePGSENMRSequence()
{
}


void gkg::MultiplePGSENMRSequence::checkOrInitializeDefaultParameters(
                                  std::vector< float >& scalarParameters,
                                  std::vector< std::string >& stringParameters )
{

  try
  {

    if ( scalarParameters.size() != 6U )
    {

      throw std::runtime_error( "invalid scalar parameter count" );

    }
    if ( stringParameters.size() != 1U )
    {

      throw std::runtime_error( "invalid string parameter count" );

    }


  }
  GKG_CATCH( "void gkg::MultiplePGSENMRSequence:: "
             "checkOrInitializeDefaultParameters( "
             "std::vector< float >& scalarParameters, "
             "std::vector< std::string >& stringParameters )" );

}


float gkg::MultiplePGSENMRSequence::getDiffusionPulsePlateauWidthInUs(
                                      float maximuGradientAmplitudeInGaussPerCm,
                                      float maximumSlewRateInGaussPerCmPerUs,
                                      float littleDeltaInUs,
                                      float gradientTimeResolution,
                                      float& rampWidthInUs )
{

  try
  {

    float plateauWidthInUs = 0.0;

    // processing the ramp width
    rampWidthInUs = ( float )RUP_GRD( ( int32_t )
                                      ( maximuGradientAmplitudeInGaussPerCm /
                                        maximumSlewRateInGaussPerCmPerUs ) );

    plateauWidthInUs = littleDeltaInUs - 2.0 * rampWidthInUs;
    // put plateau width on GRAD_UPDATE boundaries
    plateauWidthInUs = ( float )RUP_GRD( plateauWidthInUs );

    if ( plateauWidthInUs <= 0.0 )
    {

      throw std::runtime_error( "Invalid gradient plateau width." );

    }

    return plateauWidthInUs;

  }
  GKG_CATCH( "float "
             "gkg::MultiplePGSENMRSequence::getDiffusionPulsePlateauWidthInUs( "
             "float maximuGradientAmplitudeInGaussPerCm, "
             "float maximumSlewRateInGaussPerCmPerUs, "
             "float littleDeltaInUs, "
             "float gradientTimeResolution, "
             "float& rampWidthInUs )" );

}


#undef TWOPI_GAM
#undef GAM

#undef MT_PER_M_TO_G_PER_CM
#undef T_PER_M_PER_S_TO_G_PER_CM_PER_US
#undef MS_TO_US

#undef RUP_GRD

