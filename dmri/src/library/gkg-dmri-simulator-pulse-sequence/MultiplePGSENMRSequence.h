#ifndef _gkg_dmri_simulator_pulse_sequence_MultiplePGSENMRSequence_h_
#define _gkg_dmri_simulator_pulse_sequence_MultiplePGSENMRSequence_h_


#include <gkg-dmri-simulator-pulse-sequence/NMRSequence.h>


// ====== Multiple PGSE sequence diagram (with trapezoid gradient pulses) ======
// Form 1.
//
//                   180                                  180
//         delta 1    |                         delta 2    |
//  90  <---------->  |            Gmax      <---------->  |            Gmax
//   |     +----+     |     +----+<--           +----+     |     +----+<--
//   |    /|    |\    |    /|    |\            /|    |\    |    /|    |\    |
//   |   / |    | \   |   / |    | \          / |    | \   |   / |    | \   |
// --+--+--+----+--+--+--+--+----+--+--------+--+----+--+--+--+--+----+--+--+--
//      <---------------->                   <---------------->
//            DELTA 1                              DELTA 2
//                       <------------------->
//                                 TM
//
// =============================================================================
// Form 2.
//
//                          180       delta 2      180
//              delta 1      |    <------------>    |       delta 3
//     90    <---------->    |        +----+<--     |    <---------->
//      |       +----+<--    |       /|    |\ Gmax  |       +----+<--
//      |      /|    |\Gmax/2|      / |    | \      |      /|    |\Gmax/2|
//      |     / |    | \     |     /  |    |  \     |     / |    | \     |
//    --+----+--+----+--+----+----+---+----+---+----+----+--+----+--+----+--
//           <--------------------X---------------------->
//                   DELTA 1               DELTA 2
//
// =============================================================================


namespace gkg
{


class MultiplePGSENMRSequence : public NMRSequence
{

  public:

    // gradient amplitudes should be given in mT/m
    // maximum slew rate should be given in T/m/s
    // little delta and big delta should be given in ms
    // gradient time resolution should be given in us
    MultiplePGSENMRSequence( const std::set< float >& gradientAmplitudes,
                             const std::vector< float >& scalarParameters,
                             const std::vector< std::string >& stringParameters,
                             bool verbose );
    virtual ~MultiplePGSENMRSequence();

    static void checkOrInitializeDefaultParameters(
                                 std::vector< float >& scalarParameters,
                                 std::vector< std::string >& stringParameters );

  protected:

    float getDiffusionPulsePlateauWidthInUs(
                                      float maximuGradientAmplitudeInGaussPerCm,
                                      float maximumSlewRateInGaussPerCmPerUs,
                                      float littleDeltaInUs,
                                      float gradientTimeResolution,
                                      float& rampWidthInUs );

};


}


#endif
