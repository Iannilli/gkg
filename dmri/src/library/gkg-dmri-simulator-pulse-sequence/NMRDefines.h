#ifndef _gkg_dmri_simulator_pulse_sequence_NMRDefines_h_
#define _gkg_dmri_simulator_pulse_sequence_NMRDefines_h_


#define GYROMAGNETIC_RATIO   42.576 * 1e6 /* Hz/T */

#define RF_PULSE_TYPE        0
#define GRADIENT_PULSE_TYPE  1

#define RF_MAGNITUDE_AXIS    0
#define RF_PHASE_AXIS        1
#define GRADIENT_X_AXIS      2
#define GRADIENT_Y_AXIS      3
#define GRADIENT_Z_AXIS      4


#endif

