#include <gkg-dmri-simulator-pulse-sequence/NMRSequence.h>
#include <gkg-dmri-simulator-pulse-sequence/NMRDefines.h>
#include <gkg-dmri-simulator-pulse-sequence/RadioFrequencyPulse.h>
#include <gkg-processing-container/Volume_i.h>
#include <gkg-core-exception/Exception.h>
#include <cmath>


gkg::NMRSequence::NMRSequence( const std::set< float >& gradientAmplitudes,
                               bool /* verbose */ )
                 : gkg::RCObject(),
                   _gradientAmplitudes( gradientAmplitudes.begin(),
                                        gradientAmplitudes.end() ),
                   _orientationSet( 0 ),
                   _echoTimeInUs( 0.0 ),
                   _phaseShiftPerPositionInitialized( false )
{
}


gkg::NMRSequence::~NMRSequence()
{
}


int32_t gkg::NMRSequence::getGradientAmplitudeCount() const
{

  try
  {

    return ( int32_t )_gradientAmplitudes.size();

  }
  GKG_CATCH( "int32_t gkg::NMRSequence::getGradientAmplitueCount() const" );

}


float gkg::NMRSequence::getGradientAmplitude( int32_t amplitudeIndex ) const
{

  try
  {

    if ( ( amplitudeIndex < 0 ) ||
         ( amplitudeIndex >= getGradientAmplitudeCount() ) )
    {

      throw std::runtime_error( "bad gradient amplitude index" );

    }
    return _gradientAmplitudes[ amplitudeIndex ];

  }
  GKG_CATCH( "float gkg::NMRSequence::getGradientAmplitude( "
             "int32_t amplitudeIndex ) const" );

}


void gkg::NMRSequence::setOrientationSet(
                          gkg::RCPointer< gkg::OrientationSet > orientationSet )
{

  try
  {

    _orientationSet = orientationSet;

  }
  GKG_CATCH( "void gkg::NMRSequence::setOrientationSet( "
             "gkg::RCPointer< gkg::OrientationSet > orientationSet )" );

}


gkg::RCPointer< gkg::OrientationSet > 
gkg::NMRSequence::getOrientationSet() const
{

  try
  {

    return _orientationSet;

  }
  GKG_CATCH( "gkg::RCPointer< gkg::OrientationSet > "
             "gkg::NMRSequence::getOrientationSet() const" );

}


void gkg::NMRSequence::setEchoTimeInUs( float echoTimeInUs )
{

  try
  {

    if ( echoTimeInUs <= 0 )
    {

      throw std::runtime_error( "bad echo time" );

    }
    _echoTimeInUs = echoTimeInUs;

  }
  GKG_CATCH( "void gkg::NMRSequence::setEchoTimeInUs( float echoTimeInUs )" );

}


float gkg::NMRSequence::getEchoTimeInUs() const
{

  try
  {

    return _echoTimeInUs;

  }
  GKG_CATCH( "float gkg::NMRSequence::getEchoTimeInUs() const" );

}


void gkg::NMRSequence::addPulseSequence(
                            gkg::RCPointer< gkg::PulseSequence > pulseSequence )
{

  try
  {

    int32_t axis = pulseSequence->getAxis();
    _pulseSequences[ axis ] = pulseSequence;

    // if a RF pulse sequence is added, saving the timing of the RF
    // refocusing pulses
    if ( ( axis == RF_MAGNITUDE_AXIS ) ||
         ( axis == RF_PHASE_AXIS ) )
    {

      std::list< gkg::RCPointer< gkg::RadioFrequencyPulse > >
        radioFrequencyPulses;

      const std::map< float, gkg::RCPointer< gkg::Pulse > >&
        pulses = pulseSequence->getPulses();
      std::map< float, gkg::RCPointer< gkg::Pulse > >::const_iterator
        p = pulses.begin(),
        pe = pulses.end();
      while ( p != pe )
      {

        radioFrequencyPulses.push_back( p->second );
        ++ p;

      }

      std::list< gkg::RCPointer< gkg::RadioFrequencyPulse > >::const_iterator
        r = radioFrequencyPulses.begin(),
        re = radioFrequencyPulses.end();
      while ( r != re )
      {

        if ( ( *r )->getRadioFrequencyType() ==
             gkg::RadioFrequencyPulse::Refocusing )
        {

          _refocusingPulseTimings.insert( ( *r )->getCentreTime() );

        }
        ++ r;

      }

    }

  }
  GKG_CATCH( "void gkg::NMRSequence::addPulseSequence( "
             "gkg::RCPointer< gkg::PulseSequence > pulseSequence )" );

}


gkg::RCPointer< gkg::PulseSequence >
gkg::NMRSequence::getPulseSequence( int32_t axis ) const
{

  try
  {

    gkg::RCPointer< gkg::PulseSequence > pulseSequence;

    std::map< int32_t, gkg::RCPointer< gkg::PulseSequence > >::const_iterator
      s = _pulseSequences.find( axis );

    if ( s != _pulseSequences.end() )
    {

      pulseSequence = s->second;

    }

    return pulseSequence;

  }
  GKG_CATCH( "gkg::RCPointer< gkg::PulseSequence > "
             "gkg::NMRSequence::getPulseSequence( int32_t axis ) const" );

}


float gkg::NMRSequence::getRefocusingSignAt( float time ) const
{

  try
  {

    float sign = +1;

    std::set< float >::const_iterator
      t = _refocusingPulseTimings.begin(),
      te = _refocusingPulseTimings.end();
    while ( t != te )
    {

      if ( time > ( *t ) )
      {

        sign *= -1;

      }      
      ++ t;

    }

    return sign;

  }
  GKG_CATCH( "float gkg::NMRSequence::getRefocusingSignAt( "
             "float time ) const" );

}


gkg::Vector3d< float >
gkg::NMRSequence::getGradientValueAt( float time,
                                      int32_t amplitudeIndex,
                                      int32_t orientationIndex )
{

  try
  {

    // getting the gradient amplitude and orientation scaling factor
    float amplitudeScaling = _gradientAmplitudes[ amplitudeIndex ] /
                             _gradientAmplitudes.back();
    gkg::Vector3d< float >
      orientationScaling = _orientationSet->getOrientation( orientationIndex );

    // getting the gradient value
    gkg::Vector3d< float > value;
    value.x = _pulseSequences[ GRADIENT_X_AXIS ]->getValueAt(
                                      time,
                                      amplitudeScaling * orientationScaling.x );
    value.y = _pulseSequences[ GRADIENT_Y_AXIS ]->getValueAt(
                                      time,
                                      amplitudeScaling * orientationScaling.y );
    value.z = _pulseSequences[ GRADIENT_Z_AXIS ]->getValueAt(
                                      time,
                                      amplitudeScaling * orientationScaling.z );

    return value;

  }
  GKG_CATCH( "gkg::Vector3d< float > "
             "gkg::NMRSequence::getGradientValueAt( "
             "float time, "
             "int32_t amplitudeIndex, "
             "int32_t orientationIndex )" );

}


void gkg::NMRSequence::setTemporalResolution( float timeStep )
{

  try
  {

    // allocating adequate LUT of phase shift per position
    int32_t iterationCount = ( int32_t )( _echoTimeInUs / timeStep ) + 1;
    int32_t gradientAmplitudeCount = getGradientAmplitudeCount();
    int32_t orientationCount = _orientationSet->getCount();

    _phaseShiftPerPosition.reallocate( iterationCount,
                                       gradientAmplitudeCount,
                                       orientationCount );

    int32_t i, g, o;
    float time = 0.0f;
    for ( i = 0; i < iterationCount; i++ )
    {

      for ( g = 0; g < gradientAmplitudeCount; g++ )
      {

        for ( o = 0; o < orientationCount; o++ )
        {

          // timeStep -> us ( 1 us = 1e-6 s )
          // gradient -> mT / m ( 1 mT/m = 1e-3 T/m )
          // position -> um ( 1 um = 1e-6 m )
          time = timeStep * ( float )i;

          _phaseShiftPerPosition( i, g, o ) = getGradientValueAt( time, g, o );
          _phaseShiftPerPosition( i, g, o ) *=
            getRefocusingSignAt( time ) *
            1e-15 * 2 * M_PI * GYROMAGNETIC_RATIO * timeStep;

        }

      }

    }

    // setting initialization tag to true
    _phaseShiftPerPositionInitialized = true;

  }
  GKG_CATCH( "void gkg::NMRSequence::setTemporalResolution( "
             "float timeStep )" );

}


const gkg::Vector3d< float >&
gkg::NMRSequence::getPhaseShiftPerPosition( int32_t iteration,
                                            int32_t amplitudeIndex,
                                            int32_t orientationIndex ) const
{

  try
  {

    return _phaseShiftPerPosition( iteration,
                                   amplitudeIndex,
                                   orientationIndex );

  }
  GKG_CATCH( "const gkg::Vector3d< float >& "
             "gkg::NMRSequence::getPhaseShiftPerPosition( "
             "int32_t iteration, "
             "int32_t amplitudeIndex, "
             "int32_t orientationIndex ) const" );

}


float gkg::NMRSequence::getBValue( float timeStep,
                                   int32_t amplitudeIndex,
                                   int32_t orientationIndex )
{

  try
  {

    // getting the iteration count
    int32_t iterationCount = ( int32_t )( _echoTimeInUs / timeStep ) + 1;

    // computing the b-value
    gkg::Vector3d< float > value;
    float bValue = 0.0;
    float time = 0.0;
    int32_t i = 0;
    for ( i = 0; i < iterationCount; i++ )
    {

      value += getGradientValueAt( time, amplitudeIndex, orientationIndex ) *
               getRefocusingSignAt( time ) * timeStep;

      bValue += value.getNorm2();
      time += timeStep;

    }

    // gradient -> mT / m ( 1 mT/m = 1e-6 T/mm )
    // time     -> us     ( 1 us = 1e-6 s )
    bValue *= 1e-30 * 4.0 * M_PI * M_PI * timeStep *
              GYROMAGNETIC_RATIO * GYROMAGNETIC_RATIO;

    return bValue;

  }
  GKG_CATCH( "float gkg::NMRSequence::getBValue( "
             "float timeStep, "
             "int32_t amplitudeIndex, "
             "int32_t orientationIndex )" );

}


float gkg::NMRSequence::getEffectiveDiffusionTimeInMs() const
{

  try
  {

    return _effectiveDiffusionTimeInMs;

  }
  GKG_CATCH( "float gkg::NMRSequence::getEffectiveDiffusionTimeInMs() const" );

}


float gkg::NMRSequence::getMaximumQInMeterInverse() const
{

  try
  {

    return _maximumQInMeterInverse;

  }
  GKG_CATCH( "float gkg::NMRSequence::getMaximumQInMeterInverse() const" );

}


float gkg::NMRSequence::getMinimumRadiusInUm() const
{

  try
  {

    return ( 1e6f / _maximumQInMeterInverse );

  }
  GKG_CATCH( "float gkg::NMRSequence::getMinimumRadiusInUm() const" );

}

