#ifndef _gkg_dmri_simulator_pulse_sequence_NMRSequence_h_
#define _gkg_dmri_simulator_pulse_sequence_NMRSequence_h_


#include <gkg-core-pattern/RCPointer.h>
#include <gkg-processing-coordinates/Vector3d.h>
#include <gkg-processing-coordinates/OrientationSet.h>
#include <gkg-processing-container/Volume.h>
#include <gkg-dmri-simulator-pulse-sequence/PulseSequence.h>
#include <map>
#include <set>
#include <vector>


namespace gkg
{


class NMRSequence : public RCObject
{

  public:

    NMRSequence( const std::set< float >& gradientAmplitudes,
                 bool verbose );
    virtual ~NMRSequence();

    void addPulseSequence( RCPointer< PulseSequence > pulseSequence );
    RCPointer< PulseSequence > getPulseSequence( int32_t axis ) const;
    float getRefocusingSignAt( float time ) const;

    int32_t getGradientAmplitudeCount() const;
    float getGradientAmplitude( int32_t amplitudeIndex ) const;

    void setOrientationSet( RCPointer< OrientationSet > orientationSet );
    RCPointer< OrientationSet > getOrientationSet() const;

    void setEchoTimeInUs( float echoTimeInUs );
    float getEchoTimeInUs() const;

    Vector3d< float > getGradientValueAt( float time,
                                          int32_t amplitudeIndex,
                                          int32_t orientationIndex );

    void setTemporalResolution( float timeStep );
    const Vector3d< float >& getPhaseShiftPerPosition(
                                          int32_t iteration,
                                          int32_t amplitudeIndex,
                                          int32_t orientationIndex ) const;

    // methods to collect sequence parameters
    float getBValue( float timeStep,
                     int32_t amplitudeIndex,
                     int32_t orientationIndex );
    float getEffectiveDiffusionTimeInMs() const;
    float getMaximumQInMeterInverse() const;
    float getMinimumRadiusInUm() const;

  protected:

    std::map< int32_t, RCPointer< PulseSequence > > _pulseSequences;
    std::set< float > _refocusingPulseTimings;
    std::vector< float > _gradientAmplitudes;
    RCPointer< OrientationSet > _orientationSet;
    float _echoTimeInUs;

    float _effectiveDiffusionTimeInMs;
    float _maximumQInMeterInverse;

    Volume< Vector3d< float > > _phaseShiftPerPosition;
    bool _phaseShiftPerPositionInitialized;

};


}


#endif
