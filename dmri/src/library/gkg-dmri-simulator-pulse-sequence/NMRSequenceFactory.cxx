#include <gkg-dmri-simulator-pulse-sequence/NMRSequenceFactory.h>
#include <gkg-dmri-simulator-pulse-sequence/SinglePGSENMRSequence.h>
#include <gkg-dmri-simulator-pulse-sequence/MultiplePGSENMRSequence.h>
#include <gkg-dmri-simulator-pulse-sequence/StimulatedEchoNMRSequence.h>
#include <gkg-dmri-simulator-pulse-sequence/BipolarSTENMRSequence.h>
#include <gkg-dmri-simulator-pulse-sequence/BipolarDoubleSTENMRSequence.h>
#include <gkg-dmri-simulator-pulse-sequence/TwiceRefocusedSpinEchoNMRSequence.h>
#include <gkg-dmri-simulator-pulse-sequence/OscillatingGradientSENMRSequence.h>
#include <gkg-core-exception/Exception.h>


gkg::NMRSequenceFactory::NMRSequenceFactory()
{
}


gkg::NMRSequenceFactory::~NMRSequenceFactory()
{
}


void gkg::NMRSequenceFactory::registerCreator(
                                      const std::string& name,
                                      gkg::NMRSequenceFactory::Creator creator )
{

  try
  {

    std::map< std::string,
              gkg::NMRSequenceFactory::Creator >::const_iterator
      c = _creators.find( name );

    if ( c == _creators.end() )
    {

      _creators[ name ] = creator;

    }
    else
    {

      std::cerr << "gkg::NMRSequenceFactory::registerCreator: "
                << name << " already exists"
                << std::endl;

    }

  }
  GKG_CATCH( "void gkg::NMRSequenceFactory::registerCreator( "
             "const std::string& name, "
             "gkg::NMRSequenceFactory::Creator creator )" );

}


void gkg::NMRSequenceFactory::registerParameterChecker(
                    const std::string& name,
                    gkg::NMRSequenceFactory::ParameterChecker parameterChecker )
{

  try
  {

    std::map< std::string,
              gkg::NMRSequenceFactory::ParameterChecker >::const_iterator
      c = _parameterCheckers.find( name );

    if ( c == _parameterCheckers.end() )
    {

      _parameterCheckers[ name ] = parameterChecker;

    }
    else
    {

      std::cerr << "gkg::NMRSequenceFactory::registerParameterChecker: "
                << name << " already exists"
                << std::endl;

    }

  }
  GKG_CATCH( "void gkg::NMRSequenceFactory::registerParameterChecker( "
             "const std::string& name, "
             "gkg::NMRSequenceFactory::ParameterChecker parameterChecker )" );

}


std::list< std::string > gkg::NMRSequenceFactory::getNameList() const
{

  try
  {

    std::list< std::string > names;

    std::map< std::string,
              gkg::NMRSequenceFactory::Creator >::const_iterator
      c = _creators.begin(),
      ce = _creators.end();
    while ( c != ce )
    {

      names.push_back( c->first );
      ++ c;

    }

    return names;

  }
  GKG_CATCH( "std::list< std::string > "
             "gkg::NMRSequenceFactory::getNameList() const " );

}


gkg::RCPointer< gkg::NMRSequence >
gkg::NMRSequenceFactory::create(
                             const std::string& name,
                             const std::set< float >& gradientAmplitudes,
                             const std::vector< float >& scalarParameters,
                             const std::vector< std::string >& stringParameters,
                             bool verbose ) const
{

  try
  {

    std::map< std::string,
              gkg::NMRSequenceFactory::Creator >::const_iterator
      c = _creators.find( name );

    if ( c != _creators.end() )
    {

      return ( c->second )( gradientAmplitudes,
                            scalarParameters,
                            stringParameters,
                            verbose );

    }
    else
    {

      throw std::runtime_error( std::string( "'" ) + name + "' not found" );

    }

  }
  GKG_CATCH( "gkg::RCPointer< gkg::NMRSequence > "
             "gkg::NMRSequenceFactory::create( "
             "const std::string& name, "
             "const std::set< float >& gradientAmplitudes, "
             "const std::vector< float >& scalarParameters, "
             "const std::vector< std::string >& stringParameters, "
             "bool verbose ) const" );

}


void gkg::NMRSequenceFactory::checkOrInitializeDefaultParameters(
                                  const std::string& name,
                                  std::vector< float >& scalarParameters,
                                  std::vector< std::string >& stringParameters )
{

  try
  {

    std::map< std::string,
              gkg::NMRSequenceFactory::ParameterChecker >::const_iterator
      c = _parameterCheckers.find( name );

    if ( c != _parameterCheckers.end() )
    {

      return ( c->second )( scalarParameters, stringParameters );

    }
    else
    {

      throw std::runtime_error( std::string( "'" ) + name + "' not found" );

    }

  }
  GKG_CATCH( "void gkg::NMRSequenceFactory:: "
             "checkOrInitializeDefaultParameters( "
             "const std::string& name, "
             "std::vector< float >& scalarParameters, "
             "std::vector< std::string >& stringParameters )" );

}


template < class S >
gkg::RCPointer< gkg::NMRSequence >
gkg::NMRSequenceFactory::createNMRSequence(
                             const std::set< float >& gradientAmplitudes,
                             const std::vector< float >& scalarParameters,
                             const std::vector< std::string >& stringParameters,
                             bool verbose )
{

  try
  {

    gkg::RCPointer< gkg::NMRSequence > nmrSequence;

    nmrSequence.reset( new S( gradientAmplitudes,
                              scalarParameters,
                              stringParameters,
                              verbose ) );

    return nmrSequence;

  }
  GKG_CATCH( "gkg::RCPointer< gkg::NMRSequence > "
             "gkg::NMRSequenceFactory::createNMRSequence( "
             "const std::set< float >& gradientAmplitudes, "
             "const std::vector< float >& scalarParameters, "
             "const std::vector< std::string >& stringParameters, "
             "bool verbose )" );

}


//
// NMRSequence creator registration
//

static bool registerCreators()
{

  try
  {

    gkg::NMRSequenceFactory::getInstance().registerCreator(
      "single_pgse_nmr_sequence",
      gkg::NMRSequenceFactory::createNMRSequence<
                                     gkg::SinglePGSENMRSequence > );

    gkg::NMRSequenceFactory::getInstance().registerCreator(
      "multiple_pgse_nmr_sequence",
      gkg::NMRSequenceFactory::createNMRSequence<
                                     gkg::MultiplePGSENMRSequence > );

    gkg::NMRSequenceFactory::getInstance().registerCreator(
      "stimulated_echo_nmr_sequence",
      gkg::NMRSequenceFactory::createNMRSequence<
                                     gkg::StimulatedEchoNMRSequence > );

    gkg::NMRSequenceFactory::getInstance().registerCreator(
      "bipolar_ste_nmr_sequence",
      gkg::NMRSequenceFactory::createNMRSequence<
                                     gkg::BipolarSTENMRSequence > );

    gkg::NMRSequenceFactory::getInstance().registerCreator(
      "bipolar_double_ste_nmr_sequence",
      gkg::NMRSequenceFactory::createNMRSequence<
                                     gkg::BipolarDoubleSTENMRSequence > );

    gkg::NMRSequenceFactory::getInstance().registerCreator(
      "twice_refocused_se_nmr_sequence",
      gkg::NMRSequenceFactory::createNMRSequence<
                                     gkg::TwiceRefocusedSpinEchoNMRSequence > );

    gkg::NMRSequenceFactory::getInstance().registerCreator(
      "ogse_nmr_sequence",
      gkg::NMRSequenceFactory::createNMRSequence<
                                     gkg::OscillatingGradientSENMRSequence > );

    return true;

  }
  GKG_CATCH( "static bool registerCreator()" );

}

static bool registeredCreators __attribute__((unused)) = registerCreators();


//
// NMRSequence parameter checker registration
//

static bool registerParameterCheckers()
{

  try
  {

    gkg::NMRSequenceFactory::getInstance().registerParameterChecker(
      "single_pgse_nmr_sequence",
      gkg::SinglePGSENMRSequence::checkOrInitializeDefaultParameters );

    gkg::NMRSequenceFactory::getInstance().registerParameterChecker(
      "multiple_pgse_nmr_sequence",
      gkg::MultiplePGSENMRSequence::checkOrInitializeDefaultParameters );

    gkg::NMRSequenceFactory::getInstance().registerParameterChecker(
      "stimulated_echo_nmr_sequence",
      gkg::StimulatedEchoNMRSequence::checkOrInitializeDefaultParameters );

    gkg::NMRSequenceFactory::getInstance().registerParameterChecker(
      "bipolar_ste_nmr_sequence",
      gkg::BipolarSTENMRSequence::checkOrInitializeDefaultParameters );

    gkg::NMRSequenceFactory::getInstance().registerParameterChecker(
      "bipolar_double_ste_nmr_sequence",
      gkg::BipolarDoubleSTENMRSequence::checkOrInitializeDefaultParameters );

    gkg::NMRSequenceFactory::getInstance().registerParameterChecker(
      "twice_refocused_se_nmr_sequence",
      gkg::TwiceRefocusedSpinEchoNMRSequence::
                                           checkOrInitializeDefaultParameters );

    gkg::NMRSequenceFactory::getInstance().registerParameterChecker(
      "ogse_nmr_sequence",
      gkg::OscillatingGradientSENMRSequence::
                                           checkOrInitializeDefaultParameters );

    return true;

  }
  GKG_CATCH( "static bool registerParameterCheckers()" );

}

static bool registeredParameterCheckers __attribute__((unused)) =
                                                    registerParameterCheckers();
