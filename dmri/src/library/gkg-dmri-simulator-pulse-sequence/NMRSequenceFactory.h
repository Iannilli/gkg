#ifndef _gkg_dmri_simulator_pulse_sequence_NMRSequenceFactory_h_
#define _gkg_dmri_simulator_pulse_sequence_NRMSequenceFactory_h_


#include <gkg-core-pattern/Singleton.h>
#include <gkg-dmri-simulator-pulse-sequence/NMRSequence.h>
#include <string>
#include <list>


namespace gkg
{


class NMRSequenceFactory : public Singleton< NMRSequenceFactory >
{

  public:

    typedef RCPointer< NMRSequence > ( *Creator )(
                             const std::set< float >& gradientAmplitudes,
                             const std::vector< float >& scalarParameters,
                             const std::vector< std::string >& stringParameters,
                             bool verbose );
    typedef void ( *ParameterChecker )(
                                 std::vector< float >& scalarParameters,
                                 std::vector< std::string >& stringParameters );
    ~NMRSequenceFactory();

    void registerCreator( const std::string& name,
                          Creator creator );
    void registerParameterChecker( const std::string& name,
                                   ParameterChecker parameterChecker );

    std::list< std::string > getNameList() const;

    RCPointer< NMRSequence >
      create( const std::string& name,
              const std::set< float >& gradientAmplitudes,
              const std::vector< float >& scalarParameters,
              const std::vector< std::string >& stringParameters,
              bool verbose ) const;

    void checkOrInitializeDefaultParameters(
                                 const std::string& name,
                                 std::vector< float >& scalarParameters,
                                 std::vector< std::string >& stringParameters );

    template < class S >
    static RCPointer< NMRSequence >
      createNMRSequence( const std::set< float >& gradientAmplitudes,
                         const std::vector< float >& scalarParameters,
                         const std::vector< std::string >& stringParameters,
                         bool verbose );

  protected:

    friend class Singleton< NMRSequenceFactory >;

    NMRSequenceFactory();

    std::map< std::string, Creator > _creators;
    std::map< std::string, ParameterChecker > _parameterCheckers;

};


}


#endif


