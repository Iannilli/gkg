#include <gkg-dmri-simulator-pulse-sequence/OscillatingGradientPulseFactory.h>
#include <gkg-dmri-simulator-pulse-sequence/SinusoidDiffusionGradientPulse.h>
#include <gkg-dmri-simulator-pulse-sequence/DoubleSinusoidDiffusionGradientPulse.h>
#include <gkg-dmri-simulator-pulse-sequence/CosineDiffusionGradientPulse.h>
#include <gkg-dmri-simulator-pulse-sequence/TrapezoidOscillatingDiffusionGradientPulse.h>
#include <gkg-core-exception/Exception.h>


gkg::OscillatingGradientPulseFactory::OscillatingGradientPulseFactory()
{

  try
  {

    _oscillatingGradientPulseNameList.insert( "sine" );
    _oscillatingGradientPulseNameList.insert( "double-sine" );
    _oscillatingGradientPulseNameList.insert( "cosine" );
    _oscillatingGradientPulseNameList.insert( "trapezoid" );

  }
  GKG_CATCH( "gkg::OscillatingGradientPulseFactory:: "
             "OscillatingGradientPulseFactory()" );

}


gkg::OscillatingGradientPulseFactory::~OscillatingGradientPulseFactory()
{
}


gkg::RCPointer< gkg::Pulse >
gkg::OscillatingGradientPulseFactory::create(
                                const std::string& oscillatingGradientPulseName,
                                int32_t axis,
                                float startingTime,
                                float period,
                                int32_t lobeCount,
                                float magnitude,
                                float maximumSlewRate,
                                float gradientTimeResolution ) const
{

  try
  {

    gkg::RCPointer< gkg::Pulse > pulse;

    if ( oscillatingGradientPulseName == "sine" )
    {

      pulse.reset( new gkg::SinusoidDiffusionGradientPulse( axis,
                                                            startingTime,
                                                            period,
                                                            lobeCount,
                                                            magnitude ) );

    }
    else if ( oscillatingGradientPulseName == "double-sine" )
    {

      pulse.reset( new gkg::DoubleSinusoidDiffusionGradientPulse( axis,
                                                                  startingTime,
                                                                  period,
                                                                  lobeCount,
                                                                  magnitude ) );

    }
    else if ( oscillatingGradientPulseName == "cosine" )
    {

      pulse.reset( new gkg::CosineDiffusionGradientPulse( axis,
                                                          startingTime,
                                                          period,
                                                          lobeCount,
                                                          magnitude ) );

    }
    else if ( oscillatingGradientPulseName == "trapezoid" )
    {

      pulse.reset( new gkg::TrapezoidOscillatingDiffusionGradientPulse(
                                                     axis,
                                                     startingTime,
                                                     period,
                                                     lobeCount,
                                                     magnitude,
                                                     maximumSlewRate,
                                                     gradientTimeResolution ) );

    }
    else
    {

      throw std::runtime_error( "unknown oscillating gradient pulse" );

    }

    return pulse;

  }
  GKG_CATCH( "gkg::RCPointer< gkg::Pulse > "
             "gkg::OscillatingGradientPulseFactory::create( "
             "const std::string& oscillatingGradientPulseName, "
             "int32_t axis, "
             "float startingTime, "
             "float period, "
             "int32_t lobeCount, "
             "float magnitude, "
             "float maximumSlewRate, "
             "float gradientTimeResolution ) const" );

}


std::list< std::string > gkg::OscillatingGradientPulseFactory::
                                     getOscillatingGradientPulseNameList() const
{

  try
  {

    return std::list< std::string >( _oscillatingGradientPulseNameList.begin(),
                                     _oscillatingGradientPulseNameList.end() );

  }
  GKG_CATCH( "std::list< std::string > "
             "gkg::OscillatingGradientPulseFactory:: "
             "getOscillatingGradientPulseNameList() const" );

}

