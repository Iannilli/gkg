#ifndef _gkg_dmri_simulator_pulse_sequence_OscillatingGradientPulseFactory_h_
#define _gkg_dmri_simulator_pulse_sequence_OscillatingGradientPulseFactory_h_


#include <gkg-core-pattern/Singleton.h>
#include <gkg-core-pattern/RCPointer.h>
#include <gkg-dmri-simulator-pulse-sequence/Pulse.h>
#include <string>
#include <set>
#include <list>


namespace gkg
{


class OscillatingGradientPulseFactory : public Singleton<
                                               OscillatingGradientPulseFactory >
{

  public:

    ~OscillatingGradientPulseFactory();

    RCPointer< Pulse > create( const std::string& oscillatingGradientPulseName,
                               int32_t axis,
                               float startingTime,
                               float period,
                               int32_t lobeCount,
                               float magnitude,
                               float maximumSlewRate,
                               float gradientTimeResolution ) const;

    std::list< std::string > getOscillatingGradientPulseNameList() const;

  protected:

    friend class Singleton< OscillatingGradientPulseFactory >;

    OscillatingGradientPulseFactory();

    std::set< std::string > _oscillatingGradientPulseNameList;

};


}


#endif

