#include <gkg-dmri-simulator-pulse-sequence/OscillatingGradientSENMRSequence.h>
#include <gkg-dmri-simulator-pulse-sequence/RadioFrequencyPulse.h>
#include <gkg-dmri-simulator-pulse-sequence/DiffusionGradientPulse.h>
#include <gkg-dmri-simulator-pulse-sequence/OscillatingGradientPulseFactory.h>
#include <gkg-core-exception/Exception.h>
#include <cmath>


#define GAM 26748.0/ (2*M_MPI)
#define MT_PER_M_TO_G_PER_CM       0.1
#define MS_TO_US                   1000.0


gkg::OscillatingGradientSENMRSequence::OscillatingGradientSENMRSequence(
                             const std::set< float >& gradientAmplitudes,
                             const std::vector< float >& scalarParameters,
                             const std::vector< std::string >& stringParameters,
                             bool verbose )
                                      : gkg::NMRSequence( gradientAmplitudes,
                                                          verbose )
{

  try
  {

    // collecting sequence scalar parameters
    float period = scalarParameters[ 0 ];
    int32_t lobeCount = ( int32_t )scalarParameters[ 1 ];

    // collecting sequence string parameters
    std::string gradientWaveformName = stringParameters[ 0 ];

    //
    // processing the diffusion gradient parameters
    //
    float maximumGradientAmplitude = this->_gradientAmplitudes.back();
    //float maximumGradientAmplitudeInGaussPerCm = maximumGradientAmplitude *
    //						 MT_PER_M_TO_G_PER_CM;
    float periodInUs = period * MS_TO_US;
    float waveformDurationInMs = period * ( float )lobeCount;
    float waveformDurationInUs = waveformDurationInMs * MS_TO_US;

    float maximumSlewRate = scalarParameters[ 3 ];
    float gradientTimeResolution = scalarParameters[ 4 ];

    //
    // creating and adding pulses to the NMR sequence
    //

    ////////////  creating RF + Gslice + Gphase + Gread pulse sequences ////////
    gkg::RCPointer< gkg::PulseSequence >
      rfMagnitudePulseSequence( new gkg::PulseSequence( RF_MAGNITUDE_AXIS ) );

    gkg::RCPointer< gkg::PulseSequence >
      rfPhasePulseSequence( new gkg::PulseSequence( RF_PHASE_AXIS ) );

    gkg::RCPointer< gkg::PulseSequence >
      xGradientPulseSequence( new gkg::PulseSequence( GRADIENT_X_AXIS ) );

    gkg::RCPointer< gkg::PulseSequence >
      yGradientPulseSequence( new gkg::PulseSequence( GRADIENT_Y_AXIS ) );

    gkg::RCPointer< gkg::PulseSequence >
      zGradientPulseSequence( new gkg::PulseSequence( GRADIENT_Z_AXIS ) );

    /////////////////////////// focusing on RF pulses //////////////////////////

    // adding excitation pulse; we consider the excitation pulse and slice
    // selecton gradient will last 3ms + 0.75ms for its rephasing gradient
    float excitationStartingTime = -1.5 * MS_TO_US;
    float excitationCentreTime = 0.0 * MS_TO_US;
    float excitationEndingTime = 1.5 * MS_TO_US;

    gkg::RCPointer< gkg::Pulse >
      excitationRFPulse( new gkg::RadioFrequencyPulse(
 					 RF_MAGNITUDE_AXIS,
 					 gkg::RadioFrequencyPulse::Excitation,
 					 excitationStartingTime,
 					 excitationCentreTime,
 					 excitationEndingTime ) );

    // adding refocusing pulse ( duration = 6.0 ms );
    float sliceSelectionRephasingTime = 0.75 * MS_TO_US;
    float refocusingStartingTime = excitationEndingTime +
    				   sliceSelectionRephasingTime +
    				   waveformDurationInUs;
    float refocusingCentreTime = refocusingStartingTime + 3.0 * MS_TO_US;
    float refocusingEndingTime = refocusingCentreTime + 3.0 * MS_TO_US;

    gkg::RCPointer< gkg::Pulse >
      refocusingRFPulse( new gkg::RadioFrequencyPulse(
 					   RF_MAGNITUDE_AXIS,
 					   gkg::RadioFrequencyPulse::Refocusing,
 					   refocusingStartingTime,
 					   refocusingCentreTime,
 					   refocusingEndingTime ) );

    // adding RF pulses to the RF pulse sequences
    rfMagnitudePulseSequence->addPulse( excitationRFPulse );
    rfMagnitudePulseSequence->addPulse( refocusingRFPulse );

    /////////////////////// focusing on gradient pulses ////////////////////////

    // left diffusion-sensitization
    float dwLeftPulseStartingTime = excitationEndingTime +
    				    sliceSelectionRephasingTime;
    float dwPulsePeriod = periodInUs;
    float dwPulseLobeCount = lobeCount;
    float dwPulseMagnitude = maximumGradientAmplitude;

    gkg::RCPointer< gkg::Pulse > dwLabelingPulseOnXAxis =
      gkg::OscillatingGradientPulseFactory::getInstance().create(
                                                gradientWaveformName,
                                                GRADIENT_X_AXIS,
                                                dwLeftPulseStartingTime,
                                                dwPulsePeriod,
                                                dwPulseLobeCount,
                                                dwPulseMagnitude,
                                                maximumSlewRate,
                                                gradientTimeResolution );

    gkg::RCPointer< gkg::Pulse > dwLabelingPulseOnYAxis =
      gkg::OscillatingGradientPulseFactory::getInstance().create(
                                                gradientWaveformName,
                                                GRADIENT_Y_AXIS,
                                                dwLeftPulseStartingTime,
                                                dwPulsePeriod,
                                                dwPulseLobeCount,
                                                dwPulseMagnitude,
                                                maximumSlewRate,
                                                gradientTimeResolution );

    gkg::RCPointer< gkg::Pulse > dwLabelingPulseOnZAxis =
      gkg::OscillatingGradientPulseFactory::getInstance().create(
                                                gradientWaveformName,
                                                GRADIENT_Z_AXIS,
                                                dwLeftPulseStartingTime,
                                                dwPulsePeriod,
                                                dwPulseLobeCount,
                                                dwPulseMagnitude,
                                                maximumSlewRate,
                                                gradientTimeResolution );

    // right diffusion-sensitization
    float dwRightPulseStartingTime = refocusingEndingTime;

    gkg::RCPointer< gkg::Pulse > dwDelabelingPulseOnXAxis =
      gkg::OscillatingGradientPulseFactory::getInstance().create(
                                                gradientWaveformName,
                                                GRADIENT_X_AXIS,
                                                dwRightPulseStartingTime,
                                                dwPulsePeriod,
                                                dwPulseLobeCount,
                                                dwPulseMagnitude,
                                                maximumSlewRate,
                                                gradientTimeResolution );

    gkg::RCPointer< gkg::Pulse > dwDelabelingPulseOnYAxis =
      gkg::OscillatingGradientPulseFactory::getInstance().create(
                                                gradientWaveformName,
                                                GRADIENT_Y_AXIS,
                                                dwRightPulseStartingTime,
                                                dwPulsePeriod,
                                                dwPulseLobeCount,
                                                dwPulseMagnitude,
                                                maximumSlewRate,
                                                gradientTimeResolution );

    gkg::RCPointer< gkg::Pulse > dwDelabelingPulseOnZAxis =
      gkg::OscillatingGradientPulseFactory::getInstance().create(
                                                gradientWaveformName,
                                                GRADIENT_Z_AXIS,
                                                dwRightPulseStartingTime,
                                                dwPulsePeriod,
                                                dwPulseLobeCount,
                                                dwPulseMagnitude,
                                                maximumSlewRate,
                                                gradientTimeResolution );

    // adding DW gradient pulses to the gradient pulse sequences
    xGradientPulseSequence->addPulse( dwLabelingPulseOnXAxis );
    yGradientPulseSequence->addPulse( dwLabelingPulseOnYAxis );
    zGradientPulseSequence->addPulse( dwLabelingPulseOnZAxis );
    xGradientPulseSequence->addPulse( dwDelabelingPulseOnXAxis );
    yGradientPulseSequence->addPulse( dwDelabelingPulseOnYAxis );
    zGradientPulseSequence->addPulse( dwDelabelingPulseOnZAxis );


    ///////////////////////// adding pulse sequences ///////////////////////////
    this->addPulseSequence( rfMagnitudePulseSequence );
    this->addPulseSequence( rfPhasePulseSequence );
    this->addPulseSequence( xGradientPulseSequence );
    this->addPulseSequence( yGradientPulseSequence );
    this->addPulseSequence( zGradientPulseSequence );

    // finally setting echo time
    float echoTimeInUs = refocusingCentreTime * 2.0;
    this->setEchoTimeInUs( echoTimeInUs );

    // printing the sequence parameters if required
    if ( verbose )
    {

      std::cout << ""
                << std::endl;
      std::cout << "  sequence : ogse_nmr_sequence"
                << std::endl;
      std::cout << "  waveform : " << gradientWaveformName
                << std::endl;
      std::cout << "  frequency : " << 1e6f / periodInUs
                << std::endl;
      std::cout << "  period : " << periodInUs << " us"
                << std::endl;
      std::cout << "  gradient lobe count : " << lobeCount
                << std::endl;
      std::cout << "  waveform duration : " << waveformDurationInUs << " us"
                << std::endl;
      std::cout << "  echo time : " << echoTimeInUs << " us"
                << std::endl;

    }

  }
  GKG_CATCH( "gkg::OscillatingGradientSENMRSequence:: "
             "OscillatingGradientSENMRSequence( "
             "const std::set< float >& gradientAmplitudes, "
             "const std::vector< float >& scalarParameters, "
             "const std::vector< std::string >& stringParameters, "
             "bool verbose )" );

}

                                   
gkg::OscillatingGradientSENMRSequence::~OscillatingGradientSENMRSequence()  
{
}


void gkg::OscillatingGradientSENMRSequence::checkOrInitializeDefaultParameters(
                                  std::vector< float >& scalarParameters,
                                  std::vector< std::string >& stringParameters )
{

  try
  {

    if ( scalarParameters.size() != 4U )
    {

      throw std::runtime_error( "invalid scalar parameter count" );

    }
    if ( stringParameters.size() != 1U)
    {

      throw std::runtime_error( "invalid string parameter count" );

    }
    if ( ( stringParameters[ 0 ] != "sine" ) &&
         ( stringParameters[ 0 ] != "double-sine" ) &&
         ( stringParameters[ 0 ] != "cosine" ) &&
         ( stringParameters[ 0 ] != "trapezoid" ) )
    {

      throw std::runtime_error( "invalid string parameter" );

    }

  }
  GKG_CATCH( "void gkg::OscillatingGradientSENMRSequence:: "
             "checkOrInitializeDefaultParameters( "
             "std::vector< float >& scalarParameters, "
             "std::vector< std::string >& stringParameters )" );

}


#undef GAM
#undef MT_PER_M_TO_G_PER_CM
#undef MS_TO_US

