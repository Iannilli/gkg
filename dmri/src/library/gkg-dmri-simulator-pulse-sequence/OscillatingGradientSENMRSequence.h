#ifndef _gkg_dmri_simulator_pulse_sequence_OscillatingGradientSENMRSequence_h_
#define _gkg_dmri_simulator_pulse_sequence_OscillatingGradientSENMRSequence_h_


#include <gkg-dmri-simulator-pulse-sequence/NMRSequence.h>


namespace gkg
{


class OscillatingGradientSENMRSequence : public NMRSequence
{

  public:

    // gradient amplitudes should be given in mT/m
    // waveform duration should be given in ms
    OscillatingGradientSENMRSequence(
                             const std::set< float >& gradientAmplitudes,
                             const std::vector< float >& scalarParameters,
                             const std::vector< std::string >& stringParameters,
                             bool verbose );
    virtual ~OscillatingGradientSENMRSequence();

    static void checkOrInitializeDefaultParameters(
                                 std::vector< float >& scalarParameters,
                                 std::vector< std::string >& stringParameters );

};


}


#endif
