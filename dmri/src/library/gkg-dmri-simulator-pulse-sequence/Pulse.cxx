#include <gkg-dmri-simulator-pulse-sequence/Pulse.h>
#include <gkg-core-exception/Exception.h>


gkg::Pulse::Pulse( int32_t pulseType, int32_t axis )
           : gkg::RCObject(),
             _pulseType( pulseType ),
             _axis( axis )
{

  try
  {

    _breakPoints.reserve( 32 );

  }
  GKG_CATCH( "gkg::Pulse::Pulse( int32_t pulseType, int32_t axis )" );

}


gkg::Pulse::~Pulse()
{
}


int32_t gkg::Pulse::getPulseType() const
{

  try
  {

    return _pulseType;

  }
  GKG_CATCH( "int32_t gkg::Pulse::getPulseType() const" );

}


int32_t gkg::Pulse::getAxis() const
{

  try
  {

    return _axis;

  }
  GKG_CATCH( "int32_t gkg::Pulse::getAxis() const" );

}


float gkg::Pulse::getStartingTime() const
{

  try
  {

    return _breakPoints.front();

  }
  GKG_CATCH( "float gkg::Pulse::getStartingTime() const" );

}


float gkg::Pulse::getEndingTime() const
{

  try
  {

    return _breakPoints.back();

  }
  GKG_CATCH( "float gkg::Pulse::getEndingTime() const" );

}


float gkg::Pulse::getDuration() const
{

  try
  {

    return _breakPoints.back() - _breakPoints.front();

  }
  GKG_CATCH( "float gkg::Pulse::getDuration() const" );

}


void gkg::Pulse::addBreakPoint( float time )
{

  try
  {

    if ( !_breakPoints.empty() )
    {

      if ( time <= _breakPoints.back() )
      {

        throw std::runtime_error(
                               "new time older than actual ending breakpoint" );

      }

    }
    _breakPoints.push_back( time );

  }
  GKG_CATCH( "void gkg::Pulse::addBreakPoint( float time )" );

}

