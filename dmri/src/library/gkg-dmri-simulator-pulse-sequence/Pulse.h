#ifndef _gkg_dmri_simulator_pulse_sequence_Pulse_h_
#define _gkg_dmri_simulator_pulse_sequence_Pulse_h_


#include <gkg-dmri-simulator-pulse-sequence/NMRDefines.h>
#include <gkg-core-pattern/RCPointer.h>
#include <vector>


namespace gkg
{


// times are provided in us


class Pulse : public RCObject
{

  public:

    virtual ~Pulse();

    int32_t getAxis() const;
    int32_t getPulseType() const;

    float getStartingTime() const;
    float getEndingTime() const;
    float getDuration() const;

    virtual float getValueAt( float time,
                              float scaling ) const = 0;
    virtual float getSquareValueAt( float time,
                                    float scaling ) const = 0;
    virtual float getIntegralValueAt( float time,
                                      float scaling ) const = 0;
    virtual float getIntegralSquareValueAt( float time,
                                            float scaling ) const = 0;

  protected:

    Pulse( int32_t pulseType, int32_t axis );

    void addBreakPoint( float time );

    int32_t _pulseType;
    int32_t _axis;
    std::vector< float > _breakPoints;

};


}


#endif
