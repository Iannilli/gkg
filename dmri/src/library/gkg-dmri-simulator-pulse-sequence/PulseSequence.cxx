#include <gkg-dmri-simulator-pulse-sequence/PulseSequence.h>
#include <gkg-core-exception/Exception.h>


gkg::PulseSequence::PulseSequence( int32_t axis )
                   : gkg::RCObject(),
                     _axis( axis )
{
}


gkg::PulseSequence::~PulseSequence()
{
}


void gkg::PulseSequence::addPulse( gkg::RCPointer< gkg::Pulse > pulse )
{

  try
  {

    if ( pulse.isNull() )
    {

      throw std::runtime_error( "empty pointer" );

    }

    if ( pulse->getAxis() != _axis )
    {

      throw std::runtime_error( "incompatible pulse and pulse sequence axis" );

    }

    float startingTime = pulse->getStartingTime();
    float endingTime = pulse->getEndingTime();

    std::map< float, gkg::RCPointer< gkg::Pulse > >::const_iterator
      p = _pulses.begin(),
      pe = _pulses.end();
    while ( p != pe )
    {

      if ( ( ( p->first >= startingTime ) &&
             ( p->first < endingTime ) ) ||
           ( ( p->second->getEndingTime() > startingTime ) &&
             ( p->second->getEndingTime() <= endingTime ) ) ||
           ( ( p->first <= startingTime ) &&
             ( p->second->getEndingTime() >= endingTime ) ) )
      {

        throw std::runtime_error( "pulse already present" );

      }
      ++ p;

    }
    _pulses[ startingTime ] = pulse;

  }
  GKG_CATCH( "void gkg::PulseSequence::addPulse( "
             "gkg::RCPointer< gkg::Pulse > pulse )" );

}


void gkg::PulseSequence::addPulses(
                            std::vector< gkg::RCPointer< gkg::Pulse > > pulses )
{

  try
  {

    int32_t pulseCount = pulses.size();
    int32_t p = 0;
    for ( p = 0; p < pulseCount; p++ )
    {

      addPulse( pulses[ p ] );

    }

  }
  GKG_CATCH( "void gkg::PulseSequence::addPulses( "
             "std::vector< gkg::RCPointer< gkg::Pulse > > pulses )" );

}


bool gkg::PulseSequence::getActivePulseAt(
                                     float time,
                                     gkg::RCPointer< gkg::Pulse >& pulse ) const
{

  try
  {

    bool activePulseExists = false;

    std::map< float, gkg::RCPointer< gkg::Pulse > >::const_iterator
      p = _pulses.begin(),
      pe = _pulses.end();
    while ( p != pe )
    {

      if ( ( time >= p->first ) &&
           ( time < p->second->getEndingTime() ) )
      {

        pulse = p->second;
        activePulseExists = true;
        break;

      }
      ++ p;

    }
    return activePulseExists;

  }
  GKG_CATCH( "bool gkg::PulseSequence::getActivePulseAt( "
             "float time, "
             "gkg::RCPointer< gkg::Pulse > pulse ) const" );

}


int32_t gkg::PulseSequence::getAxis() const
{

  try
  {

    return _axis;

  }
  GKG_CATCH( "int32_t gkg::PulseSequence::getAxis() const" );

}


const std::map< float, gkg::RCPointer< gkg::Pulse > >&
gkg::PulseSequence::getPulses() const
{

  try
  {

    return _pulses;

  }
  GKG_CATCH( "const std::map< float, gkg::RCPointer< gkg::Pulse > >& "
             "gkg::PulseSequence::getPulses() const" )

}


float gkg::PulseSequence::getValueAt( float time, float scaling ) const
{

  try
  {

    float value = 0.0;
    gkg::RCPointer< gkg::Pulse > pulse;
    if ( getActivePulseAt( time, pulse ) )
    {

      value = pulse->getValueAt( time, scaling );

    }
    return value;

  }
  GKG_CATCH( "float gkg::PulseSequence::getValueAt( "
             "float time, "
             "float scaling ) const" );

}


float gkg::PulseSequence::getSquareValueAt( float time, float scaling ) const
{

  try
  {

    float value = 0.0;
    gkg::RCPointer< gkg::Pulse > pulse;
    if ( getActivePulseAt( time, pulse ) )
    {

      value = pulse->getSquareValueAt( time, scaling );

    }
    return value;

  }
  GKG_CATCH( "float gkg::PulseSequence::getSquareValueAt( "
             "float time, "
             "float scaling ) const" );

}


float gkg::PulseSequence::getIntegralValueAt( float time, float scaling ) const
{

  try
  {

    float value = 0.0;
    std::map< float, gkg::RCPointer< gkg::Pulse > >::const_iterator
      p = _pulses.begin(),
      pe = _pulses.end();
    while ( p != pe )
    {

      value += p->second->getIntegralValueAt( time, scaling );
      ++ p;

    }
    return value;

  }
  GKG_CATCH( "float gkg::PulseSequence::getIntegralValueAt( "
             "float time, "
             "float scaling ) const" );

}


float gkg::PulseSequence::getIntegralSquareValueAt( float time,
                                                    float scaling ) const
{

  try
  {

    float value = 0.0;
    std::map< float, gkg::RCPointer< gkg::Pulse > >::const_iterator
      p = _pulses.begin(),
      pe = _pulses.end();
    while ( p != pe )
    {

      value += p->second->getIntegralSquareValueAt( time, scaling );
      ++ p;

    }
    return value;

  }
  GKG_CATCH( "float gkg::PulseSequence::getIntegralSquareValueAt( "
             "float time, "
             "float scaling ) const" );

}

