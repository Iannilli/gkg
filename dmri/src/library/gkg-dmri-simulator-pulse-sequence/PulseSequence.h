#ifndef _gkg_dmri_simulator_pulse_sequence_PulseSequence_h_
#define _gkg_dmri_simulator_pulse_sequence_PulseSequence_h_


#include <gkg-dmri-simulator-pulse-sequence/Pulse.h>
#include <gkg-core-pattern/RCPointer.h>
#include <map>
#include <vector>


namespace gkg
{


class PulseSequence : public RCObject
{

  public:

    PulseSequence( int32_t axis );
    virtual ~PulseSequence();

    void addPulse( RCPointer< Pulse > pulse );
    void addPulses( std::vector< RCPointer< Pulse > > pulses );
    bool getActivePulseAt( float time,
                           RCPointer< Pulse >& pulse ) const;
    int32_t getAxis() const;
    const std::map< float, RCPointer< Pulse > >& getPulses() const;

    float getValueAt( float time, float scaling ) const;
    float getSquareValueAt( float time, float scaling ) const;
    float getIntegralValueAt( float time, float scaling ) const;
    float getIntegralSquareValueAt( float time, float scaling ) const;

  protected:

    int32_t _axis;
    std::map< float, RCPointer< Pulse > > _pulses;

};


}


#endif
