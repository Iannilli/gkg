#include <gkg-dmri-simulator-pulse-sequence/RadioFrequencyPulse.h>
#include <gkg-dmri-simulator-pulse-sequence/NMRDefines.h>
#include <gkg-core-exception/Exception.h>


gkg::RadioFrequencyPulse::RadioFrequencyPulse( int32_t axis,
                gkg::RadioFrequencyPulse::RadioFrequencyType radioFrequencyType,
                float startingTime,
                float centreTime,
                float endingTime )
                         : gkg::Pulse( RF_PULSE_TYPE, axis ),
                           _radioFrequencyType( radioFrequencyType )
{

  try
  {

    addBreakPoint( startingTime );
    addBreakPoint( centreTime );
    addBreakPoint( endingTime );

  }
  GKG_CATCH( "gkg::RadioFrequencyPulse::RadioFrequencyPulse( "
             "int32_t axis, "
             "gkg::RadioFrequencyPulse::RadioFrequencyType radioFrequencyType, "
             "float startingTime, "
             "float centreTime, "
             "float endingTime )" );

}


gkg::RadioFrequencyPulse::~RadioFrequencyPulse()
{
}


gkg::RadioFrequencyPulse::RadioFrequencyType 
gkg::RadioFrequencyPulse::getRadioFrequencyType() const
{

  try
  {

    return _radioFrequencyType;

  }
  GKG_CATCH( "gkg::RadioFrequencyPulse::RadioFrequencyType "
             "gkg::RadioFrequencyPulse::getRadioFrequencyType() const" );

}


float gkg::RadioFrequencyPulse::getCentreTime() const
{

  try
  {

    return _breakPoints[ 1 ];

  }
  GKG_CATCH( "float gkg::RadioFrequencyPulse::getCentreTime() const" );

}


float gkg::RadioFrequencyPulse::getValueAt( float time,
                                            float /*scaling*/ ) const
{

  try
  {

    float value = 0.0;
    if ( ( time >= _breakPoints.front() ) &&
         ( time <= _breakPoints.back() ) )
    {

      value = 1.0;

    }  

    return value;

  }
  GKG_CATCH( "float gkg::RadioFrequencyPulse::getValueAt( "
             "float time, "
             "float /*scaling*/ ) const" );

}



float gkg::RadioFrequencyPulse::getSquareValueAt( float time,
                                                  float /*scaling*/ ) const
{

  try
  {

    float value = 0.0;
    if ( ( time >= _breakPoints.front() ) &&
         ( time <= _breakPoints.back() ) )
    {

      value = 1.0;

    }  

    return value;

  }
  GKG_CATCH( "float gkg::RadioFrequencyPulse::getSquareValueAt( "
             "float time, "
             "float /*scaling*/ ) const" );

}


float gkg::RadioFrequencyPulse::getIntegralValueAt( float /*time*/,
                                                    float /*scaling*/ ) const
{

  try
  {

    float value = 0.0;

    // to be implemented

    return value;

  }
  GKG_CATCH( "float gkg::RadioFrequencyPulse::getIntegralValueAt( "
             "float /*time*/, "
             "float /*scaling*/ ) const" );

}


float gkg::RadioFrequencyPulse::getIntegralSquareValueAt(
                                                       float /*time*/,
                                                       float /*scaling*/ ) const
{

  try
  {

    float value = 0.0;

    // to be implemented

    return value;

  }
  GKG_CATCH( "float gkg::RadioFrequencyPulse::getIntegralValueAt( "
             "float /*time*/, "
             "float /*scaling*/ ) const" );

}


