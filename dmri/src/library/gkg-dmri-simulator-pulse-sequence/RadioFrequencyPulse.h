#ifndef _gkg_dmri_simulator_pulse_sequence_RadioFrequencyPulse_h_
#define _gkg_dmri_simulator_pulse_sequence_RadioFrequencyPulse_h_


#include <gkg-dmri-simulator-pulse-sequence/Pulse.h>


namespace gkg
{


class RadioFrequencyPulse : public Pulse
{

  public:

    enum RadioFrequencyType
    {

      Excitation,
      Refocusing

    };

    RadioFrequencyPulse( int32_t axis,
                         RadioFrequencyType radioFrequencyType,
                         float startingTime,
                         float centreTime,
                         float endingTime );
    virtual ~RadioFrequencyPulse();

    RadioFrequencyType getRadioFrequencyType() const;
    float getCentreTime() const;

    virtual float getValueAt( float time,
                              float /*scaling*/ ) const;
    virtual float getSquareValueAt( float time,
                                    float /*scaling*/ ) const;
    virtual float getIntegralValueAt( float time,
                                      float /*scaling*/ ) const;
    virtual float getIntegralSquareValueAt( float time,
                                            float /*scaling*/ ) const;

  protected:

    RadioFrequencyType _radioFrequencyType;

};


}


#endif

