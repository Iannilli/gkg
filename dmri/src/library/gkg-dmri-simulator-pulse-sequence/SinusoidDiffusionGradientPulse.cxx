#include <gkg-dmri-simulator-pulse-sequence/SinusoidDiffusionGradientPulse.h>
#include <gkg-core-exception/Exception.h>


gkg::SinusoidDiffusionGradientPulse::SinusoidDiffusionGradientPulse(
                                                             int32_t axis,
                                                             float startingTime,
                                                             float period,
                                                             int32_t lobeCount,
                                                             float magnitude )
                                    : gkg::SinusoidGradientPulse( axis,
                                                                  startingTime,
                                                                  period,
                                                                  lobeCount,
                                                                  magnitude )
{
}


gkg::SinusoidDiffusionGradientPulse::~SinusoidDiffusionGradientPulse()
{
}


float gkg::SinusoidDiffusionGradientPulse::getValueAt( float time,
                                                       float scaling ) const
{

  try
  {

    return this->gkg::SinusoidGradientPulse::getValueAt( time, scaling ) *
           scaling;

  }
  GKG_CATCH( "float gkg::SinusoidDiffusionGradientPulse::getValueAt( "
             "float time, "
             "float scaling ) const" );

}


float gkg::SinusoidDiffusionGradientPulse::getSquareValueAt(
                                                           float time,
                                                           float scaling ) const
{

  try
  {

    return this->gkg::SinusoidGradientPulse::getSquareValueAt( time, scaling ) *
           scaling * scaling;

  }
  GKG_CATCH( "float gkg::SinusoidDiffusionGradientPulse::getSquareValueAt( "
             "float time, "
             "float scaling ) const" );

}


float gkg::SinusoidDiffusionGradientPulse::getIntegralValueAt( 
                                                           float time,
                                                           float scaling ) const
{

  try
  {

    return this->gkg::SinusoidGradientPulse::getIntegralValueAt( time,
                                                                 scaling ) *
           scaling;

  }
  GKG_CATCH( "float gkg::SinusoidDiffusionGradientPulse::getIntegralValueAt( "
             "float time, "
             "float scaling ) const" );

}


float gkg::SinusoidDiffusionGradientPulse::getIntegralSquareValueAt(
                                                           float time,
                                                           float scaling ) const
{

  try
  {

    return this->gkg::SinusoidGradientPulse::getIntegralSquareValueAt(
                                                                  time,
                                                                  scaling ) *
           scaling * scaling;

  }
  GKG_CATCH( "float gkg::SinusoidDiffusionGradientPulse::getIntegralSquareValueAt( "
             "float time, "
             "float scaling ) const" );

}

