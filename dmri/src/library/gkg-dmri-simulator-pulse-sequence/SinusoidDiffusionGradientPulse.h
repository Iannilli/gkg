#ifndef _gkg_dmri_simulator_pulse_sequence_SinusoidDiffusionGradientPulse_h_
#define _gkg_dmri_simulator_pulse_sequence_SinusoidDiffusionGradientPulse_h_


#include <gkg-dmri-simulator-pulse-sequence/SinusoidGradientPulse.h>


namespace gkg
{


// gradient magnitudes are provided in mT/m


class SinusoidDiffusionGradientPulse : public SinusoidGradientPulse
{

  public:

    SinusoidDiffusionGradientPulse( int32_t axis,
                                    float startingTime,
                                    float period,
                                    int32_t lobeCount,
                                    float magnitude );
    virtual ~SinusoidDiffusionGradientPulse();

    virtual float getValueAt( float time,
                              float scaling ) const;
    virtual float getSquareValueAt( float time,
                                    float scaling ) const;
    virtual float getIntegralValueAt( float time,
                                      float scaling ) const;
    virtual float getIntegralSquareValueAt( float time,
                                            float scaling ) const;

};


}


#endif

