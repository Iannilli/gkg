#include <gkg-dmri-simulator-pulse-sequence/SinusoidGradientPulse.h>
#include <gkg-dmri-simulator-pulse-sequence/NMRDefines.h>
#include <gkg-core-exception/Exception.h>
#include <cmath>


gkg::SinusoidGradientPulse::SinusoidGradientPulse( int32_t axis,
                                                   float startingTime,
                                                   float period,
                                                   int32_t lobeCount,
                                                   float magnitude )
                           : gkg::GradientPulse( axis ),
                             _period( period ),
                             _lobeCount( lobeCount ),
                             _magnitude( magnitude ),
                             _angularFrequency( 2.0 * M_PI / period )
{

  try
  {

    _pulseType = GRADIENT_PULSE_TYPE;

    float time = startingTime;
    addBreakPoint( time );
    time += ( _period * ( float )_lobeCount );
    addBreakPoint( time );

  }
  GKG_CATCH( "gkg::SinusoidGradientPulse::SinusoidGradientPulse( "
             "int32_t axis, "
             "float startingTime, "
             "float period, "
             "int32_t lobeCount, "
             "float magnitude )" );

}


gkg::SinusoidGradientPulse::~SinusoidGradientPulse()
{
}


float gkg::SinusoidGradientPulse::getPeriod() const
{

  try
  {

    return _period;

  }
  GKG_CATCH( "float gkg::SinusoidGradientPulse::getPeriod() const" );

}


int32_t gkg::SinusoidGradientPulse::getLobeCount() const
{

  try
  {

    return _lobeCount;

  }
  GKG_CATCH( "int32_t gkg::SinusoidGradientPulse::getLobeCount() const" );

}


float gkg::SinusoidGradientPulse::getMagnitude() const
{

  try
  {

    return _magnitude;

  }
  GKG_CATCH( "float gkg::SinusoidGradientPulse::getAmplitdue() const" );

}


float gkg::SinusoidGradientPulse::getAngularFrequency() const
{

  try
  {

    return _angularFrequency;

  }
  GKG_CATCH( "float gkg::SinusoidGradientPulse::getAngularFrequency() const" );

}


float gkg::SinusoidGradientPulse::getValueAt( float time,
                                              float /*scaling*/ ) const
{

  try
  {	

    float value = 0.0;

    if ( ( time > _breakPoints[ 0 ] ) &&
	 ( time <= _breakPoints[ 1 ] ) )
    {

      float r = _angularFrequency * ( time - _breakPoints[ 0 ] );
      value = _magnitude * std::sin( r );

    }

    return value;

  }
  GKG_CATCH( "float gkg::SinusoidGradientPulse::getSquareValueAt( "
             "float time, "
             "float /*scaling*/ ) const" );

}


float gkg::SinusoidGradientPulse::getSquareValueAt( float time,
                                                    float /*scaling*/ ) const
{

  try
  {

    float value = 0.0;

    if ( ( time > _breakPoints[ 0 ] ) &&
	 ( time <= _breakPoints[ 1 ] ) )
    {
       
      float r = _angularFrequency * ( time - _breakPoints[ 0 ] );
      value = _magnitude * std::sin( r );
   
    }

    return value * value;
   
  }
  GKG_CATCH( "float gkg::SinusoidGradientPulse::getSquareValueAt( "
             "float time, "
             "float /*scaling*/ ) const" );

}


float gkg::SinusoidGradientPulse::getIntegralValueAt( float time,
                                                      float /*scaling*/ ) const
{

  try
  {

    float value = 0.0;

    if ( ( time > _breakPoints[ 0 ] ) &&
         ( time <= _breakPoints[ 1 ] ) )
    {

      float r = _angularFrequency * ( time - _breakPoints[ 0 ] );
      value = _magnitude * ( 1.0 - std::cos( r ) ) / _angularFrequency;

    }

    return value;

  }
  GKG_CATCH( "float gkg::SinusoidGradientPulse::getSquareValueAt( "
             "float time, "
             "float /*scaling*/ ) const" );

}


float gkg::SinusoidGradientPulse::getIntegralSquareValueAt(
                                                       float time,
                                                       float /*scaling*/ ) const
{

  try
  {
  
    float value = 0.0;

    if ( ( time > _breakPoints[ 0 ] ) &&
         ( time <= _breakPoints[ 1 ] ) )
    {

      float t = time - _breakPoints[ 0 ];
      float r = _angularFrequency * t;
      value = _magnitude * _magnitude *
              ( t / 2.0 - std::sin( 2.0 * r ) / ( 4.0 * _angularFrequency ) );

    }
    else if ( time > _breakPoints[ 1 ] )
    {

      float t = _breakPoints[ 1 ] - _breakPoints[ 0 ];
      float r = _angularFrequency * t;
      value = _magnitude * _magnitude *
              ( t / 2.0 - std::sin( 2.0 * r ) / ( 4.0 * _angularFrequency ) );

    }

    return value;

  }
  GKG_CATCH( "float gkg::SinusoidGradientPulse::getSquareValueAt( "
             "float time, "
             "float /*scaling*/ ) const" );

}

