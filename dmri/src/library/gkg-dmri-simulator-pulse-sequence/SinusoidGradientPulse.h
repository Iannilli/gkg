#ifndef _gkg_dmri_simulator_pulse_sequence_SinusoidGradientPulse_h_
#define _gkg_dmri_simulator_pulse_sequence_SinusoidGradientPulse_h_


#include <gkg-dmri-simulator-pulse-sequence/GradientPulse.h>


namespace gkg
{


// sine wave: g( t ) = G * sin( w * t );
//
// G : magnitude ( mT/m )
// w : angular frequency


class SinusoidGradientPulse : public GradientPulse
{

  public:

    SinusoidGradientPulse( int32_t axis,
                           float startingTime,
                           float period,
                           int32_t lobeCount,
                           float magnitude );
    virtual ~SinusoidGradientPulse();

    float getPeriod() const;
    int32_t getLobeCount() const;
    float getMagnitude() const;
    float getAngularFrequency() const;

    virtual float getValueAt( float time,
                              float /*scaling*/ ) const;
    virtual float getSquareValueAt( float time,
                                    float /*scaling*/ ) const;
    virtual float getIntegralValueAt( float time,
                                      float /*scaling*/ ) const;
    virtual float getIntegralSquareValueAt( float time,
                                            float /*scaling*/ ) const;

  protected:

    float _period;
    int32_t _lobeCount;
    float _magnitude;
    float _angularFrequency;

};


}


#endif

