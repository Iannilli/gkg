#include <gkg-dmri-simulator-pulse-sequence/StimulatedEchoNMRSequence.h>
#include <gkg-dmri-simulator-pulse-sequence/RadioFrequencyPulse.h>
#include <gkg-dmri-simulator-pulse-sequence/DiffusionGradientPulse.h>
#include <gkg-core-exception/Exception.h>
#include <cmath>


#define TWOPI_GAM 26748.0
#define GAM 26748.0/ (2*M_PI)

#define MT_PER_M_TO_G_PER_CM              0.1
#define T_PER_M_PER_S_TO_G_PER_CM_PER_US  0.0001
#define MS_TO_US                          1000.0

#define RUP_GRD( A )  ( ( ( int32_t )gradientTimeResolution -       \
                          ( ( int32_t )( A ) %                      \
                            ( int32_t )gradientTimeResolution ) ) % \
                          ( int32_t )gradientTimeResolution +       \
                        ( int32_t )( A ) )


gkg::StimulatedEchoNMRSequence::StimulatedEchoNMRSequence(
                       const std::set< float >& gradientAmplitudes,
                       const std::vector< float >& scalarParameters,
                       const std::vector< std::string >& /* stringParameters */,
                       bool verbose )
                               : gkg::NMRSequence( gradientAmplitudes, verbose )
{

  try
  {

    // collecting sequence scalar parameters
    float maximumSlewRate = scalarParameters[ 0 ];
    float gradientTimeResolution = scalarParameters[ 1 ];
    float littleDeltaInMs = scalarParameters[ 2 ];
    float bigDeltaInMs = scalarParameters[ 3 ];

    //
    // sanity check
    //
    if ( bigDeltaInMs < ( littleDeltaInMs + 7.5 ) )
    {

      // assume that the 90-degree RF pulse duration is 3 ms and its rephasing
      // gradient lasts 0.75 ms
      throw std::runtime_error( "Invalid delta/DELTA combination." );

    }

    //
    // processing the diffusion gradient parameters
    //
    float maximumGradientAmplitude = this->_gradientAmplitudes.back();
    float maximumGradientAmplitudeInGaussPerCm = maximumGradientAmplitude *
                                                 MT_PER_M_TO_G_PER_CM;
    float maximumSlewRateInGaussPerCmPerUs = maximumSlewRate *
                                             T_PER_M_PER_S_TO_G_PER_CM_PER_US;

    float littleDeltaInUs = littleDeltaInMs * MS_TO_US;
    float bigDeltaInUs = bigDeltaInMs * MS_TO_US;
    float timeSeparationInUs = bigDeltaInUs - littleDeltaInUs;
    float rampWidthInUs = 0.0;
    float plateauWidthInUs = getDiffusionPulsePlateauWidthInUs(
                                           maximumGradientAmplitudeInGaussPerCm,
                                           maximumSlewRateInGaussPerCmPerUs,
                                           littleDeltaInUs,
                                           gradientTimeResolution,
                                           rampWidthInUs );

    _effectiveDiffusionTimeInMs = bigDeltaInMs - littleDeltaInMs / 3.0;
    _maximumQInMeterInverse = GAM * 1e-2 * littleDeltaInMs *
                              maximumGradientAmplitude;

    //
    // processing the mixing time
    //
    float timeDelayOfExcitationRFInUs = 0.0;
    float sliceSelectionRephasingTimeInUs = 0.75 * MS_TO_US;
    float mixingTimeInUs = timeSeparationInUs - timeDelayOfExcitationRFInUs -
                           sliceSelectionRephasingTimeInUs - 3.0 * MS_TO_US;

    //
    // creating and adding pulses to the NMR sequence
    //

    ///////////// creating RF + Gslice + Gphase + Gread pulse sequences ////////
    gkg::RCPointer< gkg::PulseSequence >
      rfMagnitudePulseSequence( new gkg::PulseSequence( RF_MAGNITUDE_AXIS ) );

    gkg::RCPointer< gkg::PulseSequence >
      rfPhasePulseSequence( new gkg::PulseSequence( RF_PHASE_AXIS ) );

    gkg::RCPointer< gkg::PulseSequence >
      xGradientPulseSequence( new gkg::PulseSequence( GRADIENT_X_AXIS ) );

    gkg::RCPointer< gkg::PulseSequence >
      yGradientPulseSequence( new gkg::PulseSequence( GRADIENT_Y_AXIS ) );

    gkg::RCPointer< gkg::PulseSequence >
      zGradientPulseSequence( new gkg::PulseSequence( GRADIENT_Z_AXIS ) );

    /////////////////////////// focusing on RF pulses //////////////////////////

    // adding the 1st excitation pulse (slice selection gradient duration = 3 ms
    // and the rephasing gradient duration = 0.75 ms
    float firstRFStartingTime = -1.5 * MS_TO_US;
    float firstRFCentreTime = 0.0 * MS_TO_US;
    float firstRFEndingTime = 1.5 * MS_TO_US;

    gkg::RCPointer< gkg::Pulse >
      firstRFPulse( new gkg::RadioFrequencyPulse(
                                           RF_MAGNITUDE_AXIS,
                                           gkg::RadioFrequencyPulse::Excitation,
                                           firstRFStartingTime,
                                           firstRFCentreTime,
                          	           firstRFEndingTime ) );

    // adding the 2nd excitation pulse (slice selection gradient duration = 3 ms
    // and the rephasing gradient duration = 0.75 ms
    float secondRFStartingTime = firstRFEndingTime +
                                 sliceSelectionRephasingTimeInUs +
                                 littleDeltaInUs +
                                 timeDelayOfExcitationRFInUs;
    float secondRFCentreTime = secondRFStartingTime + 1.5 * MS_TO_US;
    float secondRFEndingTime = secondRFCentreTime + 1.5 * MS_TO_US;

    gkg::RCPointer< gkg::Pulse >
      secondRFPulse( new gkg::RadioFrequencyPulse(
                                           RF_MAGNITUDE_AXIS,
                                           gkg::RadioFrequencyPulse::Excitation,
                                           secondRFStartingTime,
                                           secondRFCentreTime,
                          	           secondRFEndingTime ) );

    // adding the 3rd excitation pulse (slice selection gradient duration = 3 ms
    // and the rephasing gradient duration = 0.75 ms
    float thirdRFStartingTime = secondRFCentreTime + mixingTimeInUs -
                                1.5 * MS_TO_US;
    float thirdRFCentreTime = thirdRFStartingTime + 1.5 * MS_TO_US;
    float thirdRFEndingTime = thirdRFCentreTime + 1.5 * MS_TO_US;

    gkg::RCPointer< gkg::Pulse >
      thirdRFPulse( new gkg::RadioFrequencyPulse(
                                           RF_MAGNITUDE_AXIS,
                                           gkg::RadioFrequencyPulse::Excitation,
                                           thirdRFStartingTime,
                                           thirdRFCentreTime,
                          	           thirdRFEndingTime ) );

    // adding RF pulses to the RF pulse sequences
    rfMagnitudePulseSequence->addPulse( firstRFPulse );
    rfMagnitudePulseSequence->addPulse( secondRFPulse );
    rfMagnitudePulseSequence->addPulse( thirdRFPulse );

    /////////////////////// focusing on gradient pulses ////////////////////////

    // left diffusion-sensitization
    float dwLeftPulseStartingTime = firstRFEndingTime + 
                                    sliceSelectionRephasingTimeInUs;
    float dwPulseStartingRampDuration = rampWidthInUs;
    float dwPulsePlateauDuration = plateauWidthInUs;
    float dwPulseEndingRampDuration = rampWidthInUs;
    float dwPulseMagnitude = maximumGradientAmplitude;

    gkg::RCPointer< gkg::Pulse >
      dwLabelingPulseOnXAxis( new gkg::DiffusionGradientPulse(
                                         GRADIENT_X_AXIS,
                                         dwLeftPulseStartingTime,
                                         dwPulseStartingRampDuration,
                                         dwPulsePlateauDuration,
                                         dwPulseEndingRampDuration,
                                         dwPulseMagnitude ) );
    gkg::RCPointer< gkg::Pulse >
      dwLabelingPulseOnYAxis( new gkg::DiffusionGradientPulse(
                                         GRADIENT_Y_AXIS,
                                         dwLeftPulseStartingTime,
                                         dwPulseStartingRampDuration,
                                         dwPulsePlateauDuration,
                                         dwPulseEndingRampDuration,
                                         dwPulseMagnitude ) );
    gkg::RCPointer< gkg::Pulse >
      dwLabelingPulseOnZAxis( new gkg::DiffusionGradientPulse(
                                         GRADIENT_Z_AXIS,
                                         dwLeftPulseStartingTime,
                                         dwPulseStartingRampDuration,
                                         dwPulsePlateauDuration,
                                         dwPulseEndingRampDuration,
                                         dwPulseMagnitude ) );

    // right diffusion-sensitization
    float dwRightPulseStartingTime = dwLeftPulseStartingTime + bigDeltaInUs;

    gkg::RCPointer< gkg::Pulse >
      dwDelabelingPulseOnXAxis( new gkg::DiffusionGradientPulse(
                                           GRADIENT_X_AXIS,
                                           dwRightPulseStartingTime,
                                           dwPulseStartingRampDuration,
                                           dwPulsePlateauDuration,
                                           dwPulseEndingRampDuration,
                                           dwPulseMagnitude ) );
    gkg::RCPointer< gkg::Pulse >
      dwDelabelingPulseOnYAxis( new gkg::DiffusionGradientPulse(
                                           GRADIENT_Y_AXIS,
                                           dwRightPulseStartingTime,
                                           dwPulseStartingRampDuration,
                                           dwPulsePlateauDuration,
                                           dwPulseEndingRampDuration,
                                           dwPulseMagnitude ) );
    gkg::RCPointer< gkg::Pulse >
      dwDelabelingPulseOnZAxis( new gkg::DiffusionGradientPulse(
                                           GRADIENT_Z_AXIS,
                                           dwRightPulseStartingTime,
                                           dwPulseStartingRampDuration,
                                           dwPulsePlateauDuration,
                                           dwPulseEndingRampDuration,
                                           dwPulseMagnitude ) );

    // adding DW gradient pulses to the gradient pulse sequences
    xGradientPulseSequence->addPulse( dwLabelingPulseOnXAxis );
    yGradientPulseSequence->addPulse( dwLabelingPulseOnYAxis );
    zGradientPulseSequence->addPulse( dwLabelingPulseOnZAxis );
    xGradientPulseSequence->addPulse( dwDelabelingPulseOnXAxis );
    yGradientPulseSequence->addPulse( dwDelabelingPulseOnYAxis );
    zGradientPulseSequence->addPulse( dwDelabelingPulseOnZAxis );


    ///////////////////////// adding pulse sequences ///////////////////////////
    this->addPulseSequence( rfMagnitudePulseSequence );
    this->addPulseSequence( rfPhasePulseSequence );
    this->addPulseSequence( xGradientPulseSequence );
    this->addPulseSequence( yGradientPulseSequence );
    this->addPulseSequence( zGradientPulseSequence );

    // finally setting echo time
    this->setEchoTimeInUs( mixingTimeInUs + secondRFCentreTime * 2.0 );

  }
  GKG_CATCH( "gkg::StimulatedEchoNMRSequence::StimulatedEchoNMRSequence( "
             "const std::set< float >& gradientAmplitudes, "
             "const std::vector< float >& scalarParameters, "
             "const std::vector< std::string >& stringParameters, "
             "bool verbose )" );

}


gkg::StimulatedEchoNMRSequence::~StimulatedEchoNMRSequence()
{
}


void gkg::StimulatedEchoNMRSequence::checkOrInitializeDefaultParameters(
                                  std::vector< float >& scalarParameters,
                                  std::vector< std::string >& stringParameters )
{

  try
  {

    if ( scalarParameters.size() != 4U )
    {

      throw std::runtime_error( "invalid scalar parameter count" );

    }
    if ( !stringParameters.empty() )
    {

      throw std::runtime_error( "invalid string parameter count" );

    }

  }
  GKG_CATCH( "void gkg::StimulatedEchoNMRSequence:: "
             "checkOrInitializeDefaultParameters( "
             "std::vector< float >& scalarParameters, "
             "std::vector< std::string >& stringParameters )" );

}


float gkg::StimulatedEchoNMRSequence::getDiffusionPulsePlateauWidthInUs(
                                      float maximuGradientAmplitudeInGaussPerCm,
                                      float maximumSlewRateInGaussPerCmPerUs,
                                      float littleDeltaInUs,
                                      float gradientTimeResolution,
                                      float& rampWidthInUs )
{

  try
  {

    float plateauWidthInUs = 0.0;

    // processing the ramp width
    rampWidthInUs = ( float )RUP_GRD( ( int32_t )
                                      ( maximuGradientAmplitudeInGaussPerCm /
                                        maximumSlewRateInGaussPerCmPerUs ) );

    plateauWidthInUs = littleDeltaInUs - 2.0 * rampWidthInUs;
    // put plateau width on GRAD_UPDATE boundaries
    plateauWidthInUs = ( float )RUP_GRD( plateauWidthInUs );

    if ( plateauWidthInUs <= 0.0 )
    {

      throw std::runtime_error( "Invalid gradient plateau width." );

    }

    return plateauWidthInUs;

  }
  GKG_CATCH( "float "
             "gkg::StimulatedEchoNMRSequence:: "
             "getDiffusionPulsePlateauWidthInUs( "
             "float maximuGradientAmplitudeInGaussPerCm, "
             "float maximumSlewRateInGaussPerCmPerUs, "
             "float littleDeltaInUs, "
             "float gradientTimeResolution, "
             "float& rampWidthInUs )" );

}


#undef TWOPI_GAM
#undef GAM

#undef MT_PER_M_TO_G_PER_CM
#undef T_PER_M_PER_S_TO_G_PER_CM_PER_US
#undef MS_TO_US

#undef RUP_GRD

