#include <gkg-dmri-simulator-pulse-sequence/TrapezoidGradientPulse.h>
#include <gkg-dmri-simulator-pulse-sequence/NMRDefines.h>
#include <gkg-core-exception/Exception.h>


/*
              ^
              |
    magnitude +- - - - - -  +-------------+
              |            /|             |\ 
              |           / |             | \ 
              |          /  |             |  \ 
              |         /   |             |   \
              |        /    |             |    \
              +-------+-----+-------------+-----+------->time
                      1     2             3     4

              1: _breakPoints[ 0 ] ( startingTime )
              2: _breakPoints[ 1 ] 
              3: _breakPoints[ 2 ]
              4: _breakPoints[ 3 ]
*/


gkg::TrapezoidGradientPulse::TrapezoidGradientPulse( int32_t axis,
			                             float startingTime,
			                             float startingRampDuration,
			                             float plateauDuration,
			                             float endingRampDuration,
			                             float magnitude )
                            : gkg::GradientPulse( axis ),
                              _startingRampDuration( startingRampDuration ),
                              _plateauDuration( plateauDuration ),
                              _endingRampDuration( endingRampDuration ),
                              _magnitude( magnitude )
{

  try
  {

    _pulseType = GRADIENT_PULSE_TYPE;

    float time = startingTime;
    addBreakPoint( time );
    time += _startingRampDuration;
    addBreakPoint( time );
    time += _plateauDuration;
    addBreakPoint( time );
    time += _endingRampDuration;
    addBreakPoint( time );

  }
  GKG_CATCH( "gkg::TrapezoidGradientPulse::TrapezoidGradientPulse( "
             "int32_t axis, "
             "float startingTime, "
             "float startingRampDuration, "
             "float plateauDuration, "
             "float endingRampDuration, "
             "float magnitude );" );

}


gkg::TrapezoidGradientPulse::~TrapezoidGradientPulse()
{
}


float gkg::TrapezoidGradientPulse::getStartingRampDuration() const
{

  try
  {

    return _startingRampDuration;

  }
  GKG_CATCH( "float "
             "gkg::TrapezoidGradientPulse::getStartingRampDuration() const" );

}


float gkg::TrapezoidGradientPulse::getPlateauDuration() const
{

  try
  {

    return _plateauDuration;

  }
  GKG_CATCH( "float gkg::TrapezoidGradientPulse::getPlateauDuration() const" );

}


float gkg::TrapezoidGradientPulse::getEndingRampDuration() const
{

  try
  {

    return _endingRampDuration;

  }
  GKG_CATCH( "float "
             "gkg::TrapezoidGradientPulse::getEndingRampDuration() const" );

}


float gkg::TrapezoidGradientPulse::getMagnitude() const
{

  try
  {

    return _magnitude;

  }
  GKG_CATCH( "float gkg::TrapezoidGradientPulse::getMagnitude() const" );

}


float gkg::TrapezoidGradientPulse::getValueAt( float time,
                                               float /*scaling*/ ) const
{

  try
  {

    float value = 0.0;

    if ( ( time > _breakPoints[ 0 ] ) &&
         ( time <= _breakPoints[ 1 ] ) )
    {

      value = ( time - _breakPoints[ 0 ] ) *
              ( _magnitude / _startingRampDuration );

    }
    else if ( ( time > _breakPoints[ 1 ] ) &&
              ( time <= _breakPoints[ 2 ] ) )
    {

      value = _magnitude;

    }
    else if ( ( time > _breakPoints[ 2 ] ) &&
              ( time <= _breakPoints[ 3 ] ) )
    {

      value = _magnitude - ( time - _breakPoints[ 2 ] ) *
                           ( _magnitude / _endingRampDuration );

    }

    return value;

  }
  GKG_CATCH( "float gkg::TrapezoidGradientPulse::getValueAt( "
             "float time, "
             "float /*scaling*/ ) const" );

}


float gkg::TrapezoidGradientPulse::getSquareValueAt( float time,
                                                     float /*scaling*/ ) const
{

  try
  {

    float value = 0.0;

    if ( ( time > _breakPoints[ 0 ] ) &&
         ( time <= _breakPoints[ 1 ] ) )
    {

      value = ( time - _breakPoints[ 0 ] ) *
              ( _magnitude / _startingRampDuration );

    }
    else if ( ( time > _breakPoints[ 1 ] ) &&
              ( time <= _breakPoints[ 2 ] ) )
    {

      value = _magnitude;

    }
    else if ( ( time > _breakPoints[ 2 ] ) &&
              ( time <= _breakPoints[ 3 ] ) )
    {

      value = _magnitude - ( time - _breakPoints[ 2 ] ) *
                           ( _magnitude / _endingRampDuration );

    }

    return value * value;

  }
  GKG_CATCH( "float gkg::TrapezoidGradientPulse::getSquareValueAt( "
             "float time, "
             "float /*scaling*/ ) const" );

}


float gkg::TrapezoidGradientPulse::getIntegralValueAt( float time,
                                                       float /*scaling*/ ) const
{

  try
  {

    float value = 0.0;

    if ( ( time > _breakPoints[ 0 ] ) &&
         ( time <= _breakPoints[ 1 ] ) )
    {

      value = ( time - _breakPoints[ 0 ] ) *
              ( time - _breakPoints[ 0 ] ) *
              ( _magnitude / _startingRampDuration ) / 2;

    }
    else if ( ( time > _breakPoints[ 1 ] ) &&
              ( time <= _breakPoints[ 2 ] ) )
    {

      value = _magnitude * ( _startingRampDuration / 2 +
                             ( time - _breakPoints[ 1 ] ) );

    }
    else if ( ( time > _breakPoints[ 2 ] ) &&
              ( time <= _breakPoints[ 3 ] ) )
    {

      float r = time - _breakPoints[ 2 ];

      value = _magnitude * ( _startingRampDuration / 2 +
                             _plateauDuration +
                             ( r * ( 1 - r / _endingRampDuration / 2 ) ) );

    }
    else if ( time > _breakPoints[3] )
    {

      value = _magnitude * ( _startingRampDuration +
                             _plateauDuration * 2 +
                             _endingRampDuration ) / 2;

    }

    return value;

  }
  GKG_CATCH( "float gkg::TrapezoidGradientPulse::getIntegralValueAt( "
             "float time, "
             "float /*scaling*/ ) const" );

}


float gkg::TrapezoidGradientPulse::getIntegralSquareValueAt(
                                                       float time,
                                                       float /*scaling*/ ) const
{

  try
  {

    float value = 0.0;

    if ( ( time > _breakPoints[ 0 ] ) &&
         ( time <= _breakPoints[ 1 ] ) )
    {

      float r = time - _breakPoints[ 0 ];

      value = ( _magnitude / _startingRampDuration ) *
              ( _magnitude / _startingRampDuration ) *
              ( r * r * r ) / 3;

    }
    else if ( ( time > _breakPoints[ 1 ] ) &&
              ( time <= _breakPoints[ 2 ] ) )
    {

      value = _magnitude * _magnitude *
              ( _startingRampDuration / 3 + ( time - _breakPoints[ 1 ] ) );

    }
    else if ( ( time > _breakPoints[ 2 ] ) &&
              ( time <= _breakPoints[ 3 ] ) )
    {

      float r = time - _breakPoints[ 2 ];

      value = _magnitude * _magnitude *
              ( _startingRampDuration / 3 +
                _plateauDuration +
                r * ( ( r / _endingRampDuration ) *
                      ( r / _endingRampDuration ) / 3 -
                      ( r / _endingRampDuration ) + 1 ) );

    }
    else if ( time > _breakPoints[3] )
    {

      value = _magnitude * _magnitude *
              ( _startingRampDuration / 3 +
                _plateauDuration +
                _endingRampDuration / 3 );

    }

    return value;

  }
  GKG_CATCH( "float gkg::TrapezoidGradientPulse::getIntegralSquareValueAt( "
             "float time, "
             "float /*scaling*/ ) const" );

}

