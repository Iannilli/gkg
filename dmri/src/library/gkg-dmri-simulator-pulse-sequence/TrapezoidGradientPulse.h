#ifndef _gkg_dmri_simulator_pulse_sequence_TrapezoidGradientPulse_h_
#define _gkg_dmri_simulator_pulse_sequence_TrapezoidGradientPulse_h_


#include <gkg-dmri-simulator-pulse-sequence/GradientPulse.h>


namespace gkg
{


// gradient magnitudes are provided in mT/m


class TrapezoidGradientPulse : public GradientPulse
{

  public:

    TrapezoidGradientPulse( int32_t axis,
                            float startingTime,
                            float startingRampDuration,
                            float plateauDuration,
                            float endingRampDuration,
                            float magnitude );
    virtual ~TrapezoidGradientPulse();

    float getStartingRampDuration() const;
    float getPlateauDuration() const;
    float getEndingRampDuration() const;
    float getMagnitude() const;

    virtual float getValueAt( float time,
                              float /*scaling*/ ) const;
    virtual float getSquareValueAt( float time,
                                    float /*scaling*/ ) const;
    virtual float getIntegralValueAt( float time,
                                      float /*scaling*/ ) const;
    virtual float getIntegralSquareValueAt( float time,
                                            float /*scaling*/ ) const;

  protected:

    float _startingRampDuration;
    float _plateauDuration;
    float _endingRampDuration;
    float _magnitude;

};


}


#endif

