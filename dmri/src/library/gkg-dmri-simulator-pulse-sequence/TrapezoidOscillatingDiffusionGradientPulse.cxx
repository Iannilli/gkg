#include <gkg-dmri-simulator-pulse-sequence/TrapezoidOscillatingDiffusionGradientPulse.h>
#include <gkg-core-exception/Exception.h>


gkg::TrapezoidOscillatingDiffusionGradientPulse::
                                     TrapezoidOscillatingDiffusionGradientPulse(
                                                  int32_t axis,
                                                  float startingTime,
                                                  float period,
                                                  int32_t lobeCount,
                                                  float magnitude,
                                                  float maximumSlewRate,
                                                  float gradientTimeResolution )
                                 : gkg::TrapezoidOscillatingGradientPulse(
                                                        axis,
                                                        startingTime,
                                                        period,
                                                        lobeCount,
                                                        magnitude,
                                                        maximumSlewRate,
                                                        gradientTimeResolution )
{
}


gkg::TrapezoidOscillatingDiffusionGradientPulse::
                                   ~TrapezoidOscillatingDiffusionGradientPulse()
{
}


float gkg::TrapezoidOscillatingDiffusionGradientPulse::getValueAt(
                                                           float time,
                                                           float scaling ) const
{

  try
  {

    return this->gkg::TrapezoidOscillatingGradientPulse::getValueAt(
                                                               time, scaling ) *
           scaling;

  }
  GKG_CATCH( "float gkg::TrapezoidOscillatingDiffusionGradientPulse::"
             "getValueAt( "
             "float time, "
             "float scaling ) const" );

}


float gkg::TrapezoidOscillatingDiffusionGradientPulse::getSquareValueAt(
                                                           float time,
                                                           float scaling ) const
{

  try
  {

    return this->gkg::TrapezoidOscillatingGradientPulse::getSquareValueAt( 
                                                               time, scaling ) *
           scaling * scaling;

  }
  GKG_CATCH( "float gkg::TrapezoidOscillatingDiffusionGradientPulse::"
             "getSquareValueAt( "
             "float time, "
             "float scaling ) const" );

}


float gkg::TrapezoidOscillatingDiffusionGradientPulse::getIntegralValueAt( 
                                                           float time,
                                                           float scaling ) const
{

  try
  {

    return this->gkg::TrapezoidOscillatingGradientPulse::getIntegralValueAt(
                                                                 time,
                                                                 scaling ) *
           scaling;

  }
  GKG_CATCH( "float gkg::TrapezoidOscillatingDiffusionGradientPulse::"
             "getIntegralValueAt( "
             "float time, "
             "float scaling ) const" );

}


float gkg::TrapezoidOscillatingDiffusionGradientPulse::getIntegralSquareValueAt(
                                                           float time,
                                                           float scaling ) const
{

  try
  {

    return this->gkg::TrapezoidOscillatingGradientPulse::
                                                       getIntegralSquareValueAt(
                                                                  time,
                                                                  scaling ) *
           scaling * scaling;

  }
  GKG_CATCH( "float gkg::TrapezoidOscillatingDiffusionGradientPulse::"
             "getIntegralSquareValueAt( "
             "float time, "
             "float scaling ) const" );

}

