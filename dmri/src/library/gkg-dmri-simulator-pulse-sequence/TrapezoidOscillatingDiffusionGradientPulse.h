#ifndef _gkg_dmri_simulator_pulse_sequence_TrapezoidOscillatingDiffusionGradientPulse_h_
#define _gkg_dmri_simulator_pulse_sequence_TrapezoidOscillatingDiffusionGradientPulse_h_


#include <gkg-dmri-simulator-pulse-sequence/TrapezoidOscillatingGradientPulse.h>


namespace gkg
{


// gradient magnitudes are provided in mT/m


class TrapezoidOscillatingDiffusionGradientPulse : public TrapezoidOscillatingGradientPulse
{

  public:

    TrapezoidOscillatingDiffusionGradientPulse( int32_t axis,
                                    float startingTime,
                                    float period,
                                    int32_t lobeCount,
                                    float magnitude,
                                    float maximumSlewRate,
                                    float gradientTimeResolution );
    virtual ~TrapezoidOscillatingDiffusionGradientPulse();

    virtual float getValueAt( float time,
                              float scaling ) const;
    virtual float getSquareValueAt( float time,
                                    float scaling ) const;
    virtual float getIntegralValueAt( float time,
                                      float scaling ) const;
    virtual float getIntegralSquareValueAt( float time,
                                            float scaling ) const;

};


}


#endif

