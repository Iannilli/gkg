#include <gkg-dmri-simulator-pulse-sequence/TrapezoidOscillatingGradientPulse.h>
#include <gkg-dmri-simulator-pulse-sequence/NMRDefines.h>
#include <gkg-core-exception/Exception.h>
#include <cmath>

#include <iostream>
#include <fstream>
#include <set>
#include <math.h>
#include <stdlib.h>


gkg::TrapezoidOscillatingGradientPulse::TrapezoidOscillatingGradientPulse(
                                                  int32_t axis,
                                                  float startingTime,
                                                  float period,
                                                  int32_t lobeCount,
                                                  float magnitude,
                                                  float maximumSlewRate,
                                                  float gradientTimeResolution )
                           : gkg::GradientPulse( axis ),
                             _period( period ),
                             _lobeCount( lobeCount ),
                             _magnitude( magnitude ),
                             _angularFrequency( 2.0 * M_PI / period ),
                             _maximumSlewRate( maximumSlewRate ),
                             _gradientTimeResolution( gradientTimeResolution ),
                             _startingTime( startingTime ),
                             _periodicTrapezoidGradientPulse( 
                                       axis,
                                       0.0f, 
                                       magnitude / maximumSlewRate,
                                       period / 2.0f - 
                                                2.0f * ( magnitude /
                                                         maximumSlewRate ),
                                       magnitude / maximumSlewRate,
                                       magnitude )
{

  try
  {

    _pulseType = GRADIENT_PULSE_TYPE;

    float time = startingTime;

    float riseTime = ( float )( magnitude / maximumSlewRate );
    float plateauDuration = ( float )( _period / 2.0f - 2.0f * riseTime );

    addBreakPoint( time );
    time += ( _period / 2.0f * ( float )_lobeCount + 
              4.0 * riseTime + plateauDuration );
    addBreakPoint( time );

  }
  GKG_CATCH( "gkg::TrapezoidOscillatingGradientPulse::"
             "TrapezoidOscillatingGradientPulse( "
             "int32_t axis, "
             "float startingTime, "
             "float period, "
             "int32_t lobeCount, "
             "float magnitude )" );

}


gkg::TrapezoidOscillatingGradientPulse::~TrapezoidOscillatingGradientPulse()
{
}


float gkg::TrapezoidOscillatingGradientPulse::getPeriod() const
{

  try
  {

    return _period;

  }
  GKG_CATCH( "float gkg::TrapezoidOscillatingGradientPulse::"
             "getPeriod() const" );

}


int32_t gkg::TrapezoidOscillatingGradientPulse::getLobeCount() const
{

  try
  {

    return _lobeCount;

  }
  GKG_CATCH( "int32_t gkg::TrapezoidOscillatingGradientPulse::"
             "getLobeCount() const" );

}


float gkg::TrapezoidOscillatingGradientPulse::getMagnitude() const
{

  try
  {

    return _magnitude;

  }
  GKG_CATCH( "float gkg::TrapezoidOscillatingGradientPulse::"
             "getAmplitdue() const" );

}


float gkg::TrapezoidOscillatingGradientPulse::getAngularFrequency() const
{

  try
  {

    return _angularFrequency;

  }
  GKG_CATCH( "float gkg::TrapezoidOscillatingGradientPulse::"
             "getAngularFrequency() const" );

}


float gkg::TrapezoidOscillatingGradientPulse::getMaximumSlewRate() const
{

  try
  {

    return _maximumSlewRate;

  }
  GKG_CATCH( "float gkg::TrapezoidOscillatingGradientPulse::"
             "getMaximumSlewRate() const" );

}


float gkg::TrapezoidOscillatingGradientPulse::getGradientTimeResolution() const
{

  try
  {

    return _gradientTimeResolution;

  }
  GKG_CATCH( "float gkg::TrapezoidOscillatingGradientPulse::"
             "getGradientTimeResolution() const" );

}


float gkg::TrapezoidOscillatingGradientPulse::getValueAt(
                                                       float time,
                                                       float /*scaling*/ ) const
{

  try
  {	

    float riseTime = ( float )( _magnitude / _maximumSlewRate );
    float plateauDuration = ( float )( _period / 2.0f - 2.0f * riseTime );

    float timeMinusStartingTime = time - _startingTime;

    float breakPointFull1 = 0.0;
    float breakPointFull2 = breakPointFull1 + riseTime;
    float breakPointFull3 = breakPointFull2 + plateauDuration;
    float breakPointFull4 = breakPointFull3 + riseTime;

    float breakPointHalf1 = 0.0;
    float breakPointHalf2 = breakPointHalf1 + riseTime;
    float breakPointHalf3 = breakPointHalf2 + plateauDuration/2.0;
    float breakPointHalf4 = breakPointHalf3 + riseTime;

    float value = 0.0f;
    float translatedTime = timeMinusStartingTime;
    if ( timeMinusStartingTime < ( 2.0 * riseTime + plateauDuration / 2.0 ) )
    {

      value = this->getTrapezoidValueAt( translatedTime,
                                         _magnitude,
                                         riseTime,
                                         riseTime,
                                         breakPointHalf1,
                                         breakPointHalf2,
                                         breakPointHalf3,
                                         breakPointHalf4);

    }
    else if ( ( timeMinusStartingTime >
                ( 2.0 * riseTime + plateauDuration / 2.0 ) ) &&
              ( ( ( timeMinusStartingTime - 2.0 * riseTime - 
                    plateauDuration / 2.0 ) / ( _period / 2.0f ) ) <=
                _lobeCount ) )
    {

      translatedTime = std::fmod( ( timeMinusStartingTime - 
                                    2.0 * riseTime - plateauDuration / 2.0 ),
                                  _period / 2.0 );

      if ( std::fmod( ( timeMinusStartingTime -
                        2.0 * riseTime - plateauDuration / 2.0 ),
                      _period ) > _period / 2.0 )
      {

        value = this->getTrapezoidValueAt( translatedTime,
                                           _magnitude,
                                           riseTime,
                                           riseTime,
                                           breakPointFull1,
                                           breakPointFull2,
                                           breakPointFull3,
                                           breakPointFull4 );

      }
      else
      {

        value = -this->getTrapezoidValueAt( translatedTime,
                                            _magnitude,
                                            riseTime,
                                            riseTime,
                                            breakPointFull1,
                                            breakPointFull2,
                                            breakPointFull3,
                                            breakPointFull4 );

      }

    }
    else if ( ( ( timeMinusStartingTime - 2.0 * riseTime -
                plateauDuration / 2.0 ) / ( _period / 2.0 ) ) > _lobeCount )
    {

      translatedTime = timeMinusStartingTime - 2.0 * riseTime -
                       plateauDuration / 2.0 - _period / 2.0 * _lobeCount;

      value = this->getTrapezoidValueAt( translatedTime,
                                         _magnitude,
                                         riseTime,
                                         riseTime,
                                         breakPointHalf1,
                                         breakPointHalf2,
                                         breakPointHalf3,
                                         breakPointHalf4 );

    }

    return value;

  }
  GKG_CATCH( "float gkg::TrapezoidOscillatingGradientPulse::getValueAt( "
             "float time, "
             "float /*scaling*/ ) const" );

}


float gkg::TrapezoidOscillatingGradientPulse::getSquareValueAt( float time,
                                                    float /*scaling*/ ) const
{

  try
  {

    float value = 0.0f;

    if ( 2.0f * ( ( time - _startingTime ) / _period ) <= _lobeCount )
    {

      float t = std::fmod( time - _startingTime, _period / 2.0f );

      value = _periodicTrapezoidGradientPulse.getSquareValueAt( t, 1.0f );

    }

    return value;
   
  }
  GKG_CATCH( "float gkg::TrapezoidOscillatingGradientPulse::getSquareValueAt( "
             "float time, "
             "float /*scaling*/ ) const" );

}


float gkg::TrapezoidOscillatingGradientPulse::getIntegralValueAt(
                                                       float time,
                                                       float /*scaling*/ ) const
{

  try
  {

    float value = 0.0f;

    if ( 2.0f * ( ( time - _startingTime ) / _period ) <= _lobeCount )
    {

      float t = std::fmod( time - _startingTime, _period / 2.0f );

      value = _periodicTrapezoidGradientPulse.getIntegralValueAt( t, 1.0f );

      if ( std::fmod( time - _startingTime, _period ) > _period / 2.0f )
      {

        value = _periodicTrapezoidGradientPulse.getIntegralValueAt( 
                                                 _period / 2.0f, 1.0f ) - value;

      }

    }

    return value;

  }
  GKG_CATCH( "float gkg::TrapezoidOscillatingGradientPulse::"
             "getIntegralValueAt( "
             "float time, "
             "float /*scaling*/ ) const" );

}


float gkg::TrapezoidOscillatingGradientPulse::getIntegralSquareValueAt(
                                                       float time,
                                                       float /*scaling*/ ) const
{

  try
  {
  
    float value = 0.0f;

    int32_t lobeCount = ( int32_t )( 2.0f * ( ( time - _startingTime ) /
                                              _period ) );
    float integralSquareOfOnePulse = _periodicTrapezoidGradientPulse.
                                                 getIntegralSquareValueAt( 
                                                         _period / 2.0f, 1.0f );

    if ( lobeCount <= _lobeCount )
    {

      float t = std::fmod( time - _startingTime, _period / 2.0f );

      value = _periodicTrapezoidGradientPulse.getIntegralSquareValueAt( t,
                                                                        1.0f ) +
              lobeCount * integralSquareOfOnePulse;

    }

    return value;

  }
  GKG_CATCH( "float gkg::TrapezoidOscillatingGradientPulse::"
             "getIntegralSquareValueAt( "
             "float time, "
             "float /*scaling*/ ) const" );

}



float gkg::TrapezoidOscillatingGradientPulse::getTrapezoidValueAt(
                                                     float time,
                                                     float magnitude,
                                                     float startingRampDuration,
                                                     float endingRampDuration,
                                                     float breakPoint1,
                                                     float breakPoint2,
                                                     float breakPoint3,
                                                     float breakPoint4 ) const
{

  try
  {

    float value = 0.0;

    if ( ( time > breakPoint1 ) && ( time <= breakPoint2 ) )
    {

      value = ( time - breakPoint1 ) * ( magnitude / startingRampDuration );

    }
    else if ( ( time > breakPoint2 ) && ( time <= breakPoint3 ) )
    {

      value = magnitude;

    }
    else if ( ( time > breakPoint3 ) && ( time <= breakPoint4 ) )
    {

      value = magnitude - ( time - breakPoint3 ) *
                          ( magnitude / endingRampDuration );

    }

    return value;

  }
  GKG_CATCH( "float "
             "gkg::TrapezoidOscillatingGradientPulse::getTrapezoidValueAt( "
             "float time, "
             "float magnitude, "
             "float startingRampDuration, "
             "float endingRampDuration, "
             "float breakPoint1, "
             "float breakPoint2, "
             "float breakPoint3, "
             "float breakPoint4 )" );

}
