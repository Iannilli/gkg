#ifndef _gkg_dmri_simulator_pulse_sequence_TrapezoidOscillatingGradientPulse_h_
#define _gkg_dmri_simulator_pulse_sequence_TrapezoidOscillatingGradientPulse_h_


#include <gkg-dmri-simulator-pulse-sequence/GradientPulse.h>
#include <gkg-dmri-simulator-pulse-sequence/TrapezoidGradientPulse.h>


namespace gkg
{


class TrapezoidOscillatingGradientPulse : public GradientPulse
{

  public:

    TrapezoidOscillatingGradientPulse( int32_t axis,
                                       float startingTime,
                                       float period,
                                       int32_t lobeCount,
                                       float magnitude,
                                       float maximumSlewRate,
                                       float gradientTimeResolution );
    virtual ~TrapezoidOscillatingGradientPulse();

    float getPeriod() const;
    int32_t getLobeCount() const;
    float getMagnitude() const;
    float getAngularFrequency() const;
    float getMaximumSlewRate() const;
    float getGradientTimeResolution() const;

    virtual float getValueAt( float time,
                              float /*scaling*/ ) const;
    virtual float getSquareValueAt( float time,
                                    float /*scaling*/ ) const;
    virtual float getIntegralValueAt( float time,
                                      float /*scaling*/ ) const;
    virtual float getIntegralSquareValueAt( float time,
                                            float /*scaling*/ ) const;

  protected:

    float getTrapezoidValueAt( float time,
                               float magnitude,
                               float startingRampDuration,
                               float endingRampDuration,
                               float breakPoint1,
                               float breakPoint2,
                               float breakPoint3,
                               float breakPoint4 ) const;
    float _period;
    int32_t _lobeCount;
    float _magnitude;
    float _angularFrequency;
    float _maximumSlewRate;
    float _gradientTimeResolution;
    float _startingTime;
    TrapezoidGradientPulse _periodicTrapezoidGradientPulse;

};


}


#endif

