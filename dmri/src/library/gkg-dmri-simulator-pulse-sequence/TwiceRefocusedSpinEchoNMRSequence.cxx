#include <gkg-dmri-simulator-pulse-sequence/TwiceRefocusedSpinEchoNMRSequence.h>
#include <gkg-dmri-simulator-pulse-sequence/RadioFrequencyPulse.h>
#include <gkg-dmri-simulator-pulse-sequence/DiffusionGradientPulse.h>
#include <gkg-core-exception/Exception.h>
#include <cmath>


#define TWOPI_GAM 26748.0
#define GAM 26748.0/ (2*M_PI)

#define MT_PER_M_TO_G_PER_CM              0.1
#define T_PER_M_PER_S_TO_G_PER_CM_PER_US  0.0001
#define MS_TO_US                          1000.0

#define RUP_GRD( A )  ( ( ( int32_t )gradientTimeResolution -       \
                          ( ( int32_t )( A ) %                      \
                            ( int32_t )gradientTimeResolution ) ) % \
                          ( int32_t )gradientTimeResolution +       \
                        ( int32_t )( A ) )


gkg::TwiceRefocusedSpinEchoNMRSequence::TwiceRefocusedSpinEchoNMRSequence(
                       const std::set< float >& gradientAmplitudes,
                       const std::vector< float >& scalarParameters,
                       const std::vector< std::string >& /* stringParameters */,
                       bool verbose )
                                       : gkg::NMRSequence( gradientAmplitudes,
                                                           verbose )
{

  try
  {

    // collecting sequence scalar parameters
    float maximumSlewRate = scalarParameters[ 0 ];
    float gradientTimeResolution = scalarParameters[ 1 ];
    float littleDeltaInMs = scalarParameters[ 2 ];
    float bigDeltaInMs = scalarParameters[ 3 ];

    //
    // Sanity check
    //
    if ( bigDeltaInMs < ( littleDeltaInMs + 6.0 ) )
    {

      // assume that the refocusing RF pulse duration is 6 ms
      throw std::runtime_error( "Invalid delta/DELTA combination" );

    }

    //
    // processing the diffusion gradient parameters
    //
    float maximumGradientAmplitude = this->_gradientAmplitudes.back();
    float maximumGradientAmplitudeInGaussPerCm = maximumGradientAmplitude *
                                                 MT_PER_M_TO_G_PER_CM;
    float maximumSlewRateInGaussPerCmPerUs = maximumSlewRate *
                                             T_PER_M_PER_S_TO_G_PER_CM_PER_US;

    float littleDeltaInUs = littleDeltaInMs * MS_TO_US;
    float bigDeltaInUs = bigDeltaInMs * MS_TO_US;
    float timeSeparationInUs = bigDeltaInUs - littleDeltaInUs;
    float rampWidthInUs = 0.0;
    float plateauWidthInUs = getDiffusionPulsePlateauWidthInUs(
                                           maximumGradientAmplitudeInGaussPerCm,
                                           maximumSlewRateInGaussPerCmPerUs,
                                           littleDeltaInUs,
                                           gradientTimeResolution,
                                           rampWidthInUs );

    _effectiveDiffusionTimeInMs = bigDeltaInMs - littleDeltaInMs / 3.0;
    _maximumQInMeterInverse = GAM * 1e-2 * littleDeltaInMs *
                              maximumGradientAmplitude;

    //
    // creating and adding pulses to the NMR sequence
    //

    ///////////// creating RF + Gslice + Gphase + Gread pulse sequences ////////
    gkg::RCPointer< gkg::PulseSequence >
      rfMagnitudePulseSequence( new gkg::PulseSequence( RF_MAGNITUDE_AXIS ) );

    gkg::RCPointer< gkg::PulseSequence >
      rfPhasePulseSequence( new gkg::PulseSequence( RF_PHASE_AXIS ) );

    gkg::RCPointer< gkg::PulseSequence >
      xGradientPulseSequence( new gkg::PulseSequence( GRADIENT_X_AXIS ) );

    gkg::RCPointer< gkg::PulseSequence >
      yGradientPulseSequence( new gkg::PulseSequence( GRADIENT_Y_AXIS ) );

    gkg::RCPointer< gkg::PulseSequence >
      zGradientPulseSequence( new gkg::PulseSequence( GRADIENT_Z_AXIS ) );

    /////////////////////////// focusing on RF pulses //////////////////////////

    // adding excitation pulse; we consider the excitation pulse and slice
    // selecton gradient will last 3ms + 0.75ms for its rephasing gradient
    float excitationStartingTime = -1.5 * MS_TO_US;
    float excitationCentreTime = 0.0 * MS_TO_US;
    float excitationEndingTime = 1.5 * MS_TO_US;

    gkg::RCPointer< gkg::Pulse >
      excitationRFPulse( new gkg::RadioFrequencyPulse(
                                           RF_MAGNITUDE_AXIS,
                                           gkg::RadioFrequencyPulse::Excitation,
                                           excitationStartingTime,
                                           excitationCentreTime,
                          	           excitationEndingTime ) );

    // adding refocusing pulse, 2nd refocusing pulse ( duration = 6.0 ms ); 
    float sliceSelectionRephasingTime = 0.75 * MS_TO_US;
    float timeDelayOfRefocusingRFInUs = ( timeSeparationInUs -
                                          6.0 * MS_TO_US ) / 2;
    float refocusingStartingTime = excitationEndingTime + 
                                   sliceSelectionRephasingTime +
                                   littleDeltaInUs +
                                   timeDelayOfRefocusingRFInUs;
    float refocusingCentreTime = refocusingStartingTime + 3.0 * MS_TO_US;
    float refocusingEndingTime = refocusingCentreTime + 3.0 * MS_TO_US;

    // Second Refocusing
    float secondRefocusingStartingTime = refocusingStartingTime +
                                         2.0 * refocusingCentreTime;
    float secondRefocusingCentreTime = secondRefocusingStartingTime +
                                       3.0 * MS_TO_US; 
    float secondRefocusingEndingTime = secondRefocusingCentreTime +
                                       3.0 * MS_TO_US;

    gkg::RCPointer< gkg::Pulse >
      refocusingRFPulse( new gkg::RadioFrequencyPulse(
                                           RF_MAGNITUDE_AXIS,
                                           gkg::RadioFrequencyPulse::Refocusing,
                                           refocusingStartingTime,
                                           refocusingCentreTime,
                                           refocusingEndingTime ) );
    gkg::RCPointer< gkg::Pulse >
      secondRefocusingRFPulse( new gkg::RadioFrequencyPulse(
                                           RF_MAGNITUDE_AXIS,
                                           gkg::RadioFrequencyPulse::Refocusing,
                                           secondRefocusingStartingTime,
                                           secondRefocusingCentreTime,
                                           secondRefocusingEndingTime ) );

    // addingRF pulses to the RF pulse sequences
    rfMagnitudePulseSequence->addPulse( excitationRFPulse );
    rfMagnitudePulseSequence->addPulse( refocusingRFPulse );
    rfMagnitudePulseSequence->addPulse( secondRefocusingRFPulse );

    /////////////////////// focusing on gradient pulses ////////////////////////

    float dwPulseStartingRampDuration = rampWidthInUs;
    float dwPulsePlateauDuration = plateauWidthInUs;
    float dwPulseEndingRampDuration = rampWidthInUs;
    float dwPulseMagnitude = maximumGradientAmplitude;

    // left diffusion-sensitization
    float dwLeftPulseStartingTime = excitationEndingTime + 
                                    sliceSelectionRephasingTime;
    gkg::RCPointer< gkg::Pulse >
      dwLabelingPulseOnXAxis( new gkg::DiffusionGradientPulse(
                                         GRADIENT_X_AXIS,
                                         dwLeftPulseStartingTime,
                                         dwPulseStartingRampDuration,
                                         dwPulsePlateauDuration,
                                         dwPulseEndingRampDuration,
                                         dwPulseMagnitude ) );
    gkg::RCPointer< gkg::Pulse >
      dwLabelingPulseOnYAxis( new gkg::DiffusionGradientPulse(
                                         GRADIENT_Y_AXIS,
                                         dwLeftPulseStartingTime,
                                         dwPulseStartingRampDuration,
                                         dwPulsePlateauDuration,
                                         dwPulseEndingRampDuration,
                                         dwPulseMagnitude ) );
    gkg::RCPointer< gkg::Pulse >
      dwLabelingPulseOnZAxis( new gkg::DiffusionGradientPulse(
                                         GRADIENT_Z_AXIS,
                                         dwLeftPulseStartingTime,
                                         dwPulseStartingRampDuration,
                                         dwPulsePlateauDuration,
                                         dwPulseEndingRampDuration,
                                         dwPulseMagnitude ) );
    // 2nd
    float secondDwLeftPulseStartingTime = dwLeftPulseStartingTime +
                                          2.0 * refocusingCentreTime; 
    gkg::RCPointer< gkg::Pulse >
      secondDwLabelingPulseOnXAxis( new gkg::DiffusionGradientPulse(
                                         GRADIENT_X_AXIS,
                                         secondDwLeftPulseStartingTime,
                                         dwPulseStartingRampDuration,
                                         dwPulsePlateauDuration,
                                         dwPulseEndingRampDuration,
                                         dwPulseMagnitude ) );
    gkg::RCPointer< gkg::Pulse >
      secondDwLabelingPulseOnYAxis( new gkg::DiffusionGradientPulse(
                                         GRADIENT_Y_AXIS,
                                         secondDwLeftPulseStartingTime,
                                         dwPulseStartingRampDuration,
                                         dwPulsePlateauDuration,
                                         dwPulseEndingRampDuration,
                                         dwPulseMagnitude ) );
    gkg::RCPointer< gkg::Pulse >
      secondDwLabelingPulseOnZAxis( new gkg::DiffusionGradientPulse(
                                         GRADIENT_Z_AXIS,
                                         secondDwLeftPulseStartingTime,
                                         dwPulseStartingRampDuration,
                                         dwPulsePlateauDuration,
                                         dwPulseEndingRampDuration,
                                         dwPulseMagnitude ) );

    // right diffusion-sensitization
    float dwRightPulseStartingTime = dwLeftPulseStartingTime + bigDeltaInUs;
    gkg::RCPointer< gkg::Pulse >
      dwDelabelingPulseOnXAxis( new gkg::DiffusionGradientPulse(
                                           GRADIENT_X_AXIS,
                                           dwRightPulseStartingTime,
                                           dwPulseStartingRampDuration,
                                           dwPulsePlateauDuration,
                                           dwPulseEndingRampDuration,
                                           dwPulseMagnitude ) );
    gkg::RCPointer< gkg::Pulse >
      dwDelabelingPulseOnYAxis( new gkg::DiffusionGradientPulse(
                                           GRADIENT_Y_AXIS,
                                           dwRightPulseStartingTime,
                                           dwPulseStartingRampDuration,
                                           dwPulsePlateauDuration,
                                           dwPulseEndingRampDuration,
                                           dwPulseMagnitude ) );
    gkg::RCPointer< gkg::Pulse >
      dwDelabelingPulseOnZAxis( new gkg::DiffusionGradientPulse(
                                           GRADIENT_Z_AXIS,
                                           dwRightPulseStartingTime,
                                           dwPulseStartingRampDuration,
                                           dwPulsePlateauDuration,
                                           dwPulseEndingRampDuration,
                                           dwPulseMagnitude ) );
    // 2nd
    float secondDwRightPulseStartingTime = secondDwLeftPulseStartingTime +
                                           bigDeltaInUs; 
    gkg::RCPointer< gkg::Pulse >
      secondDwDelabelingPulseOnXAxis( new gkg::DiffusionGradientPulse(
                                           GRADIENT_X_AXIS,
                                           secondDwRightPulseStartingTime,
                                           dwPulseStartingRampDuration,
                                           dwPulsePlateauDuration,
                                           dwPulseEndingRampDuration,
                                           dwPulseMagnitude ) );
    gkg::RCPointer< gkg::Pulse >
     secondDwDelabelingPulseOnYAxis( new gkg::DiffusionGradientPulse(
                                           GRADIENT_Y_AXIS,
                                           secondDwRightPulseStartingTime,
                                           dwPulseStartingRampDuration,
                                           dwPulsePlateauDuration,
                                           dwPulseEndingRampDuration,
                                           dwPulseMagnitude ) );
    gkg::RCPointer< gkg::Pulse >
      secondDwDelabelingPulseOnZAxis( new gkg::DiffusionGradientPulse(
                                           GRADIENT_Z_AXIS,
                                           secondDwRightPulseStartingTime,
                                           dwPulseStartingRampDuration,
                                           dwPulsePlateauDuration,
                                           dwPulseEndingRampDuration,
                                           dwPulseMagnitude ) );

    // adding the first pair of DW gradient pulses to the pulse sequences
    xGradientPulseSequence->addPulse( dwLabelingPulseOnXAxis );
    yGradientPulseSequence->addPulse( dwLabelingPulseOnYAxis );
    zGradientPulseSequence->addPulse( dwLabelingPulseOnZAxis );
    xGradientPulseSequence->addPulse( dwDelabelingPulseOnXAxis );
    yGradientPulseSequence->addPulse( dwDelabelingPulseOnYAxis );
    zGradientPulseSequence->addPulse( dwDelabelingPulseOnZAxis );

    // adding the second pair of DW gradient pulses to the pulse sequences
    xGradientPulseSequence->addPulse( secondDwLabelingPulseOnXAxis );
    yGradientPulseSequence->addPulse( secondDwLabelingPulseOnYAxis );
    zGradientPulseSequence->addPulse( secondDwLabelingPulseOnZAxis );
    xGradientPulseSequence->addPulse( secondDwDelabelingPulseOnXAxis );
    yGradientPulseSequence->addPulse( secondDwDelabelingPulseOnYAxis );
    zGradientPulseSequence->addPulse( secondDwDelabelingPulseOnZAxis );


    ///////////////////////// adding pulse sequences ///////////////////////////
    this->addPulseSequence( rfMagnitudePulseSequence );
    this->addPulseSequence( rfPhasePulseSequence );
    this->addPulseSequence( xGradientPulseSequence );
    this->addPulseSequence( yGradientPulseSequence );
    this->addPulseSequence( zGradientPulseSequence );

    // finally setting echo time
    this->setEchoTimeInUs( refocusingCentreTime * 4.0 );

  }
  GKG_CATCH( "gkg::TwiceRefocusedSpinEchoNMRSequence:: "
             "TwiceRefocusedSpinEchoNMRSequence( "
             "const std::set< float >& gradientAmplitudes, "
             "const std::vector< float >& scalarParameters, "
             "const std::vector< std::string >& stringParameters, "
             "bool verbose )" );

}


gkg::TwiceRefocusedSpinEchoNMRSequence::~TwiceRefocusedSpinEchoNMRSequence()
{
}


void gkg::TwiceRefocusedSpinEchoNMRSequence::checkOrInitializeDefaultParameters(
                                  std::vector< float >& scalarParameters,
                                  std::vector< std::string >& stringParameters )
{

  try
  {

    if ( scalarParameters.size() != 4U )
    {

      throw std::runtime_error( "invalid scalar parameter count" );

    }
    if ( !stringParameters.empty() )
    {

      throw std::runtime_error( "invalid string parameter count" );

    }

  }
  GKG_CATCH( "void gkg::TwiceRefocusedSpinEchoNMRSequence:: "
             "checkOrInitializeDefaultParameters( "
             "std::vector< float >& scalarParameters, "
             "std::vector< std::string >& stringParameters )" );

}


float gkg::TwiceRefocusedSpinEchoNMRSequence::getDiffusionPulsePlateauWidthInUs(
                                      float maximuGradientAmplitudeInGaussPerCm,
                                      float maximumSlewRateInGaussPerCmPerUs,
                                      float littleDeltaInUs,
                                      float gradientTimeResolution,
                                      float& rampWidthInUs )
{

  try
  {

    float plateauWidthInUs = 0.0;

    // processing the ramp width
    rampWidthInUs = ( float )RUP_GRD( ( int32_t )
                                      ( maximuGradientAmplitudeInGaussPerCm /
                                        maximumSlewRateInGaussPerCmPerUs ) );

    plateauWidthInUs = littleDeltaInUs - 2.0 * rampWidthInUs;
    // put plateau width on GRAD_UPDATE boundaries
    plateauWidthInUs = ( float )RUP_GRD( plateauWidthInUs );

    if ( plateauWidthInUs <= 0.0 )
    {

      throw std::runtime_error( "Invalid gradient plateau width." );

    }

    return plateauWidthInUs;

  }
  GKG_CATCH( "float "
             "gkg::TwiceRefocusedSpinEchoNMRSequence:: "
             "getDiffusionPulsePlateauWidthInUs( "
             "float maximuGradientAmplitudeInGaussPerCm, "
             "float maximumSlewRateInGaussPerCmPerUs, "
             "float littleDeltaInUs, "
             "float gradientTimeResolution, "
             "float& rampWidthInUs )" );

}


#undef TWOPI_GAM
#undef GAM

#undef MT_PER_M_TO_G_PER_CM
#undef T_PER_M_PER_S_TO_G_PER_CM_PER_US
#undef MS_TO_US

#undef RUP_GRD

