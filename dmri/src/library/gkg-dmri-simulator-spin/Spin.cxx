#include <gkg-dmri-simulator-spin/Spin.h>
#include <gkg-dmri-simulator-pulse-sequence/NMRDefines.h>
#include <gkg-dmri-simulator-pulse-sequence/RadioFrequencyPulse.h>
#include <gkg-core-exception/Exception.h>


gkg::Spin::Spin( gkg::RCPointer< gkg::Particle > particle,
                 gkg::RCPointer< gkg::NMRSequence > nmrSequence,
                 float timeStep,
                 int32_t timeStepFactor,
                 int32_t temporalSubSamplingCount,
                 bool storeIntermediatePhaseShifts,
                 bool storeSpinPhaseEvolution )
          : gkg::RCObject(),
            _particle( particle ),
            _nmrSequence( nmrSequence )
{

  try
  {

    // collecting the point count of the particle traject
    const gkg::LightCurve3d< float >& trajectory = particle->getTrajectory();
    int32_t pointCount = trajectory.getPointCount();
    int32_t iterationCount =
                  ( int32_t )( _nmrSequence->getEchoTimeInUs() / timeStep ) + 1;

    // sanity check
    if ( pointCount < iterationCount )
    {

      throw std::runtime_error( "not enough simulation steps" );

    }

    // getting the iteration count, b-value count, and orientation count
    int32_t gradientAmplitudeCount = _nmrSequence->getGradientAmplitudeCount();
    int32_t orientationCount = _nmrSequence->getOrientationSet()->getCount();

#if 0
    // Original version of the code

    // allocating vectors for the accumulated phases, instant phase shifts,
    // and spin phase evolution
    _accumulatedPhases.resize( gradientAmplitudeCount );


    int32_t g = 0;
    int32_t o = 0;
    for ( g = 0; g < gradientAmplitudeCount; g++ )

    {

      _accumulatedPhases[ g ].resize( orientationCount, 0.0f );


    }
      
    if ( storeIntermediatePhaseShifts ||
         storeSpinPhaseEvolution )
    {

      int32_t shiftCount = iterationCount / temporalSubSamplingCount;

      if ( storeIntermediatePhaseShifts )
      {

        _instantPhaseShifts.resize( gradientAmplitudeCount );

        for ( g = 0; g < gradientAmplitudeCount; g++ )
        {

          _instantPhaseShifts[ g ].resize( orientationCount );

          for ( o = 0; o < orientationCount; o++ )
          {

            _instantPhaseShifts[ g ][ o ].resize( shiftCount, 0.0f );

          }

        }

      }

      if ( storeSpinPhaseEvolution )
      {

        _spinPhaseEvolution.resize( gradientAmplitudeCount );

        for ( g = 0; g < gradientAmplitudeCount; g++ )
        {

          _spinPhaseEvolution[ g ].resize( orientationCount );

          for ( o = 0; o < orientationCount; o++ )
          {

            _spinPhaseEvolution[ g ][ o ].resize( shiftCount, 0.0f );

          }

        }

      }

    }

    // processing the spin phase information
    gkg::Vector3d< float > position;
    float phaseShift = 0.0f;
    int32_t i = 0;
    for ( i = 0; i < iterationCount; i++ )
    {

      position = trajectory.getPoint( i * timeStepFactor );

      for ( g = 0; g < gradientAmplitudeCount; g++ )
      {

        for ( o = 0; o < orientationCount; o++ )
        {

          // getting the phase shift
          phaseShift = getPhaseShift( position, i, g, o );

          // updating the accumulated phases
          _accumulatedPhases[ g ][ o ] += phaseShift;

          // if required, updating the instant phase shifts and phase evolution
          if ( ( storeIntermediatePhaseShifts ) && 
               ( i % temporalSubSamplingCount == 0 ) )
          {

            _instantPhaseShifts[ g ][ o ][ i / temporalSubSamplingCount ]
              = phaseShift;

          }
          if ( ( storeSpinPhaseEvolution ) &&
               ( i % temporalSubSamplingCount == 0 ) )
          {

            _spinPhaseEvolution[ g ][ o ][ i / temporalSubSamplingCount ]
              = _accumulatedPhases[ g ][ o ];

          }

        }

      }

    }
#else
    // Optimized version of the code (4 times faster)

    // allocating vectors for the accumulated phases, instant phase shifts,
    // and spin phase evolution
    _accumulatedPhases.resize( orientationCount * gradientAmplitudeCount, 
                               0.0f );
    _accumulatedPhasesAccessor.resize( orientationCount );
    int32_t g = 0;
    int32_t o = 0;
    float* accPhasePtr = (float*)&_accumulatedPhases[ 0 ];
    for ( o = 0; o < orientationCount; o++ )
    {

      _accumulatedPhasesAccessor[ o ] = accPhasePtr;
      accPhasePtr += gradientAmplitudeCount;

    }
      
    if ( storeIntermediatePhaseShifts ||
         storeSpinPhaseEvolution )
    {

      int32_t shiftCount = iterationCount / temporalSubSamplingCount;

      if ( storeIntermediatePhaseShifts )
      {

        _instantPhaseShifts.resize( gradientAmplitudeCount );

        for ( g = 0; g < gradientAmplitudeCount; g++ )
        {

          _instantPhaseShifts[ g ].resize( orientationCount );

          for ( o = 0; o < orientationCount; o++ )
          {

            _instantPhaseShifts[ g ][ o ].resize( shiftCount, 0.0f );

          }

        }

      }

      if ( storeSpinPhaseEvolution )
      {

        _spinPhaseEvolution.resize( gradientAmplitudeCount );

        for ( g = 0; g < gradientAmplitudeCount; g++ )
        {

          _spinPhaseEvolution[ g ].resize( orientationCount );

          for ( o = 0; o < orientationCount; o++ )
          {

            _spinPhaseEvolution[ g ][ o ].resize( shiftCount, 0.0f );

          }

        }

      }

      // processing the spin phase information
      gkg::Vector3d< float > position;
      float phaseShift = 0.0f;
      int32_t i = 0, j = 0;

      for ( i = 0; i < iterationCount; i++, j += timeStepFactor )
      {

        position = trajectory.getPoint( j );

        for ( g = 0; g < gradientAmplitudeCount; g++ )
        {

          for ( o = 0; o < orientationCount; o++ )
          {

            // getting the phase shift
            phaseShift = getPhaseShift( position, i, g, o );

            // updating the accumulated phases
            _accumulatedPhasesAccessor[ o ][ g ] += phaseShift;

            // if required, updating the instant phase shifts and 
            // phase evolution
            if ( ( i % temporalSubSamplingCount ) == 0 )
            {

              if ( storeIntermediatePhaseShifts )
              {

                _instantPhaseShifts[ g ][ o ][ i / temporalSubSamplingCount ]
                  = phaseShift;

              }
              if ( storeSpinPhaseEvolution )
              {

                _spinPhaseEvolution[ g ][ o ][ i / temporalSubSamplingCount ]
                  = _accumulatedPhasesAccessor[ o ][ g ];

              }

            }

          }

        }

      }

    }
    else
    {

      // processing the spin phase information
      int32_t i = 0;
      const float* position = (const float*)( &trajectory.getPoint( 0 ) );
      int32_t incPosition = 3 * ( timeStepFactor - 1 ) * sizeof( float );
      const float* s = 
           (const float*)( &_nmrSequence->getPhaseShiftPerPosition( 0, 0, 0 ) );
      float* accPtr = &_accumulatedPhases[ 0 ];

      for ( o = 0; o < orientationCount; o++ )
      {

        for ( g = 0; g < gradientAmplitudeCount; g++, accPtr++ )
        {

          const float* p = position;

          for ( i = 0; i < iterationCount; i++, p += incPosition )
          {

            // updating the accumulated phases
            *accPtr += *s++ * *p++;
            *accPtr += *s++ * *p++;
            *accPtr += *s++ * *p++;

          }

        }

      }

    }
#endif

  }
  GKG_CATCH( "gkg::Spin::Spin( "
             "gkg::RCPointer< gkg::Particle > particle, "
             "gkg::RCPointer< gkg::NMRSequence > nmrSequence, "
             "float timeStep, "
             "int32_t temporalSubSamplingCount, "
             "bool storeIntermediatePhaseShifts, "
             "bool storeSpinPhaseEvolution " );

}


gkg::Spin::Spin( gkg::RCPointer< gkg::Particle > particle,
                 gkg::RCPointer< gkg::NMRSequence > nmrSequence,
                 const std::vector< float >& accumulatedPhases )
          : gkg::RCObject(),
            _particle( particle ),
            _nmrSequence( nmrSequence ),
            _accumulatedPhases( accumulatedPhases )
{

  try
  {

    // getting the iteration count, b-value count, and orientation count
    int32_t gradientAmplitudeCount = _nmrSequence->getGradientAmplitudeCount();
    int32_t orientationCount = _nmrSequence->getOrientationSet()->getCount();

    _accumulatedPhasesAccessor.resize( orientationCount );
    int32_t o = 0;
    float* accPhasePtr = &_accumulatedPhases[ 0 ];
    for ( o = 0; o < orientationCount; o++ )
    {

      _accumulatedPhasesAccessor[ o ] = accPhasePtr;
      accPhasePtr += gradientAmplitudeCount;

    }

  }
  GKG_CATCH( "gkg::Spin::Spin( "
             "gkg::RCPointer< gkg::Particle > particle, "
             "gkg::RCPointer< gkg::NMRSequence > nmrSequence,"
             "const std::vector< float >& accumulatedPhases ) " );

}





gkg::Spin::~Spin()
{
}


gkg::RCPointer< gkg::Particle > gkg::Spin::getParticle() const
{

  try
  {

    return _particle;

  }
  GKG_CATCH( "gkg::RCPointer< gkg::Particle > "
             "gkg::Spin::getParticle() const" );

}



float gkg::Spin::getPhaseShift( const gkg::Vector3d< float >& position,
                                int32_t iteration,
                                int32_t gradientAmplitudeIndex,
                                int32_t orientationIndex )
{

  try
  {

    return _nmrSequence->getPhaseShiftPerPosition(
          iteration, gradientAmplitudeIndex, orientationIndex ).dot( position );

  }
  GKG_CATCH( "float gkg::Spin::getPhaseShift( "
             "const gkg::Vector3d< float >& position, "
             "int32_t iteration, "
             "int32_t gradientAmplitudeIndex, "
             "int32_t orientationIndex )" );

}


float gkg::Spin::getAccumulatedPhase( int32_t gradientAmplitudeIndex,
                                      int32_t orientationIndex ) const
{

  try
  {

    //return _accumulatedPhases[ orientationIndex ][ gradientAmplitudeIndex ];
    return _accumulatedPhasesAccessor[ orientationIndex ][
                                                       gradientAmplitudeIndex ];

  }
  GKG_CATCH( "float gkg::Spin::getAccumulatedPhase( "
             "int32_t gradientAmplitudeIndex, "
             "int32_t orientationIndex ) const" );

}


const float* gkg::Spin::getPointerToAccumulatedPhase() const
{

  try
  {

    return &_accumulatedPhases[ 0 ];

  }
  GKG_CATCH( "const float* gkg::Spin::getPointerToAccumulatedPhase() const" );

}




float gkg::Spin::getInstantPhaseShift( int32_t subIteration,
                                       int32_t gradientAmplitudeIndex,
                                       int32_t orientationIndex ) const
{

  try
  {

    return _instantPhaseShifts[ gradientAmplitudeIndex ]
                              [ orientationIndex ]
                              [ subIteration ];

  }
  GKG_CATCH( "float gkg::Spin::getInstantPhaseShift( "
             "int32_t subIteration, "
             "int32_t gradientAmplitudeIndex, "
             "int32_t orientationIndex ) const" );

}


float gkg::Spin::getPhaseEvolution( int32_t subIteration,
                                    int32_t gradientAmplitudeIndex,
                                    int32_t orientationIndex ) const
{

  try
  {

    return _spinPhaseEvolution[ gradientAmplitudeIndex ]
                              [ orientationIndex ]
                              [ subIteration ];

  }
  GKG_CATCH( "float gkg::Spin::getPhaseEvolution( "
             "int32_t subIteration, "
             "int32_t gradientAmplitudeIndex, "
             "int32_t orientationIndex ) const" );

}

