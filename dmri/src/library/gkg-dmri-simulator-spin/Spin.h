#ifndef _gkg_dmri_simulator_spin_Spin_h_
#define _gkg_dmri_simulator_spin_Spin_h_


#include <gkg-core-pattern/RCPointer.h>
#include <gkg-processing-coordinates/Vector3d.h>
#include <gkg-dmri-simulator-particle/Particle.h>
#include <gkg-dmri-simulator-pulse-sequence/NMRSequence.h>
#include <vector>


namespace gkg
{


class Spin : public RCObject
{

  public:

    Spin( RCPointer< Particle > particle,
          RCPointer< NMRSequence > nmrSequence,
          float timeStep,
          int32_t timeStepFactor,
          int32_t temporalSubSamplingCount,
          bool storeIntermediatePhaseShifts = false,
          bool storeSpinPhaseEvolution = false );
    Spin( RCPointer< Particle > particle,
          RCPointer< NMRSequence > nmrSequence,
          const std::vector< float >& accumulatedPhases );
    virtual ~Spin();

    RCPointer< Particle > getParticle() const;

    // methods to get the phase information
    float getPhaseShift( const Vector3d< float >& position,
                         int32_t iteration,
                         int32_t gradientAmplitudeIndex,
                         int32_t orientationIndex );

    float getAccumulatedPhase( int32_t gradientAmplitudeIndex,
                               int32_t orientationIndex ) const;
    const float* getPointerToAccumulatedPhase() const;

    float getInstantPhaseShift( int32_t subIteration,
                                int32_t gradientAmplitudeIndex,
                                int32_t orientationIndex ) const;

    float getPhaseEvolution( int32_t subIteration,
                             int32_t gradientAmplitudeIndex,
                             int32_t orientationIndex ) const;

  protected:

    RCPointer< Particle > _particle;
    RCPointer< NMRSequence > _nmrSequence;

    // _accumulatedPhases[ gradientAmplitudeIndex ]
    //                   [ orientationIndex ]
    // std::vector< std::vector< float > > _accumulatedPhases;
    std::vector< float > _accumulatedPhases;
    std::vector< float* > _accumulatedPhasesAccessor;

    // _instantPhaseShifts[ gradientAmplitudeIndex ]
    //                    [ orientationIndex ]
    //                    [ shift ]
    std::vector< std::vector< std::vector< float > > > _instantPhaseShifts;

    // _spinPhaseEvolution[ gradientAmplitudeIndex ]
    //                    [ orientationIndex ]
    //                    [ shift ]
    std::vector< std::vector< std::vector< float > > > _spinPhaseEvolution;

};


}


#endif
