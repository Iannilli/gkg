#include <gkg-dmri-simulator-spin/SpinCache.h>
#include <gkg-dmri-simulator-mr-imaging/MRImage.h>
#include <gkg-processing-container/Volume_i.h>
#include <gkg-core-exception/Exception.h>


gkg::SpinCache::SpinCache(
                       gkg::RCPointer< gkg::MRImage > mrImage,
                       const std::vector< gkg::RCPointer< gkg::Spin > >& spins )
               : _mrImage( mrImage )
{

  try
  {

    // initializing the lookup table
    gkg::Vector3d< int32_t > size = _mrImage->getSize();
    gkg::Vector3d< double > resolution = _mrImage->getResolution();
    _lut.reallocate( size );
    _lut.setResolution( resolution );
    _lut.fill( 0 );
    _areStatesStored = spins[ 0 ]->getParticle()->areStatesStored();

    // getting the iteration count ( at the echo time )
    int32_t iterationCount = _mrImage->getIterationCount();

    // looping over each spin and updating the spin cache
    std::vector< gkg::RCPointer< gkg::Spin > >::const_iterator
      s = spins.begin(),
      se = spins.end();
    while ( s != se )
    {

      // getting the time fraction for the spin being inside the bounding box
      float timeFraction = ( *s )->getParticle()->getTimeFractionIn(
                                                     _mrImage->getBoundingBox(),
                                                     iterationCount );

      if ( timeFraction >= 0.9 )
      {

        // getting the spin position at the echo time        
        gkg::Vector3d< float >
          position = ( *s )->getParticle()->getPositionAt( iterationCount );

        // getting the current voxel
        gkg::Vector3d< int32_t > voxel = _mrImage->getCacheVoxel( position );

        // adding the spin to the current voxel of the lut
        std::set< gkg::RCPointer< gkg::Spin > >*
          spinSet = getSpins( voxel );

        if ( !spinSet )
        {

          spinSet = new std::set< gkg::RCPointer< gkg::Spin > >;
          spinSet->insert( *s );
          _lut( voxel ) = spinSet;

        }
        else
        {

          spinSet->insert( *s );

        }

      }
      ++ s;

    }

  }
  GKG_CATCH( "gkg::SpinCache::SpinCache( "
             "gkg::RCPointer< gkg::MRImage > mrImage, "
             "const std::vector< gkg::RCPointer< gkg::Spin > >& spins )" );

}


gkg::SpinCache::~SpinCache()
{

  gkg::Volume< std::set< gkg::RCPointer< gkg::Spin > >* >::iterator
    s = _lut.begin(),
    se = _lut.end();
  while ( s != se )
  {

    delete *s;
    ++ s;

  }

}


std::set< gkg::RCPointer< gkg::Spin > >*
gkg::SpinCache::getSpins( const gkg::Vector3d< int32_t >& voxel ) const
{

  try
  {

    return _lut( voxel );

  }
  GKG_CATCH( "std::set< gkg::RCPointer< gkg::Spin > >* "
             "gkg::SpinCache::getSpins( "
             "const gkg::Vector3d< int32_t >& voxel ) const" );

}


bool gkg::SpinCache::areStatesStored() const
{

  try
  {

    return _areStatesStored;

  }
  GKG_CATCH( "bool gkg::SpinCache::areStatesStored() const" );

}
