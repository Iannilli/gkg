#ifndef _gkg_dmri_simulator_spin_SpinCache_h_
#define _gkg_dmri_simulator_spin_SpinCache_h_

#include <gkg-core-pattern/RCPointer.h>
#include <gkg-processing-coordinates/Vector3d.h>
#include <gkg-processing-container/Volume.h>
#include <gkg-dmri-simulator-spin/Spin.h>
#include <set>


namespace gkg
{


class MRImage;


class SpinCache
{

  public:

    SpinCache( RCPointer< MRImage > mrImage,
               const std::vector< RCPointer< Spin > >& spins );
    virtual ~SpinCache();

    std::set< RCPointer< Spin > >*
      getSpins( const Vector3d< int32_t >& voxel ) const;

    bool areStatesStored() const;

  protected:

    RCPointer< MRImage > _mrImage;
    Volume< std::set< RCPointer< Spin > >* > _lut;
    bool _areStatesStored;

};


}


#endif
