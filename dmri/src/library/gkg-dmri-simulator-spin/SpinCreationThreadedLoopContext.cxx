#include <gkg-dmri-simulator-spin/SpinCreationThreadedLoopContext.h>
#include <gkg-core-exception/Exception.h>


gkg::SpinCreationThreadedLoopContext::SpinCreationThreadedLoopContext(
                        std::vector< gkg::RCPointer< gkg::Spin > >& spins,
                        const gkg::ParticleMap& particleMap,
                        const gkg::RCPointer< gkg::NMRSequence >& nmrSequence,
                        const float& timeStep,
                        const int32_t& timeStepFactor,
                        const int32_t& temporalSubSamplingCount )
                                     : gkg::LoopContext(),
                                       _spins( spins ),
                                       _particleMap( particleMap ),
                                       _nmrSequence( nmrSequence ),
                                       _timeStep( &timeStep ),
                                       _timeStepFactor( &timeStepFactor ),
                                       _temporalSubSamplingCount(
                                                    &temporalSubSamplingCount ),
                                       _accumulatedPhases( 0 )
{
}


gkg::SpinCreationThreadedLoopContext::SpinCreationThreadedLoopContext(
                        std::vector< gkg::RCPointer< gkg::Spin > >& spins,
                        const gkg::ParticleMap& particleMap,
                        const gkg::RCPointer< gkg::NMRSequence >& nmrSequence,
                        const std::vector< std::vector< float > >& 
                                                             accumulatedPhases )
                                     : gkg::LoopContext(),
                                       _spins( spins ),
                                       _particleMap( particleMap ),
                                       _nmrSequence( nmrSequence ),
                                       _timeStep( 0 ),
                                       _timeStepFactor( 0 ),
                                       _temporalSubSamplingCount( 0 ),
                                       _accumulatedPhases( &accumulatedPhases )
                                       
{
}


gkg::SpinCreationThreadedLoopContext::~SpinCreationThreadedLoopContext()
{
}


void gkg::SpinCreationThreadedLoopContext::doIt( int32_t startIndex,
                                                 int32_t count )
{

  try
  {

    if ( !_accumulatedPhases )
    {

      int32_t s;
      for ( s = startIndex; s < startIndex + count; s++ )
      {

        _spins[ s ].reset( new gkg::Spin( _particleMap.getParticle( s ),
                                          _nmrSequence,
                                          ( *_accumulatedPhases )[ s ] ) );

      }

    }
    else
    {

      int32_t s;
      for ( s = startIndex; s < startIndex + count; s++ )
      {

        _spins[ s ].reset( new gkg::Spin( _particleMap.getParticle( s ),
                                          _nmrSequence,
                                          *_timeStep,
                                          *_timeStepFactor,
                                          *_temporalSubSamplingCount ) );

      }

    }

  }
  GKG_CATCH( "void gkg::SpinCreationThreadedLoopContext::doIt( "
             "int32_t startIndex, "
             "int32_t count )" );

}

