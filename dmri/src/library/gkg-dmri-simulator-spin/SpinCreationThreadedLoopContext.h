#ifndef _gkg_dmri_simulator_spin_SpinCreationThreadedLoopContext_i_h_
#define _gkg_dmri_simulator_spin_SpinCreationThreadedLoopContext_i_h_

#include <gkg-communication-thread/LoopContext.h>
#include <gkg-dmri-simulator-spin/Spin.h>
#include <gkg-dmri-simulator-particle/ParticleMap.h>
#include <gkg-dmri-simulator-pulse-sequence/NMRSequence.h>
#include <gkg-core-pattern/RCPointer.h>
#include <vector>


namespace gkg
{


class SpinCreationThreadedLoopContext : public LoopContext
{

  public:

    SpinCreationThreadedLoopContext(
                                    std::vector< RCPointer< Spin > >& spins,
                                    const ParticleMap& particleMap,
                                    const RCPointer< NMRSequence >& nmrSequence,
                                    const float& timeStep,
                                    const int32_t& timeStepFactor,
                                    const int32_t& temporalSubSamplingCount );
    SpinCreationThreadedLoopContext(
                                  std::vector< RCPointer< Spin > >& spins,
                                  const ParticleMap& particleMap,
                                  const RCPointer< NMRSequence >& nmrSequence,
                                  const std::vector< std::vector< float > >& 
                                                            accumulatedPhases );

    virtual ~SpinCreationThreadedLoopContext();

    void doIt( int32_t startIndex, int32_t count );

  private:

    std::vector< RCPointer< Spin > >& _spins;
    const ParticleMap& _particleMap;
    const RCPointer< NMRSequence >& _nmrSequence;

    // using the older approach from Jimmy
    const float* _timeStep;
    const int32_t* _timeStepFactor;
    const int32_t* _temporalSubSamplingCount;

    // using the new approach from Kevin
    const std::vector< std::vector< float > >* _accumulatedPhases;


};


}


#endif
