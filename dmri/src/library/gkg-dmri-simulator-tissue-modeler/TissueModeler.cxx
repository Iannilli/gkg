#include <gkg-dmri-simulator-tissue-modeler/TissueModeler.h>
#include <gkg-processing-container/MeshMap_i.h>
#include <gkg-processing-coordinates/ElectrostaticOrientationSet.h>
#include <gkg-processing-mesh/MeshAccumulator_i.h>
#include <gkg-processing-mesh/ConvexHull_i.h>
#include <gkg-processing-mesh/MeshScaler_i.h>
#include <gkg-processing-mesh/MeshTransformer_i.h>
#include <gkg-processing-transform/Translation3d.h>
#include <gkg-communication-thread/ThreadedLoop.h>
#include <gkg-dmri-simulator-particle/ParticleMotionThreadedLoopContext.h>
#include <gkg-core-exception/Exception.h>


gkg::TissueModeler::TissueModeler(
                                 const gkg::BoundingBox< float >& boundingBox,
                                 gkg::RCPointer< gkg::MotionModel > motionModel,
                                 float timeStep,
                                 int32_t stepCount,
                                 int32_t temporalSubSamplingCount,
                                 const gkg::Vector3d< int32_t >& cacheSize,
                                 bool verbose )
                  : gkg::SceneModeler( boundingBox,
                                       timeStep,
                                       stepCount,
                                       temporalSubSamplingCount,
                                       cacheSize,
                                       verbose ),
                    _motionModel( motionModel ),
                    _particleCache( gkg::RCPointer< gkg::TissueModeler >( this ),
                                    cacheSize ),
                    _hasMultipleCompartments( false )
{
}


gkg::TissueModeler::~TissueModeler()
{
}


gkg::RCPointer< gkg::MotionModel > gkg::TissueModeler::getMotionModel() const
{

  try
  {

    return _motionModel;

  }
  GKG_CATCH( "gkg::RCPointer< gkg::MotionModel > "
             "gkg::TissueModeler::getMotionModel() const" );

}


bool gkg::TissueModeler::hasMultipleCompartments() const
{

  try
  {

    return _hasMultipleCompartments;

  }
  GKG_CATCH( "bool gkg::TissueModeler::hasMultipleCompartments() const" );

}


void gkg::TissueModeler::addMembrane( const std::string& name,
                                     gkg::RCPointer< gkg::Membrane > membrane )
{

  try
  {

    this->addEvolvingMesh( name, membrane );

    // comparing the motion model of the added membrane to the global motion
    // model
    if ( membrane->getMotionModel()->getDisplacement( _timeStep ) !=
         _motionModel->getDisplacement( _timeStep ) )
    {

      _hasMultipleCompartments = true;

    }

  }
  GKG_CATCH( "void gkg::TissueModeler::addMembrane( "
             "const std::string& name, "
             "gkg::RCPointer< gkg::Membrane > membrane )" );

}


int32_t gkg::TissueModeler::getMembraneCount() const
{

  try
  {
  
    return this->getEvolvingMeshCount();
  
  }
  GKG_CATCH( "int32_t gkg::TissueModeler::getMembraneCount() const" );

}


void gkg::TissueModeler::addParticles(
               const std::vector< gkg::RCPointer< gkg::Particle > >& particles )
{

  try
  {

    // adding particles to the particle map
    _particleMap.setParticles( particles );

    // updating the particle cache
    _particleCache.update( particles );

  }
  GKG_CATCH( "void gkg::TissueModeler::addParticles( "
             "const std::vector< gkg::RCPointer< gkg::Particle > >& "
             "particles )" );

}


gkg::ParticleMap& gkg::TissueModeler::getParticleMap()
{

  try
  {

    return _particleMap;

  }
  GKG_CATCH( "gkg::ParticleMap& "
             "gkg::TissueModeler::getParticleMap()" );

}


const gkg::ParticleCache& gkg::TissueModeler::getParticleCache() const
{

  try
  {

    return _particleCache;

  }
  GKG_CATCH( "const gkg::ParticleCache& "
             "gkg::TissueModeler::getParticleCache() const" );

}


void gkg::TissueModeler::getParticlesOfInterest(
                     const gkg::BoundingBox< float >& boundingBox,
                     int32_t stepCount,
                     std::vector< gkg::RCPointer< gkg::Particle > >& particles )
{

  try
  {

    float timeFraction = 0.0;
    gkg::RCPointer< gkg::Particle > particle;
    int32_t particleCount = _particleMap.getParticleCount();
    int32_t p = 0;
    for ( p = 0; p < particleCount; p++ )
    {

      particle = _particleMap.getParticle( p );
      timeFraction = particle->getTimeFractionIn( boundingBox, stepCount );
      if ( timeFraction >= 0.9 )
      {

        particles.push_back( particle );

      }

    }

  }
  GKG_CATCH( "void gkg::TissueModeler::getParticlesOfInterest( "
             "const gkg::BoundingBox< float >& boundingBox, "
             "int32_t stepCount, "
             "std::vector< gkg::RCPointer< gkg::Particle > >& particles )" );

}



void gkg::TissueModeler::getGlobalParticleMesh(
                 const std::vector< gkg::RCPointer< gkg::Particle > > particles,
                 int32_t particleCount,
                 float sphereRadius,
                 int32_t sphereVertexCount,
                 gkg::MeshMap< int32_t, float, 3U >& globalParticleMesh )
{

  try
  {

    // building the mesh of a sphere
    gkg::OrientationSet orientationSet(
              gkg::ElectrostaticOrientationSet( sphereVertexCount / 2
                                               ).getSymmetricalOrientations() );

    gkg::MeshMap< int32_t, float, 3U > sphereMeshMap;
    gkg::ConvexHull::getInstance().addConvexHull( 
                                               orientationSet.getOrientations(),
                                               0,
                                               sphereMeshMap );

    int32_t sphereVertexCount = sphereMeshMap.vertices.getSiteCount( 0 );
    int32_t spherePolygonCount = sphereMeshMap.polygons.getPolygonCount( 0 );   

    // preparing mesh accumulator
    gkg::MeshAccumulator< int32_t, float, 3U > meshAccumulator;
    meshAccumulator.reserve( 0,
                             sphereVertexCount * particleCount,
                             spherePolygonCount * particleCount );

    // preparing translation
    gkg::Translation3d< float > translation;

    // preparing mesh scaler and transformer
    gkg::MeshScaler< int32_t, float, 3U > meshScaler;
    gkg::MeshTransformer< int32_t, float, 3U > meshTransformer;

    // creating the scale vector
    std::vector< float > scaling( sphereVertexCount, sphereRadius );

    // adding sphere meshes
    int32_t p = 0;
    for ( p = 0; p < particleCount; p++ )
    {

      // collecting the position of the particle p at step s
      const gkg::Vector3d< float >&
        position = particles[ p ]->getStartingPosition();

      // processing 3D transform
      translation.setDirectTranslation( position.x,
                                        position.y,
                                        position.z );

      // processing local scaled mesh map
      gkg::MeshMap< int32_t, float, 3U > localMeshMap( sphereMeshMap );
      meshScaler.scale( localMeshMap, 0, scaling, localMeshMap );
      meshTransformer.transform( localMeshMap, translation, localMeshMap );

      // accumulating local mesh map
      meshAccumulator.add( localMeshMap );

    }

    globalParticleMesh.add( meshAccumulator );

  }
  GKG_CATCH( "void gkg::TissueModeler::getGlobalParticleMesh( "
             "const std::vector< gkg::RCPointer< gkg::Particle > > particles, "
             "int32_t particleCount, "
             "float sphereRadius, "
             "int32_t sphereVertexCount, "
             "gkg::MeshMap< int32_t, float, 3U >& globalParticleMesh )" );

}


void gkg::TissueModeler::iterate( int32_t step )
{

  try
  {

    // moving the membranes (can be parallelized)
    this->gkg::SceneModeler::iterate( step );

    int32_t particleCount = _particleMap.getParticleCount();
#if 0
    int32_t p;

    // moving the particles (can be parallelized)
    for ( p = 0; p < particleCount; p++ )
    {

      _particleMap.getParticle( p )->move( step );

    }
#endif

    // creating a threaded loop context for particle motion
    gkg::ParticleMotionThreadedLoopContext
      particleMotionThreadedLoopContext( _particleMap, step );

    // creating a threaded loop
    gkg::ThreadedLoop threadedLoop( &particleMotionThreadedLoopContext,
                                    0,                       // start index
                                    particleCount
                                  );

    // launching the threaded loop
    threadedLoop.launch();

/*
    // updating particle cache
    for ( p = 0; p < particleCount; p++ )
    {

      _particleCache.update( _particleMap.getParticle( p ) );

    }
*/

  }
  GKG_CATCH( "void gkg::TissueModeler::iterate( int32_t step )" );

}

