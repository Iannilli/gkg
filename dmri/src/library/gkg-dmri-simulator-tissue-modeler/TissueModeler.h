#ifndef _gkg_dmri_simulator_tissue_modeler_TissueModeler_h_
#define _gkg_dmri_simulator_tissue_modeler_TissueModeler_h_


#include <gkg-processing-mesh/SceneModeler.h>
#include <gkg-dmri-simulator-membrane/Membrane.h>
#include <gkg-dmri-simulator-particle/ParticleMap.h>
#include <gkg-dmri-simulator-particle/ParticleCache.h>
#include <gkg-dmri-simulator-motion/MotionModel.h>


#define DEFAULT_FREE_AVERAGE_DISPLACEMENT   1


namespace gkg
{


//
// units for:
// - time step                 -> us
// - free average displacement -> um
//


class TissueModeler : public SceneModeler
{

  public:

    TissueModeler( const BoundingBox< float >& boundingBox,
                  RCPointer< MotionModel > motionModel,
                  float timeStep,
                  int32_t stepCount,
                  int32_t temporalSubSamplingCount,
                  const Vector3d< int32_t >& cacheSize,
                  bool verbose );
    virtual ~TissueModeler();

    RCPointer< MotionModel > getMotionModel() const;

    bool hasMultipleCompartments() const;

    // methods to add membranes
    void addMembrane( const std::string& name, RCPointer< Membrane > membrane );
    int32_t getMembraneCount() const;

    // methods to add particles
    void addParticles( const std::vector< RCPointer< Particle > >& particles );
    ParticleMap& getParticleMap();
    const ParticleCache& getParticleCache() const;
    void getParticlesOfInterest(
                              const BoundingBox< float >& boundingBox,
                              int32_t stepCount,
                              std::vector< RCPointer< Particle > >& particles );

    // methods to get particle renderings
    void getGlobalParticleMesh(
                           const std::vector< RCPointer< Particle > > particles,
                           int32_t particleCount,
                           float sphereRadius,
                           int32_t sphereVertexCount,
                           MeshMap< int32_t, float, 3U >& globalParticleMesh );

  protected:

    virtual void iterate( int32_t iteration );

    RCPointer< MotionModel > _motionModel;
    ParticleCache _particleCache;
    bool _hasMultipleCompartments;

    ParticleMap _particleMap;

};


}


#endif
