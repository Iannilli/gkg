#include <gkg-dmri-simulator-virtual-tissues/FiberPopulation.h>
#include <gkg-processing-coordinates/UniformOrientationSet.h>
#include <gkg-core-exception/Exception.h>
#include <list>
#include <cmath>


#define SPHERE_OVERSAMPLING_RATIO 4.0


gkg::FiberPopulation::FiberPopulation(
                       const gkg::BoundingBox< float >& fieldOfView,
                       float axonDiameterMean,
                       float axonDiameterStdDev,
                       float gRatioMean,
                       float gRatioStdDev,
                       float internodalLengthToNodeWidthRatioMean,
                       float internodalLengthToNodeWidthRatioStdDev,
                       float interbeadingLengthMean,
                       float interbeadingLengthStdDev,
                       float beadingMagnitudeRatioMean,
                       float beadingMagnitudeRatioStdDev,
                       const gkg::Vector3d< float >& meanOrientation,
                       float intraAxonalVolumeFraction,
                       float globalAngularDispersion,
                       float tortuosityVariation,
                       float tortuosityMagnitude,
                       float tortuosityWaveLength,
                       float tortuosityAngularDispersion,
                       float fiberResolutionAlongAxis,
                       gkg::RCPointer< gkg::RandomGenerator > randomGenerator )
                     : gkg::NonOverlappingSpherePopulation(),
                       _fieldOfView( fieldOfView ),
                       _axonDiameterMean( axonDiameterMean ),
                       _axonDiameterStdDev( axonDiameterStdDev ),
                       _gRatioMean( gRatioMean ),
                       _gRatioStdDev( gRatioStdDev ),
                       _internodalLengthToNodeWidthRatioMean(
                                         internodalLengthToNodeWidthRatioMean ),
                       _internodalLengthToNodeWidthRatioStdDev(
                                       internodalLengthToNodeWidthRatioStdDev ),
                       _interbeadingLengthMean( interbeadingLengthMean ),
                       _interbeadingLengthStdDev( interbeadingLengthStdDev ),
                       _beadingMagnitudeRatioMean( beadingMagnitudeRatioMean ),
                       _beadingMagnitudeRatioStdDev(
                                                  beadingMagnitudeRatioStdDev ),
                       _meanOrientation( meanOrientation ),
                       _intraAxonalVolumeFraction( intraAxonalVolumeFraction ),
                       _globalAngularDispersion( globalAngularDispersion ),
                       _tortuosityVariation( tortuosityVariation ),
                       _tortuosityMagnitude( tortuosityMagnitude ),
                       _tortuosityWaveLength( tortuosityWaveLength ),
                       _tortuosityAngularDispersion(
                                                  tortuosityAngularDispersion ),
                       _fiberResolutionAlongAxis( fiberResolutionAlongAxis ),
                       _randomGenerator( randomGenerator )
{

  try
  {

    _axonDiameterScale = ( _axonDiameterStdDev * _axonDiameterStdDev ) / 
                         _axonDiameterMean;
    _axonDiameterShape = ( _axonDiameterMean * _axonDiameterMean ) /
                         ( _axonDiameterStdDev * _axonDiameterStdDev );

    _gRatioScale = ( _gRatioStdDev * _gRatioStdDev ) / 
                   _gRatioMean;
    _gRatioShape = ( _gRatioMean * _gRatioMean ) /
                   ( _gRatioStdDev * _gRatioStdDev );

    _factory = gkg::NumericalAnalysisSelector::getInstance().
                                                     getImplementationFactory();

    _kappa = _factory->getWatsonKappaParameterFromAngularDispersion(
                                              ( double )globalAngularDispersion,
                                              true,
                                              1000,
                                              0.1 );

  }
  GKG_CATCH( "gkg::FiberPopulation::FiberPopulation( "
             "const gkg::BoundingBox< float >& fieldOfView, "
             "float axonDiameterMean, "
             "float axonDiameterStdDev, "
             "float gRatioMean, "
             "float gRatioStdDev, "
             "float internodalLengthToNodeWidthRatioMean, "
             "float internodalLengthToNodeWidthRatioStdDev, "
             "float interbeadingLengthMean, "
             "float interbeadingLengthStdDev, "
             "float beadingMagnitudeRatioMean, "
             "float beadingMagnitudeRatioStdDev, "
             "const gkg::Vector3d< float >& meanOrientation, "
             "float intraAxonalVolumeFraction, "
             "float globalAngularDispersion, "
             "float tortuosityVariation, "
             "float tortuosityMagnitude, "
             "float tortuosityWaveLength, "
             "float tortuosityAngularDispersion, "
             "float fiberResolutionAlongAxis, "
             "gkg::RCPointer< gkg::RandomGenerator > randomGenerator )" );

}


gkg::FiberPopulation::FiberPopulation( const gkg::FiberPopulation& other )
                     : gkg::NonOverlappingSpherePopulation( other ),
                       _fieldOfView( other._fieldOfView ),
                       _axonDiameterMean( other._axonDiameterMean ),
                       _axonDiameterStdDev( other._axonDiameterStdDev ),
                       _gRatioMean( other._gRatioMean ),
                       _gRatioStdDev( other._gRatioStdDev ),
                       _internodalLengthToNodeWidthRatioMean(
                                  other._internodalLengthToNodeWidthRatioMean ),
                       _internodalLengthToNodeWidthRatioStdDev(
                                other._internodalLengthToNodeWidthRatioStdDev ),
                       _interbeadingLengthMean( other._interbeadingLengthMean ),
                       _interbeadingLengthStdDev(
                                              other._interbeadingLengthStdDev ),
                       _beadingMagnitudeRatioMean(
                                             other._beadingMagnitudeRatioMean ),
                       _beadingMagnitudeRatioStdDev(
                                           other._beadingMagnitudeRatioStdDev ),
                       _meanOrientation( other._meanOrientation ),
                       _intraAxonalVolumeFraction(
                                             other._intraAxonalVolumeFraction ),
                       _globalAngularDispersion(
                                               other._globalAngularDispersion ),
                       _tortuosityVariation( other._tortuosityVariation ),
                       _tortuosityMagnitude( other._tortuosityMagnitude ),
                       _tortuosityWaveLength( other._tortuosityWaveLength ),
                       _tortuosityAngularDispersion(
                                           other._tortuosityAngularDispersion ),
                       _fiberResolutionAlongAxis(
                                              other._fiberResolutionAlongAxis ),
                       _randomGenerator( other._randomGenerator )
{

  try
  {



  }
  GKG_CATCH( "gkg::FiberPopulation::FiberPopulation( "
             "const gkg::FiberPopulation& other )" );

}


gkg::FiberPopulation::~FiberPopulation()
{
}


gkg::FiberPopulation&
gkg::FiberPopulation::operator=( const gkg::FiberPopulation& other )
{

  try
  {

    this->gkg::NonOverlappingSpherePopulation::operator=( other );

    _fieldOfView = other._fieldOfView;
    _axonDiameterMean = other._axonDiameterMean;
    _axonDiameterStdDev = other._axonDiameterStdDev;
    _gRatioMean = other._gRatioMean;
    _gRatioStdDev = other._gRatioStdDev;
    _internodalLengthToNodeWidthRatioMean =
                                    other._internodalLengthToNodeWidthRatioMean;
    _internodalLengthToNodeWidthRatioStdDev =
                                  other._internodalLengthToNodeWidthRatioStdDev;
    _interbeadingLengthMean = other._interbeadingLengthMean;
    _interbeadingLengthStdDev = other._interbeadingLengthStdDev;
    _beadingMagnitudeRatioMean = other._beadingMagnitudeRatioMean;
    _beadingMagnitudeRatioStdDev = other._beadingMagnitudeRatioStdDev;
    _meanOrientation = other._meanOrientation;
    _intraAxonalVolumeFraction = other._intraAxonalVolumeFraction;
    _globalAngularDispersion = other._globalAngularDispersion;
    _tortuosityVariation = other._tortuosityVariation;
    _tortuosityMagnitude = other._tortuosityMagnitude;
    _tortuosityWaveLength = other._tortuosityWaveLength;
    _tortuosityAngularDispersion = other._tortuosityAngularDispersion;
    _fiberResolutionAlongAxis = other._fiberResolutionAlongAxis;
    _randomGenerator = other._randomGenerator;

    return *this;

  }
  GKG_CATCH( "gkg::FiberPopulation& "
             "gkg::FiberPopulation::opeartor=( "
             "const gkg::FiberPopulation& other )" );


}


float gkg::FiberPopulation::getRandomAxonDiameter() const
{

  try
  {

    return _factory->getGammaRandomNumber( *_randomGenerator,
                                           _axonDiameterShape,
                                           _axonDiameterScale * 1e3 ) / 1e3;

  }
  GKG_CATCH( "float gkg::FiberPopulation::"
             "getRandomAxonDiameter() const" );

}


float gkg::FiberPopulation::getRandomGRatio() const
{

  try
  {

    float gRatio = _factory->getGammaRandomNumber( *_randomGenerator,
                                                   _gRatioShape,
                                                   _gRatioScale * 1e3 ) / 1e3;
    while ( gRatio > 1.0 )
    {

      gRatio = _factory->getGammaRandomNumber( *_randomGenerator,
                                               _gRatioShape,
                                               _gRatioScale * 1e3 ) / 1e3;

    }

    return gRatio;

  }
  GKG_CATCH( "float gkg::FiberPopulation::"
             "getRandomGRatio() const" );

}



void gkg::FiberPopulation::collectSpheres( 
     std::vector< std::vector<
       gkg::NonOverlappingSpherePopulationContainer::Sphere > >& spheres ) const
{

  try
  {

    ////////////////////////////////////////////////////////////////////////////
    // computing the volume of the sphere containing the field of view
    ////////////////////////////////////////////////////////////////////////////

    float maximumFieldOfViewSpan =
      std::max( _fieldOfView.getUpperX() -
                _fieldOfView.getLowerX(),
                std::max( _fieldOfView.getUpperY() -
                          _fieldOfView.getLowerY(),
                          _fieldOfView.getUpperZ() -
                          _fieldOfView.getLowerZ() ) );
    float radiusOfSphereContainingFov = maximumFieldOfViewSpan * 
                                        ( float )std::sqrt( 3.0f ) / 2.0f;
    float volumeOfSphereContainingFov = ( 4.0f * M_PI * 
                                          radiusOfSphereContainingFov *
                                          radiusOfSphereContainingFov *
                                          radiusOfSphereContainingFov ) / 3.0f;

    ////////////////////////////////////////////////////////////////////////////
    // computing the number of fibers and their diameters to reach the target
    // intra-axonal volume fraction
    ////////////////////////////////////////////////////////////////////////////

    std::vector< float > axonDiameters;
    std::vector< float > gRatios;
    std::vector< float > outerMyelinSheathDiameters;

    float currentIntraAxonalVolumeFraction = 0.0f;
    float axonDiameter = 0.0f;
    float gRatio = 0.0f;
    float outerMyelinSheathDiameter = 0.0f;
    do
    {

      axonDiameter = this->getRandomAxonDiameter();
      gRatio = this->getRandomGRatio();
      outerMyelinSheathDiameter = axonDiameter / gRatio;

      axonDiameters.push_back( axonDiameter );
      gRatios.push_back( gRatio );
      outerMyelinSheathDiameters.push_back( outerMyelinSheathDiameter );

      currentIntraAxonalVolumeFraction += 
                                        ( M_PI * axonDiameter * axonDiameter *
                                          radiusOfSphereContainingFov / 2.0f ) /
                                        volumeOfSphereContainingFov;

    }
    while ( currentIntraAxonalVolumeFraction < _intraAxonalVolumeFraction );

    int32_t fiberCount = ( int32_t )axonDiameters.size();

    ////////////////////////////////////////////////////////////////////////////
    // allocating the output vector of vector of spheres to the adequate size
    ////////////////////////////////////////////////////////////////////////////

    spheres.resize( fiberCount );


    ////////////////////////////////////////////////////////////////////////////
    // looping over fiber(s)
    ////////////////////////////////////////////////////////////////////////////

    gkg::UniformOrientationSet uniformOrientationSet( fiberCount );
    int32_t f = 0;
    gkg::Vector3d< float > startingSphereCenter;
    gkg::Vector3d< float > nextSphereCenter;
    gkg::Vector3d< float > fiberOrientation;
    gkg::Vector3d< float > delta;
    float sphereRadius = 0.0f;
    for ( f = 0; f < fiberCount; f++ )
    {

      // computing the starting point over the sphere containing the FOV
      startingSphereCenter = uniformOrientationSet.getOrientation( f ) * 
                             radiusOfSphereContainingFov;

      // computing the dispersed fiber orientation according to Watson's Kappa
      _factory->getRandomWatsonOrientation( _kappa,
                                            _meanOrientation,
                                            *_randomGenerator,
                                            fiberOrientation );
      // make sure that the orientation is pointed towards sphere center
      if ( startingSphereCenter.dot( fiberOrientation ) > 0 )
      {

        fiberOrientation *= -1.0f;

      }

      // computing sphere 
      sphereRadius = outerMyelinSheathDiameters[ f ] / 2.0f;

      nextSphereCenter = startingSphereCenter;
      std::list< gkg::NonOverlappingSpherePopulationContainer::Sphere >
        sphereList;
      sphereList.push_back( 
        gkg::NonOverlappingSpherePopulationContainer::Sphere( nextSphereCenter,
                                                              sphereRadius ) );

      delta.x = fiberOrientation.x * sphereRadius / SPHERE_OVERSAMPLING_RATIO;
      delta.y = fiberOrientation.y * sphereRadius / SPHERE_OVERSAMPLING_RATIO;
      delta.z = fiberOrientation.z * sphereRadius / SPHERE_OVERSAMPLING_RATIO;
      while ( nextSphereCenter.getNorm() < radiusOfSphereContainingFov )
      {

        nextSphereCenter += delta;
        sphereList.push_back(
          gkg::NonOverlappingSpherePopulationContainer::Sphere(
                                                               nextSphereCenter,
                                                               sphereRadius ) );

      }

      spheres[ f ] = std::vector<
                         gkg::NonOverlappingSpherePopulationContainer::Sphere >(
                                                             sphereList.begin(),
                                                             sphereList.end() );

    }

  }
  GKG_CATCH( "void gkg::FiberPopulation::collectSpheres( "
             "std::vector< std::vector< "
             "gkg::NonOverlappingSpherePopulationContainer::Sphere > >& "
             "spheres ) const" );

}


#undef SPHERE_OVERSAMPLING_RATIO
