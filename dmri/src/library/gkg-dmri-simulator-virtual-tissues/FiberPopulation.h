#ifndef _gkg_dmri_simulator_virtual_tissues_FiberPopulation_h_
#define _gkg_dmri_simulator_virtual_tissues_FiberPopulation_h_


#include <gkg-dmri-simulator-virtual-tissues/NonOverlappingSpherePopulation.h>
#include <gkg-processing-container/BoundingBox.h>
#include <gkg-processing-coordinates/Vector3d.h>
#include <gkg-processing-numericalanalysis/RandomGenerator.h>
#include <gkg-processing-numericalanalysis/NumericalAnalysisSelector.h>
#include <gkg-core-pattern/RCPointer.h>
#include <vector>


namespace gkg
{


class FiberPopulation : public NonOverlappingSpherePopulation
{

  public:

    FiberPopulation( const BoundingBox< float >& fieldOfView,
                     float axonDiameterMean,
                     float axonDiameterStdDev,
                     float gRatioMean,
                     float gRatioStdDev,
                     float internodalLengthToNodeWidthRatioMean,
                     float internodalLengthToNodeWidthRatioStdDev,
                     float interbeadingLengthMean,
                     float interbeadingLengthStdDev,
                     float beadingMagnitudeRatioMean,
                     float beadingMagnitudeRatioStdDev,
                     const Vector3d< float >& meanOrientation,
                     float intraAxonalVolumeFraction,
                     float globalAngularDispersion,
                     float tortuosityVariation,
                     float tortuosityMagnitude,
                     float tortuosityWaveLength,
                     float tortuosityAngularDispersion,
                     float fiberResolutionAlongAxis,
                     RCPointer< RandomGenerator > randomGenerator );
    FiberPopulation( const FiberPopulation& other );
    virtual ~FiberPopulation();

    FiberPopulation& operator=( const FiberPopulation& other );

    float getRandomAxonDiameter() const;
    float getRandomGRatio() const;

    void collectSpheres( 
      std::vector< std::vector<
           NonOverlappingSpherePopulationContainer::Sphere > >& spheres ) const;

  protected:

    BoundingBox< float > _fieldOfView;
    float _axonDiameterMean;
    float _axonDiameterStdDev;
    float _gRatioMean;
    float _gRatioStdDev;
    float _internodalLengthToNodeWidthRatioMean;
    float _internodalLengthToNodeWidthRatioStdDev;
    float _interbeadingLengthMean;
    float _interbeadingLengthStdDev;
    float _beadingMagnitudeRatioMean;
    float _beadingMagnitudeRatioStdDev;
    Vector3d< float > _meanOrientation;
    float _intraAxonalVolumeFraction;
    float _globalAngularDispersion;
    float _tortuosityVariation;
    float _tortuosityMagnitude;
    float _tortuosityWaveLength;
    float _tortuosityAngularDispersion;
    float _fiberResolutionAlongAxis;
    RCPointer< RandomGenerator > _randomGenerator;
    NumericalAnalysisImplementationFactory* _factory;

    float _axonDiameterScale;
    float _axonDiameterShape;
    float _gRatioScale;
    float _gRatioShape;

    std::vector< float > _axonDiameters;
    std::vector< float > _gRatios;
    std::vector< float > _outerMyelinSheathDiameters;
    double _kappa;


};


}


#endif
