#include <gkg-dmri-simulator-virtual-tissues/GlialCellPopulation.h>
#include <gkg-core-exception/Exception.h>


gkg::GlialCellPopulation::GlialCellPopulation(
                        const gkg::BoundingBox< float >& fieldOfView,
                        float diameterMean,
                        float diameterStdDev,
                        float deformationRatio,
                        float intracellularVolumeFraction,
                        gkg::RCPointer< gkg::RandomGenerator > randomGenerator )
                         : gkg::NonOverlappingSpherePopulation(),
                           _fieldOfView( fieldOfView ),
                           _diameterMean( diameterMean ),
                           _diameterStdDev( diameterStdDev ),
                           _deformationRatio( deformationRatio ),
                           _intracellularVolumeFraction(
                                                  intracellularVolumeFraction ),
                           _randomGenerator( randomGenerator )
{

  try
  {

    _factory = gkg::NumericalAnalysisSelector::getInstance().
                                                     getImplementationFactory();


  }
  GKG_CATCH( "gkg::GlialCellPopulation::GlialCellPopulation( "
             "const gkg::BoundingBox< float >& fieldOfView, "
             "float diameterMean, "
             "float dDiameterStdDev, "
             "float deformationRatio, "
             "float intracellularVolumeFraction, "
             "gkg::RCPointer< gkg::RandomGenerator > randomGenerator )" );

}


gkg::GlialCellPopulation::GlialCellPopulation(
                                         const gkg::GlialCellPopulation& other )
                         : gkg::NonOverlappingSpherePopulation( other ),
                           _fieldOfView( other._fieldOfView ),
                           _diameterMean( other._diameterMean ),
                           _diameterStdDev( other._diameterStdDev ),
                           _deformationRatio( other._deformationRatio ),
                           _intracellularVolumeFraction(
                                           other._intracellularVolumeFraction ),
                           _randomGenerator( other._randomGenerator )
{

  try
  {



  }
  GKG_CATCH( "gkg::GlialCellPopulation::GlialCellPopulation( "
             "const gkg::GlialCellPopulation& other )" );

}


gkg::GlialCellPopulation::~GlialCellPopulation()
{
}


gkg::GlialCellPopulation&
gkg::GlialCellPopulation::operator=( const gkg::GlialCellPopulation& other )
{

  try
  {

    this->gkg::NonOverlappingSpherePopulation::operator=( other );

    _fieldOfView = other._fieldOfView;
    _diameterMean = other._diameterMean;
    _diameterStdDev = other._diameterStdDev;
    _deformationRatio = other._deformationRatio;
    _intracellularVolumeFraction = other._intracellularVolumeFraction;
    _randomGenerator = other._randomGenerator;

    return *this;

  }
  GKG_CATCH( "gkg::GlialCellPopulation& "
             "gkg::GlialCellPopulation::opeartor=( "
             "const gkg::GlialCellPopulation& other )" );


}


void gkg::GlialCellPopulation::collectSpheres( 
     std::vector< std::vector<
       gkg::NonOverlappingSpherePopulationContainer::Sphere > >& /* spheres */ )
const
{

  try
  {



  }
  GKG_CATCH( "void gkg::GlialCellPopulation::collectSpheres( "
             "std::vector< std::vector< "
             "gkg::NonOverlappingSpherePopulationContainer::Sphere > >& "
             "spheres ) const" );

}


