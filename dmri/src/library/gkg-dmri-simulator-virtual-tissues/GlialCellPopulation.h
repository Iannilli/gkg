#ifndef _gkg_dmri_simulator_virtual_tissues_GlialCellPopulation_h_
#define _gkg_dmri_simulator_virtual_tissues_GlialCellPopulation_h_


#include <gkg-dmri-simulator-virtual-tissues/NonOverlappingSpherePopulation.h>
#include <gkg-processing-container/BoundingBox.h>
#include <gkg-processing-coordinates/Vector3d.h>
#include <gkg-processing-numericalanalysis/RandomGenerator.h>
#include <gkg-processing-numericalanalysis/NumericalAnalysisSelector.h>
#include <gkg-core-pattern/RCPointer.h>


namespace gkg
{


class GlialCellPopulation : public NonOverlappingSpherePopulation
{

  public:

    GlialCellPopulation( const BoundingBox< float >& fieldOfView,
                         float diameterMean,
                         float diameterStdDev,
                         float deformationRatio,
                         float intracellularVolumeFraction,
                         RCPointer< RandomGenerator > randomGenerator );
    GlialCellPopulation( const GlialCellPopulation& other );
    virtual ~GlialCellPopulation();

    GlialCellPopulation& operator=( const GlialCellPopulation& other );

    void collectSpheres( 
      std::vector< std::vector<
           NonOverlappingSpherePopulationContainer::Sphere > >& spheres ) const;

  protected:

    BoundingBox< float > _fieldOfView;
    float _diameterMean;
    float _diameterStdDev;
    float _deformationRatio;
    float _intracellularVolumeFraction;
    RCPointer< RandomGenerator > _randomGenerator;
    NumericalAnalysisImplementationFactory* _factory;

};


}


#endif
