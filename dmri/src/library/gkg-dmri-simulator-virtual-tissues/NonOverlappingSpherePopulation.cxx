#include <gkg-dmri-simulator-virtual-tissues/NonOverlappingSpherePopulation.h>
#include <gkg-core-exception/Exception.h>


gkg::NonOverlappingSpherePopulation::NonOverlappingSpherePopulation()
{
}


gkg::NonOverlappingSpherePopulation::NonOverlappingSpherePopulation(
                              const gkg::NonOverlappingSpherePopulation& other )
                                    : _centers( other._centers ),
                                      _radii( other._radii ),
                                      _populationAndItemIds(
                                                   other._populationAndItemIds )
{
}


gkg::NonOverlappingSpherePopulation::~NonOverlappingSpherePopulation()
{
}


gkg::NonOverlappingSpherePopulation& 
gkg::NonOverlappingSpherePopulation::operator=( 
                              const gkg::NonOverlappingSpherePopulation& other )
{

  try
  {

    _centers = other._centers;
    _radii = other._radii;
    _populationAndItemIds = other._populationAndItemIds;

    return *this;

  }
  GKG_CATCH( "gkg::NonOverlappingSpherePopulation& "
             "gkg::NonOverlappingSpherePopulation::operator=( "
             "const gkg::NonOverlappingSpherePopulation& other " );

}


const std::vector< gkg::Vector3d< float > >& 
gkg::NonOverlappingSpherePopulation::getCenters() const
{

  try
  {

    return _centers;

  }
  GKG_CATCH( "const std::vector< gkg::Vector3d< float > >& "
             "gkg::NonOverlappingSpherePopulation::getCenters() const" );

}


const std::vector< float >& 
gkg::NonOverlappingSpherePopulation::getRadii() const
{

  try
  {

    return _radii;

  }
  GKG_CATCH( "const std::vector< float >& "
             "gkg::NonOverlappingSpherePopulation::getRadii() const" );

}


const std::vector< std::pair< int32_t, int32_t > >&
gkg::NonOverlappingSpherePopulation::getPopulationAndItemIds() const
{

  try
  {

    return _populationAndItemIds;

  }
  GKG_CATCH( "const std::vector< std::pair< int32_t, int32_t > >& "
             "gkg::NonOverlappingSpherePopulation::getPopulationAndItemIds() "
             "const" );

}
