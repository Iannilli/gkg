#ifndef _gkg_dmri_simulator_virtual_tissues_NonOverlappingSpherePopulation_h_
#define _gkg_dmri_simulator_virtual_tissues_NonOverlappingSpherePopulation_h_


#include <gkg-dmri-simulator-virtual-tissues/NonOverlappingSpherePopulationContainer.h>
#include <gkg-processing-coordinates/Vector3d.h>
#include <vector>
#include <utility>


namespace gkg
{


class NonOverlappingSpherePopulation
{

  public:

    virtual ~NonOverlappingSpherePopulation();

    const std::vector< gkg::Vector3d< float > >& getCenters() const;
    const std::vector< float >& getRadii() const;
    const std::vector< std::pair< int32_t, int32_t > >&
      getPopulationAndItemIds() const;

    virtual void collectSpheres( 
      std::vector< std::vector<
        NonOverlappingSpherePopulationContainer::Sphere > >&
                                                            spheres ) const = 0;

  protected:

    NonOverlappingSpherePopulation();
    NonOverlappingSpherePopulation(
                                  const NonOverlappingSpherePopulation& other );
    NonOverlappingSpherePopulation& operator=( 
                                  const NonOverlappingSpherePopulation& other );

    std::vector< gkg::Vector3d< float > > _centers;
    std::vector< float > _radii;
    std::vector< std::pair< int32_t, int32_t > > _populationAndItemIds;


};


}


#endif
