#include <gkg-dmri-simulator-virtual-tissues/NonOverlappingSpherePopulationContainer.h>
#include <gkg-dmri-simulator-virtual-tissues/NonOverlappingSpherePopulation.h>
#include <gkg-core-exception/Exception.h>



gkg::NonOverlappingSpherePopulationContainer::
                                         NonOverlappingSpherePopulationContainer(
                                   const gkg::BoundingBox< float >& fieldOfView,
                                   float gridResolution )
                                            : _fieldOfView( fieldOfView ),
                                              _gridResolution( gridResolution )
{
}


gkg::NonOverlappingSpherePopulationContainer::
                                      ~NonOverlappingSpherePopulationContainer()
{
}


void gkg::NonOverlappingSpherePopulationContainer::reserve(
                                   int32_t nonOverlappingSpherePopulationCount )
{

  try
  {

    _nonOverlappingSpherePopulations.reserve(
                                          nonOverlappingSpherePopulationCount );

  }
  GKG_CATCH( "void gkg::NonOverlappingSpherePopulationContainer::reserve( "
             "int32_t nonOverlappingSpherePopulationCount )" );


}


void gkg::NonOverlappingSpherePopulationContainer::add(
                    const gkg::RCPointer< gkg::NonOverlappingSpherePopulation >&
                                                nonOverlappingSpherePopulation )
{

  try
  {

    _nonOverlappingSpherePopulations.push_back(
                                               nonOverlappingSpherePopulation );

  }
  GKG_CATCH( "void gkg::NonOverlappingSpherePopulationContainer::add( "
             "const gkg::RCPointer< gkg::NonOverlappingSpherePopulation >& "
             "nonOverlappingSpherePopulation )" );

}


void gkg::NonOverlappingSpherePopulationContainer::collectSpheres()
{

  try
  {

    // reallocating the sphere container
    int32_t nonOverlappingSpherePopulationCount = 
                             ( int32_t )_nonOverlappingSpherePopulations.size();
    _spheres.resize( nonOverlappingSpherePopulationCount );

    // looping over non overlapping sphere population(s)
    int32_t nonOverlappingSpherePopulationIndex = 0;
    for ( nonOverlappingSpherePopulationIndex = 0;
          nonOverlappingSpherePopulationIndex < 
          nonOverlappingSpherePopulationCount;
          nonOverlappingSpherePopulationIndex ++ )
    {

      _nonOverlappingSpherePopulations[ nonOverlappingSpherePopulationIndex ]->
        collectSpheres( _spheres[ nonOverlappingSpherePopulationIndex ] );

    }

  }
  GKG_CATCH( "void gkg::NonOverlappingSpherePopulationContainer::"
             "collectSpheres()" );

}


void gkg::NonOverlappingSpherePopulationContainer::removeSphereOverlaps()
{

  try
  {

/*
    float maximumFieldOfViewSpan =
      std::max( _fieldOfView.getUpperX() -
                _fieldOfView.getLowerX(),
                std::max( _fieldOfView.getUpperY() -
                          _fieldOfView.getLowerY(),
                          _fieldOfView.getUpperZ() -
                          _fieldOfView.getLowerZ() ) );
    float radiusOfSphereContainingFov = maximumFieldOfViewSpan * 
                                        ( float )std::sqrt( 3.0f ) / 2.0f;
*/

  }
  GKG_CATCH( "void gkg::NonOverlappingSpherePopulationContainer::"
             "removeOverlaps()" );

}
