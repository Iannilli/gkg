#ifndef _gkg_dmri_simulator_virtual_tissues_NonOverlappingSpherePopulationContainer_h_
#define _gkg_dmri_simulator_virtual_tissues_NonOverlappingSpherePopulationContainer_h_


#include <gkg-core-pattern/RCPointer.h>
#include <gkg-processing-container/BoundingBox.h>


namespace gkg
{


class NonOverlappingSpherePopulation;


class NonOverlappingSpherePopulationContainer
{

  public:


    struct Sphere
    {

      Sphere( const Vector3d< float >& theCenter, float theRadius ) :
        center( theCenter ),
        radius( theRadius )
      {

      }
      Sphere() : center( 0.0f, 0.0f, 0.0f ),
                 radius( 0.0f )
      {

      }
      Sphere( const Sphere& other ) : center( other.center ),
                                      radius( other.radius )
      {

      }
      Sphere& operator=( const Sphere& other )
      {

        center = other.center;
        radius = other.radius;
        return *this;

      }
      Vector3d< float > center;
      float radius;

    };

    NonOverlappingSpherePopulationContainer(
                  const BoundingBox< float >& fieldOfView,
                  float gridResolution );
    virtual ~NonOverlappingSpherePopulationContainer();

    void reserve( int32_t nonOverlappingSpherePopulationCount );
    void add( const RCPointer< NonOverlappingSpherePopulation >&
                                              nonOverlappingSpherePopulation );
    void collectSpheres();
    void removeSphereOverlaps();

  protected:

    BoundingBox< float > _fieldOfView;
    float _gridResolution;
    std::vector< RCPointer< NonOverlappingSpherePopulation > >
      _nonOverlappingSpherePopulations;

    std::vector< std::vector< std::vector< Sphere > > > _spheres;

};





}


#endif
