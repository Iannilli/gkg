#ifndef _gkg_dmri_tractography_DeterministicTractographyAlgorithm_i_h_
#define _gkg_dmri_tractography_DeterministicTractographyAlgorithm_i_h_


#include <gkg-dmri-tractography/DeterministicTractographyAlgorithm.h>
#include <gkg-dmri-tractography/StreamlineTractographyAlgorithm_i.h>
#include <gkg-core-exception/Exception.h>


template < class L, class Compare >
inline
gkg::DeterministicTractographyAlgorithm< L, Compare >::
                             DeterministicTractographyAlgorithm(
                                      const gkg::Vector3d< int32_t >& size,
                                      const gkg::Vector3d< double >& resolution,
                                      float step,
                                      int32_t storingIncrement,
                                      float minimumFiberLength,
                                      float maximumFiberLength,
                                      float apertureAngle )
                           : gkg::StreamlineTractographyAlgorithm< L, Compare >(
                                                          size,
                                                          resolution,
                                                          step,
                                                          storingIncrement,
                                                          minimumFiberLength,
                                                          maximumFiberLength,
                                                          apertureAngle )
{
}


template < class L, class Compare >
inline
gkg::DeterministicTractographyAlgorithm< L, Compare >::
                             DeterministicTractographyAlgorithm(
                      const gkg::Vector3d< int32_t >& size,
                      const gkg::Vector3d< double >& resolution,
                      const std::vector< double >& scalarParameters,
                      const std::vector< std::string >& /* stringParameters */ )
                           : gkg::StreamlineTractographyAlgorithm< L, Compare >(
                                     size,
                                     resolution,
                                     ( float )scalarParameters[ 1 ],
                                     ( int32_t )( scalarParameters[ 2 ] + 0.5 ),
                                     ( float )scalarParameters[ 3 ],
                                     ( float )scalarParameters[ 4 ],
                                     ( float )scalarParameters[ 5 ] )
{
}


template < class L, class Compare >
inline
gkg::DeterministicTractographyAlgorithm< L, Compare >::
                                           ~DeterministicTractographyAlgorithm()
{
}


template < class L, class Compare >
inline
std::string 
gkg::DeterministicTractographyAlgorithm< L, Compare >::getStaticName()
{

  return "streamline-deterministic";

}


template < class L, class Compare >
inline
bool gkg::DeterministicTractographyAlgorithm< L, Compare >::track(
                    const gkg::Vector3d< float >& startingPointOdfs,
                    const int32_t& startingOrientationIndex,
                    const int16_t& startingLabel,
                    gkg::OdfContinuousField& odfContinuousField,
                    const gkg::Volume< int16_t >& roiMask,
                    const gkg::BoundingBox< int32_t >& roiMaskBoundingBox,
                    const gkg::Volume< uint8_t >& computingMask,
                    const gkg::BoundingBox< int32_t >& computingMaskBoundingBox,
                    const gkg::Transform3d< float >&
                                         /*transform3dFromRoisVoxelToOdfsReal*/,
                    const gkg::Transform3d< float >&
                                        /*transform3dFromRoisVoxelToMaskVoxel*/,
                    const gkg::Transform3d< float >&
                                            transform3dFromOdfsRealToMaskVoxel,
                    const gkg::Transform3d< float >&
                                            transform3dFromOdfsRealToRoisVoxel,
                    std::list< gkg::Vector3d< float > >& trajectory,
                    float& fiberLength ) const
{

  try
  {

    // getting reference to the output orientation set of the ODF field
    const gkg::OrientationSet&
      orientationSet = odfContinuousField.getOutputOrientationSet();

    // initializing current point & orientation in ODF real frame
    gkg::Vector3d< float > pointOdfs = startingPointOdfs;
    int32_t orientationIndex = startingOrientationIndex;

    // processing current voxel in ODF voxel frame
    gkg::Vector3d< int32_t > voxelOdfs = this->getVoxelCoordinates( pointOdfs );

    // processing current voxel in MASK voxel frame
    gkg::Vector3d< int32_t > voxelMask;
    transform3dFromOdfsRealToMaskVoxel.getIntegerDirect( pointOdfs,
                                                         voxelMask );

    // allocating voxel in ROIs voxel frame
    gkg::Vector3d< int32_t > voxelRois;

    // adding current point to the trajectory
    trajectory.push_back( pointOdfs );

    // performing the tracking
    bool isInStartingRoi = true;
    gkg::OrientationDistributionFunction* odf = 0;
    while ( isInStartingRoi || computingMask( voxelMask ) )
    {

      // getting pointer to current ODF
      odf = odfContinuousField.getItem( pointOdfs );

      // computing the orientation index corresponding to the maximum
      // probablility within a cone indices of the orientations contained in the
      // cone, as well as the minimum and maximum probabilities and indices
      orientationIndex = odf->getPrincipalOrientationIndexFast(
                                                             orientationIndex );

      // deleting ODF
      delete odf;

      // computing new position and orientation
      pointOdfs += orientationSet.getOrientation( orientationIndex ) *
                   this->_step;
      voxelOdfs = this->getVoxelCoordinates( pointOdfs );
      transform3dFromOdfsRealToMaskVoxel.getIntegerDirect( pointOdfs,
                                                           voxelMask );
      transform3dFromOdfsRealToRoisVoxel.getIntegerDirect( pointOdfs,
                                                           voxelRois );

      if ( odfContinuousField.isValid( pointOdfs ) &&
           computingMaskBoundingBox.contains( voxelMask ) )
      {
          
        // testing if new current point is still in any of the starting ROI
        
        if ( !roiMaskBoundingBox.contains( voxelRois ) ||
             ( roiMask( voxelRois ) != startingLabel ) )
        {

          isInStartingRoi = false;

        }

        // adding current point to the trajectory
        trajectory.push_back( pointOdfs );

        // updating fiber length
        fiberLength += this->_step;

        // if fiber length exceeds the maximum allowed fiber length, then return
        if ( fiberLength > this->_maximumFiberLength )
        {

          return false;

        }

      }
      else
      {

        break;

      }

    }
    return true;

  }
  GKG_CATCH( "template < class L, class Compare > "
             "inline "
             "bool gkg::DeterministicTractographyAlgorithm< L, Compare >:: "
             "track( "
             "const gkg::Vector3d< float >& startingPointOdfs, "
             "const int32_t& startingOrientationIndex, "
             "const int16_t& startingLabel, "
             "gkg::OdfContinuousField& odfContinuousField, "
             "const gkg::Volume< int16_t >& roiMask, "
             "const gkg::BoundingBox< int32_t >& roiMaskBoundingBox, "
             "const gkg::Volume< uint8_t >& computingMask, "
             "const gkg::BoundingBox< int32_t >& computingMaskBoundingBox, "
             "const gkg::Transform3d< float >& "
             "transform3dFromRoisVoxelToOdfsReal, "
             "const gkg::Transform3d< float >& "
             "transform3dFromRoisVoxelToMaskVoxel, "
             "const gkg::Transform3d< float >& "
             "transform3dFromOdfsRealToMaskVoxel, "
             "const gkg::Transform3d< float >& "
             "transform3dFromOdfsRealToRoisVoxel, "
             "std::list< gkg::Vector3d< float > >& trajectory, "
             "float& fiberLength ) const" );

}


#endif

