#ifndef _gkg_dmri_tractography_RegularizedDeterministicTractographyAlgorithm_h_
#define _gkg_dmri_tractography_RegularizedDeterministicTractographyAlgorithm_h_


#include <gkg-dmri-tractography/StreamlineTractographyAlgorithm.h>
#include <gkg-processing-algobase/Rescaler_i.h>
#include <gkg-core-pattern/Creator.h>


namespace gkg
{


class OdfCartesianField;


template < class L, class Compare = std::less< L > >
class RegularizedDeterministicTractographyAlgorithm :
           public StreamlineTractographyAlgorithm< L, Compare >,
           public Creator4Arg< RegularizedDeterministicTractographyAlgorithm< L,
                                                                      Compare >,
                               TractographyAlgorithm< L, Compare >,
                               const Vector3d< int32_t >&,
                               const Vector3d< double >&,
                               const std::vector< double >&,
                               const std::vector< std::string >& >

{

  public:

    RegularizedDeterministicTractographyAlgorithm(
                                        const Vector3d< int32_t >& size,
                                        const Vector3d< double >& resolution,
                                        float step,
                                        int32_t storingIncrement,
                                        float minimumFiberLength,
                                        float maximumFiberLength,
                                        float apertureAngle,
                                        float gfaLowerBoundary,
                                        float gfaUpperBoundary );
    RegularizedDeterministicTractographyAlgorithm(
                           const Vector3d< int32_t >& size,
                           const Vector3d< double >& resolution,
                           const std::vector< double >& scalarParameters,
                           const std::vector< std::string >& stringParameters );
    virtual ~RegularizedDeterministicTractographyAlgorithm();

    float getGFALowerBoundary() const;
    float getGFAUpperBoundary() const;

    void prepare( OdfCartesianField& odfCartesianField,
                  const SiteMap< int32_t, int32_t,
                               std::less< int32_t > >& mask,
                  const Transform3d< float >&
                                            transform3dFromRoisVoxelToOdfsReal,
                  const Transform3d< float >&
                                            transform3dFromRoisVoxelToMaskVoxel,
                  const Transform3d< float >&
                                            transform3dFromOdfsRealToMaskVoxel,
                  const Transform3d< float >&
                                            transform3dFromOdfsRealToRoisVoxel,
                  bool verbose );

    static std::string getStaticName();

  protected:

    friend class Creator4Arg< RegularizedDeterministicTractographyAlgorithm< L,
                                                                      Compare >,
                              TractographyAlgorithm< L, Compare >,
                              const Vector3d< int32_t >&,
                              const Vector3d< double >&,
                              const std::vector< double >&,
                              const std::vector< std::string >& >;

    bool track( const Vector3d< float >& startingPointOdfs,
                const int32_t& startingOrientationIndex,
                const int16_t& startingLabel,
                OdfContinuousField& odfContinuousField,
                const Volume< int16_t >& roiMask,
                const BoundingBox< int32_t >& roiMaskBoundingBox,
                const Volume< uint8_t >& computingMask,
                const BoundingBox< int32_t >& computingMaskBoundingBox,
                const Transform3d< float >&
                                            transform3dFromRoisVoxelToOdfsReal,
                const Transform3d< float >&
                                            transform3dFromRoisVoxelToMaskVoxel,
                const Transform3d< float >&
                                            transform3dFromOdfsRealToMaskVoxel,
                const Transform3d< float >&
                                            transform3dFromOdfsRealToRoisVoxel,
                std::list< Vector3d< float > >& trajectory,
                float& fiberLength ) const;

    float _gfaLowerBoundary;
    float _gfaUpperBoundary;
    Rescaler< float, float > _gfaRescaler;

};


}


#endif
