#ifndef _gkg_dmri_tractography_RegularizedDeterministicTractographyAlgorithm_i_h_
#define _gkg_dmri_tractography_RegularizedDeterministicTractographyAlgorithm_i_h_


#include <gkg-dmri-tractography/RegularizedDeterministicTractographyAlgorithm.h>
#include <gkg-dmri-tractography/StreamlineTractographyAlgorithm_i.h>
#include <gkg-dmri-container/OdfCartesianField.h>
#include <gkg-processing-container/BoundingBox.h>
#include <gkg-processing-transform/CompositeTransform3d.h>
#include <gkg-processing-transform/Scaling3d.h>
#include <gkg-dmri-odf-functor/OdfFunctorFactory_i.h>
#include <gkg-processing-algobase/Rescaler_i.h>
#include <gkg-core-exception/Exception.h>


#define CUMULATED_HISTOGRAM_BIN_COUNT 1000
#define CUMULATED_HISTOGRAM_THRESHOLD 0.95


template < class L, class Compare >
inline
gkg::RegularizedDeterministicTractographyAlgorithm< L, Compare >::
                             RegularizedDeterministicTractographyAlgorithm(
                                      const gkg::Vector3d< int32_t >& size,
                                      const gkg::Vector3d< double >& resolution,
                                      float step,
                                      int32_t storingIncrement,
                                      float minimumFiberLength,
                                      float maximumFiberLength,
                                      float apertureAngle,
                                      float gfaLowerBoundary,
                                      float gfaUpperBoundary )
                           : gkg::StreamlineTractographyAlgorithm< L, Compare >(
                                                          size,
                                                          resolution,
                                                          step,
                                                          storingIncrement,
                                                          minimumFiberLength,
                                                          maximumFiberLength,
                                                          apertureAngle ),
                             _gfaLowerBoundary( gfaLowerBoundary ),
                             _gfaUpperBoundary( gfaUpperBoundary )
{
}


template < class L, class Compare >
inline
gkg::RegularizedDeterministicTractographyAlgorithm< L, Compare >::
                             RegularizedDeterministicTractographyAlgorithm(
                      const gkg::Vector3d< int32_t >& size,
                      const gkg::Vector3d< double >& resolution,
                      const std::vector< double >& scalarParameters,
                      const std::vector< std::string >& /* stringParameters */ )
                           : gkg::StreamlineTractographyAlgorithm< L, Compare >(
                                     size,
                                     resolution,
                                     ( float )scalarParameters[ 1 ],
                                     ( int32_t )( scalarParameters[ 2 ] + 0.5 ),
                                     ( float )scalarParameters[ 3 ],
                                     ( float )scalarParameters[ 4 ],
                                     ( float )scalarParameters[ 5 ] ),
                             _gfaLowerBoundary(
                                               ( float )scalarParameters[ 6 ] ),
                             _gfaUpperBoundary( ( float )scalarParameters[ 7 ] )
{
}


template < class L, class Compare >
inline
gkg::RegularizedDeterministicTractographyAlgorithm< L, Compare >::
                                ~RegularizedDeterministicTractographyAlgorithm()
{
}


template < class L, class Compare >
inline
float 
gkg::RegularizedDeterministicTractographyAlgorithm< L,
                                          Compare >::getGFALowerBoundary() const
{

  return _gfaLowerBoundary;

}


template < class L, class Compare >
inline
float 
gkg::RegularizedDeterministicTractographyAlgorithm< L,
                                          Compare >::getGFAUpperBoundary() const
{

  return _gfaUpperBoundary;

}


template < class L, class Compare >
inline
void gkg::RegularizedDeterministicTractographyAlgorithm< L, Compare >::prepare(
                               gkg::OdfCartesianField& odfCartesianField,
                               const gkg::SiteMap< int32_t, int32_t,
                                                   std::less< int32_t > >& mask,
                               const gkg::Transform3d< float >&
                                         /*transform3dFromRoisVoxelToOdfsReal*/,
                               const gkg::Transform3d< float >&
                                        /*transform3dFromRoisVoxelToMaskVoxel*/,
                               const gkg::Transform3d< float >&
                                            transform3dFromOdfsRealToMaskVoxel,
                               const gkg::Transform3d< float >&
                                         /*transform3dFromOdfsRealToRoisVoxel*/,
                               bool verbose )
{

  try
  {

    // getting offset
    gkg::Vector3d< int32_t > 
      offset = odfCartesianField.getOffset();

    // processing GFA map and bounding box
    std::vector< double > scalarParameters;
    std::vector< std::string > stringParameters;
    gkg::Volume< float > gfa;
    gkg::OdfFunctorFactory< gkg::Volume< float >, float >::getInstance().
                              getOdfFunctor( "gfa" ).process( gfa,
                                                              odfCartesianField,
                                                              scalarParameters,
                                                              stringParameters,
                                                              verbose );
    gkg::BoundingBox< int32_t > gfaBoundingBox( 0, gfa.getSizeX() - 1,
                                                0, gfa.getSizeY() - 1,
                                                0, gfa.getSizeZ() - 1 );


    // processing GFA boundaries
    if ( verbose )
    {

      std::cout << "processing GFA boundaries : " << std::flush;

    }
    gkg::Vector3d< int32_t > voxelOdfs, site;
    gkg::Vector3d< float > floatVoxelMask;
    gkg::Vector3d< float > pointOdfs;
    float lowerBoundary = 1.0;
    float upperBoundary = 0.0;
    float gfaLowerBoundary = 0.0;
    float gfaUpperBoundary = 1.0; 
    float value = 0.0;
    std::list< gkg::Vector3d< int32_t > >::const_iterator
      s = mask.getSites( 0 ).begin(),
      se = mask.getSites( 0 ).end();
    while ( s != se )
    {

      // processing site coordinate in the ODF voxel frame
      floatVoxelMask.x = ( float )s->x;
      floatVoxelMask.y = ( float )s->y;
      floatVoxelMask.z = ( float )s->z;
      transform3dFromOdfsRealToMaskVoxel.getInverse( floatVoxelMask,
                                                     pointOdfs );
      voxelOdfs = this->getVoxelCoordinates( pointOdfs );
      site = voxelOdfs - offset;

      if ( gfaBoundingBox.contains( site ) )
      {

        value = gfa( site );
        if ( value < lowerBoundary )
        {

          lowerBoundary = value;

        }
        else if ( value > upperBoundary )
        {

          upperBoundary = value;

        }

      }
      ++ s;

    }
    if ( verbose )
    {

      std::cout << lowerBoundary << ", " << upperBoundary << std::endl;

    }

    if ( std::fabs( upperBoundary - lowerBoundary ) > 1e-6 )
    {

      // processing GFA cumulated histogram
      if ( verbose )
      {

        std::cout << "processing GFA cumulated histogram : " << std::flush;

      }
      std::vector< float > cumulatedHistogram( CUMULATED_HISTOGRAM_BIN_COUNT );
      s = mask.getSites( 0 ).begin();
      int32_t level = 0;
      float sum = 0;
      while ( s != se )
      {

        // processing site coordinate in the ODF voxel frame
        floatVoxelMask.x = ( float )s->x;
        floatVoxelMask.y = ( float )s->y;
        floatVoxelMask.z = ( float )s->z;
        transform3dFromOdfsRealToMaskVoxel.getInverse( floatVoxelMask,
                                                       pointOdfs );
        voxelOdfs = this->getVoxelCoordinates( pointOdfs );
        site = voxelOdfs - offset;

        if ( gfaBoundingBox.contains( site ) )
        {

          value = gfa( site );
          level = ( int32_t )( ( value - lowerBoundary ) *
                               ( float )( CUMULATED_HISTOGRAM_BIN_COUNT - 1 ) /
                               ( upperBoundary - lowerBoundary ) );
          cumulatedHistogram[ level ] ++;
          ++ sum;

        }
        ++ s;

      }

      // normalizing histogram
      if ( sum > 0.0 )
      {

        std::vector< float >::iterator h = cumulatedHistogram.begin(),
                                       he = cumulatedHistogram.end();
        while ( h != he )
        {

          *h /= sum;
          ++ h;

        }

      }
      size_t h;
      for ( h = 1; h < cumulatedHistogram.size(); h++ )
      {

        cumulatedHistogram[ h ] += cumulatedHistogram[ h - 1 ];

      }
      if ( verbose )
      {

        std::cout << "done" << std::endl;

      }

      if ( verbose )
      {

        std::cout << "processing GFA rescaler boundaries : " << std::flush;

      }

      // processing GFA rescaler boundaries
      level = CUMULATED_HISTOGRAM_BIN_COUNT - 1;
      while ( ( level >= 0 ) &&
              ( cumulatedHistogram[ level ] > CUMULATED_HISTOGRAM_THRESHOLD ) )
      {

        -- level;

      }
     
       gfaLowerBoundary = lowerBoundary;
       gfaUpperBoundary = lowerBoundary + ( float )level * 
                               ( upperBoundary - lowerBoundary ) /
                               ( float )( CUMULATED_HISTOGRAM_BIN_COUNT - 1 );

    }

    if ( _gfaLowerBoundary >= 0.0 )
    {

      gfaLowerBoundary = _gfaLowerBoundary;

    }
    if ( _gfaUpperBoundary > 0.0 )
    {

      gfaUpperBoundary = _gfaUpperBoundary;

    }
    _gfaRescaler = gkg::Rescaler< float, float >( gfaLowerBoundary,
                                                  gfaUpperBoundary,
                                                  0.0,
                                                  1.0 );

    gkg::Rescaler< gkg::Volume< float >,
                   gkg::Volume< float > > rescaler( gfaLowerBoundary,
                                                    gfaUpperBoundary,
                                                    0.0,
                                                    1.0 );
    if ( verbose )
    {

      std::cout << gfaLowerBoundary << ", " << gfaUpperBoundary << std::endl;

    }

  }
  GKG_CATCH( "template < class L, class Compare > "
             "inline "
             "void gkg::RegularizedDeterministicTractographyAlgorithm< L, "
             "Compare >::"
             "prepare( gkg::OdfCartesianField& odfCartesianField, "
             "const gkg::SiteMap< int32_t, int32_t, "
             "std::less< int32_t > >& mask, "
             "const gkg::Transform3d< float >& "
             "transform3dFromRoisVoxelToOdfsReal, "
             "const gkg::Transform3d< float >& "
             "transform3dFromRoisVoxelToMaskVoxel, "
             "const gkg::Transform3d< float >& "
             "transform3dFromOdfsRealToMaskVoxel, "
             "const gkg::Transform3d< float >& "
             "transform3dFromOdfsRealToRoisVoxel, "
             "bool verbose )" );

}


template < class L, class Compare >
inline
std::string 
gkg::RegularizedDeterministicTractographyAlgorithm< L, Compare >::getStaticName(
                                                                               )
{

  return "streamline-regularized-deterministic";
}

template < class L, class Compare >
inline
bool gkg::RegularizedDeterministicTractographyAlgorithm< L, Compare >::track(
                    const gkg::Vector3d< float >& startingPointOdfs,
                    const int32_t& startingOrientationIndex,
                    const int16_t& startingLabel,
                    gkg::OdfContinuousField& odfContinuousField,
                    const gkg::Volume< int16_t >& roiMask,
                    const gkg::BoundingBox< int32_t >& roiMaskBoundingBox,
                    const gkg::Volume< uint8_t >& computingMask,
                    const gkg::BoundingBox< int32_t >& computingMaskBoundingBox,
                    const gkg::Transform3d< float >&
                                         /*transform3dFromRoisVoxelToOdfsReal*/,
                    const gkg::Transform3d< float >&
                                        /*transform3dFromRoisVoxelToMaskVoxel*/,
                    const gkg::Transform3d< float >&
                                            transform3dFromOdfsRealToMaskVoxel,
                    const gkg::Transform3d< float >&
                                            transform3dFromOdfsRealToRoisVoxel,
                    std::list< gkg::Vector3d< float > >& trajectory,
                    float& fiberLength ) const
{

  try
  {


    // getting reference to the output orientation set of the ODF field
    const gkg::OrientationSet&
      orientationSet = odfContinuousField.getOutputOrientationSet();

    // initializing current point & orientation in ODF real frame
    gkg::Vector3d< float > pointOdfs = startingPointOdfs;
    int32_t orientationIndex = startingOrientationIndex;

    // processing current voxel in ODF voxel frame
    gkg::Vector3d< int32_t > voxelOdfs = this->getVoxelCoordinates( pointOdfs );

    // processing current voxel in MASK voxel frame
    gkg::Vector3d< int32_t > voxelMask;
    transform3dFromOdfsRealToMaskVoxel.getIntegerDirect( pointOdfs,
                                                         voxelMask );

    // allocating voxel in ROIs voxel frame
    gkg::Vector3d< int32_t > voxelRois;

    // allocating current orientation
    gkg::Vector3d< float > orientation;

    // adding current point to the trajectory
    trajectory.push_back( pointOdfs );

    // performing the tracking
    bool isInStartingRoi = true;
    gkg::OrientationDistributionFunction* odf = 0;
    float alpha = 0.0;
    while ( isInStartingRoi || computingMask( voxelMask ) )
    {

      // getting pointer to current ODF
      odf = odfContinuousField.getItem( pointOdfs );

      // computing the index of the principal orientation corresponding to
      // the maximum probability in the solid angle around the current 
      // orientation and of aperture angle _apertureAngle, and collecting the 
      // orientation indices
      int32_t principalOrientationIndex = 
                      odf->getPrincipalOrientationIndexFast( orientationIndex );

      // processing alpha
      _gfaRescaler.rescale( odf->getGeneralizedFractionalAnisotropy(),
                            alpha );

      // computing the new 'regularized' orientation and orientation index
      orientation =
        orientationSet.getOrientation( orientationIndex ) * ( 1 - alpha ) +
        orientationSet.getOrientation( principalOrientationIndex ) * alpha;
      orientation.normalize();
      orientationIndex = orientationSet.getNearestOrientationIndex(
                                                                 orientation );

      // deleting ODF
      delete odf;

      // computing the new position
      pointOdfs += orientation * this->_step;
      voxelOdfs = this->getVoxelCoordinates( pointOdfs );
      transform3dFromOdfsRealToMaskVoxel.getIntegerDirect( pointOdfs,
                                                           voxelMask );
      transform3dFromOdfsRealToRoisVoxel.getIntegerDirect( pointOdfs,
                                                           voxelRois );

      if ( odfContinuousField.isValid( pointOdfs ) &&
           computingMaskBoundingBox.contains( voxelMask ) )
      {

        // testing is new current point is still in any of the starting ROI
        if ( !roiMaskBoundingBox.contains( voxelRois ) ||
             ( roiMask( voxelRois ) != startingLabel ) )
        {

          isInStartingRoi = false;

        }

        // adding current point to the trajectory
        trajectory.push_back( pointOdfs );

        // updating fiber length
        fiberLength += this->_step;

        // if fiber length exceeds the maximum allowed fiber length, then return
        if ( fiberLength > this->_maximumFiberLength )
        {

          return false;

        }

      }
      else
      {

        break;

      }

    }
    return true;

  }
  GKG_CATCH( "template < class L, class Compare > "
             "inline "
             "bool gkg::RegularizedDeterministicTractographyAlgorithm< L, "
             "Compare >:: "
             "track( "
             "const gkg::Vector3d< float >& startingPointOdfs, "
             "const int32_t& startingOrientationIndex, "
             "const int16_t& startingLabel, "
             "gkg::OdfContinuousField& odfContinuousField, "
             "const gkg::Volume< int16_t >& roiMask, "
             "const gkg::BoundingBox< int32_t >& roiMaskBoundingBox, "
             "const gkg::Volume< uint8_t >& computingMask, "
             "const gkg::BoundingBox< int32_t >& computingMaskBoundingBox, "
             "const gkg::Transform3d< float >& "
             "transform3dFromRoisVoxelToOdfsReal, "
             "const gkg::Transform3d< float >& "
             "transform3dFromRoisVoxelToMaskVoxel, "
             "const gkg::Transform3d< float >& "
             "transform3dFromOdfsRealToMaskVoxel, "
             "const gkg::Transform3d< float >& "
             "transform3dFromOdfsRealToRoisVoxel, "
             "std::list< gkg::Vector3d< float > >& trajectory, "
             "float& fiberLength ) const" );

}


#endif
