#ifndef _gkg_dmri_tractography_StreamlineTractographyAlgorithm_h_
#define _gkg_dmri_tractography_StreamlineTractographyAlgorithm_h_


#include <gkg-dmri-tractography/TractographyAlgorithm.h>
#include <gkg-processing-coordinates/OrientationSet.h>
#include <gkg-communication-thread/LoopContext.h>
#include <gkg-communication-thread/ThreadGauge.h>


namespace gkg
{


template < class T > class Volume;
template < class L, class Compare > class StreamlineTractographyAlgorithm;
template < class T > class BoundingBox;


//
// class StreamlineGauge
//

class StreamlineGauge : public ThreadGauge
{

  public:

    StreamlineGauge( int32_t maxCount );

    void add( int32_t value );
    void reset();

  private:

    int32_t _count;
    int32_t _maxCount;
    bool _fiberIndexAlreadyDisplayed;

};


//
// class ThreadedLoopContext< L, Compare >
//

template < class L, class Compare = std::less< L > >
class ThreadedLoopContext : public LoopContext
{

  public:

    ThreadedLoopContext(
                StreamlineGauge& gauge,
                const StreamlineTractographyAlgorithm< L, Compare >&
                                                streamlineTractographyAlgorithm,
                const int16_t& roiIndex,
                const std::vector< Vector3d< int32_t > >& sites,
                const VoxelSampler< float >& voxelSampler,
                OdfContinuousField& odfContinuousField,
                const Volume< int16_t >& roiMask,
                const BoundingBox< int32_t >& roiMaskBoundingBox,
                const Volume< uint8_t >& computingMask,
                const BoundingBox< int32_t >& computingMaskBoundingBox,
                const Transform3d< float >& transform3dFromRoisVoxelToOdfsReal,
                const Transform3d< float >& transform3dFromRoisVoxelToMaskVoxel,
                const Transform3d< float >& transform3dFromOdfsRealToMaskVoxel,
                const Transform3d< float >& transform3dFromOdfsRealToRoisVoxel,
                std::vector< typename BundleMap< L, Compare >::Fiber >& fibers,
                bool& verbose );
    virtual ~ThreadedLoopContext();

    void doIt( int32_t startIndex, int32_t count );

  private:

    const StreamlineTractographyAlgorithm< L, Compare >& 
      _streamlineTractographyAlgorithm;
    const int16_t& _roiIndex;
    const std::vector< Vector3d< int32_t > >& _sites;
    const VoxelSampler< float >& _voxelSampler;
    OdfContinuousField& _odfContinuousField;
    const Volume< int16_t >& _roiMask;
    const BoundingBox< int32_t >& _roiMaskBoundingBox;
    const Volume< uint8_t >& _computingMask;
    const BoundingBox< int32_t >& _computingMaskBoundingBox;
    const Transform3d< float >& _transform3dFromRoisVoxelToOdfsReal;
    const Transform3d< float >& _transform3dFromRoisVoxelToMaskVoxel;
    const Transform3d< float >& _transform3dFromOdfsRealToMaskVoxel;
    const Transform3d< float >& _transform3dFromOdfsRealToRoisVoxel;
    std::vector< typename BundleMap< L, Compare >::Fiber >& _fibers;
    bool& _verbose;
    const gkg::OrientationSet& _orientationSet;
    float _minimumFiberLength;
    float _maximumFiberLength;
    int32_t _storingIncrement;
    
};


//
// class StreamlineTractographyAlgorithm< L, Compare >
//

template < class L, class Compare = std::less< L > >
class StreamlineTractographyAlgorithm : public TractographyAlgorithm< L,
                                                                      Compare >
{

  public:

    virtual ~StreamlineTractographyAlgorithm();

    float getStep() const;
    int32_t getStoringIncrement() const;
    float getMinimumFiberLength() const;
    float getMaximumFiberLength() const;
    float getApertureAngle() const;

    virtual void track( const SiteMap< L, int32_t, Compare >& rois,
                        const SiteMap< L, int32_t, Compare >& subRois,
                        const VoxelSampler< float >& voxelSampler,
                        OdfContinuousField& odfContinuousField,
                        const SiteMap< int32_t, int32_t,
                                       std::less< int32_t > >& mask,
                        const Transform3d< float >&
                                           transform3dFromRoisVoxelToOdfsReal,
                        const Transform3d< float >&
                                           transform3dFromRoisVoxelToMaskVoxel,
                        const Transform3d< float >&
                                           transform3dFromOdfsRealToMaskVoxel,
                        const Transform3d< float >&
                                           transform3dFromOdfsRealToRoisVoxel,
                        BundleMap< L, Compare >& bundleMap,
                        bool verbose ) const;

  protected:

    friend class ThreadedLoopContext< L, Compare >;

    StreamlineTractographyAlgorithm( const Vector3d< int32_t >& size,
                                     const Vector3d< double >& resolution,
                                     float step,
                                     int32_t storingIncrement,
                                     float minimumFiberLength,
                                     float maximumFiberLength,
                                     float apertureAngle );

    virtual
      bool track( const Vector3d< float >& startingPointOdfs,
                  const int32_t& startingOrientationIndex,
                  const int16_t& startingLabel,
                  OdfContinuousField& odfContinuousField,
                  const Volume< int16_t >& roiMask,
                  const BoundingBox< int32_t >& roiMaskBoundingBox,
                  const Volume< uint8_t >& computingMask,
                  const BoundingBox< int32_t >& computingMaskBoundingBox,
                  const Transform3d< float >&
                                            transform3dFromRoisVoxelToOdfsReal,
                  const Transform3d< float >&
                                            transform3dFromRoisVoxelToMaskVoxel,
                  const Transform3d< float >&
                                            transform3dFromOdfsRealToMaskVoxel,
                  const Transform3d< float >&
                                            transform3dFromOdfsRealToRoisVoxel,
                  std::list< Vector3d< float > >& trajectory,
                  float& fiberLength ) const = 0;

    float _step;
    int32_t _storingIncrement;
    float _minimumFiberLength;
    float _maximumFiberLength;
    float _apertureAngle;

};


}


#endif
