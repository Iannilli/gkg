add_subdirectory( gkg-dmri-plugin-functors )

################################## VTK plugin ##################################
if ( BUILD_VTK_PLUGIN )
  add_subdirectory( gkg-dmri-plugin-vtk )
else ( BUILD_VTK_PLUGIN )
  message( "Warning: gkg-dmri-plugin-vtk not built due to missing VTK dependency" )
endif ( BUILD_VTK_PLUGIN )

