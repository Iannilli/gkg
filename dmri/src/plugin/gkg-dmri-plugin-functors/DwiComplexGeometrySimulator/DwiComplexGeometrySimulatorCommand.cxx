#include <gkg-dmri-plugin-functors/DwiComplexGeometrySimulator/DwiComplexGeometrySimulatorCommand.h>
#include <gkg-dmri-plugin-functors/DwiComplexGeometrySimulator/FiberPopulation.h>
#include <gkg-dmri-plugin-functors/DwiComplexGeometrySimulator/HDF5Writer.h>
#include <gkg-communication-command/CommandFactory.h>
#include <gkg-communication-getopt/Application.h>
#include <gkg-processing-coordinates/Vector2d.h>
#include <gkg-processing-mesh/MeshFactory.h>
#include <gkg-processing-container/MeshMap_i.h>
#include <gkg-processing-container/Volume_i.h>
#include <gkg-processing-mesh/MeshAccumulator_i.h>
#include <gkg-processing-mesh/CylinderCollision3d.h>
#include <gkg-processing-algobase/Math.h>
#include <gkg-processing-transform/Rotation3dFunctions.h>
#include <gkg-core-io/Writer.h>
#include <gkg-core-io/Eraser.h>
#include <gkg-core-exception/Exception.h>

#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>
#include <vector>

#include <gkg-dmri-container/BundleMap_i.h>


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// various static method(s)
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// bool hasReachedTargetIntracellularVolumeFractions()
////////////////////////////////////////////////////////////////////////////////

static bool hasReachedTargetIntracellularVolumeFractions(
 const std::vector< gkg::RCPointer< gkg::FiberPopulation > >& fiberPopulations )
{

  try
  {

    bool result = true;

    std::vector< gkg::RCPointer< gkg::FiberPopulation > >::const_iterator
      f = fiberPopulations.begin(),
      fe = fiberPopulations.end();
    while ( f != fe )
    {

      if ( !( *f )->hasReachedTargetIntracellularVolumeFraction() )
      {

        result = false;
        break;

      }
      ++ f;

    }

    int32_t fiberPopulationIndex = 0;
    f = fiberPopulations.begin();
    while ( f != fe )
    {

      /*std::cout << "fiberPopulation " << fiberPopulationIndex + 1 << " -> "
                << ( *f )->getCurrentIntracellularVolumeFraction()
                << std::endl;*/
      ++ f;
      ++ fiberPopulationIndex;

    }

    return result;

  }
  GKG_CATCH( "static bool hasReachedTargetIntracellularVolumeFractions( "
             "const std::vector< gkg::RCPointer< gkg::FiberPopulation > >& "
             "fiberPopulations )" );

}


////////////////////////////////////////////////////////////////////////////////
// bool hasReachedTargetAngularDispersions()
////////////////////////////////////////////////////////////////////////////////

static bool hasReachedTargetAngularDispersions( 
 const std::vector< gkg::RCPointer< gkg::FiberPopulation > >& fiberPopulations )
{

  try
  {

    bool result = true;

    std::vector< gkg::RCPointer< gkg::FiberPopulation > >::const_iterator
      f = fiberPopulations.begin(),
      fe = fiberPopulations.end();
    while ( f != fe )
    {

      if ( !( *f )->hasReachedTargetAngularDispersion() )
      {

        result = false;
        break;

      }
      ++ f;

    }

    return result;

  }
  GKG_CATCH( "static bool hasReachedTargetAngularDispersions( "
             "const std::vector< gkg::RCPointer< gkg::FiberPopulation > >& "
             "fiberPopulations )" );

}


////////////////////////////////////////////////////////////////////////////////
// bool hasCylinderMembraneIntersection()
////////////////////////////////////////////////////////////////////////////////

static bool hasCylinderMembraneIntersection(
                    const gkg::LightCurve3d< float >& cylinderMembraneCentroid1,
                    float radius1,
                    const gkg::LightCurve3d< float >& cylinderMembraneCentroid2,
                    float radius2,
                    int32_t& intersectingF1,
                    int32_t& intersectingF2 )
{

  try
  {

    bool hasIntersection = false;

    int32_t cylinderMembraneCentroiPointCount1 =
                                      cylinderMembraneCentroid1.getPointCount();
    int32_t cylinderMembraneCentroiPointCount2 =
                                      cylinderMembraneCentroid2.getPointCount();


    gkg::Vector3d< float > center1;
    gkg::Vector3d< float > axe1;
    float halfLength1 = 0.0f;
    gkg::Vector3d< float > center2;
    gkg::Vector3d< float > axe2;
    float halfLength2 = 0.0f;

    int32_t f1 = 0;
    int32_t f2 = 0;
    for ( f1 = 0; f1 < cylinderMembraneCentroiPointCount1 - 1; f1++ )
    {

      const gkg::Vector3d< float >&
        firstExtremity1 = cylinderMembraneCentroid1.getPoint( f1 );
      const gkg::Vector3d< float >&
        secondExtremity1 = cylinderMembraneCentroid1.getPoint( f1 + 1 );

      center1 = ( firstExtremity1 + secondExtremity1 ) * 0.5f;
      axe1 = secondExtremity1 - firstExtremity1;
      halfLength1 = axe1.getNorm() / 2.0f;
      axe1.normalize();

      for ( f2 = 0; f2 < cylinderMembraneCentroiPointCount2 - 1; f2++ )
      {

        const gkg::Vector3d< float >&
          firstExtremity2 = cylinderMembraneCentroid2.getPoint( f2 );
        const gkg::Vector3d< float >&
          secondExtremity2 = cylinderMembraneCentroid2.getPoint( f2 + 1 );

        center2 = ( firstExtremity2 + secondExtremity2 ) * 0.5f;
        axe2 = secondExtremity2 - firstExtremity2;
        halfLength2 = axe2.getNorm() / 2.0f;
        axe2.normalize();

        if ( gkg::hasCylinderCollision3d( center1,
                                          axe1,
                                          halfLength1,
                                          radius1,
                                          center2,
                                          axe2,
                                          halfLength2,
                                          radius2 ) )
        {

          intersectingF1 = f1;
          intersectingF2 = f2;
          hasIntersection = true;
          break;

        }

      }

      if ( hasIntersection )
      {

        break;

      }

    }

    return hasIntersection;

  }
  GKG_CATCH( "static bool hasCylinderMembraneIntersection( "
             "const gkg::LightCurve3d< float >& cylinderMembraneCentroid1, "
             "float radius1, "
             "const gkg::LightCurve3d< float >& cylinderMembraneCentroid2, "
             " float radius2 )" );

}


////////////////////////////////////////////////////////////////////////////////
// bool checkNoIntersection()
////////////////////////////////////////////////////////////////////////////////

static bool checkNoIntersection(
 const gkg::LightCurve3d< float >& modifiedCylinderMembraneCentroid,
 int32_t selectedFiberPopulationIndex,
 int32_t selectedFiberIndex,
 const std::vector< gkg::RCPointer< gkg::FiberPopulation > >& fiberPopulations )
{

  try
  {

    bool hasNoIntersection = true;
    float modifiedOuterMyelinRadius =
      fiberPopulations[ selectedFiberPopulationIndex ]
                       ->getOuterMyelinRadiusProfile( selectedFiberIndex )[ 0 ];

    int32_t fiberPopulationCount = ( int32_t )fiberPopulations.size();
    int32_t otherFiberPopulationIndex = 0;
    for ( otherFiberPopulationIndex = 0;
          otherFiberPopulationIndex < fiberPopulationCount;
          otherFiberPopulationIndex++ )
    {

      const gkg::RCPointer< gkg::FiberPopulation >&
        otherFiberPopulation = fiberPopulations[ selectedFiberPopulationIndex ];

      int32_t fiberCount = otherFiberPopulation->getFiberCount();
      int32_t otherFiberIndex = 0;
      for ( otherFiberIndex = 0; otherFiberIndex < fiberCount;
            otherFiberIndex++ )
      {

        if ( !( ( otherFiberPopulationIndex == selectedFiberPopulationIndex ) &&
                ( otherFiberIndex == selectedFiberIndex ) ) )
        {

          float otherOuterMyelinRadius =
            otherFiberPopulation->getOuterMyelinRadiusProfile( 
                                                         otherFiberIndex )[ 0 ];

          int32_t intersectingF1 = 0;
          int32_t intersectingF2 = 0;
          if ( hasCylinderMembraneIntersection(
                                     modifiedCylinderMembraneCentroid,
                                     modifiedOuterMyelinRadius,
                                     otherFiberPopulation->getFiberCentroid(
                                                              otherFiberIndex ),
                                     otherOuterMyelinRadius,
                                     intersectingF1, intersectingF2 ) )
          {


            std::cout << "outer myelin membranes ( "
                      << selectedFiberPopulationIndex
                      << ", " << selectedFiberIndex << ") and "
                      << "( " << otherFiberPopulationIndex
                      << ", " << otherFiberIndex << ") intersect : "
                      << "f1=" << intersectingF1 << "  f2=" << intersectingF2
                      << std::endl;
            hasNoIntersection = false;
            break;

          }

        }

      }

    }

    return hasNoIntersection;

  }
  GKG_CATCH( "static bool checkNoIntersection( "
             "const gkg::LightCurve3d< float >& "
             "modifiedCylinderMembraneCentroid, "
             "int32_t selectedPopulationIndex, "
             "int32_t selectedFiberIndex, "
             "const std::vector< gkg::RCPointer< gkg::FiberPopulation > >& "
             "fiberPopulations )" );

}


////////////////////////////////////////////////////////////////////////////////
// bool checkNoIntersectionBetweenFibers()
////////////////////////////////////////////////////////////////////////////////

static void checkNoIntersectionBetweenFibers(
 const std::vector< gkg::RCPointer< gkg::FiberPopulation > >& fiberPopulations )
{

  try
  {

    int32_t selectedFiberPopulationIndex = 0;
    int32_t fiberPopulationCount = ( int32_t )fiberPopulations.size();

    for ( selectedFiberPopulationIndex = 0;
          selectedFiberPopulationIndex < fiberPopulationCount;
          selectedFiberPopulationIndex++ )
    {

      const gkg::RCPointer< gkg::FiberPopulation >&
        fiberPopulation = fiberPopulations[ selectedFiberPopulationIndex ];

      int32_t selectedFiberIndex = 0;
      std::vector< gkg::LightCurve3d< float > >::const_iterator
        f = fiberPopulation->getFiberCentroids().begin(),
        fe = fiberPopulation->getFiberCentroids().end();
      while ( f != fe )
      {

        checkNoIntersection( *f, 
                             selectedFiberPopulationIndex,
                             selectedFiberIndex,
                             fiberPopulations );
        ++ f;
        ++ selectedFiberIndex;

      }

    }

  }
  GKG_CATCH( "static void checkNoIntersectionBetweenFibers( "
             "const std::vector< gkg::RCPointer< gkg::FiberPopulation > >& "
             "fiberPopulations )" );

}


#if 0
////////////////////////////////////////////////////////////////////////////////
// bool computeLocalAngularDeviation()
////////////////////////////////////////////////////////////////////////////////

static float computeLocalAngularDeviation( 
             const gkg::LightCurve3d< float >& selectedCylinderMembraneCentroid,
             int32_t lowerCylinderStartingPointIndex,
             int32_t upperCylinderStartingPointIndex,
             const gkg::Vector3d< float >& axis )
{

  try
  {

    float localAngularDeviation = 0.0f;
    int32_t p = 0;
    for ( p = lowerCylinderStartingPointIndex;
          p < upperCylinderStartingPointIndex; p++ )
    {

      const gkg::Vector3d< float >&
        p1 = selectedCylinderMembraneCentroid.getPoint( p );
      const gkg::Vector3d< float >&
        p2 = selectedCylinderMembraneCentroid.getPoint( p + 1 );

      localAngularDeviation += gkg::getLineAngles( p2 - p1, axis );

    }

    return localAngularDeviation;

  }
  GKG_CATCH( "static float computeLocalAngularDeviation( "
             "const gkg::LightCurve3d< float >& "
             "selectedCylinderMembraneCentroid, "
             "int32_t lowerCylinderStartingPointIndex, "
             "int32_t upperCylinderStartingPointIndex, "
             "const gkg::Vector3d< float >& axis )" );

}
#endif


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// class DwiComplexGeometrySimulatorCommand
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

gkg::DwiComplexGeometrySimulatorCommand::DwiComplexGeometrySimulatorCommand(
                                                              int32_t argc,
                                                              char* argv[],
                                                              bool loadPlugin,
                                                              bool removeFirst )
                                        : gkg::Command( argc, argv, 
                                                        loadPlugin,
                                                        removeFirst )
{

  try
  {

    parse();

  }
  GKG_CATCH( "gkg::DwiComplexGeometrySimulatorCommand::"
             "DwiComplexGeometrySimulatorCommand( "
             "int32_t argc, char* argv[], bool loadPlugin, "
             "bool removeFirst )" );

}


gkg::DwiComplexGeometrySimulatorCommand::DwiComplexGeometrySimulatorCommand(
          const std::string& fileNameOut,
          const gkg::BoundingBox< float >& fieldOfView,
          const std::vector< float >& outerMyelinMembraneDiameterMeans,
          const std::vector< float >& outerMyelinMembraneDiameterVariances,
          const std::vector< float >& gRatioMeans,
          const std::vector< float >& gRatioVariances,
          const std::vector< float >& internodalLengthToNodeWidthRatioMeans,
          const std::vector< float >& internodalLengthToNodeWidthRatioVariances,
          const std::vector< float >& interbeadingLengthMeans,
          const std::vector< float >& interbeadingLengthVariances,
          const std::vector< float >& beadingMagnitudeRatioMeans,
          const std::vector< float >& beadingMagnitudeRatioVariances,
          const std::vector< gkg::Vector3d< float > >& meanOrientations,
          const std::vector< float >& targetIntracellularVolumeFractions,
          const std::vector< float >& angularVariations,
          const std::vector< float >& targetAngularDispersions,
          int32_t maximumPopulatingIterationCount,
          float spaceRatio,
          float spaceLambda,
          int32_t maximumAngularDispersionIterationCount,
          float fovExpansionRatio,
          const gkg::Vector3d< int32_t >& lutSize,
          float lutRadiusOfInfluence,
          const std::vector< float >& tortuosityVariations,
          const std::vector< float >& maximumFiberTortuosities,
          int32_t maximumTortuosityIterationCount,
          const std::vector< int32_t >& tortuosityNeighborhoodSizes,
          int32_t crossSectionVertexCount,
          float fiberResolutionAlongAxis,
          bool createAxonalMembranes,
          bool creatingRanvierNodes,
          bool creatingBeadings,
          bool checkIntersections,
          bool creatingMesh,
          const std::string& format,
          bool verbose )
                                        : gkg::Command()
{

  try
  {

    execute( fileNameOut,
             fieldOfView,
             outerMyelinMembraneDiameterMeans,
             outerMyelinMembraneDiameterVariances,
             gRatioMeans,
             gRatioVariances,
             internodalLengthToNodeWidthRatioMeans,
             internodalLengthToNodeWidthRatioVariances,
             interbeadingLengthMeans,
             interbeadingLengthVariances,
             beadingMagnitudeRatioMeans,
             beadingMagnitudeRatioVariances,
             meanOrientations,
             targetIntracellularVolumeFractions,
             angularVariations,
             targetAngularDispersions,
             maximumPopulatingIterationCount,
             spaceRatio,
             spaceLambda,
             maximumAngularDispersionIterationCount,
             fovExpansionRatio,
             lutSize,
             lutRadiusOfInfluence,
             tortuosityVariations,
             maximumFiberTortuosities,
             maximumTortuosityIterationCount,
             tortuosityNeighborhoodSizes,
             crossSectionVertexCount,
             fiberResolutionAlongAxis,
             createAxonalMembranes,
             creatingRanvierNodes,
             creatingBeadings,
             checkIntersections,
             creatingMesh,
             format,
             verbose );

  }
  GKG_CATCH( "gkg::DwiComplexGeometrySimulatorCommand::"
             "DwiComplexGeometrySimulatorCommand( "
             "const std::string& fileNameOut, "
             "const gkg::BoundingBox< float >& fieldOfView, "
             "const std::vector< float >& outerMyelinMembraneDiameterMeans, "
             "const std::vector< float >& "
             "outerMyelinMembraneDiameterVariances, "
             "const std::vector< float >& gRatioMeans, "
             "const std::vector< float >& gRatioVariances, "
             "const std::vector< float >& "
             "internodalLengthToNodeWidthRatioMeans, "
             "const std::vector< float >& "
             "internodalLengthToNodeWidthRatioVariances, "
             "const std::vector< float >& interbeadingLengthMeans, "
             "const std::vector< float >& interbeadingLengthVariances, "
             "const std::vector< float >& beadingMagnitudeRatioMeans, "
             "const std::vector< float >& beadingMagnitudeRatioVariances, "
             "const std::vector< gkg::Vector3d< float > >& meanOrientations, "
             "const std::vector< float >& targetIntracellularVolumeFractions, "
             "const std::vector< float >& angularVariations, "
             "const std::vector< float >& targetAngularDispersions, "
             "int32_t maximumPopulatingIterationCount, "
             "float spaceRatio, "
             "float spaceLambda, "
             "int32_t maximumAngularDispersionIterationCount, "
             "float fovExpansionRatio, "
             "const gkg::Vector3d< int32_t >& lutSize, "
             "float lutRadiusOfInfluence, "
             "const std::vector< float >& tortuosityVariations, "
             "const std::vector< float >& maximumFiberTortuosities, "
             "int32_t maximumTortuosityIterationCount, "
             "const std::vector< int32_t >& tortuosityNeighborhoodSizes, "
             "int32_t crossSectionVertexCount, "
             "float fiberResolutionAlongAxis, "
             "bool creatingAxonalMembrane, "
             "bool creatingRanvierNodes, "
             "bool creatingBeadings, "
             "bool checkIntersections, "
             "bool checkMesh, "
             "const std::string& format, "
             "bool verbose )" );

}


gkg::DwiComplexGeometrySimulatorCommand::DwiComplexGeometrySimulatorCommand(
                                             const gkg::Dictionary& parameters )
                                        : gkg::Command( parameters )
{

  try
  {

    DECLARE_STRING_PARAMETER( parameters, std::string, fileNameOut );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER( parameters, std::vector< float >,
                                           fieldOfViewVector );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER( parameters, std::vector< float >,
                                           outerMyelinMembraneDiameterMeans );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER(
                                         parameters, std::vector< float >,
                                         outerMyelinMembraneDiameterVariances );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER( parameters, std::vector< float >,
                                           gRatioMeans );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER( parameters, std::vector< float >,
                                           gRatioVariances );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER(
                                        parameters, std::vector< float >,
                                        internodalLengthToNodeWidthRatioMeans );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER(
                                    parameters, std::vector< float >,
                                    internodalLengthToNodeWidthRatioVariances );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER( parameters, std::vector< float >,
                                           interbeadingLengthMeans );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER( parameters, std::vector< float >,
                                           interbeadingLengthVariances );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER( parameters, std::vector< float >,
                                           beadingMagnitudeRatioMeans );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER( parameters, std::vector< float >,
                                           beadingMagnitudeRatioVariances );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER( parameters, std::vector< float >,
                                           meanOrientationVector );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER( parameters, std::vector< float >,
                                           targetIntracellularVolumeFractions );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER( parameters, std::vector< float >,
                                           angularVariations );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER( parameters, std::vector< float >,
                                           targetAngularDispersions );
    DECLARE_INTEGER_PARAMETER( parameters, int32_t,
                               maximumPopulatingIterationCount );
    DECLARE_FLOATING_PARAMETER( parameters, float, spaceRatio );
    DECLARE_FLOATING_PARAMETER( parameters, float, spaceLambda );
    DECLARE_INTEGER_PARAMETER( parameters, int32_t,
                               maximumAngularDispersionIterationCount );
    DECLARE_FLOATING_PARAMETER( parameters, float, fovExpansionRatio );
    DECLARE_VECTOR_OF_INTEGERS_PARAMETER( parameters, std::vector< int32_t >,
                                          lutSizeVector );
    DECLARE_FLOATING_PARAMETER( parameters, float, lutRadiusOfInfluence );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER( parameters, std::vector< float >,
                                           tortuosityVariations );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER( parameters, std::vector< float >,
                                           maximumFiberTortuosities );
    DECLARE_INTEGER_PARAMETER( parameters, int32_t, 
                               maximumTortuosityIterationCount );
    DECLARE_VECTOR_OF_INTEGERS_PARAMETER( parameters, std::vector< int32_t >,
                                          tortuosityNeighborhoodSizes );
    DECLARE_INTEGER_PARAMETER( parameters, int32_t, crossSectionVertexCount );
    DECLARE_FLOATING_PARAMETER( parameters, float, fiberResolutionAlongAxis );
    DECLARE_BOOLEAN_PARAMETER( parameters, bool, creatingAxonalMembrane );
    DECLARE_BOOLEAN_PARAMETER( parameters, bool, creatingRanvierNodes );
    DECLARE_BOOLEAN_PARAMETER( parameters, bool, creatingBeadings );
    DECLARE_BOOLEAN_PARAMETER( parameters, bool, checkIntersections );
    DECLARE_BOOLEAN_PARAMETER( parameters, bool, creatingMesh );
    DECLARE_STRING_PARAMETER( parameters, std::string, format );
    DECLARE_BOOLEAN_PARAMETER( parameters, bool, verbose );

    gkg::BoundingBox< float > fieldOfView( fieldOfViewVector[ 0 ],
                                           fieldOfViewVector[ 1 ],
                                           fieldOfViewVector[ 2 ],
                                           fieldOfViewVector[ 3 ],
                                           fieldOfViewVector[ 4 ],
                                           fieldOfViewVector[ 5 ] );

    int32_t populationCount = 
                             ( int32_t )outerMyelinMembraneDiameterMeans.size();
    std::vector< gkg::Vector3d< float > > meanOrientations( populationCount );

    int32_t p = 0;
    for ( p = 0; p < populationCount; p++ )
    {
 
       
       meanOrientations[ p ] = gkg::Vector3d< float >(
                                           meanOrientationVector[ 3 * p + 0 ],
                                           meanOrientationVector[ 3 * p + 1 ],
                                           meanOrientationVector[ 3 * p + 2 ] );

    }

    gkg::Vector3d< int32_t > lutSize( lutSizeVector[ 0 ],
                                      lutSizeVector[ 1 ],
                                      lutSizeVector[ 2 ] );
    
    execute( fileNameOut,
             fieldOfView,
             outerMyelinMembraneDiameterMeans,
             outerMyelinMembraneDiameterVariances,
             gRatioMeans,
             gRatioVariances,
             internodalLengthToNodeWidthRatioMeans,
             internodalLengthToNodeWidthRatioVariances,
             interbeadingLengthMeans,
             interbeadingLengthVariances,
             beadingMagnitudeRatioMeans,
             beadingMagnitudeRatioVariances,
             meanOrientations,
             targetIntracellularVolumeFractions,
             angularVariations,
             targetAngularDispersions,
             maximumPopulatingIterationCount,
             spaceRatio,
             spaceLambda,
             maximumAngularDispersionIterationCount,
             fovExpansionRatio,
             lutSize,
             lutRadiusOfInfluence,
             tortuosityVariations,
             maximumFiberTortuosities,
             maximumTortuosityIterationCount,
             tortuosityNeighborhoodSizes,
             crossSectionVertexCount,
             fiberResolutionAlongAxis,
             creatingAxonalMembrane,
             creatingRanvierNodes,
             creatingBeadings,
             checkIntersections,
             creatingMesh,
             format,
             verbose );

  }
  GKG_CATCH( "gkg::DwiComplexGeometrySimulatorCommand::"
             "DwiComplexGeometrySimulatorCommand( "
             "const gkg::Dictionary& parameters )" );

}


gkg::DwiComplexGeometrySimulatorCommand::~DwiComplexGeometrySimulatorCommand()
{
}


std::string gkg::DwiComplexGeometrySimulatorCommand::getStaticName()
{

  try
  {

    return "DwiComplexGeometrySimulator";

  }
  GKG_CATCH( "std::string "
             "gkg::DwiComplexGeometrySimulatorCommand::getStaticName()" );

}


void gkg::DwiComplexGeometrySimulatorCommand::addSegmentToLut(
  const gkg::BoundingBox< float >& extendedFieldOfView,
  int32_t currentFiberPopulationIndex,
  int32_t currentFiberCentroidIndex,
  const gkg::Vector3d< float >& p1,
  int32_t currentPointIndex1,
  const gkg::Vector3d< float >& p2,
  int32_t currentPointIndex2,
  const gkg::Vector3d< float >& lutResolution,
  float minimumLutResolution,
  const gkg::Vector3d< int32_t >& voxelRadiusOfInfluence,
  const gkg::BoundingBox< int32_t >& voxelBoundingBox,
  gkg::Volume< std::map< std::pair< int32_t, int32_t >,
                         std::set< int32_t > > >& lut )
{

  try
  {

    gkg::Vector3d< float > segment = p2 - p1;
    float segmentLength = segment.getNorm();
    int32_t pointCount = ( int32_t )( segmentLength /
                                      minimumLutResolution ) + 2;

    gkg::Vector3d< int32_t > currentVoxel;
    gkg::Vector3d< int32_t > neighborVoxel;
    gkg::Vector3d< float > point;
    int32_t p = 0;
    int32_t nx = 0;
    int32_t ny = 0;
    int32_t nz = 0;
    for ( p = 0; p < pointCount; p++ )
    {

      point.x = p1.x + segment.x * ( ( float )p / ( float )pointCount );
      point.y = p1.y + segment.y * ( ( float )p / ( float )pointCount );
      point.z = p1.z + segment.z * ( ( float )p / ( float )pointCount );

      currentVoxel.x = 
                    ( int32_t )( ( point.x - extendedFieldOfView.getLowerX() ) /
                                 lutResolution.x );
      currentVoxel.y = 
                    ( int32_t )( ( point.y - extendedFieldOfView.getLowerY() ) /
                                 lutResolution.y );
      currentVoxel.z =
                    ( int32_t )( ( point.z - extendedFieldOfView.getLowerZ() ) /
                                 lutResolution.z );
             

      for ( nz = -voxelRadiusOfInfluence.z; nz <= voxelRadiusOfInfluence.z;
            nz++ )
      {

        for ( ny = -voxelRadiusOfInfluence.y; ny <= voxelRadiusOfInfluence.y;
              ny++ )
        {

          for ( nx = -voxelRadiusOfInfluence.x;  nx <= voxelRadiusOfInfluence.x;
                nx++ )
          {

            neighborVoxel.x = currentVoxel.x + nx;
            neighborVoxel.y = currentVoxel.y + ny;
            neighborVoxel.z = currentVoxel.z + nz;

            if ( voxelBoundingBox.contains( neighborVoxel ) )
            {

              if ( p < pointCount / 2 )
              {

                lut( neighborVoxel )[ std::pair< int32_t, int32_t >(
                                           currentFiberPopulationIndex,
                                           currentFiberCentroidIndex ) ].insert(
                                                           currentPointIndex1 );

              }
              else
              {

                lut( neighborVoxel )[ std::pair< int32_t, int32_t >(
                                           currentFiberPopulationIndex,
                                           currentFiberCentroidIndex ) ].insert(
                                                           currentPointIndex2 );

              }

            }

          }

        }

      }

    }

  }
  GKG_CATCH( "void gkg::DwiComplexGeometrySimulatorCommand::addSegmentToLut( "
             "const gkg::BoundingBox< float >& extendedFieldOfView, "
             "int32_t currentFiberPopulationIndex, "
             "int32_t currentFiberCentroidIndex, "
             "const gkg::Vector3d< float >& p1, "
             "int32_t currentPointIndex1, " 
             "const gkg::Vector3d< float >& p2, "
             "int32_t currentPointIndex2, "
             "const gkg::Vector3d< float >& lutResolution, "
             "float minimumLutResolution, "
             "const gkg::Vector3d< int32_t >& voxelRadiusOfInfluence, "
             "const gkg::BoundingBox< int32_t >& voxelBoundingBox, "
             "gkg::Volume< std::map< std::pair< int32_t, int32_t >, "
             "std::set< int32_t > > >& lut )" );

}


void gkg::DwiComplexGeometrySimulatorCommand::removeSegmentFromLut(
  const gkg::BoundingBox< float >& extendedFieldOfView,
  int32_t currentFiberPopulationIndex,
  int32_t currentFiberCentroidIndex,
  const gkg::Vector3d< float >& p1,
  int32_t currentPointIndex1,
  const gkg::Vector3d< float >& p2,
  int32_t currentPointIndex2,
  const gkg::Vector3d< float >& lutResolution,
  float minimumLutResolution,
  const gkg::Vector3d< int32_t >& voxelRadiusOfInfluence,
  const gkg::BoundingBox< int32_t >& voxelBoundingBox,
  gkg::Volume< std::map< std::pair< int32_t, int32_t >,
                         std::set< int32_t > > >& lut )
{

  try
  {

    gkg::Vector3d< float > segment = p2 - p1;
    float segmentLength = segment.getNorm();
    int32_t pointCount = ( int32_t )( segmentLength /
                                      minimumLutResolution ) + 2;

    gkg::Vector3d< int32_t > currentVoxel;
    gkg::Vector3d< int32_t > neighborVoxel;
    gkg::Vector3d< float > point;
    int32_t p = 0;
    int32_t nx = 0;
    int32_t ny = 0;
    int32_t nz = 0;
    for ( p = 0; p < pointCount; p++ )
    {

      point.x = p1.x + segment.x * ( ( float )p / ( float )pointCount );
      point.y = p1.y + segment.y * ( ( float )p / ( float )pointCount );
      point.z = p1.z + segment.z * ( ( float )p / ( float )pointCount );

      currentVoxel.x = 
                    ( int32_t )( ( point.x - extendedFieldOfView.getLowerX() ) /
                                 lutResolution.x );
      currentVoxel.y = 
                    ( int32_t )( ( point.y - extendedFieldOfView.getLowerY() ) /
                                 lutResolution.y );
      currentVoxel.z =
                    ( int32_t )( ( point.z - extendedFieldOfView.getLowerZ() ) /
                                 lutResolution.z );
             

      for ( nz = -voxelRadiusOfInfluence.z; nz <= voxelRadiusOfInfluence.z;
            nz++ )
      {

        for ( ny = -voxelRadiusOfInfluence.y; ny <= voxelRadiusOfInfluence.y;
              ny++ )
        {

          for ( nx = -voxelRadiusOfInfluence.x;  nx <= voxelRadiusOfInfluence.x;
                nx++ )
          {

            neighborVoxel.x = currentVoxel.x + nx;
            neighborVoxel.y = currentVoxel.y + ny;
            neighborVoxel.z = currentVoxel.z + nz;

            if ( voxelBoundingBox.contains( neighborVoxel ) )
            {

              std::pair< int32_t, int32_t > 
                currentPopulationAndFiberCentroidIndex(
                                                    currentFiberPopulationIndex,
                                                    currentFiberCentroidIndex );
              std::set< int32_t >& 
                         currentSet = lut( neighborVoxel )[ 
                                       currentPopulationAndFiberCentroidIndex ];
              if ( p < pointCount / 2 )
              {

                if ( currentSet.find( currentPointIndex1 ) !=
                     currentSet.end() )
                {

                  currentSet.erase( currentPointIndex1 );

                  if ( currentSet.empty() )
                  {

                    lut( neighborVoxel ).erase(
                                       currentPopulationAndFiberCentroidIndex );

                  }

                }

              }
              else
              {

                if ( currentSet.find( currentPointIndex2 ) !=
                     currentSet.end() )
                {

                  currentSet.erase( currentPointIndex2 );
                  if ( currentSet.empty() )
                  {

                    lut( neighborVoxel ).erase(
                                       currentPopulationAndFiberCentroidIndex );

                  }

                }

              }

            }

          }

        }

      }

    }

  }
  GKG_CATCH( "void gkg::DwiComplexGeometrySimulatorCommand::"
             "removeSegmentFromLut( "
             "const gkg::BoundingBox< float >& extendedFieldOfView, "
             "int32_t currentFiberPopulationIndex, "
             "int32_t currentFiberCentroidIndex, "
             "const gkg::Vector3d< float >& p1, "
             "int32_t currentPointIndex1, " 
             "const gkg::Vector3d< float >& p2, "
             "int32_t currentPointIndex2, "
             "const gkg::Vector3d< float >& lutResolution, "
             "float minimumLutResolution, "
             "const gkg::Vector3d< int32_t >& voxelRadiusOfInfluence, "
             "const gkg::BoundingBox< int32_t >& voxelBoundingBox, "
             "gkg::Volume< std::map< std::pair< int32_t, int32_t >, "
             "std::set< int32_t > > >& lut )" );

}


void gkg::DwiComplexGeometrySimulatorCommand::parse()
{

  try
  {

    std::string fileNameOut;
    gkg::BoundingBox< float > fieldOfView;
    std::vector< float > fieldOfViewVector;
    std::vector< float > outerMyelinMembraneDiameterMeans;
    std::vector< float > outerMyelinMembraneDiameterVariances;
    std::vector< float > gRatioMeans;
    std::vector< float > gRatioVariances;
    std::vector< float > internodalLengthToNodeWidthRatioMeans;
    std::vector< float > internodalLengthToNodeWidthRatioVariances;
    std::vector< float > interbeadingLengthMeans;
    std::vector< float > interbeadingLengthVariances;
    std::vector< float > beadingMagnitudeRatioMeans;
    std::vector< float > beadingMagnitudeRatioVariances;
    std::vector< float > meanOrientationVector;
    std::vector< float > targetIntracellularVolumeFractions;
    std::vector< float > angularVariations;
    std::vector< float > targetAngularDispersions;
    int32_t maximumPopulatingIterationCount = 1000000;
    float spaceRatio = 4.0;
    float spaceLambda = 1.0;
    int32_t maximumAngularDispersionIterationCount = 1000000;
    float fovExpansionRatio = 1.05;
    gkg::Vector3d< int32_t > lutSize( 100, 100, 100 );
    std::vector< int32_t > lutSizeVector;
    float lutRadiusOfInfluence = 0.0;
    std::vector< float > tortuosityVariations;
    std::vector< float > maximumFiberTortuosities;
    int32_t maximumTortuosityIterationCount = 1000000;
    std::vector< int32_t > tortuosityNeighborhoodSizes;
    int32_t crossSectionVertexCount = 36;
    float fiberResolutionAlongAxis = 0.1;  // in um
    bool creatingAxonalMembrane = true;
    bool creatingRanvierNodes = true;
    bool creatingBeadings = true;
    bool checkIntersections = false;
    bool creatingMesh = true;
    std::string format = "membrane";
    bool verbose = false;


    ////////////////////////////////////////////////////////////////////////////
    // managing command lines argument(s)
    ////////////////////////////////////////////////////////////////////////////

    gkg::Application application( _argc, _argv,
                                  "Diffusion geometry simulator",
                                  _loadPlugin );

    application.addSingleOption( "-o",
                                 "Output base filename (no extension provided)",
                                 fileNameOut );
    application.addSeriesOption( "-fieldOfView",
                                 "Field of view",
                                 fieldOfViewVector,
                                 6, 6 );
    application.addSeriesOption( "-outerMyelinMembraneDiameterMeans",
                                 "List of outer myelin membrane diameter means"
                                 " in m",
                                 outerMyelinMembraneDiameterMeans, 1 );
    application.addSeriesOption( "-outerMyelinMembraneDiameterVariances",
                                 "List of outer myelin membrane diameter"
                                 " variances in m^2",
                                 outerMyelinMembraneDiameterVariances, 1 );
    application.addSeriesOption( "-gRatioMeans",
                                 "List of G-ratio means",
                                 gRatioMeans, 1 );
    application.addSeriesOption( "-gRatioVariances",
                                 "List of G-ratio variances",
                                 gRatioVariances, 1 );
    application.addSeriesOption( "-internodalLengthToNodeWidthRatioMeans",
                                 "List of internodal length to node width ratio"
                                 " means",
                                 internodalLengthToNodeWidthRatioMeans, 1 );
    application.addSeriesOption( "-internodalLengthToNodeWidthRatioVariances",
                                 "List of internodal length to node width ratio"
                                 " variances",
                                 internodalLengthToNodeWidthRatioVariances, 1 );
    application.addSeriesOption( "-interbeadingLengthMeans",
                                 "List of interbeading length means",
                                 interbeadingLengthMeans, 1 );
    application.addSeriesOption( "-interbeadingLengthVariances",
                                 "List of interbeading length variances",
                                 interbeadingLengthVariances, 1 );
    application.addSeriesOption( "-beadingMagnitudeRatioMeans",
                                 "List of beading magnitude ratio means",
                                 beadingMagnitudeRatioMeans, 1 );
    application.addSeriesOption( "-beadingMagnitudeRatioVariances",
                                 "List of beading magnitude ratio variances",
                                 beadingMagnitudeRatioVariances, 1 );
    application.addSeriesOption( "-meanOrientations",
                                 "Mean orientation list",
                                 meanOrientationVector,
                                 3 );
    application.addSeriesOption( "-targetIntracellularVolumeFractions",
                                 "Intracellular volume fraction list",
                                 targetIntracellularVolumeFractions, 1 );
    application.addSeriesOption( "-angularVariations",
                                 "Angular variation list in tangent "
                                 "coordinates",
                                 angularVariations,
                                 1 );
    application.addSeriesOption( "-targetAngularDispersions",
                                 "Target angular dispersion list",
                                 targetAngularDispersions, 1 );
    application.addSingleOption( "-maximumPopulatingIterationCount",
                                 "Maximum populating iteration count "
                                 "(default=1000000)",
                                 maximumPopulatingIterationCount,
                                 true );
    application.addSingleOption( "-spaceRatio",
                                 "Number of fibers from other population "
                                 "between two fibers of a same population "
                                 "(default=2.0)\n",
                                 spaceRatio,
                                 true );
    application.addSingleOption( "-spaceLambda",
                                 "Space ratio is decreasing according to "
                                 "iterations following the rule "
                                 "exp( -spaceLambda * iteration / "
                                 "maximumPopulatingIterationCount ) "
                                 "(default=1.0)\n",
                                 spaceLambda,
                                 true );
    application.addSingleOption( "-maximumAngularDispersionIterationCount",
                                 "Maximum angular dispersion iteration count "
                                 "(default=1000000)",
                                 maximumAngularDispersionIterationCount,
                                 true );
    application.addSingleOption( "-fovExpansionRatio",
                                 "Field-of-view expansion ratio (default=1.05)",
                                 fovExpansionRatio,
                                 true );
    application.addSeriesOption( "-lutSize",
                                 "Lut size (default=(100,100,100))",
                                 lutSizeVector,
                                 3, 0 );
    application.addSingleOption( "-lutRadiusOfInfluence",
                                 "LUT radius of influence in mm",
                                 lutRadiusOfInfluence );
    application.addSeriesOption( "-tortuosityVariations",
                                 "Tortuosity displacement variation list in mm "
                                 "(default=0.005mm)",
                                 tortuosityVariations, 1 );
    application.addSeriesOption( "-maximumFiberTortuosities",
                                 "Maximum fiber tortuosity list",
                                 maximumFiberTortuosities, 1 );
    application.addSingleOption( "-maximumTortuosityIterationCount",
                                 "Maximum angular dispersion iteration count "
                                 "(default=1000000)",
                                 maximumTortuosityIterationCount,
                                 true );
    application.addSeriesOption( "-tortuosityNeighborhoodSizes",
                                 "Tortuosity neighborhood size list",
                                 tortuosityNeighborhoodSizes,
                                 1 );
    application.addSingleOption( "-crossSectionVertexCount",
                                 "Cross section vertex count (default=36)",
                                 crossSectionVertexCount,
                                 true );
    application.addSingleOption( "-fiberResolutionAlongAxis",
                                 "Fiber resolution along axis",
                                 fiberResolutionAlongAxis,
                                 true );
    application.addSingleOption( "-checkIntersections",
                                 "Check intersections (default=false)",
                                 checkIntersections,
                                 true );
    application.addSingleOption( "-creatingAxonalMembrane",
                                 "Create axonal membrane (default=true)",
                                 creatingAxonalMembrane,
                                 true );
    application.addSingleOption( "-creatingRanvierNodes",
                                 "Create Ranvier nodes (default=true)",
                                 creatingRanvierNodes,
                                 true );
    application.addSingleOption( "-creatingBeadings",
                                 "Create beadings (default=true)",
                                 creatingBeadings,
                                 true );
    application.addSingleOption( "-creatingMesh",
                                 "Create mesh (default=true)",
                                 creatingMesh,
                                 true );
    application.addSingleOption( "-format",
                                 "Output format (default=membrane)\n"
                                "\t- membrane\n\t- hdf5",
                                 format,
                                 true );
    application.addSingleOption( "-verbose",
                                 "Show as much information as possible",
                                 verbose,
                                 true );

    ////// launching parser
    application.initialize();

    fieldOfView.setLowerX( fieldOfViewVector[ 0 ] );
    fieldOfView.setUpperX( fieldOfViewVector[ 1 ] );
    fieldOfView.setLowerY( fieldOfViewVector[ 2 ] );
    fieldOfView.setUpperY( fieldOfViewVector[ 3 ] );
    fieldOfView.setLowerZ( fieldOfViewVector[ 4 ] );
    fieldOfView.setUpperZ( fieldOfViewVector[ 5 ] );


    int32_t populationCount =
                             ( int32_t )outerMyelinMembraneDiameterMeans.size();
    std::vector< gkg::Vector3d< float > > meanOrientations( populationCount );

    int32_t p = 0;
    for ( p = 0; p < populationCount; p++ )
    {
 
       
       meanOrientations[ p ] = gkg::Vector3d< float >(
                                           meanOrientationVector[ 3 * p + 0 ],
                                           meanOrientationVector[ 3 * p + 1 ],
                                           meanOrientationVector[ 3 * p + 2 ] );

    }

    lutSize.x = lutSizeVector[ 0 ];
    lutSize.y = lutSizeVector[ 1 ];
    lutSize.z = lutSizeVector[ 2 ];

    execute( fileNameOut,
             fieldOfView,
             outerMyelinMembraneDiameterMeans,
             outerMyelinMembraneDiameterVariances,
             gRatioMeans,
             gRatioVariances,
             internodalLengthToNodeWidthRatioMeans,
             internodalLengthToNodeWidthRatioVariances,
             interbeadingLengthMeans,
             interbeadingLengthVariances,
             beadingMagnitudeRatioMeans,
             beadingMagnitudeRatioVariances,
             meanOrientations,
             targetIntracellularVolumeFractions,
             angularVariations,
             targetAngularDispersions,
             maximumPopulatingIterationCount,
             spaceRatio,
             spaceLambda,
             maximumAngularDispersionIterationCount,
             fovExpansionRatio,
             lutSize,
             lutRadiusOfInfluence,
             tortuosityVariations,
             maximumFiberTortuosities,
             maximumTortuosityIterationCount,
             tortuosityNeighborhoodSizes,
             crossSectionVertexCount,
             fiberResolutionAlongAxis,
             creatingAxonalMembrane,
             creatingRanvierNodes,
             creatingBeadings,
             checkIntersections,
             creatingMesh,
             format,
             verbose );

  }
  GKG_CATCH_FUNCTOR( "void gkg::DwiComplexGeometrySimulatorCommand::parse()" );

}


void gkg::DwiComplexGeometrySimulatorCommand::execute(
          const std::string& fileNameOut,
          const gkg::BoundingBox< float >& fieldOfView,
          const std::vector< float >& outerMyelinMembraneDiameterMeans,
          const std::vector< float >& outerMyelinMembraneDiameterVariances,
          const std::vector< float >& gRatioMeans,
          const std::vector< float >& gRatioVariances,
          const std::vector< float >& internodalLengthToNodeWidthRatioMeans,
          const std::vector< float >& internodalLengthToNodeWidthRatioVariances,
          const std::vector< float >& interbeadingLengthMeans,
          const std::vector< float >& interbeadingLengthVariances,
          const std::vector< float >& beadingMagnitudeRatioMeans,
          const std::vector< float >& beadingMagnitudeRatioVariances,
          const std::vector< gkg::Vector3d< float > >& meanOrientations,
          const std::vector< float >& targetIntracellularVolumeFractions,
          const std::vector< float >& angularVariations,
          const std::vector< float >& targetAngularDispersions,
          int32_t maximumPopulatingIterationCount,
          float spaceRatio,
          float spaceLambda,
          int32_t maximumAngularDispersionIterationCount,
          float fovExpansionRatio,
          const gkg::Vector3d< int32_t >& lutSize,
          float lutRadiusOfInfluence,
          const std::vector< float >& tortuosityVariations,
          const std::vector< float >& maximumFiberTortuosities,
          int32_t maximumTortuosityIterationCount,
          const std::vector< int32_t >& tortuosityNeighborhoodSizes,
          int32_t crossSectionVertexCount,
          float fiberResolutionAlongAxis,
          bool creatingAxonalMembrane,
          bool creatingRanvierNodes,
          bool creatingBeadings,
          bool checkIntersections,
          bool creatingMesh,
          const std::string& format,
          bool verbose )
{

  try
  {


    ////////////////////////////////////////////////////////////////////////////
    // collecting fiber population count
    ////////////////////////////////////////////////////////////////////////////

    int32_t fiberPopulationCount = 
                             ( int32_t )outerMyelinMembraneDiameterMeans.size();

    // sanity check(s)
    if ( ( ( int32_t )outerMyelinMembraneDiameterVariances.size() !=
           fiberPopulationCount ) ||
         ( ( int32_t )gRatioMeans.size() !=
           fiberPopulationCount ) ||
         ( ( int32_t )gRatioVariances.size() !=
           fiberPopulationCount ) ||
         ( ( int32_t )internodalLengthToNodeWidthRatioMeans.size() !=
           fiberPopulationCount ) ||
         ( ( int32_t )internodalLengthToNodeWidthRatioVariances.size() !=
           fiberPopulationCount ) ||
         ( ( int32_t )interbeadingLengthMeans.size() !=
           fiberPopulationCount ) ||
         ( ( int32_t )interbeadingLengthVariances.size() !=
           fiberPopulationCount ) ||
         ( ( int32_t )beadingMagnitudeRatioMeans.size() !=
           fiberPopulationCount ) ||
         ( ( int32_t )beadingMagnitudeRatioVariances.size() !=
           fiberPopulationCount ) ||
         ( ( int32_t )meanOrientations.size() != fiberPopulationCount ) ||
         ( ( int32_t )targetIntracellularVolumeFractions.size() != 
           fiberPopulationCount ) ||
         ( ( int32_t )angularVariations.size() != fiberPopulationCount ) ||
         ( ( int32_t )targetAngularDispersions.size() !=
           fiberPopulationCount ) ||
         ( ( int32_t )tortuosityVariations.size() != fiberPopulationCount ) ||
         ( ( int32_t )maximumFiberTortuosities.size() !=
           fiberPopulationCount ) ||
         ( ( int32_t )tortuosityNeighborhoodSizes.size() != 
           fiberPopulationCount ) )
    {

      throw std::runtime_error( "incoherent size between input vector(s)" );

    }



    ////////////////////////////////////////////////////////////////////////////
    // getting pointer to the numerical analysis factory
    ////////////////////////////////////////////////////////////////////////////

    gkg::NumericalAnalysisImplementationFactory* factory =
     gkg::NumericalAnalysisSelector::getInstance().getImplementationFactory();

    gkg::RCPointer< gkg::RandomGenerator > randomGenerator(
                         new gkg::RandomGenerator( gkg::RandomGenerator::Taus ) );


    ////////////////////////////////////////////////////////////////////////////
    // computing the extended field of view
    ////////////////////////////////////////////////////////////////////////////

    gkg::BoundingBox< float >
      enlargedFieldOfView( fieldOfView.getLowerX() * std::sqrt( 3.0 ),
                           fieldOfView.getUpperX() * std::sqrt( 3.0 ),
                           fieldOfView.getLowerY() * std::sqrt( 3.0 ),
                           fieldOfView.getUpperY() * std::sqrt( 3.0 ),
                           fieldOfView.getLowerZ() * std::sqrt( 3.0 ),
                           fieldOfView.getUpperZ() * std::sqrt( 3.0 ) );

    gkg::BoundingBox< float >
      extendedFieldOfView( fieldOfView.getLowerX() * std::sqrt( 3.0 ) *
                           fovExpansionRatio,
                           fieldOfView.getUpperX() * std::sqrt( 3.0 ) *
                           fovExpansionRatio,
                           fieldOfView.getLowerY() * std::sqrt( 3.0 ) *
                           fovExpansionRatio,
                           fieldOfView.getUpperY() * std::sqrt( 3.0 ) *
                           fovExpansionRatio,
                           fieldOfView.getLowerZ() * std::sqrt( 3.0 ) *
                           fovExpansionRatio,
                           fieldOfView.getUpperZ() * std::sqrt( 3.0 ) *
                           fovExpansionRatio );
    
    ////////////////////////////////////////////////////////////////////////////
    // creating the fiber population(s)
    ////////////////////////////////////////////////////////////////////////////


    if ( verbose )
    {

      std::cout << "creating fiber population(s) : " << std::flush;

    }

    std::vector< gkg::RCPointer< gkg::FiberPopulation > >
      fiberPopulations( fiberPopulationCount );

    int32_t fiberPopulationIndex = 0;
    for ( fiberPopulationIndex = 0; fiberPopulationIndex < fiberPopulationCount;
          fiberPopulationIndex ++ )
    {

      fiberPopulations[ fiberPopulationIndex ].reset(
        new gkg::FiberPopulation(
              enlargedFieldOfView,
              extendedFieldOfView,
              outerMyelinMembraneDiameterMeans[ fiberPopulationIndex ],
              outerMyelinMembraneDiameterVariances[ fiberPopulationIndex ],
              gRatioMeans[ fiberPopulationIndex ],
              gRatioVariances[ fiberPopulationIndex ],
              internodalLengthToNodeWidthRatioMeans[ fiberPopulationIndex ],
              internodalLengthToNodeWidthRatioVariances[ fiberPopulationIndex ],
              interbeadingLengthMeans[ fiberPopulationIndex ],
              interbeadingLengthVariances[ fiberPopulationIndex ],
              beadingMagnitudeRatioMeans[ fiberPopulationIndex ],
              beadingMagnitudeRatioVariances[ fiberPopulationIndex ],
              meanOrientations[ fiberPopulationIndex ],
              targetIntracellularVolumeFractions[ fiberPopulationIndex ],
              angularVariations[ fiberPopulationIndex ],
              targetAngularDispersions[ fiberPopulationIndex ],
              tortuosityVariations[ fiberPopulationIndex ],
              maximumFiberTortuosities[ fiberPopulationIndex ],
              tortuosityNeighborhoodSizes[ fiberPopulationIndex ],
              fiberResolutionAlongAxis,
              crossSectionVertexCount,
              randomGenerator ) );

    }

    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }


    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    // generating populations of rectilinear cylinders with orientation and
    // radii distributions following the Gamma distributions specified for each
    // population, and such that the intracellular volume fractions match the 
    // prescribed ones
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    gkg::Vector3d< float > currentPosition;
    float currentOuterMyelinDiameter = 0.0f;
    int32_t currentIterationIndex = 0;
    while ( !hasReachedTargetIntracellularVolumeFractions( fiberPopulations ) &&
            ( currentIterationIndex < maximumPopulatingIterationCount ) )
    {

      // selecting a random fiber population
      uint32_t selectedFiberPopulationIndex = 0U;
      if ( fiberPopulationCount > 1 )
      {

        selectedFiberPopulationIndex =
                  factory->getUniformRandomUInt32(
                                         *randomGenerator,
                                         ( uint32_t )( fiberPopulationCount ) );

      }

      gkg::RCPointer< gkg::FiberPopulation >&
             fiberPopulation = fiberPopulations[ selectedFiberPopulationIndex ];

      // we propose the creation of a new fiber only if the target intracellular
      // volume fraction of the selection fiber population is not reached yet
      if ( !fiberPopulation->hasReachedTargetIntracellularVolumeFraction() )
      {

        currentOuterMyelinDiameter = 
                        fiberPopulation->getRandomOuterMyelinMembraneDiameter();

        currentPosition.x = factory->getUniformRandomNumber(
           *randomGenerator,
           enlargedFieldOfView.getLowerX() + currentOuterMyelinDiameter / 2.0,
           enlargedFieldOfView.getUpperX() - currentOuterMyelinDiameter / 2.0 );
        currentPosition.y = factory->getUniformRandomNumber(
           *randomGenerator,
           enlargedFieldOfView.getLowerY() + currentOuterMyelinDiameter / 2.0,
           enlargedFieldOfView.getUpperY() - currentOuterMyelinDiameter / 2.0 );
        currentPosition.z = ( enlargedFieldOfView.getLowerZ() +
                              enlargedFieldOfView.getUpperZ() ) / 2.0;

        const gkg::Vector3d< float >& 
                            currentAxis = fiberPopulation->getMeanOrientation();

        if ( hasEnoughSpace( currentPosition,
                             currentOuterMyelinDiameter,
                             currentAxis,
                             fiberPopulations,
                             currentIterationIndex,
                             maximumPopulatingIterationCount,
                             spaceRatio,
                             spaceLambda ) )
        {

          fiberPopulation->addFiber( currentPosition,
                                     currentOuterMyelinDiameter );

        }

      }
      ++ currentIterationIndex;

    }

    if ( verbose )
    {

      for ( fiberPopulationIndex = 0;
            fiberPopulationIndex < fiberPopulationCount;
            fiberPopulationIndex ++ )
      {

        std::cout << "fiber count in population "
                  << fiberPopulationIndex + 1 
                  << " : " << fiberPopulations[ fiberPopulationIndex ]->
                                                                 getFiberCount()
                  << " fibers"
                  << std::endl;

      }

    }


    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    // creating initial rectilinear full resolution fibers for all populations
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

      std::cout << "creating initial list of fibers : " << std::flush;

    }

    for ( fiberPopulationIndex = 0;
          fiberPopulationIndex < fiberPopulationCount;
          fiberPopulationIndex ++ )
    {

      fiberPopulations[ fiberPopulationIndex ]->setFullResolutionFibers();

    }


    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }


    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    // inducing global angular dispersion
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

      std::cout << "inducing global angular dispersion : " << std::flush;

    }

    currentIterationIndex = 0;
    while ( !hasReachedTargetAngularDispersions( fiberPopulations ) &&
            ( currentIterationIndex < maximumAngularDispersionIterationCount ) )
    {

      // selecting a random fiber population
      uint32_t selectedFiberPopulationIndex = 0U;
      if ( fiberPopulationCount > 1 )
      {

        selectedFiberPopulationIndex =
            factory->getUniformRandomUInt32( *randomGenerator,
                                             ( uint32_t )fiberPopulationCount );

      }

      gkg::RCPointer< gkg::FiberPopulation >&
             fiberPopulation = fiberPopulations[ selectedFiberPopulationIndex ];

      if ( !fiberPopulation->hasReachedTargetAngularDispersion() )
      {

        fiberPopulation->induceGlobalDispersionOfARandomFiber(
                                                             fiberPopulations );
      }

      ++ currentIterationIndex;

    }


    if ( verbose )
    {

      std::cout << "done" << std::endl;
      for ( fiberPopulationIndex = 0;
            fiberPopulationIndex < fiberPopulationCount;
            fiberPopulationIndex ++ )
      {

        std::cout << "current angular global dispersion reached after "
                  << currentIterationIndex
                  << " iterations for population " 
                  << fiberPopulationIndex + 1 
                  << " : "
                  << fiberPopulations[ fiberPopulationIndex ]->
                                                   getCurrentAngularDispersion()
                  << " degrees (target="
                  << fiberPopulations[ fiberPopulationIndex ]->
                                                   getTargetAngularDispersion()
                  << " degrees)"
                  << std::endl;

      }

    }


    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    // creating look up table to speed-up computation
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    gkg::Vector3d< float > lutResolution( 
         ( extendedFieldOfView.getUpperX() - extendedFieldOfView.getLowerX() ) /
         ( float )lutSize.x,
         ( extendedFieldOfView.getUpperY() - extendedFieldOfView.getLowerY() ) /
         ( float )lutSize.y,
         ( extendedFieldOfView.getUpperZ() - extendedFieldOfView.getLowerZ() ) /
         ( float )lutSize.z );
    gkg::Vector3d< int32_t > voxelRadiusOfInfluence(
                    ( int32_t )( lutRadiusOfInfluence / lutResolution.x ) + 1,
                    ( int32_t )( lutRadiusOfInfluence / lutResolution.y ) + 1,
                    ( int32_t )( lutRadiusOfInfluence / lutResolution.z ) + 1 );


    float minimumLutResolution = std::min( lutResolution.x,
                                           std::min( lutResolution.y,
                                                     lutResolution.z ) );


    if ( verbose )
    {

      std::cout << "minimum LUT resolution : " 
                << minimumLutResolution
                << " um"
                << std::endl;
      std::cout << "creating lookup table of fibers : " << std::flush;

    }

    // std::pair< int32_t, int32_t > -> (populationIndex, fiberIndex)
    // std::set< int32_t > -> (fiberPointIndex1, fiberPointIndex2, ....)
    gkg::Volume< std::map< std::pair< int32_t, int32_t >,
                           std::set< int32_t > > > lut( lutSize );

    gkg::Volume< std::map< std::pair< int32_t, int32_t >,
                           std::set< int32_t > > >::iterator
      l = lut.begin(),
      le = lut.end();
    while ( l != le )
    {

      *l = std::map< std::pair< int32_t, int32_t >, std::set< int32_t > >();
      ++ l;

    }

    gkg::BoundingBox< int32_t > voxelBoundingBox( lut );


    int32_t currentFiberPopulationIndex = 0;
    for ( currentFiberPopulationIndex = 0;
          currentFiberPopulationIndex < fiberPopulationCount;
          currentFiberPopulationIndex ++ )
    {

      const gkg::RCPointer< gkg::FiberPopulation >&
       currentFiberPopulation = fiberPopulations[ currentFiberPopulationIndex ];

      std::vector< gkg::LightCurve3d< float > >::const_iterator
        f = currentFiberPopulation->getFiberCentroids().begin(),
        fe = currentFiberPopulation->getFiberCentroids().end();

      int32_t currentFiberCentroidIndex = 0;
      int32_t currentPointIndex1 = 0;
      int32_t currentPointIndex2 = 0;
      while ( f != fe )
      {

        currentPointIndex1 = 0;
        currentPointIndex2 = 1;
        gkg::LightCurve3d< float >::const_iterator
          pi1 = f->begin(),
          pi2 = f->begin(),
          pie = f->end();

        ++ pi2;
        do
        {

          gkg::DwiComplexGeometrySimulatorCommand::addSegmentToLut(
                                                    extendedFieldOfView,
                                                    currentFiberPopulationIndex,
                                                    currentFiberCentroidIndex,
                                                    *pi1, currentPointIndex1,
                                                    *pi2, currentPointIndex2,
                                                    lutResolution,
                                                    minimumLutResolution,
                                                    voxelRadiusOfInfluence,
                                                    voxelBoundingBox,
                                                    lut );

          ++ pi1;
          ++ pi2;
          ++ currentPointIndex1;
          ++ currentPointIndex2;

        }
        while ( pi2 != pie );

        ++ f;
        ++ currentFiberCentroidIndex;

      }

    }

    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }

//     int32_t nx = 0;
//     int32_t ny = 0;
//     int32_t nz = 0;
//     for ( nz = 0; nz < lutSize.z; nz++ )
//     {
// 
//       for ( ny = 0; ny < lutSize.y; ny++ )
//       {
// 
//         for ( nx = 0; nx < lutSize.x; nx++ )
//         {
// 
//           std::cout << "(" << nx << ", " << ny << ", " << nz << " ) = "
//                     << std::flush;
// 
//           std::map< int32_t, std::set< int32_t > >::const_iterator
//             fl = lut( nx, ny, nz ).begin(),
//             fle = lut( nx, ny, nz ).end();
//           while ( fl != fle )
//           {
// 
// 
//             std::cout << "( "
//                       << fl->first.first
//                       << ", "
//                       << fl->first.second
//                       << " )" << std::flush;
//             ++ fl;
// 
//           }
// 
//           std::cout << std::endl;
// 
//         }
// 
//       }
// 
//     }


    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    // checking that fibers have really no intersection after inducing global
    // dispersion
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    if ( checkIntersections )
    {

      if ( verbose )
      {

        std::cout << "checking that fibers have really no intersection  "
                  << std::endl
                  << "after inducing global angular dispersion : "
                  << std::flush;

      }

      checkNoIntersectionBetweenFibers( fiberPopulations );

      if ( verbose )
      {

        std::cout << "done" << std::endl;

      }

    }


    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    // inducing tortuosity by moving individual points of fiber(s)
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

      std::cout << "inducing tortuosity by moving individual fiber points : "
                << std::flush;

    }

    // for all the populations, multiplying the angular deviation of each fiber 
    // by the number of cylinders composing the fiber
    for ( fiberPopulationIndex = 0; fiberPopulationIndex < fiberPopulationCount;
          fiberPopulationIndex ++ )
    {

      fiberPopulations[ fiberPopulationIndex ]->
                              scaleAngularDeviationsWithFiberSubdivisionCount();

    }

    currentIterationIndex = 0;
    while ( !hasReachedTargetAngularDispersions( fiberPopulations ) &&
            ( currentIterationIndex < maximumTortuosityIterationCount ) )
    {

      if ( verbose )
      {

        if ( ( ( currentIterationIndex + 1 ) % 1000 == 0 ) ||
             ( currentIterationIndex == 0 ) ||
             ( currentIterationIndex == maximumTortuosityIterationCount - 1 ) )
        {

          if ( currentIterationIndex )
          {

            std::cout << gkg::Eraser( 28 );

          }
          std::cout << " [ " << std::setw( 10 )
                    << currentIterationIndex + 1
                    << " / " << std::setw( 10 )
                    << maximumTortuosityIterationCount
                    << " ]" << std::flush;

        }

      }

      // selecting a random fiber
      // selecting a random fiber population
      uint32_t selectedFiberPopulationIndex = 0U;
      if ( fiberPopulationCount > 1 )
      {

        selectedFiberPopulationIndex =
            factory->getUniformRandomUInt32( *randomGenerator,
                                             ( uint32_t )fiberPopulationCount );

      }

      gkg::RCPointer< gkg::FiberPopulation >&
             fiberPopulation = fiberPopulations[ selectedFiberPopulationIndex ];

      if ( !fiberPopulation->hasReachedTargetAngularDispersion() )
      {

        fiberPopulation->induceTortuosityOfARandomFiber( fiberPopulations,
                                                         lut,
                                                         lutSize,
                                                         lutResolution,
                                                         minimumLutResolution,
                                                         voxelRadiusOfInfluence,
                                                         voxelBoundingBox );
      }

      ++ currentIterationIndex;

    }


    if ( verbose )
    {

      std::cout << std::endl;
      for ( fiberPopulationIndex = 0;
            fiberPopulationIndex < fiberPopulationCount;
            fiberPopulationIndex ++ )
      {

        std::cout << "current angular dispersion reached after "
                  << currentIterationIndex
                  << " iterations for population " 
                  << fiberPopulationIndex + 1 
                  << " : "
                  << fiberPopulations[ fiberPopulationIndex ]->
                                                   getCurrentAngularDispersion()
                  << " degrees (target="
                  << fiberPopulations[ fiberPopulationIndex ]->
                                                    getTargetAngularDispersion()
                  << " degrees)"
                  << std::endl;

      }

    }


    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    // checking that fibers have really no intersection after inducing 
    // tortuosity
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    if ( checkIntersections )
    {


      if ( verbose )
      {

        std::cout << "checking that fibers have really no intersection  "
                  << std::endl
                  << "after inducing tortuosity : "
                  << std::flush;

      }

      checkNoIntersectionBetweenFibers( fiberPopulations );

      if ( verbose )
      {

        std::cout << "done" << std::endl;

      }

    }


    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    // creating the axon membrane
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    if ( creatingAxonalMembrane )
    {

      if ( verbose )
      {

        std::cout << "creating axonal membranes : " << std::flush;

      }

      for ( fiberPopulationIndex = 0;
            fiberPopulationIndex < fiberPopulationCount;
            fiberPopulationIndex ++ )
      {

        fiberPopulations[ fiberPopulationIndex ]->createAxonalMembranes();

      }

      if ( verbose )
      {

        std::cout << "done" << std::endl;

      }

    }


    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    // creating the Ranvier nodes
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    if ( creatingRanvierNodes )
    {

      if ( verbose )
      {

        std::cout << "creating Ranvier nodes : " << std::flush;

      }

      for ( fiberPopulationIndex = 0;
            fiberPopulationIndex < fiberPopulationCount;
            fiberPopulationIndex ++ )
      {

        fiberPopulations[ fiberPopulationIndex ]->createRanvierNodes();

      }

      ///// WE NEED TO RECOMPUTE THE LUT IN CASE OF USE IN THE FUTURE

      if ( verbose )
      {

        std::cout << "done" << std::endl;

      }

    }


    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    // creating the beadings
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    if ( creatingBeadings )
    {

      if ( verbose )
      {

        std::cout << "creating beadings : " << std::flush;

      }

      for ( fiberPopulationIndex = 0;
            fiberPopulationIndex < fiberPopulationCount;
            fiberPopulationIndex ++ )
      {

        fiberPopulations[ fiberPopulationIndex ]->createBeadings();

      }

      ///// WE NEED TO RECOMPUTE THE LUT IN CASE OF USE IN THE FUTURE

      if ( verbose )
      {

        std::cout << "done" << std::endl;

      }

    }


    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    // creating mesh
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    if ( creatingMesh )
    {

      if ( verbose )
      {

        std::cout << "creating mesh : " << std::flush;

      }

      int32_t globalVertexCount = 0;
      int32_t globalPolygonCount = 0;
      for ( currentFiberPopulationIndex = 0;
            currentFiberPopulationIndex < fiberPopulationCount;
            currentFiberPopulationIndex ++ )
      {

        const gkg::RCPointer< gkg::FiberPopulation >&
         currentFiberPopulation = fiberPopulations[ currentFiberPopulationIndex ];

        std::vector< gkg::LightCurve3d< float > >::const_iterator
          f = currentFiberPopulation->getFiberCentroids().begin(),
          fe = currentFiberPopulation->getFiberCentroids().end();
        int32_t fiberCentroidPointCount = 0;
        while ( f != fe )
        {

          fiberCentroidPointCount = f->getPointCount();
          globalVertexCount += ( fiberCentroidPointCount - 1 ) * 
                               crossSectionVertexCount * 10;
          globalPolygonCount += ( fiberCentroidPointCount - 2 ) * 
                                crossSectionVertexCount * 20;
          ++ f;

        }

      }

      if ( creatingAxonalMembrane )
      {

        // we need to double these figures, since we are creating two membranes:
        // - the outer myelin membrane
        // - the axon membrane
        globalVertexCount *= 2;
        globalPolygonCount *= 2;
    
      }

      gkg::MeshAccumulator< int32_t, float, 3U >
        meshAccumulator;
      meshAccumulator.reserve( 0, globalVertexCount, globalPolygonCount );

      for ( currentFiberPopulationIndex = 0;
            currentFiberPopulationIndex < fiberPopulationCount;
            currentFiberPopulationIndex ++ )
      {

        const gkg::RCPointer< gkg::FiberPopulation >&
          currentFiberPopulation = fiberPopulations[ 
                                                  currentFiberPopulationIndex ];

        std::vector< gkg::LightCurve3d< float > >::const_iterator
          c = currentFiberPopulation->getFiberCentroids().begin(),
          ce = currentFiberPopulation->getFiberCentroids().end();
        std::vector< std::vector< float > >::const_iterator
          omr = currentFiberPopulation->getOuterMyelinRadiusProfiles().begin();
        std::vector< std::vector< float > >::const_iterator
          ar = currentFiberPopulation->getAxonRadiusProfiles().begin();
        while ( c != ce )
        {

          gkg::RCPointer< gkg::MeshMap< int32_t, float, 3U > > 
            outerMyelinMembrane = gkg::MeshFactory::getInstance().getFiber(
                                                       *c,
                                                       *omr,
                                                       crossSectionVertexCount,
                                                       false );
          meshAccumulator.add( *outerMyelinMembrane );

          if ( creatingAxonalMembrane )
          {
            gkg::RCPointer< gkg::MeshMap< int32_t, float, 3U > > 
              axonMembrane = gkg::MeshFactory::getInstance().getFiber(
                                                    *c,
                                                   *ar,
                                                   crossSectionVertexCount,
                                                   false );
            meshAccumulator.add( *axonMembrane );

          }

          ++ c;
          ++ omr;
          ++ ar;
        }

      }

      if ( verbose )
      {

        std::cout << "done" << std::endl;

      }


      //////////////////////////////////////////////////////////////////////////
      //////////////////////////////////////////////////////////////////////////
      // saving mesh
      //////////////////////////////////////////////////////////////////////////
      //////////////////////////////////////////////////////////////////////////

      if ( verbose )
      {

        std::cout << "saving mesh : " << std::flush;

      }

      gkg::MeshMap< int32_t, float, 3U >
        meshMap;
      meshMap.add( meshAccumulator );

      std::string fileNameMesh = fileNameOut + ".mesh";

      gkg::Writer::getInstance().write( fileNameMesh, meshMap );

      if ( verbose )
      {

        std::cout << "done" << std::endl;

      }

    }


    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    // creating .membrane
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

      std::cout << "creating membrane and centroids : " << std::flush;

    }

    if ( format == "hdf5" )
    {

      std::string fileNameHDF5 = fileNameOut + ".h5";
      gkg::HDF5Writer hdf5Writer;
      hdf5Writer.write( fileNameHDF5, fiberPopulations );

    }
    else
    {

      std::string fileNameMembrane = fileNameOut + ".membrane";

      std::ofstream os( fileNameMembrane.c_str() );

      gkg::BundleMap< std::string > bundleMapCentroids;

      os << "attributes = {" << std::endl;

      for ( currentFiberPopulationIndex = 0;
            currentFiberPopulationIndex < fiberPopulationCount;
            currentFiberPopulationIndex ++ )
      {

        const gkg::RCPointer< gkg::FiberPopulation >&
          currentFiberPopulation = 
                               fiberPopulations[ currentFiberPopulationIndex ];

        bundleMapCentroids.addCurve3ds(
                             std::string( "fiber_population_" ) +
                             gkg::StringConverter::toString(
                                              currentFiberPopulationIndex + 1 ),
                             currentFiberPopulation->getFiberCentroids() );

        std::vector< gkg::LightCurve3d< float > >::const_iterator
          c = currentFiberPopulation->getFiberCentroids().begin(),
          ce = currentFiberPopulation->getFiberCentroids().end();
        std::vector< std::vector< float > >::const_iterator
          omr = currentFiberPopulation->getOuterMyelinRadiusProfiles().begin();
        std::vector< std::vector< float > >::const_iterator
          ar = currentFiberPopulation->getAxonRadiusProfiles().begin();
        int32_t currentFiberCentroidIndex = 0;
        int32_t p = 0;
        while ( c != ce )
        {

          //////////////////////////////////////////////////////////////////////
          // creating outer myelin membrane
          //////////////////////////////////////////////////////////////////////

          std::string name = "outer_myelin_membrane_" +
                             gkg::StringConverter::toString( 
                                             currentFiberPopulationIndex +1 ) +
                             "_" +
                             gkg::StringConverter::toString(
                                                currentFiberCentroidIndex + 1 );
          os << "'" << name << "': {" << std::endl;

          os << "'type': 'fiber'," << std::endl;
          os << "'parameters': {" << std::endl;

          os << "'points': (" << std::endl;

          int32_t pointCount = c->getPointCount();
          for ( p = 0; p < pointCount - 1; p++ )
          {

            os << c->getPoint( p ) << ", " << std::endl;

          }
          os << c->getPoint( p ) << std::endl;

          os << ")," << std::endl;

          os << "'radii': (" << std::endl;

          pointCount = ( int32_t )omr->size();
          for ( p = 0; p < pointCount - 1; p++ )
          {
 
            os << ( *omr )[ p ] << ", " << std::endl;

          }
          os << ( *omr )[ p ] << std::endl;

          os << ")," << std::endl;

          os << "'vertex_count_per_plane': "
             << crossSectionVertexCount << ","
             << std::endl;

          os << "'random_rotation': 0" << std::endl;

          os << "}" << std::endl;

          os << "}" << std::endl;

          //////////////////////////////////////////////////////////////////////
          // creating outer myelin membrane
          ///////////////////////////////////////////////////////////////////////

          if ( creatingAxonalMembrane )
          {

            name = "axon_membrane_" +
                   gkg::StringConverter::toString( 
                                     currentFiberPopulationIndex +1 ) +
                   "_" +
                   gkg::StringConverter::toString(
                                        currentFiberCentroidIndex + 1 );
            os << "'" << name << "': {" << std::endl;

            os << "'type': 'fiber'," << std::endl;
            os << "'parameters': {" << std::endl;

            os << "'points': (" << std::endl;

            pointCount = c->getPointCount();
            for ( p = 0; p < pointCount - 1; p++ )
            {

              os << c->getPoint( p ) << ", " << std::endl;

            }
            os << c->getPoint( p ) << std::endl;

            os << ")," << std::endl;

            os << "'radii': (" << std::endl;

            pointCount = ( int32_t )ar->size();
            for ( p = 0; p < pointCount - 1; p++ )
            {

              os << ( *ar )[ p ] << ", " << std::endl;

            }
            os << ( *ar )[ p ] << std::endl;

            os << ")," << std::endl;

            os << "'vertex_count_per_plane': "
               << crossSectionVertexCount << ","
               << std::endl;

            os << "'random_rotation': 0" << std::endl;

            os << "}" << std::endl;

            os << "}" << std::endl;

          }
        
          ++ c;
          ++ omr;
          ++ ar;
          ++ currentFiberCentroidIndex;

        }

      }

      os << "}" << std::endl;

      os.close();

      std::string fileNameCentroids = fileNameOut + "_centroids.bundles";
      gkg::Writer::getInstance().write( fileNameCentroids, bundleMapCentroids );
      
    }

    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }

  }
  GKG_CATCH( "void gkg::DwiComplexGeometrySimulatorCommand::execute( "
             "const gkg::BoundingBox< float >& fieldOfView, "
             "float meanDiameter, "
             "float diameterVariance, "
             "const gkg::Vector3d< float >& meanOrientation, "
             "float targetIntracellularVolumeFraction, "
             "float targetAngularDispersion, "
             "float maximumFiberTortuosity, "
             "int32_t vertexCountPerPlane, "
             "int32_t fiberSubdivisionCount, "
             "bool verbose )" );

}


bool gkg::DwiComplexGeometrySimulatorCommand::hasEnoughSpace(
                    const gkg::Vector3d< float >& currentPosition,
                    float currentOuterMyelinDiameter,
                    const gkg::Vector3d< float >& currentAxis,
                    const std::vector< gkg::RCPointer< gkg::FiberPopulation > >&
                                                        fiberPopulations,
                    int32_t iteration,
                    int32_t iterationCount,
                    float spaceRatio,
                    float spaceLambda ) const
{

  try
  {

    const gkg::BoundingBox< float >&
      extendedFieldOfView = fiberPopulations[ 0 ]->getExtendedFieldOfView();

    float halfLength = ( extendedFieldOfView.getUpperX() - 
                         extendedFieldOfView.getLowerX() ) +
                       ( extendedFieldOfView.getUpperY() - 
                         extendedFieldOfView.getLowerY() ) +
                       ( extendedFieldOfView.getUpperZ() - 
                         extendedFieldOfView.getLowerZ() );
    
    int32_t fiberPopulationCount = ( int32_t )fiberPopulations.size();

    // determining the population of the current fiber
    int32_t currentFiberPopulationIndex = 0;
    int32_t fiberPopulationIndex = 0;
    for ( fiberPopulationIndex = 0;
          fiberPopulationIndex < fiberPopulationCount;
          fiberPopulationIndex++ )
    {

      const gkg::RCPointer< gkg::FiberPopulation >&
        fiberPopulation = fiberPopulations[ fiberPopulationIndex ];      

      const gkg::Vector3d< float > & fiberPopulationMeanOrientation = 
        fiberPopulation->getMeanOrientation();

      if ( fiberPopulationMeanOrientation.cross( currentAxis ).getNorm() 
                                                                      == 0.0f )
      {

        currentFiberPopulationIndex = fiberPopulationIndex;
        break;

      }

    }

    // computing the common normal vector between the current population and 
    // each other population 
    std::vector< gkg::Vector3d< float > > commonNormalVectors( 
                                  fiberPopulationCount - 1,
                                  gkg::Vector3d< float >( 0.0f, 0.0f, 0.0f ) );

    fiberPopulationIndex = 0;
    int32_t commonNormalVectorsIndex = 0;
    for ( fiberPopulationIndex = 0;
          fiberPopulationIndex < fiberPopulationCount;
          fiberPopulationIndex++ )
    {

      if ( fiberPopulationIndex != currentFiberPopulationIndex )
      {

        const gkg::RCPointer< gkg::FiberPopulation >&
          fiberPopulation = fiberPopulations[ fiberPopulationIndex ];      

        const gkg::Vector3d< float > & fiberPopulationMeanOrientation = 
          fiberPopulation->getMeanOrientation();

        commonNormalVectors[ commonNormalVectorsIndex ] = 
               currentAxis.cross( fiberPopulationMeanOrientation ).normalize();
        commonNormalVectorsIndex ++; 
        
      }

    }    

    gkg::Vector3d< float > vectorBetweenPointsOfEachFiber;
    fiberPopulationIndex = 0;
    for ( fiberPopulationIndex = 0;
          fiberPopulationIndex < fiberPopulationCount;
          fiberPopulationIndex++ )
    {

      if ( fiberPopulationIndex != currentFiberPopulationIndex )
      {

        const gkg::RCPointer< gkg::FiberPopulation >&
          fiberPopulation = fiberPopulations[ fiberPopulationIndex ];

        int32_t otherFiberCount = fiberPopulation->getFiberCount();
        int32_t otherFiberCentroidIndex = 0;
        for ( otherFiberCentroidIndex = 0;
              otherFiberCentroidIndex < otherFiberCount;
              otherFiberCentroidIndex++ )
        {


          const gkg::LightCurve3d< float >& 
            otherFiberCentroid = fiberPopulation->getFiberCentroid(
                                                      otherFiberCentroidIndex );
          float otherOuterMyelinRadius = 
                                   fiberPopulation->getOuterMyelinRadiusProfile(
                                                 otherFiberCentroidIndex )[ 0 ];
          //float otherOuterMyelinDiameter = 2.0f * otherOuterMyelinRadius;

          const gkg::Vector3d< float >&
            firstPoint = otherFiberCentroid.getPoint( 0 );
          const gkg::Vector3d< float >&
            secondPoint = otherFiberCentroid.getPoint( 1 );

          gkg::Vector3d< float >
            otherPosition = ( firstPoint + secondPoint ) * 0.5f;
          gkg::Vector3d< float >
            otherAxis = secondPoint - firstPoint;
          otherAxis.normalize();

          if ( gkg::hasCylinderCollision3d( currentPosition,
                                            currentAxis,
                                            halfLength,
                                            currentOuterMyelinDiameter / 2.0f,
                                            otherPosition,
                                            otherAxis,
                                            halfLength,
                                            otherOuterMyelinRadius ) )
          {

            return false;

          }

        }

      }
      else
      {

        const gkg::RCPointer< gkg::FiberPopulation >&
          fiberPopulation = fiberPopulations[ fiberPopulationIndex ];

        //const gkg::Vector3d< float > & fiberPopulationMeanOrientation = 
        //  fiberPopulation->getMeanOrientation();

        float minimumDistanceRatio = spaceRatio *
                                     std::exp( -spaceLambda *
                                               ( double )iteration /
                                               ( double )iterationCount );

        // impose the constraint for two populations only 
        //commonNormalVectors[ commonNormalVectorsIndex ] = 
        //     currentAxis.cross( fiberPopulationMeanOrientation ).normalize();

        int32_t otherFiberCount = fiberPopulation->getFiberCount();
        int32_t otherFiberCentroidIndex = 0;
        for ( otherFiberCentroidIndex = 0;
              otherFiberCentroidIndex < otherFiberCount;
              otherFiberCentroidIndex++ )
        {

          const gkg::LightCurve3d< float >& 
            otherFiberCentroid = fiberPopulation->getFiberCentroid(
                                                     otherFiberCentroidIndex );
      
          float otherOuterMyelinRadius = 
                                   fiberPopulation->getOuterMyelinRadiusProfile(
                                                 otherFiberCentroidIndex )[ 0 ];

          float otherOuterMyelinDiameter = 2.0f * otherOuterMyelinRadius;

          const gkg::Vector3d< float >&
            middlePointOfOtherFiberCentroid = otherFiberCentroid.getPoint( 
                 ( int32_t )( otherFiberCentroid.getPointCount() / 2 + 0.5 ) );

          vectorBetweenPointsOfEachFiber = currentPosition - 
                                               middlePointOfOtherFiberCentroid;

          float distance = 
            currentAxis.cross( vectorBetweenPointsOfEachFiber ).getNorm() / 
              currentAxis.getNorm();
                         
          if ( distance <= ( float )( currentOuterMyelinDiameter / 2.0f +
                                      otherOuterMyelinRadius ) )
          {

             return false;

          }  

          uint32_t n = 0;
          for ( n = 0; n < commonNormalVectors.size(); ++n )
          {

            const gkg::Vector3d< float >& normalVector = 
                                                      commonNormalVectors[ n ];

            float dotProduct = std::abs( vectorBetweenPointsOfEachFiber.dot(
                                                              normalVector ) );

            float 
              spacingBetweenFibersWithinAPopulation = dotProduct /
                                                      normalVector.getNorm();
            // we should let at least the space corresponding to the other
            // population radius but not more than spaceRatio times the 
            // ither outer myelin radius
            if ( ( spacingBetweenFibersWithinAPopulation <
                   ( minimumDistanceRatio * otherOuterMyelinDiameter ) ) &&
                 ( spacingBetweenFibersWithinAPopulation > 
                                                      otherOuterMyelinRadius ) )
            {

              return false;

            }

          }

        }  

      }

    }
    return true;

/*
    const gkg::BoundingBox< float >&
      extendedFieldOfView = fiberPopulations[ 0 ]->getExtendedFieldOfView();

    float halfLength = ( extendedFieldOfView.getUpperX() - 
                         extendedFieldOfView.getLowerX() ) +
                       ( extendedFieldOfView.getUpperY() - 
                         extendedFieldOfView.getLowerY() ) +
                       ( extendedFieldOfView.getUpperZ() - 
                         extendedFieldOfView.getLowerZ() );
                       

    
    int32_t fiberPopulationCount = ( int32_t )fiberPopulations.size();
    int32_t fiberPopulationIndex = 0;
    for ( fiberPopulationIndex = 0;
          fiberPopulationIndex < fiberPopulationCount;
          fiberPopulationIndex++ )
    {

      const gkg::RCPointer< gkg::FiberPopulation >&
        fiberPopulation = fiberPopulations[ fiberPopulationIndex ];

      int32_t otherFiberCount = fiberPopulation->getFiberCount();
      int32_t otherFiberCentroidIndex = 0;
      for ( otherFiberCentroidIndex = 0;
            otherFiberCentroidIndex < otherFiberCount;
            otherFiberCentroidIndex++ )
      {


        const gkg::LightCurve3d< float >& 
          otherFiberCentroid = fiberPopulation->getFiberCentroid(
                                                      otherFiberCentroidIndex );
        float otherOuterMyelinRadius = 
                                   fiberPopulation->getOuterMyelinRadiusProfile(
                                                 otherFiberCentroidIndex )[ 0 ];

        const gkg::Vector3d< float >&
          firstPoint = otherFiberCentroid.getPoint( 0 );
        const gkg::Vector3d< float >&
          secondPoint = otherFiberCentroid.getPoint( 1 );

        gkg::Vector3d< float >
          otherPosition = ( firstPoint + secondPoint ) * 0.5f;
        gkg::Vector3d< float >
          otherAxis = secondPoint - firstPoint;
        otherAxis.normalize();

        if ( gkg::hasCylinderCollision3d( currentPosition,
                                          currentAxis,
                                          halfLength,
                                          currentOuterMyelinDiameter / 2.0f,
                                          otherPosition,
                                          otherAxis,
                                          halfLength,
                                          otherOuterMyelinRadius ) )
        {

          return false;

        }

      }

    }
    return true;
*/

  }
  GKG_CATCH( "bool gkg::DwiComplexGeometrySimulatorCommand::hasEnoughSpace( "
             "const gkg::Vector3d< float >& currentPosition, "
             "float currentOuterMyelinDiameter, "
             "const gkg::Vector3d< float >& currentAxis, "
             "const std::vector< gkg::RCPointer< gkg::FiberPopulation > >& "
             "fiberPopulations ) const" );

}


RegisterCommandCreator( 
    DwiComplexGeometrySimulatorCommand,
    DECLARE_STRING_PARAMETER_HELP( fileNameOut ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( fieldOfView ) +
    DECLARE_FLOATING_PARAMETER_HELP( outerMyelinMembraneDiameterMeans ) +
    DECLARE_FLOATING_PARAMETER_HELP( outerMyelinMembraneDiameterVariances ) +
    DECLARE_FLOATING_PARAMETER_HELP( gRatioMeans ) +
    DECLARE_FLOATING_PARAMETER_HELP( gRatioVariances ) +
    DECLARE_FLOATING_PARAMETER_HELP( internodalLengthToNodeWidthRatioMeans ) +
    DECLARE_FLOATING_PARAMETER_HELP( internodalLengthToNodeWidthRatioVariances
                                                                             ) +
    DECLARE_FLOATING_PARAMETER_HELP( interbeadingLengthMeans ) +
    DECLARE_FLOATING_PARAMETER_HELP( interbeadingLengthVariances ) +
    DECLARE_FLOATING_PARAMETER_HELP( beadingMagnitudeRatioMeans ) +
    DECLARE_FLOATING_PARAMETER_HELP( beadingMagnitudeRatioVariances ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( meanOrientations ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP(
                                          targetIntracellularVolumeFractions ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( angularVariations ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( targetAngularDispersions ) +
    DECLARE_INTEGER_PARAMETER_HELP( maximumPopulatingIterationCount ) +
    DECLARE_FLOATING_PARAMETER_HELP( spaceRatio ) +
    DECLARE_FLOATING_PARAMETER_HELP( spaceLambda ) +
    DECLARE_INTEGER_PARAMETER_HELP( maximumAngularDispersionIterationCount ) +
    DECLARE_FLOATING_PARAMETER_HELP( fovExpansionRatio ) +
    DECLARE_VECTOR_OF_INTEGERS_PARAMETER_HELP( lutSize ) +
    DECLARE_FLOATING_PARAMETER_HELP( lutRadiusOfInfluence ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( tortuosityVariations ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( maximumFiberTortuosities ) +
    DECLARE_INTEGER_PARAMETER_HELP( maximumTortuosityIterationCount ) +
    DECLARE_VECTOR_OF_INTEGERS_PARAMETER_HELP( tortuosityNeighborhoodSizes ) +
    DECLARE_INTEGER_PARAMETER_HELP( crossSectionVertexCount ) +
    DECLARE_INTEGER_PARAMETER_HELP( fiberResolutionAlongAxis ) +
    DECLARE_BOOLEAN_PARAMETER_HELP( creatingAxonalMembrane ) +
    DECLARE_BOOLEAN_PARAMETER_HELP( creatingRanvierNodes ) +
    DECLARE_BOOLEAN_PARAMETER_HELP( creatingBeadings ) +
    DECLARE_BOOLEAN_PARAMETER_HELP( checkIntersections ) +
    DECLARE_BOOLEAN_PARAMETER_HELP( creatingMesh ) +
    DECLARE_STRING_PARAMETER_HELP( format ) +
    DECLARE_BOOLEAN_PARAMETER_HELP( verbose ) );
