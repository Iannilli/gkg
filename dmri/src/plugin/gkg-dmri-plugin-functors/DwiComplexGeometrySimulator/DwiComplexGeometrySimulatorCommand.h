#ifndef _gkg_dmri_plugin_functors_DwiComplexGeometrySimulator_DwiComplexGeometrySimulatorCommand_h_
#define _gkg_dmri_plugin_functors_DwiComplexGeometrySimulator_DwiComplexGeometrySimulatorCommand_h_

#include <gkg-communication-command/Command.h>
#include <gkg-core-pattern/Creator.h>
#include <gkg-processing-container/BoundingBox.h>
#include <gkg-processing-coordinates/Vector3d.h>
#include <gkg-processing-coordinates/Vector2d.h>
#include <gkg-processing-container/Volume.h>
#include <gkg-processing-container/LightCurve3d.h>
#include <gkg-processing-numericalanalysis/NumericalAnalysisSelector.h>
#include <gkg-processing-numericalanalysis/RandomGenerator.h>
#include <gkg-processing-mesh/RayBoxIntersection.h>
#include <gkg-core-pattern/RCPointer.h>

#include <string>
#include <map>
#include <set>


namespace gkg
{


class FiberPopulation;


class DwiComplexGeometrySimulatorCommand :
                         public Command,
                         public Creator2Arg< DwiComplexGeometrySimulatorCommand,
                                             Command,
                                             int32_t,
                                             char** >,
                         public Creator1Arg< DwiComplexGeometrySimulatorCommand,
                                             Command,
                                             const Dictionary& >
{

  public:

    DwiComplexGeometrySimulatorCommand( int32_t argc,
                                 char* argv[],
                                 bool loadPlugin = false,
                                 bool removeFirst = true );
    DwiComplexGeometrySimulatorCommand(
      const std::string& fileNameOut,
      const BoundingBox< float >& fieldOfView,
      const std::vector< float >& outerMyelinMembraneDiameterMeans,
      const std::vector< float >& outerMyelinMembraneDiameterVariances,
      const std::vector< float >& gRatioMeans,
      const std::vector< float >& gRatioVariances,
      const std::vector< float >& internodalLengthToNodeWidthRatioMeans,
      const std::vector< float >& internodalLengthToNodeWidthRatioVariances,
      const std::vector< float >& interbeadingLengthMeans,
      const std::vector< float >& interbeadingLengthVariances,
      const std::vector< float >& beadingMagnitudeRatioMeans,
      const std::vector< float >& beadingMagnitudeRatioVariances,
      const std::vector< gkg::Vector3d< float > >& meanOrientations,
      const std::vector< float >& targetIntracellularVolumeFractions,
      const std::vector< float >& angularVariations,
      const std::vector< float >& targetAngularDispersions,
      int32_t maximumPopulatingIterationCount,
      float spaceRatio,
      float spaceLambda,
      int32_t maximumAngularDispersionIterationCount,
      float fovExpansionRatio,
      const Vector3d< int32_t >& lutSize,
      float lutRadiusOfInfluence,
      const std::vector< float >& tortuosityVariations,
      const std::vector< float >& maximumFiberTortuosities,
      int32_t maximumTortuosityIterationCount,
      const std::vector< int32_t >& tortuosityNeighborhoodSizes,
      int32_t crossSectionVertexCount,
      float fiberResolutionAlongAxis,
      bool creatingAxonalMembrane,
      bool creatingRanvierNodes,
      bool creatingBeadings,
      bool checkIntersections,
      bool creatingMesh,
      const std::string& format,
      bool verbose );
    DwiComplexGeometrySimulatorCommand( const Dictionary& parameters );
    virtual ~DwiComplexGeometrySimulatorCommand();

    static std::string getStaticName();

    static void addSegmentToLut(
                const BoundingBox< float >& extendedFieldOfView,
                int32_t currentFiberPopulationIndex,
                int32_t currentFiberCentroidIndex,
                const Vector3d< float >& p1,
                int32_t currentPointIndex1,
                const Vector3d< float >& p2,
                int32_t currentPointIndex2,
                const Vector3d< float >& lutResolution,
                float minimumLutResolution,
                const Vector3d< int32_t >& voxelRadiusOfInfluence,
                const BoundingBox< int32_t >& voxelBoundingBox,
                Volume< std::map< std::pair< int32_t, int32_t >,
                                  std::set< int32_t > > >& lut );

    static void removeSegmentFromLut(
                const BoundingBox< float >& extendedFieldOfView,
                int32_t currentFiberPopulationIndex,
                int32_t currentFiberCentroidIndex,
                const Vector3d< float >& p1,
                int32_t currentPointIndex1,
                const Vector3d< float >& p2,
                int32_t currentPointIndex2,
                const Vector3d< float >& lutResolution,
                float minimumLutResolution,
                const Vector3d< int32_t >& voxelRadiusOfInfluence,
                const BoundingBox< int32_t >& voxelBoundingBox,
                Volume< std::map< std::pair< int32_t, int32_t >,
                                  std::set< int32_t > > >& lut );

  protected:

    friend class Creator2Arg< DwiComplexGeometrySimulatorCommand, Command, 
                              int32_t, char** >;
    friend class Creator1Arg< DwiComplexGeometrySimulatorCommand, Command,
                              const Dictionary& >;

    void parse();
    void execute(
      const std::string& fileNameOut,
      const BoundingBox< float >& fieldOfView,
      const std::vector< float >& outerMyelinMembraneDiameterMeans,
      const std::vector< float >& outerMyelinMembraneDiameterVariances,
      const std::vector< float >& gRatioMeans,
      const std::vector< float >& gRatioVariances,
      const std::vector< float >& internodalLengthToNodeWidthRatioMeans,
      const std::vector< float >& internodalLengthToNodeWidthRatioVariances,
      const std::vector< float >& interbeadingLengthMeans,
      const std::vector< float >& interbeadingLengthVariances,
      const std::vector< float >& beadingMagnitudeRatioMeans,
      const std::vector< float >& beadingMagnitudeRatioVariances,
      const std::vector< gkg::Vector3d< float > >& meanOrientations,
      const std::vector< float >& targetIntracellularVolumeFractions,
      const std::vector< float >& angularVariations,
      const std::vector< float >& targetAngularDispersions,
      int32_t maximumPopulatingIterationCount,
      float spaceRatio,
      float spaceLambda,
      int32_t maximumAngularDispersionIterationCount,
      float fovExpansionRatio,
      const Vector3d< int32_t >& lutSize,
      float lutRadiusOfInfluence,
      const std::vector< float >& tortuosityVariations,
      const std::vector< float >& maximumFiberTortuosities,
      int32_t maximumTortuosityIterationCount,
      const std::vector< int32_t >& tortuosityNeighborhoodSizes,
      int32_t crossSectionVertexCount,
      float fiberResolutionAlongAxis,
      bool creatingAxonalMembrane,
      bool creatingRanvierNodes,
      bool creatingBeadings,
      bool checkIntersections,
      bool creatingMesh,
      const std::string& format,
      bool verbose );

    bool hasEnoughSpace(
                    const gkg::Vector3d< float >& currentPosition,
                    float currentDiameter,
                    const gkg::Vector3d< float >& currentAxis,
                    const std::vector< gkg::RCPointer< gkg::FiberPopulation > >&
                                                       fiberPopulations,
                    int32_t iteration,
                    int32_t iterationCount,
                    float spaceRatio,
                    float spaceLambda ) const;

};


}


#endif
