#include <gkg-dmri-plugin-functors/DwiComplexGeometrySimulator/FiberPopulation.h>
#include <gkg-dmri-plugin-functors/DwiComplexGeometrySimulator/DwiComplexGeometrySimulatorCommand.h>
#include <gkg-processing-container/Volume_i.h>
#include <gkg-processing-mesh/CylinderCollision3d.h>
#include <gkg-processing-algobase/Math.h>
#include <gkg-processing-transform/Rotation3dFunctions.h>
#include <gkg-core-io/Writer.h>
#include <gkg-core-io/Eraser.h>
#include <gkg-core-exception/Exception.h>

#include <iostream>
#include <fstream>
#include <iomanip>


////////////////////////////////////////////////////////////////////////////////
// bool computeLocalAngularDeviation()
////////////////////////////////////////////////////////////////////////////////

static float computeLocalAngularDeviation( 
             const gkg::LightCurve3d< float >& selectedCylinderMembraneCentroid,
             int32_t lowerCylinderStartingPointIndex,
             int32_t upperCylinderStartingPointIndex,
             const gkg::Vector3d< float >& axis )
{

  try
  {

    float localAngularDeviation = 0.0f;
    int32_t p = 0;
    for ( p = lowerCylinderStartingPointIndex;
          p < upperCylinderStartingPointIndex; p++ )
    {

      const gkg::Vector3d< float >&
        p1 = selectedCylinderMembraneCentroid.getPoint( p );
      const gkg::Vector3d< float >&
        p2 = selectedCylinderMembraneCentroid.getPoint( p + 1 );

      localAngularDeviation += gkg::getLineAngles( p2 - p1, axis );

    }

    return localAngularDeviation;

  }
  GKG_CATCH( "static float computeLocalAngularDeviation( "
             "const gkg::LightCurve3d< float >& "
             "selectedCylinderMembraneCentroid, "
             "int32_t lowerCylinderStartingPointIndex, "
             "int32_t upperCylinderStartingPointIndex, "
             "const gkg::Vector3d< float >& axis )" );

}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// FiberPopulation
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


gkg::FiberPopulation::FiberPopulation(
                        const gkg::BoundingBox< float >& fieldOfView,
                        const gkg::BoundingBox< float >& extendedFieldOfView,
                        float outerMyelinMembraneDiameterMean,
                        float outerMyelinMembraneDiameterVariance,
                        float gRatioMean,
                        float gRatioVariance,
                        float internodalLengthToNodeWidthRatioMean,
                        float internodalLengthToNodeWidthRatioVariance,
                        float interbeadingLengthMean,
                        float interbeadingLengthVariance,
                        float beadingMagnitudeRatioMean,
                        float beadingMagnitudeRatioVariance,
                        const gkg::Vector3d< float >& meanOrientation,
                        float targetIntracellularVolumeFraction,
                        float angularVariation,
                        float targetAngularDispersion,
                        float tortuosityVariation,
                        float maximumFiberTortuosity,
                        int32_t tortuosityNeighborhoodSize,
                        float fiberResolutionAlongAxis,
                        int32_t crossSectionVertexCount,
                        gkg::RCPointer< gkg::RandomGenerator > randomGenerator )
                     : _fieldOfView( fieldOfView ),
                       _extendedFieldOfView( extendedFieldOfView ),

                       _outerMyelinMembraneDiameterMean( 
                                              outerMyelinMembraneDiameterMean ),
                       _outerMyelinMembraneDiameterVariance(
                                          outerMyelinMembraneDiameterVariance ),
                       _gRatioMean( gRatioMean ),
                       _gRatioVariance( gRatioVariance ),
                       _internodalLengthToNodeWidthRatioMean(
                                         internodalLengthToNodeWidthRatioMean ),
                       _internodalLengthToNodeWidthRatioVariance(
                                     internodalLengthToNodeWidthRatioVariance ),
                       _interbeadingLengthMean( interbeadingLengthMean ),
                       _interbeadingLengthVariance(
                                                   interbeadingLengthVariance ),
                       _beadingMagnitudeRatioMean( beadingMagnitudeRatioMean ),
                       _beadingMagnitudeRatioVariance(
                                                beadingMagnitudeRatioVariance ),
                       _meanOrientation( meanOrientation ),
                       _targetIntracellularVolumeFraction(
                                            targetIntracellularVolumeFraction ),
                       _angularVariation( angularVariation ),
                       _targetAngularDispersion( targetAngularDispersion ),
                       _tortuosityVariation( tortuosityVariation ),
                       _maximumFiberTortuosity( maximumFiberTortuosity ),
                       _tortuosityNeighborhoodSize(
                                                   tortuosityNeighborhoodSize ),
                       _fiberResolutionAlongAxis( fiberResolutionAlongAxis ),
                       _crossSectionVertexCount( crossSectionVertexCount ),
                        _factory( gkg::NumericalAnalysisSelector::getInstance().
                                                   getImplementationFactory() ),
                       _randomGenerator( randomGenerator ),
                       _outerMyelinMembraneDiameterScale(
                                           outerMyelinMembraneDiameterVariance /
                                           outerMyelinMembraneDiameterMean ),
                       _outerMyelinMembraneDiameterShape(
                                          outerMyelinMembraneDiameterMean *
                                          outerMyelinMembraneDiameterMean /
                                          outerMyelinMembraneDiameterVariance ),
                       _gRatioScale( gRatioVariance / gRatioMean ),
                       _gRatioShape( gRatioMean * gRatioMean / gRatioVariance ),
                       _internodalLengthToNodeWidthRatioScale( 
                                      internodalLengthToNodeWidthRatioVariance /
                                      internodalLengthToNodeWidthRatioMean ),
                       _internodalLengthToNodeWidthRatioShape( 
                                     internodalLengthToNodeWidthRatioMean *
                                     internodalLengthToNodeWidthRatioMean /
                                     internodalLengthToNodeWidthRatioVariance ),
                       _interbeadingLengthScale( interbeadingLengthVariance /
                                                 interbeadingLengthMean ),
                       _interbeadingLengthShape( interbeadingLengthMean *
                                                 interbeadingLengthMean /
                                                 interbeadingLengthVariance ),
                       _beadingMagnitudeRatioScale(
                                                beadingMagnitudeRatioVariance / 
                                                beadingMagnitudeRatioMean ),
                       _beadingMagnitudeRatioShape( 
                                                beadingMagnitudeRatioMean *
                                                beadingMagnitudeRatioMean /
                                                beadingMagnitudeRatioVariance ),
                       _fieldOfViewVolume( ( _fieldOfView.getUpperX() -
                                             _fieldOfView.getLowerX() ) *
                                           ( _fieldOfView.getUpperY() -
                                             _fieldOfView.getLowerY() ) *
                                           ( _fieldOfView.getUpperZ() -
                                             _fieldOfView.getLowerZ() ) ),
                       _currentIntracellularVolumeFraction( 0.0f ),
                       _currentAngularDispersion( 0.0f )
{

  try
  {

    _rayBoxIntersection.setBox(
                           gkg::Vector3d< float >( _fieldOfView.getLowerX(),
                                                   _fieldOfView.getLowerY(),
                                                   _fieldOfView.getLowerZ() ),
                           gkg::Vector3d< float >( _fieldOfView.getUpperX(),
                                                   _fieldOfView.getUpperY(),
                                                   _fieldOfView.getUpperZ() ) );                        

  }
  GKG_CATCH( "gkg::FiberPopulation::FiberPopulation( "
             "const gkg::BoundingBox< float >& fieldOfView, "
             "const gkg::BoundingBox< float >& extendedFieldOfView, "
             "float outerMyelinMembraneDiameterMean, "
             "float outerMyelinMembraneDiameterVariance, "
             "float gRatioMean, "
             "float gRatioVariance, "
             "float internodalLengthToNodeWidthRatioMean, "
             "float internodalLengthToNodeWidthRatioVariance, "
             "float interbeadingLengthMean, "
             "float interbeadingLengthVariance, "
             "float beadingMagnitudeRatioMean, "
             "float beadingMagnitudeRatioVariance, "
             "const gkg::Vector3d< float >& meanOrientation, "
             "float targetIntracellularVolumeFraction, "
             "float angularVariation, "
             "float targetAngularDispersion, "
             "float tortuosityVariation, "
             "float maximumFiberTortuosity, "
             "int32_t tortuosityNeighborhoodSize, "
             "float fiberResolutionAlongAxis, "
             "int32_t crossSectionVertexCount, "
             "gkg::RCPointer< gkg::RandomGenerator > randomGenerator )" );

}


gkg::FiberPopulation::FiberPopulation( const gkg::FiberPopulation& other )
                     : _fieldOfView( other._fieldOfView ),
                       _extendedFieldOfView( other._extendedFieldOfView ),
                       _outerMyelinMembraneDiameterMean( 
                                       other._outerMyelinMembraneDiameterMean ),
                       _outerMyelinMembraneDiameterVariance(
                                   other._outerMyelinMembraneDiameterVariance ),
                       _gRatioMean( other._gRatioMean ),
                       _gRatioVariance( other._gRatioVariance ),
                       _internodalLengthToNodeWidthRatioMean(
                                  other._internodalLengthToNodeWidthRatioMean ),
                       _internodalLengthToNodeWidthRatioVariance(
                              other._internodalLengthToNodeWidthRatioVariance ),
                       _interbeadingLengthMean( other._interbeadingLengthMean ),
                       _interbeadingLengthVariance(
                                            other._interbeadingLengthVariance ),
                       _beadingMagnitudeRatioMean(
                                             other._beadingMagnitudeRatioMean ),
                       _beadingMagnitudeRatioVariance(
                                         other._beadingMagnitudeRatioVariance ),
                       _meanOrientation( other._meanOrientation ),
                       _targetIntracellularVolumeFraction(
                                     other._targetIntracellularVolumeFraction ),
                       _angularVariation( other._angularVariation ),
                       _targetAngularDispersion( 
                                               other._targetAngularDispersion ),
                       _tortuosityVariation( other._tortuosityVariation ),
                       _maximumFiberTortuosity( other._maximumFiberTortuosity ),
                       _tortuosityNeighborhoodSize(
                                            other._tortuosityNeighborhoodSize ),
                       _fiberResolutionAlongAxis( 
                                              other._fiberResolutionAlongAxis ),
                       _crossSectionVertexCount( 
                                               other._crossSectionVertexCount ),
                       _factory( other._factory ),
                       _randomGenerator( other._randomGenerator ),
                       _outerMyelinMembraneDiameterScale(
                                    other._outerMyelinMembraneDiameterScale ),
                       _outerMyelinMembraneDiameterShape(
                                   other._outerMyelinMembraneDiameterShape ),
                       _gRatioScale( other._gRatioScale ),
                       _gRatioShape( other._gRatioShape ),
                       _internodalLengthToNodeWidthRatioScale( 
                               other._internodalLengthToNodeWidthRatioScale ),
                       _internodalLengthToNodeWidthRatioShape( 
                              other._internodalLengthToNodeWidthRatioShape ),
                       _interbeadingLengthScale( 
                                             other._interbeadingLengthScale ),
                       _interbeadingLengthShape(
                                            other._interbeadingLengthShape ),
                       _beadingMagnitudeRatioScale(
                                          other._beadingMagnitudeRatioScale ),
                       _beadingMagnitudeRatioShape( 
                                         other._beadingMagnitudeRatioShape ),
                       _fieldOfViewVolume( other._fieldOfViewVolume ),
                       _currentIntracellularVolumeFraction( 
                                    other._currentIntracellularVolumeFraction ),
                       _fiberCentroids( other._fiberCentroids ),
                       _outerMyelinRadiusProfiles(
                                             other._outerMyelinRadiusProfiles ),
                       _axonRadiusProfiles( other._axonRadiusProfiles ),
                       _angularDeviations( other._angularDeviations ),
                       _currentAngularDispersion(
                                               other._currentAngularDispersion )
{

  try
  {

    _rayBoxIntersection.setBox(
                           gkg::Vector3d< float >( _fieldOfView.getLowerX(),
                                                   _fieldOfView.getLowerY(),
                                                   _fieldOfView.getLowerZ() ),
                           gkg::Vector3d< float >( _fieldOfView.getUpperX(),
                                                   _fieldOfView.getUpperY(),
                                                   _fieldOfView.getUpperZ() ) );


  }
  GKG_CATCH( "gkg::FiberPopulation::FiberPopulation( "
             "const gkg::FiberPopulation& other )" );

}


gkg::FiberPopulation::~FiberPopulation()
{
}


gkg::FiberPopulation& 
gkg::FiberPopulation::operator=( const gkg::FiberPopulation& other )
{

  try
  {

    _fieldOfView = other._fieldOfView;
    _extendedFieldOfView = other._extendedFieldOfView;
    _outerMyelinMembraneDiameterMean = other._outerMyelinMembraneDiameterMean;
    _outerMyelinMembraneDiameterVariance =
                                     other._outerMyelinMembraneDiameterVariance;
    _gRatioMean = other._gRatioMean;
    _gRatioVariance = other._gRatioVariance;
    _internodalLengthToNodeWidthRatioMean =
                                    other._internodalLengthToNodeWidthRatioMean;
    _internodalLengthToNodeWidthRatioVariance =
                                other._internodalLengthToNodeWidthRatioVariance;
    _interbeadingLengthMean = other._interbeadingLengthMean;
    _interbeadingLengthVariance = other._interbeadingLengthVariance;
    _beadingMagnitudeRatioMean = other._beadingMagnitudeRatioMean;
    _beadingMagnitudeRatioVariance = other._beadingMagnitudeRatioVariance;
    _meanOrientation = other._meanOrientation;
    _targetIntracellularVolumeFraction = 
                                       other._targetIntracellularVolumeFraction;
    _angularVariation = other._angularVariation;
    _targetAngularDispersion = other._targetAngularDispersion;
    _tortuosityVariation = other._tortuosityVariation;
    _maximumFiberTortuosity = other._maximumFiberTortuosity;
    _tortuosityNeighborhoodSize = other._tortuosityNeighborhoodSize;
    _fiberResolutionAlongAxis = other._fiberResolutionAlongAxis;
    _crossSectionVertexCount = other._crossSectionVertexCount;

    _factory = other._factory;
    _randomGenerator = other._randomGenerator;
    _outerMyelinMembraneDiameterScale = other._outerMyelinMembraneDiameterScale;
    _outerMyelinMembraneDiameterShape = other._outerMyelinMembraneDiameterShape;
    _gRatioScale = other._gRatioScale;
    _gRatioShape = other._gRatioShape;
    _internodalLengthToNodeWidthRatioScale =
                                   other._internodalLengthToNodeWidthRatioScale;
    _internodalLengthToNodeWidthRatioShape =
                                   other._internodalLengthToNodeWidthRatioShape;
    _interbeadingLengthScale = other._interbeadingLengthScale;
    _interbeadingLengthShape = other._interbeadingLengthShape;
    _beadingMagnitudeRatioScale = other._beadingMagnitudeRatioScale;
    _beadingMagnitudeRatioShape = other._beadingMagnitudeRatioShape;
    _fieldOfViewVolume = other._fieldOfViewVolume;
    _currentIntracellularVolumeFraction =
                                      other._currentIntracellularVolumeFraction;
    _fiberCentroids = other._fiberCentroids;
    _outerMyelinRadiusProfiles = other._outerMyelinRadiusProfiles;
    _axonRadiusProfiles = other._axonRadiusProfiles;
    _rayBoxIntersection.setBox(
                           gkg::Vector3d< float >( _fieldOfView.getLowerX(),
                                                   _fieldOfView.getLowerY(),
                                                   _fieldOfView.getLowerZ() ),
                           gkg::Vector3d< float >( _fieldOfView.getUpperX(),
                                                   _fieldOfView.getUpperY(),
                                                   _fieldOfView.getUpperZ() ) );
    _angularDeviations = other._angularDeviations;
    _currentAngularDispersion = other._currentAngularDispersion;

    return *this;

  }
  GKG_CATCH( "gkg::FiberPopulation& "
             "gkg::FiberPopulation::FiberPopulation( "
             "const gkg::FiberPopulation& other )" );

}


const gkg::BoundingBox< float >&
gkg::FiberPopulation::getFieldOfView() const
{

  try
  {

    return _fieldOfView;

  }
  GKG_CATCH( "const gkg::BoundingBox< float >& "
             "gkg::FiberPopulation::getFieldOfView() const" );

}


const gkg::BoundingBox< float >&
gkg::FiberPopulation::getExtendedFieldOfView() const
{

  try
  {

    return _extendedFieldOfView;

  }
  GKG_CATCH( "const gkg::BoundingBox< float >& "
             "gkg::FiberPopulation::getExtendedFieldOfView() const" );

}


float gkg::FiberPopulation::getOuterMyelinMembraneDiameterMean() const
{

  try
  {

    return _outerMyelinMembraneDiameterMean;

  }
  GKG_CATCH( "float "
             "gkg::FiberPopulation::"
             "getOuterMyelinMembraneDiameterMean() const" );

}


float gkg::FiberPopulation::getOuterMyelinMembraneDiameterVariance() const
{

  try
  {

    return _outerMyelinMembraneDiameterVariance;

  }
  GKG_CATCH( "float "
             "gkg::FiberPopulation::"
             "getOuterMyelinMembraneDiameterVariance() const" );

}


float gkg::FiberPopulation::getGRatioMean() const
{

  try
  {

    return _gRatioMean;

  }
  GKG_CATCH( "float "
             "gkg::FiberPopulation::"
             "getGRatioMean() const" );

}


float gkg::FiberPopulation::getGRatioVariance() const
{

  try
  {

    return _gRatioVariance;

  }
  GKG_CATCH( "float "
             "gkg::FiberPopulation::"
             "getGRatioVariance() const" );

}


float gkg::FiberPopulation::getInternodalLengthToNodeWidthRatioMean() const
{

  try
  {

    return _internodalLengthToNodeWidthRatioMean;

  }
  GKG_CATCH( "float "
             "gkg::FiberPopulation::"
             "getInternodalLengthToNodeWidthRatioMean() const" );

}


float gkg::FiberPopulation::getInternodalLengthToNodeWidthRatioVariance() const
{

  try
  {

    return _internodalLengthToNodeWidthRatioVariance;

  }
  GKG_CATCH( "float "
             "gkg::FiberPopulation::"
             "getInternodalLengthToNodeWidthRatioVariance() const" );

}


float gkg::FiberPopulation::getInterbeadingLengthMean() const
{

  try
  {

    return _interbeadingLengthMean;

  }
  GKG_CATCH( "float "
             "gkg::FiberPopulation::"
             "getInterbeadingLengthMean() const" );

}


float gkg::FiberPopulation::getInterbeadingLengthVariance() const
{

  try
  {

    return _interbeadingLengthVariance;

  }
  GKG_CATCH( "float "
             "gkg::FiberPopulation::"
             "getInterbeadingLengthVariance() const" );

}


float gkg::FiberPopulation::getBeadingMagnitudeRatioMean() const
{

  try
  {

    return _beadingMagnitudeRatioMean;

  }
  GKG_CATCH( "float "
             "gkg::FiberPopulation::"
             "getBeadingMagnitudeRatioMean() const" );

}


float gkg::FiberPopulation::getBeadingMagnitudeRatioVariance() const
{

  try
  {

    return _beadingMagnitudeRatioVariance;

  }
  GKG_CATCH( "float "
             "gkg::FiberPopulation::"
             "getBeadingMagnitudeRatioVariance() const" );

}


const gkg::Vector3d< float >& gkg::FiberPopulation::getMeanOrientation() const
{

  try
  {

    return _meanOrientation;

  }
  GKG_CATCH( "const gkg::Vector3d< float >& "
             "gkg::FiberPopulation::getMeanOrientation() const" );

}


float gkg::FiberPopulation::getTargetIntracellularVolumeFraction() const
{

  try
  {

    return _targetIntracellularVolumeFraction;

  }
  GKG_CATCH( "float "
             "gkg::FiberPopulation::getTargetIntracellularVolumeFraction() "
             "const" );

}


float gkg::FiberPopulation::getAngularVariation() const
{

  try
  {

    return _angularVariation;

  }
  GKG_CATCH( "float gkg::FiberPopulation::getAngularVariation() const" );

}


float gkg::FiberPopulation::getTargetAngularDispersion() const
{

  try
  {

    return _targetAngularDispersion;

  }
  GKG_CATCH( "float gkg::FiberPopulation::getTargetAngularDispersion() const" );

}


float gkg::FiberPopulation::getTortuosityVariation() const
{

  try
  {

    return _tortuosityVariation;

  }
  GKG_CATCH( "float gkg::FiberPopulation::getTortuosityVariation() const" );

}


float gkg::FiberPopulation::getMaximumFiberTortuosity() const
{

  try
  {

    return _maximumFiberTortuosity;

  }
  GKG_CATCH( "float gkg::FiberPopulation::getMaximumFiberTortuosity() const" );

}


int32_t gkg::FiberPopulation::getTortuosityNeighborhoodSize() const
{

  try
  {

    return _tortuosityNeighborhoodSize;

  }
  GKG_CATCH( "int32_t "
             "gkg::FiberPopulation::getTortuosityNeighborhoodSize() const" );

}


float gkg::FiberPopulation::getFiberResolutionAlongAxis() const
{

  try
  {

    return _fiberResolutionAlongAxis;

  }
  GKG_CATCH( "float "
             "gkg::FiberPopulation::getFiberResolutionAlongAxis() const" );

}


int32_t gkg::FiberPopulation::getCrossSectionVertexCount() const
{

  try
  {

    return _crossSectionVertexCount;

  }
  GKG_CATCH( "int32_t "
             "gkg::FiberPopulation::getCrossSectionVertexCount() const" );

}



float gkg::FiberPopulation::getCurrentIntracellularVolumeFraction() const
{

  try
  {

    return _currentIntracellularVolumeFraction;

  }
  GKG_CATCH( "float "
             "gkg::FiberPopulation::getCurrentIntracellularVolumeFraction() "
             "const" );

}


float gkg::FiberPopulation::getCurrentAngularDispersion() const
{

  try
  {

    return ( _currentAngularDispersion * 180.0 ) /
           ( this->getFiberCount() * M_PI );

  }
  GKG_CATCH( "float "
             "gkg::FiberPopulation::getCurrentAngularDispersion() "
             "const" );

}


bool gkg::FiberPopulation::hasReachedTargetIntracellularVolumeFraction() const
{

  try
  {

    return ( _currentIntracellularVolumeFraction >= 
             _targetIntracellularVolumeFraction );

  }
  GKG_CATCH( "bool "
             "gkg::FiberPopulation::"
             "hasReachedTargetIntracellularVolumeFraction() "
             "const" );

}


bool gkg::FiberPopulation::hasReachedTargetAngularDispersion() const
{

  try
  {



    return ( ( ( _currentAngularDispersion * 180.0 ) /
               ( this->getFiberCount() * M_PI ) ) >= 
             _targetAngularDispersion );

  }
  GKG_CATCH( "bool "
             "gkg::FiberPopulation::"
             "hasReachedTargetAngularDispersion() "
             "const" );

}


float gkg::FiberPopulation::getRandomOuterMyelinMembraneDiameter() const
{

  try
  {

    return _factory->getGammaRandomNumber(
                                *_randomGenerator,
                                _outerMyelinMembraneDiameterShape,
                                _outerMyelinMembraneDiameterScale * 1e3 ) / 1e3;

  }
  GKG_CATCH( "float gkg::FiberPopulation::"
             "getRandomOuterMyelinMembraneDiameter() const" );

}


float gkg::FiberPopulation::getRandomGRatio() const
{

  try
  {

    float gRatio = _factory->getGammaRandomNumber(
                                *_randomGenerator,
                                _gRatioShape,
                                _gRatioScale * 1e3 ) / 1e3;

    while ( gRatio > 1.0 )
    {

      gRatio = _factory->getGammaRandomNumber(
                                *_randomGenerator,
                                _gRatioShape,
                                _gRatioScale * 1e3 ) / 1e3;

    }

    return gRatio;

  }
  GKG_CATCH( "float gkg::FiberPopulation::"
             "getRandomGRatio() const" );

}


float gkg::FiberPopulation::getRandomInternodalLengthToNodeWidthRatio() const
{

  try
  {

    return _factory->getGammaRandomNumber(
                           *_randomGenerator,
                           _internodalLengthToNodeWidthRatioShape,
                           _internodalLengthToNodeWidthRatioScale * 1e3 ) / 1e3;

  }
  GKG_CATCH( "float gkg::FiberPopulation::"
             "getRandomInternodalLengthToNodeWidthRatio() const" );

}


float gkg::FiberPopulation::getRandomInterbeadingLength() const
{

  try
  {

    return _factory->getGammaRandomNumber(
                                *_randomGenerator,
                                _interbeadingLengthShape,
                                _interbeadingLengthScale * 1e3 ) / 1e3;

  }
  GKG_CATCH( "float gkg::FiberPopulation::"
             "getRandomInterbeadingLength() const" );

}


float gkg::FiberPopulation::getRandomBeadingMagnitudeRatio() const
{

  try
  {

    return _factory->getGammaRandomNumber(
                                *_randomGenerator,
                                _beadingMagnitudeRatioShape,
                                _beadingMagnitudeRatioScale * 1e3 ) / 1e3;

  }
  GKG_CATCH( "float gkg::FiberPopulation::"
             "getRandomBeadingMagnitudeRatio() const" );

}


void gkg::FiberPopulation::addFiber( const Vector3d< float >& currentPosition,
                                     float currentDiameter )
{

  try
  {

    _rayBoxIntersection.setRayWithDirection( currentPosition,
                                             -_meanOrientation );

    gkg::Vector3d< float > p1;
    _rayBoxIntersection.getIntersection( p1 );


    _rayBoxIntersection.setRayWithDirection( currentPosition,
                                             +_meanOrientation );

    gkg::Vector3d< float > p2;
    _rayBoxIntersection.getIntersection( p2 );


    float fiberLength = ( p2 - p1 ).getNorm();

    gkg::LightCurve3d< float > lightCurve3d;
    lightCurve3d.addPoint( p1 );
    lightCurve3d.addPoint( p2 );

    std::vector< float > outerMyelinRadiusProfile( 2, currentDiameter / 2.0 );

    _fiberCentroids.push_back( lightCurve3d );
    _outerMyelinRadiusProfiles.push_back( outerMyelinRadiusProfile );

    _currentIntracellularVolumeFraction += ( ( M_PI *
                                               currentDiameter *
                                               currentDiameter / 4.0 ) *
                                               fiberLength ) /
                                           _fieldOfViewVolume;

  }
  GKG_CATCH( "void gkg::FiberPopulation::addFiber( "
             "const Vector3d< float >& currentPosition, "
             "float currentDiameter )" );

}


void gkg::FiberPopulation::setFullResolutionFibers()
{

  try
  {

    std::vector< gkg::LightCurve3d< float > >::iterator
      f = _fiberCentroids.begin(),
      fe = _fiberCentroids.end();
    std::vector< std::vector< float > >::iterator
      r = _outerMyelinRadiusProfiles.begin();

    while ( f != fe )
    {

      int32_t fiberPointCount = ( int32_t )( f->getLength() /
                                           _fiberResolutionAlongAxis + 0.5 );

      *f = f->getEquidistantCurve( fiberPointCount );
      *r = std::vector< float >( fiberPointCount, ( *r )[ 0 ] );

      ++ f;
      ++ r;

    }

    // resetting the vector of angular deviations to the adequate size and
    // setting to nul values
    _angularDeviations = std::vector< float >( _fiberCentroids.size(), 0.0f );

  }
  GKG_CATCH( "void gkg::FiberPopulation::setFullResolutionFibers()" );

}


int32_t gkg::FiberPopulation::getFiberCount() const
{

  try
  {

    return ( int32_t )_fiberCentroids.size();

  }
  GKG_CATCH( "int32_t gkg::FiberPopulation::getFiberCount() const" );

}


const gkg::LightCurve3d< float >& 
gkg::FiberPopulation::getFiberCentroid( int32_t fiberIndex ) const
{

  try
  {

    if ( ( fiberIndex < 0 ) ||
         ( fiberIndex >= ( int32_t )_fiberCentroids.size() ) )
    {

      throw std::runtime_error( "bad fiber index" );

    }
    return _fiberCentroids[ fiberIndex ];

  }
  GKG_CATCH( "const gkg::LightCurve3d< float >& "
             "gkg::FiberPopulation::getFiberCentroid( "
             "int32_t fiberIndex ) const" );

}


const std::vector< float >&
gkg::FiberPopulation::getOuterMyelinRadiusProfile( int32_t fiberIndex ) const
{

  try
  {

    if ( ( fiberIndex < 0 ) ||
         ( fiberIndex >= ( int32_t )_fiberCentroids.size() ) )
    {

      throw std::runtime_error( "bad fiber index" );

    }
    return _outerMyelinRadiusProfiles[ fiberIndex ];


  }
  GKG_CATCH( "const std::vector< float >& "
             "gkg::FiberPopulation::getOuterMyelinRadiusProfile( "
             "int32_t fiberIndex ) const" );

}


const std::vector< float >&
gkg::FiberPopulation::getAxonRadiusProfile( int32_t fiberIndex ) const
{

  try
  {

    if ( ( fiberIndex < 0 ) ||
         ( fiberIndex >= ( int32_t )_fiberCentroids.size() ) )
    {

      throw std::runtime_error( "bad fiber index" );

    }
    return _axonRadiusProfiles[ fiberIndex ];


  }
  GKG_CATCH( "const std::vector< float >& "
             "gkg::FiberPopulation::getAxonRadiusProfile( "
             "int32_t fiberIndex ) const" );

}


const std::vector< gkg::LightCurve3d< float > >&
 gkg::FiberPopulation::getFiberCentroids() const
{

  try
  {

    return _fiberCentroids;

  }
  GKG_CATCH( "const std::vector< gkg::LightCurve3d< float > >& "
            "gkg::FiberPopulation::getFiberCentroids() const" );

}


const std::vector< std::vector< float > >& 
gkg::FiberPopulation::getOuterMyelinRadiusProfiles() const
{

  try
  {

    return _outerMyelinRadiusProfiles;

  }
  GKG_CATCH( "const std::vector< std::vector< float > >& "
             "gkg::FiberPopulation::getOuterMyelinRadiusProfiles() const" );

}


const std::vector< std::vector< float > >& 
gkg::FiberPopulation::getAxonRadiusProfiles() const
{

  try
  {

    return _axonRadiusProfiles;

  }
  GKG_CATCH( "const std::vector< std::vector< float > >& "
             "gkg::FiberPopulation::getAxonRadiusProfiles() const" );

}


bool gkg::FiberPopulation::induceGlobalDispersionOfARandomFiber(
              const std::vector< gkg::RCPointer< gkg::FiberPopulation > >&
                                                              fiberPopulations )
{

  try
  {

    bool fiberIsModified = false;

    // selecting a random fiber
    uint32_t selectedFiberCentroidIndex = _factory->getUniformRandomUInt32(
                                       *_randomGenerator,
                                       ( uint32_t )( _fiberCentroids.size() ) );

    const gkg::LightCurve3d< float >& 
      selectedFiberCentroid = _fiberCentroids[ selectedFiberCentroidIndex ];
    const float& selectedOuterMyelinRadius =
                  _outerMyelinRadiusProfiles[ selectedFiberCentroidIndex ][ 0 ];

    // randomly selecting the center of rotation
    int32_t selectedFiberCentroidPointCount =
                                          selectedFiberCentroid.getPointCount();
    double fiberCentroidIndexMean = 
                              ( double )( selectedFiberCentroidPointCount / 2 );
    double fiberCentroidIndexStdDev =
                              ( double )( selectedFiberCentroidPointCount / 5 );
    int32_t centerIndex = ( int32_t )_factory->getGaussianRandomNumber(
                                                     *_randomGenerator,
                                                     fiberCentroidIndexMean,
                                                     fiberCentroidIndexStdDev );
    while ( ( centerIndex < 0 ) &&
            ( centerIndex >= selectedFiberCentroidPointCount ) )
    {

      centerIndex = ( int32_t )_factory->getGaussianRandomNumber(
                                                     *_randomGenerator,
                                                     fiberCentroidIndexMean,
                                                     fiberCentroidIndexStdDev );

    }

    const gkg::Vector3d< float >& 
      selectedRotationCenter = selectedFiberCentroid.getPoint( centerIndex );

    // selected fiber information
    const gkg::Vector3d< float >&
      selectedFirstExtremity = selectedFiberCentroid.getPoint( 0 );
    const gkg::Vector3d< float >&
      selectedSecondExtremity = selectedFiberCentroid.getPoint(
                                          selectedFiberCentroidPointCount - 1 );
    float selectedHalfLength =
       ( selectedSecondExtremity - selectedFirstExtremity ).getNorm() / 2.0;

    gkg::Vector3d< float > selectedFiberCentroidCenter =
                    ( selectedFirstExtremity + selectedSecondExtremity ) * 0.5f;

    gkg::Vector3d< float > selectedAxis = selectedSecondExtremity -
                                          selectedFirstExtremity;
    selectedAxis.normalize();

    gkg::Vector3d< float > newSelectedAxis = selectedAxis;
    newSelectedAxis.x += ( float )_factory->getGaussianRandomNumber(
                                                         *_randomGenerator,
                                                         0.0,
                                                         _angularVariation );
    newSelectedAxis.y += ( float )_factory->getGaussianRandomNumber(
                                                         *_randomGenerator,
                                                         0.0,
                                                         _angularVariation );
    newSelectedAxis.z += ( float )_factory->getGaussianRandomNumber(
                                                         *_randomGenerator,
                                                         0.0,
                                                         _angularVariation );
    newSelectedAxis.normalize();

    float alpha = ( selectedRotationCenter - selectedFirstExtremity ).getNorm()
                  / ( 2.0 * selectedHalfLength );

    gkg::Vector3d< float > 
      newSelectedFiberCentroidCenter = selectedFiberCentroidCenter +
                                       ( selectedAxis - newSelectedAxis ) *
                                       ( alpha - selectedHalfLength );

    gkg::Vector3d< float> 
      newSelectedFirstExtremity = newSelectedFiberCentroidCenter -
                                  newSelectedAxis * selectedHalfLength;
    gkg::Vector3d< float>
      newSelectedSecondExtremity = newSelectedFiberCentroidCenter +
                                   newSelectedAxis * selectedHalfLength;

    if ( _extendedFieldOfView.contains( newSelectedFirstExtremity ) &&
         _extendedFieldOfView.contains( newSelectedSecondExtremity ) )
    {

      bool hasCollision = false;
      int32_t fiberPopulationIndex = 0;
      uint32_t currentFiberIndex = 0;
      for ( fiberPopulationIndex = 0;
            fiberPopulationIndex < ( int32_t )fiberPopulations.size();
            fiberPopulationIndex++ )
      {

        const gkg::RCPointer< gkg::FiberPopulation >&
          fiberPopulation = fiberPopulations[ fiberPopulationIndex ];

        uint32_t fiberCount = fiberPopulation->getFiberCount();
        for ( currentFiberIndex = 0; currentFiberIndex < fiberCount;
              currentFiberIndex++ )
        {

          if ( !( ( fiberPopulation == this ) &&
                  ( currentFiberIndex == selectedFiberCentroidIndex ) ) )
          {

            const gkg::LightCurve3d< float >&
              currentFiberCentroid = fiberPopulation->getFiberCentroid(
                                                            currentFiberIndex );
            float currentOuterMyelinRadius = 
              fiberPopulation->getOuterMyelinRadiusProfile(
                                                       currentFiberIndex )[ 0 ];
            const gkg::Vector3d< float >&
              currentFirstExtremity = currentFiberCentroid.getPoints().front();
            const gkg::Vector3d< float >&
              currentSecondExtremity = currentFiberCentroid.getPoints().back();
            gkg::Vector3d< float > currentAxis = currentSecondExtremity -
                                                 currentFirstExtremity;
            currentAxis.normalize();
            float currentHalfLength =
             ( currentSecondExtremity - currentFirstExtremity ).getNorm() / 2.0;
 
            gkg::Vector3d< float > currentFiberCentroidCenter =
                      ( currentFirstExtremity + currentSecondExtremity ) * 0.5f;

            if ( gkg::hasCylinderCollision3d( newSelectedFiberCentroidCenter,
                                              newSelectedAxis,
                                              selectedHalfLength,
                                              selectedOuterMyelinRadius,
                                              currentFiberCentroidCenter,
                                              currentAxis,
                                              currentHalfLength,
                                              currentOuterMyelinRadius ) )
            {

              hasCollision = true;
              break;

            }

          }       

        }
        if ( hasCollision )
        {

          break;

        }

      }


      if ( !hasCollision )
      {

        _currentAngularDispersion -=
                               _angularDeviations[ selectedFiberCentroidIndex ];
        std::vector< gkg::Vector3d< float > > rotatedExtremities( 2U );
        rotatedExtremities[ 0 ] = newSelectedFirstExtremity;
        rotatedExtremities[ 1 ] = newSelectedSecondExtremity;
        gkg::LightCurve3d< float > rotatedLightCurve( rotatedExtremities );
 
        _fiberCentroids[ selectedFiberCentroidIndex ] =
          rotatedLightCurve.getEquidistantCurve(
                                              selectedFiberCentroidPointCount );
        _angularDeviations[ selectedFiberCentroidIndex ] = gkg::getLineAngles(
                                                             newSelectedAxis,
                                                             _meanOrientation );
        _currentAngularDispersion +=
                               _angularDeviations[ selectedFiberCentroidIndex ];

        fiberIsModified = true;

      }

    }

    return fiberIsModified;

  }
  GKG_CATCH( "bool gkg::FiberPopulation::induceGlobalDispersionOfARandomFiber( "
             "const std::vector< gkg::RCPointer< gkg::FiberPopulation > >& "
             "otherFiberPopulations )" );

}



void gkg::FiberPopulation::scaleAngularDeviationsWithFiberSubdivisionCount()
{

  try
  {

    int32_t fiberCount = this->getFiberCount();
    int32_t fiberIndex = 0;
    for ( fiberIndex = 0; fiberIndex < fiberCount; fiberIndex++ )
    {

      //  multiplying the angular deviation of a fiber by the number of 
      // cylinders composing the fiber
      _angularDeviations[ fiberIndex ] *= 
                 ( float )( _fiberCentroids[ fiberIndex ].getPointCount() - 1 );

    }

  }
  GKG_CATCH( "void gkg::FiberPopulation::"
             "scaleAngularDeviationsWithFiberSubdivisionCount()" );

}


bool gkg::FiberPopulation::induceTortuosityOfARandomFiber(
 const std::vector< gkg::RCPointer< gkg::FiberPopulation > >& fiberPopulations,
 gkg::Volume< std::map< std::pair< int32_t, int32_t >,
                        std::set< int32_t > > >& lut,
 const gkg::Vector3d< int32_t >& lutSize,
 const gkg::Vector3d< float >& lutResolution,
 float minimumLutResolution,
 const gkg::Vector3d< int32_t >& voxelRadiusOfInfluence,
 const gkg::BoundingBox< int32_t >& voxelBoundingBox )
{

  try
  {

    bool fiberIsModified = false;

    int32_t selectedFiberPopulationIndex = 0;
    for ( selectedFiberPopulationIndex = 0;
          selectedFiberPopulationIndex < ( int32_t )fiberPopulations.size();
          selectedFiberPopulationIndex++ )
    {

      if ( fiberPopulations[ selectedFiberPopulationIndex ] == this )
      {

        break;

      }

    }


    float newAngularDeviationOfFiberCentroidPortion = 0.0f;
    gkg::Vector3d< int32_t > currentVoxel;
    gkg::Vector3d< int32_t > neighborVoxel;

    // selecting a random fiber
    int32_t selectedFiberCentroidIndex =
                                    ( int32_t )_factory->getUniformRandomUInt32(
                                       *_randomGenerator,
                                       ( uint32_t )( _fiberCentroids.size() ) );

    const gkg::LightCurve3d< float >& 
      selectedFiberCentroid = _fiberCentroids[ selectedFiberCentroidIndex ];
    const float& selectedOuterMyelinRadius =
                  _outerMyelinRadiusProfiles[ selectedFiberCentroidIndex ][ 0 ];

    // randomly selecting the point around which tortuosity is induced
    int32_t selectedFiberCentroidPointCount =
                                          selectedFiberCentroid.getPointCount();
    int32_t selectedFiberCentroidPointIndex =
                                    ( int32_t )_factory->getUniformRandomUInt32(
                                  *_randomGenerator,
                                  ( uint32_t )selectedFiberCentroidPointCount );

    // computing lower and upper bounds of fiber point indices to be moved
    int32_t lowerFiberCentroidPointIndex = std::max(
                                          selectedFiberCentroidPointIndex -
                                          _tortuosityNeighborhoodSize,
                                          0 );
    int32_t upperFiberCentroidPointIndex = std::min(
                                          selectedFiberCentroidPointIndex +
                                          _tortuosityNeighborhoodSize,
                                          selectedFiberCentroidPointCount - 1 );


    // computing lower and upper indices of all impacted cylinder(s) and
    // the number of impacted cylinder(s)
    int32_t lowerCylinderStartingPointIndex = std::max(
                                               lowerFiberCentroidPointIndex - 1,
                                               0 );
    int32_t upperCylinderStartingPointIndex = upperFiberCentroidPointIndex;
    if ( upperFiberCentroidPointIndex == selectedFiberCentroidPointCount - 1 )
    {

      -- upperCylinderStartingPointIndex;

    }
    int32_t impactedCylinderCount = upperCylinderStartingPointIndex - 
                                    lowerCylinderStartingPointIndex + 1;

    // computing the old angular deviation of the selected fiber portion
    float oldAngularDeviationOfFiberCentroidPortion =
                  computeLocalAngularDeviation( selectedFiberCentroid,
                                                lowerCylinderStartingPointIndex,
                                                upperCylinderStartingPointIndex,
                                                _meanOrientation );

    // computing the orthonormal trieder at selected point
    gkg::Vector3d< float > firstAxis;
    if ( selectedFiberCentroidPointIndex < selectedFiberCentroidPointCount - 1 )
    {

      firstAxis = selectedFiberCentroid.getPoint(
                                         selectedFiberCentroidPointIndex + 1 ) -
                  selectedFiberCentroid.getPoint(
                                         selectedFiberCentroidPointIndex );
      firstAxis.normalize();


    }
    else 
    {

      firstAxis = selectedFiberCentroid.getPoint(
                                          selectedFiberCentroidPointIndex ) -
                  selectedFiberCentroid.getPoint(
                                          selectedFiberCentroidPointIndex - 1 );
      firstAxis.normalize();

    }
    gkg::Vector3d< float > secondAxis;
    gkg::Vector3d< float > thirdAxis;
    gkg::getRandomOrthonormalTriederFromFirstAxis( firstAxis,
                                                   secondAxis,
                                                   thirdAxis );

    float displacementAlongSecondAxis =
             ( float )_factory->getGaussianRandomNumber( *_randomGenerator,
                                                         0.0,
                                                         _tortuosityVariation );
    float displacementAlongThirdAxis =
             ( float )_factory->getGaussianRandomNumber( *_randomGenerator,
                                                         0.0,
                                                         _tortuosityVariation );

    std::vector< gkg::Vector3d< float > > 
      displacements( upperFiberCentroidPointIndex -
                     lowerFiberCentroidPointIndex + 1 );

    int32_t neighborIndex = 0;
    int32_t displacementIndex = 0;
    double offset = 0.0f;
    bool exitingExtendedFov = false;
    gkg::Vector3d< float > displacement;
    for ( neighborIndex = lowerFiberCentroidPointIndex;
          neighborIndex <= upperFiberCentroidPointIndex;
          neighborIndex ++ )
    {

      displacementIndex = neighborIndex - lowerFiberCentroidPointIndex;
      offset = ( float )std::abs( neighborIndex - 
                                  selectedFiberCentroidPointIndex );
      displacement = 
                  ( secondAxis * displacementAlongSecondAxis +
                    thirdAxis * displacementAlongThirdAxis ) *
                  ( float )std::exp( -( offset * offset ) /
                                     ( double )_tortuosityNeighborhoodSize  );
      displacements[ displacementIndex ] = displacement;


      if ( !_extendedFieldOfView.contains(
              selectedFiberCentroid.getPoint( neighborIndex ) + displacement ) )
      {

        exitingExtendedFov = true;
        break;

      }

    }

    if ( !exitingExtendedFov )
    {

      bool hasCollision = false;
      int32_t cylinderIndex = 0;
      gkg::Vector3d< float > selectedFirstExtremity;
      gkg::Vector3d< float > selectedSecondExtremity;
      gkg::Vector3d< float > selectedCylinderCenter;
      gkg::Vector3d< float > selectedCylinderAxis;
      float selectedCylinderHalfLength = 0.0f;
      gkg::Vector3d< float > currentCylinderCenter;
      gkg::Vector3d< float > currentCylinderAxis;
      float currentCylinderHalfLength = 0.0f;

      for ( cylinderIndex = 0; cylinderIndex < impactedCylinderCount;
            cylinderIndex++ )
      {



        int32_t firstExtremityIndex = lowerCylinderStartingPointIndex +
                                      cylinderIndex;
        selectedFirstExtremity = selectedFiberCentroid.getPoint(
                                                          firstExtremityIndex );

        if ( ( firstExtremityIndex >= lowerFiberCentroidPointIndex ) &&
             ( firstExtremityIndex <= upperFiberCentroidPointIndex ) )
        {

          selectedFirstExtremity += 
            displacements[ firstExtremityIndex - lowerFiberCentroidPointIndex ];

        }

        int32_t secondExtremityIndex = firstExtremityIndex + 1;
        selectedSecondExtremity = selectedFiberCentroid.getPoint(
                                                         secondExtremityIndex );

        if ( ( secondExtremityIndex >= lowerFiberCentroidPointIndex ) &&
             ( secondExtremityIndex <= upperFiberCentroidPointIndex ) )
        {

          selectedSecondExtremity += 
           displacements[ secondExtremityIndex - lowerFiberCentroidPointIndex ];

        }


        selectedCylinderCenter = ( selectedFirstExtremity +
                                   selectedSecondExtremity ) * 0.5f;
        selectedCylinderAxis = ( selectedSecondExtremity -
                                 selectedFirstExtremity );
        selectedCylinderAxis.normalize();
        selectedCylinderHalfLength = ( selectedSecondExtremity -
                                        selectedFirstExtremity ).getNorm() *
                                      0.5f;

        // computing the map of fibers and their associate points possibly
        // impacted by the cylinder modification, using the precomputed
        // look-up table
        std::map< std::pair< int32_t, int32_t >,
                  std::set< int32_t > > impactedFibersAndPoints;

        int32_t cylinderStartingPointIndex = 0;
        for ( cylinderStartingPointIndex = lowerCylinderStartingPointIndex;
              cylinderStartingPointIndex <= upperCylinderStartingPointIndex+1;
              cylinderStartingPointIndex ++ )
        {


          const gkg::Vector3d< float >&
            fiberCentroidPoint = selectedFiberCentroid.getPoint(
                                                   cylinderStartingPointIndex );

          currentVoxel.x = ( int32_t )( ( fiberCentroidPoint.x -
                                          _extendedFieldOfView.getLowerX() ) /
                                        lutResolution.x );
          currentVoxel.y = ( int32_t )( ( fiberCentroidPoint.y -
                                          _extendedFieldOfView.getLowerY() ) /
                                        lutResolution.y );
          currentVoxel.z = ( int32_t )( ( fiberCentroidPoint.z -
                                          _extendedFieldOfView.getLowerZ() ) /
                                        lutResolution.z );


          if ( currentVoxel.x >= lutSize.x )
          {

            -- currentVoxel.x;


          }
          if ( currentVoxel.y >= lutSize.y )
          {

            -- currentVoxel.y;

          }
          if ( currentVoxel.z >= lutSize.z )
          {

            -- currentVoxel.z;

          }
          const std::map< std::pair< int32_t, int32_t >,
                          std::set< int32_t > >&
            currentFibersAndPoints = lut( currentVoxel );

          std::map< std::pair< int32_t, int32_t >,
                    std::set< int32_t > >::const_iterator
            fp = currentFibersAndPoints.begin(),
            fpe = currentFibersAndPoints.end();
          while ( fp != fpe )
          {

            if ( !( ( fp->first.first == selectedFiberPopulationIndex ) &&
                    ( fp->first.second == selectedFiberCentroidIndex ) ) )
            {

              impactedFibersAndPoints[ fp->first ].insert(
                                       fp->second.begin(), fp->second.end() );

            }
            ++ fp;

          }

        }

        // looping over fibers and points to check putative collisions
        std::map< std::pair< int32_t, int32_t >,
                  std::set< int32_t > >::const_iterator
          fp = impactedFibersAndPoints.begin(),
          fpe = impactedFibersAndPoints.end();
        while ( fp != fpe )
        {

          const int32_t& currentFiberPopulationIndex = fp->first.first;
          const gkg::RCPointer< gkg::FiberPopulation >&
            fiberPopulation = fiberPopulations[ currentFiberPopulationIndex ];

          const int32_t& currentFiberCentroidIndex = fp->first.second;

          const std::set< int32_t >& currentFiberCentroidPoints = fp->second;

          const gkg::LightCurve3d< float >&
            currentFiberCentroid = fiberPopulation->getFiberCentroid(
                                                    currentFiberCentroidIndex );
          int32_t currentFiberCentroidPointCount = 
                                           currentFiberCentroid.getPointCount();
          const float& currentOuterMyelinRadius = 
                                   fiberPopulation->getOuterMyelinRadiusProfile(
                                               currentFiberCentroidIndex )[ 0 ];

          std::set< int32_t >::const_iterator
            p = currentFiberCentroidPoints.begin(),
            pe = currentFiberCentroidPoints.end();
          while ( p != pe )
          {

            int32_t currentPointIndex = *p;

            const gkg::Vector3d< float >&
              currentPoint = currentFiberCentroid.getPoint( currentPointIndex );

            if ( currentPointIndex > 0 )
            {

              const gkg::Vector3d< float >&
                previousPoint = currentFiberCentroid.getPoint( 
                                                      currentPointIndex - 1 );

              currentCylinderCenter = ( previousPoint + currentPoint ) * 0.5f;
              currentCylinderAxis = ( currentPoint - previousPoint );
              currentCylinderAxis.normalize();
              currentCylinderHalfLength = ( currentPoint -
                                            previousPoint ).getNorm() * 0.5f;

              if ( gkg::hasCylinderCollision3d( selectedCylinderCenter,
                                                selectedCylinderAxis,
                                                selectedCylinderHalfLength,
                                                selectedOuterMyelinRadius,
                                                currentCylinderCenter,
                                                currentCylinderAxis,
                                                currentCylinderHalfLength,
                                                currentOuterMyelinRadius ) )
              {

                hasCollision = true;
                break;

              }

            }
            if ( currentPointIndex < currentFiberCentroidPointCount - 1 )
            {

              const gkg::Vector3d< float >&
                nextPoint = currentFiberCentroid.getPoint(
                                                        currentPointIndex + 1 );

              currentCylinderCenter = ( currentPoint + nextPoint ) * 0.5f;
              currentCylinderAxis = ( nextPoint - currentPoint );
              currentCylinderAxis.normalize();
              currentCylinderHalfLength = ( nextPoint -
                                            currentPoint ).getNorm() * 0.5f;

              if ( gkg::hasCylinderCollision3d( selectedCylinderCenter,
                                                selectedCylinderAxis,
                                                selectedCylinderHalfLength,
                                                selectedOuterMyelinRadius,
                                                currentCylinderCenter,
                                                currentCylinderAxis,
                                                currentCylinderHalfLength,
                                                currentOuterMyelinRadius ) )
              {

                hasCollision = true;
                break;

              }

            }

            ++ p;

          }

          if ( hasCollision )
          {

            break;

          }

          ++ fp;

        }

        if ( hasCollision )
        {

          break;

        }

      }


      if ( !hasCollision )
      {


        gkg::LightCurve3d< float > 
          modifiedFiberCentroid( selectedFiberCentroid );
        int32_t fiberCentroidPointIndex = 0;
        for ( fiberCentroidPointIndex = lowerFiberCentroidPointIndex;
              fiberCentroidPointIndex <= upperFiberCentroidPointIndex;
              fiberCentroidPointIndex++ )
        {


          modifiedFiberCentroid.setPoint(
                    fiberCentroidPointIndex,
                    modifiedFiberCentroid.getPoint( fiberCentroidPointIndex ) +
                    displacements[ fiberCentroidPointIndex -
                                   lowerFiberCentroidPointIndex ] );
        }
 
        if ( modifiedFiberCentroid.getTortuosity() < _maximumFiberTortuosity )
        {

          // removing information corresponding to portion of selected fiber
          // from the LUT
          int32_t cylinderStartingPointIndex = 0;
          for ( cylinderStartingPointIndex = lowerCylinderStartingPointIndex;
                cylinderStartingPointIndex <=
                upperCylinderStartingPointIndex + 1;
                cylinderStartingPointIndex ++ )
          {

            gkg::DwiComplexGeometrySimulatorCommand::removeSegmentFromLut(
              _extendedFieldOfView,
              selectedFiberPopulationIndex,
              selectedFiberCentroidIndex,
              selectedFiberCentroid.getPoint( cylinderStartingPointIndex ),
              cylinderStartingPointIndex,
              selectedFiberCentroid.getPoint( cylinderStartingPointIndex + 1 ),
              cylinderStartingPointIndex + 1,
              lutResolution,
              minimumLutResolution,
              voxelRadiusOfInfluence,
              voxelBoundingBox,
              lut );

          }

          // adding information corresponding to portion of modified fiber
          // to the LUT
          for ( cylinderStartingPointIndex = lowerCylinderStartingPointIndex;
                cylinderStartingPointIndex <=
                upperCylinderStartingPointIndex + 1;
                cylinderStartingPointIndex ++ )
          {

            gkg::DwiComplexGeometrySimulatorCommand::addSegmentToLut(
              _extendedFieldOfView,
              selectedFiberPopulationIndex,
              selectedFiberCentroidIndex,
              modifiedFiberCentroid.getPoint( cylinderStartingPointIndex ),
              cylinderStartingPointIndex,
              modifiedFiberCentroid.getPoint( cylinderStartingPointIndex + 1 ),
              cylinderStartingPointIndex + 1,
              lutResolution,
              minimumLutResolution,
              voxelRadiusOfInfluence,
              voxelBoundingBox,
              lut );

          }

          _fiberCentroids[ selectedFiberCentroidIndex ] = modifiedFiberCentroid;
          fiberIsModified = true;

          // computing the new angular deviation of the modified fiber portion
          newAngularDeviationOfFiberCentroidPortion =
                  computeLocalAngularDeviation( modifiedFiberCentroid,
                                                lowerCylinderStartingPointIndex,
                                                upperCylinderStartingPointIndex,
                                                _meanOrientation );

          _angularDeviations[ selectedFiberCentroidIndex ] +=
                                  ( newAngularDeviationOfFiberCentroidPortion -
                                    oldAngularDeviationOfFiberCentroidPortion );

          _currentAngularDispersion = 0.0f;
          int32_t fiberIndex = 0;
          int32_t fiberCount = ( int32_t )_fiberCentroids.size();
          for ( fiberIndex = 0; fiberIndex < fiberCount; fiberIndex++ )
          {

            _currentAngularDispersion +=
                _angularDeviations[ fiberIndex ] /
                ( float )( _fiberCentroids[ fiberIndex ].getPointCount() - 1 );

          }

        }

      }

    }

    return fiberIsModified;

  }
  GKG_CATCH( "bool gkg::FiberPopulation::induceTortuosityOfARandomFiber( "
             "const std::vector< gkg::RCPointer< gkg::FiberPopulation > >& "
             "fiberPopulations, "
             "gkg::Volume< std::map< std::pair< int32_t, int32_t >, "
             "std::set< int32_t > > >& lut, "
             "const gkg::Vector3d< int32_t >& lutSize, "
             "const gkg::Vector3d< float >& lutResolution, "
             "float minimumLutResolution, "
             "const gkg::Vector3d< int32_t >& voxelRadiusOfInfluence, "
             "const gkg::BoundingBox< int32_t >& voxelBoundingBox )" );

}


void gkg::FiberPopulation::createAxonalMembranes()
{

  try
  {

    // collecting the fiber count
    int32_t fiberCount = this->getFiberCount();

    // allocating the axon radius profiles
    _axonRadiusProfiles.resize( fiberCount );


    int32_t fiberIndex = 0;
    int32_t fiberCentroidPointCount = 0;
    float gRatio = 0.0f;
    float outerMyelinDiameter = 0.0f;
    float axonDiameter = 0.0f;
    for ( fiberIndex = 0; fiberIndex < fiberCount; fiberIndex++ )
    {

      gRatio = this->getRandomGRatio();

      // at this time, the outer myelin membrane has got a constant diameter
      // so we only need to get the first one
      
      outerMyelinDiameter = 2 * _outerMyelinRadiusProfiles[ fiberIndex ][ 0 ];
      axonDiameter = gRatio * outerMyelinDiameter;
      fiberCentroidPointCount = _fiberCentroids[ fiberIndex ].getPointCount();
      _axonRadiusProfiles[ fiberIndex ] = 
                                  std::vector< float >( fiberCentroidPointCount,
                                                        axonDiameter / 2.0 );

    }

  }
  GKG_CATCH( "void gkg::FiberPopulation::createAxonalMembranes()" );

}


void gkg::FiberPopulation::createRanvierNodes()
{

  try
  {

    // collecting the fiber count
    int32_t fiberCount = this->getFiberCount();

    //
    //                  |     internodal length    |
    //                  <-------------------------->
    //                  |                          |
    //  -------------+  |  +--------------------+  |  +----------
    //               |  v  |                    |  v  |
    //               +-----+                    +-----+
    //
    //
    //               +-----+                    +-----+
    //               |     |                    |     |
    //  -------------+     +--------------------+     +----------
    //               ^     ^
    //               |     |
    //               <----->------------------------
    //               |     |       node width


    int32_t fiberIndex = 0;
    int32_t fiberCentroidPointCount = 0;
    float outerMyelinDiameter = 0.0f;
    float minimumMyelinDiameter = 0.0f;
    float axonDiameter = 0.0f;
    float gRatio = 0.0f;
    float internodalLength = 0.0f;
    float internodalLengthToNodeWidthRatio = 0;
    float nodeWidth = 0.0f;
    float curvilinearAbscissa = 0.0f;
    float offset = 0.0f;
    float varyingPortionLength = 0.0f;
    float plateauLength = 0.0f;
    gkg::Vector3d< float > currentPoint;
    gkg::Vector3d< float > nextPoint;
    int32_t p = 0;
    int32_t   originalFiberCentroidPointCount = 0;
    int32_t superResolvedFiberCentroidPointCount = 0;
    float ranvierNodeRadiusRatio = 0.0f;
    gkg::Vector3d< float > previousPoint;
    for ( fiberIndex = 0; fiberIndex < fiberCount; fiberIndex++ )
    {

      axonDiameter = 2 * _axonRadiusProfiles[ fiberIndex ][ 0 ];
      minimumMyelinDiameter = axonDiameter * 1.05;
      outerMyelinDiameter = 2 * _outerMyelinRadiusProfiles[ fiberIndex ][ 0 ];
     
      // at this time, the outer myelin membrane has got a constant diameter,
      // and the axon membrane too, so we can compute the g-ratio
      gRatio = axonDiameter / outerMyelinDiameter;


      // computing the internodal length
      internodalLength = 30.0 * axonDiameter *
                         std::sqrt( std::log( 1.0 / gRatio ) );

      // computing the ration between the internodal length and the node width
      internodalLengthToNodeWidthRatio = 
                              this->getRandomInternodalLengthToNodeWidthRatio();

      // computing the node width
      nodeWidth = internodalLength / internodalLengthToNodeWidthRatio;

      // computing impacted portion length
      varyingPortionLength = ( internodalLength - nodeWidth ) / 12.0;
      plateauLength = ( internodalLength - nodeWidth ) -
                       2 * varyingPortionLength;

      const gkg::LightCurve3d< float >&
        originalCentroid = _fiberCentroids[ fiberIndex ];
      
      originalFiberCentroidPointCount = originalCentroid.getPointCount();

      // computing offset
      offset = ( float )_factory->getUniformRandomNumber(
                                             *_randomGenerator,
                                             0.0,
                                             ( double )internodalLength / 2.0 );

      // estimating boundaries of Ranvier nodes
      superResolvedFiberCentroidPointCount = 
                                           originalFiberCentroidPointCount * 20;
      gkg::LightCurve3d< float > superResolvedCentroid =
                                          originalCentroid.getEquidistantCurve( 
                                         superResolvedFiberCentroidPointCount );
      std::vector< bool > 
        belongsToARanvierNode( superResolvedFiberCentroidPointCount, false );
      curvilinearAbscissa = offset;
      for ( p = 0; p < superResolvedFiberCentroidPointCount; p++ )
      {

        currentPoint = superResolvedCentroid.getPoint( p );
        if ( p < superResolvedFiberCentroidPointCount - 1 )
        {

          nextPoint = superResolvedCentroid.getPoint( p + 1 );
          curvilinearAbscissa += ( nextPoint - currentPoint ).getNorm();

        }

        ranvierNodeRadiusRatio = this->getRanvierNodeRadius(
                                                           curvilinearAbscissa,
                                                           internodalLength,
                                                           nodeWidth,
                                                           varyingPortionLength,
                                                           plateauLength );
        belongsToARanvierNode[ p ] = ( ( ranvierNodeRadiusRatio < 1.0 ) ? 
                                       true : false );
        if ( ( p > 0 ) && belongsToARanvierNode[ p ] )
        {

          belongsToARanvierNode[ p - 1 ] = true;


        }
        else if ( ( ( p + 1 ) < fiberCentroidPointCount ) &&
                   belongsToARanvierNode[ p ] )
        {

          belongsToARanvierNode[ p + 1 ] = true;


        }        

      }

      // resampling outer myelin membrane around Ranvier nodes
      std::map< float, gkg::Vector3d< float > > pointMap;
      curvilinearAbscissa = 0.0;
      previousPoint = originalCentroid.getPoint( 0 );
      for ( p = 0; p < originalFiberCentroidPointCount; p++ )
      {

         const gkg::Vector3d< float >&
           point = originalCentroid.getPoint( p );
         curvilinearAbscissa += ( point - previousPoint ).getNorm();

         pointMap[ curvilinearAbscissa ] = point;

         previousPoint = point;

      }

      curvilinearAbscissa = 0.0;
      previousPoint = superResolvedCentroid.getPoint( 0 );
      for ( p = 0; p < superResolvedFiberCentroidPointCount; p++ )
      {

         const gkg::Vector3d< float >&
           point = superResolvedCentroid.getPoint( p );
         curvilinearAbscissa += ( point - previousPoint ).getNorm();

         if ( belongsToARanvierNode[ p ] )
         {

           pointMap[ curvilinearAbscissa ] = point;

        }

         previousPoint = point;

      }

      // creating the new centroid
      std::list< gkg::Vector3d< float > > points;
      std::map< float, gkg::Vector3d< float > >::const_iterator
        pm = pointMap.begin(),
        pme = pointMap.end();

      while ( pm != pme )
      {

        points.push_back( pm->second );
        ++ pm;

      }

      _fiberCentroids[ fiberIndex ] = gkg::LightCurve3d< float >( points );

      // resizing consistently the outer myelin membrane profile and the axon 
      // membrane profile
      _outerMyelinRadiusProfiles[ fiberIndex ].resize( points.size() );
      _axonRadiusProfiles[ fiberIndex ].resize( points.size() );


      // computing the outer myelin membrane profile and the axon membrane
      // profile
      const gkg::LightCurve3d< float >&
        newCentroid = _fiberCentroids[ fiberIndex ];

      //std::string filename = "profile_";
      //filename += gkg::StringConverter::toString( fiberIndex + 1 );
      //std::ofstream os( filename.c_str() );
      int32_t newFiberCentroidPointCount = ( int32_t )points.size();
      curvilinearAbscissa = offset;
      previousPoint = newCentroid.getPoint( 0 );
      for ( p = 0; p < newFiberCentroidPointCount; p++ )
      {

        const gkg::Vector3d< float >&
          point = newCentroid.getPoint( p );
        curvilinearAbscissa += ( point - previousPoint ).getNorm();

        ranvierNodeRadiusRatio = this->getRanvierNodeRadius(
                                                           curvilinearAbscissa,
                                                           internodalLength,
                                                           nodeWidth,
                                                           varyingPortionLength,
                                                           plateauLength );

         _outerMyelinRadiusProfiles[ fiberIndex ][ p ] =
                       0.5 * ( minimumMyelinDiameter +
                               ( outerMyelinDiameter - minimumMyelinDiameter ) *
                               ranvierNodeRadiusRatio );

         _axonRadiusProfiles[ fiberIndex ][ p ] = axonDiameter * 0.5;

         //os << curvilinearAbscissa << " "
         //   << _outerMyelinRadiusProfiles[ fiberIndex ][ p ] << " "
         //   << _axonRadiusProfiles[ fiberIndex ][ p ]
         //   << std::endl;

         previousPoint = point;

      }

    }

  }
  GKG_CATCH( "void gkg::FiberPopulation::createRanvierNodes()" );

}




float gkg::FiberPopulation::getRanvierNodeRadius(
                                             float curvilinearAbscissa,
                                             float internodalLength,
                                             float nodeWidth,
                                             float varyingPortionLength,
                                             float plateauLength ) const
{

  try
  {

    float ratio = 0.0f;
    float modulo = fmodf( curvilinearAbscissa, internodalLength );
    float exponentialDecayFactor = 5.0f;

    if ( modulo < ( internodalLength - nodeWidth ) )
    {

      if ( modulo < varyingPortionLength )
      {

        ratio = ( 1.0f - std::exp( -exponentialDecayFactor * modulo 
                                       / varyingPortionLength ) ) / 
                ( 1.0f - std::exp( -exponentialDecayFactor ) );



      }
      else if ( modulo > ( varyingPortionLength + plateauLength ) )
      {

        ratio = ( 1.0f - std::exp( -exponentialDecayFactor * 
                                     ( 2 * varyingPortionLength + 
                                             plateauLength - modulo ) /
                                             varyingPortionLength ) ) / 
                ( 1.0f - std::exp( -exponentialDecayFactor ) );

      }
      else
      {

        ratio = 1.0f;

      }
      
    }

    return ratio;

  }
  GKG_CATCH( "float gkg::FiberPopulation::getRanvierNodeRadius( "
             "float curvilinearAbscissa, "
             "float internodalLength, "
             "float nodeWidth, "
             "float impactedPortionLength ) const" );

}


void gkg::FiberPopulation::createBeadings()
{

  try
  {

    // collecting the fiber count
    int32_t fiberCount = this->getFiberCount();

    int32_t fiberIndex = 0;
    //int32_t fiberCentroidPointCount = 0;
    float formerOuterMyelinRadius = 0.0f;
    float newOuterMyelinRadius = 0.0f;
    float interBeadingLength = 0.0f;
    float beadingMagnitudeRatio = 0.0f;
    float beadingWidth = 0.0f;
    float curvilinearAbscissa = 0.0f;
    float offset = 0.0f;
    gkg::Vector3d< float > currentPoint;
    gkg::Vector3d< float > nextPoint;
    int32_t p = 0;
    int32_t originalFiberCentroidPointCount = 0;
    //int32_t superResolvedFiberCentroidPointCount = 0;
    float beadingRadiusRatio = 0.0f;
    //int32_t newFiberCentroidPointCount = 0;
    gkg::Vector3d< float > previousPoint;
    //int32_t resolutionFactor = 1; // IMPORTANT CHANGES MUST BE DONE IF
                                  // resolutionFactor != 1.0f
    for ( fiberIndex = 0; fiberIndex < fiberCount; fiberIndex++ )
    {

      // computing the interbeading length
      interBeadingLength = this->getRandomInterbeadingLength();

      // computing the beading magnitude ratio
      beadingMagnitudeRatio = this->getRandomBeadingMagnitudeRatio();

      // computing beading width
      //beadingWidth = this->getRandomBeadingWidth(); // TO BE CONFIRMED WITH CYRIL
      beadingWidth = ( float )( interBeadingLength / 2.0f );

      const gkg::LightCurve3d< float >&
        originalCentroid = _fiberCentroids[ fiberIndex ];
      
      originalFiberCentroidPointCount = originalCentroid.getPointCount();

      // computing offset
      offset = ( float )_factory->getUniformRandomNumber(
                                          *_randomGenerator,
                                          0.0,
                                          ( double )interBeadingLength / 2.0 );
/*
      // estimating boundaries of beadings
      superResolvedFiberCentroidPointCount = 
                                           originalFiberCentroidPointCount * 
                                                              resolutionFactor;
      gkg::LightCurve3d< float > superResolvedCentroid =
                                          originalCentroid.getEquidistantCurve( 
                                         superResolvedFiberCentroidPointCount );

      std::vector< bool > 
        belongsToABeading( superResolvedFiberCentroidPointCount, false );

      curvilinearAbscissa = offset;
      for ( p = 0; p < superResolvedFiberCentroidPointCount; p++ )
      {

        currentPoint = superResolvedCentroid.getPoint( p );
        if ( p < superResolvedFiberCentroidPointCount - 1 )
        {

          nextPoint = superResolvedCentroid.getPoint( p + 1 );
          curvilinearAbscissa += ( nextPoint - currentPoint ).getNorm();

        }

        beadingRadiusRatio = this->getBeadingRadius( curvilinearAbscissa,
                                                     interBeadingLength,
                                                     beadingMagnitudeRatio,
                                                     beadingWidth );
        belongsToABeading[ p ] = ( ( beadingRadiusRatio > 1.0 ) ? 
                                       true : false );
        if ( ( p > 0 ) && belongsToABeading[ p ] )
        {

          belongsToABeading[ p - 1 ] = true;


        }
        else if ( ( ( p + 1 ) < fiberCentroidPointCount ) &&
                   belongsToABeading[ p ] )
        {

          belongsToABeading[ p + 1 ] = true;


        }        

      }

      // resampling outer myelin membrane around beadings
      std::map< float, gkg::Vector3d< float > > pointMap;
      curvilinearAbscissa = 0.0;
      previousPoint = originalCentroid.getPoint( 0 );
      for ( p = 0; p < originalFiberCentroidPointCount; p++ )
      {

         const gkg::Vector3d< float >&
           point = originalCentroid.getPoint( p );
         curvilinearAbscissa += ( point - previousPoint ).getNorm();

         pointMap[ curvilinearAbscissa ] = point;

         previousPoint = point;

      }

      curvilinearAbscissa = 0.0;
      previousPoint = superResolvedCentroid.getPoint( 0 );
      for ( p = 0; p < superResolvedFiberCentroidPointCount; p++ )
      {

         const gkg::Vector3d< float >&
           point = superResolvedCentroid.getPoint( p );
         curvilinearAbscissa += ( point - previousPoint ).getNorm();

         if ( belongsToABeading[ p ] )
         {

           pointMap[ curvilinearAbscissa ] = point;

        }

         previousPoint = point;

      }

      // creating the new centroid
      std::list< gkg::Vector3d< float > > points;
      std::map< float, gkg::Vector3d< float > >::const_iterator
        pm = pointMap.begin(),
        pme = pointMap.end();

      while ( pm != pme )
      {

        points.push_back( pm->second );
        ++ pm;

      }

      _fiberCentroids[ fiberIndex ] = gkg::LightCurve3d< float >( points );

      // resizing consistently the outer myelin membrane profile and the axon 
      // membrane profile
      _outerMyelinRadiusProfiles[ fiberIndex ].resize( points.size() );
      _axonRadiusProfiles[ fiberIndex ].resize( points.size() );


      // computing the outer myelin membrane profile and the axon membrane
      // profile
      const gkg::LightCurve3d< float >&
        newCentroid = _fiberCentroids[ fiberIndex ];

      //std::string filename = "profile_";
      //filename += gkg::StringConverter::toString( fiberIndex + 1 );
      //std::ofstream os( filename.c_str() );
      int32_t newFiberCentroidPointCount = ( int32_t )points.size();
      curvilinearAbscissa = offset;
      previousPoint = newCentroid.getPoint( 0 );
      for ( p = 0; p < newFiberCentroidPointCount; p++ )
      {

        const gkg::Vector3d< float >&
          point = newCentroid.getPoint( p );
*/

      curvilinearAbscissa = offset;
      for ( p = 0; p < originalFiberCentroidPointCount; p++ )
      {

        const gkg::Vector3d< float >&
          point = originalCentroid.getPoint( p );

        curvilinearAbscissa += ( point - previousPoint ).getNorm();

        beadingRadiusRatio = this->getBeadingRadius( curvilinearAbscissa,
                                                     interBeadingLength,
                                                     beadingMagnitudeRatio,
                                                     beadingWidth );

        formerOuterMyelinRadius = _outerMyelinRadiusProfiles[ fiberIndex ][ p ];
        newOuterMyelinRadius = formerOuterMyelinRadius * beadingRadiusRatio;

        _outerMyelinRadiusProfiles[ fiberIndex ][ p ] = newOuterMyelinRadius; 

        _axonRadiusProfiles[ fiberIndex ][ p ] += ( newOuterMyelinRadius - 
                                                    formerOuterMyelinRadius );

        //os << curvilinearAbscissa << " "
        //   << _outerMyelinRadiusProfiles[ fiberIndex ][ p ] << " "
        //   << _axonRadiusProfiles[ fiberIndex ][ p ]
        //   << std::endl;

        previousPoint = point;

      }

    }

  }
  GKG_CATCH( "void gkg::FiberPopulation::createBeadings()" );

}


float gkg::FiberPopulation::getBeadingRadius( 
                                            float curvilinearAbscissa,
                                            float interBeadingLength,
                                            float beadingMagnitudeRatio,
                                            float beadingWidth ) const
{

  try
  {

    float ratio = 0.0f;
    float modulo = fmodf( curvilinearAbscissa, interBeadingLength );
    //float exponentialDecayFactor = 1.0f;
    float halfBeadingWidth = ( float )( beadingWidth / 2.0f );

    if ( modulo <= halfBeadingWidth )
    {

/*
      ratio = 1.0f + ( beadingMagnitudeRatio - 1.0f ) * 
                     ( 1.0f - std::exp( -exponentialDecayFactor * modulo 
                                       / halfBeadingWidth ) ) / 
                     ( 1.0f - std::exp( -exponentialDecayFactor ) );
*/

      ratio = 1.0f + ( beadingMagnitudeRatio - 1.0f ) * 
                     std::sin( M_PI / 2.0 * modulo / halfBeadingWidth );      

    }
    else if ( modulo < beadingWidth )
    {

/*
      ratio = 1.0f + ( beadingMagnitudeRatio - 1.0f ) * 
                     ( 1.0f - std::exp( -exponentialDecayFactor * 
                       ( beadingWidth - modulo ) / halfBeadingWidth ) ) / 
                     ( 1.0f - std::exp( -exponentialDecayFactor ) );
*/

      ratio = 1.0f + ( beadingMagnitudeRatio - 1.0f ) * 
                     std::sin( M_PI / 2.0 * ( beadingWidth - modulo ) /
                                                            halfBeadingWidth ); 

    }
    else
    {

      ratio = 1.0f;

    }

    return ratio;

  }
  GKG_CATCH( "float gkg::FiberPopulation::getBeadingRadius( "
             "float curvilinearAbscissa, "
             "float beadingLength, "
             "float beadingMagnitudeRatio, "
             "float beadingWidth ) const" );

}
