#ifndef _gkg_dmri_plugin_functors_DwiComplexGeometrySimulator_FiberPopulation_h_
#define _gkg_dmri_plugin_functors_DwiComplexGeometrySimulator_FiberPopulation_h_


#include <gkg-processing-container/BoundingBox.h>
#include <gkg-processing-container/LightCurve3d.h>
#include <gkg-processing-coordinates/Vector3d.h>
#include <gkg-processing-container/Volume.h>
#include <gkg-processing-mesh/RayBoxIntersection.h>
#include <gkg-processing-numericalanalysis/NumericalAnalysisSelector.h>
#include <gkg-core-pattern/RCPointer.h>

#include <vector>
#include <map>


namespace gkg
{


class FiberPopulation
{

  public:

    FiberPopulation( const BoundingBox< float >& fieldOfView,
                     const BoundingBox< float >& extendedFieldOfView,
                     float outerMyelinMembraneDiameterMean,
                     float outerMyelinMembraneDiameterVariance,
                     float gRatioMean,
                     float gRatioVariance,
                     float internodalLengthToNodeWidthRatioMean,
                     float internodalLengthToNodeWidthRatioVariance,
                     float interbeadingLengthMean,
                     float interbeadingLengthVariance,
                     float beadingMagnitudeRatioMean,
                     float beadingMagnitudeRatioVariance,
                     const Vector3d< float >& meanOrientation,
                     float targetIntracellularVolumeFraction,
                     float angularVariation,
                     float targetAngularDispersion,
                     float tortuosityVariation,
                     float maximumFiberTortuosity,
                     int32_t tortuosityNeighborhoodSize,
                     float fiberResolutionAlongAxis,
                     int32_t crossSectionVertexCount,
                     RCPointer< RandomGenerator > randomGenerator );
    FiberPopulation( const FiberPopulation& other );
    virtual ~FiberPopulation();

    FiberPopulation& operator=( const FiberPopulation& other );

    const BoundingBox< float >& getFieldOfView() const;
    const BoundingBox< float >& getExtendedFieldOfView() const;

    float getOuterMyelinMembraneDiameterMean() const;
    float getOuterMyelinMembraneDiameterVariance() const;

    float getGRatioMean() const;
    float getGRatioVariance() const;
    float getInternodalLengthToNodeWidthRatioMean() const;
    float getInternodalLengthToNodeWidthRatioVariance() const;

    float getInterbeadingLengthMean() const;
    float getInterbeadingLengthVariance() const;
    float getBeadingMagnitudeRatioMean() const;
    float getBeadingMagnitudeRatioVariance() const;

    const Vector3d< float >& getMeanOrientation() const;
    float getTargetIntracellularVolumeFraction() const;
    float getAngularVariation() const;
    float getTargetAngularDispersion() const;
    float getTortuosityVariation() const;
    float getMaximumFiberTortuosity() const;
    int32_t getTortuosityNeighborhoodSize() const;
    float getFiberResolutionAlongAxis() const;
    int32_t getCrossSectionVertexCount() const;

    float getCurrentIntracellularVolumeFraction() const;
    float getCurrentAngularDispersion() const;
    bool hasReachedTargetIntracellularVolumeFraction() const;
    bool hasReachedTargetAngularDispersion() const;

    float getRandomOuterMyelinMembraneDiameter() const;
    float getRandomGRatio() const;
    float getRandomInternodalLengthToNodeWidthRatio() const;
    float getRandomInterbeadingLength() const;
    float getRandomBeadingMagnitudeRatio() const;

    void addFiber( const Vector3d< float >& currentPosition,
                   float currentDiameter );
    void setFullResolutionFibers();

    int32_t getFiberCount() const;
    const LightCurve3d< float >&
      getFiberCentroid( int32_t fiberIndex ) const;
    const std::vector< float >& 
      getOuterMyelinRadiusProfile( int32_t fiberIndex ) const;
    const std::vector< float >&
      getAxonRadiusProfile( int32_t fiberIndex ) const;

    const std::vector< LightCurve3d< float > >&
      getFiberCentroids() const;
    const std::vector< std::vector< float > >&
      getOuterMyelinRadiusProfiles() const;
    const std::vector< std::vector< float > >&
      getAxonRadiusProfiles() const;

    bool induceGlobalDispersionOfARandomFiber(
              const std::vector< gkg::RCPointer< gkg::FiberPopulation > >&
                                                            fiberPopulations );
    void scaleAngularDeviationsWithFiberSubdivisionCount();
    bool induceTortuosityOfARandomFiber(
              const std::vector< gkg::RCPointer< gkg::FiberPopulation > >&
                                                            fiberPopulations,
              Volume< std::map< std::pair< int32_t, int32_t >,
                                std::set< int32_t > > >& lut,
              const Vector3d< int32_t >& lutSize,
              const Vector3d< float >& lutResolution,
              float minimumLutResolution,
              const Vector3d< int32_t >& voxelRadiusOfInfluence,
              const BoundingBox< int32_t >& voxelBoundingBox );

    void createAxonalMembranes();
    void createRanvierNodes();
    void createBeadings();

  protected:


    float getRanvierNodeRadius( float curvilinearAbscissa,
                                float internodalLength,
                                float nodeWidth,
                                float varyingPortionLength,
                                float plateauLength ) const;
    float getBeadingRadius( float curvilinearAbscissa,
                            float interBeadingLength,
                            float beadingMagnitudeRatio,
                            float beadingWidth ) const;


    BoundingBox< float > _fieldOfView;
    BoundingBox< float > _extendedFieldOfView;
    float _outerMyelinMembraneDiameterMean;
    float _outerMyelinMembraneDiameterVariance;
    float _gRatioMean;
    float _gRatioVariance;
    float _internodalLengthToNodeWidthRatioMean;
    float _internodalLengthToNodeWidthRatioVariance;
    float _interbeadingLengthMean;
    float _interbeadingLengthVariance;
    float _beadingMagnitudeRatioMean;
    float _beadingMagnitudeRatioVariance;
    Vector3d< float > _meanOrientation;
    float _targetIntracellularVolumeFraction;
    float _angularVariation;
    float _targetAngularDispersion;
    float _tortuosityVariation;
    float _maximumFiberTortuosity;
    int32_t _tortuosityNeighborhoodSize;
    float _fiberResolutionAlongAxis;
    int32_t _crossSectionVertexCount;


    NumericalAnalysisImplementationFactory* _factory;
    RCPointer< RandomGenerator > _randomGenerator;

    float _outerMyelinMembraneDiameterScale;
    float _outerMyelinMembraneDiameterShape;
    float _gRatioScale;
    float _gRatioShape;
    float _internodalLengthToNodeWidthRatioScale;
    float _internodalLengthToNodeWidthRatioShape;
    float _interbeadingLengthScale;
    float _interbeadingLengthShape;
    float _beadingMagnitudeRatioScale;
    float _beadingMagnitudeRatioShape;

    float _fieldOfViewVolume;
    float _currentIntracellularVolumeFraction;

    std::vector< LightCurve3d< float > > _fiberCentroids;
    std::vector< std::vector< float > > _outerMyelinRadiusProfiles;
    std::vector< std::vector< float > > _axonRadiusProfiles;

    RayBoxIntersection _rayBoxIntersection;

    std::vector< float > _angularDeviations;
    float _currentAngularDispersion;

};


}


#endif
