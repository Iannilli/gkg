#include <gkg-dmri-plugin-functors/DwiComplexGeometrySimulator/HDF5Writer.h>

#include <hdf5.h>

#include <sstream>
#include <string>
#include <iostream>


gkg::HDF5Writer::HDF5Writer()
{
}


gkg::HDF5Writer::~HDF5Writer()
{
}


bool gkg::HDF5Writer::write( 
           const std::string& fileName,
           const std::vector< gkg::RCPointer< gkg::FiberPopulation > >&
                fiberPopulations )
{

  if ( !fiberPopulations.size() )
  {

    return false;

  }

  herr_t status;
  hsize_t dims[ 2 ];
  hid_t strtype = H5Tcopy( H5T_C_S1 );

  hid_t fileId = H5Fcreate( fileName.c_str(),
                            H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT );

  std::string attributeString( "membrane\0" );
  status = H5Tset_size( strtype, attributeString.length() );
  dims[ 0 ] = 1;
  hid_t dataspaceId = H5Screate_simple( 1, dims, NULL );
  hid_t attributeId = H5Acreate2( fileId, "Content type",
  			          strtype, dataspaceId,
  			          H5P_DEFAULT, H5P_DEFAULT );

  status = H5Awrite( attributeId, strtype, &attributeString[ 0 ] );
  status = H5Aclose( attributeId );
  status = H5Sclose( dataspaceId );

  hid_t tissueId = H5Gcreate( fileId, "/tissue", 
                              H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

  hid_t populationId = H5Gcreate( tissueId, "cell_populations", 
                                  H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

  hid_t bundlesId = H5Gcreate2( populationId, "fiber_bundles", 
                              H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

  std::vector< gkg::RCPointer< gkg::FiberPopulation > >::const_iterator
    f = fiberPopulations.begin(),
    fe = fiberPopulations.end();
  uint32_t population = 1;
  std::ostringstream oss;

  while ( f != fe )
  {

    std::vector< gkg::LightCurve3d< float > >::const_iterator
      c = (*f)->getFiberCentroids().begin(),
      ce = (*f)->getFiberCentroids().end();
    std::vector< std::vector< float > >::const_iterator
      omr = (*f)->getOuterMyelinRadiusProfiles().begin(),
      omre = (*f)->getOuterMyelinRadiusProfiles().end();
    std::vector< std::vector< float > >::const_iterator
      ar = (*f)->getAxonRadiusProfiles().begin(),
      are = (*f)->getAxonRadiusProfiles().end();
    int32_t index = 1;

    oss.str( "" );
    oss << population;

    hid_t groupId = H5Gcreate2( bundlesId, oss.str().c_str(), 
                                H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

    while ( c != ce )
    {

      oss.str( "" );
      oss << index;

      // create fiber group
      hid_t fiberId = H5Gcreate2( groupId, oss.str().c_str(), 
                                  H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

      // create layer group
      hid_t layerId = H5Gcreate2( fiberId, "layers", 
                                  H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT );
      
      if ( omr != omre )
      {

        // create outer myelin membrane group
        std::string layerName( "Outer myelin membrane" );
        status = H5Tset_size( strtype, layerName.length() );
        dims[ 0 ] = 1;

        hid_t layer1Id = H5Gcreate2( layerId, "1", 
                                     H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT );

        // add an attribute to specify its name
        dataspaceId = H5Screate_simple( 1, dims, NULL );
        attributeId = H5Acreate2( layer1Id, "Name",
                                  strtype, dataspaceId,
                                  H5P_DEFAULT, H5P_DEFAULT );

        status = H5Awrite( attributeId, strtype, layerName.c_str() );
        status = H5Aclose( attributeId );
        status = H5Sclose( dataspaceId );

        // add points to the outer myelin layer
        dims[ 0 ] = c->getPointCount();
        dims[ 1 ] = 3;
        dataspaceId = H5Screate_simple( 2, dims, NULL );
        hid_t datasetId = H5Dcreate2( layer1Id, "points",
                                      H5T_NATIVE_FLOAT, dataspaceId,
                                      H5P_DEFAULT, H5P_DEFAULT,
                                      H5P_DEFAULT);
        status = H5Dwrite( datasetId, H5T_NATIVE_FLOAT, 
                           H5S_ALL, H5S_ALL, H5P_DEFAULT,
                           &c->getPoints()[ 0 ] );
        status = H5Sclose( dataspaceId );
        status = H5Dclose( datasetId );

        // add radii to the outer myelin layer
        dims[ 0 ] = int32_t( omr->size() );
        dataspaceId = H5Screate_simple( 1, dims, NULL );
        datasetId = H5Dcreate2( layer1Id, "radii",
                                H5T_NATIVE_FLOAT, dataspaceId,
                                H5P_DEFAULT, H5P_DEFAULT,
                                H5P_DEFAULT);
        status = H5Dwrite( datasetId, H5T_NATIVE_FLOAT, 
                           H5S_ALL, H5S_ALL, H5P_DEFAULT,
                           &(*omr)[ 0 ] );
        status = H5Sclose( dataspaceId );
        status = H5Dclose( datasetId );
        status = H5Gclose( layer1Id );
        ++omr;

      }

      if ( ar != are )
      {

        // create axon membrane group
        std::string layerName = ( "Axon membrane" );
        status = H5Tset_size( strtype, layerName.length() );
        dims[ 0 ] = 1;

        hid_t layer2Id = H5Gcreate2( layerId, "2", 
                                     H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT );

        // add an attribute to specify its name
        dataspaceId = H5Screate_simple( 1, dims, NULL );
        attributeId = H5Acreate2( layer2Id, "Name",
                             			strtype, dataspaceId,
        			                    H5P_DEFAULT, H5P_DEFAULT );

        status = H5Awrite( attributeId, strtype, layerName.c_str() );
        status = H5Aclose( attributeId );
        status = H5Sclose( dataspaceId );

        // add points to the axon layer
        dims[ 0 ] = c->getPointCount();
        dims[ 1 ] = 3;
        dataspaceId = H5Screate_simple( 2, dims, NULL );
        hid_t datasetId = H5Dcreate2( layer2Id, "points",
                                      H5T_NATIVE_FLOAT, dataspaceId,
                                      H5P_DEFAULT, H5P_DEFAULT,
                                      H5P_DEFAULT);
        status = H5Dwrite( datasetId, H5T_NATIVE_FLOAT, 
                           H5S_ALL, H5S_ALL, H5P_DEFAULT,
                           &c->getPoints()[ 0 ] );
        status = H5Sclose( dataspaceId );
        status = H5Dclose( datasetId );

        // add radii to the axon layer
        dims[ 0 ] = int32_t( ar->size() );
        dataspaceId = H5Screate_simple( 1, dims, NULL );
        datasetId = H5Dcreate2( layer2Id, "radii",
                                H5T_NATIVE_FLOAT, dataspaceId,
                                H5P_DEFAULT, H5P_DEFAULT,
                                H5P_DEFAULT);
        status = H5Dwrite( datasetId, H5T_NATIVE_FLOAT, 
                           H5S_ALL, H5S_ALL, H5P_DEFAULT,
                           &(*ar)[ 0 ] );
        status = H5Sclose( dataspaceId );
        status = H5Dclose( datasetId );
        status = H5Gclose( layer2Id );
        ++ar;

      }

      status = H5Gclose( layerId );

      // close the fiber group
      status = H5Gclose( fiberId );
      index++;
      ++c;

    }

    status = H5Gclose( groupId );

    population++;
    ++f;

  }

  status = H5Gclose( bundlesId );
  status = H5Gclose( populationId );
  status = H5Gclose( tissueId );
  status = H5Fclose( fileId );

  return ( status < 0 ) ? false : true;

}
