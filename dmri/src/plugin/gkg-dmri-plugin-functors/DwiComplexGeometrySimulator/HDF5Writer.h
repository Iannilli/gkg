#ifndef _HDF5Writer_h_
#define _HDF5Writer_h_


#include <gkg-dmri-plugin-functors/DwiComplexGeometrySimulator/FiberPopulation.h>
#include <gkg-core-pattern/RCPointer.h>

#include <string>
#include <vector>


namespace gkg
{


class HDF5Writer
{

  public:

    HDF5Writer();
    virtual ~HDF5Writer();

    bool write( const std::string& fileName,
                const std::vector< gkg::RCPointer< gkg::FiberPopulation > >&
                  fiberPopulations );

};


}


#endif

