#include <gkg-dmri-plugin-functors/DwiFiberLabelling/DwiFiberLabellingCommand.h>
#include <gkg-communication-command/CommandFactory.h>
#include <gkg-communication-getopt/Application.h>
#include <gkg-dmri-container/BundleMap_i.h>
#include <gkg-processing-container/Volume_i.h>
#include <gkg-core-io/Reader_i.h>
#include <gkg-core-io/Writer_i.h>
#include <gkg-core-io/StringConverter.h>
#include <gkg-core-io/BaseObjectReader.h>
#include <gkg-core-object/GenericObject_i.h>
#include <gkg-core-object/TypedObject_i.h>
#include <gkg-core-object/Dictionary.h>
#include <gkg-core-pattern/RCPointer.h>
#include <gkg-processing-process/Process.h>
#include <gkg-dmri-bundlemap-operator/BundleMapOperatorFactory.h>
#include <gkg-processing-transform/IdentityTransform3d.h>
#include <gkg-processing-transform/NonLinearTransform3d.h>
#include <gkg-communication-sysinfo/Directory.h>
#include <gkg-core-io/Eraser.h>
#include <gkg-core-exception/Exception.h>
#include <string>
#include <iostream>
#include <map>
#include <iostream>
#include <fstream>
#include <iomanip>


class FiberLabellingProcess : public gkg::Process
{

  public:

    FiberLabellingProcess( 
       const std::vector< std::string >& theFileNameInputBundleMaps,
       const std::string& theFileNameAtlasBundleMap,
       const std::vector< std::string >& theFileNameTransform3dBundleMapToAtlas,
       const std::string& theFileNameMaximumDistanceToBundleMap,
       int32_t theFiberResamplingPointCount,
       float theDensityMaskFiberResamplingStep, 
       const gkg::Vector3d< double >& theDensityMaskResolution, 
       const std::string& theOutputDirectoryName,
       bool theIntermediate,
       bool theSingleFile,
       bool theComputeDensityMasks,
       bool theSaveDiscarded,
       const std::string& theOutputVolumeFormat,
       const std::string& theOutputBundleFormat,
       bool theAscii,
       bool theVerbose );

    const std::vector< std::string >& fileNameInputBundleMaps;
    const std::string& fileNameAtlasBundleMap;
    const std::vector< std::string >& fileNameTransform3dBundleMapToAtlas; 
    const std::string& fileNameMaximumDistanceToBundleMap;
    int32_t fiberResamplingPointCount;
    float densityMaskFiberResamplingStep;
    const gkg::Vector3d< double >& densityMaskResolution;
    const std::string& outputDirectoryName;
    bool intermediate;
    bool singleFile;
    bool computeDensityMasks;
    bool saveDiscarded;
    const std::string& outputVolumeFormat;
    const std::string& outputBundleFormat;
    bool ascii;
    bool verbose;

  private:

    template < class L >
    static void label( gkg::Process& process,
                       const std::string& fileNameIn,
                       const gkg::AnalyzedObject&,
                       const std::string& );

    static std::string getLabel(
                       const gkg::LightCurve3d< float >& fiber,
                       const gkg::BundleMap< std::string >& bundleMapAtlas,
                       const std::map< std::string, float >& maximumDistances );

};


FiberLabellingProcess::FiberLabellingProcess(
       const std::vector< std::string >& theFileNameInputBundleMaps,
       const std::string& theFileNameAtlasBundleMap,
       const std::vector< std::string >& theFileNameTransform3dBundleMapToAtlas,
       const std::string& theFileNameMaximumDistanceToBundleMap,
       int32_t theFiberResamplingPointCount,
       float theDensityMaskFiberResamplingStep, 
       const gkg::Vector3d< double >& theDensityMaskResolution, 
       const std::string& theOutputDirectoryName,
       bool theIntermediate,
       bool theSingleFile,
       bool theComputeDensityMasks,
       bool theSaveDiscarded,
       const std::string& theOutputVolumeFormat,
       const std::string& theOutputBundleFormat,
       bool theAscii,
       bool theVerbose )
          : gkg::Process( "BundleMap" ),
            fileNameInputBundleMaps(  theFileNameInputBundleMaps ),
            fileNameAtlasBundleMap( theFileNameAtlasBundleMap ),
            fileNameTransform3dBundleMapToAtlas(
                                       theFileNameTransform3dBundleMapToAtlas ),
            fileNameMaximumDistanceToBundleMap( 
                                        theFileNameMaximumDistanceToBundleMap ),
            fiberResamplingPointCount( theFiberResamplingPointCount ),
            densityMaskFiberResamplingStep( theDensityMaskFiberResamplingStep ),
            densityMaskResolution( theDensityMaskResolution ),
            outputDirectoryName( theOutputDirectoryName ),
            intermediate( theIntermediate ),
            singleFile( theSingleFile ),
            computeDensityMasks( theComputeDensityMasks ),
            saveDiscarded( theSaveDiscarded ),
            outputVolumeFormat( theOutputVolumeFormat ),
            outputBundleFormat( theOutputBundleFormat ),
            ascii( theAscii ),
            verbose( theVerbose )

{

  try
  {

    registerProcess(
               "BundleMap", gkg::TypeOf< std::string >::getName(),
               &FiberLabellingProcess::label< std::string > );
    registerProcess(
               "BundleMap", gkg::TypeOf< int16_t >::getName(),
               &FiberLabellingProcess::label< int16_t > );

  }
  GKG_CATCH( "FiberLabellingProcess::"
             "FiberLabellingProcess( "
             "const std::vector< std::string >& theFileNameInputBundleMaps, "
             "const std::string& theFileNameAtlasBundleMap, "
             "const std::vector< std::string >& "
             "theFileNameTransform3dBundleMapToAtlas, "
             "const std::string& theFileNameMaximumDistanceToBundleMap, "
             "int32_t theFiberResamplingPointCount, "
             "float theDensityMaskFiberResamplingStep, "
             "const Vector3d< double >& theDensityMaskResolution, "
             "const std::string& theOutputDirectoryName, "
             "bool theIntermediate, "
             "bool theSingleFile, "
             "bool theComputeDensityMasks, "
             "bool theSaveDiscarded, "
             "const std::string& theOutputVolumeFormat, "
             "const std::string& theOutputBundleFormat, "
             "bool theAscii, "
             "bool theVerbose )" );

}


template < class L >
void FiberLabellingProcess::label( gkg::Process& process,
                                   const std::string& /* fileNameIn */,
                                   const gkg::AnalyzedObject&,
                                   const std::string& )
{

  try
  {

    FiberLabellingProcess&
      fiberLabellingProcess = static_cast< FiberLabellingProcess& >( process );

    const std::vector< std::string >&
      fileNameInputBundleMaps =
        fiberLabellingProcess.fileNameInputBundleMaps;
    const std::string&
      fileNameAtlasBundleMap =
        fiberLabellingProcess.fileNameAtlasBundleMap;
    const std::vector< std::string >&
      fileNameTransform3dBundleMapToAtlas =
        fiberLabellingProcess.fileNameTransform3dBundleMapToAtlas;
    const std::string&
      fileNameMaximumDistanceToBundleMap =
        fiberLabellingProcess.fileNameMaximumDistanceToBundleMap;
    const int32_t&
      fiberResamplingPointCount =
        fiberLabellingProcess.fiberResamplingPointCount;
    const float&
      densityMaskFiberResamplingStep = 
        fiberLabellingProcess.densityMaskFiberResamplingStep;
    const gkg::Vector3d< double >& 
      densityMaskResolution =
        fiberLabellingProcess.densityMaskResolution;
    const std::string&
      outputDirectoryName =
        fiberLabellingProcess.outputDirectoryName;
    const bool&
      singleFile =
        fiberLabellingProcess.singleFile;
    const bool&
      computeDensityMasks =
        fiberLabellingProcess.computeDensityMasks;
    const bool&
      saveDiscarded =
        fiberLabellingProcess.saveDiscarded;
    const std::string&
      outputVolumeFormat =
        fiberLabellingProcess.outputVolumeFormat;
    const std::string&
      outputBundleFormat =
        fiberLabellingProcess.outputBundleFormat;
    const bool&
      ascii =
        fiberLabellingProcess.ascii;
    const bool&
      verbose =
        fiberLabellingProcess.verbose;


    ////////////////////////////////////////////////////////////////////////////
    // reading bundle atlas
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

      std::cout << "reading bundle atlas : "
                << std::flush;

    }

    gkg::RCPointer< gkg::BundleMap< std::string > > 
      bundleMapAtlas( new gkg::BundleMap< std::string > );
    gkg::Reader::getInstance().read( fileNameAtlasBundleMap, *bundleMapAtlas );

    std::vector< std::string > 
      atlasBundleLabels( bundleMapAtlas->getBundleCount() );
    std::map< std::string, int32_t > 
      lutAtlasBundleLabelToIndex;

    int32_t atlasBundleLabelIndex = 0;
    gkg::BundleMap< std::string >::const_iterator
      ab = bundleMapAtlas->begin(),
      abe = bundleMapAtlas->end();
    while ( ab != abe )
    {

      atlasBundleLabels[ atlasBundleLabelIndex ] = ab->first;
      lutAtlasBundleLabelToIndex[ ab->first ] = atlasBundleLabelIndex;
      ++ ab;
      ++ atlasBundleLabelIndex;

    }
    lutAtlasBundleLabelToIndex[ "discarded" ] = -1;

    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }


    ////////////////////////////////////////////////////////////////////////////
    // resampling it to 'fiberResamplingPointCount' point(s)
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

      std::cout << "resampling atlas fibers to "
                << fiberResamplingPointCount
                << " points : "
                << std::flush;

    }

    gkg::RCPointer< gkg::BundleMap< std::string > > resampledBundleMapAtlas(
                                            new gkg::BundleMap< std::string > );

    std::vector< std::string > stringParameters( 1U );
    stringParameters[ 0 ] = "constant-point-count";
    std::vector< double > scalarParameters( 1U );
    scalarParameters[ 0 ] = ( double )fiberResamplingPointCount;
    gkg::RCPointer< gkg::BundleMapOperator< std::string > >
      bundleMapResampler(
         gkg::BundleMapOperatorFactory< std::string >::getInstance().create(
                                                     "resampler",
                                                     stringParameters,
                                                     scalarParameters,
                                                     verbose ) );
    bundleMapResampler->endContribution( bundleMapAtlas,
                                         resampledBundleMapAtlas );

    bundleMapAtlas.reset();

    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }


    ////////////////////////////////////////////////////////////////////////////
    // reading maximum distance to bundle map parameter file
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

      std::cout << "reading dictionary of maximum distance to bundles : "
                << std::flush;

    }

    gkg::BaseObjectReader maximumDistanceToBundleMapReader;
    std::ifstream is( fileNameMaximumDistanceToBundleMap.c_str() );

    gkg::TypedObject< gkg::Dictionary > dictionary;
    maximumDistanceToBundleMapReader.read( is, dictionary );
    is.close();

    // converting the dictionary to a map of bundle string -> maximum distance
    std::map< std::string, float > maximumSquareDistances;
    gkg::Dictionary::iterator b = dictionary.getTypedValue().begin(),
                              be = dictionary.getTypedValue().end();
    while ( b != be )
    {

      if ( verbose )
      {

        std::cout << b->first << ":" << b->second->getScalar() << " ";

      }
      const std::string& bundleName = b->first;
      maximumSquareDistances[ bundleName ] = ( float )b->second->getScalar() *
                                             ( float )b->second->getScalar();
      ++ b;

    }

    if ( verbose )
    {

      std::cout << std::endl;

    }


    ////////////////////////////////////////////////////////////////////////////
    // allocation bundle map to atlas 3D transformation
    ////////////////////////////////////////////////////////////////////////////

    gkg::RCPointer< gkg::Transform3d< float > > transform3d;

    if ( fileNameTransform3dBundleMapToAtlas.empty() )
    {

      if ( verbose )
      {

      std::cout << "setting 3D transformation to identity : "
                << std::flush;

      }
      transform3d.reset( new gkg::IdentityTransform3d< float > );

    }
    else
    {

      gkg::RCPointer< gkg::NonLinearTransform3d< float > > 
        nonLinearTransform3d( new gkg::NonLinearTransform3d< float > );

      if ( fileNameTransform3dBundleMapToAtlas.size() == 1U )
      {

        if ( verbose )
        {

          std::cout << "reading linear 3D transformation : "
                    << std::flush;

        }
        nonLinearTransform3d->readTrm( fileNameTransform3dBundleMapToAtlas[ 0 ],
                                       "",
                                       "" );

      }
      else if ( fileNameTransform3dBundleMapToAtlas.size() == 3U )
      {

        if ( verbose )
        {

          std::cout << "reading non-linear 3D transformation : "
                    << std::flush;

        }
        nonLinearTransform3d->readTrm(
                                     fileNameTransform3dBundleMapToAtlas[ 0 ],
                                     fileNameTransform3dBundleMapToAtlas[ 1 ],
                                     fileNameTransform3dBundleMapToAtlas[ 2 ] );

      }

      transform3d = nonLinearTransform3d;

    }

    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }


    ////////////////////////////////////////////////////////////////////////////
    // looping over bundle map files to label fibers
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

      std::cout << "labelling fibers : "
                << std::flush;

    }

    int32_t fileNameIndex = 0;
    int32_t fileNameCount = ( int32_t )fileNameInputBundleMaps.size();
    int32_t bundleIndex = 0;
    int32_t bundleCount = 0;
    int32_t fiberIndex = 0;
    int32_t fiberCount = 0;

    if ( verbose )
    {

      std::cout << "[ " << std::setw( 3 ) << fileNameIndex
                << " / " << std::setw( 3 ) << fileNameCount
                << " ]" << std::flush;

    }


    std::vector< std::vector< std::vector< int32_t > > >
      labels( fileNameCount );
    std::vector< std::vector< L > > inputBundleLabels( fileNameCount );

    std::vector< std::string >::const_iterator
      fn = fileNameInputBundleMaps.begin(),
      fne = fileNameInputBundleMaps.end();
    while ( fn != fne )
    {

      if ( verbose )
      {

        std::cout << gkg::Eraser( 13 );
        std::cout << "[ " << std::setw( 3 ) << fileNameIndex + 1
                  << " / " << std::setw( 3 ) << fileNameCount
                  << " ]" << std::flush;


      }

      // reading the current bundle map
      gkg::BundleMap< L > inputBundleMap;
      gkg::Reader::getInstance().read( *fn, inputBundleMap );

      bundleIndex = 0;
      bundleCount = inputBundleMap.getBundleCount();
      if ( verbose )
      {

        std::cout << "[ " << std::setw( 3 ) << bundleIndex
                  << " / " << std::setw( 3 ) << bundleCount
                  << " ]" << std::flush;

      }

      labels[ fileNameIndex ].resize( bundleCount );
      inputBundleLabels[ fileNameIndex ].resize( bundleCount );

      // looping over bundles
      typename gkg::BundleMap< L >::const_iterator
        ib = inputBundleMap.begin(),
        ibe = inputBundleMap.end();
      while ( ib != ibe )
      {

        if ( verbose )
        {

          std::cout << gkg::Eraser( 13 );
          std::cout << "[ " << std::setw( 3 ) << bundleIndex + 1
                    << " / " << std::setw( 3 ) << bundleCount
                    << " ]" << std::flush;


        }

        // storing input bundle name
        inputBundleLabels[ fileNameIndex ][ bundleIndex ] = ib->first;

        // resetting fiber index
        fiberIndex = 0;
        fiberCount = ib->second.getCurve3dCount();

        if ( verbose )
        {

            std::cout << "[ " << std::setw( 7 ) << 0
                      << " / " << std::setw( 7 ) << fiberCount
                      << " ]" << std::flush;

        }

        labels[ fileNameIndex ][ bundleIndex ].resize( fiberCount );

        // looping over fibers
        std::vector< gkg::LightCurve3d< float > >::const_iterator
          f = ib->second.begin(),
          fe = ib->second.end();
        while ( f != fe )
        {

          if ( verbose )
          {

            if ( ( ( fiberIndex + 1 ) % 1000 == 0 ) ||
                 ( ( fiberIndex + 1 == fiberCount ) ) )
            {

              
              std::cout << gkg::Eraser( 21 );
              std::cout << "[ " << std::setw( 7 ) << fiberIndex + 1
                        << " / " << std::setw( 7 ) << fiberCount
                        << " ]" << std::flush;

            }

          }

          // resampling the fiber with 'fiberResamplingPointCount' point(s)
          gkg::LightCurve3d< float > resampledFiber = f->getEquidistantCurve(
                                                    fiberResamplingPointCount );

          // applying non linear 3D transformation
          std::list< gkg::Vector3d< float > > points;
          gkg::LightCurve3d< float >::const_iterator p = resampledFiber.begin(),
                                                     pe = resampledFiber.end();
          gkg::Vector3d< float > tp;
          while ( p != pe )
          {

            transform3d->getDirect( *p, tp );
            points.push_back( tp );

            ++ p;

          }
          gkg::LightCurve3d< float > transformedAndResampledFiber( points );

          // looking for the adequate label for the current fiber
          std::string label =
               FiberLabellingProcess::getLabel( transformedAndResampledFiber,
                                                *resampledBundleMapAtlas,
                                                maximumSquareDistances );
          labels[ fileNameIndex ][ bundleIndex ][ fiberIndex ] =
                                            lutAtlasBundleLabelToIndex[ label ];

          ++ f;
          ++ fiberIndex;

        }
        if ( verbose )
        {

          std::cout << gkg::Eraser( 21 );

        }

        ++ ib;

      }
      if ( verbose )
      {

        std::cout << gkg::Eraser( 13 );

      }

      ++ fn;
      ++ fileNameIndex;

    }
    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }


    ////////////////////////////////////////////////////////////////////////////
    // reorganizing labels to extract individual bundle
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

      std::cout << "reorganizing labels to extract individual bundles : "
                << std::flush;

    }

    std::map< std::string, 
              std::vector< std::list< std::pair< int32_t, int32_t > > > >
      labelledBundleMap;

    for ( fileNameIndex = 0; fileNameIndex < fileNameCount; fileNameIndex++ )
    {

      bundleCount = ( int32_t )labels[ fileNameIndex ].size();
      for ( bundleIndex = 0; bundleIndex < bundleCount; bundleIndex++ )
      {

        fiberCount = ( int32_t )labels[ fileNameIndex ][ bundleIndex ].size();
        for ( fiberIndex = 0; fiberIndex < fiberCount; fiberIndex++ )
        {

          atlasBundleLabelIndex = 
                           labels[ fileNameIndex ][ bundleIndex ][ fiberIndex ];
          if ( atlasBundleLabelIndex >= 0 )
          {

            if ( labelledBundleMap.find( atlasBundleLabels[ 
                                            atlasBundleLabelIndex ] ) ==
                 labelledBundleMap.end() )
            {

              labelledBundleMap[ atlasBundleLabels[ atlasBundleLabelIndex ] ] =
                std::vector< std::list< std::pair< int32_t, int32_t > > >(
                      fileNameCount );

            }
            labelledBundleMap[ atlasBundleLabels[ atlasBundleLabelIndex ] ]
                             [ fileNameIndex ].push_back( 
                                  std::pair< int32_t, int32_t >( bundleIndex,
                                                                 fiberIndex ) );
          }
          else
          {

            if ( labelledBundleMap.find( "discarded" ) ==
                 labelledBundleMap.end() )
            {

              labelledBundleMap[ "discarded" ] =
                std::vector< std::list< std::pair< int32_t, int32_t > > >(
                      fileNameCount );

            }
            labelledBundleMap[ "discarded" ]
                             [ fileNameIndex ].push_back( 
                                  std::pair< int32_t, int32_t >( bundleIndex,
                                                                 fiberIndex ) );
 
          }

        }

      }

    }

    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }


    ////////////////////////////////////////////////////////////////////////////
    // extracting individual bundles using the identified labels
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

      std::cout << "extracting and saving individual bundles : "
                << std::flush;

    }

    
    gkg::BundleMap< std::string > singleFileBundleMap;

    std::map< std::string, 
              std::vector< std::list< 
                             std::pair< int32_t, int32_t > > > >::const_iterator
      ob = labelledBundleMap.begin(),
      obe = labelledBundleMap.end();
    while ( ob != obe )
    {

      const std::string& bundleName = ob->first;

      if ( saveDiscarded || ( !saveDiscarded && bundleName != "discarded" ) )
      {

        const std::vector< std::list< std::pair< int32_t, int32_t > > >&
          listOfBundleAndFiberIndices = ob->second;
        std::list< gkg::LightCurve3d< float > > fibers;

        for ( fileNameIndex = 0; fileNameIndex < fileNameCount;
              fileNameIndex++ )
        {

          if ( !listOfBundleAndFiberIndices[ fileNameIndex ].empty() )
          {

            // reading the input bundle map

            gkg::BundleMap< L > inputBundleMap;
            gkg::Reader::getInstance().read(
                                       fileNameInputBundleMaps[ fileNameIndex ],
                                       inputBundleMap );
            std::list< std::pair< int32_t, int32_t > >::const_iterator
              bfi = listOfBundleAndFiberIndices[ fileNameIndex ].begin(),
              bfie = listOfBundleAndFiberIndices[ fileNameIndex ].end();
            while ( bfi != bfie )
            {

              fibers.push_back(
                             inputBundleMap.getBundle(
                             inputBundleLabels[ fileNameIndex ][ bfi->first ] ).
                                                    getCurve3d( bfi->second ) );
              ++ bfi;

            }

          }

        }

        if ( !fibers.empty() )
        {

          if ( !singleFile )
          {

            gkg::BundleMap< std::string > individualBundle;
            individualBundle.addCurve3ds( bundleName, fibers );

            gkg::Directory outputDirectory( outputDirectoryName );
            if ( !outputDirectory.isValid() )
            {

              outputDirectory.mkdir();

            }

            std::string fileNameIndividualBundle =
                                                  outputDirectory.getPath() +
                                                  gkg::getDirectorySeparator() +
                                                  bundleName;

            gkg::Writer::getInstance().write( fileNameIndividualBundle,
                                              individualBundle,
                                              ascii,
                                              outputBundleFormat );

            if ( computeDensityMasks )
            {

              gkg::Volume< float > individualDensityMask;
              individualBundle.getDensityMask( individualDensityMask,
                                               densityMaskFiberResamplingStep,
                                               densityMaskResolution,
                                               false );
              std::string fileNameIndividualDensityMask =
                                                 outputDirectory.getPath() +
                                                 gkg::getDirectorySeparator() +
                                                 bundleName;
              gkg::Writer::getInstance().write( fileNameIndividualDensityMask,
                                                individualDensityMask,
                                                ascii,
                                                outputVolumeFormat );

            }

          }
          else
          {

            singleFileBundleMap.addCurve3ds( bundleName, fibers );

            if ( computeDensityMasks )
            {

              gkg::Directory outputDirectory( outputDirectoryName );
              if ( !outputDirectory.isValid() )
              {

                outputDirectory.mkdir();

              }

              gkg::BundleMap< std::string > individualBundle;
              individualBundle.addCurve3ds( bundleName, fibers );

              gkg::Volume< float > individualDensityMask;
              individualBundle.getDensityMask( individualDensityMask,
                                               densityMaskFiberResamplingStep,
                                               densityMaskResolution,
                                               false );
              std::string fileNameIndividualDensityMask =
                                                 outputDirectory.getPath() +
                                                 gkg::getDirectorySeparator() +
                                                 bundleName;
              gkg::Writer::getInstance().write( fileNameIndividualDensityMask,
                                                individualDensityMask,
                                                ascii,
                                                outputVolumeFormat );

            }

          }

          if ( verbose )
          {

            std::cout << bundleName << " " << std::flush;

          }

        }

      }

      ++ ob;

    }
    if ( singleFile )
    {

      gkg::Directory outputDirectory( outputDirectoryName );
      if ( !outputDirectory.isValid() )
      {

        outputDirectory.mkdir();

      }

      std::string fileNameSingleFileBundleMap = outputDirectory.getPath() +
                                                gkg::getDirectorySeparator() +
                                                "white_matter_bundles";

      gkg::Writer::getInstance().write( fileNameSingleFileBundleMap,
                                        singleFileBundleMap,
                                        ascii,
                                        outputBundleFormat );

    }

    if ( verbose )
    {

      std::cout << std::endl;

    }

  }
  GKG_CATCH( "template < class L > "
             "void FiberLabellingProcess::operate( "
             "gkg::Process& process, "
             "const std::string& fileNameIn, "
             "const gkg::AnalyzedObject&, "
             "const std::string& )" );

}


std::string FiberLabellingProcess::getLabel(
                  const gkg::LightCurve3d< float >& fiber,
                  const gkg::BundleMap< std::string >& bundleMapAtlas,
                  const std::map< std::string, float >& maximumSquareDistances )
{

  try
  {

    std::string label = "discarded";

    ////////////////////////////////////////////////////////////////////////////
    // keeping centroids from the atlas for which the distance between the 
    // center of the fiber and the center of the centroid remains below a given
    // threshold
    ////////////////////////////////////////////////////////////////////////////

    std::list< const gkg::LightCurve3d< float >* > firstKeptCentroids;
    std::list< std::string > firstKeptLabels;
    std::list< float > firstKeptMaximumSquareDistances;

    int32_t fiberResamplingPointCount = fiber.getPointCount();

    int32_t middlePointIndex = fiberResamplingPointCount / 2;
    const gkg::Vector3d< float >&
      fiberMiddlePoint = fiber.getPoint( middlePointIndex );
    float currentSquareDistance = 0.0f;
    int32_t centroidIndex = 0;
    gkg::BundleMap< std::string >::const_iterator
      b = bundleMapAtlas.begin(),
      be = bundleMapAtlas.end();
    while ( b != be )
    {

      const std::string& bundleName = b->first;
      std::map< std::string, float >::const_iterator
        d2 = maximumSquareDistances.find( bundleName );
      if ( d2 == maximumSquareDistances.end() )
      {

        throw std::runtime_error( 
          std::string( "bundle '" ) + bundleName + "' does not have an entry "+
          "in the distance map" );

      }
      const float& maximumSquareDistance = d2->second;

      centroidIndex = 0;
      std::vector< gkg::LightCurve3d< float > >::const_iterator
        c = b->second.begin(),
        ce = b->second.end();
      while ( c != ce )
      {

        const gkg::Vector3d< float >&
          atlasMiddlePoint = c->getPoint( middlePointIndex );

        currentSquareDistance = ( atlasMiddlePoint -
                                  fiberMiddlePoint ).getNorm2();
        if ( currentSquareDistance < maximumSquareDistance )
        {

          firstKeptCentroids.push_back( &( *c ) );
          firstKeptLabels.push_back( bundleName );
          firstKeptMaximumSquareDistances.push_back( maximumSquareDistance );
        
        }

        ++ centroidIndex;
        ++ c;

      }
      ++ b;

    }


    ////////////////////////////////////////////////////////////////////////////
    // keeping centroids from the last centroid selection for which the distance
    // between the extremities of the fiber and the extremities of the centroid
    // remain below a given threshold
    ////////////////////////////////////////////////////////////////////////////

    std::list< const gkg::LightCurve3d< float >* > secondKeptCentroids;
    std::list< std::string > secondKeptLabels;
    std::list< float > secondKeptMaximumSquareDistances;

    std::list< const gkg::LightCurve3d< float >* >::const_iterator
      c = firstKeptCentroids.begin(),
      ce = firstKeptCentroids.end();
    std::list< std::string >::const_iterator
      l = firstKeptLabels.begin();
    std::list< float >::const_iterator
      d2 = firstKeptMaximumSquareDistances.begin();
    while ( c != ce )
    {

      if ( std::min(
             std::max(
               ( fiber.getPoint( 0 ) -
                 ( *c )->getPoint( 0 ) ).getNorm2(),
               ( fiber.getPoint( fiberResamplingPointCount - 1 ) -
                 ( *c )->getPoint( fiberResamplingPointCount - 1 ) ).getNorm2()
                     ),
             std::max(
               ( fiber.getPoint( 0 ) -
                 ( *c )->getPoint( fiberResamplingPointCount - 1 ) ).getNorm2(),
               ( fiber.getPoint( fiberResamplingPointCount - 1 ) -
                 ( *c )->getPoint( 0 ) ).getNorm2()
                     )
                   ) < *d2 )
      {

        secondKeptCentroids.push_back( *c );
        secondKeptLabels.push_back( *l );
        secondKeptMaximumSquareDistances.push_back( *d2 );

      }
      ++ c;
      ++ l;
      ++ d2;

    }


    ////////////////////////////////////////////////////////////////////////////
    // keeping centroids from the last selection for which the distance between 
    // all he other points of the fiber and all the other point of the centroid
    // remain below a given threshold
    ////////////////////////////////////////////////////////////////////////////

    std::list< const gkg::LightCurve3d< float >* > thirdKeptCentroids;
    std::list< std::string > thirdKeptLabels;
    std::list< float > thirdKeptMaximumSquareDistances;

    c = secondKeptCentroids.begin(),
    ce = secondKeptCentroids.end();
    l = secondKeptLabels.begin();
    d2 = secondKeptMaximumSquareDistances.begin();
    int32_t p = 0;
    while ( c != ce )
    {

      float maximumSquareDistanceDirect = 0;
      float maximumSquareDistanceInverse = 0;
      for ( p = 0; p < fiberResamplingPointCount; p++ )
      {

        if ( ( p != 0 ) &&
             ( p != fiberResamplingPointCount - 1 ) &&
             ( p != middlePointIndex ) )
        {

          float squareDistanceDirect = 
              ( fiber.getPoint( p ) -
                ( *c )->getPoint( p ) ).getNorm2();
          if ( squareDistanceDirect > maximumSquareDistanceDirect )
          {

            maximumSquareDistanceDirect = squareDistanceDirect;

          }

          float squareDistanceInverse = 
              ( fiber.getPoint( p ) -
                ( *c )->getPoint(
                              fiberResamplingPointCount - 1 - p ) ).getNorm2();
          if ( squareDistanceInverse > maximumSquareDistanceInverse )
          {

            maximumSquareDistanceInverse = squareDistanceInverse;

          }

        }

      }
      if ( std::min( maximumSquareDistanceDirect,
                     maximumSquareDistanceInverse ) < *d2 )
      {

        thirdKeptCentroids.push_back( *c );
        thirdKeptLabels.push_back( *l );
        thirdKeptMaximumSquareDistances.push_back( *d2 );

      }
      ++ c;
      ++ l;
      ++ d2;

    }


    ////////////////////////////////////////////////////////////////////////////
    // keeping the centroid with minimum distance from the last centroid
    // selection, with an added penalty to prevent the selection of a centroid
    // of too different length with respect to the fiber
    ////////////////////////////////////////////////////////////////////////////

    // computing the current fiber resolution
    float fiberResolution = ( fiber.getPoint( 1 ) -
                              fiber.getPoint( 0 ) ).getNorm();
    float centroidResolution = 0.0f;
    float lengthFactor = 0.0f;
    float lengthDifferencePenaltyFactor = 0.0f;
    c = thirdKeptCentroids.begin(),
    ce = thirdKeptCentroids.end();
    l = thirdKeptLabels.begin();
    d2 = thirdKeptMaximumSquareDistances.begin();
    while ( c != ce )
    {

      // computing the current centroid resolution
      centroidResolution = ( ( *c )->getPoint( 1 ) -
                             ( *c )->getPoint( 0 ) ).getNorm();

      // computing the penalizing factor
      if ( centroidResolution < fiberResolution )
      {

        lengthFactor = ( fiberResolution - centroidResolution ) / 
                       fiberResolution;

      }
      else
      {

        lengthFactor = ( centroidResolution - fiberResolution ) / 
                       centroidResolution;

      }
      lengthDifferencePenaltyFactor = ( 1.0 + lengthFactor );;
      lengthDifferencePenaltyFactor *= lengthDifferencePenaltyFactor - 1.0f;


      float maximumSquareDistanceDirect = 0;
      float maximumSquareDistanceInverse = 0;
      for ( p = 0; p < fiberResamplingPointCount; p++ )
      {

         float squareDistanceDirect = 
             ( fiber.getPoint( p ) -
               ( *c )->getPoint( p ) ).getNorm2();
         if ( squareDistanceDirect > maximumSquareDistanceDirect )
         {

           maximumSquareDistanceDirect = squareDistanceDirect;

         }

         float squareDistanceInverse = 
             ( fiber.getPoint( p ) -
               ( *c )->getPoint(
                             fiberResamplingPointCount - 1 - p ) ).getNorm2();
         if ( squareDistanceInverse > maximumSquareDistanceInverse )
         {

           maximumSquareDistanceInverse = squareDistanceInverse;

         }

      }
      float minimumSquareDistance = std::min( maximumSquareDistanceDirect,
                                              maximumSquareDistanceInverse );
      if ( ( std::sqrt( minimumSquareDistance ) + 
             lengthDifferencePenaltyFactor ) < std::sqrt( *d2 ) )
      {

        label = *l;

      }

      ++ c;
      ++ l;
      ++ d2;

    }

    return label;

  }
  GKG_CATCH( "std::string FiberLabellingProcess::getLabel( "
             "const gkg::LightCurve3d< float >& fiber, "
             "const gkg::BundleMap< std::string >& bundleMapAtlas, "
             "const std::map< std::string, float >& maximumSquareDistances )" );

}


//
// class DwiFiberLabellingCommand
//

gkg::DwiFiberLabellingCommand::DwiFiberLabellingCommand( int32_t argc,
                                                         char* argv[],
                                                         bool loadPlugin,
                                                         bool removeFirst )
                              : gkg::Command( argc, argv,
                                              loadPlugin,
                                              removeFirst )
{

  try
  {

    parse();

  }
  GKG_CATCH( "gkg::DwiFiberLabellingCommand::"
             "DwiFiberLabellingCommand( "
             "int32_t argc, char* argv[], bool loadPlugin, "
             "bool removeFirst )" );

}


gkg::DwiFiberLabellingCommand::DwiFiberLabellingCommand(
          const std::vector< std::string >& fileNameInputBundleMaps,
          const std::string& fileNameAtlasBundleMap,
          const std::vector< std::string >& fileNameTransform3dBundleMapToAtlas,
          const std::string& fileNameMaximumDistanceToBundleMap,
          int32_t fiberResamplingPointCount,
          float densityMaskFiberResamplingStep, 
          const gkg::Vector3d< double >& densityMaskResolution, 
          const std::string& outputDirectoryName,
          bool intermediate,
          bool singleFile,
          bool computeDensityMasks,
          bool saveDiscarded,
          const std::string& outputVolumeFormat,
          const std::string& outputBundleFormat,
          bool ascii,
          bool verbose )
                              : gkg::Command()
{

  try
  {

    execute( fileNameInputBundleMaps,
             fileNameAtlasBundleMap,
             fileNameTransform3dBundleMapToAtlas,  
             fileNameMaximumDistanceToBundleMap,
             fiberResamplingPointCount,
             densityMaskFiberResamplingStep,
             densityMaskResolution,
             outputDirectoryName,
             intermediate,
             singleFile,
             computeDensityMasks,
             saveDiscarded,
             outputVolumeFormat,
             outputBundleFormat,
             ascii,
             verbose );

  }
  GKG_CATCH( "gkg::DwiFiberLabellingCommand::DwiFiberLabellingCommand( "
             "const std::vector< std::string >& fileNameInputBundleMaps, "
             "const std::string& fileNameAtlasBundleMap, "
             "const std::vector< std::string >& "
             "fileNameTransform3dBundleMapToAtlas, "
             "const std::string& fileNameMaximumDistanceToBundleMap, "
             "int32_t fiberResamplingPointCount, "
             "float densityMaskFiberResamplingStep, "
             "const gkg::Vector3d< double >& densityMaskResolution, "
             "const std::string& outputDirectoryName, "
             "bool intermediate, "
             "bool singleFile, "
             "bool computeDensityMasks, "
             "bool saveDiscarded, "
             "const std::string& outputVolumeFormat, "
             "const std::string& outputBundleFormat, "
             "bool ascii, "
             "bool verbose )" );

}


gkg::DwiFiberLabellingCommand::DwiFiberLabellingCommand(
                                             const gkg::Dictionary& parameters )
                              : gkg::Command( parameters )
{

  try
  {

    DECLARE_VECTOR_OF_STRINGS_PARAMETER( parameters, std::vector< std::string >,
                                         fileNameInputBundleMaps );
    DECLARE_STRING_PARAMETER( parameters, std::string,
                              fileNameAtlasBundleMap );
    DECLARE_VECTOR_OF_STRINGS_PARAMETER( parameters, std::vector< std::string >,
                                         fileNameTransform3dBundleMapToAtlas );
    DECLARE_STRING_PARAMETER( parameters, std::string,
                              fileNameMaximumDistanceToBundleMap );
    DECLARE_INTEGER_PARAMETER( parameters, int32_t, fiberResamplingPointCount );
    DECLARE_FLOATING_PARAMETER( parameters, float, 
                                densityMaskFiberResamplingStep );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER( parameters, std::vector< double >,
                                           densityMaskResolution );
    DECLARE_STRING_PARAMETER( parameters, std::string,
                              outputDirectoryName );
    DECLARE_BOOLEAN_PARAMETER( parameters, bool, intermediate );
    DECLARE_BOOLEAN_PARAMETER( parameters, bool, singleFile );
    DECLARE_BOOLEAN_PARAMETER( parameters, bool, computeDensityMasks );
    DECLARE_BOOLEAN_PARAMETER( parameters, bool, saveDiscarded );
    DECLARE_STRING_PARAMETER( parameters, std::string, outputVolumeFormat );
    DECLARE_STRING_PARAMETER( parameters, std::string, outputBundleFormat );
    DECLARE_BOOLEAN_PARAMETER( parameters, bool, ascii );
    DECLARE_BOOLEAN_PARAMETER( parameters, bool, verbose );

    gkg::Vector3d< double > densityMaskResolutionVector;
    if ( densityMaskResolution.empty() )
    {

      densityMaskResolutionVector.x = 1.0;
      densityMaskResolutionVector.y = 1.0;
      densityMaskResolutionVector.z = 1.0;

    }
    else if ( densityMaskResolution.size() != 3U )
    {

      throw std::runtime_error( "densityMaskResolution must be a vector "
                                "containing 3 floating parameters" );

    }
    else
    {

      densityMaskResolutionVector.x = densityMaskResolution[ 0 ];
      densityMaskResolutionVector.y = densityMaskResolution[ 1 ];
      densityMaskResolutionVector.z = densityMaskResolution[ 2 ];

    }

    execute( fileNameInputBundleMaps,
             fileNameAtlasBundleMap,
             fileNameTransform3dBundleMapToAtlas,  
             fileNameMaximumDistanceToBundleMap,
             fiberResamplingPointCount,
             densityMaskFiberResamplingStep,
             densityMaskResolutionVector,
             outputDirectoryName,
             intermediate,
             singleFile,
             computeDensityMasks,
             saveDiscarded,
             outputVolumeFormat,
             outputBundleFormat,
             ascii,
             verbose );

  }
  GKG_CATCH( "gkg::DwiFiberLabellingCommand::"
             "DwiFiberLabellingCommand( "
             "const gkg::Dictionary& parameters )" );

}


gkg::DwiFiberLabellingCommand::~DwiFiberLabellingCommand()
{
}


std::string gkg::DwiFiberLabellingCommand::getStaticName()
{

  try
  {

    return "DwiFiberLabelling";

  }
  GKG_CATCH( "std::string gkg::DwiFiberLabellingCommand::"
             "getStaticName()" );

}


void gkg::DwiFiberLabellingCommand::parse()
{

  try
  {

    std::vector< std::string > fileNameInputBundleMaps;
    std::string fileNameAtlasBundleMap;
    std::vector< std::string > fileNameTransform3dBundleMapToAtlas;
    std::string fileNameMaximumDistanceToBundleMap;
    int32_t fiberResamplingPointCount = 21;
    float densityMaskFiberResamplingStep = 0.1; 
    std::vector< double > densityMaskResolution;
    std::string outputDirectoryName;
    bool intermediate = false;
    bool singleFile = true;
    bool computeDensityMasks = false;
    bool saveDiscarded = false;
    std::string outputVolumeFormat = "aimsbundlemap";
    std::string outputBundleFormat = "gis";
    bool ascii = false;
    bool verbose = false;


    ////////////////////////////////////////////////////////////////////////////
    // managing command lines argument(s)
    ////////////////////////////////////////////////////////////////////////////

    gkg::Application application( _argc, _argv,
                                  "DWI fiber labelling tool",
                                  _loadPlugin );
    application.addSeriesOption( "-i",
                                 "Input bundle map file names",
                                 fileNameInputBundleMaps,
                                 1 );
    application.addSingleOption( "-a",
                                 "Bundle map atlas",
                                 fileNameAtlasBundleMap );
    application.addSeriesOption( "-t",
                                 "Bundle map to atlas 3D transformation "
                                 "(default=id)",
                                 fileNameTransform3dBundleMapToAtlas,
                                 0, 3 );
    application.addSingleOption( "-d",
                                 "Maximum distance to bundle map configuration "
                                 "file",
                                 fileNameMaximumDistanceToBundleMap );
    application.addSingleOption( "-fiberResamplingPointCount",
                                 "Fiber resampling point count (default=21)",
                                 fiberResamplingPointCount,
                                 true );
    application.addSingleOption( "-densityMaskFiberResamplingStep",
                                 "Density mask fiber resampling step "
                                 "(default=0.1mm)",
                                 densityMaskFiberResamplingStep,
                                 true );
    application.addSeriesOption( "-densityMaskResolution",
                                 "Density mask resolution "
                                 "(default=(1.0mm,1.0mm,1.0mm))",
                                 densityMaskResolution,
                                 0, 3 );
    application.addSingleOption( "-o",
                                 "Output directory",
                                 outputDirectoryName );
    application.addSingleOption( "-intermediate",
                                 "Save intermediate results (default=false)",
                                 intermediate,
                                 true );
    application.addSingleOption( "-single",
                                 "Save results in a single file (default=true)",
                                 singleFile,
                                 true );
    application.addSingleOption( "-computeDensityMasks",
                                 "Compute density mask (default=false)",
                                 computeDensityMasks,
                                 true );
    application.addSingleOption( "-saveDiscarded",
                                 "Save discarded fibers in a bundle labelled "
                                 "discarded (default=false)",
                                 saveDiscarded,
                                 true );
    application.addSingleOption( "-ovf",
                                 "Output volume format name "
                                 "(default=gis)",
                                 outputVolumeFormat,
                                 true );
    application.addSingleOption( "-obf",
                                 "Output bundle format name "
                                 "(default=aimsbundlemap)",
                                 outputBundleFormat,
                                 true );
    application.addSingleOption( "-ascii",
                                 "Save ouput in ASCII mode",
                                 ascii,
                                 true );
    application.addSingleOption( "-verbose",
                                 "Show as much information as possible",
                                 verbose, 
                                 true );

    application.initialize();

    gkg::Vector3d< double > densityMaskResolutionVector;
    if ( densityMaskResolution.empty() )
    {

      densityMaskResolutionVector.x = 1.0;
      densityMaskResolutionVector.y = 1.0;
      densityMaskResolutionVector.z = 1.0;

    }
    else if ( densityMaskResolution.size() != 3U )
    {

      throw std::runtime_error( "densityMaskResolution must be a vector "
                                "containing 3 floating parameters" );

    }
    else
    {

      densityMaskResolutionVector.x = densityMaskResolution[ 0 ];
      densityMaskResolutionVector.y = densityMaskResolution[ 1 ];
      densityMaskResolutionVector.z = densityMaskResolution[ 2 ];

    }

    execute( fileNameInputBundleMaps,
             fileNameAtlasBundleMap,
             fileNameTransform3dBundleMapToAtlas,  
             fileNameMaximumDistanceToBundleMap,
             fiberResamplingPointCount,
             densityMaskFiberResamplingStep,
             densityMaskResolutionVector,
             outputDirectoryName,
             intermediate,
             singleFile,
             computeDensityMasks,
             saveDiscarded,
             outputVolumeFormat,
             outputBundleFormat,
             ascii,
             verbose );

  }
  GKG_CATCH_FUNCTOR( "void "
                     "gkg::DwiFiberLabellingCommand::parse()" );

}


void gkg::DwiFiberLabellingCommand::execute(
          const std::vector< std::string >& fileNameInputBundleMaps,
          const std::string& fileNameAtlasBundleMap,
          const std::vector< std::string >& fileNameTransform3dBundleMapToAtlas,
          const std::string& fileNameMaximumDistanceToBundleMap,
          int32_t fiberResamplingPointCount,
          float densityMaskFiberResamplingStep, 
          const gkg::Vector3d< double >& densityMaskResolution, 
          const std::string& outputDirectoryName,
          bool intermediate,
          bool singleFile,
          bool computeDensityMasks,
          bool saveDiscarded,
          const std::string& outputVolumeFormat,
          const std::string& outputBundleFormat,
          bool ascii,
          bool verbose )
{

  try
  {

    //
    // sanity check
    //

    if ( !fileNameTransform3dBundleMapToAtlas.empty() )
    {

      if ( ( fileNameTransform3dBundleMapToAtlas.size() != 1U ) &&
           ( fileNameTransform3dBundleMapToAtlas.size() != 3U ) )
      {

        throw std::runtime_error(
                                 "number of 3D transformation must be 1 or 3" );

      }

    }

    FiberLabellingProcess 
      fiberLabellingProcess( fileNameInputBundleMaps,
                             fileNameAtlasBundleMap,
                             fileNameTransform3dBundleMapToAtlas,
                             fileNameMaximumDistanceToBundleMap,
                             fiberResamplingPointCount,
                             densityMaskFiberResamplingStep, 
                             densityMaskResolution, 
                             outputDirectoryName,
                             intermediate,
                             singleFile,
                             computeDensityMasks,
                             saveDiscarded,
                             outputVolumeFormat,
                             outputBundleFormat,
                             ascii,
                             verbose );
                                          
    fiberLabellingProcess.execute( fileNameInputBundleMaps[ 0 ] );

  }
  GKG_CATCH( "void gkg::DwiFiberLabellingCommand::execute( "
             "const std::vector< std::string >& fileNameInputBundleMaps, "
             "const std::string& fileNameAtlasBundleMap, "
             "const std::vector< std::string >& "
             "fileNameTransform3dBundleMapToAtlas, "
             "const std::string& fileNameMaximumDistanceToBundleMap, "
             "int32_t fiberResamplingPointCount, "
             "float densityMaskFiberResamplingStep, "
             "const gkg::Vector3d< double >& densityMaskResolution, "
             "const std::string& outputDirectoryName, "
             "bool intermediate, "
             "bool singleFile, "
             "bool computeDensityMasks, "
             "bool saveDiscarded, "
             "const std::string& outputVolumeFormat, "
             "const std::string& outputBundleFormat, "
             "bool ascii, "
             "bool verbose )" );

}


RegisterCommandCreator(
    DwiFiberLabellingCommand,
    DECLARE_VECTOR_OF_STRINGS_PARAMETER_HELP( fileNameInputBundleMaps ) +
    DECLARE_STRING_PARAMETER_HELP( fileNameAtlasBundleMap ) +
    DECLARE_VECTOR_OF_STRINGS_PARAMETER_HELP( 
                                         fileNameTransform3dBundleMapToAtlas ) +
    DECLARE_STRING_PARAMETER_HELP( fileNameMaximumDistanceToBundleMap ) +
    DECLARE_INTEGER_PARAMETER_HELP( fiberResamplingPointCount ) +
    DECLARE_FLOATING_PARAMETER_HELP( densityMaskFiberResamplingStep ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( densityMaskResolution ) +
    DECLARE_STRING_PARAMETER_HELP( outputDirectoryName ) +
    DECLARE_BOOLEAN_PARAMETER_HELP( intermediate ) +
    DECLARE_BOOLEAN_PARAMETER_HELP( singleFile ) +
    DECLARE_BOOLEAN_PARAMETER_HELP( computeDensityMasks ) +
    DECLARE_BOOLEAN_PARAMETER_HELP( saveDiscarded ) +
    DECLARE_STRING_PARAMETER_HELP( outputVolumeFormat ) +
    DECLARE_STRING_PARAMETER_HELP( outputBundleFormat ) +
    DECLARE_BOOLEAN_PARAMETER_HELP( ascii ) +
    DECLARE_BOOLEAN_PARAMETER_HELP( verbose ) );
