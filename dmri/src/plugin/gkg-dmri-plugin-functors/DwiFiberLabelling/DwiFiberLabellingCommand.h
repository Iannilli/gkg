#ifndef _gkg_dmri_plugin_functors_DwiFiberLabelling_DwiFiberLabellingCommand_h_
#define _gkg_dmri_plugin_functors_DwiFiberLabelling_DwiFiberLabellingCommand_h_


#include <gkg-communication-command/Command.h>
#include <gkg-processing-coordinates/Vector3d.h>
#include <gkg-core-pattern/Creator.h>

#include <string>
#include <vector>


namespace gkg
{


class DwiFiberLabellingCommand :
                      public Command,
                      public Creator2Arg< DwiFiberLabellingCommand,
                                          Command,
                                          int32_t,
                                          char** >,
                      public Creator1Arg< DwiFiberLabellingCommand,
                                          Command,
                                          const Dictionary& >
{

  public:

    DwiFiberLabellingCommand( int32_t argc,
                                           char* argv[],
                                           bool loadPlugin = false,
                                           bool removeFirst = true );
    DwiFiberLabellingCommand(
          const std::vector< std::string >& fileNameInputBundleMaps,
          const std::string& fileNameAtlasBundleMap,
          const std::vector< std::string >& fileNameTransform3dBundleMapToAtlas,  
          const std::string& fileNameMaximumDistanceToBundleMap,
          int32_t fiberResamplingPointCount,
          float densityMaskFiberResamplingStep, 
          const Vector3d< double >& densityMaskResolution, 
          const std::string& outputDirectoryName,
          bool intermediate,
          bool singleFile,
          bool computeDensityMasks,
          bool saveDiscarded,
          const std::string& outputVolumeFormat,
          const std::string& outputBundleFormat,
          bool ascii,
          bool verbose );
    DwiFiberLabellingCommand( const Dictionary& parameters );
    virtual ~DwiFiberLabellingCommand();

    static std::string getStaticName();

  protected:

    friend class Creator2Arg< DwiFiberLabellingCommand, Command, 
                              int32_t, char** >;
    friend class Creator1Arg< DwiFiberLabellingCommand, Command,
                              const Dictionary& >;

    void parse();
    void execute(
          const std::vector< std::string >& fileNameInputBundleMaps,
          const std::string& fileNameAtlasBundleMap,
          const std::vector< std::string >& fileNameTransform3dBundleMapToAtlas,  
          const std::string& fileNameMaximumDistanceToBundleMap,
          int32_t fiberResamplingPointCount,
          float densityMaskFiberResamplingStep, 
          const Vector3d< double >& densityMaskResolution, 
          const std::string& outputDirectoryName,
          bool intermediate,
          bool singleFile,
          bool computeDensityMasks,
          bool saveDiscarded,
          const std::string& outputVolumeFormat,
          const std::string& outputBundleFormat,
          bool ascii,
          bool verbose );

};


}


#endif
