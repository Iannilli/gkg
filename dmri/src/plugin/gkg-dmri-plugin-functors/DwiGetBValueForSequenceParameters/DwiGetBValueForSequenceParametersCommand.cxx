#include <gkg-dmri-plugin-functors/DwiGetBValueForSequenceParameters/DwiGetBValueForSequenceParametersCommand.h>
#include <gkg-communication-command/CommandFactory.h>
#include <gkg-communication-getopt/Application.h>
#include <gkg-core-pattern/RCPointer.h>
#include <gkg-processing-coordinates/ElectrostaticOrientationSet.h>
#include <gkg-dmri-simulator-pulse-sequence/NMRSequenceFactory.h>
#include <gkg-core-exception/Exception.h>

#include <iostream>
#include <list>


gkg::DwiGetBValueForSequenceParametersCommand::
                                       DwiGetBValueForSequenceParametersCommand(
                                                              int32_t argc,
                                                              char* argv[],
                                                              bool loadPlugin,
                                                              bool removeFirst )
                                            : gkg::Command( argc, argv,
                                                            loadPlugin,
                                                            removeFirst )
{

  try
  {

    parse();

  }
  GKG_CATCH( "gkg::DwiGetBValueForSequenceParametersCommand::"
             "DwiGetBValueForSequenceParametersCommand( "
             "int32_t argc, char* argv[], bool loadPlugin, "
             "bool removeFirst )" );

}


gkg::DwiGetBValueForSequenceParametersCommand::
                                       DwiGetBValueForSequenceParametersCommand(
                             const std::string& sequence,
                             const std::vector< float >& gradientAmplitudes,
                             const std::vector< float >& scalarParameters,
                             const std::vector< std::string >& stringParameters,
                             const std::string& dwOrientationSetFileName,
                             int32_t dwOrientationCount,
                             float timeStep,
                             bool verbose )
                                            : gkg::Command()
{

  try
  {

    execute( sequence,
             gradientAmplitudes,
             scalarParameters,
             stringParameters,
             dwOrientationSetFileName,
             dwOrientationCount,
             timeStep,
             verbose );

  }
  GKG_CATCH( "gkg::DwiGetBValueForSequenceParametersCommand::"
             "DwiGetBValueForSequenceParametersCommand( "
             "const std::string& sequence, "
             "const std::vector< float >& gradientAmplitudes, "
             "const std::vector< float >& scalarParameters, "
             "const std::vector< std::string >& stringParameters, "
             "const std::string& dwOrientationSetFileName, "
             "int32_t dwOrientationCount, "
             "float timeStep, "
             "bool verbose )" );

}


gkg::DwiGetBValueForSequenceParametersCommand::
                                       DwiGetBValueForSequenceParametersCommand(
                                             const gkg::Dictionary& parameters )
                                              : gkg::Command( parameters )
{

  try
  {

    DECLARE_STRING_PARAMETER( parameters, std::string, sequence );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER( parameters, std::vector< float >,
                                           gradientAmplitudes );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER( parameters, std::vector< float >,
                                           scalarParameters );
    DECLARE_VECTOR_OF_STRINGS_PARAMETER( parameters, std::vector< std::string >,
                                         stringParameters );
    DECLARE_STRING_PARAMETER( parameters, std::string,
                              dwOrientationSetFileName );
    DECLARE_INTEGER_PARAMETER( parameters, int32_t, dwOrientationCount );
    DECLARE_FLOATING_PARAMETER( parameters, float, timeStep );
    DECLARE_BOOLEAN_PARAMETER( parameters, bool, verbose );
    
    execute( sequence,
             gradientAmplitudes,
             scalarParameters,
             stringParameters,
             dwOrientationSetFileName,
             dwOrientationCount,
             timeStep,
             verbose );

  }
  GKG_CATCH( "gkg::DwiGetBValueForSequenceParametersCommand::"
             "DwiGetBValueForSequenceParametersCommand( "
             "const gkg::Dictionary& parameters )" );

}


gkg::DwiGetBValueForSequenceParametersCommand::
                                     ~DwiGetBValueForSequenceParametersCommand()
{
}


std::string gkg::DwiGetBValueForSequenceParametersCommand::getStaticName()
{

  try
  {

    return "DwiGetBValueForSequenceParameter";

  }
  GKG_CATCH( "std::string "
             "gkg::DwiGetBValueForSequenceParametersCommand::getStaticName()" );

}


void gkg::DwiGetBValueForSequenceParametersCommand::parse()
{

  try
  {

    std::string sequence;
    std::vector< float > gradientAmplitudes;
    std::vector< float > scalarParameters;
    std::vector< std::string > stringParameters;
    std::string dwOrientationSetFileName;
    int32_t dwOrientationCount = 6;
    float timeStep = 1.0;
    bool verbose = false;

    ////////////////////////////////////////////////////////////////////////////
    // building NMR sequence list
    ////////////////////////////////////////////////////////////////////////////

    std::string sequenceText = "NMR sequences : \n";
    std::list< std::string >
      sequenceNameList = gkg::NMRSequenceFactory::getInstance().getNameList();
    std::list< std::string >::const_iterator
      s = sequenceNameList.begin(),
      se = sequenceNameList.end();
    while ( s != se )
    {

      sequenceText += "- " + *s + "\n";
      ++ s;

    }

    ////////////////////////////////////////////////////////////////////////////
    // managing command lines argument(s)
    ////////////////////////////////////////////////////////////////////////////

    gkg::Application application( _argc, _argv,
                                  "b-value estimation for specific sequence "
                                  "parameters",
                                  _loadPlugin );

    application.addSingleOption( "-sequence",
                                 sequenceText,
                                 sequence );

    application.addSeriesOption( "-gradientAmplitudes",
                                 "Set of gradient amplitudes to simulate",
                                 gradientAmplitudes,
                                 1 );

    application.addSeriesOption(
                  "-scalarParameters",
                  "NMR sequence parameters as a vector of float "
                  "<P1> <P2> ... <Pn>:"
                  "\n\n"
                  "- in case of single_pgse_nmr_sequence, \n"
                  ".  <P1>: maximum gradient slew rate in T/m/s \n"
                  ".  <P2>: gradient time resolution in us \n"
                  ".  <P3>: little delta in ms \n"
                  ".  <P4>: big delta in ms"
                  "\n\n"
                  "- in case of multiple_pgse_nmr_sequence, \n"
                  ".  <P1>: maximum gradient slew rate in T/m/s \n"
                  ".  <P2>: gradient time resolution in us \n"
                  ".  <P3>: little delta in ms \n"
                  ".  <P4>: big delta in ms \n"
                  ".  <P5>: mixing time in ms \n"
                  ".  <P6>: gradient pair count"
                  "\n\n"
                  "- in case of stimulated_echo_nmr_sequence, \n"
                  ".  <P1>: maximum gradient slew rate in T/m/s \n"
                  ".  <P2>: gradient time resolution in us \n"
                  ".  <P3>: little delta in ms \n"
                  ".  <P4>: big delta in ms"
                  "\n\n"
                  "- in case of twice_refocused_se_nmr_sequence \n"
                  ".  <P1>: maximum gradient slew rate in T/m/s \n"
                  ".  <P2>: gradient time resolution in us \n"
                  ".  <P3>: little delta in ms \n"
                  ".  <P4>: big delta in ms"
                  "\n\n"
                  "- in case of ogse_nmr_sequence, \n"
                  ".  <P1>: period of oscillating gradient in ms \n"
                  ".  <P2>: lobe count of oscillating gradient"
                  "\n",
                  scalarParameters );

    application.addSeriesOption(
                  "-stringParameters",
                  "NMR sequence parameters as a vector of string "
                  "<P1> <P2> ... <Pn>:"
                  "\n\n"
                  "- in case of single_pgse_nmr_sequence, \n"
                  ".  N/A"
                  "\n\n"
                  "- in case of multiple_pgse_nmr_sequence, \n"
                  ".  <P1>: wavevectors file name"
                  "\n\n"
                  "- in case of stimulated_echo_nmr_sequence, \n"
                  ".  N/A"
                  "\n\n"
                  "- in case of twice_refocused_se_nmr_sequence \n"
                  ".  N/A"
                  "\n\n"
                  "- in case of ogse_nmr_sequence, \n"
                  ".  <P1>: oscillating gradient waveform \n"
                  ".        - sine \n"
                  ".        - double-sine \n"
                  ".        - cosine"
                  "\n",
                  stringParameters );

    application.addSingleOption( "-dwOrientationSetFileName",
                                 "Discrete orientation file of output DW image",
                                 dwOrientationSetFileName,
                                 true );

    application.addSingleOption( "-dwOrientationCount",
                                 "Discrete orientation count of output DW "
                                 "image (default=6)",
                                 dwOrientationCount,
                                 true );

    application.addSingleOption( "-timeStep",
                                 "Simulation time step (default=1us)",
                                 timeStep,
                                 true );

    application.addSingleOption( "-verbose",
                                 "Show as much information as possible",
                                 verbose,
                                 true );

    application.initialize();

    execute( sequence,
             gradientAmplitudes,
             scalarParameters,
             stringParameters,
             dwOrientationSetFileName,
             dwOrientationCount,
             timeStep,
             verbose );

  }
  GKG_CATCH_FUNCTOR( "void "
                     "gkg::DwiGetBValueForSequenceParametersCommand::parse()" );

}


void gkg::DwiGetBValueForSequenceParametersCommand::execute(
                             const std::string& sequence,
                             const std::vector< float >& gradientAmplitudes,
                             const std::vector< float >& scalarParameters,
                             const std::vector< std::string >& stringParameters,
                             const std::string& dwOrientationSetFileName,
                             int32_t dwOrientationCount,
                             float timeStep,
                             bool verbose )
{

  try
  {

    ////////////////////////////////////////////////////////////////////////////
    // sanity checks
    ////////////////////////////////////////////////////////////////////////////

    std::vector< float > theScalarParameters = scalarParameters;
    std::vector< std::string > theStringParameters = stringParameters;
    gkg::NMRSequenceFactory::getInstance().
      checkOrInitializeDefaultParameters( sequence,
                                          theScalarParameters,
                                          theStringParameters );

    ////////////////////////////////////////////////////////////////////////////
    // building the DW gradient orientation set
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

      std::cout << "building the DW gradient encoding scheme : "
                << std::flush;

    }

    gkg::RCPointer< gkg::OrientationSet > dwOrientationSet;

    if ( !dwOrientationSetFileName.empty() )
    {

      dwOrientationSet = gkg::RCPointer< gkg::OrientationSet >(
                          new gkg::OrientationSet( dwOrientationSetFileName ) );

    }
    else
    {

      dwOrientationSet = gkg::RCPointer< gkg::OrientationSet >(
                   new gkg::ElectrostaticOrientationSet( dwOrientationCount ) );

    }

    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }


    ////////////////////////////////////////////////////////////////////////////
    // initializing NMR pulse sequence
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

      std::cout << "building NMR pulse sequence ("
                << sequence << ") : "
                << std::flush;

    }

    gkg::RCPointer< gkg::NMRSequence >
      nmrSequence = gkg::NMRSequenceFactory::getInstance().create(
                      sequence,
                      std::set< float >( gradientAmplitudes.begin(),
                                         gradientAmplitudes.end() ),
                      theScalarParameters,
                      theStringParameters,
                      verbose );

    nmrSequence->setOrientationSet( dwOrientationSet );

    // getting the b-values
    int32_t bValueCount = gradientAmplitudes.size();
    std::vector< float > bValues( bValueCount );
    int32_t b = 0;
    for ( b = 0; b < bValueCount; b++ )
    {

      bValues[ b ] = nmrSequence->getBValue( timeStep, b, 0 );

    }

    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }

    ////////////////////////////////////////////////////////////////////////////
    // printing the results
    ////////////////////////////////////////////////////////////////////////////

    std::cout << std::endl;

    std::cout << "NMR pulse sequence                            : "
              << sequence
              << std::endl;

    std::cout << "maximum gradient amplitude in mT/m            : ";
    int32_t g = 0;
    for ( g = 0; g < nmrSequence->getGradientAmplitudeCount(); g++ )
    {

      std::cout << nmrSequence->getGradientAmplitude( g ) << " ";

    }
    std::cout << std::endl;

    std::cout << "b-value in s/mm2                              : ";
    for ( b = 0; b < bValueCount; b++ )
    {

      std::cout << bValues[ b ] << " ";

    }
    std::cout << std::endl;

    std::cout << "maximum q in 1/m                              : "
              << nmrSequence->getMaximumQInMeterInverse()
              << std::endl;

    std::cout << "minimum radius in um                          : "
              << nmrSequence->getMinimumRadiusInUm()
              << std::endl;

  }
  GKG_CATCH( "void gkg::DwiGetBValueForSequenceParametersCommand::execute( "
             "const std::string& sequence, "
             "const std::vector< float >& gradientAmplitudes, "
             "const std::vector< float >& scalarParameters, "
             "const std::vector< std::string >& stringParameters, "
             "const std::string& dwOrientationSetFileName, "
             "int32_t dwOrientationCount, "
             "float timeStep, "
             "bool verbose )" );

}


RegisterCommandCreator( 
    DwiGetBValueForSequenceParametersCommand,
    DECLARE_STRING_PARAMETER_HELP( sequence ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( gradientAmplitudes ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( scalarParameters ) +
    DECLARE_VECTOR_OF_STRINGS_PARAMETER_HELP( stringParameters ) +
    DECLARE_STRING_PARAMETER_HELP( dwOrientationSetFileName ) +
    DECLARE_INTEGER_PARAMETER_HELP( dwOrientationCount ) +
    DECLARE_FLOATING_PARAMETER_HELP( timeStep ) +
    DECLARE_BOOLEAN_PARAMETER_HELP( verbose ) );
