#ifndef _gkg_dmri_plugin_functors_DwiGetBValueForSequenceParameters_DwiGetBValueForSequenceParametersCommand_h_
#define _gkg_dmri_plugin_functors_DwiGetBValueForSequenceParameters_DwiGetBValueForSequenceParametersCommand_h_


#include <gkg-communication-command/Command.h>
#include <gkg-core-pattern/Creator.h>

#include <string>
#include <vector>


namespace gkg
{


class DwiGetBValueForSequenceParametersCommand :
                   public Command,
                   public Creator2Arg< DwiGetBValueForSequenceParametersCommand,
                                       Command,
                                       int32_t,
                                       char** >,
                   public Creator1Arg< DwiGetBValueForSequenceParametersCommand,
                                       Command,
                                       const Dictionary& >
{

  public:

    DwiGetBValueForSequenceParametersCommand( int32_t argc,
                                             char* argv[],
                                             bool loadPlugin = false,
                                             bool removeFirst = true );
    DwiGetBValueForSequenceParametersCommand( 
                             const std::string& sequence,
                             const std::vector< float >& gradientAmplitudes,
                             const std::vector< float >& scalarParameters,
                             const std::vector< std::string >& stringParameters,
                             const std::string& dwOrientationSetFileName,
                             int32_t dwOrientationCount,
                             float timeStep,
                             bool verbose );
    DwiGetBValueForSequenceParametersCommand( const Dictionary& parameters );
    virtual ~DwiGetBValueForSequenceParametersCommand();

    static std::string getStaticName();

  protected:

    friend class Creator2Arg< DwiGetBValueForSequenceParametersCommand, Command,
                              int32_t, char** >;
    friend class Creator1Arg< DwiGetBValueForSequenceParametersCommand, Command,
                              const Dictionary& >;

    void parse();
    void execute( const std::string& sequence,
                  const std::vector< float >& gradientAmplitudes,
                  const std::vector< float >& scalarParameters,
                  const std::vector< std::string >& stringParameters,
                  const std::string& dwOrientationSetFileName,
                  int32_t dwOrientationCount,
                  float timeStep,
                  bool verbose );

};


}


#endif
