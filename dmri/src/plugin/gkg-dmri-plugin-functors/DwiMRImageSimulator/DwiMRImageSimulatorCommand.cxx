#include <gkg-dmri-plugin-functors/DwiMRImageSimulator/DwiMRImageSimulatorCommand.h>
#include <gkg-communication-command/CommandFactory.h>
#include <gkg-communication-getopt/Application.h>
#include <gkg-core-pattern/RCPointer.h>
#include <gkg-processing-container/BoundingBox.h>
#include <gkg-processing-coordinates/ElectrostaticOrientationSet.h>
#include <gkg-dmri-simulator-pulse-sequence/NMRSequenceFactory.h>
#include <gkg-dmri-simulator-noise/NoiseModelFactory.h>
#include <gkg-dmri-simulator-mr-imaging/MRImage.h>
#include <gkg-dmri-simulator-particle/ParticleMap.h>
#include <gkg-communication-thread/ThreadedLoop.h>
#include <gkg-dmri-simulator-spin/SpinCreationThreadedLoopContext.h>
#include <gkg-dmri-simulator-spin/SpinCache.h>
#include <gkg-dmri-simulator-motion-pdf/ParticleMotionAnalyzer.h>
#include <gkg-processing-container/Volume_i.h>
#include <gkg-processing-container/MeshMap_i.h>
#include <gkg-core-io/Reader_i.h>
#include <gkg-core-io/Writer_i.h>
#include <gkg-core-io/StringConverter.h>
#include <gkg-core-cppext/UncommentCounterInputFileStream.h>
#include <gkg-core-exception/Exception.h>

#include <iostream>
#include <fstream>
#include <set>


gkg::DwiMRImageSimulatorCommand::DwiMRImageSimulatorCommand( int32_t argc,
                                                             char* argv[],
                                                             bool loadPlugin,
                                                             bool removeFirst )
                                : gkg::Command( argc, argv, loadPlugin,
                                                removeFirst )
{

  try
  {

    parse();

  }
  GKG_CATCH( "gkg::DwiMRImageSimulatorCommand::DwiMRImageSimulatorCommand( "
             "int32_t argc, char* argv[], bool loadPlugin, "
             "bool removeFirst )" );

}


gkg::DwiMRImageSimulatorCommand::DwiMRImageSimulatorCommand(
                             const std::string& sequence,
                             const std::vector< float >& gradientAmplitudes,
                             const std::vector< float >& scalarParameters,
                             const std::vector< std::string >& stringParameters,
                             const std::string& dwOrientationSetFileName,
                             int32_t dwOrientationCount,
                             int32_t startingSession,
                             int32_t sessionCount,
                             const std::string& particleMapFileName,
                             float timeStep,
                             int32_t temporalSubSamplingCount,
                             const std::string& pdfOrientationSetFileName,
                             int32_t pdfOrientationCount,
                             float lowerLimit,
                             float upperLimit,
                             float apertureAngleLimit,
                             const std::vector< float >& mrBoundingBoxVector,
                             const std::vector< int32_t >& mrVolumeSizeVector,
                             float S0,
                             float noiseStdDev,
                             float closeToMembraneLifeTimeFraction,
                             const std::string& t2FileName,
                             const std::string& dwFileName,
                             const std::string& closeToMembraneDwFileName,
                             const std::string& farFromMembraneDwFileName,
                             const std::string& maskFileName,
                             const std::string& meanDisplacementFileName,
                             const std::string& pdfFileName,
                             const std::string& pdfMeshFileName,
                             const std::string& particleDistributionFileName,
                             bool ascii,
                             bool verbose )
                                : gkg::Command()
{

  try
  {

    execute( sequence,
             gradientAmplitudes,
             scalarParameters,
             stringParameters,
             dwOrientationSetFileName,
             dwOrientationCount,
             startingSession,
             sessionCount,
             particleMapFileName,
             timeStep,
             temporalSubSamplingCount,
             pdfOrientationSetFileName,
             pdfOrientationCount,
             lowerLimit,
             upperLimit,
             apertureAngleLimit,
             mrBoundingBoxVector,
             mrVolumeSizeVector,
             S0,
             noiseStdDev,
             closeToMembraneLifeTimeFraction,
             t2FileName,
             dwFileName,
             closeToMembraneDwFileName,
             farFromMembraneDwFileName,
             maskFileName,
             meanDisplacementFileName,
             pdfFileName,
             pdfMeshFileName,
             particleDistributionFileName,
             ascii,
             verbose );

  }
  GKG_CATCH( "gkg::DwiMRImageSimulatorCommand::DwiMRImageSimulatorCommand( "
             "const std::string& sequence, "
             "const std::vector< float >& gradientAmplitudes, "
             "const std::vector< float >& scalarParameters, "
             "const std::vector< std::string >& stringParameters, "
             "const std::string& dwOrientationSetFileName, "
             "int32_t dwOrientationCount, "
             "int32_t startingSession, "
             "int32_t sessionCount, "
             "const std::string& particleMapFileName, "
             "float timeStep, "
             "int32_t temporalSubSamplingCount, "
             "const std::string& pdfOrientationSetFileName, "
             "int32_t pdfOrientationCount, "
             "float lowerLimit, "
             "float upperLimit, "
             "float apertureAngleLimit, "
             "const std::vector< float >& mrBoundingBoxVector, "
             "const std::vector< int32_t >& mrVolumeSizeVector, "
             "float S0, "
             "float noiseStdDev, "
             "float closeToMembraneLifeTimeFraction, "
             "const std::string& t2FileName, "
             "const std::string& dwFileName, "
             "const std::string& closeToMembraneDwFileName, "
             "const std::string& farFromMembraneDwFileName, "
             "const std::string& maskFileName, "
             "const std::string& meanDisplacementFileName, "
             "const std::string& pdfFileName, "
             "const std::string& pdfMeshFileName, "
             "const std::string& particleDistributionFileName, "
             "bool ascii, "
             "bool verbose )" );
 
}


gkg::DwiMRImageSimulatorCommand::DwiMRImageSimulatorCommand(
                                             const gkg::Dictionary& parameters )
                                : gkg::Command( parameters )
{

  try
  {

    DECLARE_STRING_PARAMETER( parameters, std::string, sequence );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER( parameters, std::vector< float >,
                                           gradientAmplitudes );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER( parameters, std::vector< float >,
                                           scalarParameters );
    DECLARE_VECTOR_OF_STRINGS_PARAMETER( parameters, std::vector< std::string >,
                                         stringParameters );
    DECLARE_STRING_PARAMETER( parameters, std::string,
                              dwOrientationSetFileName );
    DECLARE_INTEGER_PARAMETER( parameters, int32_t, dwOrientationCount );
    DECLARE_INTEGER_PARAMETER( parameters, int32_t, startingSession );
    DECLARE_INTEGER_PARAMETER( parameters, int32_t, sessionCount );
    DECLARE_STRING_PARAMETER( parameters, std::string, particleMapFileName );
    DECLARE_FLOATING_PARAMETER( parameters, float, timeStep );
    DECLARE_INTEGER_PARAMETER( parameters, int32_t, temporalSubSamplingCount );
    DECLARE_STRING_PARAMETER( parameters, std::string,
                              pdfOrientationSetFileName );
    DECLARE_INTEGER_PARAMETER( parameters, int32_t, pdfOrientationCount );
    DECLARE_FLOATING_PARAMETER( parameters, float, lowerLimit );
    DECLARE_FLOATING_PARAMETER( parameters, float, upperLimit );
    DECLARE_FLOATING_PARAMETER( parameters, float, apertureAngleLimit );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER( parameters, std::vector< float >,
                                           mrBoundingBoxVector );
    DECLARE_VECTOR_OF_INTEGERS_PARAMETER( parameters, std::vector< int32_t >,
                                          mrVolumeSizeVector );
    DECLARE_FLOATING_PARAMETER( parameters, float, S0 );
    DECLARE_FLOATING_PARAMETER( parameters, float, noiseStdDev );
    DECLARE_FLOATING_PARAMETER( parameters, float, 
                                closeToMembraneLifeTimeFraction );
    DECLARE_STRING_PARAMETER( parameters, std::string, t2FileName );
    DECLARE_STRING_PARAMETER( parameters, std::string, dwFileName );
    DECLARE_STRING_PARAMETER( parameters, std::string,
                              closeToMembraneDwFileName );
    DECLARE_STRING_PARAMETER( parameters, std::string,
                              farFromMembraneDwFileName );
    DECLARE_STRING_PARAMETER( parameters, std::string, maskFileName );
    DECLARE_STRING_PARAMETER( parameters, std::string,
                              meanDisplacementFileName );
    DECLARE_STRING_PARAMETER( parameters, std::string, pdfFileName );
    DECLARE_STRING_PARAMETER( parameters, std::string, pdfMeshFileName );
    DECLARE_STRING_PARAMETER( parameters, std::string,
                              particleDistributionFileName );
    DECLARE_BOOLEAN_PARAMETER( parameters, bool, ascii );
    DECLARE_BOOLEAN_PARAMETER( parameters, bool, verbose );
    
    execute( sequence,
             gradientAmplitudes,
             scalarParameters,
             stringParameters,
             dwOrientationSetFileName,
             dwOrientationCount,
             startingSession,
             sessionCount,
             particleMapFileName,
             timeStep,
             temporalSubSamplingCount,
             pdfOrientationSetFileName,
             pdfOrientationCount,
             lowerLimit,
             upperLimit,
             apertureAngleLimit,
             mrBoundingBoxVector,
             mrVolumeSizeVector,
             S0,
             noiseStdDev,
             closeToMembraneLifeTimeFraction,
             t2FileName,
             dwFileName,
             closeToMembraneDwFileName,
             farFromMembraneDwFileName,
             maskFileName,
             meanDisplacementFileName,
             pdfFileName,
             pdfMeshFileName,
             particleDistributionFileName,
             ascii,
             verbose );

  }
  GKG_CATCH( "gkg::DwiMRImageSimulatorCommand::DwiMRImageSimulatorCommand( "
             "const gkg::Dictionary& parameters )" );

}


gkg::DwiMRImageSimulatorCommand::~DwiMRImageSimulatorCommand()
{
}


std::string gkg::DwiMRImageSimulatorCommand::getStaticName()
{

  try
  {

    return "DwiMRImageSimulator";

  }
  GKG_CATCH( "std::string gkg::DwiMRImageSimulatorCommand::getStaticName()" );

}


void gkg::DwiMRImageSimulatorCommand::parse()
{

  try
  {

    ////////////////////////////////////////////////////////////////////////////
    // building NMR sequence list
    ////////////////////////////////////////////////////////////////////////////

    std::string sequenceText = "NMR sequences : \n";
    std::list< std::string >
      sequenceNameList = gkg::NMRSequenceFactory::getInstance().getNameList();
    std::list< std::string >::const_iterator
      n = sequenceNameList.begin(),
      ne = sequenceNameList.end();
    while ( n != ne )
    {

      sequenceText += "- " + *n + "\n";
      ++ n;

    }

    ////////////////////////////////////////////////////////////////////////////
    // managing command lines argument(s)
    ////////////////////////////////////////////////////////////////////////////

    ////// parameters for the NMR pulse sequence
    std::string sequence;
    std::vector< float > gradientAmplitudes;
    std::vector< float > scalarParameters;
    std::vector< std::string > stringParameters;
    ////// parameters for the DW gradient orientations
    std::string dwOrientationSetFileName;
    int32_t dwOrientationCount = 6;
    ////// parameters for the spins
    int32_t startingSession = 1;
    int32_t sessionCount;
    std::string particleMapFileName;
    float timeStep = 1.0;
    int32_t temporalSubSamplingCount = 1;
    ////// parameters for the diffusion pdf and displacement
    std::string pdfOrientationSetFileName;
    int32_t pdfOrientationCount;
    float lowerLimit = 0.0;
    float upperLimit = 1e38;
    float apertureAngleLimit = 1e38;
    ////// parameters for T2/DW/MASK/PDF/MeanDisplacement outputs
    std::vector< float > mrBoundingBoxVector;
    std::vector< int32_t > mrVolumeSizeVector;
    float S0 = 10000.0;
    float noiseStdDev = 0.0;
    float closeToMembraneLifeTimeFraction = 0.5;
    std::string t2FileName;
    std::string dwFileName;
    std::string closeToMembraneDwFileName;
    std::string farFromMembraneDwFileName;
    std::string maskFileName;
    std::string meanDisplacementFileName;
    std::string pdfFileName;
    std::string pdfMeshFileName;
    std::string particleDistributionFileName;
     ////// parameter for output data format
    bool ascii = false;
    ////// parameter for verbosity
    bool verbose = false;


    gkg::Application application( _argc, _argv,
                                  "Diffusion MR Image simulator",
                                  _loadPlugin );

    application.addSingleOption( "-sequence",
                                 sequenceText,
                                 sequence );

    application.addSeriesOption( "-gradientAmplitudes",
                                 "A set of gradient amplitudes to simulate",
                                 gradientAmplitudes,
                                 1 );

    application.addSeriesOption(
                  "-scalarParameters",
                  "NMR sequence parameters as a vector of float "
                  "<P1> <P2> ... <Pn>:"
                  "\n\n"
                  "- in case of single_pgse_nmr_sequence, \n"
                  ".  <P1>: maximum gradient slew rate in T/m/s \n"
                  ".  <P2>: gradient time resolution in us \n"
                  ".  <P3>: little delta in ms \n"
                  ".  <P4>: big delta in ms"
                  "\n\n"
                  "- in case of multiple_pgse_nmr_sequence, \n"
                  ".  <P1>: maximum gradient slew rate in T/m/s \n"
                  ".  <P2>: gradient time resolution in us \n"
                  ".  <P3>: little delta in ms \n"
                  ".  <P4>: big delta in ms \n"
                  ".  <P5>: mixing time in ms \n"
                  ".  <P6>: gradient pair count"
                  "\n\n"
                  "- in case of stimulated_echo_nmr_sequence, \n"
                  ".  <P1>: maximum gradient slew rate in T/m/s \n"
                  ".  <P2>: gradient time resolution in us \n"
                  ".  <P3>: little delta in ms \n"
                  ".  <P4>: big delta in ms"
                  "\n\n"
                  "- in case of bipolar_ste_nmr_sequence, \n"
                  ".  <P1>: maximum gradient slew rate in T/m/s \n"
                  ".  <P2>: gradient time resolution in us \n"
                  ".  <P3>: little delta in ms \n"
                  ".  <P4>: big delta in ms"
                  "\n\n"
                  "- in case of bipolar_double_ste_nmr_sequence, \n"
                  ".  <P1>: maximum gradient slew rate in T/m/s \n"
                  ".  <P2>: gradient time resolution in us \n"
                  ".  <P3>: little delta in ms \n"
                  ".  <P4>: big delta in ms \n"
                  ".  <P5>: mixing time in ms"
                  "\n\n"
                  "- in case of twice_refocused_se_nmr_sequence \n"
                  ".  <P1>: maximum gradient slew rate in T/m/s \n"
                  ".  <P2>: gradient time resolution in us \n"
                  ".  <P3>: little delta in ms \n"
                  ".  <P4>: big delta in ms"
                  "\n\n"
                  "- in case of ogse_nmr_sequence, \n"
                  ".  <P1>: period of oscillating gradient in ms \n"
                  ".  <P2>: lobe count of oscillating gradient \n"
                  ".  <P3>: maximum gradient slew rate in T/m/s \n"
                  ".  <P4>: gradient time resolution in us"
                  "\n",
                  scalarParameters );

    application.addSeriesOption(
                  "-stringParameters",
                  "NMR sequence parameters as a vector of string "
                  "<P1> <P2> ... <Pn>:"
                  "\n\n"
                  "- in case of single_pgse_nmr_sequence, \n"
                  ".  N/A"
                  "\n\n"
                  "- in case of multiple_pgse_nmr_sequence, \n"
                  ".  <P1>: wavevectors file name"
                  "\n\n"
                  "- in case of stimulated_echo_nmr_sequence, \n"
                  ".  N/A"
                  "\n\n"
                  "- in case of bipolar_ste_nmr_sequence, \n"
                  ".  N/A"
                  "\n\n"
                  "- in case of bipolar_double_ste_nmr_sequence, \n"
                  ".  <P1>: wavevectors file name"
                  "\n\n"
                  "- in case of twice_refocused_se_nmr_sequence \n"
                  ".  N/A"
                  "\n\n"
                  "- in case of ogse_nmr_sequence, \n"
                  ".  <P1>: oscillating gradient waveform \n"
                  ".        - sine \n"
                  ".        - double-sine \n"
                  ".        - cosine \n"
                  ".        - trapezoid"
                  "\n",
                  stringParameters );

    application.addSingleOption( "-dwOrientationSetFileName",
                                 "Discrete orientation file of output DW image",
                                 dwOrientationSetFileName,
                                 true );
    application.addSingleOption( "-dwOrientationCount",
                                 "Discrete orientation count of output DW "
                                 "image (default=6)",
                                 dwOrientationCount,
                                 true );

    application.addSingleOption( "-startingSession",
                                 "Simulation starting session (default=1)",
                                 startingSession,
                                 true );
    application.addSingleOption( "-sessionCount",
                                 "Input simulation session count",
                                 sessionCount );
    application.addSingleOption( "-particleMapFileName",
                                 "Input particle map file name",
                                 particleMapFileName );
    application.addSingleOption( "-timeStep",
                                 "Simulation time step (default=1us)",
                                 timeStep,
                                 true );
    application.addSingleOption( "-temporalSubSamplingCount",
                                 "Temporal Sub-sampling count "
                                 "(default=1, i.e., recording every step)",
                                 temporalSubSamplingCount,
                                 true );

    application.addSingleOption( "-pdfOrientationSetFileName",
                                 "Discrete orientation file for the output Pdf",
                                 pdfOrientationSetFileName,
                                 true );
    application.addSingleOption( "-pdfOrientationCount",
                                 "Pdf orientation count",
                                 pdfOrientationCount,
                                 true );
    application.addSingleOption( "-lowerLimit",
                                 "Lower Limit of displacement (default=0)",
                                 lowerLimit,
                                 true );
    application.addSingleOption( "-upperLimit",
                                 "Upper Limit of displacement (default=1e38)",
                                 upperLimit,
                                 true );
    application.addSingleOption( "-apertureAngleLimit",
                                 "Aperture angle threshold (unit=degree) "
                                 "(default=1e38)",
                                 apertureAngleLimit,
                                 true );

   application.addSeriesOption( "-mrbox",
                                 "Dimension of the MR float bounding box ",
                                 mrBoundingBoxVector,
                                 0,
                                 6 );
    application.addSeriesOption( "-mrVolumeSize",
                                 "Dimension of the MR volume size ",
                                 mrVolumeSizeVector,
                                 0,
                                 3 );
    application.addSingleOption( "-S0",
                                 "Signal value at b = 0 s/mm2 "
                                 "(default=10000.0)",
                                 S0,
                                 true );
    application.addSingleOption( "-noiseStdDev",
                                 "Standard deviation of noise "
                                 "(default=0.0)",
                                 noiseStdDev,
                                 true );
    application.addSingleOption( "-closeToMembraneLifeTimeFraction",
                                 "Close to membrane life time fraction "
                                 "(default=0.5)",
                                 closeToMembraneLifeTimeFraction,
                                 true );
    application.addSingleOption( "-t2",
                                 "Output filename for the T2 image volume",
                                 t2FileName );
    application.addSingleOption( "-dw",
                                 "Output filename for the DW image volume",
                                 dwFileName );
    application.addSingleOption( "-dwClose",
                                 "Output filename for the DW close-to-membrane "
                                 "compartment volume",
                                 closeToMembraneDwFileName,
                                 true );
    application.addSingleOption( "-dwFar",
                                 "Output filename for the DW far-from-membrane "
                                 "compartment volume",
                                 farFromMembraneDwFileName,
                                 true );
    application.addSingleOption( "-m",
                                 "Output filename for the mask image volume",
                                 maskFileName );
    application.addSingleOption( "-meanDisplacement",
                                 "Output filename for the mean displacement "
                                 "volume",
                                 meanDisplacementFileName,
                                 true );
    application.addSingleOption( "-pdf",
                                 "Output filename for the Pdf volume",
                                 pdfFileName,
                                 true );
    application.addSingleOption( "-pdfMesh",
                                 "Output filename for the Pdf meshes",
                                 pdfMeshFileName,
                                 true );
    application.addSingleOption( "-particleDistribution",
                                 "Output filename for the particle "
                                 "Distribution volume",
                                 particleDistributionFileName,
                                 true );

    application.addSingleOption( "-ascii",
                                 "Saving the output data in ascii format",
                                 ascii,
                                 true );

    application.addSingleOption( "-verbose",
                                 "Show as much information as possible",
                                 verbose,
                                 true );

    ////// launching parser
    application.initialize();

    execute( sequence,
             gradientAmplitudes,
             scalarParameters,
             stringParameters,
             dwOrientationSetFileName,
             dwOrientationCount,
             startingSession,
             sessionCount,
             particleMapFileName,
             timeStep,
             temporalSubSamplingCount,
             pdfOrientationSetFileName,
             pdfOrientationCount,
             lowerLimit,
             upperLimit,
             apertureAngleLimit,
             mrBoundingBoxVector,
             mrVolumeSizeVector,
             S0,
             noiseStdDev,
             closeToMembraneLifeTimeFraction,
             t2FileName,
             dwFileName,
             closeToMembraneDwFileName,
             farFromMembraneDwFileName,
             maskFileName,
             meanDisplacementFileName,
             pdfFileName,
             pdfMeshFileName,
             particleDistributionFileName,
             ascii,
             verbose );

  }
  GKG_CATCH_FUNCTOR( "void gkg::DwiMRImageSimulatorCommand::parse()" );

}


void gkg::DwiMRImageSimulatorCommand::execute(
                             const std::string& sequence,
                             const std::vector< float >& gradientAmplitudes,
                             const std::vector< float >& scalarParameters,
                             const std::vector< std::string >& stringParameters,
                             const std::string& dwOrientationSetFileName,
                             int32_t dwOrientationCount,
                             int32_t startingSession,
                             int32_t sessionCount,
                             const std::string& particleMapFileName,
                             float timeStep,
                             int32_t temporalSubSamplingCount,
                             const std::string& pdfOrientationSetFileName,
                             int32_t pdfOrientationCount,
                             float lowerLimit,
                             float upperLimit,
                             float apertureAngleLimit,
                             const std::vector< float >& mrBoundingBoxVector,
                             const std::vector< int32_t >& mrVolumeSizeVector,
                             float S0,
                             float noiseStdDev,
                             float closeToMembraneLifeTimeFraction,
                             const std::string& t2FileName,
                             const std::string& dwFileName,
                             const std::string& closeToMembraneDwFileName,
                             const std::string& farFromMembraneDwFileName,
                             const std::string& maskFileName,
                             const std::string& meanDisplacementFileName,
                             const std::string& pdfFileName,
                             const std::string& pdfMeshFileName,
                             const std::string& particleDistributionFileName,
                             bool ascii,
                             bool verbose )
{

  try
  {

    ////////////////////////////////////////////////////////////////////////////
    // sanity check(s)
    ////////////////////////////////////////////////////////////////////////////

    std::vector< float > theScalarParameters = scalarParameters;
    std::vector< std::string > theStringParameters = stringParameters;

    gkg::NMRSequenceFactory::getInstance().
      checkOrInitializeDefaultParameters( sequence,
                                          theScalarParameters,
                                          theStringParameters );

    if ( ( mrBoundingBoxVector.empty() ) ||
         ( mrBoundingBoxVector.size() != 6U ) )
    {

      throw std::runtime_error( "not a valid MR bounding box" );

    }

    if ( ( mrVolumeSizeVector.empty() ) ||
         ( mrVolumeSizeVector.size() != 3U ) )
    {

      throw std::runtime_error( "not a valid cache size" );

    }


    ////////////////////////////////////////////////////////////////////////////
    // creating MR bounding boxes and volume size
    ////////////////////////////////////////////////////////////////////////////

    gkg::BoundingBox< float > mrBoundingBox( mrBoundingBoxVector[ 0 ],
                                             mrBoundingBoxVector[ 1 ],
                                             mrBoundingBoxVector[ 2 ],
                                             mrBoundingBoxVector[ 3 ],
                                             mrBoundingBoxVector[ 4 ],
                                             mrBoundingBoxVector[ 5 ] );
    gkg::Vector3d< int32_t > mrVolumeSize( mrVolumeSizeVector[ 0 ],
                                           mrVolumeSizeVector[ 1 ],
                                           mrVolumeSizeVector[ 2 ] );


    ////////////////////////////////////////////////////////////////////////////
    // building the DW gradient orientation set
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

      std::cout << "building the DW gradient encoding scheme : "
                << std::flush;

    }

    gkg::RCPointer< gkg::OrientationSet > dwOrientationSet;

    if ( !dwOrientationSetFileName.empty() )
    {

      dwOrientationSet = gkg::RCPointer< gkg::OrientationSet >(
                          new gkg::OrientationSet( dwOrientationSetFileName ) );

    }
    else
    {

      dwOrientationSet = gkg::RCPointer< gkg::OrientationSet >(
                   new gkg::ElectrostaticOrientationSet( dwOrientationCount ) );

    }

    if ( verbose )
    {

      std::cout << "( orientations : " << dwOrientationSet->getCount()
                << " ) done" << std::endl;

    }


    ////////////////////////////////////////////////////////////////////////////
    // initializing NMR pulse sequence
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

      std::cout << "building the NMR pulse sequence : " << sequence
                << " " << std::flush;

    }

    gkg::RCPointer< gkg::NMRSequence >
      nmrSequence = gkg::NMRSequenceFactory::getInstance().create(
                      sequence,
                      std::set< float >( gradientAmplitudes.begin(),
                                         gradientAmplitudes.end() ),
                      theScalarParameters,
                      theStringParameters,
                      verbose );

    nmrSequence->setOrientationSet( dwOrientationSet );
    nmrSequence->setTemporalResolution( timeStep );

    // getting the b-values
    int32_t bValueCount = nmrSequence->getGradientAmplitudeCount();
    std::vector< float > bValues( bValueCount );
    int32_t b = 0;
    for ( b = 0; b < bValueCount; b++ )
    {

      bValues[ b ] = nmrSequence->getBValue( timeStep, b, 0 );

    }

    if ( verbose )
    {

      std::cout << "( TE = " << nmrSequence->getEchoTimeInUs()
                << " ) done" << std::endl;

    }


    ////////////////////////////////////////////////////////////////////////////
    // building MRI noise model
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

      std::cout << "building the MR noise model : " << std::flush;

    }

    gkg::RCPointer< gkg::NoiseModel >
      noiseModel = gkg::NoiseModelFactory::getInstance().
                                           getGaussianNoiseModel( noiseStdDev );

    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }


    ////////////////////////////////////////////////////////////////////////////
    // building MR image synthesizer
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

      std::cout << "building the MR image synthesizer : " << std::flush;

    }

    gkg::RCPointer< gkg::MRImage >
      mrImage( new gkg::MRImage( mrBoundingBox,
                                 mrVolumeSize,
                                 nmrSequence,
                                 noiseModel,
                                 S0,
                                 timeStep,
                                 closeToMembraneLifeTimeFraction) );

    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }


    ////////////////////////////////////////////////////////////////////////////
    // building the particle motion analyzer
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

      std::cout << "building the particle motion analyzer : " << std::flush;

    }

    int32_t stepCount = ( int32_t )( nmrSequence->getEchoTimeInUs() /
                                     timeStep );

    gkg::RCPointer< gkg::ParticleMotionAnalyzer >
      particleMotionAnalyzer( new gkg::ParticleMotionAnalyzer( mrBoundingBox,
                                                               mrVolumeSize,
                                                               stepCount ) );

    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }


    ////////////////////////////////////////////////////////////////////////////
    // building the PDF gradient orientation set
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

      std::cout << "building the pdf gradient encoding scheme : "
                << std::flush;

    }

    gkg::RCPointer< gkg::OrientationSet > pdfOrientationSet;

    if ( !pdfOrientationSetFileName.empty() )
    {

      gkg::OrientationSet orientationSet( pdfOrientationSetFileName );
      std::vector< gkg::Vector3d< float > >
        symmetricalOrientations = orientationSet.getSymmetricalOrientations();

      pdfOrientationSet.reset( new gkg::OrientationSet(
                                                    symmetricalOrientations ) );

    }
    else
    {

      if ( pdfOrientationCount > 0 )
      {

        gkg::OrientationSet orientationSet(
              gkg::ElectrostaticOrientationSet( pdfOrientationCount / 2
                                               ).getSymmetricalOrientations() );
        pdfOrientationSet.reset( new gkg::OrientationSet( orientationSet ) );

      }

    }

    if ( pdfOrientationSet.isNull() )
    {

      throw std::runtime_error( "Invalid Pdf orientation set" );

    }

    particleMotionAnalyzer->setPdfOrientationSet( pdfOrientationSet );

    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }


    ////////////////////////////////////////////////////////////////////////////
    // reading the particle map sesseion data
    ////////////////////////////////////////////////////////////////////////////

    int32_t particleCount = 0;
    int32_t timeStepFactor = 0;

    bool areStatesStored = false;
    int32_t s = 0;
    for ( s = 0; s < sessionCount; s++ )
    {

      //////////////////////////////////////////////////////////////////////////
      // reading the particle map
      //////////////////////////////////////////////////////////////////////////

      if ( verbose )
      {

        std::cout << "reading particle map : ( "
                  << particleMapFileName
                  << "_session_"
                  << s + startingSession
                  << " ): " << std::flush;

      }

      gkg::ParticleMap particleMap;
      gkg::Reader::getInstance().read(
                        particleMapFileName + "_session_" +
                          gkg::StringConverter::toString( s + startingSession ),
                        particleMap,
                        "particlemap" );

      particleCount += particleMap.getParticleCount();

      // sanity check
      if ( s == 0 )
      {

        float monteCarloTimeStep = particleMap.getTimeStep();
        timeStepFactor = ( int32_t )( timeStep / monteCarloTimeStep );

        if ( timeStepFactor == 0 )
        {

          std::cout << "( MRI time step < Monte Carlo simulation time step ), "
                    << "timeStepFactor is set to 1"
                    << std::endl;

          timeStepFactor = 1;

        }
        else if ( ( timeStep / monteCarloTimeStep - ( float )timeStepFactor )
                  != 0.0 )
        {

          std::cout << "timeStepFactor must be integer !" << std::endl;

        }
        std::cout << " ( timeStepFactor = " << timeStepFactor << " ) ";

        // getting the total monte carlo step count
        int32_t pointCount = particleMap.getParticle( 0 )->
                                                getTrajectory().getPointCount();

        if ( ( ( int32_t )( pointCount / timeStepFactor ) ) <
             ( stepCount + 1 ) )
        {

          std::cout << "" << std::endl;
          std::cout << "  Monte Carlo simulation steps     : "
                    << pointCount
                    << std::endl;
          std::cout << "  Time step factor                 : "
                    << timeStepFactor
                    << std::endl;
          std::cout << "  Iteration required by Spin phase : "
                    << stepCount
                    << std::endl;

          throw std::runtime_error( "Insufficient Monte Carlo step count" );

        }

      }

      if ( verbose )
      {

        std::cout << "done" << std::endl;

      }

      //////////////////////////////////////////////////////////////////////////
      // Creating and adding the spins
      //////////////////////////////////////////////////////////////////////////

      if ( verbose )
      {

        std::cout << "creating and adding the spins : " << std::flush;

      }

      // collecting the number of spin to be allocated
      int32_t spinCount = particleMap.getParticleCount();
      std::vector< gkg::RCPointer< gkg::Spin > > spins( spinCount );

      // creating a threaded loop context for spin creation
      gkg::SpinCreationThreadedLoopContext
        spinCreationThreadedLoopContext( spins,
                                         particleMap,
                                         nmrSequence,
                                         timeStep,
                                         timeStepFactor,
                                         temporalSubSamplingCount );

      // creating a threaded loop
      gkg::ThreadedLoop threadedLoop( &spinCreationThreadedLoopContext,
                                      0,
                                      spinCount
                                    );

      // launching the threaded loop
      threadedLoop.launch();

      // creating a spin cache and checking if storing states was asked
      gkg::SpinCache spinCache( mrImage, spins );
      areStatesStored = spinCache.areStatesStored();

      // adding the spins to the MR image synthesizer
      mrImage->addSpins( spinCache );

      if ( verbose )
      {

        std::cout << "done" << std::endl;

      }

      //////////////////////////////////////////////////////////////////////////
      // passing the particle map and updating pdf volumes if required
      //////////////////////////////////////////////////////////////////////////

      if ( ( !meanDisplacementFileName.empty() ) ||
           ( !pdfFileName.empty() ) ||
           ( !pdfMeshFileName.empty() ) )
      {

        if ( verbose )
        {

          std::cout << "updating the diffusion function volumes : "
                    << std::flush;

        }

        particleMotionAnalyzer->updateVolumes( particleMap,
                                               lowerLimit,
                                               upperLimit,
                                               apertureAngleLimit );

        if ( verbose )
        {

          std::cout << "done" << std::endl;

        }

      }

      if ( !particleDistributionFileName.empty() )
      {

        if ( verbose )
        {

          std::cout << "updating the particle distribution volume : "
                    << std::flush;

        }

        particleMotionAnalyzer->updateParticleCountVolume(
                                                     particleMap,
                                                     temporalSubSamplingCount );

        if ( verbose )
        {

          std::cout << "done" << std::endl;

        }

      }

    }

    std::cout << "total particle count = " << particleCount << std::endl;

    ////////////////////////////////////////////////////////////////////////////
    // collecting the MR image volume(s)
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

      std::cout << "collecting the DW-MRI volume(s) : " << std::flush;

    }

    // getting T2 image volume
    {

      gkg::Volume< float > t2Volume;
      mrImage->getT2Volume( t2Volume,
                            true );
      gkg::Writer::getInstance().write( t2FileName,
                                        t2Volume,
                                        ascii );

    }

    // getting DW image volume
    {

      for ( b = 0; b < bValueCount; b++ )
      {

        gkg::Volume< float > dwVolume;
        mrImage->getDWVolume( b, dwVolume );
        gkg::Writer::getInstance().write(
          dwFileName +
          "_o" + gkg::StringConverter::toString(
                     ( int32_t )nmrSequence->getOrientationSet()->getCount() ) +
          "_gIndex" + gkg::StringConverter::toString( b ),
          dwVolume,
          ascii );

      }

    }

    // getting two compartment volume if required
    if ( areStatesStored )
    {

      for ( b = 0; b < bValueCount; b++ )
      {

        gkg::Volume< float > closeToMembraneDwVolume;
        gkg::Volume< float > farFromMembraneDwVolume;
        mrImage->getCloseToAndFarFromMembraneCompartmentDWVolume(
                                              b,
                                              closeToMembraneDwVolume,
                                              farFromMembraneDwVolume,
                                              closeToMembraneLifeTimeFraction );

        gkg::Writer::getInstance().write(
          closeToMembraneDwFileName +
          "_o" + gkg::StringConverter::toString(
                     ( int32_t )nmrSequence->getOrientationSet()->getCount() ) +
          "_gIndex" + gkg::StringConverter::toString( b ),
          closeToMembraneDwVolume,
          ascii );

        gkg::Writer::getInstance().write(
          farFromMembraneDwFileName +
          "_o" + gkg::StringConverter::toString(
                     ( int32_t )nmrSequence->getOrientationSet()->getCount() ) +
          "_gIndex" + gkg::StringConverter::toString( b ),
          farFromMembraneDwVolume,
          ascii );

      }

    }

    // creating a mask
    {

      gkg::Volume< int16_t > maskVolume;
      mrImage->getMaskVolume( maskVolume );
      gkg::Writer::getInstance().write( maskFileName,
                                        maskVolume,
                                        ascii );

    }


    // collecting mean displacement volume if required
    if ( !meanDisplacementFileName.empty() )
    {

      gkg::Volume< float > meanDisplacementVolume;
      particleMotionAnalyzer->getMeanDisplacementVolume(
                                                       meanDisplacementVolume );
      gkg::Writer::getInstance().write( meanDisplacementFileName,
                                        meanDisplacementVolume );

    }

    // collecting pdf volume if required
    if ( !pdfFileName.empty() )
    {

      gkg::Volume< float > pdfVolume;
      particleMotionAnalyzer->getDiffusionPdfVolume( pdfVolume );
      gkg::Writer::getInstance().write( pdfFileName, pdfVolume );

    }

    // collecting pdf meshes
    if ( !pdfMeshFileName.empty() )
    {

      gkg::MeshMap< int32_t, float, 3U > pdfMeshes;
      particleMotionAnalyzer->getDiffusionPdfMeshes( pdfMeshFileName,
                                                     pdfMeshes );
      gkg::Writer::getInstance().write( pdfMeshFileName,
                                        pdfMeshes,
                                        false,
                                        "aimsmesh" );

    }

    // collecting particle distribution volume if required
    if ( !particleDistributionFileName.empty() )
    {

      gkg::Volume< int32_t > particleDistributionVolume;
      particleMotionAnalyzer->getParticleDistributionVolume(
                                                   particleDistributionVolume );
      gkg::Writer::getInstance().write( particleDistributionFileName,
                                        particleDistributionVolume );

    }

    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }

  }
  GKG_CATCH( "void gkg::DwiMRImageSimulatorCommand::execute( "
             "const std::string& sequence, "
             "const std::vector< float >& gradientAmplitudes, "
             "const std::vector< float >& scalarParameters, "
             "const std::vector< std::string >& stringParameters, "
             "const std::string& dwOrientationSetFileName, "
             "int32_t dwOrientationCount, "
             "int32_t startingSession, "
             "int32_t sessionCount, "
             "const std::string& particleMapFileName, "
             "float timeStep, "
             "int32_t temporalSubSamplingCount, "
             "const std::string& pdfOrientationSetFileName, "
             "int32_t pdfOrientationCount, "
             "float lowerLimit, "
             "float upperLimit, "
             "float apertureAngleLimit, "
             "const std::vector< float >& mrBoundingBoxVector, "
             "const std::vector< int32_t >& mrVolumeSizeVector, "
             "float S0, "
             "float noiseStdDev, "
             "float closeToMembraneLifeTimeFraction, "
             "const std::string& t2FileName, "
             "const std::string& dwFileName, "
             "const std::string& closeToMembraneDwFileName, "
             "const std::string& farFromMembraneDwFileName, "
             "const std::string& maskFileName, "
             "const std::string& meanDisplacementFileName, "
             "const std::string& pdfFileName, "
             "const std::string& pdfMeshFileName, "
             "const std::string& particleDistributionFileName, "
             "bool ascii, "
             "bool verbose )" );

}


RegisterCommandCreator( 
    DwiMRImageSimulatorCommand,
    DECLARE_STRING_PARAMETER_HELP( sequence ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( gradientAmplitudes ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( scalarParameters ) +
    DECLARE_VECTOR_OF_STRINGS_PARAMETER_HELP(  stringParameters ) +
    DECLARE_STRING_PARAMETER_HELP( dwOrientationSetFileName ) +
    DECLARE_INTEGER_PARAMETER_HELP( dwOrientationCount ) +
    DECLARE_INTEGER_PARAMETER_HELP( startingSession ) +
    DECLARE_INTEGER_PARAMETER_HELP( sessionCount ) +
    DECLARE_STRING_PARAMETER_HELP( particleMapFileName ) +
    DECLARE_FLOATING_PARAMETER_HELP( timeStep ) +
    DECLARE_INTEGER_PARAMETER_HELP( temporalSubSamplingCount ) +
    DECLARE_STRING_PARAMETER_HELP( pdfOrientationSetFileName ) +
    DECLARE_INTEGER_PARAMETER_HELP( pdfOrientationCount ) +
    DECLARE_FLOATING_PARAMETER_HELP( lowerLimit ) +
    DECLARE_FLOATING_PARAMETER_HELP( upperLimit ) +
    DECLARE_FLOATING_PARAMETER_HELP( apertureAngleLimit ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( mrBoundingBoxVector ) +
    DECLARE_VECTOR_OF_INTEGERS_PARAMETER_HELP( mrVolumeSizeVector ) +
    DECLARE_FLOATING_PARAMETER_HELP( S0 ) +
    DECLARE_FLOATING_PARAMETER_HELP( noiseStdDev ) +
    DECLARE_FLOATING_PARAMETER_HELP( closeToMembraneLifeTimeFraction ) +
    DECLARE_STRING_PARAMETER_HELP( t2FileName ) +
    DECLARE_STRING_PARAMETER_HELP( dwFileName ) +
    DECLARE_STRING_PARAMETER_HELP( closeToMembraneDwFileName ) +
    DECLARE_STRING_PARAMETER_HELP( farFromMembraneDwFileName ) +
    DECLARE_STRING_PARAMETER_HELP( maskFileName ) +
    DECLARE_STRING_PARAMETER_HELP( meanDisplacementFileName ) +
    DECLARE_STRING_PARAMETER_HELP( pdfFileName ) +
    DECLARE_STRING_PARAMETER_HELP( pdfMeshFileName ) +
    DECLARE_STRING_PARAMETER_HELP( particleDistributionFileName ) +
    DECLARE_BOOLEAN_PARAMETER_HELP( ascii ) +
    DECLARE_BOOLEAN_PARAMETER_HELP( verbose ) );
