#ifndef _gkg_dmri_plugin_functors_DwiMRImageSimulator_DwiMRImageSimulatorCommand_h_
#define _gkg_dmri_plugin_functors_DwiMRImageSimulator_DwiMRImageSimulatorCommand_h_


#include <gkg-communication-command/Command.h>
#include <gkg-core-pattern/Creator.h>

#include <string>
#include <vector>


namespace gkg
{

// unit:
// time -> us
// distance -> um

class DwiMRImageSimulatorCommand :
                                 public Command,
                                 public Creator2Arg< DwiMRImageSimulatorCommand,
                                                     Command,
                                                     int32_t,
                                                     char** >,
                                 public Creator1Arg< DwiMRImageSimulatorCommand,
                                                     Command,
                                                     const Dictionary& >
{

  public:

    DwiMRImageSimulatorCommand( int32_t argc,
                                char* argv[],
                                bool loadPlugin = false,
                                bool removeFirst = true );
    DwiMRImageSimulatorCommand(
                   ////// parameters for the NMR pulse sequence
                   const std::string& sequence,
                   const std::vector< float >& gradientAmplitudes,
                   const std::vector< float >& scalarParameters,
                   const std::vector< std::string >& stringParameters,
                   ////// parameters for the DW gradient orientations
                   const std::string& dwOrientationSetFileName,
                   int32_t dwOrientationCount,
                   ////// parameters for the spins
                   int32_t startingSession,
                   int32_t sessionCount,
                   const std::string& particleMapFileName,
                   float timeStep,
                   int32_t temporalSubSamplingCount,
                   ////// parameters for the diffusion pdf and displacement
                   const std::string& pdfOrientationSetFileName,
                   int32_t pdfOrientationCount,
                   float lowerLimit,
                   float upperLimit,
                   float apertureAngleLimit,
                   ////// parameters for T2/DW/MASK/PDF/MeanDisplacement outputs
                   const std::vector< float >& mrBoundingBoxVector,
                   const std::vector< int32_t >& mrVolumeSizeVector,
                   float S0,
                   float noiseStdDev,
                   float closeToMembraneLifeTimeFraction,
                   const std::string& t2FileName,
                   const std::string& dwFileName,
                   const std::string& closeToMembraneDwFileName,
                   const std::string& farFromMembraneDwFileName,
                   const std::string& maskFileName,
                   const std::string& meanDisplacementFileName,
                   const std::string& pdfFileName,
                   const std::string& pdfMeshFileName,
                   const std::string& particleDistributionFileName,
                    ////// parameter for output data format
                   bool ascii,
                   ////// parameter for verbosity
                   bool verbose );
    DwiMRImageSimulatorCommand( const Dictionary& parameters );
    virtual ~DwiMRImageSimulatorCommand();

    static std::string getStaticName();

  protected:

    friend class Creator2Arg< DwiMRImageSimulatorCommand, Command, 
                              int32_t, char** >;
    friend class Creator1Arg< DwiMRImageSimulatorCommand, Command,
                              const Dictionary& >;

    void parse();
    void execute( ////// parameters for the NMR pulse sequence
                  const std::string& sequence,
                  const std::vector< float >& gradientAmplitudes,
                  const std::vector< float >& scalarParameters,
                  const std::vector< std::string >& stringParameters,
                  ////// parameters for the DW gradient orientations
                  const std::string& dwOrientationSetFileName,
                  int32_t dwOrientationCount,
                  ////// parameters for the spins
                  int32_t startingSession,
                  int32_t sessionCount,
                  const std::string& particleMapFileName,
                  float timeStep,
                  int32_t temporalSubSamplingCount,
                  ////// parameters for the diffusion pdf and displacement
                  const std::string& pdfOrientationSetFileName,
                  int32_t pdfOrientationCount,
                  float lowerLimit,
                  float upperLimit,
                  float apertureAngleLimit,
                  ////// parameters for T2/DW/MASK/PDF/MeanDisplacement outputs
                  const std::vector< float >& mrBoundingBoxVector,
                  const std::vector< int32_t >& mrVolumeSizeVector,
                  float S0,
                  float noiseStdDev,
                  float closeToMembraneLifeTimeFraction,
                  const std::string& t2FileName,
                  const std::string& dwFileName,
                  const std::string& closeToMembraneDwFileName,
                  const std::string& farFromMembraneDwFileName,
                  const std::string& maskFileName,
                  const std::string& meanDisplacementFileName,
                  const std::string& pdfFileName,
                  const std::string& pdfMeshFileName,
                  const std::string& particleDistributionFileName,
                   ////// parameter for output data format
                  bool ascii,
                  ////// parameter for verbosity
                  bool verbose );

};


}


#endif
