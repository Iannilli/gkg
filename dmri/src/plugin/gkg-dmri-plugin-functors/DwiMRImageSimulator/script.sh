mkdir dms_mri_example
cd dms_mri_example
GkgDwiMRImageSimulator \
-sequence single_pgse_nmr_sequence \
-gradientAmplitudes 40.0 \
-scalarParameters 200.0 1.0 4.0 10.0 \
-dwOrientationCount 30 \
-startingSession 1 \
-sessionCount 1 \
-particleMapFileName ../particles \
-timeStep 5.0 \
-temporalSubSamplingCount 1 \
-pdfOrientationCount 1000 \
-mrbox -50.0 50.0 -50.0 50.0 -50.0 50.0 \
-mrVolumeSize 3 3 3 \
-S0 10000.0 \
-noiseStdDev 0.0 \
-t2 t2 \
-dw dw \
-m mask \
-meanDisplacement meanDisplacement \
-pdf pdf \
-pdfMesh pdfMesh \
-particleDistribution particleDistribution \
-verbose
cd ..
