#include <gkg-dmri-plugin-functors/DwiMonteCarloSimulator/DwiMonteCarloSimulatorCommand.h>
#include <gkg-communication-command/CommandFactory.h>
#include <gkg-communication-getopt/Application.h>
#include <gkg-communication-sysinfo/FileFinder.h>

#include <gkg-core-pattern/RCPointer.h>

#include <gkg-processing-container/BoundingBox.h>
#include <gkg-dmri-simulator-motion/MotionModelFactory.h>
#include <gkg-dmri-simulator-tissue-modeler/TissueModeler.h>

#include <gkg-processing-mesh/MeshFactory.h>
#include <gkg-processing-mesh/VertexEvolutionFunctionFactory.h>
#include <gkg-dmri-simulator-particle-membrane-interaction/ParticleToMembraneInteractionFactory.h>
#include <gkg-dmri-simulator-particle-membrane-interaction/InteractionLayerFactory.h>
#include <gkg-dmri-simulator-membrane/Membrane.h>

#include <gkg-dmri-simulator-particle/ParticlePositioningFactory.h>
#include <gkg-dmri-simulator-particle/Particle.h>
#include <gkg-dmri-simulator-motion-pdf/ParticleMotionAnalyzer.h>

#include <gkg-processing-container/Volume_i.h>
#include <gkg-processing-container/MeshMap_i.h>
#include <gkg-core-io/Writer_i.h>
#include <gkg-core-io/StringConverter.h>
#include <gkg-core-exception/Exception.h>

#include <iostream>
#include <iostream>
#include <fstream>


gkg::DwiMonteCarloSimulatorCommand::DwiMonteCarloSimulatorCommand(
                                                              int32_t argc,
                                                              char* argv[],
                                                              bool loadPlugin,
                                                              bool removeFirst )
                                   : gkg::Command( argc, argv, loadPlugin,
                                                   removeFirst )
{

  try
  {

    parse();

  }
  GKG_CATCH( "gkg::DwiMonteCarloSimulatorCommand::"
             "DwiMonteCarloSimulatorCommand( "
             "int32_t argc, char* argv[], bool loadPlugin, "
             "bool removeFirst )" );

}


gkg::DwiMonteCarloSimulatorCommand::DwiMonteCarloSimulatorCommand(
           ////// simulation session count
           int32_t sessionCount,
           ////// parameters for the far and close from/to membrane motion model
           float farFromMembraneDiffusivity,
           float closeToMembraneDiffusivity,
           ////// parameters for the tissue modeler
           const std::vector< float >& globalBoundingBoxVector,
           float timeStep,
           int32_t stepCount,
           int32_t temporalSubSamplingCount,
           const std::vector< int32_t >& cacheSizeVector,
           ////// parameters for membranes
           const std::string& fileNameMembraneGeometry,
           const std::string& vertexEvolution,
           float amplitude,
           float period,
           float phase,
           float layerThickness,
           float permeability,
           float radiusOfInfluence,
           bool storeEvolvingMembrane,
           ////// parameters for particles
           int32_t globalParticleCount,
           const std::string& initialParticlePositioning,
           const std::vector< float >& customBoundingBoxVector,
           float intraCellularFraction,
           bool storeCloseToMembranePositionCount,
           const std::string& particleMapFileName,
           int32_t particleFileCount,
           ////// parameters for renderings
           const std::vector< float >& localBoundingBoxVector,
           int32_t localParticleCount,
           float particleSphereRadius,
           int32_t particleSphereVertexCount,
           ////// parameters for 3D rendering outputs
           const std::string& globalBoundingBoxFileName,
           const std::string& localBoundingBoxFileName,
           const std::string& customBoundingBoxFileName,
           const std::string& membraneMeshFileName,
           const std::string& particleTrajectoryFileName,
           const std::string& particleMeshFileName,
           const std::string& particleStartingPositionMeshFileName,
           ////// parameter for verbosity
           bool verbose )
                                   : gkg::Command()
{

  try
  {

    execute( sessionCount,
             farFromMembraneDiffusivity,
             closeToMembraneDiffusivity,
             globalBoundingBoxVector,
             timeStep,
             stepCount,
             temporalSubSamplingCount,
             cacheSizeVector,
             fileNameMembraneGeometry,
             vertexEvolution,
             amplitude,
             period,
             phase,
             layerThickness,
             permeability,
             radiusOfInfluence,
             storeEvolvingMembrane,
             globalParticleCount,
             initialParticlePositioning,
             customBoundingBoxVector,
             intraCellularFraction,
             storeCloseToMembranePositionCount,
             particleMapFileName,
             particleFileCount,
             localBoundingBoxVector,
             localParticleCount,
             particleSphereRadius,
             particleSphereVertexCount,
             globalBoundingBoxFileName,
             localBoundingBoxFileName,
             customBoundingBoxFileName,
             membraneMeshFileName,
             particleTrajectoryFileName,
             particleMeshFileName,
             particleStartingPositionMeshFileName,
             verbose );

  }
  GKG_CATCH( "gkg::DwiMonteCarloSimulatorCommand::"
             "DwiMonteCarloSimulatorCommand( "
             "int32_t sessionCount, "
             "float farFromMembraneDiffusivity, "
             "float closeToMembraneDiffusivity, "
             "const std::vector< float >& globalBoundingBoxVector, "
             "float timeStep, "
             "int32_t stepCount, "
             "int32_t temporalSubSamplingCount, "
             "const std::vector< int32_t >& cacheSizeVector, "
             "const std::string& fileNameMembraneGeometry, "
             "const std::string& vertexEvolution, "
             "float amplitude, "
             "float period, "
             "float phase, "
             "float layerThickness, "
             "float permeability, "
             "float radiusOfInfluence, "
             "bool storeEvolvingMembrane, "
             "int32_t globalParticleCount, "
             "const std::string& initialParticlePositioning, "
             "const std::vector< float >& customBoundingBoxVector, "
             "float intraCellularFraction, "
             "bool storeCloseToMembranePositionCount, "
             "const std::string& particleMapFileName, "
             "int32_t particleFileCount, "
             "const std::vector< float >& localBoundingBoxVector, "
             "int32_t localParticleCount, "
             "float particleSphereRadius, "
             "int32_t particleSphereVertexCount, "
             "const std::string& globalBoundingBoxFileName, "
             "const std::string& localBoundingBoxFileName, "
             "const std::string& customBoundingBoxFileName, "
             "const std::string& membraneMeshFileName, "
             "const std::string& particleTrajectoryFileName, "
             "const std::string& particleMeshFileName, "
             "const std::string& particleStartingPositionMeshFileName, "
             "bool verbose )" );

}


gkg::DwiMonteCarloSimulatorCommand::DwiMonteCarloSimulatorCommand(
                                             const gkg::Dictionary& parameters )
                                   : gkg::Command( parameters )
{

  try
  {

    DECLARE_INTEGER_PARAMETER( parameters, int32_t, sessionCount );
    DECLARE_FLOATING_PARAMETER( parameters, float, farFromMembraneDiffusivity );
    DECLARE_FLOATING_PARAMETER( parameters, float, closeToMembraneDiffusivity );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER( parameters, std::vector< float >,
                                           globalBoundingBoxVector );
    DECLARE_FLOATING_PARAMETER( parameters, float, timeStep );
    DECLARE_INTEGER_PARAMETER( parameters, int32_t, stepCount );
    DECLARE_INTEGER_PARAMETER( parameters, int32_t, temporalSubSamplingCount );
    DECLARE_VECTOR_OF_INTEGERS_PARAMETER( parameters, std::vector< int32_t >,
                                          cacheSizeVector );
    DECLARE_STRING_PARAMETER( parameters, std::string,
                             fileNameMembraneGeometry );
    DECLARE_STRING_PARAMETER( parameters, std::string, vertexEvolution );
    DECLARE_FLOATING_PARAMETER( parameters, float, amplitude );
    DECLARE_FLOATING_PARAMETER( parameters, float, period );
    DECLARE_FLOATING_PARAMETER( parameters, float, phase );
    DECLARE_FLOATING_PARAMETER( parameters, float, layerThickness );
    DECLARE_FLOATING_PARAMETER( parameters, float, permeability );
    DECLARE_FLOATING_PARAMETER( parameters, float, radiusOfInfluence );
    DECLARE_BOOLEAN_PARAMETER( parameters, bool, storeEvolvingMembrane );
    DECLARE_INTEGER_PARAMETER( parameters, int32_t, globalParticleCount );
    DECLARE_STRING_PARAMETER( parameters, std::string,
                              initialParticlePositioning );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER( parameters, std::vector< float >,
                                           customBoundingBoxVector );
    DECLARE_FLOATING_PARAMETER( parameters, float, intraCellularFraction );
    DECLARE_BOOLEAN_PARAMETER( parameters, bool,
                               storeCloseToMembranePositionCount );
    DECLARE_STRING_PARAMETER( parameters, std::string, particleMapFileName );
    DECLARE_INTEGER_PARAMETER( parameters, int32_t, particleFileCount );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER( parameters, std::vector< float >,
                                           localBoundingBoxVector );
    DECLARE_INTEGER_PARAMETER( parameters, int32_t, localParticleCount );
    DECLARE_FLOATING_PARAMETER( parameters, float, particleSphereRadius );
    DECLARE_INTEGER_PARAMETER( parameters, int32_t, particleSphereVertexCount );
    DECLARE_STRING_PARAMETER( parameters, std::string,
                              globalBoundingBoxFileName );
    DECLARE_STRING_PARAMETER( parameters, std::string,
                              localBoundingBoxFileName );
    DECLARE_STRING_PARAMETER( parameters, std::string,
                              customBoundingBoxFileName );
    DECLARE_STRING_PARAMETER( parameters, std::string, membraneMeshFileName );
    DECLARE_STRING_PARAMETER( parameters, std::string, 
                              particleTrajectoryFileName );
    DECLARE_STRING_PARAMETER( parameters, std::string, particleMeshFileName );
    DECLARE_STRING_PARAMETER( parameters, std::string,
                              particleStartingPositionMeshFileName );
    DECLARE_BOOLEAN_PARAMETER( parameters, bool, verbose );
    
    execute( sessionCount,
             farFromMembraneDiffusivity,
             closeToMembraneDiffusivity,
             globalBoundingBoxVector,
             timeStep,
             stepCount,
             temporalSubSamplingCount,
             cacheSizeVector,
             fileNameMembraneGeometry,
             vertexEvolution,
             amplitude,
             period,
             phase,
             layerThickness,
             permeability,
             radiusOfInfluence,
             storeEvolvingMembrane,
             globalParticleCount,
             initialParticlePositioning,
             customBoundingBoxVector,
             intraCellularFraction,
             storeCloseToMembranePositionCount,
             particleMapFileName,
             particleFileCount,
             localBoundingBoxVector,
             localParticleCount,
             particleSphereRadius,
             particleSphereVertexCount,
             globalBoundingBoxFileName,
             localBoundingBoxFileName,
             customBoundingBoxFileName,
             membraneMeshFileName,
             particleTrajectoryFileName,
             particleMeshFileName,
             particleStartingPositionMeshFileName,
             verbose );

  }
  GKG_CATCH( "gkg::DwiMonteCarloSimulatorCommand::"
             "DwiMonteCarloSimulatorCommand( "
             "const gkg::Dictionary& parameters )" );

}


gkg::DwiMonteCarloSimulatorCommand::~DwiMonteCarloSimulatorCommand()
{
}


std::string gkg::DwiMonteCarloSimulatorCommand::getStaticName()
{

  try
  {

    return "DwiMonteCarloSimulator";

  }
  GKG_CATCH( "std::string gkg::DwiMonteCarloSimulatorCommand::getStaticName()" );

}


void gkg::DwiMonteCarloSimulatorCommand::parse()
{

  try
  {

    ////////////////////////////////////////////////////////////////////////////
    // managing command lines argument(s)
    ////////////////////////////////////////////////////////////////////////////

    ////// simulation session count
    int32_t sessionCount = 1;
    ////// parameters for the far and close from/to membrane motion model
    float farFromMembraneDiffusivity = 0.002;
    float closeToMembraneDiffusivity = 0.002;
    ////// parameters for the tissue modeler
    std::vector< float > globalBoundingBoxVector;
    float timeStep = 1.0;
    int32_t stepCount = 100;
    int32_t temporalSubSamplingCount = 1;
    std::vector< int32_t > cacheSizeVector;
    ////// parameters for membranes
    std::string fileNameMembraneGeometry;
    std::string vertexEvolution = "static";
    float amplitude = 0.0;
    float period = 0.0;
    float phase = 0.0;
    float layerThickness = 0.1;
    float permeability = 0.0;
    float radiusOfInfluence = 0.0;
    bool storeEvolvingMembrane = false;
    ////// parameters for particles
    int32_t globalParticleCount = 100;
    std::string initialParticlePositioning = "gbox-uniform-random";
    std::vector< float > customBoundingBoxVector;
    float intraCellularFraction = -1.0;
    bool storeCloseToMembranePositionCount = true;
    std::string particleMapFileName;
    int32_t particleFileCount = 1;
    ////// parameters for renderings
    std::vector< float > localBoundingBoxVector;
    int32_t localParticleCount = globalParticleCount;
    float particleSphereRadius = 0.5;
    int32_t particleSphereVertexCount = 20;
    ////// parameters for 3D rendering outputs
    std::string globalBoundingBoxFileName;
    std::string localBoundingBoxFileName;
    std::string customBoundingBoxFileName;
    std::string membraneMeshFileName;
    std::string particleTrajectoryFileName;
    std::string particleMeshFileName;
    std::string particleStartingPositionMeshFileName;
    ////// parameter for verbosity
    bool verbose = false;


    gkg::Application application( _argc, _argv,
                                  "Diffusion Monte Carlo simulator",
                                  _loadPlugin );

    application.addSingleOption( "-sessionCount",
                                 "Simulation session count (default=1)",
                                 sessionCount,
                                 true );


    application.addSingleOption( "-farFromMembraneDiffusivity",
                                 "Far from membrane diffusion coefficient "
                                 "(default=0.002 um^2/us)",
                                 farFromMembraneDiffusivity,
                                 true );
    application.addSingleOption( "-closeToMembraneDiffusivity",
                                 "Close to membrane diffusion coefficient "
                                 "(default=0.002 um^2/us)",
                                 closeToMembraneDiffusivity,
                                 true );

    application.addSeriesOption( "-gbox",
                                 "Dimension of the Global float bounding box "
                                 "(default=(-50.0,+50.0,"
                                           "-50.0,+50.0,"
                                           "-50.0,+50.0) (um) )",
                                 globalBoundingBoxVector,
                                 0,
                                 6 );
    application.addSingleOption( "-timeStep",
                                 "Time step of each iteration (default=1.0 us)",
                                 timeStep,
                                 true );
    application.addSingleOption( "-stepCount",
                                 "The number of simulation step "
                                 "(default=100)",
                                 stepCount,
                                 true );
    application.addSingleOption( "-temporalSubSamplingCount",
                                 "Temporal sub-sampling count "
                                 "(default=1, i.e., recording every step)",
                                 temporalSubSamplingCount,
                                 true );
    application.addSeriesOption( "-cacheSize",
                                 "Dimension of the cache volume size "
                                 "(default=100,100,100)",
                                 cacheSizeVector,
                                 0,
                                 3 );

    application.addSingleOption( "-geometry",
                                 "Membrane geometry file name",
                                 fileNameMembraneGeometry );

    application.addSingleOption( "-vertexEvolution",
                                 "Vertex evolution function of"
                                 " evolving meshes:\n"
                                 "- static\n"
                                 "- sinusoid\n"
                                 "(default=static)",
                                 vertexEvolution,
                                 true );
    application.addSingleOption( "-amplitude",
                                 "amplitude of sinusoid vertex evolution "
                                 "(default=0.0um)",
                                 amplitude,
                                 true );
    application.addSingleOption( "-period",
                                 "period of sinusoid vertex evolution "
                                 "(default=0.0)",
                                 period,
                                 true );
    application.addSingleOption( "-phase",
                                 "phase of sinusoid vertex evolution "
                                 "(default=0.0)",
                                 phase,
                                 true );
    application.addSingleOption( "-layerThickness",
                                 "Layer thickness close to the membrane "
                                 "(default=0.1um)",
                                 layerThickness,
                                 true );
    application.addSingleOption( "-permeability",
                                 "Permeability of the membrane "
                                 "(default=0.0)",
                                 permeability,
                                 true );
    application.addSingleOption( "-radiusOfInfluence",
                                 "Radius of influence of the membrane for the "
                                 "membrane cache (default=0.0)",
                                 radiusOfInfluence,
                                 true );
    application.addSingleOption( "-storeEvolvingMembrane",
                                 "Storing dynamic membrane mesh evolution "
                                 "(default=false)",
                                 storeEvolvingMembrane,
                                 true );

    application.addSingleOption( "-globalParticleCount",
                                 "Global particle count per session "
                                 "(default=100)",
                                 globalParticleCount,
                                 true );
    application.addSingleOption( "-initialParticlePositioning",
                                 "Initial particle positioning:\n"
                                 "- gbox-uniform-random\n"
                                 "- gbox-center\n"
                                 "- intra-extra\n"
                                 "- custom-box-uniform-random\n"
                                 "- custom-position\n"
                                 "(default=gbox-uniform-random)",
                                 initialParticlePositioning,
                                 true );
    application.addSeriesOption( "-cbox",
                                 "Dimension of the custom float bounding box "
                                 "for partiles' positions initialization "
                                 "(default=gbox)",
                                 customBoundingBoxVector,
                                 0,
                                 6 );
    application.addSingleOption( "-intraCellularFraction",
                                 "Fraction of particles initialized in the "
                                 "intra-celluar space (default=unspecified)",
                                 intraCellularFraction,
                                 true );
    application.addSingleOption( "-storeCloseToMembranePositionCount",
                                 "Storing close to membrane position count "
                                 "(default=true)",
                                 storeCloseToMembranePositionCount,
                                 true );
    application.addSingleOption( "-o",
                                 "Output particle map file name",
                                 particleMapFileName );
    application.addSingleOption( "-particleFileCount",
                                 "Output particle map file count (default=1)",
                                 particleFileCount,
                                 true );

    application.addSeriesOption( "-lbox",
                                 "Local float bounding box "
                                 "(default=gbox)",
                                 localBoundingBoxVector,
                                 0,
                                 6 );
    application.addSingleOption( "-localParticleCount",
                                 "Local particle count "
                                 "(default=global particle)",
                                 localParticleCount,
                                 true );
    application.addSingleOption( "-particleSphereRadius",
                                 "Sphere radius of the particle mesh "
                                 "(default=0.5 um)",
                                 particleSphereRadius,
                                 true );
    application.addSingleOption( "-particleSphereVertexCount",
                                 "Sphere vertex count of the particle mesh "
                                 "(default=20)",
                                 particleSphereVertexCount,
                                 true );

    application.addSingleOption( "-globalBoundingBoxFileName",
                                 "Output file name for the global bounding box "
                                 "rendering ",
                                 globalBoundingBoxFileName,
                                 true );
    application.addSingleOption( "-localBoundingBoxFileName",
                                 "Output file name for the local bounding box "
                                 "rendering ",
                                 localBoundingBoxFileName,
                                 true );
    application.addSingleOption( "-customBoundingBoxFileName",
                                 "Output file name for the custom bounding box "
                                 "rendering ",
                                 customBoundingBoxFileName,
                                 true );
    application.addSingleOption( "-membraneMeshFileName",
                                 "Output file name for the local membrane "
                                 "mesh renderings ",
                                 membraneMeshFileName,
                                 true );
    application.addSingleOption( "-particleTrajectoryFileName",
                                 "Output file name for the local particle "
                                 "trajectory renderings ",
                                 particleTrajectoryFileName,
                                 true );
    application.addSingleOption( "-particleMeshFileName",
                                 "Output file name for the local particle "
                                 "mesh renderings ",
                                 particleMeshFileName,
                                 true );
    application.addSingleOption( "-particleStartingPositionMeshFileName",
                                 "Output file name for the mesh renderings of "
                                 "particles' starting positions ",
                                 particleStartingPositionMeshFileName,
                                 true );

    application.addSingleOption( "-verbose",
                                 "Show as much information as possible",
                                 verbose,
                                 true );

    ////// launching parser
    application.initialize();

    execute( sessionCount,
             farFromMembraneDiffusivity,
             closeToMembraneDiffusivity,
             globalBoundingBoxVector,
             timeStep,
             stepCount,
             temporalSubSamplingCount,
             cacheSizeVector,
             fileNameMembraneGeometry,
             vertexEvolution,
             amplitude,
             period,
             phase,
             layerThickness,
             permeability,
             radiusOfInfluence,
             storeEvolvingMembrane,
             globalParticleCount,
             initialParticlePositioning,
             customBoundingBoxVector,
             intraCellularFraction,
             storeCloseToMembranePositionCount,
             particleMapFileName,
             particleFileCount,
             localBoundingBoxVector,
             localParticleCount,
             particleSphereRadius,
             particleSphereVertexCount,
             globalBoundingBoxFileName,
             localBoundingBoxFileName,
             customBoundingBoxFileName,
             membraneMeshFileName,
             particleTrajectoryFileName,
             particleMeshFileName,
             particleStartingPositionMeshFileName,
             verbose );

  }
  GKG_CATCH_FUNCTOR( "void gkg::DwiMonteCarloSimulatorCommand::parse()" );

}


void gkg::DwiMonteCarloSimulatorCommand::execute(
           ////// simulation session count
           int32_t sessionCount,
           ////// parameters for the far and close from/to membrane motion model
           float farFromMembraneDiffusivity,
           float closeToMembraneDiffusivity,
           ////// parameters for the tissue modeler
           const std::vector< float >& globalBoundingBoxVector,
           float timeStep,
           int32_t stepCount,
           int32_t temporalSubSamplingCount,
           const std::vector< int32_t >& cacheSizeVector,
           ////// parameters for membranes
           const std::string& fileNameMembraneGeometry,
           const std::string& vertexEvolution,
           float amplitude,
           float period,
           float phase,
           float layerThickness,
           float permeability,
           float radiusOfInfluence,
           bool storeEvolvingMembrane,
           ////// parameters for particles
           int32_t globalParticleCount,
           const std::string& initialParticlePositioning,
           const std::vector< float >& customBoundingBoxVector,
           float intraCellularFraction,
           bool storeCloseToMembranePositionCount,
           const std::string& particleMapFileName,
           int32_t particleFileCount,
           ////// parameters for renderings
           const std::vector< float >& localBoundingBoxVector,
           int32_t localParticleCount,
           float particleSphereRadius,
           int32_t particleSphereVertexCount,
           ////// parameters for 3D rendering outputs
           const std::string& globalBoundingBoxFileName,
           const std::string& localBoundingBoxFileName,
           const std::string& customBoundingBoxFileName,
           const std::string& membraneMeshFileName,
           const std::string& particleTrajectoryFileName,
           const std::string& particleMeshFileName,
           const std::string& particleStartingPositionMeshFileName,
           ////// parameter for verbosity
           bool verbose )
{

  try
  {

    ////////////////////////////////////////////////////////////////////////////
    // sanity check(s)
    ////////////////////////////////////////////////////////////////////////////

    std::vector< float > theGlobalBoundingBoxVector = globalBoundingBoxVector;
    if ( theGlobalBoundingBoxVector.empty() )
    {

      theGlobalBoundingBoxVector.resize( 6 );
      theGlobalBoundingBoxVector[ 0 ] = -50.0;
      theGlobalBoundingBoxVector[ 1 ] = +50.0;
      theGlobalBoundingBoxVector[ 2 ] = -50.0;
      theGlobalBoundingBoxVector[ 3 ] = +50.0;
      theGlobalBoundingBoxVector[ 4 ] = -50.0;
      theGlobalBoundingBoxVector[ 5 ] = +50.0;

    }
    else if ( theGlobalBoundingBoxVector.size() != 6U )
    {

      throw std::runtime_error( "not a valid global bounding box" );

    }

    std::vector< int32_t > theCacheSizeVector = cacheSizeVector;
    if ( theCacheSizeVector.empty() )
    {

      theCacheSizeVector.resize( 3 );
      theCacheSizeVector[ 0 ] = 100;
      theCacheSizeVector[ 1 ] = 100;
      theCacheSizeVector[ 2 ] = 100;

    }
    else if ( theCacheSizeVector.size() != 3U )
    {

      throw std::runtime_error( "not a valid cache size" );

    }

    std::vector< float > theCustomBoundingBoxVector = customBoundingBoxVector;
    if ( theCustomBoundingBoxVector.empty() )
    {

      theCustomBoundingBoxVector = theGlobalBoundingBoxVector;

    }
    else if ( theCustomBoundingBoxVector.size() != 6U )
    {

      throw std::runtime_error( "not a valid custom bounding box" );

    }

    std::vector< float > theLocalBoundingBoxVector = localBoundingBoxVector;
    if ( theLocalBoundingBoxVector.empty() )
    {

      theLocalBoundingBoxVector = theGlobalBoundingBoxVector;

    }
    else if ( theLocalBoundingBoxVector.size() != 6U )
    {

      throw std::runtime_error( "not a valid local bounding box" );

    }

    gkg::FileFinder fileFinder;
    if ( !fileFinder.locateFromPath( fileNameMembraneGeometry ) )
    {

      throw std::runtime_error( "Membrane geometry file not found" );

    }

    ////////////////////////////////////////////////////////////////////////////
    // creating global and local bounding boxes
    ////////////////////////////////////////////////////////////////////////////

    gkg::BoundingBox< float >
      globalBoundingBox( theGlobalBoundingBoxVector[ 0 ],
                         theGlobalBoundingBoxVector[ 1 ],
                         theGlobalBoundingBoxVector[ 2 ],
                         theGlobalBoundingBoxVector[ 3 ],
                         theGlobalBoundingBoxVector[ 4 ],
                         theGlobalBoundingBoxVector[ 5 ] );
    gkg::BoundingBox< float >
      customBoundingBox( theCustomBoundingBoxVector[ 0 ],
                         theCustomBoundingBoxVector[ 1 ],
                         theCustomBoundingBoxVector[ 2 ],
                         theCustomBoundingBoxVector[ 3 ],
                         theCustomBoundingBoxVector[ 4 ],
                         theCustomBoundingBoxVector[ 5 ] );
    gkg::BoundingBox< float >
     localBoundingBox( theLocalBoundingBoxVector[ 0 ],
                       theLocalBoundingBoxVector[ 1 ],
                       theLocalBoundingBoxVector[ 2 ],
                       theLocalBoundingBoxVector[ 3 ],
                       theLocalBoundingBoxVector[ 4 ],
                       theLocalBoundingBoxVector[ 5 ] );


    ////////////////////////////////////////////////////////////////////////////
    // building the far from membrane motion model
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

      std::cout << "building the far from membrane motion model : "
                << std::flush;

    }

    gkg::RCPointer< gkg::MotionModel > farFromMembraneMotionModel =
      gkg::MotionModelFactory::getInstance().getRandomWalkModel(
                                                   farFromMembraneDiffusivity );

    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }


    ////////////////////////////////////////////////////////////////////////////
    // building the tissue modeler
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

      std::cout << "building the tissue modeler : " << std::flush;

    }

    gkg::Vector3d< int32_t > cacheSize( theCacheSizeVector[ 0 ],
                                        theCacheSizeVector[ 1 ],
                                        theCacheSizeVector[ 2 ] );
    gkg::RCPointer< gkg::TissueModeler >
      tissueModeler( new gkg::TissueModeler( globalBoundingBox,
                                             farFromMembraneMotionModel,
                                             timeStep,
                                             stepCount,
                                             temporalSubSamplingCount,
                                             cacheSize,
                                             verbose ) );

    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }


    ////////////////////////////////////////////////////////////////////////////
    // creating membrane meshes
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

      std::cout << "creating membrane meshes : " << std::flush;

    }

    std::map< std::string,
              gkg::RCPointer< gkg::MeshMap< int32_t, float, 3U > > >
      meshes = gkg::MeshFactory::getInstance().getMeshes(
                                                     fileNameMembraneGeometry );

    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }


    ////////////////////////////////////////////////////////////////////////////
    // adding membranes to the tissue modeler
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

      std::cout << "adding membranes to the tissue modeler : " << std::flush;

    }

    // building the static vertex evolution function
    gkg::RCPointer< gkg::VertexEvolutionFunction > vertexEvolutionFunction;
    if ( vertexEvolution == "static" )
    {

      vertexEvolutionFunction =
        gkg::VertexEvolutionFunctionFactory::getInstance().
                                             getStaticVertexEvolutionFunction();

    }
    else if ( vertexEvolution == "sinusoid" )
    {

      vertexEvolutionFunction =
        gkg::VertexEvolutionFunctionFactory::getInstance().
                                 getSinusVertexEvolutionFunction( tissueModeler,
                                                                  amplitude,
                                                                  period,
                                                                  phase );

    }
    else
    {

      throw std::runtime_error( "bad vertex evolution function for meshes" );

    }

    // building the particle to membrane interaction
    gkg::RCPointer< gkg::ParticleToMembraneInteraction >
      particleToMembraneInteraction =
        gkg::ParticleToMembraneInteractionFactory::getInstance().
          getTotalReflectionInteraction();

    // building the close-to-membrane motion model
    gkg::RCPointer< gkg::MotionModel > closeToMembraneMotionModel =
      gkg::MotionModelFactory::getInstance().getRandomWalkModel(
                                                   closeToMembraneDiffusivity );

    // building the interaction layer
    gkg::RCPointer< gkg::InteractionLayer >
      interactionLayer = gkg::InteractionLayerFactory::getInstance().
                                     getThickInteractionLayer( layerThickness );

    // buiding the membranes
    std::map< std::string,
              gkg::RCPointer<
                gkg::MeshMap< int32_t, float, 3U > > >::const_iterator
      m = meshes.begin(),
      me = meshes.end();
    while ( m != me )
    {

      if ( verbose )
      {

        std::cout << m->first << std::flush;

      }

      gkg::RCPointer< gkg::Membrane >
        membrane( new gkg::Membrane( tissueModeler,
                                     m->second,
                                     vertexEvolutionFunction,
                                     particleToMembraneInteraction,
                                     closeToMembraneMotionModel,
                                     interactionLayer,
                                     permeability,
                                     radiusOfInfluence,
                                     storeEvolvingMembrane ) );

      // adding the membranes to the tissue modeler
      tissueModeler->addMembrane( m->first, membrane );

      if ( verbose )
      {

        std::cout << " ( done ) " << std::flush;

      }

      ++ m;

    }

    if ( verbose )
    {

      std::cout << std::endl;

    }


    //////////////////////////////////////////////////////////////////////////
    // performing simulation sessions
    //////////////////////////////////////////////////////////////////////////

    // allocating particles
    std::vector< gkg::RCPointer< gkg::Particle > > particles(
                                                          globalParticleCount );
    int32_t p = 0;
    for ( p = 0; p < globalParticleCount; p++ )
    {

        particles[ p ].reset( new gkg::Particle(
                                          tissueModeler,
                                          storeCloseToMembranePositionCount ) );

    }

    // allocating positions
    std::vector< gkg::Vector3d< float > > positions;

    // looping over simulation sessions
    int32_t s = 0;
    for ( s = 0; s < sessionCount; s++ )
    {

      //////////////////////////////////////////////////////////////////////////
      // adding particles to the tissue modeler
      //////////////////////////////////////////////////////////////////////////

      if ( verbose )
      {

        std::cout << "adding particles to the tissue modeler (session "
                  << s + 1
                  << ") : " << std::flush;

      }

      if ( initialParticlePositioning == "gbox-uniform-random" )
      {

        gkg::ParticlePositioningFactory::getInstance().
          getBoxUniformRandomPositions( globalBoundingBox,
                                        globalParticleCount,
                                        positions );

      }
      else if ( initialParticlePositioning == "gbox-center" )
      {

        gkg::ParticlePositioningFactory::getInstance().
          getBoxCenterPosition( globalBoundingBox,
                                globalParticleCount,
                                positions );

      }
      else if ( initialParticlePositioning == "intra-extra" )
      {

        gkg::ParticlePositioningFactory::getInstance().
          getIntraExtraMembranePositions( tissueModeler,
                                          customBoundingBox,
                                          intraCellularFraction,
                                          globalParticleCount,
                                          positions );

      }
      else if ( initialParticlePositioning == "custom-box-uniform-random" )
      {

        gkg::ParticlePositioningFactory::getInstance().
          getBoxUniformRandomPositions( customBoundingBox,
                                        globalParticleCount,
                                        positions );

      }
      else if ( initialParticlePositioning == "custom-position" )
      {

        gkg::Vector3d< float > point;
        gkg::ParticlePositioningFactory::getInstance().
          getSpecifiedPosition( point, globalParticleCount, positions );

      }
      else
      {

        throw std::runtime_error( "bad initial particle positioning" );

      }

      // initializing particles' positions
      for ( p = 0; p < globalParticleCount; p++ )
      {

        particles[ p ]->initialize( positions[ p ] );

      }
      positions.clear();

      // adding particles to tissue modeler
      tissueModeler->addParticles( particles );

      // collecting meshes for particles' initial positions
      if ( ( !particleStartingPositionMeshFileName.empty() ) &&
           ( s == 0 ) )
      {

        gkg::MeshMap< int32_t, float, 3U > particleStartingPositionMeshes;
        tissueModeler->getGlobalParticleMesh( particles,
                                              localParticleCount,
                                              particleSphereRadius,
                                              particleSphereVertexCount,
                                              particleStartingPositionMeshes );
        gkg::Writer::getInstance().write( particleStartingPositionMeshFileName,
                                          particleStartingPositionMeshes,
                                          false,
                                          "aimsmesh" );

      }

      if ( verbose )
      {

        std::cout << "done" << std::endl;

      }

      //////////////////////////////////////////////////////////////////////////
      // performing the simulation for the current session
      //////////////////////////////////////////////////////////////////////////

      if ( verbose )
      {

        std::cout << "simulating (session "
                  << s + 1
                  << ") : " << std::flush;

      }

      tissueModeler->simulate();

      if ( verbose )
      {

        std::cout << std::endl;

      }

      //////////////////////////////////////////////////////////////////////////
      // collecting particles of interest
      //////////////////////////////////////////////////////////////////////////

      if ( verbose )
      {

        std::cout << "collecting the particles of interest : " << std::flush;

      }

      std::vector< gkg::RCPointer< gkg::Particle > > particlesOfInterest;
      tissueModeler->getParticlesOfInterest( globalBoundingBox,
                                             stepCount,
                                             particlesOfInterest );
      gkg::ParticleMap particleMap( particlesOfInterest,
                                    particleFileCount,
                                    timeStep );

      if ( verbose )
      {

        std::cout << "done" << std::endl;

      }

      //////////////////////////////////////////////////////////////////////////
      // writing the particle map
      //////////////////////////////////////////////////////////////////////////

      if ( verbose )
      {

        std::cout << "writing the particle map : " << std::flush;

      }

      gkg::Writer::getInstance().write( particleMapFileName + "_session_" +
                                        gkg::StringConverter::toString( s + 1 ),
                                        particleMap,
                                        false,
                                        "particlemap" );

      if ( verbose )
      {

        std::cout << "done" << std::endl;

      }

    }


    ////////////////////////////////////////////////////////////////////////////
    // collecting the bounding box outline
    ////////////////////////////////////////////////////////////////////////////

    if ( !globalBoundingBoxFileName.empty() )
    {

      if ( verbose )
      {

        std::cout << "collecting the global bounding box : " << std::flush;

      }
      gkg::Curve3dMap< float > globalBoundingBoxOutline;
      tissueModeler->getBoundingBoxOutline( globalBoundingBox,
                                            globalBoundingBoxOutline );
      gkg::Writer::getInstance().write( globalBoundingBoxFileName,
                                        globalBoundingBoxOutline,
                                        false,
                                        "aimsmesh" );
      if ( verbose )
      {

        std::cout << "done" << std::endl;

      }

    }

    if ( !localBoundingBoxFileName.empty() )
    {

      if ( verbose )
      {

        std::cout << "collecting the local bounding box : " << std::flush;

      }
      gkg::Curve3dMap< float > localBoundingBoxOutline;
      tissueModeler->getBoundingBoxOutline( localBoundingBox,
                                            localBoundingBoxOutline );
      gkg::Writer::getInstance().write( localBoundingBoxFileName,
                                        localBoundingBoxOutline,
                                        false,
                                        "aimsmesh" );
      if ( verbose )
      {

        std::cout << "done" << std::endl;

      }

    }

    if ( !customBoundingBoxFileName.empty() )
    {

      if ( verbose )
      {

        std::cout << "collecting the custom bounding box : " << std::flush;

      }
      gkg::Curve3dMap< float > customBoundingBoxOutline;
      tissueModeler->getBoundingBoxOutline( customBoundingBox,
                                            customBoundingBoxOutline );
      gkg::Writer::getInstance().write( customBoundingBoxFileName,
                                        customBoundingBoxOutline,
                                        false,
                                        "aimsmesh" );
      if ( verbose )
      {

        std::cout << "done" << std::endl;

      }

    }


    ////////////////////////////////////////////////////////////////////////////
    // collecting the membrane meshes
    ////////////////////////////////////////////////////////////////////////////

    if ( !membraneMeshFileName.empty() )
    {

      if ( verbose )
      {

        std::cout << "collecting the membrane meshes : " << std::flush;

      }
      gkg::MeshMap< int32_t, float, 3U > membraneMeshes;
      tissueModeler->getGlobalEvolvingMesh( localBoundingBox,
                                            membraneMeshes );
      gkg::Writer::getInstance().write( membraneMeshFileName,
                                        membraneMeshes,
                                        false,
                                        "aimsmesh" );
      if ( verbose )
      {

        std::cout << "done" << std::endl;

      }

    }


    ////////////////////////////////////////////////////////////////////////////
    // building the particle motion analyzer
    ////////////////////////////////////////////////////////////////////////////

    if ( ( !particleTrajectoryFileName.empty() ) ||
         ( !particleMeshFileName.empty() ) )
    {

      if ( verbose )
      {

        std::cout << "building the particle motion analyzer : " << std::flush;

      }
      gkg::RCPointer< gkg::ParticleMotionAnalyzer >
        particleMotionAnalyzer( new gkg::ParticleMotionAnalyzer(
                                                              globalBoundingBox,
                                                              cacheSize,
                                                              stepCount ) );
      if ( verbose )
      {

        std::cout << "done" << std::endl;

      }

      //////////////////////////////////////////////////////////////////////////
      // collecting the particle trajectories
      //////////////////////////////////////////////////////////////////////////

      if ( !particleTrajectoryFileName.empty() )
      {

        if ( verbose )
        {

          std::cout << "collecting the particle trajectories : " << std::flush;

        }
        gkg::Curve3dMap< float > particleTrajectories;
        particleMotionAnalyzer->getParticleTrajectories(
                                                tissueModeler->getParticleMap(),
                                                localBoundingBox,
                                                localParticleCount,
                                                temporalSubSamplingCount,
                                                particleTrajectories );
        gkg::Writer::getInstance().write( particleTrajectoryFileName,
                                          particleTrajectories,
                                          false,
                                          "aimsmesh" );
        if ( verbose )
        {

          std::cout << "done" << std::endl;

        }

      }

      //////////////////////////////////////////////////////////////////////////
      // collecting the particle meshes
      //////////////////////////////////////////////////////////////////////////

      if ( !particleMeshFileName.empty() )
      {

        if ( verbose )
        {

          std::cout << "collecting the particle meshes : " << std::flush;

        }
        gkg::MeshMap< int32_t, float, 3U > particleMeshes;
        particleMotionAnalyzer->getParticleMeshes(
                                                tissueModeler->getParticleMap(),
                                                localBoundingBox,
                                                localParticleCount,
                                                particleSphereRadius,
                                                particleSphereVertexCount,
                                                temporalSubSamplingCount,
                                                particleMeshes );
        gkg::Writer::getInstance().write( particleMeshFileName,
                                          particleMeshes,
                                          false,
                                          "aimsmesh" );
        if ( verbose )
        {

          std::cout << "done" << std::endl;

        }

      }

    }

  }
  GKG_CATCH( "void gkg::DwiMonteCarloSimulatorCommand::execute( "
             "DwiMonteCarloSimulatorCommand( "
             "int32_t sessionCount, "
             "float farFromMembraneDiffusivity, "
             "float closeToMembraneDiffusivity, "
             "const std::vector< float >& globalBoundingBoxVector, "
             "float timeStep, "
             "int32_t stepCount, "
             "int32_t temporalSubSamplingCount, "
             "const std::vector< int32_t >& cacheSizeVector, "
             "const std::string& fileNameMembraneGeometry, "
             "const std::string& vertexEvolution, "
             "float amplitude, "
             "float period, "
             "float phase, "
             "float layerThickness, "
             "float permeability, "
             "float radiusOfInfluence, "
             "bool storeEvolvingMembrane, "
             "int32_t globalParticleCount, "
             "const std::string& initialParticlePositioning, "
             "const std::vector< float >& customBoundingBoxVector, "
             "float intraCellularFraction, "
             "bool storeCloseToMembranePositionCount, "
             "const std::string& particleMapFileName, "
             "int32_t particleFileCount, "
             "const std::vector< float >& localBoundingBoxVector, "
             "int32_t localParticleCount, "
             "float particleSphereRadius, "
             "int32_t particleSphereVertexCount, "
             "const std::string& globalBoundingBoxFileName, "
             "const std::string& localBoundingBoxFileName, "
             "const std::string& customBoundingBoxFileName, "
             "const std::string& membraneMeshFileName, "
             "const std::string& particleTrajectoryFileName, "
             "const std::string& particleMeshFileName, "
             "const std::string& particleStartingPositionMeshFileName, "
             "bool verbose )" );

}


RegisterCommandCreator(
    DwiMonteCarloSimulatorCommand,
    DECLARE_INTEGER_PARAMETER_HELP( sessionCount ) +
    DECLARE_FLOATING_PARAMETER_HELP( farFromMembraneDiffusivity ) +
    DECLARE_FLOATING_PARAMETER_HELP( closeToMembraneDiffusivity ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( globalBoundingBoxVector ) +
    DECLARE_FLOATING_PARAMETER_HELP( timeStep ) +
    DECLARE_INTEGER_PARAMETER_HELP( stepCount ) +
    DECLARE_INTEGER_PARAMETER_HELP( temporalSubSamplingCount ) +
    DECLARE_VECTOR_OF_INTEGERS_PARAMETER_HELP( cacheSizeVector ) +
    DECLARE_STRING_PARAMETER_HELP( fileNameMembraneGeometry ) +
    DECLARE_STRING_PARAMETER_HELP( vertexEvolution ) +
    DECLARE_FLOATING_PARAMETER_HELP( amplitude ) +
    DECLARE_FLOATING_PARAMETER_HELP( period ) +
    DECLARE_FLOATING_PARAMETER_HELP( phase ) +
    DECLARE_FLOATING_PARAMETER_HELP( layerThickness ) +
    DECLARE_FLOATING_PARAMETER_HELP( permeability ) +
    DECLARE_FLOATING_PARAMETER_HELP( radiusOfInfluence ) +
    DECLARE_BOOLEAN_PARAMETER_HELP( storeEvolvingMembrane ) +
    DECLARE_INTEGER_PARAMETER_HELP( globalParticleCount ) +
    DECLARE_STRING_PARAMETER_HELP( initialParticlePositioning ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( customBoundingBoxVector ) +
    DECLARE_FLOATING_PARAMETER_HELP( intraCellularFraction ) +
    DECLARE_BOOLEAN_PARAMETER_HELP( storeCloseToMembranePositionCount ) +
    DECLARE_STRING_PARAMETER_HELP( particleMapFileName ) +
    DECLARE_INTEGER_PARAMETER_HELP( particleFileCount ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( localBoundingBoxVector ) +
    DECLARE_INTEGER_PARAMETER_HELP( localParticleCount ) +
    DECLARE_FLOATING_PARAMETER_HELP( particleSphereRadius ) +
    DECLARE_INTEGER_PARAMETER_HELP( particleSphereVertexCount ) +
    DECLARE_STRING_PARAMETER_HELP( globalBoundingBoxFileName ) +
    DECLARE_STRING_PARAMETER_HELP( localBoundingBoxFileName ) +
    DECLARE_STRING_PARAMETER_HELP( customBoundingBoxFileName ) +
    DECLARE_STRING_PARAMETER_HELP( membraneMeshFileName ) +
    DECLARE_STRING_PARAMETER_HELP( particleTrajectoryFileName ) +
    DECLARE_STRING_PARAMETER_HELP( particleMeshFileName ) +
    DECLARE_STRING_PARAMETER_HELP( particleStartingPositionMeshFileName ) +
    DECLARE_BOOLEAN_PARAMETER_HELP( verbose ) );
