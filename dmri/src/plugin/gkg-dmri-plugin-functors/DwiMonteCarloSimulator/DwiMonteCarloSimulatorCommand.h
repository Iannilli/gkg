#ifndef _gkg_dmri_plugin_functors_DwiMonteCarloSimulator_DwiMonteCarloSimulatorCommand_h_
#define _gkg_dmri_plugin_functors_DwiMonteCarloSimulator_DwiMonteCarloSimulatorCommand_h_


#include <gkg-communication-command/Command.h>
#include <gkg-core-pattern/Creator.h>

#include <string>
#include <vector>


namespace gkg
{


// unit: time (us); distance (um)


class DwiMonteCarloSimulatorCommand :
                              public Command,
                              public Creator2Arg< DwiMonteCarloSimulatorCommand,
                                                  Command,
                                                  int32_t,
                                                  char** >,
                              public Creator1Arg< DwiMonteCarloSimulatorCommand,
                                                  Command,
                                                  const Dictionary& >
{

  public:

    DwiMonteCarloSimulatorCommand( int32_t argc,
                  char* argv[], 
                  bool loadPlugin = false,
                  bool removeFirst = true );
    DwiMonteCarloSimulatorCommand( 
           ////// simulation session count
           int32_t sessionCount,
           ////// parameters for the far and close from/to membrane motion model
           float farFromMembraneDiffusivity,
           float closeToMembraneDiffusivity,
           ////// parameters for the tissue modeler
           const std::vector< float >& globalBoundingBoxVector,
           float timeStep,
           int32_t stepCount,
           int32_t temporalSubSamplingCount,
           const std::vector< int32_t >& cacheSizeVector,
           ////// parameters for membranes
           const std::string& fileNameMembraneGeometry,
           const std::string& vertexEvolution,
           float amplitude,
           float period,
           float phase,
           float layerThickness,
           float permeability,
           float radiusOfInfluence,
           bool storeEvolvingMembrane,
           ////// parameters for particles
           int32_t globalParticleCount,
           const std::string& initialParticlePositioning,
           const std::vector< float >& customBoundingBoxVector,
           float intraCellularFraction,
           bool storeCloseToMembranePositionCount,
           const std::string& particleMapFileName,
           int32_t particleFileCount,
           ////// parameters for renderings
           const std::vector< float >& localBoundingBoxVector,
           int32_t localParticleCount,
           float particleSphereRadius,
           int32_t particleSphereVertexCount,
           ////// parameters for 3D rendering outputs
           const std::string& globalBoundingBoxFileName,
           const std::string& localBoundingBoxFileName,
           const std::string& customBoundingBoxFileName,
           const std::string& membraneMeshFileName,
           const std::string& particleTrajectoryFileName,
           const std::string& particleMeshFileName,
           const std::string& particleStartingPositionMeshFileName,
           ////// parameter for verbosity
           bool verbose );
    DwiMonteCarloSimulatorCommand( const Dictionary& parameters );
    virtual ~DwiMonteCarloSimulatorCommand();

    static std::string getStaticName();

  protected:

    friend class Creator2Arg< DwiMonteCarloSimulatorCommand, Command, 
                              int32_t, char** >;
    friend class Creator1Arg< DwiMonteCarloSimulatorCommand, Command,
                              const Dictionary& >;

    void parse();
    void execute(
           ////// simulation session count
           int32_t sessionCount,
           ////// parameters for the far and close from/to membrane motion model
           float farFromMembraneDiffusivity,
           float closeToMembraneDiffusivity,
           ////// parameters for the tissue modeler
           const std::vector< float >& globalBoundingBoxVector,
           float timeStep,
           int32_t stepCount,
           int32_t temporalSubSamplingCount,
           const std::vector< int32_t >& cacheSizeVector,
           ////// parameters for membranes
           const std::string& fileNameMembraneGeometry,
           const std::string& vertexEvolution,
           float amplitude,
           float period,
           float phase,
           float layerThickness,
           float permeability,
           float radiusOfInfluence,
           bool storeEvolvingMembrane,
           ////// parameters for particles
           int32_t globalParticleCount,
           const std::string& initialParticlePositioning,
           const std::vector< float >& customBoundingBoxVector,
           float intraCellularFraction,
           bool storeCloseToMembranePositionCount,
           const std::string& particleMapFileName,
           int32_t particleFileCount,
           ////// parameters for renderings
           const std::vector< float >& localBoundingBoxVector,
           int32_t localParticleCount,
           float particleSphereRadius,
           int32_t particleSphereVertexCount,
           ////// parameters for 3D rendering outputs
           const std::string& globalBoundingBoxFileName,
           const std::string& localBoundingBoxFileName,
           const std::string& customBoundingBoxFileName,
           const std::string& membraneMeshFileName,
           const std::string& particleTrajectoryFileName,
           const std::string& particleMeshFileName,
           const std::string& particleStartingPositionMeshFileName,
           ////// parameter for verbosity
           bool verbose );

};


}


#endif
