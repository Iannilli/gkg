mkdir dms_mcs_example
cd dms_mcs_example
GkgDwiMonteCarloSimulator \
-sessionCount 1 \
-farFromMembraneDiffusivity 0.002 \
-closeToMembraneDiffusivity 0.002 \
-gbox -50.0 50.0 -50.0 50.0 -50.0 50.0 \
-timeStep 5.0 \
-stepCount 10000 \
-temporalSubSamplingCount 50 \
-cacheSize 100 100 100 \
-geometry ../example.membrane \
-vertexEvolution static \
-layerThickness 0.0 \
-permeability 0.0 \
-storeEvolvingMembrane false \
-globalParticleCount 5000 \
-initialParticlePositioning custom-box-uniform-random \
-cbox -40.0 40.0 -40.0 40.0 -40.0 40.0 \
-storeCloseToMembranePositionCount false \
-o particles \
-particleFileCount 1 \
-lbox -50.0 50.0 -50.0 50.0 -50.0 50.0 \
-localParticleCount 5000 \
-particleSphereRadius 0.25 \
-particleSphereVertexCount 30 \
-globalBoundingBoxFileName gbox \
-localBoundingBoxFileName lbox \
-customBoundingBoxFileName cbox \
-membraneMeshFileName membranes \
-particleTrajectoryFileName trajectories \
-particleMeshFileName particles \
-verbose
cd ..
