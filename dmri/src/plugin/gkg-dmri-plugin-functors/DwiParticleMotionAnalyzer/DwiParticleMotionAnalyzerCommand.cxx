#include <gkg-dmri-plugin-functors/DwiParticleMotionAnalyzer/DwiParticleMotionAnalyzerCommand.h>
#include <gkg-communication-command/CommandFactory.h>
#include <gkg-communication-getopt/Application.h>
#include <gkg-core-pattern/RCPointer.h>
#include <gkg-processing-container/BoundingBox.h>
#include <gkg-dmri-simulator-motion-pdf/ParticleMotionAnalyzer.h>
#include <gkg-processing-coordinates/ElectrostaticOrientationSet.h>
#include <gkg-processing-container/Volume_i.h>
#include <gkg-processing-container/MeshMap_i.h>
#include <gkg-core-io/Reader_i.h>
#include <gkg-core-io/Writer_i.h>
#include <gkg-core-io/StringConverter.h>
#include <gkg-core-exception/Exception.h>

#include <iostream>
#include <fstream>


gkg::DwiParticleMotionAnalyzerCommand::DwiParticleMotionAnalyzerCommand(
                                                              int32_t argc,
                                                              char* argv[],
                                                              bool loadPlugin,
                                                              bool removeFirst )
                                      : gkg::Command( argc, argv, loadPlugin,
                                                      removeFirst )
{

  try
  {

    parse();

  }
  GKG_CATCH( "gkg::DwiParticleMotionAnalyzerCommand::"
             "DwiParticleMotionAnalyzerCommand( "
             "int32_t argc, char* argv[], bool loadPlugin, "
             "bool removeFirst )" );

}


gkg::DwiParticleMotionAnalyzerCommand::DwiParticleMotionAnalyzerCommand(
                            int32_t sessionCount,
                            const std::string& particleMapFileName,
                            const std::vector< float >& globalBoundingBoxVector,
                            const std::vector< int32_t >& volumeSizeVector,
                            int32_t stepCount,
                            const std::string& pdfOrientationSetFileName,
                            int32_t pdfOrientationCount,
                            float lowerLimit,
                            float upperLimit,
                            float apertureAngleLimit,
                            const std::vector< float >& localBoundingBoxVector,
                            int32_t localParticleCount,
                            int32_t temporalSubSamplingCount,
                            float particleSphereRadius,
                            int32_t particleSphereVertexCount,
                            const std::string& meanDisplacementFileName,
                            const std::string& pdfFileName,
                            const std::string& pdfMeshFileName,
                            const std::string& particleDistributionFileName,
                            const std::string& globalBoundingBoxFileName,
                            const std::string& localBoundingBoxFileName,
                            const std::string& particleTrajectoryFileName,
                            const std::string& particleMeshFileName,
                            bool verbose )
                                      : gkg::Command()
{

  try
  {

    execute( sessionCount,
             particleMapFileName,
             globalBoundingBoxVector,
             volumeSizeVector,
             stepCount,
             pdfOrientationSetFileName,
             pdfOrientationCount,
             lowerLimit,
             upperLimit,
             apertureAngleLimit,
             localBoundingBoxVector,
             localParticleCount,
             temporalSubSamplingCount,
             particleSphereRadius,
             particleSphereVertexCount,
             meanDisplacementFileName,
             pdfFileName,
             pdfMeshFileName,
             particleDistributionFileName,
             globalBoundingBoxFileName,
             localBoundingBoxFileName,
             particleTrajectoryFileName,
             particleMeshFileName,
             verbose );

  }
  GKG_CATCH( "gkg::DwiParticleMotionAnalyzerCommand::"
             "DwiParticleMotionAnalyzerCommand( "
             "int32_t sessionCount, "
             "const std::string& particleMapFileName, "
             "const std::vector< float >& globalBoundingBoxVector, "
             "const std::vector< int32_t >& volumeSizeVector, "
             "int32_t stepCount, "
             "const std::string& pdfOrientationSetFileName, "
             "int32_t pdfOrientationCount, "
             "float lowerLimit, "
             "float upperLimit, "
             "float apertureAngleLimit, "
             "const std::vector< float >& localBoundingBoxVector, "
             "int32_t localParticleCount, "
             "int32_t temporalSubSamplingCount, "
             "float particleSphereRadius, "
             "int32_t particleSphereVertexCount, "
             "const std::string& meanDisplacementFileName, "
             "const std::string& pdfFileName, "
             "const std::string& pdfMeshFileName, "
             "const std::string& particleDistributionFileName, "
             "const std::string& globalBoundingBoxFileName, "
             "const std::string& localBoundingBoxFileName, "
             "const std::string& particleTrajectoryFileName, "
             "const std::string& particleMeshFileName, "
             "bool verbose )" );

}


gkg::DwiParticleMotionAnalyzerCommand::DwiParticleMotionAnalyzerCommand(
                                             const gkg::Dictionary& parameters )
                                      : gkg::Command( parameters )
{

  try
  {

    DECLARE_INTEGER_PARAMETER( parameters, int32_t, sessionCount );
    DECLARE_STRING_PARAMETER( parameters, std::string, particleMapFileName );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER( parameters, std::vector< float >,
                                           globalBoundingBoxVector );
    DECLARE_VECTOR_OF_INTEGERS_PARAMETER( parameters, std::vector< int32_t >,
                                          volumeSizeVector );
    DECLARE_INTEGER_PARAMETER( parameters, int32_t, stepCount );
    DECLARE_STRING_PARAMETER( parameters, std::string, pdfOrientationSetFileName );
    DECLARE_INTEGER_PARAMETER( parameters, int32_t, pdfOrientationCount );
    DECLARE_FLOATING_PARAMETER( parameters, float, lowerLimit );
    DECLARE_FLOATING_PARAMETER( parameters, float, upperLimit );
    DECLARE_FLOATING_PARAMETER( parameters, float, apertureAngleLimit );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER( parameters, std::vector< float >,
                                           localBoundingBoxVector );
    DECLARE_INTEGER_PARAMETER( parameters, int32_t, localParticleCount );
    DECLARE_INTEGER_PARAMETER( parameters, int32_t, temporalSubSamplingCount );
    DECLARE_FLOATING_PARAMETER( parameters, float, particleSphereRadius );
    DECLARE_INTEGER_PARAMETER( parameters, int32_t, particleSphereVertexCount );

    DECLARE_STRING_PARAMETER( parameters, std::string,
                              meanDisplacementFileName );
    DECLARE_STRING_PARAMETER( parameters, std::string, pdfFileName );
    DECLARE_STRING_PARAMETER( parameters, std::string, pdfMeshFileName );
    DECLARE_STRING_PARAMETER( parameters, std::string, 
                              particleDistributionFileName );
    DECLARE_STRING_PARAMETER( parameters, std::string, 
                              globalBoundingBoxFileName );
    DECLARE_STRING_PARAMETER( parameters, std::string, 
                              localBoundingBoxFileName );
    DECLARE_STRING_PARAMETER( parameters, std::string,
                              particleTrajectoryFileName );
    DECLARE_STRING_PARAMETER( parameters, std::string, particleMeshFileName );
    DECLARE_BOOLEAN_PARAMETER( parameters, bool, verbose );
    
    execute( sessionCount,
             particleMapFileName,
             globalBoundingBoxVector,
             volumeSizeVector,
             stepCount,
             pdfOrientationSetFileName,
             pdfOrientationCount,
             lowerLimit,
             upperLimit,
             apertureAngleLimit,
             localBoundingBoxVector,
             localParticleCount,
             temporalSubSamplingCount,
             particleSphereRadius,
             particleSphereVertexCount,
             meanDisplacementFileName,
             pdfFileName,
             pdfMeshFileName,
             particleDistributionFileName,
             globalBoundingBoxFileName,
             localBoundingBoxFileName,
             particleTrajectoryFileName,
             particleMeshFileName,
             verbose );

  }
  GKG_CATCH( "gkg::DwiParticleMotionAnalyzerCommand::"
             "DwiParticleMotionAnalyzerCommand( "
             "const gkg::Dictionary& parameters )" );

}


gkg::DwiParticleMotionAnalyzerCommand::~DwiParticleMotionAnalyzerCommand()
{
}


std::string gkg::DwiParticleMotionAnalyzerCommand::getStaticName()
{

  try
  {

    return "DwiParticleMotionAnalyzer";

  }
  GKG_CATCH( "std::string gkg::DwiParticleMotionAnalyzerCommand::getStaticName()" );

}


void gkg::DwiParticleMotionAnalyzerCommand::parse()
{

  try
  {

    ////// simulation session count
    int32_t sessionCount = 1;
    std::string particleMapFileName;

    ////// parameters for the particle motion analyzer
    std::vector< float > globalBoundingBoxVector;
    std::vector< int32_t > volumeSizeVector;
    int32_t stepCount = 100;

    ////// parameters for the diffusion functions
    std::string pdfOrientationSetFileName;
    int32_t pdfOrientationCount;
    float lowerLimit = 0.0;
    float upperLimit = 1e38;
    float apertureAngleLimit = 1e38;

    ////// parameters for renderings
    std::vector< float > localBoundingBoxVector;
    int32_t localParticleCount = 100;
    int32_t temporalSubSamplingCount = 1;
    float particleSphereRadius = 0.01;
    int32_t particleSphereVertexCount = 20;

    ////// parameters for output volumes and 3D renderings
    std::string meanDisplacementFileName;
    std::string pdfFileName;
    std::string pdfMeshFileName;
    std::string particleDistributionFileName;
    std::string globalBoundingBoxFileName;
    std::string localBoundingBoxFileName;
    std::string particleTrajectoryFileName;
    std::string particleMeshFileName;

    ////// parameter for verbosity
    bool verbose = false;

    ////////////////////////////////////////////////////////////////////////////
    // managing command lines argument(s)
    ////////////////////////////////////////////////////////////////////////////

    gkg::Application application( _argc, _argv,
                                  "Diffusion particle motion analyzer",
                                  _loadPlugin );

    application.addSingleOption( "-sessionCount",
                                 "Simulation session count (default=1)",
                                 sessionCount,
                                 true );
    application.addSingleOption( "-particleMapFileName",
                                 "Input particle map file name",
                                 particleMapFileName );

    application.addSeriesOption( "-gbox",
                                 "Dimension of the global float bounding box "
                                 "(um)",
                                 globalBoundingBoxVector,
                                 0,
                                 6 );
    application.addSeriesOption( "-volumeSize",
                                 "Dimension of the pdf volume size ",
                                 volumeSizeVector,
                                 0,
                                 3 );
    application.addSingleOption( "-stepCount",
                                 "The number of simulation step "
                                 "(default=100)",
                                 stepCount,
                                 true );

    application.addSingleOption( "-pdfOrientationSetFileName",
                                 "Discrete orientation file for the output Pdf",
                                 pdfOrientationSetFileName,
                                 true );
    application.addSingleOption( "-pdfOrientationCount",
                                 "Pdf orientation count",
                                 pdfOrientationCount,
                                 true );
    application.addSingleOption( "-lowerLimit",
                                 "Lower Limit of displacement (default=0)",
                                 lowerLimit,
                                 true );
    application.addSingleOption( "-upperLimit",
                                 "Upper Limit of displacement (default=1e38)",
                                 upperLimit,
                                 true );
    application.addSingleOption( "-apertureAngleLimit",
                                 "Aperture angle threshold (default=1e38)",
                                 apertureAngleLimit,
                                 true );

    application.addSeriesOption( "-lbox",
                                 "Local float bounding box "
                                 "(default=gbox)",
                                 localBoundingBoxVector,
                                 0,
                                 6 );
    application.addSingleOption( "-localParticleCount",
                                 "Local particle count "
                                 "(default=100)",
                                 localParticleCount,
                                 true );
    application.addSingleOption( "-particleSphereRadius",
                                 "Sphere radius of the particle mesh "
                                 "(default=0.01 um)",
                                 particleSphereRadius,
                                 true );
    application.addSingleOption( "-particleSphereVertexCount",
                                 "Sphere vertex count of the particle mesh "
                                 "(default=20)",
                                 particleSphereVertexCount,
                                 true );
    application.addSingleOption( "-temporalSubSamplingCount",
                                 "Temporal sub-sampling count "
                                 "(default=1, i.e., recording every step)",
                                 temporalSubSamplingCount,
                                 true );

    application.addSingleOption( "-meanDisplacementFileName",
                                 "Output filename for the mean displacement "
                                 "volume",
                                 meanDisplacementFileName,
                                 true );
    application.addSingleOption( "-pdfFileName",
                                 "Output filename for the Pdf volume",
                                 pdfFileName,
                                 true );
    application.addSingleOption( "-pdfMeshFileName",
                                 "Output filename for the Pdf meshes",
                                 pdfMeshFileName,
                                 true );
    application.addSingleOption( "-particleDistribution",
                                 "Output filename for the particle "
                                 "Distribution volume",
                                 particleDistributionFileName,
                                 true );
    application.addSingleOption( "-globalBoundingBoxFileName",
                                 "Output file name for the global bounding box "
                                 "rendering ",
                                 globalBoundingBoxFileName,
                                 true );
    application.addSingleOption( "-localBoundingBoxFileName",
                                 "Output file name for the local bounding box "
                                 "rendering ",
                                 localBoundingBoxFileName,
                                 true );
    application.addSingleOption( "-particleTrajectoryFileName",
                                 "Output file name for the local particle "
                                 "trajectory renderings ",
                                 particleTrajectoryFileName,
                                 true );
    application.addSingleOption( "-particleMeshFileName",
                                 "Output file name for the local particle "
                                 "mesh renderings ",
                                 particleMeshFileName,
                                 true );

    application.addSingleOption( "-verbose",
                                 "Show as much information as possible",
                                 verbose,
                                 true );

    ////// launching parser
    application.initialize();

    execute( sessionCount,
             particleMapFileName,
             globalBoundingBoxVector,
             volumeSizeVector,
             stepCount,
             pdfOrientationSetFileName,
             pdfOrientationCount,
             lowerLimit,
             upperLimit,
             apertureAngleLimit,
             localBoundingBoxVector,
             localParticleCount,
             temporalSubSamplingCount,
             particleSphereRadius,
             particleSphereVertexCount,
             meanDisplacementFileName,
             pdfFileName,
             pdfMeshFileName,
             particleDistributionFileName,
             globalBoundingBoxFileName,
             localBoundingBoxFileName,
             particleTrajectoryFileName,
             particleMeshFileName,
             verbose );

  }
  GKG_CATCH_FUNCTOR( "void gkg::DwiParticleMotionAnalyzerCommand::parse()" );

}


void gkg::DwiParticleMotionAnalyzerCommand::execute(
                            int32_t sessionCount,
                            const std::string& particleMapFileName,
                            const std::vector< float >& globalBoundingBoxVector,
                            const std::vector< int32_t >& volumeSizeVector,
                            int32_t stepCount,
                            const std::string& pdfOrientationSetFileName,
                            int32_t pdfOrientationCount,
                            float lowerLimit,
                            float upperLimit,
                            float apertureAngleLimit,
                            const std::vector< float >& localBoundingBoxVector,
                            int32_t localParticleCount,
                            int32_t temporalSubSamplingCount,
                            float particleSphereRadius,
                            int32_t particleSphereVertexCount,
                            const std::string& meanDisplacementFileName,
                            const std::string& pdfFileName,
                            const std::string& pdfMeshFileName,
                            const std::string& particleDistributionFileName,
                            const std::string& globalBoundingBoxFileName,
                            const std::string& localBoundingBoxFileName,
                            const std::string& particleTrajectoryFileName,
                            const std::string& particleMeshFileName,
                            bool verbose )
{

  try
  {

    ////////////////////////////////////////////////////////////////////////////
    // sanity check(s)
    ////////////////////////////////////////////////////////////////////////////

    if ( ( globalBoundingBoxVector.empty() ) ||
         ( globalBoundingBoxVector.size() != 6U ) )
    {

      throw std::runtime_error( "not a valid global bounding box" );

    }

    if ( ( volumeSizeVector.empty() ) ||
         ( volumeSizeVector.size() != 3U ) )
    {

      throw std::runtime_error( "not a valid pdf volume size" );

    }

    std::vector< float > theLocalBoundingBoxVector = localBoundingBoxVector;
    if ( theLocalBoundingBoxVector.empty() )
    {

      theLocalBoundingBoxVector = globalBoundingBoxVector;

    }
    else if ( theLocalBoundingBoxVector.size() != 6U )
    {

      throw std::runtime_error( "not a valid local bounding box" );

    }


    ////////////////////////////////////////////////////////////////////////////
    // creating global & local bounding boxes, and Pdf volume size
    ////////////////////////////////////////////////////////////////////////////

    gkg::BoundingBox< float > globalBoundingBox( globalBoundingBoxVector[ 0 ],
                                                 globalBoundingBoxVector[ 1 ],
                                                 globalBoundingBoxVector[ 2 ],
                                                 globalBoundingBoxVector[ 3 ],
                                                 globalBoundingBoxVector[ 4 ],
                                                 globalBoundingBoxVector[ 5 ] );
    gkg::BoundingBox< float >
      localBoundingBox( theLocalBoundingBoxVector[ 0 ],
                        theLocalBoundingBoxVector[ 1 ],
                        theLocalBoundingBoxVector[ 2 ],
                        theLocalBoundingBoxVector[ 3 ],
                        theLocalBoundingBoxVector[ 4 ],
                        theLocalBoundingBoxVector[ 5 ] );
    gkg::Vector3d< int32_t > volumeSize( volumeSizeVector[ 0 ],
                                         volumeSizeVector[ 1 ],
                                         volumeSizeVector[ 2 ] );


    ////////////////////////////////////////////////////////////////////////////
    // building the particle motion analyzer
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

      std::cout << "building the particle motion analyzer : " << std::flush;

    }

    gkg::RCPointer< gkg::ParticleMotionAnalyzer >
      particleMotionAnalyzer( new gkg::ParticleMotionAnalyzer(
                                                   globalBoundingBox,
                                                   volumeSize,
                                                   stepCount ) );

    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }


    ////////////////////////////////////////////////////////////////////////////
    // building the PDF gradient orientation set
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

      std::cout << "building the pdf gradient encoding scheme : "
                << std::flush;

    }

    gkg::RCPointer< gkg::OrientationSet > pdfOrientationSet;

    if ( !pdfOrientationSetFileName.empty() )
    {

      gkg::OrientationSet orientationSet( pdfOrientationSetFileName );
      std::vector< gkg::Vector3d< float > >
        symmetricalOrientations = orientationSet.getSymmetricalOrientations();

      pdfOrientationSet.reset( new gkg::OrientationSet(
                                                    symmetricalOrientations ) );

    }
    else if ( pdfOrientationCount > 0 )
    {

      gkg::OrientationSet orientationSet(
              gkg::ElectrostaticOrientationSet( pdfOrientationCount / 2
                                               ).getSymmetricalOrientations() );
      pdfOrientationSet.reset( new gkg::OrientationSet( orientationSet ) );

    }

    particleMotionAnalyzer->setPdfOrientationSet( pdfOrientationSet );

    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }


    ////////////////////////////////////////////////////////////////////////////
    // reading the particle map sesseion data
    ////////////////////////////////////////////////////////////////////////////

    int32_t s = 0;
    for ( s = 0; s < sessionCount; s++ )
    {

      //////////////////////////////////////////////////////////////////////////
      // reading the particle map
      //////////////////////////////////////////////////////////////////////////

      if ( verbose )
      {

        std::cout << "reading particle map : ( "
                  << particleMapFileName
                  << "_session_"
                  << s + 1
                  << " ): " << std::flush;

      }

      gkg::ParticleMap particleMap;
      gkg::Reader::getInstance().read( particleMapFileName + "_session_" +
                                       gkg::StringConverter::toString( s + 1 ),
                                       particleMap,
                                       "particlemap" );

      if ( verbose )
      {

        std::cout << "done" << std::endl;

      }


      //////////////////////////////////////////////////////////////////////////
      // passing the particle map and updating pdf volumes if required
      //////////////////////////////////////////////////////////////////////////

      if ( ( !meanDisplacementFileName.empty() ) ||
           ( !pdfFileName.empty() ) ||
           ( !pdfMeshFileName.empty() ) )
      {

        if ( verbose )
        {

          std::cout << "updating the diffusion function volumes : "
                    << std::flush;

        }

        particleMotionAnalyzer->updateVolumes( particleMap,
                                               lowerLimit,
                                               upperLimit,
                                               apertureAngleLimit );

        if ( verbose )
        {

          std::cout << "done" << std::endl;

        }

      }

      if ( !particleDistributionFileName.empty() )
      {

        if ( verbose )
        {

          std::cout << "updating the particle distribution volume : "
                    << std::flush;

        }

        particleMotionAnalyzer->updateParticleCountVolume(
                                                     particleMap,
                                                     temporalSubSamplingCount );

        if ( verbose )
        {

          std::cout << "done" << std::endl;

        }

      }

      //////////////////////////////////////////////////////////////////////////
      // collecting the particle trajectories for the current session
      //////////////////////////////////////////////////////////////////////////

      if ( !particleTrajectoryFileName.empty() )
      {

        if ( verbose )
        {

          std::cout << "collecting the particle trajectories : " << std::flush;

        }

        gkg::Curve3dMap< float > particleTrajectories;
        particleMotionAnalyzer->getParticleTrajectories(
                                                       particleMap,
                                                       localBoundingBox,
                                                       localParticleCount,
                                                       temporalSubSamplingCount,
                                                       particleTrajectories );
        gkg::Writer::getInstance().write(
                                       particleTrajectoryFileName+ "_session_" +
                                       gkg::StringConverter::toString( s + 1 ),
                                       particleTrajectories,
                                       false,
                                       "aimsmesh" );

        if ( verbose )
        {

          std::cout << "done" << std::endl;

        }

      }


      //////////////////////////////////////////////////////////////////////////
      // collecting the particle meshes
      //////////////////////////////////////////////////////////////////////////
    
      if ( !particleMeshFileName.empty() )
      {

        if ( verbose )
        {

          std::cout << "collecting the particle meshes : " << std::flush;

        }

        gkg::MeshMap< int32_t, float, 3U > particleMeshes;
        particleMotionAnalyzer->getParticleMeshes( particleMap,
                                                   localBoundingBox,
                                                   localParticleCount,
                                                   particleSphereRadius,
                                                   particleSphereVertexCount,
                                                   temporalSubSamplingCount,
                                                   particleMeshes );
        gkg::Writer::getInstance().write(
                                        particleMeshFileName + "_session_" +
                                        gkg::StringConverter::toString( s + 1 ),
                                        particleMeshes,
                                        false,
                                        "aimsmesh" );

        if ( verbose )
        {

          std::cout << "done" << std::endl;

        }

      }

    }


    ////////////////////////////////////////////////////////////////////////////
    // collecting the bounding box outlines
    ////////////////////////////////////////////////////////////////////////////

    if ( !globalBoundingBoxFileName.empty() )
    {

      if ( verbose )
      {

        std::cout << "collecting the global bouding box : " << std::flush;

      }

      gkg::Curve3dMap< float > globalBoundingBoxOutline;
      particleMotionAnalyzer->getBoundingBoxOutline( globalBoundingBox,
                                                     globalBoundingBoxOutline );
      gkg::Writer::getInstance().write( globalBoundingBoxFileName,
                                        globalBoundingBoxOutline,
                                        false,
                                        "aimsmesh" );

      if ( verbose )
      {

        std::cout << "done" << std::endl;

      }

    }

    if ( !localBoundingBoxFileName.empty() )
    {

      if ( verbose )
      {

        std::cout << "collecting the local bouding box : " << std::flush;

      }

      gkg::Curve3dMap< float > localBoundingBoxOutline;
      particleMotionAnalyzer->getBoundingBoxOutline( localBoundingBox,
                                                     localBoundingBoxOutline );
      gkg::Writer::getInstance().write( localBoundingBoxFileName,
                                        localBoundingBoxOutline,
                                        false,
                                        "aimsmesh" );

      if ( verbose )
      {

        std::cout << "done" << std::endl;

      }

    }


    ////////////////////////////////////////////////////////////////////////////
    // collecting the pdf volume and meshes
    ////////////////////////////////////////////////////////////////////////////

    // collecting mean displacement
    if ( !meanDisplacementFileName.empty() )
    {

      if ( verbose )
      {

        std::cout << "collecting the mean displacement volume : "
                  << std::flush;

      }
      {

        gkg::Volume< float > meanDisplacementVolume;
        particleMotionAnalyzer->getMeanDisplacementVolume(
                                                       meanDisplacementVolume );
        gkg::Writer::getInstance().write( meanDisplacementFileName,
                                          meanDisplacementVolume );

      }
      if ( verbose )
      {

        std::cout << "done" << std::endl;

      }

    }

    // collecting pdf volume
    if ( !pdfFileName.empty() )
    {

      if ( verbose )
      {

        std::cout << "collecting the pdf volume : "
                  << std::flush;

      }
      {

        gkg::Volume< float > pdfVolume;
        particleMotionAnalyzer->getDiffusionPdfVolume( pdfVolume );
        gkg::Writer::getInstance().write( pdfFileName, pdfVolume );

      }
      if ( verbose )
      {

        std::cout << "done" << std::endl;

      }

    }

    // collecting pdf meshes
    if ( !pdfMeshFileName.empty() )
    {

      if ( verbose )
      {

        std::cout << "collecting the pdf meshes : "
                  << std::flush;

      }
      {

        gkg::MeshMap< int32_t, float, 3U > pdfMeshes;
        particleMotionAnalyzer->getDiffusionPdfMeshes( pdfMeshFileName,
                                                       pdfMeshes );
        gkg::Writer::getInstance().write( pdfMeshFileName,
                                          pdfMeshes,
                                          false,
                                          "aimsmesh" );

      }
      if ( verbose )
      {

        std::cout << "done" << std::endl;

      }

    }

    // collecting particle distribution volume
    if ( !particleDistributionFileName.empty() )
    {

      if ( verbose )
      {

        std::cout << "collecting the particle distribution volume : "
                  << std::flush;

      }
      {

        gkg::Volume< int32_t > particleDistributionVolume;
        particleMotionAnalyzer->getParticleDistributionVolume(
                                                   particleDistributionVolume );
        gkg::Writer::getInstance().write( particleDistributionFileName,
                                          particleDistributionVolume );
      }
      if ( verbose )
      {

        std::cout << "done" << std::endl;

      }

    }

  }
  GKG_CATCH( "void gkg::DwiParticleMotionAnalyzerCommand::execute( "
             "int32_t sessionCount, "
             "const std::string& particleMapFileName, "
             "const std::vector< float >& globalBoundingBoxVector, "
             "const std::vector< int32_t >& volumeSizeVector, "
             "int32_t stepCount, "
             "const std::string& pdfOrientationSetFileName, "
             "int32_t pdfOrientationCount, "
             "float lowerLimit, "
             "float upperLimit, "
             "float apertureAngleLimit, "
             "const std::vector< float >& localBoundingBoxVector, "
             "int32_t localParticleCount, "
             "int32_t temporalSubSamplingCount, "
             "float particleSphereRadius, "
             "int32_t particleSphereVertexCount, "
             "const std::string& meanDisplacementFileName, "
             "const std::string& pdfFileName, "
             "const std::string& pdfMeshFileName, "
             "const std::string& particleDistributionFileName, "
             "const std::string& globalBoundingBoxFileName, "
             "const std::string& localBoundingBoxFileName, "
             "const std::string& particleTrajectoryFileName, "
             "const std::string& particleMeshFileName, "
             "bool verbose )" );

}


RegisterCommandCreator( 
    DwiParticleMotionAnalyzerCommand,
    DECLARE_INTEGER_PARAMETER_HELP( sessionCount ) +
    DECLARE_STRING_PARAMETER_HELP(  particleMapFileName ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( globalBoundingBoxVector ) +
    DECLARE_VECTOR_OF_INTEGERS_PARAMETER_HELP( volumeSizeVector ) +
    DECLARE_INTEGER_PARAMETER_HELP( stepCount ) +
    DECLARE_STRING_PARAMETER_HELP( pdfOrientationSetFileName ) +
    DECLARE_INTEGER_PARAMETER_HELP( pdfOrientationCount ) +
    DECLARE_FLOATING_PARAMETER_HELP( lowerLimit ) +
    DECLARE_FLOATING_PARAMETER_HELP( upperLimit ) +
    DECLARE_FLOATING_PARAMETER_HELP( apertureAngleLimit ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( localBoundingBoxVector ) +
    DECLARE_INTEGER_PARAMETER_HELP( localParticleCount ) +
    DECLARE_INTEGER_PARAMETER_HELP( temporalSubSamplingCount ) +
    DECLARE_FLOATING_PARAMETER_HELP( particleSphereRadius ) +
    DECLARE_INTEGER_PARAMETER_HELP( particleSphereVertexCount ) +
    DECLARE_STRING_PARAMETER_HELP( meanDisplacementFileName ) +
    DECLARE_STRING_PARAMETER_HELP( pdfFileName ) +
    DECLARE_STRING_PARAMETER_HELP( pdfMeshFileName ) +
    DECLARE_STRING_PARAMETER_HELP( particleDistributionFileName ) +
    DECLARE_STRING_PARAMETER_HELP( globalBoundingBoxFileName ) +
    DECLARE_STRING_PARAMETER_HELP( localBoundingBoxFileName ) +
    DECLARE_STRING_PARAMETER_HELP( particleTrajectoryFileName ) +
    DECLARE_STRING_PARAMETER_HELP( particleMeshFileName ) +
    DECLARE_BOOLEAN_PARAMETER_HELP( verbose ) );
