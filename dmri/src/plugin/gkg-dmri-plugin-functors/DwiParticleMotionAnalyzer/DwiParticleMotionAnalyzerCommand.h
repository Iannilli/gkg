#ifndef _gkg_dmri_plugin_functors_DwiParticleMotionAnalyzer_DwiParticleMotionAnalyzerCommand_h_
#define _gkg_dmri_plugin_functors_DwiParticleMotionAnalyzer_DwiParticleMotionAnalyzerCommand_h_


#include <gkg-communication-command/Command.h>
#include <gkg-core-pattern/Creator.h>

#include <string>
#include <vector>


namespace gkg
{


class DwiParticleMotionAnalyzerCommand :
                           public Command,
                           public Creator2Arg< DwiParticleMotionAnalyzerCommand,
                                               Command,
                                               int32_t,
                                               char** >,
                           public Creator1Arg< DwiParticleMotionAnalyzerCommand,
                                               Command,
                                               const Dictionary& >
{

  public:

    DwiParticleMotionAnalyzerCommand( int32_t argc,
                                      char* argv[],
                                      bool loadPlugin = false,
                                      bool removeFirst = true );
    DwiParticleMotionAnalyzerCommand( 
                          ////// simulation session count
                          int32_t sessionCount,
                          const std::string& particleMapFileName,
                          ////// parameters for the particle motion analyzer
                          const std::vector< float >& globalBoundingBoxVector,
                          const std::vector< int32_t >& volumeSizeVector,
                          int32_t stepCount,
                          ////// parameters for the diffusion functions
                          const std::string& pdfOrientationSetFileName,
                          int32_t pdfOrientationCount,
                          float lowerLimit,
                          float upperLimit,
                          float apertureAngleLimit,
                          ////// parameters for renderings
                          const std::vector< float >& localBoundingBoxVector,
                          int32_t localParticleCount,
                          int32_t temporalSubSamplingCount,
                          float particleSphereRadius,
                          int32_t particleSphereVertexCount,
                          ////// parameters for output volumes and 3D renderings
                          const std::string& meanDisplacementFileName,
                          const std::string& pdfFileName,
                          const std::string& pdfMeshFileName,
                          const std::string& particleDistributionFileName,
                          const std::string& globalBoundingBoxFileName,
                          const std::string& localBoundingBoxFileName,
                          const std::string& particleTrajectoryFileName,
                          const std::string& particleMeshFileName,
                          ////// parameter for verbosity
                          bool verbose );
    DwiParticleMotionAnalyzerCommand( const Dictionary& parameters );
    virtual ~DwiParticleMotionAnalyzerCommand();

    static std::string getStaticName();

  protected:

    friend class Creator2Arg< DwiParticleMotionAnalyzerCommand, Command, 
                              int32_t, char** >;
    friend class Creator1Arg< DwiParticleMotionAnalyzerCommand, Command,
                              const Dictionary& >;

    void parse();
    void execute( ////// simulation session count
                  int32_t sessionCount,
                  const std::string& particleMapFileName,
                  ////// parameters for the particle motion analyzer
                  const std::vector< float >& globalBoundingBoxVector,
                  const std::vector< int32_t >& volumeSizeVector,
                  int32_t stepCount,
                  ////// parameters for the diffusion functions
                  const std::string& pdfOrientationSetFileName,
                  int32_t pdfOrientationCount,
                  float lowerLimit,
                  float upperLimit,
                  float apertureAngleLimit,
                  ////// parameters for renderings
                  const std::vector< float >& localBoundingBoxVector,
                  int32_t localParticleCount,
                  int32_t temporalSubSamplingCount,
                  float particleSphereRadius,
                  int32_t particleSphereVertexCount,
                  ////// parameters for output volumes and 3D renderings
                  const std::string& meanDisplacementFileName,
                  const std::string& pdfFileName,
                  const std::string& pdfMeshFileName,
                  const std::string& particleDistributionFileName,
                  const std::string& globalBoundingBoxFileName,
                  const std::string& localBoundingBoxFileName,
                  const std::string& particleTrajectoryFileName,
                  const std::string& particleMeshFileName,
                  ////// parameter for verbosity
                  bool verbose );

};


}


#endif
