#include <gkg-dmri-plugin-functors/DwiTractography/DwiTractographyCommand.h>
#include <gkg-communication-command/CommandFactory.h>
#include <gkg-communication-getopt/Application.h>
#include <gkg-processing-container/Volume_i.h>
#include <gkg-processing-container/SiteMapFactory_i.h>
#include <gkg-processing-coordinates/CartesianVoxelSampler.h>
#include <gkg-processing-coordinates/ElectrostaticOrientationSet.h>
#include <gkg-processing-coordinates/OrientationSetCache.h>
#include <gkg-processing-algobase/EqualToFunction_i.h>
#include <gkg-processing-algobase/Math.h>
#include <gkg-processing-container/TextureMap_i.h>
#include <gkg-processing-transform/Referential.h>
#include <gkg-processing-transform/Transform3dManager.h>
#include <gkg-processing-transform/IdentityTransform3d.h>
#include <gkg-processing-transform/RigidTransform3d.h>
#include <gkg-processing-transform/AffineWoShearingTransform3d.h>
#include <gkg-processing-transform/AffineTransform3d.h>
#include <gkg-processing-transform/Scaling3d.h>
#include <gkg-dmri-odf/OrientationDistributionFunction_i.h>
#include <gkg-dmri-container/DiffusionCartesianField_i.h>
#include <gkg-dmri-container/OdfCartesianField.h>
#include <gkg-dmri-container/OdfContinuousField.h>
#include <gkg-dmri-container/OdfCartesianToContinuousField.h>
#include <gkg-dmri-container/BundleMap_i.h>
#include <gkg-dmri-tractography/TractographyAlgorithm_i.h>
#include <gkg-dmri-tractography/TractographyAlgorithmFactory.h>
#include <gkg-dmri-sh-basis/SymmetricalSphericalHarmonicsCache.h>
#include <gkg-core-io/TypeOf.h>
#include <gkg-core-io/StringConverter.h>
#include <gkg-core-io/Reader_i.h>
#include <gkg-core-io/Writer_i.h>
#include <gkg-core-io/Eraser.h>
#include <gkg-communication-sysinfo/ConfigurationInfo.h>
#include <gkg-core-object/Dictionary.h>
#include <gkg-core-object/GenericObject_i.h>
#include <gkg-core-exception/Exception.h>

#include <iostream>
#include <fstream>
#include <list>


#define ODFS_VOXEL_REFERENTIAL_NAME  "odfs_voxel_referential"
#define ODFS_REAL_REFERENTIAL_NAME   "odfs_real_referential"

#define ROIS_VOXEL_REFERENTIAL_NAME  "rois_voxel_referential"
#define ROIS_REAL_REFERENTIAL_NAME   "rois_real_referential"

#define MASK_VOXEL_REFERENTIAL_NAME  "mask_voxel_referential"
#define MASK_REAL_REFERENTIAL_NAME   "mask_real_referential"


template < class L > inline
int32_t getGlobalSiteCount( const gkg::SiteMap< L, int32_t >& rois )
{

  try
  {

    // processing the global site count
    int32_t siteCount = 0;
    typename gkg::SiteMap< L, int32_t >::const_iterator
      l = rois.begin(),
      le = rois.end();
    while ( l != le )
    {

      siteCount += ( int32_t )l->second.size();
      ++ l;

    }
    return siteCount;

  }
  GKG_CATCH( "template < class L > "
             "int32_t getGlobalSiteCount( "
             "const gkg::SiteMap< L, int32_t >& rois )" );

}


static int32_t getSiteCountForStep( int32_t step,
                                    int32_t stepCount,
                                    int32_t globalSiteCount )
{

  try
  {

    int32_t siteCountPerStep = globalSiteCount / stepCount;
    if ( stepCount * siteCountPerStep != globalSiteCount )
    {

      ++ siteCountPerStep;

    }
    int32_t siteCountForStep = 0;
    if ( step != stepCount )
    {

      siteCountForStep = siteCountPerStep;

    }
    else
    {

      siteCountForStep = globalSiteCount - ( stepCount - 1 ) * siteCountPerStep;

    }
    return siteCountForStep;

  }
  GKG_CATCH( "int32_t getSiteCountForStep( "
             "int32_t step, "
             "int32_t stepCount, "
             "int32_t globalSiteCount )" );

}


template < class L > inline
gkg::SiteMap< L, int32_t >
getSubRegions( const gkg::SiteMap< L, int32_t >& rois,
	       int32_t step,
               int32_t stepCount )
{

  try
  {

    // speeding-up if one step is used
    if ( stepCount == 1 )
    {

      return rois;

    }

    // get site count already processed for previous step(s)
    int32_t siteCountForPreviousSteps = 0;
    int32_t globalSiteCount = getGlobalSiteCount( rois );
    int32_t s = 1;
    while ( s < step )
    {

      siteCountForPreviousSteps += getSiteCountForStep( s,
                                                        stepCount,
                                                        globalSiteCount );
      ++ s;

    }

    // get required site count for the current step
    int32_t siteCountForCurrentStep = getSiteCountForStep( step,
                                                           stepCount,
                                                           globalSiteCount );

    // allocating the sub-regions
    gkg::SiteMap< L, int32_t > subRois;

    // processing the global site count
    int32_t siteCount = 0;
    int32_t siteCountAccumulator = 0;
    typename gkg::SiteMap< L, int32_t >::const_iterator
      l = rois.begin(),
      le = rois.end();
    while ( ( l != le ) && ( siteCount < siteCountForCurrentStep ) )
    {

      int32_t siteCountForRank = ( int32_t )l->second.size();
      if ( ( siteCountAccumulator + siteCountForRank ) <
           siteCountForPreviousSteps )
      {

        siteCountAccumulator += siteCountForRank;

      }
      else
      {

        const L& label = l->first;
        std::list< gkg::Vector3d< int32_t > > sites;
        std::list< gkg::Vector3d< int32_t > >::const_iterator
          s = l->second.begin(),
          se = l->second.end();
        while ( ( s != se ) &&  ( siteCount < siteCountForCurrentStep ) )
        {

          if ( siteCountAccumulator < siteCountForPreviousSteps )
          {

            ++ siteCountAccumulator;

          }
          else
          {

            sites.push_back( *s );
            ++ siteCount;

          }
          ++ s;

        }
        subRois.addSites( label, sites );

      }
      ++ l;

    }
    return subRois;

  }
  GKG_CATCH( "template < class L > "
             "gkg::SiteMap< L, int32_t > "
             "getSubRegions( "
             "const gkg::SiteMap< L, int32_t >& rois, "
             "int32_t step, "
             "int32_t stepCount )" );

}


static std::string getFileNameBundleMapForStep( const std::string& name,
                                                int32_t step,
                                                int32_t stepCount )
{

  try
  {

    // speeding-up if one step is used
    if ( stepCount == 1 )
    {

      return name;

    }

    // getting header extension
    std::string headerExtensionName = 
      gkg::ConfigurationInfo::getInstance().getHeaderExtensionName();

    // building extension set
    std::set< std::string > extensions;
    extensions.insert( ".bundlemap" );
    extensions.insert( std::string( ".bundlemap" ) + headerExtensionName );
    extensions.insert( ".bundlesdata" );
    extensions.insert( ".bundles" );

    // processing name output name
    std::string suffix = '_' + gkg::StringConverter::toString( step ) +
                         '_' + gkg::StringConverter::toString( stepCount );
    std::string fileName = name + suffix;
    std::set< std::string >::const_iterator e = extensions.begin(),
                                            ee = extensions.end();
    while ( e != ee )
    {

      if ( name.length() > e->length() )
      {

        if ( name.substr( name.length() - e->length(), e->length()) == *e )
        {

          fileName = name.substr( 0, name.length() - e->length() ) + 
                     suffix + *e;
          break;

        }

      }
      ++ e;

    }
    return fileName;

  }
  GKG_CATCH( "std::string getFileNameBundleMapForStep( "
             "const std::string& name, "
             "int32_t step, "
             "int32_t stepCount )" );

}


//
// class DwiTractographyCommand
//

gkg::DwiTractographyCommand::DwiTractographyCommand( int32_t argc,
                                                     char* argv[],
                                                     bool loadPlugin,
                                                     bool removeFirst )
                            : gkg::Command( argc, argv, loadPlugin, 
                                            removeFirst )
{

  try
  {

    parse();

  }
  GKG_CATCH( "gkg::DwiTractographyCommand::DwiTractographyCommand( "
             "int32_t argc, char* argv[], bool loadPlugin, "
             "bool removeFirst )" );

}


gkg::DwiTractographyCommand::DwiTractographyCommand(
                    const std::string& fileNameSiteMap,
                    const std::string& fileNameOdfs,
                    const std::string& fileNameRois,
                    const std::string& fileNameMask,
                    const std::string& fileNameRoisToOdfsTransform3d,
                    const std::string& fileNameMaskToOdfsTransform3d,
                    const std::string& fileNameBundleMap,
                    const std::string& algorithmType,
                    const std::vector< double >& algorithmScalarParameters,
                    const std::vector< std::string >& algorithmStringParameters,
                    int32_t outputOrientationCount,
                    const std::string& volumeFormat,
                    const std::string& bundleMapFormat,
                    int32_t stepCount,
                    bool ascii,
                    bool verbose )
                            : gkg::Command()
{

  try
  {

    execute( fileNameSiteMap,
             fileNameOdfs,
             fileNameRois,
             fileNameMask,
             fileNameRoisToOdfsTransform3d,
             fileNameMaskToOdfsTransform3d,
             fileNameBundleMap,
             algorithmType,
             algorithmScalarParameters,
             algorithmStringParameters,
             outputOrientationCount,
             volumeFormat,
             bundleMapFormat,
             stepCount,
             ascii,
             verbose );

  }
  GKG_CATCH( "gkg::DwiTractographyCommand::DwiTractographyCommand( "
             "const std::string& fileNameSiteMap, "
             "const std::string& fileNameOdfs, "
             "const std::string& fileNameRois, "
             "const std::string& fileNameMask, "
             "const std::string& fileNameRoisToOdfsTransform3d, "
             "const std::string& fileNameMaskToOdfsTransform3d, "
             "const std::string& fileNameBundleMap, "
             "const std::string& algorithmType, "
             "const std::vector< double >& algorithmScalarParameters, "
             "const std::vector< std::string >& algorithmStringParameters, "
             "int32_t outputOrientationCount, "
             "const std::string& volumeFormat, "
             "const std::string& bundleMapFormat, "
             "int32_t stepCount, "
             "bool ascii, "
             "bool verbose  )" );

}


gkg::DwiTractographyCommand::DwiTractographyCommand(
                                             const gkg::Dictionary& parameters )
                                 : gkg::Command( parameters )
{

  try
  {

    DECLARE_STRING_PARAMETER( parameters, std::string, fileNameSiteMap );
    DECLARE_STRING_PARAMETER( parameters, std::string, fileNameOdfs );
    DECLARE_STRING_PARAMETER( parameters, std::string, fileNameRois );
    DECLARE_STRING_PARAMETER( parameters, std::string, fileNameMask );
    DECLARE_STRING_PARAMETER( parameters, std::string,
                              fileNameRoisToOdfsTransform3d );
    DECLARE_STRING_PARAMETER( parameters, std::string,
                              fileNameMaskToOdfsTransform3d );
    DECLARE_STRING_PARAMETER( parameters, std::string, fileNameBundleMap );
    DECLARE_STRING_PARAMETER( parameters, std::string, algorithmType );

    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER( parameters, std::vector< double >,
                                           algorithmScalarParameters );
    DECLARE_VECTOR_OF_STRINGS_PARAMETER( parameters, std::vector< std::string >,
                                         algorithmStringParameters );
    DECLARE_INTEGER_PARAMETER( parameters, int32_t, outputOrientationCount );
    DECLARE_STRING_PARAMETER( parameters, std::string, volumeFormat );
    DECLARE_STRING_PARAMETER( parameters, std::string, bundleMapFormat );
    DECLARE_INTEGER_PARAMETER( parameters, int32_t, stepCount );
    DECLARE_BOOLEAN_PARAMETER( parameters, bool, ascii );
    DECLARE_BOOLEAN_PARAMETER( parameters, bool, verbose );
    
    execute( fileNameSiteMap,
             fileNameOdfs,
             fileNameRois,
             fileNameMask,
             fileNameRoisToOdfsTransform3d,
             fileNameMaskToOdfsTransform3d,
             fileNameBundleMap,
             algorithmType,
             algorithmScalarParameters,
             algorithmStringParameters,
             outputOrientationCount,
             volumeFormat,
             bundleMapFormat,
             stepCount,
             ascii,
             verbose );

  }
  GKG_CATCH( "gkg::DwiTractographyCommand::DwiTractographyCommand( "
             "const gkg::Dictionary& parameters )" );

}


gkg::DwiTractographyCommand::~DwiTractographyCommand()
{
}


std::string gkg::DwiTractographyCommand::getStaticName()
{

  try
  {

    return "DwiTractography";

  }
  GKG_CATCH( "std::string gkg::DwiTractographyCommand::getStaticName()" );

}


void gkg::DwiTractographyCommand::parse()
{

  try
  {

    std::string fileNameSiteMap;
    std::string fileNameOdfs;

    std::string fileNameRois;
    std::string fileNameMask = "";
    std::string fileNameRoisToOdfsTransform3d = "";
    std::string fileNameMaskToOdfsTransform3d = "";

    std::string fileNameBundleMap;

    std::string algorithmType = "streamline-deterministic";

    std::vector< double > algorithmScalarParameters;
    std::vector< std::string > algorithmStringParameters;

    int32_t outputOrientationCount = 0;

    std::string volumeFormat = "gis";
    std::string bundleMapFormat = "bundlemap";
    int32_t stepCount = 1;
    bool ascii = false;
    bool verbose = false;

    ////////////////////////////////////////////////////////////////////////////
    // managing command lines argument(s)
    ////////////////////////////////////////////////////////////////////////////

    gkg::Application application( _argc, _argv, "DWI tractography tool",
                                  _loadPlugin );
    application.addSingleOption( "-s",
                                 "Input site map",
                                 fileNameSiteMap );
    application.addSingleOption( "-o",
                                 "Input odf texture map file name",
                                 fileNameOdfs );
    application.addSingleOption( "-r",
                                 "Input volume/site map ROI file name",
                                 fileNameRois );
    application.addSingleOption( "-m",
                                 "Input mask file name",
                                 fileNameMask );
    application.addSingleOption( "-tr",
                                 "3D rigid/affine wo shearing/affine transform"
                                 " from ROIs to ODF "
                                 "referential (default=identity)",
                                 fileNameRoisToOdfsTransform3d,
                                 true );
    application.addSingleOption( "-tm",
                                 "3D rigid/affine wo shearing/affine transform"
                                 " from mask to ODF "
                                 "referential (default=identity)",
                                 fileNameMaskToOdfsTransform3d,
                                 true );
    application.addSingleOption( "-b",
                                 "Output bundle map file name",
                                 fileNameBundleMap );

    application.addSingleOption( "-type",
                                 "Tractography algorithm type "
                                 "(default=streamline-deterministic): \n"
                                 "- streamline-deterministic\n"
                                 "- streamline-regularized-deterministic\n"
                                 "- streamline-probabilistic\n"
                                 "- global-spin-glass\n",
                                 algorithmType,
                                 true );

    application.addSeriesOption( "-scalarParameters",
                                 "Tractography algorithm scalar parameters as a "
                                 "vector of double <P1> <P2> <P3> ... <Pn>:\n\n"
                                 "- in case of streamline-deterministic,\n"
                                 "   <P1>: voxel sampler point count\n"
                                 "   <P2>: forward step during fiber tracking\n"
                                 "   <P3>: storing increment, ie do not store"
                                         " each fiber point but only one out"
                                         " of the provided argument\n"
                                 "   <P4>: minimum fiber length in mm\n"
                                 "   <P5>: maximum fiber length in mm\n"
                                 "   <P6>: aperture angle,ie half cone inside"
                                         " which to look for neighbors given"
                                         " degrees\n"
                                 "   (default= 1 / 0.25 x minimum voxel "
                                 "resolution / 10 / 5 / 200 / 90)\n\n"
                                 "- in case of "
                                 "streamline-regularized-deterministic,\n"
                                 "   <P1>: voxel sampler point count\n"
                                 "   <P2>: forward step during fiber tracking\n"
                                 "   <P3>: storing increment, ie do not store"
                                         " each fiber point but only one out"
                                         " of the provided argument\n"
                                 "   <P4>: minimum fiber length in mm\n"
                                 "   <P5>: maximum fiber length in mm\n"
                                 "   <P6>: aperture angle,ie half cone inside"
                                         " which to look for neighbors given"
                                         " degrees\n"
                                 "   <P7>: imposed lower GFA boundary\n"
                                        " (-1 = automatically processed)\n"
                                 "   <P8>: imposed upper GFA boundary\n"
                                        " (-1 = automatically processed)\n"
                                 "   (default= 1 / 0.25 x minimum voxel "
                                 "resolution / 10 / 5 / 200 / 90 / -1 / -1)\n\n"
                                 "- in case of streamline-probabilistic,\n"
                                 "   <P1>: voxel sampler point count\n"
                                 "   <P2>: forward step during fiber tracking\n"
                                 "   <P3>: storing increment, ie do not store"
                                         " each fiber point but only one out"
                                         " of the provided argument\n"
                                 "   <P4>: minimum fiber length in mm\n"
                                 "   <P5>: maximum fiber length in mm\n"
                                 "   <P6>: aperture angle,ie half cone inside"
                                         " which to look for neighbors given"
                                         " degrees\n"
                                 "   <P7>: temperature of the Gibb's sampler\n"
                                 "   <P8>: imposed lower GFA boundary"
                                        " (-1 = automatically processed)\n"
                                 "   <P9>: imposed upper GFA boundary"
                                        " (-1 = automatically processed)\n"
                                 "   (default= 1 / 0.25 x minimum voxel "
                                 "resolution / 10 / 5 / 200 / 90 / 1 / -1 / "
                                 "-1)\n\n"
                                 "- in case of global-spin-glass,\n"
                                 "   <P1>: voxel sampler point count\n"
                                 "   <P2>: minimum fiber length in mm\n"
                                 "   <P3>: maximum fiber length in mm\n"
                                 "   <P4>: iteration count\n"
                                 "   <P5>: Gibb's sampler temperature\n"
                                 "   <P6>: initial external temperature\n"
                                 "   <P7>: initial internal temperature\n"
                                 "   <P8>: final internal temperature\n"
                                 "   <P9>: external energy weight\n"
                                 "   <P10>: internal energy weight\n"
                                 "   <P11>: neighbor count for the clique"
                                 " used in the optimal  motion proposal\n"
                                 "   <P12>: orientation dispersion scaling "
                                 "(>0 in general and used to set the fixed "
                                 "value in case no orientation dispersion "
                                 "volume is given )\n"
                                 "   <P13>: spin glass density "
                                 "(-1 -> used string parameter P3 instead)\n"
                                 "   <P14>: connection likelihood\n"
                                 "   <P15>: curvature threshold\n"
                                 "   <P16>: maximum distance to mesh"
                                 " to be considered for sharp turn guidance\n"
                                 "   <P17>: target cache stride of scene"
                                 " modeler\n"
                                 "   (default= 1 / 5 / 200 / 10000000 / 0.01 / "
                                 "0.001 / 0.1 / 0.001 / 20 / 200 / "
                                 "5 / 1.0 / 1.0 / 0.5 / 10deg / "
                                 "1.0 / 1 )\n",
                                 algorithmScalarParameters );
    application.addSeriesOption( "-stringParameters",
                                 "Tractography algorithm string parameters as a"
                                 " vector of string <P1> <P2> <P3> ...<Pn>:\n\n"
                                 "- in case of streamline-deterministic,\n"
                                 "   N/A\n\n"
                                 "- in case of "
                                 "streamline-regularized-deterministic,\n"
                                 "   N/A\n\n"
                                 "- in case of streamline-probabilistic,\n"
                                 "   N/A\n\n"
                                 "- in case of global-spin-glass,\n"
                                 "   <P1>: pial surface filename "
                                 "(default= none)\n"
                                 "   <P2>: pial surface to ROI 3D transform "
                                 "filename (default= id)\n"
                                 "   <P3>: orientation dispersion volume "
                                 "filename (default= none)\n"
                                 "   <P4>: orientation dispersion volume to ROI"
                                 " 3D transform filename (default= id)\n"
                                 "   <P5>: spin glass density volume filename "
                                 "(default= none)\n"
                                 "   <P6>: spin glass density volume to ROI 3D "
                                 "transform filename (default= id)\n"
                                 "   <P7>: connection likelihood volume "
                                 "filename (default= none)\n"
                                 "   <P8>: connection likelihood volume to ROI"
                                 "3D transform filename (default= id)\n"
                                 "   <P9>: curvature threshold volume "
                                 "filename (default= none)\n"
                                 "   <P10>: curvature thresholed volume to ROI "
                                 "3D transform filename (default= id)\n"
                                 "   <P11>: distance to ventricles map volume "
                                 "filename (default= none)\n"
                                 "   <P12>: distance to ventricles map volume "
                                 "to ROI 3D transform filename (default= id)\n",
                                 algorithmStringParameters );

    application.addSingleOption( "-outputOrientationCount",
                                 "Ouput orientation count (default=0)",
                                 outputOrientationCount,
                                 true );

    application.addSingleOption( "-volumeFormat",
                                 "Ouput volume format (default=GIS)",
                                 volumeFormat,
                                 true );
    application.addSingleOption( "-bundleMapFormat",
                                 "Ouput BundleMap format (default=bundlemap)",
                                 bundleMapFormat,
                                 true );
    application.addSingleOption( "-stepCount",
                                 "Divide the tractography into several steps "
				 "to prevent memory runout (default=1)",
                                 stepCount,
                                 true );
    application.addSingleOption( "-ascii",
                                 "Save ouput volume in ASCII mode",
                                 ascii,
                                 true );
    application.addSingleOption( "-verbose",
                                 "Show as much information as possible",
                                 verbose, 
                                 true );
    application.initialize();

    execute( fileNameSiteMap,
             fileNameOdfs,
             fileNameRois,
             fileNameMask,
             fileNameRoisToOdfsTransform3d,
             fileNameMaskToOdfsTransform3d,
             fileNameBundleMap,
             algorithmType,
             algorithmScalarParameters,
             algorithmStringParameters,
             outputOrientationCount,
             volumeFormat,
             bundleMapFormat,
             stepCount,
             ascii,
             verbose );

  }
  GKG_CATCH_FUNCTOR( "void gkg::DwiTractographyCommand::parse()" );

}


void gkg::DwiTractographyCommand::execute(
                    const std::string& fileNameSiteMap,
                    const std::string& fileNameOdfs,
                    const std::string& fileNameRois,
                    const std::string& fileNameMask,
                    const std::string& fileNameRoisToOdfsTransform3d,
                    const std::string& fileNameMaskToOdfsTransform3d,
                    const std::string& fileNameBundleMap,
                    const std::string& algorithmType,
                    const std::vector< double >& algorithmScalarParameters,
                    const std::vector< std::string >& algorithmStringParameters,
                    int32_t outputOrientationCount,
                    const std::string& /* volumeFormat */,
                    const std::string& bundleMapFormat,
                    int32_t stepCount,
                    bool ascii,
                    bool verbose )
 {

  try
  {

    ////////////////////////////////////////////////////////////////////////////
    // sanity checks
    ////////////////////////////////////////////////////////////////////////////

    if ( ( algorithmType != "streamline-deterministic" ) &&
         ( algorithmType != "streamline-regularized-deterministic" ) &&
         ( algorithmType != "streamline-probabilistic" ) &&
         ( algorithmType != "global-spin-glass" ) )
    {

      throw std::runtime_error( "bad tractography algorithm type" );

    }
    if ( outputOrientationCount < 0 )
    {

      throw std::runtime_error( "output orientation count must be positive" );

    }
    if ( stepCount < 1 )
    {

      throw std::runtime_error( "step count must be strictly positive" );

    }
    if ( algorithmType == "global-spin-glass" )
    {

      if ( stepCount != 1 )
      {

        throw std::runtime_error( "cannot divide the optimization process "
                                  "into several steps when using "
                                  "'global-spin-glass' tractography and" 
                                  "step count must be equal to 1" );

      }

    }


    ////////////////////////////////////////////////////////////////////////////
    // algorithm parameter(s) check or initialization
    ////////////////////////////////////////////////////////////////////////////

    std::vector< double >
      theAlgorithmScalarParameters = algorithmScalarParameters;
    std::vector< std::string >
      theAlgorithmStringParameters = algorithmStringParameters;
    if ( algorithmType == "streamline-deterministic" )
    {

      if ( !theAlgorithmScalarParameters.empty() )
      {

        if ( theAlgorithmScalarParameters.size() != 6U )
        {

          throw std::runtime_error( "there must be 6 scalar parameters" );

        }

        // voxel sampler point count
        if ( theAlgorithmScalarParameters[ 0 ] < 1.0 )
        {

          throw std::runtime_error(
                               "voxel sampler point count must be at least 1" );

        }
        // forward step
        if ( theAlgorithmScalarParameters[ 1 ] < 0.0 )
        {

          throw std::runtime_error( "forward step must be positive or null" );

        }
        // storing increment
        if ( theAlgorithmScalarParameters[ 2 ] < 1 )
        {

          throw std::runtime_error( "storing increment must be at least 1" );

        }
        // minimum fiber length
        if ( theAlgorithmScalarParameters[ 3 ] <= 0.0 )
        {

          throw std::runtime_error(
                             "minimum fiber length must be at least positive" );

        }
        // maximum fiber length
        if ( theAlgorithmScalarParameters[ 4 ] <
             theAlgorithmScalarParameters[ 3 ] )
        {

          throw std::runtime_error(
                             "storing increment must be at least greater than "
                             "the minimum fiber length" );

        }
        // aperture angle
        if ( theAlgorithmScalarParameters[ 5 ] <= 0.0 )
        {

          throw std::runtime_error(
                                  "aperture angle must be strictly positive" );

        }
        // converting from degree to radian
        theAlgorithmScalarParameters[ 5 ] *= M_PI / 180.0;

      }
      else if ( theAlgorithmScalarParameters.empty() )
      {

        theAlgorithmScalarParameters.resize( 6 );
        theAlgorithmScalarParameters[ 0 ] = 1;
        theAlgorithmScalarParameters[ 1 ] = 0;     // initialized after
        theAlgorithmScalarParameters[ 2 ] = 10;
        theAlgorithmScalarParameters[ 3 ] = 5.0;
        theAlgorithmScalarParameters[ 4 ] = 200.0;
        theAlgorithmScalarParameters[ 5 ] = M_PI / 2;

      }

    }
    else if ( algorithmType == "streamline-regularized-deterministic" )
    {

      if ( !theAlgorithmScalarParameters.empty() )
      {

        if ( theAlgorithmScalarParameters.size() != 8U )
        {

          throw std::runtime_error( "there must be 8 scalar parameters" );

        }

        // voxel sampler point count
        if ( theAlgorithmScalarParameters[ 0 ] < 1.0 )
        {

          throw std::runtime_error(
                               "voxel sampler point count must be at least 1" );

        }
        // forward step
        if ( theAlgorithmScalarParameters[ 1 ] < 0.0 )
        {

          throw std::runtime_error( "forward step must be positive or null" );

        }
        // storing increment
        if ( theAlgorithmScalarParameters[ 2 ] < 1 )
        {

          throw std::runtime_error( "storing increment must be at least 1" );

        }
        // minimum fiber length
        if ( theAlgorithmScalarParameters[ 3 ] <= 0.0 )
        {

          throw std::runtime_error(
                             "minimum fiber length must be at least positive" );

        }
        // maximum fiber length
        if ( theAlgorithmScalarParameters[ 4 ] <
             theAlgorithmScalarParameters[ 3 ] )
        {

          throw std::runtime_error(
                             "storing increment must be at least greater than "
                             "the minimum fiber length" );

        }
        // aperture angle
        if ( theAlgorithmScalarParameters[ 5 ] <= 0.0 )
        {

          throw std::runtime_error(
                                  "aperture angle must be strictly positive" );

        }
        // converting from degree to radian
        theAlgorithmScalarParameters[ 5 ] *= M_PI / 180.0;

      }
      else if ( theAlgorithmScalarParameters.empty() )
      {

        theAlgorithmScalarParameters.resize( 8 );
        theAlgorithmScalarParameters[ 0 ] = 1;
        theAlgorithmScalarParameters[ 1 ] = 0;     // initialized after
        theAlgorithmScalarParameters[ 2 ] = 10;
        theAlgorithmScalarParameters[ 3 ] = 5.0;
        theAlgorithmScalarParameters[ 4 ] = 200.0;
        theAlgorithmScalarParameters[ 5 ] = M_PI / 2;
        theAlgorithmScalarParameters[ 6 ] = -1.0;
        theAlgorithmScalarParameters[ 7 ] = -1.0;

      }

    }
    else if ( algorithmType == "streamline-probabilistic" )
    {

      if ( !theAlgorithmScalarParameters.empty() )
      {

        if ( theAlgorithmScalarParameters.size() != 9U )
        {

          throw std::runtime_error( "there must be 9 scalar parameters" );

        }

        // voxel sampler point count
        if ( theAlgorithmScalarParameters[ 0 ] < 1.0 )
        {

          throw std::runtime_error(
                               "voxel sampler point count must be at least 1" );

        }
        // forward step
        if ( theAlgorithmScalarParameters[ 1 ] < 0.0 )
        {

          throw std::runtime_error( "forward step must be positive or null" );

        }
        // storing increment
        if ( theAlgorithmScalarParameters[ 2 ] < 1 )
        {

          throw std::runtime_error( "storing increment must be at least 1" );

        }
        // minimum fiber length
        if ( theAlgorithmScalarParameters[ 3 ] <= 0.0 )
        {

          throw std::runtime_error(
                             "minimum fiber length must be at least positive" );

        }
        // maximum fiber length
        if ( theAlgorithmScalarParameters[ 4 ] <
             theAlgorithmScalarParameters[ 3 ] )
        {

          throw std::runtime_error(
                             "storing increment must be at least greater than "
                             "the minimum fiber length" );

        }
        // aperture angle
        if ( theAlgorithmScalarParameters[ 5 ] <= 0.0 )
        {

          throw std::runtime_error(
                                  "aperture angle must be strictly positive" );

        }
        // Gibb's temperature
        if ( theAlgorithmScalarParameters[ 6 ] <= 0.0 )
        {

          throw std::runtime_error(
                               "Gibb's temperature must be strictly positive" );

        }
        // converting from degree to radian
        theAlgorithmScalarParameters[ 5 ] *= M_PI / 180.0;

      }
      else if ( theAlgorithmScalarParameters.empty() )
      {

        theAlgorithmScalarParameters.resize( 9 );
        theAlgorithmScalarParameters[ 0 ] = 1;
        theAlgorithmScalarParameters[ 1 ] = 0;     // initialized after
        theAlgorithmScalarParameters[ 2 ] = 10;
        theAlgorithmScalarParameters[ 3 ] = 5.0;
        theAlgorithmScalarParameters[ 4 ] = 200.0;
        theAlgorithmScalarParameters[ 5 ] = M_PI / 2;
        theAlgorithmScalarParameters[ 6 ] = 1;
        theAlgorithmScalarParameters[ 7 ] = -1.0;
        theAlgorithmScalarParameters[ 8 ] = -1.0;

      }

    }
    else if ( algorithmType == "global-spin-glass" )
    {

      if ( !theAlgorithmScalarParameters.empty() )
      {

        if ( theAlgorithmScalarParameters.size() != 17U )
        {

          throw std::runtime_error( "there must be 17 scalar parameters" );

        }

        // voxel sampler point count
        if ( theAlgorithmScalarParameters[ 0 ] < 1.0 )
        {

          throw std::runtime_error(
                               "voxel sampler point count must be at least 1" );

        }
        // minimum fiber length
        if ( theAlgorithmScalarParameters[ 1 ] <= 0.0 )
        {

          throw std::runtime_error(
                             "minimum fiber length must be strictly positive" );

        }
        // maximum fiber length
        if ( theAlgorithmScalarParameters[ 2 ] <
             theAlgorithmScalarParameters[ 1 ] )
        {

          throw std::runtime_error( "the maximum fiber length must be greater "
                                    "than the minimum fiber length" );

        }
        // iteration count
        if ( theAlgorithmScalarParameters[ 3 ] <= 0.0 )
        {

          throw std::runtime_error(
                                  "iteration count must be strictly positive" );

        }
        // Gibb's temperature for the spin glass creation orientation
        // computation
        if ( theAlgorithmScalarParameters[ 4 ] <= 0.0 )
        {

          throw std::runtime_error(
                                "Gibbs temperature must be strictly positive" );

        }
        // initial external temperature
        if ( theAlgorithmScalarParameters[ 5 ] <= 0.0 )
        {

          throw std::runtime_error(
                     "initial external temperature must be strictly positive" );

        }
        // initial internal temperature
        if ( theAlgorithmScalarParameters[ 6 ] <= 0.0 )
        {

          throw std::runtime_error(
                     "initial internal temperature must be strictly positive" );

        }
        // final internal temperature
        if ( theAlgorithmScalarParameters[ 7 ] >
             theAlgorithmScalarParameters[ 6 ] )
        {

          throw std::runtime_error( "final internal temperature must be lower "
                                    "than the initial one" );

        }
        // external energy weight
        if ( theAlgorithmScalarParameters[ 8 ] < 0.0 )
        {

          throw std::runtime_error( "external energy weight must be positive" );

        }
        // internal energy weight
        if ( theAlgorithmScalarParameters[ 9 ] < 0.0 )
        {

          throw std::runtime_error( "internal energy weight must be positive" );

        }
                                     
        // neighbor count used for the clique size in the optimal motion 
        // proposal
        if ( theAlgorithmScalarParameters[ 10 ] < 0.0 )
        {

          throw std::runtime_error( 
                   "neighbor count used for the clique size in the "
                   "optimal motion proposal must be strictly positive or nul" );

        }

        // orientation dispersion scaling
        if ( theAlgorithmScalarParameters[ 11 ] < 0.0 )
        {

          throw std::runtime_error( 
                  "orientation dispersion scaling must be strictly positive " );

        }

        // spin glass density
        if ( theAlgorithmScalarParameters[ 12 ] < 0.0 )
        {

          throw std::runtime_error( 
                                 "spin glass density must be positive or nul" );

        }

        // connection likelihood
        if ( theAlgorithmScalarParameters[ 13 ] <= 0.0 )
        {

          throw std::runtime_error( "connection likelihood must be positive or "
                                    "equal to 0.0 if the volume is given" );

        }

        // curvature threshold
        if ( theAlgorithmScalarParameters[ 14 ] <= 0.0 &&
             theAlgorithmScalarParameters[ 14 ] >= 360.0 )
        {

          throw std::runtime_error( "curvature threshold must be positive or "
                                    "equal to 0.0 if the volume is given" );

        }

        // maximum distance to mesh to be considered for sharp turn guidance
        if ( theAlgorithmScalarParameters[ 15 ] < 0.0 )
        {

          throw std::runtime_error(
                     "maximum distance to mesh to be considered for sharp turn "
                     "guidance must be positive" );

        }

        // target cache stride
        if ( theAlgorithmScalarParameters[ 16 ] < 1.0 )
        {

          throw std::runtime_error(
                          "target cache stride must be greater or equal to 1" );

        }

      }
      else if ( theAlgorithmScalarParameters.empty() )
      {

        theAlgorithmScalarParameters.resize( 17 );
        theAlgorithmScalarParameters[ 0 ] = 1;
        theAlgorithmScalarParameters[ 1 ] = 5.0;
        theAlgorithmScalarParameters[ 2 ] = 200.0;
        theAlgorithmScalarParameters[ 3 ] = 10000000;
        theAlgorithmScalarParameters[ 4 ] = 0.01;
        theAlgorithmScalarParameters[ 5 ] = 0.001;
        theAlgorithmScalarParameters[ 6 ] = 0.1;
        theAlgorithmScalarParameters[ 7 ] = 0.001;
        theAlgorithmScalarParameters[ 8 ] = 200;
        theAlgorithmScalarParameters[ 9 ] = 20;
        theAlgorithmScalarParameters[ 10 ] = 5.0;
        theAlgorithmScalarParameters[ 11 ] = 1.0;
        theAlgorithmScalarParameters[ 12 ] = 1.0;
        theAlgorithmScalarParameters[ 13 ] = 0.5;
        theAlgorithmScalarParameters[ 14 ] = 20.0;
        theAlgorithmScalarParameters[ 15 ] = 1.0;
        theAlgorithmScalarParameters[ 16 ] = 1.0;

      }
      else
      {

        throw std::runtime_error( "bad algorithm parameter count" );

      }

      if ( !theAlgorithmStringParameters.empty() )
      {

        if ( theAlgorithmStringParameters.size() != 12U )
        {

          throw std::runtime_error( "there must be 12 string parameters" );

        }

      }
      else
      {

        theAlgorithmStringParameters.resize( 12 );
        theAlgorithmStringParameters[ 0 ] = "none";
        theAlgorithmStringParameters[ 1 ] = "id";
        theAlgorithmStringParameters[ 2 ] = "none";
        theAlgorithmStringParameters[ 3 ] = "id";
        theAlgorithmStringParameters[ 4 ] = "none";
        theAlgorithmStringParameters[ 5 ] = "id";
        theAlgorithmStringParameters[ 6 ] = "none";
        theAlgorithmStringParameters[ 7 ] = "id";
        theAlgorithmStringParameters[ 8 ] = "none";
        theAlgorithmStringParameters[ 9 ] = "id";
        theAlgorithmStringParameters[ 10 ] = "none";
        theAlgorithmStringParameters[ 11 ] = "id";

      }

    }


    ////////////////////////////////////////////////////////////////////////////
    // reading site(s)
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

     std::cout << "reading '" << fileNameSiteMap << "' : " << std::flush;

    }
    gkg::SiteMap< int32_t, int32_t > siteMap;
    gkg::Reader::getInstance().read( fileNameSiteMap, siteMap );
    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }


    ////////////////////////////////////////////////////////////////////////////
    // reading ODF texture map
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

     std::cout << "reading '" << fileNameOdfs << "' : " << std::flush;

    }
    gkg::TextureMap< gkg::OrientationDistributionFunction > odfs;
    gkg::Reader::getInstance().read( fileNameOdfs, odfs );
    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }

    ////////////////////////////////////////////////////////////////////////////
    // reading ROIs
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

     std::cout << "reading '" << fileNameRois << "' : " << std::flush;

    }
    gkg::SiteMap< std::string, int32_t > roisString;
    gkg::SiteMap< int16_t, int32_t > roisInt16;
    gkg::Vector3d< double > roisResolution( 1.0, 1.0, 1.0 );
    try
    {

      // first case where the file format on disk fit with a site map
      // labelled with strings
      gkg::Reader::getInstance().read( fileNameRois, roisString );
      if ( roisString.getHeader().hasAttribute( "resolutionX" ) )
      {

        roisString.getHeader().getAttribute( "resolutionX",
                                             roisResolution.x );

      }
      if ( roisString.getHeader().hasAttribute( "resolutionY" ) )
      {

        roisString.getHeader().getAttribute( "resolutionY",
                                             roisResolution.y );

      }
      if ( roisString.getHeader().hasAttribute( "resolutionZ" ) )
      {

        roisString.getHeader().getAttribute( "resolutionZ",
                                             roisResolution.z );

      }

    }
    catch ( std::exception& )
    {


      try
      {

        // first case where the file format on disk fit with a site map
        // labelled with int16_t
        gkg::Reader::getInstance().read( fileNameRois, roisInt16 );
        if ( roisInt16.getHeader().hasAttribute( "resolutionX" ) )
        {

          roisInt16.getHeader().getAttribute( "resolutionX",
                                              roisResolution.x );

        }
        if ( roisInt16.getHeader().hasAttribute( "resolutionY" ) )
        {

          roisInt16.getHeader().getAttribute( "resolutionY",
                                              roisResolution.y );

        }
        if ( roisInt16.getHeader().hasAttribute( "resolutionZ" ) )
        {

          roisInt16.getHeader().getAttribute( "resolutionZ",
                                              roisResolution.z );

        }

      }
      catch ( std::exception& )
      {


        try
        {

          // in case disk file is a volume of int16_t, convert it to a site map
          // on the fly
          gkg::Volume< int16_t > volumeOfRoisInt16;
          gkg::Reader::getInstance().read( fileNameRois, volumeOfRoisInt16 );
          gkg::EqualToFunction< int16_t > inBackgroundTest( 0 );
          roisInt16 = 
            gkg::SiteMapFactory< int16_t, int32_t >::getInstance().create(
                                                             volumeOfRoisInt16,
                                                             inBackgroundTest );
          volumeOfRoisInt16.getResolution( roisResolution ); 

        }
        catch ( std::exception& thing )
        {

          throw std::runtime_error( "unable to read ROIs" );

        }

      }

    }
    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }


    if ( algorithmType == "global-spin-glass" )
    {

      if ( !roisString.getRanks().empty() )
      {

        if ( roisString.getRankCount() != 1 )
        {

           throw std::runtime_error(
                                    "ROI with only one rank allowed when using "
                                    "'global-spin-glass' tractography" );

        }

      }
      if ( !roisInt16.getRanks().empty() )
      {

        if ( roisInt16.getRankCount() != 1 )
        {

           throw std::runtime_error(
                                    "ROI with only one rank allowed when using "
                                    "'global-spin-glass' tractography" );

        }

      }

    }


    ////////////////////////////////////////////////////////////////////////////
    // reading mask
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

     std::cout << "reading '" << fileNameMask << "' : " << std::flush;

    }
    gkg::SiteMap< int32_t, int32_t > mask;
    gkg::Vector3d< int32_t > maskSize( 1, 1, 1 );
    gkg::Vector3d< double > maskResolution( 1.0, 1.0, 1.0 );
    try
    {

      // first case where the file format on disk fit with a site map
      gkg::Reader::getInstance().read( fileNameMask, mask );

    }
    catch ( std::exception& )
    {

      try
      {

        // in case disk file is a volume of int16_t, convert it to a site map
        // on the fly
        gkg::Volume< int16_t > volumeMaskInt16;
        gkg::Reader::getInstance().read( fileNameMask, volumeMaskInt16 );
        volumeMaskInt16.getSize( maskSize );

        gkg::EqualToFunction< int16_t > inBackgroundTest( 0 );
        gkg::Vector3d< int32_t > site;
        std::list< gkg::Vector3d< int32_t > > sites;
        for ( site.z = 0; site.z < maskSize.z; site.z++ )
        {

          for ( site.y = 0; site.y < maskSize.y; site.y++ )
          {

            for ( site.x = 0; site.x < maskSize.x; site.x++ )
            {

              if ( !inBackgroundTest.getState( volumeMaskInt16( site ) ) )
              {

                sites.push_back( site );

              }

            }

          }

        }

        // adding sites
        mask.addSites( 0, sites );

        // adding size and resolution information
        volumeMaskInt16.getResolution( maskResolution);
        mask.getHeader().addAttribute( "sizeX", maskSize.x );
        mask.getHeader().addAttribute( "sizeY", maskSize.y );
        mask.getHeader().addAttribute( "sizeZ", maskSize.z );
        mask.getHeader().addAttribute( "resolutionX", maskResolution.x );
        mask.getHeader().addAttribute( "resolutionY", maskResolution.y );
        mask.getHeader().addAttribute( "resolutionZ", maskResolution.z );

      }
      catch ( std::exception& )
      {

        try
        {

          // in case disk file is a volume of int16_t, convert it to a site map
          // on the fly
          gkg::Volume< uint8_t > volumeMaskUInt8;
          gkg::Reader::getInstance().read( fileNameMask, volumeMaskUInt8 );
          volumeMaskUInt8.getSize( maskSize );

          gkg::EqualToFunction< int8_t > inBackgroundTest( 0 );
          gkg::Vector3d< int32_t > site;
          std::list< gkg::Vector3d< int32_t > > sites;
          for ( site.z = 0; site.z < maskSize.z; site.z++ )
          {

            for ( site.y = 0; site.y < maskSize.y; site.y++ )
            {

              for ( site.x = 0; site.x < maskSize.x; site.x++ )
              {

                if ( !inBackgroundTest.getState( volumeMaskUInt8( site ) ) )
                {

                  sites.push_back( site );

                }

              }

            }

          }

          // adding sites
          mask.addSites( 0, sites );

          // adding resolution information
          volumeMaskUInt8.getResolution( maskResolution );
          mask.getHeader().addAttribute( "sizeX", maskSize.x );
          mask.getHeader().addAttribute( "sizeY", maskSize.y );
          mask.getHeader().addAttribute( "sizeZ", maskSize.z );
          mask.getHeader().addAttribute( "resolutionX", maskResolution.x );
          mask.getHeader().addAttribute( "resolutionY", maskResolution.y );
          mask.getHeader().addAttribute( "resolutionZ", maskResolution.z );

        }
        catch ( std::exception& thing )
        {

          throw std::runtime_error( "unable to read mask" );

        }

      }

    }
    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }


    ////////////////////////////////////////////////////////////////////////////
    // building output orientation count
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

     std::cout << "building output orientation set : " << std::flush;

    }

    gkg::TextureMap< gkg::OrientationDistributionFunction >::const_iterator
      o = odfs.begin();
    if ( o == odfs.end() )
    {

      throw std::runtime_error( "ODF field is empty!" );

    }
    gkg::OrientationDistributionFunction::BasisType
      basisType = o->second.getBasisType();

    if ( basisType == gkg::OrientationDistributionFunction::SphericalHarmonics )
    {

      if ( outputOrientationCount <= 0 )
      {

        throw std::runtime_error( "missing or bad output orientation count" );

      }

    }
    else if ( basisType == gkg::OrientationDistributionFunction::Standard )
    {

      if ( outputOrientationCount > 0 )
      {

        std::cerr << std::endl <<
               "'outputOrientationCount' parameter will be ignored as ODF "
               "texture map provided has got a standard basis, and therefore a "
               "fixed output orientation count" << std::endl;

      }
      outputOrientationCount = o->second.getValueCount();

    }
    gkg::OrientationSet outputOrientationSet( gkg::ElectrostaticOrientationSet(
                    outputOrientationCount / 2 ).getSymmetricalOrientations() );

    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }


    ////////////////////////////////////////////////////////////////////////////
    // building cartesian ODF field from ODF texture map / site map
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

     std::cout << "building ODF cartesian field : " << std::flush;

    }

    gkg::OdfCartesianField* 
      odfCartesianField = new gkg::OdfCartesianField( siteMap,
                                                      odfs,
                                                      outputOrientationSet );
    gkg::Vector3d< int32_t > odfsSize = odfCartesianField->getSize();
    gkg::Vector3d< double > odfsResolution = odfCartesianField->getResolution();

    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }


    ////////////////////////////////////////////////////////////////////////////
    // building 3D transforms between ODFs / ROIs / MASK referential(s)
    ////////////////////////////////////////////////////////////////////////////

    // creating referential(s)
    gkg::Referential* referentialOfOdfsVoxel =
                       new gkg::Referential( ODFS_VOXEL_REFERENTIAL_NAME );
    gkg::Referential* referentialOfOdfsReal =
                       new gkg::Referential( ODFS_REAL_REFERENTIAL_NAME );

    gkg::Referential* referentialOfRoisVoxel = 
                       new gkg::Referential( ROIS_VOXEL_REFERENTIAL_NAME );
    gkg::Referential* referentialOfRoisReal =
                       new gkg::Referential( ROIS_REAL_REFERENTIAL_NAME );

    gkg::Referential* referentialOfMaskVoxel =
                       new gkg::Referential( MASK_VOXEL_REFERENTIAL_NAME );
    gkg::Referential* referentialOfMaskReal =
                       new gkg::Referential( MASK_REAL_REFERENTIAL_NAME );

    // adding 3D transform from ODFs voxel to real coordinates
    gkg::RCPointer< gkg::Transform3d< float > >
      transform3dFromOdfsVoxelToOdfsReal( new gkg::Scaling3d< float >(
                                                  ( float )odfsResolution.x,
                                                  ( float )odfsResolution.y,
                                                  ( float )odfsResolution.z ) );
    gkg::Transform3dManager< float >::getInstance().registerTransform3d(
      referentialOfOdfsVoxel,
      referentialOfOdfsReal,
      transform3dFromOdfsVoxelToOdfsReal );

    // adding 3D transform from ROIs voxel to real coordinates
    gkg::RCPointer< gkg::Transform3d< float > >
      transform3dFromRoisVoxelToRoisReal ( new gkg::Scaling3d< float >(
                                                  ( float )roisResolution.x,
                                                  ( float )roisResolution.y,
                                                  ( float )roisResolution.z ) );
    gkg::Transform3dManager< float >::getInstance().registerTransform3d(
      referentialOfRoisVoxel,
      referentialOfRoisReal,
      transform3dFromRoisVoxelToRoisReal );

    // adding 3D transform from MASK voxel to real coordinates
    gkg::RCPointer< gkg::Transform3d< float > >
      transform3dFromMaskVoxelToMaskReal( new gkg::Scaling3d< float >(
                                                  ( float )maskResolution.x,
                                                  ( float )maskResolution.y,
                                                  ( float )maskResolution.z ) );
    gkg::Transform3dManager< float >::getInstance().registerTransform3d(
      referentialOfMaskVoxel,
      referentialOfMaskReal,
      transform3dFromMaskVoxelToMaskReal );

    // adding 3D rigid transform from ROIs to ODFs real coordinates
    gkg::RCPointer< gkg::Transform3d< float > >
      transform3dFromRoisRealToOdfsReal;
    if ( fileNameRoisToOdfsTransform3d.empty() )
    {

      if ( verbose )
      {

       std::cout << "initializing 3D ROIs->ODFs transform to identity : "
                 << std::flush;

      }
      // by default, initializaing to inverse scaling
      transform3dFromRoisRealToOdfsReal.reset(
          new gkg::IdentityTransform3d< float > );
      if ( verbose )
      {

        std::cout << "done" << std::endl;

      }

    }
    else
    {

      if ( verbose )
      {

       std::cout << "reading 3D ROIs->ODFs transform : " << std::flush;

      }

      try
      {

        gkg::RCPointer< gkg::RigidTransform3d< float > >
          rigidTransform3d( new gkg::RigidTransform3d< float > );
        std::ifstream is( fileNameRoisToOdfsTransform3d.c_str() );
        if ( is.good() )
        {

          rigidTransform3d->readTrm( is );
          is.close();
          transform3dFromRoisRealToOdfsReal = rigidTransform3d;

        }
        else
        {

          throw std::runtime_error( "invalid 3D ROIs->ODFs transform path" );

        }

      }
      catch( std::exception& )
      {

        try
        {

          gkg::RCPointer< gkg::AffineWoShearingTransform3d< float > >
            affineWoShearingTransform3d(
                               new gkg::AffineWoShearingTransform3d< float > );
          std::ifstream is( fileNameRoisToOdfsTransform3d.c_str() );
          if ( is.good() )
          {

            affineWoShearingTransform3d->readTrm( is );
            is.close();
            transform3dFromRoisRealToOdfsReal = affineWoShearingTransform3d;

          }
          else
          {

            throw std::runtime_error( "invalid 3D ROIs->ODFs transform path" );

          }

        }
        catch( std::exception& )
        {

          try
          {

            gkg::RCPointer< gkg::AffineTransform3d< float > >
              affineTransform3d( new gkg::AffineTransform3d< float > );
            std::ifstream is( fileNameRoisToOdfsTransform3d.c_str() );
            if ( is.good() )
            {

              affineTransform3d->readTrm( is );
              is.close();
              transform3dFromRoisRealToOdfsReal = affineTransform3d;

            }
            else
            {

              throw std::runtime_error(
                                       "invalid 3D ROIs->ODFs transform path" );

            }

          }
          catch( std::exception& )
          {

            throw std::runtime_error( "invalid 3D ROIs->ODFs transform" );

          }

        }

      }


      if ( verbose )
      {

          std::cout << "done" << std::endl;

      }

    }
    gkg::Transform3dManager< float >::getInstance().registerTransform3d(
      referentialOfRoisReal,
      referentialOfOdfsReal,
      transform3dFromRoisRealToOdfsReal );


    // adding 3D rigid transform from MASK to ODFs real coordinates
    gkg::RCPointer< gkg::Transform3d< float > >
      transform3dFromMaskRealToOdfsReal;
    if ( fileNameMaskToOdfsTransform3d.empty() )
    {

      if ( verbose )
      {

       std::cout << "initializing 3D MASK->ODFs transform to identity : "
                 << std::flush;

      }
      // by default, initializaing to inverse scaling
      transform3dFromMaskRealToOdfsReal.reset(
          new gkg::IdentityTransform3d< float > );
      if ( verbose )
      {

        std::cout << "done" << std::endl;

      }

    }
    else
    {

      if ( verbose )
      {

       std::cout << "reading 3D MASK->ODFs transform : " << std::flush;

      }

      try
      {

        gkg::RCPointer< gkg::RigidTransform3d< float > >
          rigidTransform3d( new gkg::RigidTransform3d< float > );
        std::ifstream is( fileNameMaskToOdfsTransform3d.c_str() );
        rigidTransform3d->readTrm( is );
        is.close();
        transform3dFromMaskRealToOdfsReal = rigidTransform3d;

      }
      catch( std::exception& )
      {

        try
        {

          gkg::RCPointer< gkg::AffineWoShearingTransform3d< float > >
            affineWoShearingTransform3d(
                                new gkg::AffineWoShearingTransform3d< float > );
          std::ifstream is( fileNameMaskToOdfsTransform3d.c_str() );
          affineWoShearingTransform3d->readTrm( is );
          is.close();
          transform3dFromMaskRealToOdfsReal = affineWoShearingTransform3d;

        }
        catch( std::exception& )
        {

          try
          {

            gkg::RCPointer< gkg::AffineTransform3d< float > >
              affineTransform3d( new gkg::AffineTransform3d< float > );
            std::ifstream is( fileNameMaskToOdfsTransform3d.c_str() );
            affineTransform3d->readTrm( is );
            is.close();
            transform3dFromMaskRealToOdfsReal = affineTransform3d;

          }
          catch( std::exception& )
          {

            throw std::runtime_error( "invalid 3D MASK->ODFs transform" );

          }

        }

      }
      if ( verbose )
      {

        std::cout << "done" << std::endl;

      }

    }
    gkg::Transform3dManager< float >::getInstance().registerTransform3d(
      referentialOfMaskReal,
      referentialOfOdfsReal,
      transform3dFromMaskRealToOdfsReal );

    // obtaining the 3D transforms required during tractography
    gkg::Transform3d< float >*
      transform3dFromRoisVoxelToOdfsReal =
        gkg::Transform3dManager< float >::getInstance().getTransform3d(
          referentialOfRoisVoxel,
          referentialOfOdfsReal );

    gkg::Transform3d< float >*
      transform3dFromRoisVoxelToMaskVoxel =
        gkg::Transform3dManager< float >::getInstance().getTransform3d(
          referentialOfRoisVoxel,
          referentialOfMaskVoxel );

    gkg::Transform3d< float >*
      transform3dFromOdfsRealToMaskVoxel =
        gkg::Transform3dManager< float >::getInstance().getTransform3d(
          referentialOfOdfsReal,
          referentialOfMaskVoxel );

    gkg::Transform3d< float >*
      transform3dFromOdfsRealToRoisVoxel =
        gkg::Transform3dManager< float >::getInstance().getTransform3d(
          referentialOfOdfsReal,
          referentialOfRoisVoxel );


    ////////////////////////////////////////////////////////////////////////////
    // updating forward step
    ////////////////////////////////////////////////////////////////////////////

    if ( algorithmType == "streamline-deterministic" )
    {

      if ( theAlgorithmScalarParameters[ 1 ] == 0.0 )
      {

        theAlgorithmScalarParameters[ 1 ] = 0.25 *
                                   std::min( std::min( odfsResolution.x,
                                                       odfsResolution.y ),
                                             odfsResolution.z );

      }

    }
    else if ( algorithmType == "streamline-regularized-deterministic" )
    {

      if ( theAlgorithmScalarParameters[ 1 ] == 0.0 )
      {

        theAlgorithmScalarParameters[ 1 ] = 0.25 *
                                   std::min( std::min( odfsResolution.x,
                                                       odfsResolution.y ),
                                             odfsResolution.z );

      }

    }
    else if ( algorithmType == "streamline-probabilistic" )
    {

      if ( theAlgorithmScalarParameters[ 1 ] == 0.0 )
      {

        theAlgorithmScalarParameters[ 1 ] = 0.25 *
                                   std::min( std::min( odfsResolution.x,
                                                       odfsResolution.y ),
                                             odfsResolution.z );

      }

    }


    ////////////////////////////////////////////////////////////////////////////
    // building continuous ODF field from ODF cartesian field
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

     std::cout << "building ODF continuous field : " << std::flush;

    }

    gkg::OdfContinuousField*
      odfContinuousField = new gkg::OdfCartesianToContinuousField(
                                                           *odfCartesianField,
                                                           false );

    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }


    ////////////////////////////////////////////////////////////////////////////
    // building voxel sampler
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

     std::cout << "building voxel sampler : " << std::flush;

    }

    gkg::VoxelSampler< float >*
      voxelSampler = new gkg::CartesianVoxelSampler< float >(
                       roisResolution,
                       ( int32_t )( theAlgorithmScalarParameters[ 0 ] + 0.5 ) );

    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }

    float apertureAngle = ( algorithmType == "global-spin-glass" ) ?
                          theAlgorithmScalarParameters[ 14 ] * M_PI / 180.0 :
                          theAlgorithmScalarParameters[ 5 ];
    gkg::OrientationSetCache::getInstance().update(
                                   odfCartesianField->getOutputOrientationSet(),
                                   apertureAngle );

    ////////////////////////////////////////////////////////////////////////////
    // performing fiber tracking
    ////////////////////////////////////////////////////////////////////////////

    if ( !roisString.getRanks().empty() )
    {

      // allocating tractography algorithm
      gkg::TractographyAlgorithm< std::string >* tractographyAlgorithm =
      gkg::TractographyAlgorithmFactory< std::string >::getInstance().create(
                                                 algorithmType,
                                                 odfsSize,
                                                 odfsResolution,
                                                 theAlgorithmScalarParameters,
                                                 theAlgorithmStringParameters );

      // preparing fiber tracking
      tractographyAlgorithm->prepare( *odfCartesianField,
                                      mask,
                                      *transform3dFromRoisVoxelToOdfsReal,
                                      *transform3dFromRoisVoxelToMaskVoxel,
                                      *transform3dFromOdfsRealToMaskVoxel,
                                      *transform3dFromOdfsRealToRoisVoxel,
                                      verbose );

      // looping over step(s)
      int32_t step;
      for ( step = 1; step <= stepCount; step++ )
      {
      
        if ( verbose )
        {

          std::cout << "tracking ( step " << step << "/" << stepCount 
	            << " ) : " << std::flush;

        }

        // allocating bundle map of string labels
        gkg::BundleMap< std::string > bundleMap;

        // tracking fiber bundles
        tractographyAlgorithm->track( roisString,
                                      getSubRegions( roisString,
	                                             step,
						     stepCount ),
                                      *voxelSampler,
                                      *odfContinuousField,
                                      mask,
                                      *transform3dFromRoisVoxelToOdfsReal,
                                      *transform3dFromRoisVoxelToMaskVoxel,
                                      *transform3dFromOdfsRealToMaskVoxel,
                                      *transform3dFromOdfsRealToRoisVoxel,
                                      bundleMap,
                                      verbose );

        if ( verbose )
        {

          std::cout << "done" << std::endl;

        }

        // saving bundle map
        std::string fileNameBundleMapForStep = getFileNameBundleMapForStep(
                                                              fileNameBundleMap,
                                                              step,
							      stepCount );
        if ( verbose )
        {

          std::cout << "saving bundle map '"
	            << fileNameBundleMapForStep
		    << "' : "
                    << std::flush;

        }
        bundleMap.getHeader().addAttribute( "sizeX", odfsSize.x );
        bundleMap.getHeader().addAttribute( "sizeY", odfsSize.y );
        bundleMap.getHeader().addAttribute( "sizeZ", odfsSize.z );
        bundleMap.getHeader().addAttribute( "resolutionX", odfsResolution.x );
        bundleMap.getHeader().addAttribute( "resolutionY", odfsResolution.y );
        bundleMap.getHeader().addAttribute( "resolutionZ", odfsResolution.z );
        gkg::Writer::getInstance().write( fileNameBundleMapForStep,
                                          bundleMap,
                                          ascii,
                                          bundleMapFormat );
        if ( verbose )
        {

          std::cout << "done" << std::endl;

        }
	
      }

      // deleting tractography algorithm
      if ( tractographyAlgorithm )
      {

        delete tractographyAlgorithm;

      }

    }
    else if ( !roisInt16.getRanks().empty() )
    {


      // allocating tractography algorithm
      gkg::TractographyAlgorithm< int16_t >* tractographyAlgorithm =
      gkg::TractographyAlgorithmFactory< int16_t >::getInstance().create(
                                             algorithmType,
                                             odfCartesianField->getSize(),
                                             odfCartesianField->getResolution(),
                                             theAlgorithmScalarParameters,
                                             theAlgorithmStringParameters );

      // preparing fiber tracking
      tractographyAlgorithm->prepare( *odfCartesianField,
                                      mask,
                                      *transform3dFromRoisVoxelToOdfsReal,
                                      *transform3dFromRoisVoxelToMaskVoxel,
                                      *transform3dFromOdfsRealToMaskVoxel,
                                      *transform3dFromOdfsRealToRoisVoxel,
                                      verbose );

      // looping over step(s)
      int32_t step;
      for ( step = 1; step <= stepCount; step++ )
      {
      
        if ( verbose )
        {

          std::cout << "tracking ( step " << step << "/" << stepCount 
	            << " ) : " << std::flush;

        }

        // allocating bundle map of int16_t labels
        gkg::BundleMap< int16_t > bundleMap;

        // tracking fiber bundles
        tractographyAlgorithm->track( roisInt16,
                                      getSubRegions( roisInt16,
	                                             step,
                                                     stepCount ),
                                      *voxelSampler,
                                      *odfContinuousField,
                                      mask,
                                      *transform3dFromRoisVoxelToOdfsReal,
                                      *transform3dFromRoisVoxelToMaskVoxel,
                                      *transform3dFromOdfsRealToMaskVoxel,
                                      *transform3dFromOdfsRealToRoisVoxel,
                                      bundleMap,
                                      verbose );
        if ( verbose )
        {

          std::cout << "done" << std::endl;

        }

        // saving bundle map
        std::string fileNameBundleMapForStep = getFileNameBundleMapForStep(
                                                              fileNameBundleMap,
                                                              step,
                                                              stepCount );
        if ( verbose )
        {

          std::cout << "saving bundle map '"
                    << fileNameBundleMapForStep << "' : "
                    << std::flush;

        }
        if ( bundleMapFormat == "aimsbundlemap" )
        {


          gkg::BundleMap< std::string > bundleMapString;
          std::set< int16_t> labels = bundleMap.getLabels();
          std::set< int16_t >::const_iterator l = labels.begin(),
                                              le = labels.end();
          while ( l != le )
          {

            bundleMapString.addBundle( gkg::StringConverter::toString( *l ),
                                       bundleMap.getBundle( *l ) );
            ++ l;

          }

          bundleMapString.getHeader().addAttribute( "sizeX", odfsSize.x );
          bundleMapString.getHeader().addAttribute( "sizeY", odfsSize.y );
          bundleMapString.getHeader().addAttribute( "sizeZ", odfsSize.z );
          bundleMapString.getHeader().addAttribute( "resolutionX",
                                                    odfsResolution.x );
          bundleMapString.getHeader().addAttribute( "resolutionY",
                                                    odfsResolution.y );
          bundleMapString.getHeader().addAttribute( "resolutionZ",
                                                    odfsResolution.z );
          gkg::Writer::getInstance().write( fileNameBundleMapForStep,
	                                    bundleMapString,
                                            ascii,
                                            bundleMapFormat );

        }
        else
        {

          bundleMap.getHeader().addAttribute( "sizeX", odfsSize.x );
          bundleMap.getHeader().addAttribute( "sizeY", odfsSize.y );
          bundleMap.getHeader().addAttribute( "sizeZ", odfsSize.z );
          bundleMap.getHeader().addAttribute( "resolutionX", odfsResolution.x );
          bundleMap.getHeader().addAttribute( "resolutionY", odfsResolution.y );
          bundleMap.getHeader().addAttribute( "resolutionZ", odfsResolution.z );
          gkg::Writer::getInstance().write( fileNameBundleMapForStep,
	                                    bundleMap,
                                            ascii,
                                            bundleMapFormat );

        }

        if ( verbose )
        {

          std::cout << "done" << std::endl;

        }
	
      }

      // deleting tractography algorithm
      if ( tractographyAlgorithm )
      {

        delete tractographyAlgorithm;

      }

    }


    ////////////////////////////////////////////////////////////////////////////
    // deleting voxel sampler
    ////////////////////////////////////////////////////////////////////////////

    if ( voxelSampler )
    {

      delete voxelSampler;

    }


    ////////////////////////////////////////////////////////////////////////////
    // deleting continuous ODF field
    ////////////////////////////////////////////////////////////////////////////

    if ( odfContinuousField )
    {

      delete odfContinuousField;

    }


    ////////////////////////////////////////////////////////////////////////////
    // deleting cartesian ODF field
    ////////////////////////////////////////////////////////////////////////////

    if ( odfCartesianField )
    {

      delete odfCartesianField;

    }

    ////////////////////////////////////////////////////////////////////////////
    // deleting 3D transform
    ////////////////////////////////////////////////////////////////////////////

    gkg::Transform3dManager< float >::getInstance().unregisterTransform3d(
      referentialOfOdfsVoxel,
      referentialOfOdfsReal );
    gkg::Transform3dManager< float >::getInstance().unregisterTransform3d(
      referentialOfRoisVoxel,
      referentialOfRoisReal );
    gkg::Transform3dManager< float >::getInstance().unregisterTransform3d(
      referentialOfMaskVoxel,
      referentialOfMaskReal );
    gkg::Transform3dManager< float >::getInstance().unregisterTransform3d(
      referentialOfRoisReal,
      referentialOfOdfsReal );
    gkg::Transform3dManager< float >::getInstance().unregisterTransform3d(
      referentialOfMaskReal,
      referentialOfOdfsReal );


    if ( referentialOfOdfsVoxel )
    {

      delete referentialOfOdfsVoxel;

    }
    if ( referentialOfOdfsReal )
    {

      delete referentialOfOdfsReal;

    }
    if ( referentialOfRoisVoxel )
    {

      delete referentialOfRoisVoxel;

    }
    if ( referentialOfRoisReal )
    {

      delete referentialOfRoisReal;

    }
    if ( referentialOfMaskVoxel )
    {

      delete referentialOfMaskVoxel;

    }
    if ( referentialOfMaskReal )
    {

      delete referentialOfMaskReal;

    }

  }
  GKG_CATCH( "void gkg::DwiTractographyCommand::execute( "
             "const std::string& fileNameSiteMap, "
             "const std::string& fileNameOdfs, "
             "const std::string& fileNameRois, "
             "const std::string& fileNameMask, "
             "const std::string& fileNameRoisToOdfsTransform3d, "
             "const std::string& fileNameMaskToOdfsTransform3d, "
             "const std::string& fileNameBundleMap, "
             "const std::string& algorithmType, "
             "const std::vector< double >& algorithmScalarParameters, "
             "int32_t outputOrientationCount, "
             "const std::string& volumeFormat, "
             "const std::string& bundleMapFormat, "
             "int32_t stepCount, "
             "bool ascii, "
             "bool verbose  )" );

}


RegisterCommandCreator(
    DwiTractographyCommand,
    DECLARE_STRING_PARAMETER_HELP( fileNameSiteMap ) +
    DECLARE_STRING_PARAMETER_HELP( fileNameOdfs ) +
    DECLARE_STRING_PARAMETER_HELP( fileNameRois ) +
    DECLARE_STRING_PARAMETER_HELP( fileNameMask ) +
    DECLARE_STRING_PARAMETER_HELP( fileNameRoisToOdfsTransform3d ) +
    DECLARE_STRING_PARAMETER_HELP( fileNameMaskToOdfsTransform3d ) +
    DECLARE_STRING_PARAMETER_HELP( fileNameBundleMap ) +
    DECLARE_STRING_PARAMETER_HELP( algorithmType ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( algorithmScalarParameters ) +
    DECLARE_VECTOR_OF_STRINGS_PARAMETER_HELP( algorithmStringParameters ) +
    DECLARE_INTEGER_PARAMETER_HELP( outputOrientationCount ) +
    DECLARE_STRING_PARAMETER_HELP( volumeFormat ) +
    DECLARE_STRING_PARAMETER_HELP( bundleMapFormat ) +
    DECLARE_INTEGER_PARAMETER_HELP( stepCount ) +
    DECLARE_BOOLEAN_PARAMETER_HELP( ascii ) +
    DECLARE_BOOLEAN_PARAMETER_HELP( verbose ) );


#undef ODFS_VOXEL_REFERENTIAL_NAME
#undef ODFS_REAL_REFERENTIAL_NAME 

#undef ROIS_VOXEL_REFERENTIAL_NAME
#undef ROIS_REAL_REFERENTIAL_NAME 

#undef MASK_VOXEL_REFERENTIAL_NAME
#undef MASK_REAL_REFERENTIAL_NAME 

