#include <gkg-dmri-plugin-functors/DwiWhiteMatterTissueSimulator/DwiWhiteMatterTissueSimulatorCommand.h>
#include <gkg-dmri-simulator-virtual-tissues/FiberPopulation.h>
#include <gkg-dmri-simulator-virtual-tissues/GlialCellPopulation.h>
#include <gkg-dmri-simulator-virtual-tissues/NonOverlappingSpherePopulationContainer.h>
//#include <gkg-dmri-io/HDF5Writer.h>
#include <gkg-communication-command/CommandFactory.h>
#include <gkg-communication-getopt/Application.h>
//#include <gkg-processing-numericalanalysis/NumericalAnalysisSelector.h>
#include <gkg-processing-numericalanalysis/RandomGenerator.h>
#include <gkg-dmri-container/BundleMap_i.h>
#include <gkg-core-io/Writer.h>
#include <gkg-core-io/Eraser.h>
#include <gkg-core-exception/Exception.h>

//#include <gkg-processing-mesh/MeshFactory.h>
//#include <gkg-processing-container/MeshMap_i.h>
//#include <gkg-processing-container/Volume_i.h>
//#include <gkg-processing-mesh/MeshAccumulator_i.h>
//#include <gkg-processing-mesh/CylinderCollision3d.h>
//#include <gkg-processing-algobase/Math.h>
//#include <gkg-processing-transform/Rotation3dFunctions.h>

#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>



gkg::DwiWhiteMatterTissueSimulatorCommand::DwiWhiteMatterTissueSimulatorCommand(
                                                              int32_t argc,
                                                              char* argv[],
                                                              bool loadPlugin,
                                                              bool removeFirst )
                                        : gkg::Command( argc, argv, 
                                                        loadPlugin,
                                                        removeFirst )
{

  try
  {

    parse();

  }
  GKG_CATCH( "gkg::DwiWhiteMatterTissueSimulatorCommand::"
             "DwiWhiteMatterTissueSimulatorCommand( "
             "int32_t argc, char* argv[], bool loadPlugin, "
             "bool removeFirst )" );

}


gkg::DwiWhiteMatterTissueSimulatorCommand::DwiWhiteMatterTissueSimulatorCommand(
      const gkg::BoundingBox< float >& fieldOfView,
      float gridResolution,
      const std::vector< float >& fiberPopulationAxonDiameterMeans,
      const std::vector< float >& fiberPopulationAxonDiameterStdDevs,
      const std::vector< float >& fiberPopulationGRatioMeans,
      const std::vector< float >& fiberPopulationGRatioStdDevs,
      const std::vector< float >& 
                           fiberPopulationInternodalLengthToNodeWidthRatioMeans,
      const std::vector< float >&
                       fiberPopulationInternodalLengthToNodeWidthRatioStdDevs,
      const std::vector< float >& fiberPopulationInterbeadingLengthMeans,
      const std::vector< float >& fiberPopulationInterbeadingLengthStdDevs,
      const std::vector< float >& fiberPopulationBeadingMagnitudeRatioMeans,
      const std::vector< float >& fiberPopulationBeadingMagnitudeRatioStdDevs,
      const std::vector< gkg::Vector3d< float > >& 
                                                fiberPopulationMeanOrientations,
      const std::vector< float >& fiberPopulationIntraAxonalVolumeFractions,
      const std::vector< float >& fiberPopulationGlobalAngularDispersions,
      const std::vector< float >& fiberPopulationTortuosityVariations,
      const std::vector< float >& fiberPopulationTortuosityMagnitudes,
      const std::vector< float >& fiberPopulationTortuosityWaveLengths,
      const std::vector< float >& fiberPopulationTortuosityAngularDispersions,
      int32_t fiberPopulationTortuosityMaximumIterationCount,
      float fiberResolutionAlongAxis,
      bool fiberPopulationCreateAxonMembranes,
      bool fiberPopulationCreateOuterMyelinSheaths,
      bool fiberPopulationCreateRanvierNodes,
      bool fiberPopulationCreateBeadings,
      const std::vector< float >& glialCellPopulationDiameterMeans,
      const std::vector< float >& glialCellPopulationDiameterStdDevs,
      const std::vector< float >& glialCellPopulationDeformationRatios,
      const std::vector< float >&
                               glialCellPopulationIntracellularVolumeFractions,
      bool glialCellPopulationCreateMembranes,
      const std::string& fileNameOut,
      const std::string& format,
      bool verbose )
                                          : gkg::Command()
{

  try
  {

    execute( fieldOfView,
             gridResolution,
             fiberPopulationAxonDiameterMeans,
             fiberPopulationAxonDiameterStdDevs,
             fiberPopulationGRatioMeans,
             fiberPopulationGRatioStdDevs,
             fiberPopulationInternodalLengthToNodeWidthRatioMeans,
             fiberPopulationInternodalLengthToNodeWidthRatioStdDevs,
             fiberPopulationInterbeadingLengthMeans,
             fiberPopulationInterbeadingLengthStdDevs,
             fiberPopulationBeadingMagnitudeRatioMeans,
             fiberPopulationBeadingMagnitudeRatioStdDevs,
             fiberPopulationMeanOrientations,
             fiberPopulationIntraAxonalVolumeFractions,
             fiberPopulationGlobalAngularDispersions,
             fiberPopulationTortuosityVariations,
             fiberPopulationTortuosityMagnitudes,
             fiberPopulationTortuosityWaveLengths,
             fiberPopulationTortuosityAngularDispersions,
             fiberPopulationTortuosityMaximumIterationCount,
             fiberResolutionAlongAxis,
             fiberPopulationCreateAxonMembranes,
             fiberPopulationCreateOuterMyelinSheaths,
             fiberPopulationCreateRanvierNodes,
             fiberPopulationCreateBeadings,
             glialCellPopulationDiameterMeans,
             glialCellPopulationDiameterStdDevs,
             glialCellPopulationDeformationRatios,
             glialCellPopulationIntracellularVolumeFractions,
             glialCellPopulationCreateMembranes,
             fileNameOut,
             format,
             verbose );

  }
  GKG_CATCH( "gkg::DwiWhiteMatterTissueSimulatorCommand::"
             "DwiWhiteMatterTissueSimulatorCommand( "
             "const gkg::BoundingBox< float >& fieldOfView, "
             "float gridResolution, "
             "const std::vector< float >& fiberPopulationAxonDiameterMeans, "
             "const std::vector< float >& "
             "fiberPopulationAxonDiameterStdDevs, "
             "const std::vector< float >& fiberPopulationGRatioMeans, "
             "const std::vector< float >& fiberPopulationGRatioStdDevs, "
             "const std::vector< float >& "
             "fiberPopulationInternodalLengthToNodeWidthRatioMeans, "
             "const std::vector< float >& "
             "fiberPopulationInternodalLengthToNodeWidthRatioStdDevs, "
             "const std::vector< float >& "
             "fiberPopulationInterbeadingLengthMeans, "
             "const std::vector< float >& "
             "fiberPopulationInterbeadingLengthStdDevs, "
             "const std::vector< float >& "
             "fiberPopulationBeadingMagnitudeRatioMeans, "
             "const std::vector< float >& "
             "fiberPopulationBeadingMagnitudeRatioStdDevs, "
             "const std::vector< gkg::Vector3d< float > >& "
             "fiberPopulationMeanOrientations, "
             "const std::vector< float >& "
             "fiberPopulationIntraAxonalVolumeFractions, "
             "const std::vector< float >& "
             "fiberPopulationGlobalAngularDispersions, "
             "const std::vector< float >& "
             "fiberPopulationTortuosityVariations, "
             "const std::vector< float >& "
             "fiberPopulationTortuosityMagnitudes, "
             "const std::vector< float >& "
             "fiberPopulationTortuosityWaveLengths, "
             "const std::vector< float >& "
             "fiberPopulationTortuosityAngularDispersions, "
             "int32_t fiberPopulationTortuosityMaximumIterationCount, "
             "float fiberResolutionAlongAxis, "
             "bool fiberPopulationCreateAxonMembranes, "
             "bool fiberPopulationCreateOuterMyelinSheaths, "
             "bool fiberPopulationCreateRanvierNodes, "
             "bool fiberPopulationCreateBeadings, "
             "const std::vector< float >& glialCellPopulationDiameterMeans, "
             "const std::vector< float >& "
             "glialCellPopulationDiameterStdDevs, "
             "const std::vector< float >& "
             "glialCellPopulationDeformationRatios, "
             "const std::vector< float >& "
             "glialCellPopulationIntracellularVolumeFractions, "
             "bool glialCellPopulationCreateMembranes, "
             "const std::string& fileNameOut, "
             "const std::string& format, "
             "bool verbose )" );

}


gkg::DwiWhiteMatterTissueSimulatorCommand::DwiWhiteMatterTissueSimulatorCommand(
                                             const gkg::Dictionary& parameters )
                                        : gkg::Command( parameters )
{

  try
  {

    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER( parameters, std::vector< float >,
                                           fieldOfViewVector );
    DECLARE_FLOATING_PARAMETER( parameters, float, gridResolution );

    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER( parameters, std::vector< float >,
                                           fiberPopulationAxonDiameterMeans );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER(
                                         parameters, std::vector< float >,
                                         fiberPopulationAxonDiameterStdDevs );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER( parameters, std::vector< float >,
                                           fiberPopulationGRatioMeans );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER( parameters, std::vector< float >,
                                           fiberPopulationGRatioStdDevs );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER(
                         parameters, std::vector< float >,
                         fiberPopulationInternodalLengthToNodeWidthRatioMeans );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER(
                     parameters, std::vector< float >,
                     fiberPopulationInternodalLengthToNodeWidthRatioStdDevs );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER(
                                       parameters, std::vector< float >,
                                       fiberPopulationInterbeadingLengthMeans );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER(
                                    parameters, std::vector< float >,
                                    fiberPopulationInterbeadingLengthStdDevs );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER(
                                    parameters, std::vector< float >,
                                    fiberPopulationBeadingMagnitudeRatioMeans );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER(
                                 parameters, std::vector< float >,
                                 fiberPopulationBeadingMagnitudeRatioStdDevs );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER(
                                        parameters, std::vector< float >,
                                        fiberPopulationMeanOrientationVector );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER(
                                    parameters, std::vector< float >,
                                    fiberPopulationIntraAxonalVolumeFractions );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER(
                                      parameters, std::vector< float >,
                                      fiberPopulationGlobalAngularDispersions );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER(
                                          parameters, std::vector< float >,
                                          fiberPopulationTortuosityVariations );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER(
                                          parameters, std::vector< float >,
                                          fiberPopulationTortuosityMagnitudes );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER(
                                         parameters, std::vector< float >,
                                         fiberPopulationTortuosityWaveLengths );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER(
                                  parameters, std::vector< float >,
                                  fiberPopulationTortuosityAngularDispersions );
    DECLARE_INTEGER_PARAMETER( parameters, int32_t,
                               fiberPopulationTortuosityMaximumIterationCount );
    DECLARE_FLOATING_PARAMETER( parameters, float, fiberResolutionAlongAxis );
    DECLARE_BOOLEAN_PARAMETER( parameters, bool,
                               fiberPopulationCreateAxonMembranes );
    DECLARE_BOOLEAN_PARAMETER( parameters, bool,
                               fiberPopulationCreateOuterMyelinSheaths );
    DECLARE_BOOLEAN_PARAMETER( parameters, bool,
                               fiberPopulationCreateRanvierNodes );
    DECLARE_BOOLEAN_PARAMETER( parameters, bool,
                               fiberPopulationCreateBeadings );

    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER( parameters, std::vector< float >,
                                           glialCellPopulationDiameterMeans );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER(
                                         parameters, std::vector< float >,
                                         glialCellPopulationDiameterStdDevs );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER(
                                         parameters, std::vector< float >,
                                         glialCellPopulationDeformationRatios );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER(
                              parameters, std::vector< float >,
                              glialCellPopulationIntracellularVolumeFractions );
    DECLARE_BOOLEAN_PARAMETER( parameters, bool,
                               glialCellPopulationCreateMembranes );

    DECLARE_STRING_PARAMETER( parameters, std::string,
                              fileNameOut );
    DECLARE_STRING_PARAMETER( parameters, std::string,
                              format );
    DECLARE_BOOLEAN_PARAMETER( parameters, bool,
                               verbose );


    gkg::BoundingBox< float > fieldOfView( fieldOfViewVector[ 0 ],
                                           fieldOfViewVector[ 1 ],
                                           fieldOfViewVector[ 2 ],
                                           fieldOfViewVector[ 3 ],
                                           fieldOfViewVector[ 4 ],
                                           fieldOfViewVector[ 5 ] );

    int32_t fiberPopulationCount = 
                             ( int32_t )fiberPopulationAxonDiameterMeans.size();
    std::vector< gkg::Vector3d< float > > 
      fiberPopulationMeanOrientations( fiberPopulationCount );

    int32_t p = 0;
    for ( p = 0; p < fiberPopulationCount; p++ )
    {
 
       
       fiberPopulationMeanOrientations[ p ] = gkg::Vector3d< float >(
                            fiberPopulationMeanOrientationVector[ 3 * p + 0 ],
                            fiberPopulationMeanOrientationVector[ 3 * p + 1 ],
                            fiberPopulationMeanOrientationVector[ 3 * p + 2 ] );

    }

    execute( fieldOfView,
             gridResolution,
             fiberPopulationAxonDiameterMeans,
             fiberPopulationAxonDiameterStdDevs,
             fiberPopulationGRatioMeans,
             fiberPopulationGRatioStdDevs,
             fiberPopulationInternodalLengthToNodeWidthRatioMeans,
             fiberPopulationInternodalLengthToNodeWidthRatioStdDevs,
             fiberPopulationInterbeadingLengthMeans,
             fiberPopulationInterbeadingLengthStdDevs,
             fiberPopulationBeadingMagnitudeRatioMeans,
             fiberPopulationBeadingMagnitudeRatioStdDevs,
             fiberPopulationMeanOrientations,
             fiberPopulationIntraAxonalVolumeFractions,
             fiberPopulationGlobalAngularDispersions,
             fiberPopulationTortuosityVariations,
             fiberPopulationTortuosityMagnitudes,
             fiberPopulationTortuosityWaveLengths,
             fiberPopulationTortuosityAngularDispersions,
             fiberPopulationTortuosityMaximumIterationCount,
             fiberResolutionAlongAxis,
             fiberPopulationCreateAxonMembranes,
             fiberPopulationCreateOuterMyelinSheaths,
             fiberPopulationCreateRanvierNodes,
             fiberPopulationCreateBeadings,
             glialCellPopulationDiameterMeans,
             glialCellPopulationDiameterStdDevs,
             glialCellPopulationDeformationRatios,
             glialCellPopulationIntracellularVolumeFractions,
             glialCellPopulationCreateMembranes,
             fileNameOut,
             format,
             verbose );

  }
  GKG_CATCH( "gkg::DwiWhiteMatterTissueSimulatorCommand::"
             "DwiWhiteMatterTissueSimulatorCommand( "
             "const gkg::Dictionary& parameters )" );

}


gkg::DwiWhiteMatterTissueSimulatorCommand::
                                         ~DwiWhiteMatterTissueSimulatorCommand()
{
}


std::string gkg::DwiWhiteMatterTissueSimulatorCommand::getStaticName()
{

  try
  {

    return "DwiWhiteMatterTissueSimulator";

  }
  GKG_CATCH( "std::string "
             "gkg::DwiWhiteMatterTissueSimulatorCommand::getStaticName()" );

}


void gkg::DwiWhiteMatterTissueSimulatorCommand::parse()
{

  try
  {


    gkg::BoundingBox< float > fieldOfView;
    std::vector< float > fieldOfViewVector;
    float gridResolution = 10.0; // in um

    //////////////////////////////////////////////////////////////////////////
    // fiber population parameter(s)
    //////////////////////////////////////////////////////////////////////////

    std::vector< float > fiberPopulationAxonDiameterMeans;
    std::vector< float > fiberPopulationAxonDiameterStdDevs;
    std::vector< float > fiberPopulationGRatioMeans;
    std::vector< float > fiberPopulationGRatioStdDevs;
    std::vector< float > fiberPopulationInternodalLengthToNodeWidthRatioMeans;
    std::vector< float >
                       fiberPopulationInternodalLengthToNodeWidthRatioStdDevs;
    std::vector< float > fiberPopulationInterbeadingLengthMeans;
    std::vector< float > fiberPopulationInterbeadingLengthStdDevs;
    std::vector< float > fiberPopulationBeadingMagnitudeRatioMeans;
    std::vector< float > fiberPopulationBeadingMagnitudeRatioStdDevs;
    std::vector< float > fiberPopulationMeanOrientationVector;
    std::vector< float > fiberPopulationIntraAxonalVolumeFractions;
    std::vector< float > fiberPopulationGlobalAngularDispersions;

    std::vector< float > fiberPopulationTortuosityVariations;
    std::vector< float > fiberPopulationTortuosityMagnitudes;
    std::vector< float > fiberPopulationTortuosityWaveLengths;
    std::vector< float > fiberPopulationTortuosityAngularDispersions;
    int32_t fiberPopulationTortuosityMaximumIterationCount = 1000000;

    float fiberResolutionAlongAxis = 0.1;  // in um 

    bool fiberPopulationCreateAxonMembranes = true;
    bool fiberPopulationCreateOuterMyelinSheaths = true;
    bool fiberPopulationCreateRanvierNodes = true;
    bool fiberPopulationCreateBeadings = true;

    //////////////////////////////////////////////////////////////////////////
    // glial cell population parameter(s)
    //////////////////////////////////////////////////////////////////////////

    std::vector< float > glialCellPopulationDiameterMeans;
    std::vector< float > glialCellPopulationDiameterStdDevs;
    std::vector< float > glialCellPopulationDeformationRatios;
    std::vector< float > glialCellPopulationIntracellularVolumeFractions;
    bool glialCellPopulationCreateMembranes = true;

    //////////////////////////////////////////////////////////////////////////
    // output filename, format and verbosity parameter(s)
    //////////////////////////////////////////////////////////////////////////

    std::string fileNameOut;
    std::string format = "membrane";
    bool verbose = false;


    ////////////////////////////////////////////////////////////////////////////
    // managing command lines argument(s)
    ////////////////////////////////////////////////////////////////////////////

    gkg::Application application( _argc, _argv,
                                  "Diffusion white matter tissue simulator",
                                  _loadPlugin );

    application.addSeriesOption( "-fieldOfView",
                                 "Field of view in um",
                                 fieldOfViewVector,
                                 6, 6 );
    application.addSingleOption( "-gridResolution",
                                 "Grid resolution in um (default=10.0um)",
                                 gridResolution );

     application.addSeriesOption( "-fiberPopulationAxonDiameterMeans",
                                 "List of fiber population axon diameter means"
                                 " in um",
                                 fiberPopulationAxonDiameterMeans,
                                 1 );
     application.addSeriesOption( "-fiberPopulationAxonDiameterStdDevs",
                                 "List of fiber population axon diameter"
                                 " standard deviations in um",
                                 fiberPopulationAxonDiameterStdDevs,
                                 1 );


    application.addSeriesOption( "-fiberPopulationGRatioMeans",
                                 "List of fiber population G-ratio means",
                                 fiberPopulationGRatioMeans,
                                 1 );
    application.addSeriesOption( "-fiberPopulationGRatioStdDevs",
                                 "List of fiber population G-ratio standard"
                                 " deviations",
                                 fiberPopulationGRatioStdDevs,
                                 1 );

    application.addSeriesOption(
                        "-fiberPopulationInternodalLengthToNodeWidthRatioMeans",
                        "List of fiber population internodal length to node"
                        " width ratio means",
                        fiberPopulationInternodalLengthToNodeWidthRatioMeans,
                        1 );
    application.addSeriesOption(
                      "-fiberPopulationInternodalLengthToNodeWidthRatioStdDevs",
                      "List of fiber population internodal length to node"
                      " width ratio standard deviations",
                      fiberPopulationInternodalLengthToNodeWidthRatioStdDevs,
                      1 );

    application.addSeriesOption( "-fiberPopulationInterbeadingLengthMeans",
                                 "List of fiber population interbeading length"
                                 " means in um",
                                 fiberPopulationInterbeadingLengthMeans,
                                 1 );
    application.addSeriesOption( "-fiberPopulationInterbeadingLengthStdDevs",
                                 "List of fiber population interbeading length"
                                 " standard deviations in um",
                                 fiberPopulationInterbeadingLengthStdDevs,
                                 1 );

    application.addSeriesOption( "-fiberPopulationBeadingMagnitudeRatioMeans",
                                 "List of fiber population beading magnitude"
                                 " ratio means",
                                 fiberPopulationBeadingMagnitudeRatioMeans,
                                 1 );
    application.addSeriesOption( "-fiberPopulationBeadingMagnitudeRatioStdDevs",
                                 "List of fiber population beading magnitude"
                                 " ratio standard deviations",
                                 fiberPopulationBeadingMagnitudeRatioStdDevs,
                                 1 );

    application.addSeriesOption( "-fiberPopulationMeanOrientations",
                                 "List of fiber population mean orientations",
                                 fiberPopulationMeanOrientationVector,
                                 3 );

    application.addSeriesOption( "-fiberPopulationIntraAxonalVolumeFractions",
                                 "List of fiber population intra-axonal volume"
                                 " fractions",
                                 fiberPopulationIntraAxonalVolumeFractions,
                                 1 );
    application.addSeriesOption( "-fiberPopulationGlobalAngularDispersions",
                                 "List of fiber population angular dispersions"
                                 " in degrees",
                                 fiberPopulationGlobalAngularDispersions,
                                 1 );

    application.addSeriesOption( "-fiberPopulationTortuosityVariations",
                                 "List of fiber population tortuosity"
                                 " variations in um",
                                 fiberPopulationTortuosityVariations,
                                 1 );
    application.addSeriesOption( "-fiberPopulationTortuosityMagnitudes",
                                 "List of fiber population tortuosity"
                                 " magnitudes in um",
                                 fiberPopulationTortuosityMagnitudes,
                                 1 );
    application.addSeriesOption( "-fiberPopulationTortuosityAngularDispersions",
                                 "List of fiber population tortuosity "
                                 " angular dispersions in degrees",
                                 fiberPopulationTortuosityAngularDispersions,
                                 1 );
    application.addSeriesOption( "-fiberPopulationTortuosityWaveLengths",
                                 "List of fiber population tortuosity "
                                 " wavelengths in um",
                                 fiberPopulationTortuosityWaveLengths,
                                 1 );
    application.addSingleOption(
                          "-fiberPopulationTortuosityMaximumIterationCount",
                          "Fiber population tortuosity maximum iteration count "
                          "(default=1000000)",
                          fiberPopulationTortuosityMaximumIterationCount,
                          true );

    application.addSingleOption( "-fiberResolutionAlongAxis",
                                 "Fiber resolution along axis in um"
                                 " (default=0.1um)",
                                 fiberResolutionAlongAxis,
                                 true );

    application.addSingleOption( "-fiberPopulationCreateAxonMembranes",
                                 "Create fiber axon membranes (default=true)",
                                 fiberPopulationCreateAxonMembranes,
                                 true );
    application.addSingleOption( "-fiberPopulationCreateOuterMyelinSheaths",
                                 "Create fiber myelin sheaths (default=true)",
                                 fiberPopulationCreateOuterMyelinSheaths,
                                 true );
    application.addSingleOption( "-fiberPopulationCreateRanvierNodes",
                                 "Create fiber Ranvier nodes (default=true)",
                                 fiberPopulationCreateRanvierNodes,
                                 true );
    application.addSingleOption( "-fiberPopulationCreateBeadings",
                                 "Create fiber beadings (default=true)",
                                 fiberPopulationCreateBeadings,
                                 true );

    application.addSeriesOption( "-glialCellPopulationDiameterMeans",
                                 "List of glial cell population diameter means"
                                 " in um",
                                 glialCellPopulationDiameterMeans,
                                 1 );
    application.addSeriesOption( "-glialCellPopulationDiameterStdDevs",
                                 "List of glial cell population diameter"
                                 " standard deviations in um",
                                 glialCellPopulationDiameterStdDevs,
                                 1 );
    application.addSeriesOption( "-glialCellPopulationDeformationRatios",
                                 "List of glial cell population deformation"
                                 " ratios",
                                 glialCellPopulationDeformationRatios,
                                 1 );
    application.addSeriesOption(
                            "-glialCellPopulationIntracellularlVolumeFractions",
                            "List of glial cell population intracellular volume"
                            " fractions",
                            glialCellPopulationIntracellularVolumeFractions,
                            1 );
    application.addSingleOption( "-glialCellPopulationCreateMembranes",
                                 "Create glial cell membranes (default=true)",
                                 glialCellPopulationCreateMembranes,
                                 true );


    application.addSingleOption( "-o",
                                 "Output base filename (no extension provided)",
                                 fileNameOut );
    application.addSingleOption( "-format",
                                 "Output format (default=membrane)\n"
                                "\t- membrane\n\t- hdf5",
                                 format,
                                 true );
    application.addSingleOption( "-verbose",
                                 "Show as much information as possible",
                                 verbose,
                                 true );

    ////// launching parser
    application.initialize();

    fieldOfView.setLowerX( fieldOfViewVector[ 0 ] );
    fieldOfView.setUpperX( fieldOfViewVector[ 1 ] );
    fieldOfView.setLowerY( fieldOfViewVector[ 2 ] );
    fieldOfView.setUpperY( fieldOfViewVector[ 3 ] );
    fieldOfView.setLowerZ( fieldOfViewVector[ 4 ] );
    fieldOfView.setUpperZ( fieldOfViewVector[ 5 ] );


    int32_t populationCount =
                             ( int32_t )fiberPopulationAxonDiameterMeans.size();
    std::vector< gkg::Vector3d< float > >
      fiberPopulationMeanOrientations( populationCount );

    int32_t p = 0;
    for ( p = 0; p < populationCount; p++ )
    {
 
       
       fiberPopulationMeanOrientations[ p ] = gkg::Vector3d< float >(
                            fiberPopulationMeanOrientationVector[ 3 * p + 0 ],
                            fiberPopulationMeanOrientationVector[ 3 * p + 1 ],
                            fiberPopulationMeanOrientationVector[ 3 * p + 2 ] );

    }

    execute( fieldOfView,
             gridResolution,
             fiberPopulationAxonDiameterMeans,
             fiberPopulationAxonDiameterStdDevs,
             fiberPopulationGRatioMeans,
             fiberPopulationGRatioStdDevs,
             fiberPopulationInternodalLengthToNodeWidthRatioMeans,
             fiberPopulationInternodalLengthToNodeWidthRatioStdDevs,
             fiberPopulationInterbeadingLengthMeans,
             fiberPopulationInterbeadingLengthStdDevs,
             fiberPopulationBeadingMagnitudeRatioMeans,
             fiberPopulationBeadingMagnitudeRatioStdDevs,
             fiberPopulationMeanOrientations,
             fiberPopulationIntraAxonalVolumeFractions,
             fiberPopulationGlobalAngularDispersions,
             fiberPopulationTortuosityVariations,
             fiberPopulationTortuosityMagnitudes,
             fiberPopulationTortuosityWaveLengths,
             fiberPopulationTortuosityAngularDispersions,
             fiberPopulationTortuosityMaximumIterationCount,
             fiberResolutionAlongAxis,
             fiberPopulationCreateAxonMembranes,
             fiberPopulationCreateOuterMyelinSheaths,
             fiberPopulationCreateRanvierNodes,
             fiberPopulationCreateBeadings,
             glialCellPopulationDiameterMeans,
             glialCellPopulationDiameterStdDevs,
             glialCellPopulationDeformationRatios,
             glialCellPopulationIntracellularVolumeFractions,
             glialCellPopulationCreateMembranes,
             fileNameOut,
             format,
             verbose );

  }
  GKG_CATCH_FUNCTOR( "void gkg::DwiWhiteMatterTissueSimulatorCommand::parse()" );

}


void gkg::DwiWhiteMatterTissueSimulatorCommand::execute(
      const gkg::BoundingBox< float >& fieldOfView,
      float gridResolution,
      const std::vector< float >& fiberPopulationAxonDiameterMeans,
      const std::vector< float >& fiberPopulationAxonDiameterStdDevs,
      const std::vector< float >& fiberPopulationGRatioMeans,
      const std::vector< float >& fiberPopulationGRatioStdDevs,
      const std::vector< float >& 
                           fiberPopulationInternodalLengthToNodeWidthRatioMeans,
      const std::vector< float >&
                       fiberPopulationInternodalLengthToNodeWidthRatioStdDevs,
      const std::vector< float >& fiberPopulationInterbeadingLengthMeans,
      const std::vector< float >& fiberPopulationInterbeadingLengthStdDevs,
      const std::vector< float >& fiberPopulationBeadingMagnitudeRatioMeans,
      const std::vector< float >& fiberPopulationBeadingMagnitudeRatioStdDevs,
      const std::vector< gkg::Vector3d< float > >& 
                                                fiberPopulationMeanOrientations,
      const std::vector< float >& fiberPopulationIntraAxonalVolumeFractions,
      const std::vector< float >& fiberPopulationGlobalAngularDispersions,
      const std::vector< float >& fiberPopulationTortuosityVariations,
      const std::vector< float >& fiberPopulationTortuosityMagnitudes,
      const std::vector< float >& fiberPopulationTortuosityWaveLengths,
      const std::vector< float >& fiberPopulationTortuosityAngularDispersions,
      int32_t /* fiberPopulationTortuosityMaximumIterationCount */,
      float fiberResolutionAlongAxis,
      bool /* fiberPopulationCreateAxonMembranes */,
      bool /* fiberPopulationCreateOuterMyelinSheaths */,
      bool /* fiberPopulationCreateRanvierNodes */,
      bool /* fiberPopulationCreateBeadings */,
      const std::vector< float >& glialCellPopulationDiameterMeans,
      const std::vector< float >& glialCellPopulationDiameterStdDevs,
      const std::vector< float >& glialCellPopulationDeformationRatios,
      const std::vector< float >&
                                glialCellPopulationIntracellularVolumeFractions,
      bool /* glialCellPopulationCreateMembranes */,
      const std::string& /* fileNameOut */,
      const std::string& /* format */,
      bool verbose )
{

  try
  {

    ////////////////////////////////////////////////////////////////////////////
    // collecting fiber population count
    ////////////////////////////////////////////////////////////////////////////

    int32_t fiberPopulationCount = 
                             ( int32_t )fiberPopulationAxonDiameterMeans.size();

    // sanity check(s)
    if ( ( ( int32_t )fiberPopulationAxonDiameterStdDevs.size() !=
           fiberPopulationCount ) ||
         ( ( int32_t )fiberPopulationGRatioMeans.size() !=
           fiberPopulationCount ) ||
         ( ( int32_t )fiberPopulationGRatioStdDevs.size() !=
           fiberPopulationCount ) ||
         ( ( int32_t )fiberPopulationInternodalLengthToNodeWidthRatioMeans.
                                                                       size() !=
           fiberPopulationCount ) ||
         ( ( int32_t )fiberPopulationInternodalLengthToNodeWidthRatioStdDevs.
                                                                       size() !=
           fiberPopulationCount ) ||
         ( ( int32_t )fiberPopulationInterbeadingLengthMeans.size() !=
           fiberPopulationCount ) ||
         ( ( int32_t )fiberPopulationInterbeadingLengthStdDevs.size() !=
           fiberPopulationCount ) ||
         ( ( int32_t )fiberPopulationBeadingMagnitudeRatioMeans.size() !=
           fiberPopulationCount ) ||
         ( ( int32_t )fiberPopulationBeadingMagnitudeRatioStdDevs.size() !=
           fiberPopulationCount ) ||
         ( ( int32_t )fiberPopulationMeanOrientations.size() !=
           fiberPopulationCount ) ||
         ( ( int32_t )fiberPopulationIntraAxonalVolumeFractions.size() != 
           fiberPopulationCount ) ||
         ( ( int32_t )fiberPopulationGlobalAngularDispersions.size() !=
           fiberPopulationCount ) ||
         ( ( int32_t )fiberPopulationTortuosityVariations.size() !=
           fiberPopulationCount ) ||
         ( ( int32_t )fiberPopulationTortuosityMagnitudes.size() !=
           fiberPopulationCount ) ||
         ( ( int32_t )fiberPopulationTortuosityWaveLengths.size() != 
           fiberPopulationCount ) ||
         ( ( int32_t )fiberPopulationTortuosityAngularDispersions.size() != 
           fiberPopulationCount ) )
    {

      throw std::runtime_error( "incoherent size between input "
                                "fiber population vector(s)" );

    }


    ////////////////////////////////////////////////////////////////////////////
    // collecting glial cell population count
    ////////////////////////////////////////////////////////////////////////////

    int32_t glialCellPopulationCount = 
                             ( int32_t )glialCellPopulationDiameterMeans.size();

    // sanity check(s)
    if ( ( ( int32_t )glialCellPopulationDiameterStdDevs.size() !=
           glialCellPopulationCount ) ||
         ( ( int32_t )glialCellPopulationDeformationRatios.size() !=
           glialCellPopulationCount ) ||
         ( ( int32_t )glialCellPopulationIntracellularVolumeFractions.size() !=
           glialCellPopulationCount ) )
    {

      throw std::runtime_error( "incoherent size between input "
                                "glial cell population vector(s)" );

    }


    ////////////////////////////////////////////////////////////////////////////
    // getting pointer to the numerical analysis factory
    ////////////////////////////////////////////////////////////////////////////

    //gkg::NumericalAnalysisImplementationFactory* factory =
    // gkg::NumericalAnalysisSelector::getInstance().getImplementationFactory();

    gkg::RCPointer< gkg::RandomGenerator > randomGenerator(
                       new gkg::RandomGenerator( gkg::RandomGenerator::Taus ) );



    ////////////////////////////////////////////////////////////////////////////
    // creating the non-overlapping sphere container
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

      std::cout << "creating non-pverlapping sphere container : " << std::flush;

    }

    gkg::NonOverlappingSpherePopulationContainer 
     nonOverlappingSpherePopulationContainer( fieldOfView,
                                              gridResolution );

    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }



    ////////////////////////////////////////////////////////////////////////////
    // creating the fiber population(s)
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

      std::cout << "creating fiber population(s) : " << std::flush;

    }

    std::vector< gkg::RCPointer< gkg::FiberPopulation > >
      fiberPopulations( fiberPopulationCount );

    int32_t fiberPopulationIndex = 0;
    for ( fiberPopulationIndex = 0;
          fiberPopulationIndex < fiberPopulationCount;
          fiberPopulationIndex ++ )
    {

      fiberPopulations[ fiberPopulationIndex ].reset(
        new gkg::FiberPopulation(
              fieldOfView,
              fiberPopulationAxonDiameterMeans[ fiberPopulationIndex ],
              fiberPopulationAxonDiameterStdDevs[ fiberPopulationIndex ],
              fiberPopulationGRatioMeans[ fiberPopulationIndex ],
              fiberPopulationGRatioStdDevs[ fiberPopulationIndex ],
              fiberPopulationInternodalLengthToNodeWidthRatioMeans[
                                                         fiberPopulationIndex ],
              fiberPopulationInternodalLengthToNodeWidthRatioStdDevs[
                                                         fiberPopulationIndex ],
              fiberPopulationInterbeadingLengthMeans[ fiberPopulationIndex ],
              fiberPopulationInterbeadingLengthStdDevs[ fiberPopulationIndex ],
              fiberPopulationBeadingMagnitudeRatioMeans[ fiberPopulationIndex ],
              fiberPopulationBeadingMagnitudeRatioStdDevs[
                                                         fiberPopulationIndex ],
              fiberPopulationMeanOrientations[ fiberPopulationIndex ],
              fiberPopulationIntraAxonalVolumeFractions[ fiberPopulationIndex ],
              fiberPopulationGlobalAngularDispersions[ fiberPopulationIndex ],

              fiberPopulationTortuosityVariations[ fiberPopulationIndex ],
              fiberPopulationTortuosityMagnitudes[ fiberPopulationIndex ],
              fiberPopulationTortuosityWaveLengths[ fiberPopulationIndex ],
              fiberPopulationTortuosityAngularDispersions[
                                                         fiberPopulationIndex ],
              fiberResolutionAlongAxis,
              randomGenerator ) );

      nonOverlappingSpherePopulationContainer.add(
                                     fiberPopulations[ fiberPopulationIndex ] );

    }

    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }


    ////////////////////////////////////////////////////////////////////////////
    // creating the glial cell population(s)
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

      std::cout << "creating glial cell population(s) : " << std::flush;

    }

    std::vector< gkg::RCPointer< gkg::GlialCellPopulation > >
      glialCellPopulations( glialCellPopulationCount );

    int32_t glialCellPopulationIndex = 0;
    for ( glialCellPopulationIndex = 0;
          glialCellPopulationIndex < glialCellPopulationCount;
          glialCellPopulationIndex ++ )
    {

      glialCellPopulations[ glialCellPopulationIndex ].reset(
        new gkg::GlialCellPopulation(
              fieldOfView,
              glialCellPopulationDiameterMeans[ glialCellPopulationIndex ],
              glialCellPopulationDiameterStdDevs[ glialCellPopulationIndex ],
              glialCellPopulationDeformationRatios[ glialCellPopulationIndex ],
              glialCellPopulationIntracellularVolumeFractions[
                                                     glialCellPopulationIndex ],
              randomGenerator ) );

      nonOverlappingSpherePopulationContainer.add(
                             glialCellPopulations[ glialCellPopulationIndex ] );

    }

    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }

    ////////////////////////////////////////////////////////////////////////////
    // removing sphere overlap(s)
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

      std::cout << " removing sphere overlap(s) : " << std::flush;

    }

    nonOverlappingSpherePopulationContainer.reserve( fiberPopulationCount +
                                                     glialCellPopulationCount );
    nonOverlappingSpherePopulationContainer.collectSpheres();
    nonOverlappingSpherePopulationContainer.removeSphereOverlaps();

    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }

  }
  GKG_CATCH( "void gkg::DwiWhiteMatterTissueSimulatorCommand::execute( "
             "const gkg::BoundingBox< float >& fieldOfView, "
             "float gridResolution, "
             "const std::vector< float >& fiberPopulationAxonDiameterMeans, "
             "const std::vector< float >& fiberPopulationAxonDiameterStdDevs, "
             "const std::vector< float >& fiberPopulationGRatioMeans, "
             "const std::vector< float >& fiberPopulationGRatioStdDevs, "
             "const std::vector< float >& "
             "fiberPopulationInternodalLengthToNodeWidthRatioMeans, "
             "const std::vector< float >& "
             "fiberPopulationInternodalLengthToNodeWidthRatioStdDevs, "
             "const std::vector< float >& "
             "fiberPopulationInterbeadingLengthMeans, "
             "const std::vector< float >& "
             "fiberPopulationInterbeadingLengthStdDevs, "
             "const std::vector< float >& "
             "fiberPopulationBeadingMagnitudeRatioMeans, "
             "const std::vector< float >& "
             "fiberPopulationBeadingMagnitudeRatioStdDevs, "
             "const std::vector< gkg::Vector3d< float > >& "
             "fiberPopulationMeanOrientations, "
             "const std::vector< float >& "
             "fiberPopulationIntraAxonalVolumeFractions, "
             "const std::vector< float >& "
             "fiberPopulationGlobalAngularDispersions, "
             "const std::vector< float >& "
             "fiberPopulationTortuosityVariations, "
             "const std::vector< float >& "
             "fiberPopulationTortuosityMagnitudes, "
             "const std::vector< float >& "
             "fiberPopulationTortuosityWaveLengths, "
             "const std::vector< float >& "
             "fiberPopulationTortuosityAngularDispersions, "
             "int32_t fiberPopulationTortuosityMaximumIterationCount, "
             "float fiberResolutionAlongAxis, "
             "bool fiberPopulationCreateAxonMembranes, "
             "bool fiberPopulationCreateOuterMyelinSheaths, "
             "bool fiberPopulationCreateRanvierNodes, "
             "bool fiberPopulationCreateBeadings, "
             "const std::vector< float >& glialCellPopulationDiameterMeans, "
             "const std::vector< float >& glialCellPopulationDiameterStdDevs, "
             "const std::vector< float >& "
             "glialCellPopulationDeformationRatios, "
             "const std::vector< float >& "
             "glialCellPopulationIntracellularVolumeFractions, "
             "bool glialCellPopulationCreateMembranes, "
             "const std::string& fileNameOut, "
             "const std::string& format, "
             "bool verbose )" );

}


RegisterCommandCreator( 
    DwiWhiteMatterTissueSimulatorCommand,
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( 
                 fieldOfViewVector ) +
    DECLARE_FLOATING_PARAMETER_HELP( 
                 gridResolution ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( 
                 fiberPopulationAxonDiameterMeans ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( 
                 fiberPopulationAxonDiameterStdDevs ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( 
                 fiberPopulationGRatioMeans ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( 
                 fiberPopulationGRatioStdDevs ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( 
                 fiberPopulationInternodalLengthToNodeWidthRatioMeans ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( 
                 fiberPopulationInternodalLengthToNodeWidthRatioStdDevs ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( 
                 fiberPopulationInterbeadingLengthMeans ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( 
                  fiberPopulationInterbeadingLengthStdDevs ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( 
                 fiberPopulationBeadingMagnitudeRatioMeans ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( 
                 fiberPopulatioBeadingMagnitudeRatioStdDevs ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( 
                 fiberPopulationMeanOrientationVector ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( 
                 fiberPopulationIntraAxonalVolumeFractions ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( 
                 fiberPopulationGlobalAngularDispersions ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( 
                 fiberPopulationTortuosityVariations ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( 
                 fiberPopulationTortuosityMagnitudes ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( 
                 fiberPopulationTortuosityWaveLengths ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( 
                 fiberPopulationTortuosityAngularDispersions ) +
    DECLARE_INTEGER_PARAMETER_HELP( 
                 fiberPopulationTortuosityMaximumIterationCount ) +
    DECLARE_FLOATING_PARAMETER_HELP( 
                 fiberResolutionAlongAxis ) +
    DECLARE_BOOLEAN_PARAMETER_HELP( 
                 fiberPopulationCreateAxonMembranes ) +
    DECLARE_BOOLEAN_PARAMETER_HELP( 
                 fiberPopulationCreateOuterMyelinSheaths ) +
    DECLARE_BOOLEAN_PARAMETER_HELP( 
                 fiberPopulationCreateRanvierNodes ) +
    DECLARE_BOOLEAN_PARAMETER_HELP( 
                 fiberPopulationCreateBeadings ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( 
                 glialCellPopulationDiameterMeans ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( 
                 glialCellPopulationDiameterStdDevs ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( 
                 glialCellPopulationDeformationRatios ) +
    DECLARE_BOOLEAN_PARAMETER_HELP( 
                 glialCellPopulationCreateMembranes ) +
    DECLARE_STRING_PARAMETER_HELP( 
                 fileNameOut ) +
    DECLARE_STRING_PARAMETER_HELP( 
                 format ) +
    DECLARE_BOOLEAN_PARAMETER_HELP( 
                 verbose ) );
