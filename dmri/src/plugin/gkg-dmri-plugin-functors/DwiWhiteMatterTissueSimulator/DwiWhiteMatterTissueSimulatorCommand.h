#ifndef _gkg_dmri_plugin_functors_DwiWhiteMatterTissueSimulator_DwiWhiteMatterTissueSimulatorCommand_h_
#define _gkg_dmri_plugin_functors_DwiWhiteMatterTissueSimulator_DwiWhiteMatterTissueSimulatorCommand_h_

#include <gkg-communication-command/Command.h>
#include <gkg-core-pattern/Creator.h>
#include <gkg-processing-container/BoundingBox.h>
#include <gkg-processing-coordinates/Vector3d.h>
#include <gkg-processing-coordinates/Vector2d.h>
#include <gkg-core-pattern/RCPointer.h>

#include <string>
#include <vector>


namespace gkg
{


class DwiWhiteMatterTissueSimulatorCommand :
                       public Command,
                       public Creator2Arg< DwiWhiteMatterTissueSimulatorCommand,
                                           Command,
                                           int32_t,
                                           char** >,
                       public Creator1Arg< DwiWhiteMatterTissueSimulatorCommand,
                                           Command,
                                           const Dictionary& >
{

  public:

    DwiWhiteMatterTissueSimulatorCommand( int32_t argc,
                                          char* argv[],
                                          bool loadPlugin = false,
                                          bool removeFirst = true );
    DwiWhiteMatterTissueSimulatorCommand(

      //////////////////////////////////////////////////////////////////////////
      // field of view and LUT parameter(s)
      //////////////////////////////////////////////////////////////////////////

      const BoundingBox< float >& fieldOfView,
      float gridResolution,

      //////////////////////////////////////////////////////////////////////////
      // fiber population parameter(s)
      //////////////////////////////////////////////////////////////////////////

      const std::vector< float >& fiberPopulationAxonDiameterMeans,
      const std::vector< float >& fiberPopulationAxonDiameterStdDevs,
      const std::vector< float >& fiberPopulationGRatioMeans,
      const std::vector< float >& fiberPopulationGRatioStdDevs,
      const std::vector< float >& 
                           fiberPopulationInternodalLengthToNodeWidthRatioMeans,
      const std::vector< float >&
                       fiberPopulationInternodalLengthToNodeWidthRatioStdDevs,
      const std::vector< float >& fiberPopulationInterbeadingLengthMeans,
      const std::vector< float >& fiberPopulationInterbeadingLengthStdDevs,
      const std::vector< float >& fiberPopulationBeadingMagnitudeRatioMeans,
      const std::vector< float >& fiberPopulationBeadingMagnitudeRatioStdDevs,
      const std::vector< gkg::Vector3d< float > >& 
                                                fiberPopulationMeanOrientations,
      const std::vector< float >& fiberPopulationIntraAxonalVolumeFractions,
      const std::vector< float >& fiberPopulationGlobalAngularDispersions,

      const std::vector< float >& fiberPopulationTortuosityVariations,
      const std::vector< float >& fiberPopulationTortuosityMagnitudes,
      const std::vector< float >& fiberPopulationTortuosityWaveLengths,
      const std::vector< float >& fiberPopulationTortuosityAngularDispersions,
      int32_t fiberPopulationTortuosityMaximumIterationCount,

      float fiberResolutionAlongAxis,

      bool fiberPopulationCreateAxonMembranes,
      bool fiberPopulationCreateOuterMyelinSheaths,
      bool fiberPopulationCreateRanvierNodes,
      bool fiberPopulationCreateBeadings,

      //////////////////////////////////////////////////////////////////////////
      // glial cell population parameter(s)
      //////////////////////////////////////////////////////////////////////////

      const std::vector< float >& glialCellPopulationDiameterMeans,
      const std::vector< float >& glialCellPopulationDiameterStdDevs,
      const std::vector< float >& glialCellPopulationDeformationRatios,
      const std::vector< float >&
                                glialCellPopulationIntracellularVolumeFractions,
      bool glialCellPopulationCreateMembranes,

      //////////////////////////////////////////////////////////////////////////
      // output filename, format and verbosity parameter(s)
      //////////////////////////////////////////////////////////////////////////

      const std::string& fileNameOut,
      const std::string& format,
      bool verbose );

    DwiWhiteMatterTissueSimulatorCommand( const Dictionary& parameters );
    virtual ~DwiWhiteMatterTissueSimulatorCommand();

    static std::string getStaticName();


  protected:

    friend class Creator2Arg< DwiWhiteMatterTissueSimulatorCommand, Command, 
                              int32_t, char** >;
    friend class Creator1Arg< DwiWhiteMatterTissueSimulatorCommand, Command,
                              const Dictionary& >;

    void parse();
    void execute(
      //////////////////////////////////////////////////////////////////////////
      // field of view and LUT parameter(s)
      //////////////////////////////////////////////////////////////////////////

      const BoundingBox< float >& fieldOfView,
      float gridResolution,

      //////////////////////////////////////////////////////////////////////////
      // fiber population parameter(s)
      //////////////////////////////////////////////////////////////////////////

      const std::vector< float >& fiberPopulationAxonDiameterMeans,
      const std::vector< float >& fiberPopulationAxonDiameterStdDevs,
      const std::vector< float >& fiberPopulationGRatioMeans,
      const std::vector< float >& fiberPopulationGRatioStdDevs,
      const std::vector< float >& 
                           fiberPopulationInternodalLengthToNodeWidthRatioMeans,
      const std::vector< float >&
                       fiberPopulationInternodalLengthToNodeWidthRatioStdDevs,
      const std::vector< float >& fiberPopulationInterbeadingLengthMeans,
      const std::vector< float >& fiberPopulationInterbeadingLengthStdDevs,
      const std::vector< float >& fiberPopulationBeadingMagnitudeRatioMeans,
      const std::vector< float >& fiberPopulationBeadingMagnitudeRatioStdDevs,
      const std::vector< gkg::Vector3d< float > >& 
                                                fiberPopulationMeanOrientations,
      const std::vector< float >& fiberPopulationIntraAxonalVolumeFractions,
      const std::vector< float >& fiberPopulationGlobalAngularDispersions,

      const std::vector< float >& fiberPopulationTortuosityVariations,
      const std::vector< float >& fiberPopulationTortuosityMagnitudes,
      const std::vector< float >& fiberPopulationTortuosityWaveLengths,
      const std::vector< float >& fiberPopulationTortuosityAngularDispersions,
      int32_t fiberPopulationTortuosityMaximumIterationCount,

      float fiberResolutionAlongAxis,

      bool fiberPopulationCreateAxonMembranes,
      bool fiberPopulationCreateOuterMyelinSheaths,
      bool fiberPopulationCreateRanvierNodes,
      bool fiberPopulationCreateBeadings,

      //////////////////////////////////////////////////////////////////////////
      // glial cell population parameter(s)
      //////////////////////////////////////////////////////////////////////////

      const std::vector< float >& glialCellPopulationDiameterMeans,
      const std::vector< float >& glialCellPopulationDiameterStdDevs,
      const std::vector< float >& glialCellPopulationDeformationRatios,
      const std::vector< float >&
                                glialCellPopulationIntracellularVolumeFractions,
      bool glialCellPopulationCreateMembranes,

      //////////////////////////////////////////////////////////////////////////
      // output filename, format and verbosity parameter(s)
      //////////////////////////////////////////////////////////////////////////

      const std::string& fileNameOut,
      const std::string& format,
      bool verbose );

};


}


#endif
