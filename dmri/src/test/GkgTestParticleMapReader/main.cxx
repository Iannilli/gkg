#include <gkg-communication-getopt/Application.h>
#include <gkg-dmri-simulator-particle/ParticleMap.h>
#include <gkg-core-io/Reader_i.h>
#include <gkg-core-exception/Exception.h>

#include <iostream>
#include <fstream>
#include <set>


int main( int argc, char* argv[] )
{

  int result = EXIT_SUCCESS;

  try
  {


    std::string particleMapFileName;

    gkg::Application application( argc, argv,
                                  "Test particle map reader" );

    application.addSingleOption( "-particleMapFileName",
                                 "Input particle map file name",
                                 particleMapFileName );
    ////// launching parser
    application.initialize();

    gkg::ParticleMap particleMap;
    gkg::Reader::getInstance().read(
                        particleMapFileName,
                        particleMap,
                        "particlemap" );

    std::cout << "particle count : " 
              << ( int32_t )( particleMap.getParticleCount() )
              << std::endl; 

    std::cout << "point count : " 
              << ( int32_t )( particleMap.getParticle( 0 )->
                                getTrajectory().getPointCount() )
              << std::endl; 

    std::cout << "point : " 
              << particleMap.getParticle( 0 )->getTrajectory().getPoint( 0 ) 
              << std::endl;

  }
  GKG_CATCH_COMMAND( result );

  return result;

}



