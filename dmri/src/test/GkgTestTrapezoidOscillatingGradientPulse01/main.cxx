#include <gkg-communication-getopt/Application.h>
#include <gkg-dmri-simulator-pulse-sequence/TrapezoidOscillatingGradientPulse.h>
#include <gkg-core-exception/Exception.h>
#include <string>
#include <iostream>
#include <fstream>



int main( int argc, char* argv[] )
{

  int result = EXIT_SUCCESS;

  try
  {

    std::string fileName;

    gkg::Application application( argc, argv,
                                  "Test of trapezoid oscillating "
                                  "gradient pulse" );
    application.addSingleOption( "-o",
                                 "Output file name",
                                 fileName );

    application.initialize();

    int32_t axis = 0;
    float startingTime = 2.0f;
    float period = 1.0f;
    int32_t lobeCount = 5;
    float magnitude = 10.0f;
    float maximumSlewRate = 160.0f;
    float gradientTimeResolution = 0.001f;

    gkg::TrapezoidOscillatingGradientPulse trapezoidOscillatingGradientPulse(
                                                       axis,
                                                       startingTime,
                                                       period,
                                                       lobeCount,
                                                       magnitude,
                                                       maximumSlewRate,
                                                       gradientTimeResolution );

    std::vector< float > timeVector;
    std::vector< float > valueVector;
    float timeIncrement = period * gradientTimeResolution;

    std::ofstream os( fileName.c_str() );

    os << "import numpy as np" << std::endl;
    os << "import matplotlib.pyplot as plt" << std::endl;
    os << std::endl;

    float t = startingTime;
    while ( t < ( 0.5f * lobeCount * period + startingTime ) )
    {

      timeVector.push_back( t );
      valueVector.push_back( trapezoidOscillatingGradientPulse.getValueAt( 
                                                                    t, 1.0f ) );

      t += timeIncrement;      

    }

    os << "time = [ " << std::flush;
    uint32_t l = 0;
    for ( l = 0; l < timeVector.size(); l++ )
    {

      os << timeVector[ l ] << ", ";


    }
    os << " ]" << std::endl;


    os << "value = [ " << std::flush;
    l = 0;
    for ( l = 0; l < valueVector.size(); l++ )
    {

      os << valueVector[ l ] << ", ";


    }
    os << " ]" << std::endl;

    os << "fig = plt.figure()" << std::endl;
    os << "fig.set_size_inches( 5, 5 )" << std::endl;
    os << "ax = fig.add_subplot(1, 1, 1)" << std::endl;
    os << std::endl;
    os << "ax.set_xlim( 0.0, 7.0)" << std::endl;
    os << "ax.set_ylim( -120.0, 120.0)" << std::endl;
    os << std::endl;


    os << "markers = [ 'o', '^' ]" << std::endl;
    os << "colors = [ 'r', 'g', 'b', 'c', 'm', 'y', 'k', 'w' ]" << std::endl;
    os << std::endl;


    os << "plt.plot( time, " << std::endl;
    os << "          value, " << std::endl;
    os << "          marker=markers[ 0 ], " << std::endl;
    os << "          color=colors[ 0 ], " << std::endl;
    os << "          linestyle='None', " << std::endl;
    os << "          label='waveform' ) " << std::endl;
    os << std::endl;
    os << "plt.grid( True )" << std::endl;
    os << "plt.legend()" << std::endl;
    os << std::endl;
    os << "plt.xlabel( 'time' )" << std::endl;
    os << "plt.ylabel( 'value' )" << std::endl;
    os << std::endl;
    os << "plt.show()" << std::endl;
    os << std::endl;

    os.close();


  }
  GKG_CATCH_COMMAND( result );

  return result;

}




