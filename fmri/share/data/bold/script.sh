GkgBoldAnalysis -i $1.ima \
                -a $1_average.ima \
                -s $1_smoothed.ima \
                -p visual.paradigm.minf \
                -c visual.contrasts.minf \
                -d $1_design_matrix.ima \
                -effects $1_effects.ima \
                -variances $1_variances.ima \
                -stats pvalue $1_pvalue.ima \
                       tscore $1_tscore.ima \
                       zscore $1_zscore.ima \
                -startingTime 0.0 \
                -dummyScanCount 0 \
                -driftPolynomOrder 3 \
                -smoothing 1.5 \
                -smoothingAxis xy \
                -hrf $1_hrf.txt \
                -verbose
