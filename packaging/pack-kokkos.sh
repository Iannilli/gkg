#! /bin/bash

NCPU=`nproc --all`

cd ${GIT_PATH}

if [ -d kokkos ];
then
  cd kokkos
  \rm -rf cmake/Modules/FindKOKKOS.cmake
  git checkout -- CMakeLists.txt cmake/kokkos_build.cmake
  git pull
  cd ..
else
  git clone https://github.com/kokkos/kokkos.git
fi

# Create the file kokkos/cmake/Modules/FindKOKKOS.cmake

touch kokkos/cmake/Modules/FindKOKKOS.cmake
ed -s kokkos/cmake/Modules/FindKOKKOS.cmake <<EOF
i
# - Try to find the Kokkos library
#
# Once done this will define
#
#  KOKKOS_FOUND - system has Kokkos
#  KOKKOS_INCLUDE_DIR - the Kokkos include directory
#  KOKKOS_LIBRARIES - the libraries needed to use Kokkos

find_path( KOKKOS_INCLUDE_DIR Kokkos_Core.hpp
           PATHS
           /usr/include/kokkos
           /usr/local/include/kokkos )

find_library( KOKKOS_LIBRARIES kokkos )

# handle the QUIETLY and REQUIRED arguments and set KOKKOS_FOUND to TRUE if
# all listed variables are TRUE
include( FindPackageHandleStandardArgs )
FIND_PACKAGE_HANDLE_STANDARD_ARGS( KOKKOS REQUIRED_VARS KOKKOS_INCLUDE_DIR KOKKOS_LIBRARIES )

if ( KOKKOS_FOUND )
message( STATUS "Kokkos found" )
endif( KOKKOS_FOUND )

mark_as_advanced( KOKKOS_LIBRARIES KOKKOS_INCLUDE_DIR )
.
w
EOF

# Modification of the file kokkos/CMakeLists.txt

kokkos_version=`tail -n 1 kokkos/master_history.txt |awk '{print $2}'`
kokkos_major=`echo ${kokkos_version} |awk -F"." '{print $1}'`
kokkos_minor=`echo ${kokkos_version} |awk -F"." '{print $2}'`
kokkos_patch=`echo ${kokkos_version} |awk -F"." '{print $3}'`

sed -i "s/.*\#\ Basic.*/\ \ set( \${PROJECT_NAME}_VERSION_MAJOR ${kokkos_major} )\n&/" kokkos/CMakeLists.txt
sed -i "s/.*\#\ Basic.*/\ \ set( \${PROJECT_NAME}_VERSION_MINOR ${kokkos_minor} )\n&/" kokkos/CMakeLists.txt
sed -i "s/.*\#\ Basic.*/\ \ set( \${PROJECT_NAME}_VERSION_PATCH ${kokkos_patch} )\n&/" kokkos/CMakeLists.txt
sed -i "s/.*\#\ Basic.*/\ \ set( \${PROJECT_NAME}_VERSION "\${\${PROJECT_NAME}_VERSION_MAJOR}.\${\${PROJECT_NAME}_VERSION_MINOR}.\${\${PROJECT_NAME}_VERSION_PATCH}" )\n\n&/" kokkos/CMakeLists.txt
sed -i "s/.*\#\ Basic.*/\ \ set( CMAKE_INSTALL_PREFIX \"\$\{CMAKE_BINARY_DIR\}\" )\n\n&/" kokkos/CMakeLists.txt
sed -i "s/.*\#\ Basic.*/\ \ set( CMAKE_CXX_FLAGS \"\$\{CMAKE_CXX_FLAGS\} -fPIC\" )\n\n&/" kokkos/CMakeLists.txt

ed -s kokkos/CMakeLists.txt <<EOF
/DESTINATION\ \$.CMAKE_INSTALL_PREFIX
s/install/\#install/
+1
i
  install( DIRECTORY cmake/Modules DESTINATION share/kokkos/cmake )
.
$
a

# build a CPack driven debian installer package

find_program( LSB_RELEASE_EXEC lsb_release )
if( LSB_RELEASE_EXEC )
  execute_process( COMMAND \${LSB_RELEASE_EXEC} -is
                   OUTPUT_VARIABLE LSB_RELEASE_ID_SHORT
                   OUTPUT_STRIP_TRAILING_WHITESPACE
  )
  message( "Build platform : \${LSB_RELEASE_ID_SHORT}" )
  if( LSB_RELEASE_ID_SHORT STREQUAL "Ubuntu" )
    execute_process( COMMAND bash -c "dpkg --print-architecture"
                     COMMAND tr -d '\n' OUTPUT_VARIABLE outArch )
  else( LSB_RELEASE_ID_SHORT STREQUAL "Ubuntu" )
    set( outArch "amd64" )
  endif( LSB_RELEASE_ID_SHORT STREQUAL "Ubuntu" )
else( LSB_RELEASE_EXEC )
  set( LSB_RELEASE_ID_SHORT "Generic" )
  set( outArch "amd64" )
endif( LSB_RELEASE_EXEC )

message( "Build architecture : \${outArch}" )

include( InstallRequiredSystemLibraries )

set( CPACK_RESOURCE_FILE_LICENSE "\${\${PROJECT_NAME}_SOURCE_DIR}/Copyright.txt" )
set( CPACK_PACKAGE_VERSION_MAJOR "\${\${PROJECT_NAME}_VERSION_MAJOR}" )
set( CPACK_PACKAGE_VERSION_MINOR "\${\${PROJECT_NAME}_VERSION_MINOR}" )
set( CPACK_PACKAGE_VERSION_PATCH "\${\${PROJECT_NAME}_VERSION_PATCH}" )

if ( LSB_RELEASE_ID_SHORT STREQUAL "Ubuntu" )
  set( CPACK_GENERATOR "DEB" )
  set( OS_NAME "" )
  set( CPACK_DEBIAN_PACKAGE_SHLIBDEPS ON)
  set( CPACK_DEBIAN_PACKAGE_VERSION "\${CPACK_PACKAGE_VERSION}" )
  set( CPACK_DEBIAN_PACKAGE_ARCHITECTURE "\${outArch}" )
  set( CPACK_DEBIAN_PACKAGE_NAME "\${PROJECT_NAME}" )
  set( CPACK_DEBIAN_PACKAGE_MAINTAINER "Fabrice Poupon <fabrice.poupon@cea.fr>" )
  set( CPACK_DEBIAN_PACKAGE_DESCRIPTION "Kokkos debian package" )
else( LSB_RELEASE_ID_SHORT STREQUAL "Ubuntu" )
  if ( LSB_RELEASE_ID_SHORT STREQUAL "CentOS" )
    set( CPACK_GENERATOR "RPM" )
    set( OS_NAME "" )
    set( CPACK_RPM_PACKAGE_RELOCATABLE ON )
    set( CPACK_RPM_PACKAGE_AUTOREQ 0 )
    set( CPACK_RPM_PACKAGE_VERSION "\${CPACK_PACKAGE_VERSION}" )
    set( CPACK_RPM_PACKAGE_NAME "\${PROJECT_NAME}" )
    set( CPACK_RPM_PACKAGE_VENDOR "Fabrice Poupon <fabrice.poupon@cea.fr>" )
    set( CPACK_RPM_PACKAGE_DESCRIPTION "Kokkos debian package" )
  else ( LSB_RELEASE_ID_SHORT STREQUAL "CentOS" )
    set( CPACK_GENERATOR "STGZ" )
    set( OS_NAME "_\${LSB_RELEASE_ID_SHORT}" )
  endif ( LSB_RELEASE_ID_SHORT STREQUAL "CentOS" )
endif( LSB_RELEASE_ID_SHORT STREQUAL "Ubuntu" )

set( CPACK_PACKAGE_FILE_NAME
     "\${PROJECT_NAME}_\${\${PROJECT_NAME}_VERSION}\${OS_NAME}_\${outArch}" )

include( CPack )
.
w
EOF

# Modification of the file kokkos/cmake/kokkos_build.cmake

sed -i "s/lib\/CMake/lib\/cmake/" kokkos/cmake/kokkos_build.cmake
sed -i "s/ARCHIVE\ DESTINATION\ .*\/lib/ARCHIVE\ DESTINATION\ lib/" kokkos/cmake/kokkos_build.cmake
sed -i "s/LIBRARY\ DESTINATION\ .*\/lib/LIBRARY\ DESTINATION\ lib/" kokkos/cmake/kokkos_build.cmake
sed -i "s/RUNTIME\ DESTINATION\ .*\/bin/RUNTIME\ DESTINATION\ bin/" kokkos/cmake/kokkos_build.cmake
sed -i "s/\${INSTALL_CMAKE_DIR\}/\${DEF_INSTALL_CMAKE_DIR\}/" kokkos/cmake/kokkos_build.cmake
sed -i "s/REL_INCLUDE_DIR \"\${DEF_INSTALL_CMAKE/REL_INCLUDE_DIR \"\${INSTALL_CMAKE/" kokkos/cmake/kokkos_build.cmake

ed -s kokkos/cmake/kokkos_build.cmake <<EOF
/Standalone
+1
s/include\"/include\/kokkos\"/
.
w
EOF

# Compilation and creation of the Debian package

mkdir build-kokkos
cd build-kokkos
cmake -DCMAKE_BUILD_TYPE=Release -DKokkos_ENABLE_OpenMP=ON -DKOKKOS_ENABLE_OPENMP=ON ../kokkos
make -j${NCPU} package
cd ..

