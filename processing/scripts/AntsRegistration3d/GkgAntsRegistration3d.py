import os
import sys
import glob
import optparse
import math

scriptDirectory = os.path.dirname( os.path.abspath( __file__) )
gkgSharePath = os.path.dirname( scriptDirectory )
sys.path.insert( 0, os.path.join( gkgSharePath, 'python' ) )

import gkg


################################################################################
################################################################################
# various functions
################################################################################
################################################################################

################################################################################
# removing .minf
################################################################################

def removeMinf( fileNameIma ):

  fileNameMinf = fileNameIma + '.minf'
  if ( os.path.exists( fileNameMinf ) ):
  
    os.remove( fileNameMinf )


################################################################################
# removing an entire directory
################################################################################

def rm( *args ):

  sources = []
  for pattern in args:
  
    sources += glob.glob( pattern )
    
  sys.stdout.flush()
  for path in sources:
  
    if not os.path.islink( path ):

      # this test avoids an error on dead symlinks
      os.chmod( path, 0777 )

    if os.path.isdir( path ) and not os.path.islink( path ):

      rm( os.path.join( path, '*' ) )
      os.rmdir( path )

    else:
      os.remove( path )


################################################################################
# trigonometry
################################################################################

def vectorNorm( v ):

  return math.sqrt( v[ 0 ] * v[ 0 ] + v[ 1 ] * v[ 1 ] + v[ 2 ] * v[ 2 ] )


def vectorCross( u, v ):

  return [ u[ 1 ] * v[ 2 ] - u[ 2 ] * v[ 1 ],
           -u[ 2 ] * v[ 0 ] + u[ 0 ] * v[ 2 ],
           u[ 0 ] * v[ 1 ] - u[ 1 ] * v[ 0 ] ]


def vectorDot( u, v ):

  return u[ 0 ] * v[ 0 ] + u[ 1 ] * v[ 1 ] + u[ 2 ] * v[ 2 ]
           

def normalizedVector( v, norm ):

  return [ v[ 0 ] / norm, v[ 1 ] / norm, v[ 2 ] / norm ]


def getXYZScalingsAndShearingsAndTrieder( rotationAndScalingAndShearings ):


  u = [ rotationAndScalingAndShearings[ 0 ][ 0 ],
        rotationAndScalingAndShearings[ 1 ][ 0 ],
        rotationAndScalingAndShearings[ 2 ][ 0 ] ]
  
  v = [ rotationAndScalingAndShearings[ 0 ][ 1 ],
        rotationAndScalingAndShearings[ 1 ][ 1 ],
        rotationAndScalingAndShearings[ 2 ][ 1 ] ]
  
  w = [ rotationAndScalingAndShearings[ 0 ][ 2 ],
        rotationAndScalingAndShearings[ 1 ][ 2 ],
        rotationAndScalingAndShearings[ 2 ][ 2 ] ]

  scalings = [ 1.0, 1.0, 1.0 ]
    
  scalings[ 2 ] = vectorNorm( w )
  unitW = normalizedVector( w, scalings[ 2 ] )
  
  unitU = vectorCross( v, unitW )
  scalings[ 1 ] = vectorNorm( unitU )
  unitU = normalizedVector( unitU, scalings[ 1 ] )
  
  scalings[ 0 ] = vectorDot( u, unitU )
  unitV = vectorCross( unitW, unitU )
  
  shearings = [ 0.0, 0.0, 0.0 ]
  shearings[ 0 ] = vectorDot( u, unitV ) / scalings[ 0 ]
  shearings[ 2 ] = vectorDot( v, unitW ) / scalings[ 1 ]
  shearings[ 1 ] = vectorDot( u, unitW ) / scalings[ 0 ] \
                   - shearings[ 0 ] - shearings[ 2 ]
  
  rotation = [ [ unitU[ 0 ], unitV[ 0 ], unitW[ 0 ] ],
               [ unitU[ 1 ], unitV[ 1 ], unitW[ 1 ] ],
               [ unitU[ 2 ], unitV[ 2 ], unitW[ 2 ] ] ]
               
  return scalings, shearings, rotation


################################################################################
################################################################################
# main function
#
#  GkgAnstRegistration3d
#    -reference <*.ima>
#    -floating <*.ima>
#    -t  <rigid/affine/affine_and_diffeomorphic>
#    -o <float_to_reference_transformation.trm,.ima>
#    -oi <reference_to_float_transformation.trm,.ima>
#    -intermediate <True/False>
#    -verbose <True/False>
#
################################################################################
################################################################################


parser = optparse.OptionParser()
parser.add_option( '-r', '--reference',
                   dest = 'fileNameReferenceVolume',
                   help = 'reference volume filename',
                   metavar = 'FILE' )
parser.add_option( '-f', '--floating',
                   dest = 'fileNameFloatingVolume',
                   help = 'floating volume filename',
                   metavar = 'FILE' )
parser.add_option( '-d', '--outputDirectTransform3d',
                   dest = 'fileNameDirectTransform3d',
                   default = 'direct_transform3d',
                   help = 'Output direct 3D transform (default: %default)',
                   metavar = 'FILE' )
parser.add_option( '-i', '--outputInverseTransform3d',
                   dest = 'fileNameInverseTransform3d',
                   default = 'inverse_transform3d',
                   help = 'Output inverse 3D transform (default: %default)',
                   metavar = 'FILE' )
parser.add_option( '-t', '--type',
                   dest = 'transformationType',
                   default = 'affine_and_diffeomorphic',
                   help = 'Transformation type: rigid, affine, '
                          'affine_and_diffeomorphic, rigid_and_diffeomorphic, '
                          'diffeomorphic (default: %default)' )
parser.add_option( '-R', '--transformedReference',
                   dest = 'fileNameTransformedReferenceVolume',
                   help = 'Transformed (in floating frame) reference volume '
                          'filename',
                   metavar = 'FILE' )
parser.add_option( '-F', '--transformedFloating',
                   dest = 'fileNameTransformedFloatingVolume',
                   help = 'Transformed (in reference frame) floating volume '
                          'filename',
                   metavar = 'FILE' )
parser.add_option( '-I', '--intermediate',
                   dest = 'intermediate',
                   action = 'store_true',
                   default = False,
                   help = 'Store intermediate result(s) (default: %default)' )
parser.add_option( '-a', '--furtherANTsOptions',
                   dest = 'furtherANTsOptions',
                   default = '',
                   help = 'Applying further ANTs options (default: \'\')' )
parser.add_option( '-v', '--verbose',
                   dest = 'verbose',
                   action = 'store_true',
                   default = False,
                   help = 'Show as much information as possible '
                          '(default: %default)' )


( options, args ) = parser.parse_args()


################################################################################
# checking that input files exist
################################################################################

if ( options.fileNameReferenceVolume is None ):

  raise Exception( 'a reference volume is required' )

else:

  if ( not os.path.exists( options.fileNameReferenceVolume ) ):

    raise Exception( 'cannot read reference volume' )


if ( options.fileNameFloatingVolume is None ):

  raise Exception( 'a floating volume is required' )

else:

  if ( not os.path.exists( options.fileNameFloatingVolume ) ):

    raise Exception( 'cannot read floating volume' )



################################################################################
# creating an intermediate directory
################################################################################

intermediateDirectory = os.path.join(
                          os.path.dirname( 
                            os.path.abspath(
                                          options.fileNameDirectTransform3d ) ),
                          'intermediate' )

if ( options.verbose ):

  print 'reference volume :', options.fileNameReferenceVolume
  print 'floating volume :', options.fileNameFloatingVolume
  print 'direct 3D transform :', options.fileNameDirectTransform3d
  print 'inverse 3D transform :', options.fileNameInverseTransform3d
  print '3D transform type : ', options.transformationType
  print 'keep intermediate results : ', options.intermediate
  print 'intermediate directory : ', intermediateDirectory


 
if ( not os.path.exists( intermediateDirectory ) ):

  os.mkdir( intermediateDirectory )
  if ( not os.path.exists( intermediateDirectory ) ):

    raise Exception( 'cannot create directory ' + intermediateDirectory )


################################################################################
# copying reference and floating to intermediate directory
################################################################################

fileNameReferenceImaVolume = os.path.join( intermediateDirectory,
                                           'reference.ima' )
gkg.executeCommand( { 'algorithm' : 'DiskFormatConverter',
                      'parameters' : \
                        { 'fileNameIn' : options.fileNameReferenceVolume,
                          'fileNameOut' : fileNameReferenceImaVolume,
                          'inputFormat' : '',
                          'outputFormat' : 'gis',
                          'ascii' : False,
                          'verbose' : options.verbose
                        },
                      'verbose' : options.verbose
                    } )
removeMinf( fileNameReferenceImaVolume )

fileNameFloatingImaVolume = os.path.join( intermediateDirectory,
                                          'floating.ima' )
gkg.executeCommand( { 'algorithm' : 'DiskFormatConverter',
                      'parameters' : \
                        { 'fileNameIn' : options.fileNameFloatingVolume,
                          'fileNameOut' : fileNameFloatingImaVolume,
                          'inputFormat' : '',
                          'outputFormat' : 'gis',
                          'ascii' : False,
                          'verbose' : options.verbose
                        },
                      'verbose' : options.verbose
                    } )
removeMinf( fileNameFloatingImaVolume )


################################################################################
# converting GIS to NIFTI format
################################################################################

fileNameReferenceNiiVolume = os.path.join( intermediateDirectory,
                                           'reference.nii' ) 
fileNameFloatingNiiVolume = os.path.join( intermediateDirectory,
                                          'floating.nii' )


gkg.executeCommand( { 'algorithm' : 'Gis2NiftiConverter',
                      'parameters' : \
                        { 'fileNameIn' : fileNameReferenceImaVolume,
                          'fileNameOut' : fileNameReferenceNiiVolume,
                          'verbose' : options.verbose
                        },
                      'verbose' : options.verbose
                    } )

gkg.executeCommand( { 'algorithm' : 'Gis2NiftiConverter',
                      'parameters' : \
                        { 'fileNameIn' : fileNameFloatingImaVolume,
                          'fileNameOut' : fileNameFloatingNiiVolume,
                          'verbose' : options.verbose
                        },
                      'verbose' : options.verbose
                    } )


################################################################################
# converting registration type to ANTs format
################################################################################

registrationType = ''
if ( options.transformationType == 'rigid' ):

  registrationType = 'r'

elif ( options.transformationType == 'affine' ):

  registrationType = 'a'
  
elif ( options.transformationType == 'affine_and_diffeomorphic' ):

  registrationType = 's'
  
elif ( options.transformationType == 'rigid_and_diffeomorphic' ):

  registrationType = 'sr'
  
elif ( options.transformationType == 'diffeomorphic' ):

  registrationType = 'so'
  
else:

  raise Exception( 'bad transformation type; must be one of '
                   'rigid, affine, affine_and_diffeomorphic, '
                   'rigid_and_diffeomorphic, diffeomorphic' )
  



################################################################################
# performing ANTs registration
################################################################################

fileNameAntsTransform = os.path.join( intermediateDirectory,
                                      'ants_transform' )

command = 'antsRegistrationSyNQuick.sh ' + \
          ' -d 3 ' + \
          ' -f ' + fileNameReferenceNiiVolume + \
          ' -m ' + fileNameFloatingNiiVolume + \
          ' -t ' + registrationType + \
          ' -o ' + fileNameAntsTransform + \
          ' ' + options.furtherANTsOptions

if ( options.verbose ):

  print command
  
os.system( command )

fileNameTxtAntsTransform = os.path.join( intermediateDirectory,
                                         'ants_transform.txt' )
command = 'ConvertTransformFile ' + \
          ' 3 ' + \
          fileNameAntsTransform + '0GenericAffine.mat ' + \
          fileNameTxtAntsTransform

if ( options.verbose ):

  print command
  
os.system( command )


################################################################################
# converting ANTs rigid or affine registration to Anatomist frame
################################################################################

floatingTransformations = ()
referenceTransformations = ()

if ( options.transformationType != 'diffeomorphic' ):

  # extracting information from Ants txt file
  f = open( fileNameTxtAntsTransform, 'r' )
  lines = f.readlines()
  f.close()

  line3 = lines[ 3 ].split()
  line4 =  lines[ 4 ].split()


  T = [ float( line3[ 10 ] ),
        float( line3[ 11 ] ),
        float( line3[ 12 ] ) ]
 
  C = [ float( line4[ 1 ] ),
        float( line4[ 2 ] ),
        float( line4[ 3 ] ) ]
 

  R = [ [ float( line3[ 1 ] ), float( line3[ 2 ] ), float( line3[ 3 ] ) ],
        [ float( line3[ 4 ] ), float( line3[ 5 ] ), float( line3[ 6 ] ) ],
        [ float( line3[ 7 ] ), float( line3[ 8 ] ), float( line3[ 9 ] ) ] ]

  Tprime = [ 0.0, 0.0, 0.0 ]
  for i in range( 0, 3 ):

     Tprime[ i ] = T[ i ] + C[ i ]
     for j in range( 0, 3 ):
   
       Tprime[ i ] -= R[ i ][ j ] * C[ j ]

  Tprime[ 2 ] *= -1;

  if ( options.verbose ):
  
    scalings, shearings, rotation = getXYZScalingsAndShearingsAndTrieder( R )
    print
    print '======================= R ====================\n'
    print R
    print
    print '======================= T ====================\n'
    print Tprime
    print
    print '===================== scalings ================\n'
    print scalings
    print
    print '==================== shearings ===============\n'
    print shearings
    print
    print '==================== rotation ================\n'
    print rotation
    print

  # saving rigid/affine inverse transformation to Anatomist format
  # ( note that the Ants output is the affine transformation from the reference
  #   to the floating space )
  fileNameTrmInverseTransform3d = options.fileNameInverseTransform3d + '.trm'
  f = open( fileNameTrmInverseTransform3d, 'w' )

  f.write( '%g ' % Tprime[ 0 ] )
  f.write( '%g ' % Tprime[ 1 ] )
  f.write( '%g\n' % Tprime[ 2 ] )

  f.write( '%g ' % R[ 0 ][ 0 ] )
  f.write( '%g ' % R[ 0 ][ 1 ] )
  f.write( '%g\n' % -R[ 0 ][ 2 ] )

  f.write( '%g ' % R[ 1 ][ 0 ] )
  f.write( '%g ' % R[ 1 ][ 1 ] )
  f.write( '%g\n' % -R[ 1 ][ 2 ] )

  f.write( '%g ' % -R[ 2 ][ 0 ] )
  f.write( '%g ' % -R[ 2 ][ 1 ] )
  f.write( '%g\n' % R[ 2 ][ 2 ] )

  f.close()

  # inverting Anatomist transformation
  fileNameTrmDirectTransform3d = options.fileNameDirectTransform3d + '.trm'
  gkg.executeCommand( { 'algorithm' : 'Transform3dInverter',
                        'parameters' : \
                          { 'fileNameIn' : fileNameTrmInverseTransform3d,
                            'fileNameOut' : fileNameTrmDirectTransform3d,
                            'verbose' : options.verbose
                          },
                        'verbose' : options.verbose
                      } )
                      
  floatingTransformations = ( fileNameTrmDirectTransform3d )
  referenceTransformations = ( fileNameTrmInverseTransform3d )


################################################################################
# converting ANTs diffeomorphic registration to Anatomist frame and GIS format
################################################################################

if ( ( options.transformationType == 'affine_and_diffeomorphic' ) or
     ( options.transformationType == 'rigid_and_diffeomorphic' ) or
     ( options.transformationType == 'diffeomorphic' ) ):

  # converting NIFTI direct diffeomorphism to GIS
  fileNameDirectNiiDiffeomorphism = fileNameAntsTransform + \
                                     '1InverseWarp.nii.gz'
  fileNameDirectImaDiffeomorphism = os.path.join( intermediateDirectory,
                                                  'diffeomorphism_direct.ima')
  gkg.executeCommand( { 'algorithm' : 'Nifti2GisConverter',
                        'parameters' : \
                          { 'fileNameIn' : fileNameDirectNiiDiffeomorphism,
                            'fileNameOut' : fileNameDirectImaDiffeomorphism,
                            'verbose' : options.verbose
                          },
                        'verbose' : options.verbose
                      } )
  removeMinf( fileNameDirectImaDiffeomorphism )


  # converting NIFTI iinverse diffeomorphism to GIS
  fileNameInverseNiiDiffeomorphism = fileNameAntsTransform + \
                                     '1Warp.nii.gz' 
  fileNameInverseImaDiffeomorphism = os.path.join( intermediateDirectory,
                                                  'diffeomorphism_inverse.ima' )
  gkg.executeCommand( { 'algorithm' : 'Nifti2GisConverter',
                        'parameters' : \
                          { 'fileNameIn' : fileNameInverseNiiDiffeomorphism,
                            'fileNameOut' : fileNameInverseImaDiffeomorphism,
                            'verbose' : options.verbose
                          },
                        'verbose' : options.verbose
                      } )
  removeMinf( fileNameInverseImaDiffeomorphism )


  # extracting dimensions of diffeomorphism
  f = open( fileNameDirectImaDiffeomorphism[ : -3 ] + 'dim', 'r' )
  lines = f.readlines()
  f.close()
  line1 = lines[ 0 ].split()
  sizeX = int( line1[ 0 ] )
  sizeY = int( line1[ 1 ] )
  sizeZ = int( line1[ 2 ] )

  # extractiong X coordinates of direct diffeomorphism
  fileNameDirectImaDiffeomorphismX = os.path.join( intermediateDirectory,
                                                 'diffeomorphism_direct_x.ima' )
  gkg.executeCommand( { 'algorithm' : 'SubVolume',
                        'parameters' : \
                          { 'fileNameIn' : fileNameDirectImaDiffeomorphism,
                            'fileNameOut' : fileNameDirectImaDiffeomorphismX,
                            'sx' : 0,
                            'sy' : 0,
                            'sz' : 0,
                            'st' : 0,
                            'ex' : sizeX - 1,
                            'ey' : sizeY - 1,
                            'ez' : sizeZ - 1,
                            'et' : 0,
                            'xIndices' : (),
                            'yIndices' : (),
                            'zIndices' : (),
                            'tIndices' : (),
                            'ascii' : False,
                            'format' : 'gis',
                            'verbose' : options.verbose
                          },
                        'verbose' : options.verbose
                      } )
  removeMinf( fileNameDirectImaDiffeomorphismX )

  # extractiong Y coordinates of direct diffeomorphism
  fileNameDirectImaDiffeomorphismY = os.path.join( intermediateDirectory,
                                                 'diffeomorphism_direct_y.ima' )
  gkg.executeCommand( { 'algorithm' : 'SubVolume',
                        'parameters' : \
                          { 'fileNameIn' : fileNameDirectImaDiffeomorphism,
                            'fileNameOut' : fileNameDirectImaDiffeomorphismY,
                            'sx' : 0,
                            'sy' : 0,
                            'sz' : 0,
                            'st' : 1,
                            'ex' : sizeX - 1,
                            'ey' : sizeY - 1,
                            'ez' : sizeZ - 1,
                            'et' : 1,
                            'xIndices' : (),
                            'yIndices' : (),
                            'zIndices' : (),
                            'tIndices' : (),
                            'ascii' : False,
                            'format' : 'gis',
                            'verbose' : options.verbose
                          },
                        'verbose' : options.verbose
                      } )
  removeMinf( fileNameDirectImaDiffeomorphismY )

  # extractiong Z coordinates of direct diffeomorphism
  fileNameDirectImaDiffeomorphismZ = os.path.join( intermediateDirectory,
                                                 'diffeomorphism_direct_z.ima' )
  gkg.executeCommand( { 'algorithm' : 'SubVolume',
                        'parameters' : \
                          { 'fileNameIn' : fileNameDirectImaDiffeomorphism,
                            'fileNameOut' : fileNameDirectImaDiffeomorphismZ,
                            'sx' : 0,
                            'sy' : 0,
                            'sz' : 0,
                            'st' : 2,
                            'ex' : sizeX - 1,
                            'ey' : sizeY - 1,
                            'ez' : sizeZ - 1,
                            'et' : 2,
                            'xIndices' : (),
                            'yIndices' : (),
                            'zIndices' : (),
                            'tIndices' : (),
                            'ascii' : False,
                            'format' : 'gis',
                            'verbose' : options.verbose
                          },
                        'verbose' : options.verbose
                      } )
  removeMinf( fileNameDirectImaDiffeomorphismZ )

  # extractiong X coordinates of inverse diffeomorphism
  fileNameInverseImaDiffeomorphismX = os.path.join( intermediateDirectory,
                                                'diffeomorphism_inverse_x.ima' )
  gkg.executeCommand( { 'algorithm' : 'SubVolume',
                        'parameters' : \
                          { 'fileNameIn' : fileNameInverseImaDiffeomorphism,
                            'fileNameOut' : fileNameInverseImaDiffeomorphismX,
                            'sx' : 0,
                            'sy' : 0,
                            'sz' : 0,
                            'st' : 0,
                            'ex' : sizeX - 1,
                            'ey' : sizeY - 1,
                            'ez' : sizeZ - 1,
                            'et' : 0,
                            'xIndices' : (),
                            'yIndices' : (),
                            'zIndices' : (),
                            'tIndices' : (),
                            'ascii' : False,
                            'format' : 'gis',
                            'verbose' : options.verbose
                          },
                        'verbose' : options.verbose
                      } )
  removeMinf( fileNameInverseImaDiffeomorphismX )

  # extractiong Y coordinates of inverse diffeomorphism
  fileNameInverseImaDiffeomorphismY = os.path.join( intermediateDirectory,
                                                'diffeomorphism_inverse_y.ima' )
  gkg.executeCommand( { 'algorithm' : 'SubVolume',
                        'parameters' : \
                          { 'fileNameIn' : fileNameInverseImaDiffeomorphism,
                            'fileNameOut' : fileNameInverseImaDiffeomorphismY,
                            'sx' : 0,
                            'sy' : 0,
                            'sz' : 0,
                            'st' : 1,
                            'ex' : sizeX - 1,
                            'ey' : sizeY - 1,
                            'ez' : sizeZ - 1,
                            'et' : 1,
                            'xIndices' : (),
                            'yIndices' : (),
                            'zIndices' : (),
                            'tIndices' : (),
                            'ascii' : False,
                            'format' : 'gis',
                            'verbose' : options.verbose
                          },
                        'verbose' : options.verbose
                      } )
  removeMinf( fileNameInverseImaDiffeomorphismY )

  # extractiong Z coordinates of inverse diffeomorphism
  fileNameInverseImaDiffeomorphismZ = os.path.join( intermediateDirectory,
                                                'diffeomorphism_inverse_z.ima' )
  gkg.executeCommand( { 'algorithm' : 'SubVolume',
                        'parameters' : \
                          { 'fileNameIn' : fileNameInverseImaDiffeomorphism,
                            'fileNameOut' : fileNameInverseImaDiffeomorphismZ,
                            'sx' : 0,
                            'sy' : 0,
                            'sz' : 0,
                            'st' : 2,
                            'ex' : sizeX - 1,
                            'ey' : sizeY - 1,
                            'ez' : sizeZ - 1,
                            'et' : 2,
                            'xIndices' : (),
                            'yIndices' : (),
                            'zIndices' : (),
                            'tIndices' : (),
                            'ascii' : False,
                            'format' : 'gis',
                            'verbose' : options.verbose
                          },
                        'verbose' : options.verbose
                      } )
  removeMinf( fileNameInverseImaDiffeomorphismZ )

  # converting X coordinates of direct diffeomorphism to float
  fileNameDirectImaDiffeomorphismXFloat = os.path.join( intermediateDirectory,
                                           'diffeomorphism_direct_x_float.ima' )
  gkg.executeCommand( { 'algorithm' : 'Combiner',
                        'parameters' : \
                          { 'fileNameIns' : 
                                           ( fileNameDirectImaDiffeomorphismX ),
                            'fileNameOut' :
                                         fileNameDirectImaDiffeomorphismXFloat,
                            'functor1s' : (),
                            'functor2s' : (),
                            'numerator1s' : ( 1.0, 0.0 ),
                            'denominator1s' : ( 1.0, 1.0 ),
                            'numerator2s' : ( 1.0 ),
                            'denominator2s' : ( 1.0 ),
                            'operators' : ( '+' ),
                            'fileNameMask' : '',
                            'mode' : 'gt',
                            'threshold1' : 0.0,
                            'threshold2' : 0.0,
                            'background' : 0.0,
                            'outputType' : 'float',
                            'ascii' : False,
                            'format' : 'gis',
                            'verbose' : options.verbose
                          },
                        'verbose' : options.verbose
                      } )
  removeMinf( fileNameDirectImaDiffeomorphismXFloat )

  # converting Y coordinates of direct diffeomorphism to float
  fileNameDirectImaDiffeomorphismYFloat = os.path.join( intermediateDirectory,
                                           'diffeomorphism_direct_y_float.ima' )
  gkg.executeCommand( { 'algorithm' : 'Combiner',
                        'parameters' : \
                          { 'fileNameIns' :
                                           ( fileNameDirectImaDiffeomorphismY ),
                            'fileNameOut' : 
                                          fileNameDirectImaDiffeomorphismYFloat,
                            'functor1s' : (),
                            'functor2s' : (),
                            'numerator1s' : ( 1.0, 0.0 ),
                            'denominator1s' : ( 1.0, 1.0 ),
                            'numerator2s' : ( 1.0 ),
                            'denominator2s' : ( 1.0 ),
                            'operators' : ( '+' ),
                            'fileNameMask' : '',
                            'mode' : 'gt',
                            'threshold1' : 0.0,
                            'threshold2' : 0.0,
                            'background' : 0.0,
                            'outputType' : 'float',
                            'ascii' : False,
                            'format' : 'gis',
                            'verbose' : options.verbose
                          },
                        'verbose' : options.verbose
                      } )
  removeMinf( fileNameDirectImaDiffeomorphismYFloat )

  # converting Z coordinates of direct diffeomorphism to float
  fileNameDirectImaDiffeomorphismZFloat = os.path.join( intermediateDirectory,
                                           'diffeomorphism_direct_z_float.ima' )
  gkg.executeCommand( { 'algorithm' : 'Combiner',
                        'parameters' : \
                          { 'fileNameIns' :
                                           ( fileNameDirectImaDiffeomorphismZ ),
                            'fileNameOut' :
                                          fileNameDirectImaDiffeomorphismZFloat,
                            'functor1s' : (),
                            'functor2s' : (),
                            'numerator1s' : ( -1.0, 0.0 ),
                            'denominator1s' : ( 1.0, 1.0 ),
                            'numerator2s' : ( 1.0 ),
                            'denominator2s' : ( 1.0 ),
                            'operators' : ( '+' ),
                            'fileNameMask' : '',
                            'mode' : 'gt',
                            'threshold1' : 0.0,
                            'threshold2' : 0.0,
                            'background' : 0.0,
                            'outputType' : 'float',
                            'ascii' : False,
                            'format' : 'gis',
                            'verbose' : options.verbose
                          },
                        'verbose' : options.verbose
                      } )
  removeMinf( fileNameDirectImaDiffeomorphismZFloat )

  # converting X coordinates of inverse diffeomorphism to float
  fileNameInverseImaDiffeomorphismXFloat = os.path.join( intermediateDirectory,
                                          'diffeomorphism_inverse_x_float.ima' )
  gkg.executeCommand( { 'algorithm' : 'Combiner',
                        'parameters' : \
                          { 'fileNameIns' :
                                          ( fileNameInverseImaDiffeomorphismX ),
                            'fileNameOut' :
                                         fileNameInverseImaDiffeomorphismXFloat,
                            'functor1s' : (),
                            'functor2s' : (),
                            'numerator1s' : ( 1.0, 0.0 ),
                            'denominator1s' : ( 1.0, 1.0 ),
                            'numerator2s' : ( 1.0 ),
                            'denominator2s' : ( 1.0 ),
                            'operators' : ( '+' ),
                            'fileNameMask' : '',
                            'mode' : 'gt',
                            'threshold1' : 0.0,
                            'threshold2' : 0.0,
                            'background' : 0.0,
                            'outputType' : 'float',
                            'ascii' : False,
                            'format' : 'gis',
                            'verbose' : options.verbose
                          },
                        'verbose' : options.verbose
                      } )
  removeMinf( fileNameInverseImaDiffeomorphismXFloat )

  # converting Y coordinates of inverse diffeomorphism to float
  fileNameInverseImaDiffeomorphismYFloat = os.path.join( intermediateDirectory,
                                          'diffeomorphism_inverse_y_float.ima' )
  gkg.executeCommand( { 'algorithm' : 'Combiner',
                        'parameters' : \
                          { 'fileNameIns' :
                                          ( fileNameInverseImaDiffeomorphismY ),
                            'fileNameOut' :
                                         fileNameInverseImaDiffeomorphismYFloat,
                            'functor1s' : (),
                            'functor2s' : (),
                            'numerator1s' : ( 1.0, 0.0 ),
                            'denominator1s' : ( 1.0, 1.0 ),
                            'numerator2s' : ( 1.0 ),
                            'denominator2s' : ( 1.0 ),
                            'operators' : ( '+' ),
                            'fileNameMask' : '',
                            'mode' : 'gt',
                            'threshold1' : 0.0,
                            'threshold2' : 0.0,
                            'background' : 0.0,
                            'outputType' : 'float',
                            'ascii' : False,
                            'format' : 'gis',
                            'verbose' : options.verbose
                          },
                        'verbose' : options.verbose
                      } )
  removeMinf( fileNameInverseImaDiffeomorphismYFloat )

  # converting Z coordinates of inverse diffeomorphism to float
  fileNameInverseImaDiffeomorphismZFloat = os.path.join( intermediateDirectory,
                                          'diffeomorphism_inverse_z_float.ima' )
  gkg.executeCommand( { 'algorithm' : 'Combiner',
                        'parameters' : \
                          { 'fileNameIns' :
                                          ( fileNameInverseImaDiffeomorphismZ ),
                            'fileNameOut' :
                                         fileNameInverseImaDiffeomorphismZFloat,
                            'functor1s' : (),
                            'functor2s' : (),
                            'numerator1s' : ( -1.0, 0.0 ),
                            'denominator1s' : ( 1.0, 1.0 ),
                            'numerator2s' : ( 1.0 ),
                            'denominator2s' : ( 1.0 ),
                            'operators' : ( '+' ),
                            'fileNameMask' : '',
                            'mode' : 'gt',
                            'threshold1' : 0.0,
                            'threshold2' : 0.0,
                            'background' : 0.0,
                            'outputType' : 'float',
                            'ascii' : False,
                            'format' : 'gis',
                            'verbose' : options.verbose
                          },
                        'verbose' : options.verbose
                      } )
  removeMinf( fileNameInverseImaDiffeomorphismZFloat )

  # concatenating coordinates and creating direct diffeomorphism
  fileNameDiffeoDirectTransform3d = options.fileNameDirectTransform3d + '.ima'
  gkg.executeCommand( { 'algorithm' : 'Cat',
                        'parameters' : \
                        { 'inputNames' :
                                     ( fileNameDirectImaDiffeomorphismXFloat,
                                       fileNameDirectImaDiffeomorphismYFloat,
                                       fileNameDirectImaDiffeomorphismZFloat,
                                       fileNameInverseImaDiffeomorphismXFloat,
                                       fileNameInverseImaDiffeomorphismYFloat,
                                       fileNameInverseImaDiffeomorphismZFloat ),
                          'outputName' : fileNameDiffeoDirectTransform3d,
                          'type' : 't',
                          'ascii' : False,
                          'format' : 'gis',
                          'verbose' : options.verbose
                        },
                      'verbose' : options.verbose
                    } )
  removeMinf( fileNameDiffeoDirectTransform3d )


  # concatenating coordinates and creating inverse diffeomorphism
  fileNameDiffeoInverseTransform3d = options.fileNameInverseTransform3d + '.ima'
  gkg.executeCommand( { 'algorithm' : 'Cat',
                        'parameters' : \
                        { 'inputNames' :
                                     ( fileNameInverseImaDiffeomorphismXFloat,
                                       fileNameInverseImaDiffeomorphismYFloat,
                                       fileNameInverseImaDiffeomorphismZFloat,
                                       fileNameDirectImaDiffeomorphismXFloat,
                                       fileNameDirectImaDiffeomorphismYFloat,
                                       fileNameDirectImaDiffeomorphismZFloat ),
                          'outputName' : fileNameDiffeoInverseTransform3d,
                          'type' : 't',
                          'ascii' : False,
                          'format' : 'gis',
                          'verbose' : options.verbose
                        },
                      'verbose' : options.verbose
                    } )
  removeMinf( fileNameDiffeoInverseTransform3d )
  
  floatingTransformations = ( fileNameTrmDirectTransform3d,
                              fileNameDiffeoDirectTransform3d )
  referenceTransformations = ( fileNameTrmInverseTransform3d,
                               fileNameDiffeoInverseTransform3d )

if ( options.fileNameTransformedFloatingVolume is not None ):

  gkg.executeCommand( { 'algorithm' : 'Resampling3d',
                        'parameters' : \
                          { 'fileNameReference' : fileNameFloatingImaVolume,
                            'fileNameTemplate' : fileNameReferenceImaVolume,
                            'fileNameTransforms' :  floatingTransformations,
                            'fileNameOut' : 
                                      options.fileNameTransformedFloatingVolume,
                            'order' : 3,
                            'outBackground' : 0.0,
                            'ascii' : False,
                            'format' : 'gis',
                            'verbose' : True
                          },
                        'verbose' : True
                      } )


if ( options.fileNameTransformedReferenceVolume is not None ):

  gkg.executeCommand( { 'algorithm' : 'Resampling3d',
                        'parameters' : \
                          { 'fileNameReference' : fileNameReferenceImaVolume,
                            'fileNameTemplate' : fileNameFloatingImaVolume,
                            'fileNameTransforms' :  referenceTransformations,
                            'fileNameOut' : 
                                     options.fileNameTransformedReferenceVolume,
                            'order' : 3,
                            'outBackground' : 0.0,
                            'ascii' : False,
                            'format' : 'gis',
                            'verbose' : True
                          },
                        'verbose' : True
                      } )

# last removing intermediate directory if not required
if options.intermediate == False:

  rm( intermediateDirectory )

