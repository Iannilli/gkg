#ifndef _gkg_processing_algobase_AverageFilter_i_h_
#define _gkg_processing_algobase_AverageFilter_i_h_


#include <gkg-processing-algobase/AverageFilter.h>
#include <gkg-processing-algobase/FilterFunction_i.h>
#include <gkg-processing-algobase/Converter_i.h>
#include <gkg-core-io/TypeOf.h>
#include <gkg-processing-container/Volume_i.h>
#include <gkg-core-exception/Exception.h>


//
// class AverageFilter< IN, OUT >
//

template < class IN, class OUT >
inline
void gkg::AverageFilter< IN, OUT >::filter( const IN& in, OUT& out ) const
{

  gkg::Converter< IN, OUT > converter;
  converter.convert( in, out );

}


//
// class AverageFilter< std::vector< IN >, OUT >
//

template < class IN, class OUT >
inline
void gkg::AverageFilter< std::vector< IN >, OUT >::filter(
                                   const std::vector< IN >& in, OUT& out ) const
{

  try
  {

    if ( in.empty() )
    {

      throw std::runtime_error( "empty vector" );

    }

    typename std::vector< IN >::const_iterator i = in.begin(),
                                               ie = in.end();
    double sum = 0.0;
    while ( i != ie )
    {

      sum += ( double )*i;
      ++ i;

    }
    sum /= ( double )in.size();
    out = ( OUT )( sum );

  }
  GKG_CATCH( "template < class IN, class OUT > "
             "inline "
             "void gkg::AverageFilter< std::vector< IN >, OUT >::filter( "
             "const std::vector< IN >& in, OUT& out ) const" );

}


//
// class AverageFilter< std::vector< std::complex< IN > >, std::complex< OUT > >
//

template < class IN, class OUT >
inline
void gkg::AverageFilter< std::vector< std::complex< IN > >,
                         std::complex< OUT > >::filter(
                                    const std::vector< std::complex< IN > >& in,
                                    std::complex< OUT >& out ) const
{

  try
  {

    if ( in.empty() )
    {

      throw std::runtime_error( "empty vector" );

    }

    typename std::vector< std::complex< IN > >::const_iterator i = in.begin(),
                                                               ie = in.end();
    std::complex< double > sum = 0.0;
    while ( i != ie )
    {

      sum += std::complex< double >( std::real( *i ), std::imag( *i ) );
      ++ i;

    }
    sum /= ( double )in.size();
    out = std::complex< OUT >( std::real( sum ), std::imag( sum ) );

  }
  GKG_CATCH( "template < class IN, class OUT > "
             "inline void "
             "gkg::AverageFilter< std::vector< std::complex< IN > >, "
             "std::complex< OUT > >::filter( "
             "const std::vector< std::complex< IN > >& in, "
             "std::complex< OUT >& out ) const" );

}


//
// class AverageFilter< Vector, OUT >
//

template < class OUT >
inline
void gkg::AverageFilter< gkg::Vector, OUT >::filter(
                                         const gkg::Vector& in, OUT& out ) const
{

  try
  {

    int32_t i = 0,
            ie = in.getSize();
    double sum = 0.0;
    while ( i != ie )
    {

      sum += ( double )in( i );
      ++ i;

    }
    sum /= ( double )in.getSize();
    out = ( OUT )( sum );

  }
  GKG_CATCH( "template < class OUT > "
             "inline "
             "void gkg::AverageFilter< gkg::Vector, OUT >::filter( "
             "const gkg::Vector& in, OUT& out ) const" );

}


//
// class AverageFilter< gkg::CVector, std::complex< OUT > >
//

template < class OUT >
inline
void gkg::AverageFilter< gkg::CVector,
                         std::complex< OUT > >::filter(
                                    const gkg::CVector& in,
                                    std::complex< OUT >& out ) const
{

  try
  {

    int32_t i = 0,
            ie = in.getSize();
    std::complex< double > sum = 0.0;
    while ( i != ie )
    {

      sum += std::complex< double >( std::real( in( i ) ),
                                     std::imag( in ( i ) ) );
      ++ i;

    }
    sum /= ( double )in.getSize();
    out = std::complex< OUT >( std::real( sum ), std::imag( sum ) );

  }
  GKG_CATCH( "template < class OUT > "
             "inline void "
             "gkg::AverageFilter< gkg::CVector, "
             "std::complex< OUT > >::filter( "
             "const gkg::CVector& in, "
             "std::complex< OUT >& out ) const" );

}


//
// class AverageFilter< Volume< IN >, OUT >
//

template < class IN, class OUT >
inline
void gkg::AverageFilter< gkg::Volume< IN >, OUT >::filter(
                                   const gkg::Volume< IN >& in, OUT& out ) const
{

  try
  {

    typename gkg::Volume< IN >::const_iterator i = in.begin(),
                                               ie = in.end();
    double sum = 0.0;
    while ( i != ie )
    {

      sum += ( double )*i;
      ++ i;

    }
    sum /= ( double )in.getSizeXYZT();
    out = ( OUT )( sum );

  }
  GKG_CATCH( "template < class IN, class OUT > "
             "inline "
             "void gkg::AverageFilter< gkg::Volume< IN >, OUT >::filter( "
             "const gkg::Volume< IN >& in, OUT& out ) const" );

}


//
// class AverageFilter< Volume< std::complex< IN > >, std::complex< OUT > >
//

template < class IN, class OUT >
inline
void gkg::AverageFilter< gkg::Volume< std::complex< IN > >,
                         std::complex< OUT > >::filter(
                                    const gkg::Volume< std::complex< IN > >& in,
                                    std::complex< OUT >& out ) const
{

  try
  {

    typename gkg::Volume< std::complex< IN > >::const_iterator i = in.begin(),
                                                               ie = in.end();
    std::complex< double > sum = 0.0;
    while ( i != ie )
    {

      sum += std::complex< double >( std::real( *i ), std::imag( *i ) );
      ++ i;

    }
    sum /= ( double )in.getSizeXYZT();
    out = std::complex< OUT >( std::real( sum ), std::imag( sum ) );

  }
  GKG_CATCH( "template < class IN, class OUT > "
             "inline void "
             "gkg::AverageFilter< gkg::Volume< std::complex< IN > >, "
             "std::complex< OUT > >::filter( "
             "const gkg::Volume< std::complex< IN > >& in, "
             "std::complex< OUT >& out ) const" );

}


#endif
