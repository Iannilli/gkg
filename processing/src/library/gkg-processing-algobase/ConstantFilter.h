#ifndef _gkg_processing_algobase_ConstantFilter_h_
#define _gkg_processing_algobase_ConstantFilter_h_


#include <gkg-processing-algobase/FilterFunction.h>


namespace gkg
{


template < class T > class Volume;


//
// class ConstantFilter< IN, OUT >
//

template < class IN, class OUT >
class ConstantFilter : public FilterFunction< IN, OUT >
{

  public:

    ConstantFilter( const OUT& out );

    void filter( const IN& in, OUT& out ) const;

  protected:

    OUT _out;

};


//
// class ConstantFilter< std::vector< IN >, OUT >
//

template < class IN, class OUT >
class ConstantFilter< std::vector< IN >, OUT > :
                                 public FilterFunction< std::vector< IN >, OUT >
{

  public:

    ConstantFilter( const OUT& out );

    void filter( const std::vector< IN >& in, OUT& out ) const;

  protected:

    OUT _out;

};


//
// class ConstantFilter< Vector, OUT >
//

template < class OUT >
class ConstantFilter< gkg::Vector, OUT > :
                                       public FilterFunction< gkg::Vector, OUT >
{

  public:

    ConstantFilter( const OUT& out );

    void filter( const gkg::Vector& in, OUT& out ) const;

  protected:

    OUT _out;

};


//
// class ConstantFilter< Volume< IN >, OUT >
//

template < class IN, class OUT >
class ConstantFilter< Volume< IN >, OUT > :
                                 public FilterFunction< gkg::Volume< IN >, OUT >
{

  public:

    ConstantFilter( const OUT& out );

    void filter( const Volume< IN >& in, OUT& out ) const;

  protected:

    OUT _out;

};


}


#endif
