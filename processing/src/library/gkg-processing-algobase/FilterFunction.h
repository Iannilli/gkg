#ifndef _gkg_processing_algobase_FilterFunction_h_
#define _gkg_processing_algobase_FilterFunction_h_


#include <gkg-processing-numericalanalysis/Vector.h>
#include <gkg-processing-numericalanalysis/CVector.h>
#include <vector>
#include <complex>


namespace gkg
{


template < class T > class Volume;

//
// class FilterFunction< IN, OUT >
//

template < class IN, class OUT >
class FilterFunction
{

  public:

    virtual ~FilterFunction();

    virtual void filter( const IN& in, OUT& out ) const = 0;

};


//
// class FilterFunction< std::vector< IN >, OUT >
//

template < class IN, class OUT >
class FilterFunction< std::vector< IN >, OUT >
{

  public:

    virtual ~FilterFunction();

    virtual void filter( const std::vector< IN >& in, OUT& out ) const = 0;

};


//
// class FilterFunction< std::vector< std::complex< IN > >,
//                       std::complex< OUT > >
//

template < class IN, class OUT >
class FilterFunction< std::vector< std::complex< IN > >, std::complex< OUT > >
{

  public:

    virtual ~FilterFunction();

    virtual void filter( const std::vector< std::complex< IN > >& in,
                         std::complex< OUT >& out ) const = 0;

};


//
// class FilterFunction< Vector, OUT >
//

template < class OUT >
class FilterFunction< gkg::Vector, OUT >
{

  public:

    virtual ~FilterFunction();

    virtual void filter( const gkg::Vector& in, OUT& out ) const = 0;

};


//
// class FilterFunction< CVector, OUT >
//

template < class OUT >
class FilterFunction< gkg::CVector, std::complex< OUT > >
{

  public:

    virtual ~FilterFunction();

    virtual void filter( const gkg::CVector& in,
                         std::complex< OUT >& out ) const = 0;

};


//
// class FilterFunction< Volume< IN >, OUT >
//

template < class IN, class OUT >
class FilterFunction< Volume< IN >, OUT >
{

  public:

    virtual ~FilterFunction();

    virtual void filter( const Volume< IN >& in, OUT& out ) const = 0;

};


//
// class FilterFunction< Volume< std::complex< IN > >,
//                       std::complex< OUT > >
//

template < class IN, class OUT >
class FilterFunction< Volume< std::complex< IN > >, std::complex< OUT > >
{

  public:

    virtual ~FilterFunction();

    virtual void filter( const Volume< std::complex< IN > >& in,
                         std::complex< OUT >& out ) const = 0;

};


}


#endif


