#ifndef _gkg_processing_algobase_FilterFunction_i_h_
#define _gkg_processing_algobase_FilterFunction_i_h_


#include <gkg-processing-algobase/FilterFunction.h>


//
// class FilterFunction< IN, OUT >
//

template < class IN, class OUT >
inline
gkg::FilterFunction< IN, OUT >::~FilterFunction()
{
}


//
// class FilterFunction< std::vector< IN >, OUT >
//

template < class IN, class OUT >
inline
gkg::FilterFunction< std::vector< IN >, OUT >::~FilterFunction()
{
}


//
// class FilterFunction< std::vector< std::complex< IN > >,
//                       std::complex< OUT > >
//

template < class IN, class OUT >
inline
gkg::FilterFunction< std::vector< std::complex< IN > >,
                     std::complex< OUT > >::~FilterFunction()
{
}


//
// class FilterFunction< Vector, OUT >
//

template < class OUT >
inline
gkg::FilterFunction< gkg::Vector, OUT >::~FilterFunction()
{
}


//
// class FilterFunction< CVector, OUT >
//

template < class OUT >
inline
gkg::FilterFunction< gkg::CVector, std::complex< OUT > >::~FilterFunction()
{
}


//
// class FilterFunction< Volume< IN >, OUT >
//

template < class IN, class OUT >
inline
gkg::FilterFunction< gkg::Volume< IN >, OUT >::~FilterFunction()
{
}


//
// class FilterFunction< Volume< std::complex< IN > >,
//                       std::complex< OUT > >
//

template < class IN, class OUT >
inline
gkg::FilterFunction< gkg::Volume< std::complex< IN > >,
                     std::complex< OUT > >::~FilterFunction()
{
}


#endif

