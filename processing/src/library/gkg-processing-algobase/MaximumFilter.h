#ifndef _gkg_processing_algobase_MaximumFilter_h_
#define _gkg_processing_algobase_MaximumFilter_h_


#include <gkg-processing-algobase/FilterFunction.h>


namespace gkg
{


template < class T > class Volume;
class SparseMatrix;
class Matrix;


//
// class MaximumFilter< IN, OUT >
//

template < class IN, class OUT >
class MaximumFilter : public FilterFunction< IN, OUT >
{

  public:

    void filter( const IN& in, OUT& out ) const;


};


//
// class MaximumFilter< std::vector< IN >, OUT >
//

template < class IN, class OUT >
class MaximumFilter< std::vector< IN >, OUT > :
                                 public FilterFunction< std::vector< IN >, OUT >
{

  public:

    void filter( const std::vector< IN >& in, OUT& out ) const;

};


//
// class MaximumFilter< Vector, OUT >
//

template < class OUT >
class MaximumFilter< gkg::Vector, OUT > : public FilterFunction< Vector, OUT >
{

  public:

    void filter( const gkg::Vector& in, OUT& out ) const;

};


//
// class MaximumFilter< Volume< IN >, OUT >
//

template < class IN, class OUT >
class MaximumFilter< Volume< IN >, OUT > :
                                      public FilterFunction< Volume< IN >, OUT >
{

  public:

    void filter( const Volume< IN >& in, OUT& out ) const;

};


//
// class MaximumFilter< SparseMatrix, OUT >
//

template < class OUT >
class MaximumFilter< SparseMatrix, OUT > :
                                   public FilterFunction< SparseMatrix, OUT >
{

  public:

    void filter( const SparseMatrix& in, OUT& out ) const;

};


//
// class MaximumFilter< Matrix, OUT >
//

template < class OUT >
class MaximumFilter< Matrix, OUT > :
                                   public FilterFunction< Matrix, OUT >
{

  public:

    void filter( const Matrix& in, OUT& out ) const;

};


}


#endif
