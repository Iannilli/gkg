#ifndef _gkg_processing_algobase_MaximumIndexFilter_h_
#define _gkg_processing_algobase_MaximumIndexFilter_h_


#include <gkg-processing-algobase/FilterFunction.h>


namespace gkg
{


template < class T > class Volume;


//
// class MaximumIndexFilter< IN, OUT >
//

template < class IN, class OUT >
class MaximumIndexFilter : public FilterFunction< IN, OUT >
{

  public:

    void filter( const IN& in, OUT& out ) const;


};


//
// class MaximumIndexFilter< std::vector< IN >, OUT >
//

template < class IN, class OUT >
class MaximumIndexFilter< std::vector< IN >, OUT > :
                                 public FilterFunction< std::vector< IN >, OUT >
{

  public:

    void filter( const std::vector< IN >& in, OUT& out ) const;

};


//
// class MaximumIndexFilter< Vector, OUT >
//

template < class OUT >
class MaximumIndexFilter< gkg::Vector, OUT > :
                                       public FilterFunction< gkg::Vector, OUT >
{

  public:

    void filter( const gkg::Vector& in, OUT& out ) const;

};


//
// class MaximumIndexFilter< Volume< IN >, OUT >
//

template < class IN, class OUT >
class MaximumIndexFilter< Volume< IN >, OUT > :
                                 public FilterFunction< gkg::Volume< IN >, OUT >
{

  public:

    void filter( const Volume< IN >& in, OUT& out ) const;

};


}


#endif
