#ifndef _gkg_processing_algobase_MedianFilter_h_
#define _gkg_processing_algobase_MedianFilter_h_


#include <gkg-processing-algobase/FilterFunction.h>


namespace gkg
{


template < class T > class Volume;


//
// class MedianFilter< IN, OUT >
//

template < class IN, class OUT >
class MedianFilter : public FilterFunction< IN, OUT >
{

  public:

    void filter( const IN& in, OUT& out ) const;


};


//
// class MedianFilter< std::vector< IN >, OUT >
//

template < class IN, class OUT >
class MedianFilter< std::vector< IN >, OUT > :
                                 public FilterFunction< std::vector< IN >, OUT >
{

  public:

    void filter( const std::vector< IN >& in, OUT& out ) const;

};


//
// class MedianFilter< std::vector< std::complex< IN > >, std::complex< OUT > >
//

template < class IN, class OUT >
class MedianFilter< std::vector< std::complex< IN > >, std::complex< OUT > > :
                       public FilterFunction< std::vector< std::complex< IN > >,
                                              std::complex< OUT > >
{

  public:

    void filter( const std::vector< std::complex< IN > >& in,
                  std::complex< OUT >& out ) const;

};


//
// class MedianFilter< gkg::Vector, class OUT >
//

template < class OUT >
class MedianFilter< gkg::Vector, OUT > :
                                 public FilterFunction< gkg::Vector, OUT >
{

  public:

    void filter( const gkg::Vector& in, OUT& out ) const;

};


//
// class MedianFilter< Volume< IN >, OUT >
//

template < class IN, class OUT >
class MedianFilter< Volume< IN >, OUT > :
                                 public FilterFunction< gkg::Volume< IN >, OUT >
{

  public:

    void filter( const Volume< IN >& in, OUT& out ) const;

};


//
// class MedianFilter< Volume< std::complex< IN > >, std::complex< OUT > >
//

template < class IN, class OUT >
class MedianFilter< Volume< std::complex< IN > >, std::complex< OUT > > :
                            public FilterFunction< Volume< std::complex< IN > >,
                                                   std::complex< OUT > >
{

  public:

    void filter( const Volume< std::complex< IN > >& in,
                 std::complex< OUT >& out ) const;

};


}


#endif
