#ifndef _gkg_processing_algobase_MedianFilter_i_h_
#define _gkg_processing_algobase_MedianFilter_i_h_


#include <gkg-processing-algobase/MedianFilter.h>
#include <gkg-processing-algobase/FilterFunction_i.h>
#include <gkg-processing-algobase/Converter_i.h>
#include <gkg-processing-algobase/MagnitudeConverter_i.h>
#include <gkg-processing-algobase/PhaseConverter_i.h>
#include <gkg-core-io/TypeOf.h>
#include <gkg-processing-container/Volume_i.h>
#include <gkg-core-exception/Exception.h>
#include <algorithm>


//
// class MedianFilter< IN, OUT >
//

template < class IN, class OUT >
inline
void gkg::MedianFilter< IN, OUT >::filter( const IN& in, OUT& out ) const
{

  gkg::Converter< IN, OUT > converter;
  converter.convert( in, out );

}


//
// class MedianFilter< std::vector< IN >, OUT >
//

template < class IN, class OUT >
inline
void gkg::MedianFilter< std::vector< IN >, OUT >::filter(
                                   const std::vector< IN >& in, OUT& out ) const
{

  try
  {

    if ( in.empty() )
    {

      throw std::runtime_error( "empty vector" );

    }

    std::vector< IN > copy( in );
    std::sort( copy.begin(), copy.end() );
    out = ( OUT )copy[ copy.size() / 2 - ( copy.size() % 2 ?  0U : 1U ) ];

  }
  GKG_CATCH( "template < class IN, class OUT > "
             "inline "
             "void gkg::MedianFilter< std::vector< IN >, OUT >::filter( "
             "const std::vector< IN >& in, OUT& out ) const" );

}


//
// class MedianFilter< std::vector< std::complex< IN > >, std::complex< OUT > >
//

template < class IN, class OUT >
inline
void gkg::MedianFilter< std::vector< std::complex< IN > >,
                        std::complex< OUT > >::filter(
                                    const std::vector< std::complex< IN > >& in,
                                    std::complex< OUT >& out ) const
{

  try
  {

    if ( in.empty() )
    {

      throw std::runtime_error( "empty vector" );

    }

    // taking magnitude and phase
    std::vector< double > magnitude;
    std::vector< double > phase;

    gkg::MagnitudeConverter< std::vector< std::complex< IN > >,
                             std::vector< double > > magnitudeConverter;
    gkg::PhaseConverter< std::vector< std::complex< IN > >,
                             std::vector< double > > phaseConverter;

    magnitudeConverter.convert( in, magnitude );
    phaseConverter.convert( in, phase );

    double medianMagnitude;
    double medianPhase;

    gkg::MedianFilter< std::vector< double >, double > medianFilter;
    medianFilter.filter( magnitude, medianMagnitude );
    medianFilter.filter( phase, medianPhase );

    std::complex< double > doubleOut = std::polar( medianMagnitude,
                                                   medianPhase );

    out = std::complex< OUT >( ( OUT )std::real( doubleOut ),
                               ( OUT )std::imag( doubleOut ) );

  }
  GKG_CATCH( "template < class IN, class OUT > "
             "inline void "
             "gkg::MedianFilter< std::vector< std::complex< IN > >, "
             "std::complex< OUT > >::filter( "
             "const std::vector< std::complex< IN > >& in, "
             "std::complex< OUT >& out ) const" );

}


//
// class MedianFilter< gkg::Vector, OUT >
//

template < class OUT >
inline
void gkg::MedianFilter< gkg::Vector, OUT >::filter(
                                         const gkg::Vector& in, OUT& out ) const
{

  try
  {

    if ( in.getSize() == 0 )
    {

      throw std::runtime_error( "empty vector" );

    }

    std::vector< double > copy( in.getSize() );

    int32_t index = 0;
    std::vector< double >::iterator
      c = copy.begin(),
      ce = copy.end();
    while ( c != ce )
    {

      *c = in( index );
      ++ index;
      ++ c;

    }

    std::sort( copy.begin(), copy.end() );
    out = ( OUT )copy[ copy.size() / 2 - ( copy.size() % 2 ?  0U : 1U ) ];

  }
  GKG_CATCH( "template < class OUT > "
             "inline "
             "void gkg::MedianFilter< gkg::Vector, OUT >::filter( "
             "const gkg::Vector& in, OUT& out ) const" );

}


//
// class MedianFilter< Volume< IN >, OUT >
//

template < class IN, class OUT >
inline
void gkg::MedianFilter< gkg::Volume< IN >, OUT >::filter(
                                   const gkg::Volume< IN >& in, OUT& out ) const
{

  try
  {

    std::vector< IN > copy( in.getSizeXYZT() );
    typename gkg::Volume< IN >::const_iterator i = in.begin(),
                                               ie = in.end();
    int32_t count = 0;
    while ( i != ie )
    {

      copy[ count ] = *i;
      ++ count;
      ++ i;

    }
    std::sort( copy.begin(), copy.end() );
    out = ( OUT )copy[ copy.size() / 2 - ( copy.size() % 2 ?  0U : 1U ) ];

  }
  GKG_CATCH( "template < class IN, class OUT > "
             "inline "
             "void gkg::MedianFilter< gkg::Volume< IN >, OUT >::filter( "
             "const gkg::Volume< IN >& in, OUT& out ) const" );

}


//
// class MedianFilter< Volume< std::complex< IN > >, std::complex< OUT > >
//

template < class IN, class OUT >
inline
void gkg::MedianFilter< gkg::Volume< std::complex< IN > >,
                        std::complex< OUT > >::filter(
                                    const gkg::Volume< std::complex< IN > >& in,
                                    std::complex< OUT >& out ) const
{

  try
  {

    // taking magnitude and phase
    gkg::Volume< double > magnitude;
    gkg::Volume< double > phase;

    gkg::MagnitudeConverter< gkg::Volume< std::complex< IN > >,
                             gkg::Volume< double > > magnitudeConverter;
    gkg::PhaseConverter< gkg::Volume< std::complex< IN > >,
                         gkg::Volume< double > > phaseConverter;

    magnitudeConverter.convert( in, magnitude );
    phaseConverter.convert( in, phase );

    double medianMagnitude;
    double medianPhase;

    gkg::MedianFilter< gkg::Volume< double >, double > medianFilter;
    medianFilter.filter( magnitude, medianMagnitude );
    medianFilter.filter( phase, medianPhase );

    std::complex< double > doubleOut = std::polar( medianMagnitude,
                                                   medianPhase );

    out = std::complex< OUT >( ( OUT )std::real( doubleOut ),
                               ( OUT )std::imag( doubleOut ) );

  }
  GKG_CATCH( "template < class IN, class OUT > "
             "inline void "
             "gkg::MedianFilter< gkg::Volume< std::complex< IN > >, "
             "std::complex< OUT > >::filter( "
             "const gkg::Volume< std::complex< IN > >& in, "
             "std::complex< OUT >& out ) const" );

}


#endif
