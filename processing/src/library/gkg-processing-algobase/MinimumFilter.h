#ifndef _gkg_processing_algobase_MinimumFilter_h_
#define _gkg_processing_algobase_MinimumFilter_h_


#include <gkg-processing-algobase/FilterFunction.h>


namespace gkg
{


template < class T > class Volume;


//
// class MinimumFilter< IN, OUT >
//

template < class IN, class OUT >
class MinimumFilter : public FilterFunction< IN, OUT >
{

  public:

    void filter( const IN& in, OUT& out ) const;

};


//
// class MinimumFilter< std::vector< IN >, OUT >
//

template < class IN, class OUT >
class MinimumFilter< std::vector< IN >, OUT > :
                                 public FilterFunction< std::vector< IN >, OUT >
{

  public:

    void filter( const std::vector< IN >& in, OUT& out ) const;

};


//
// class MinimumFilter< Vector, OUT >
//

template < class OUT >
class MinimumFilter< gkg::Vector, OUT > :
                                       public FilterFunction< gkg::Vector, OUT >
{

  public:

    void filter( const gkg::Vector& in, OUT& out ) const;

};


//
// class MinimumFilter< Volume< IN >, OUT >
//

template < class IN, class OUT >
class MinimumFilter< Volume< IN >, OUT > :
                                 public FilterFunction< gkg::Volume< IN >, OUT >
{

  public:

    void filter( const Volume< IN >& in, OUT& out ) const;

};


}


#endif
