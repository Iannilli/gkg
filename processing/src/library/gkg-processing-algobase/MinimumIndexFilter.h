#ifndef _gkg_processing_algobase_MinimumIndexFilter_h_
#define _gkg_processing_algobase_MinimumIndexFilter_h_


#include <gkg-processing-algobase/FilterFunction.h>


namespace gkg
{


template < class T > class Volume;


//
// class MinimumIndexFilter< IN, OUT >
//

template < class IN, class OUT >
class MinimumIndexFilter : public FilterFunction< IN, OUT >
{

  public:

    void filter( const IN& in, OUT& out ) const;


};


//
// class MinimumIndexFilter< std::vector< IN >, OUT >
//

template < class IN, class OUT >
class MinimumIndexFilter< std::vector< IN >, OUT > :
                                 public FilterFunction< std::vector< IN >, OUT >
{

  public:

    void filter( const std::vector< IN >& in, OUT& out ) const;

};


//
// class MinimumIndexFilter< Vector, OUT >
//

template < class OUT >
class MinimumIndexFilter< gkg::Vector, OUT > :
                                       public FilterFunction< gkg::Vector, OUT >
{

  public:

    void filter( const gkg::Vector& in, OUT& out ) const;

};


//
// class MinimumIndexFilter< Volume< IN >, OUT >
//

template < class IN, class OUT >
class MinimumIndexFilter< Volume< IN >, OUT > :
                                 public FilterFunction< gkg::Volume< IN >, OUT >
{

  public:

    void filter( const Volume< IN >& in, OUT& out ) const;

};


}


#endif
