#ifndef _gkg_processing_algobase_StdDevFilter_h_
#define _gkg_processing_algobase_StdDevFilter_h_


#include <gkg-processing-algobase/FilterFunction.h>


namespace gkg
{


template < class T > class Volume;


//
// class StdDevFilter< IN, OUT >
//

template < class IN, class OUT >
class StdDevFilter : public FilterFunction< IN, OUT >
{

  public:

    void filter( const IN& in, OUT& out ) const;


};


//
// class StdDevFilter< std::vector< IN >, OUT >
//

template < class IN, class OUT >
class StdDevFilter< std::vector< IN >, OUT > :
                                 public FilterFunction< std::vector< IN >, OUT >
{

  public:

    void filter( const std::vector< IN >& in, OUT& out ) const;

};


//
// class StdDevFilter< std::vector< std::complex< IN > >, std::complex< OUT > >
//

template < class IN, class OUT >
class StdDevFilter< std::vector< std::complex< IN > >, std::complex< OUT > > :
                       public FilterFunction< std::vector< std::complex< IN > >,
                                              std::complex< OUT > >
{

  public:

    void filter( const std::vector< std::complex< IN > >& in,
                  std::complex< OUT >& out ) const;

};


//
// class StdDevFilter< Vector, OUT >
//

template < class OUT >
class StdDevFilter< gkg::Vector, OUT > :
                                       public FilterFunction< gkg::Vector, OUT >
{

  public:

    void filter( const gkg::Vector& in, OUT& out ) const;

};


//
// class StdDevFilter< CVector, std::complex< OUT > >
//

template < class OUT >
class StdDevFilter< gkg::CVector, std::complex< OUT > > :
                                    public FilterFunction< gkg::CVector,
                                                           std::complex< OUT > >
{

  public:

    void filter( const gkg::CVector& in,
                  std::complex< OUT >& out ) const;

};


//
// class StdDevFilter< Volume< IN >, OUT >
//

template < class IN, class OUT >
class StdDevFilter< Volume< IN >, OUT > :
                                      public FilterFunction< Volume< IN >, OUT >
{

  public:

    void filter( const Volume< IN >& in, OUT& out ) const;

};


//
// class StdDevFilter< Volume< std::complex< IN > >, std::complex< OUT > >
//

template < class IN, class OUT >
class StdDevFilter< Volume< std::complex< IN > >, std::complex< OUT > > :
                            public FilterFunction< Volume< std::complex< IN > >,
                                                   std::complex< OUT > >
{

  public:

    void filter( const Volume< std::complex< IN > >& in,
                 std::complex< OUT >& out ) const;

};


}


#endif
