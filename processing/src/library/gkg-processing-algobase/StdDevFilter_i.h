#ifndef _gkg_processing_algobase_StdDevFilter_i_h_
#define _gkg_processing_algobase_StdDevFilter_i_h_


#include <gkg-processing-algobase/StdDevFilter.h>
#include <gkg-processing-algobase/AverageFilter.h>
#include <gkg-processing-algobase/FilterFunction_i.h>
#include <gkg-processing-algobase/Converter_i.h>
#include <gkg-core-io/TypeOf.h>
#include <gkg-processing-container/Volume_i.h>
#include <gkg-core-exception/Exception.h>
#include <cmath>


//
// class StdDevFilter< IN, OUT >
//

template < class IN, class OUT >
inline
void gkg::StdDevFilter< IN, OUT >::filter( const IN& in, OUT& out ) const
{

  gkg::Converter< IN, OUT > converter;
  converter.convert( in, out );

}


//
// class StdDevFilter< std::vector< IN >, OUT >
//

template < class IN, class OUT >
inline
void gkg::StdDevFilter< std::vector< IN >, OUT >::filter(
                                   const std::vector< IN >& in, OUT& out ) const
{

  try
  {

    if ( in.empty() )
    {

      throw std::runtime_error( "empty vector" );

    }

    gkg::AverageFilter< std::vector< IN >, double > averageFilter;
    double average;
    averageFilter.filter( in, average );

    typename std::vector< IN >::const_iterator i = in.begin(),
                                               ie = in.end();
    double sum = 0.0;
    while ( i != ie )
    {

      sum += ( ( double )*i - average ) * ( ( double )*i - average );
      ++ i;

    }
    sum /= ( double )in.size();
    out = ( OUT )std::sqrt( sum );

  }
  GKG_CATCH( "template < class IN, class OUT > "
             "inline "
             "void gkg::StdDevFilter< std::vector< IN >, OUT >::filter( "
             "const std::vector< IN >& in, OUT& out ) const" );

}


//
// class StdDevFilter< std::vector< std::complex< IN > >, std::complex< OUT > >
//

template < class IN, class OUT >
inline
void gkg::StdDevFilter< std::vector< std::complex< IN > >,
                         std::complex< OUT > >::filter(
                                    const std::vector< std::complex< IN > >& in,
                                    std::complex< OUT >& out ) const
{

  try
  {

    if ( in.empty() )
    {

      throw std::runtime_error( "empty vector" );

    }

    gkg::AverageFilter< std::vector< std::complex< IN > >,
                        std::complex< double > > averageFilter;
    std::complex< double > average;
    averageFilter.filter( in, average );

    typename std::vector< std::complex< IN > >::const_iterator i = in.begin(),
                                                               ie = in.end();
    std::complex< double > tmp, sum = 0.0;
    while ( i != ie )
    {

      tmp = std::complex< double >( std::real( *i ), std::imag( *i ) ) -
            average;
      sum += std::complex< double >( std::real( tmp ) * std::real( tmp ),
                                     std::imag( tmp ) * std::imag( tmp ) );
      ++ i;

    }
    sum /= ( double )in.size();
    out = std::complex< OUT >( std::sqrt( std::real( sum ) ),
                               std::sqrt( std::imag( sum ) ) );

  }
  GKG_CATCH( "template < class IN, class OUT > "
             "inline void "
             "gkg::StdDevFilter< std::vector< std::complex< IN > >, "
             "std::complex< OUT > >::filter( "
             "const std::vector< std::complex< IN > >& in, "
             "std::complex< OUT >& out ) const" );

}


//
// class StdDevFilter< Vector, OUT >
//

template < class OUT >
inline
void gkg::StdDevFilter< gkg::Vector, OUT >::filter(
                                         const gkg::Vector& in, OUT& out ) const
{

  try
  {

    gkg::AverageFilter< gkg::Vector, double > averageFilter;
    double average;
    averageFilter.filter( in, average );

    int32_t i = 0,
            ie = in.getSize();
    double sum = 0.0;
    while ( i != ie )
    {

      sum += ( ( double )in( i ) - average ) * ( ( double )in( i ) - average );
      ++ i;

    }
    sum /= ( double )in.getSize();
    out = ( OUT )std::sqrt( sum );

  }
  GKG_CATCH( "template < class OUT > "
             "inline "
             "void gkg::StdDevFilter< gkg::Vector, OUT >::filter( "
             "const gkg::Vector& in, OUT& out ) const" );

}


//
// class StdDevFilter< gkg::CVector, std::complex< OUT > >
//

template < class OUT >
inline
void gkg::StdDevFilter< gkg::CVector, std::complex< OUT > >::filter(
                                                const gkg::CVector& in,
                                                std::complex< OUT >& out ) const
{

  try
  {

    gkg::AverageFilter< gkg::CVector,
                        std::complex< double > > averageFilter;
    std::complex< double > average;
    averageFilter.filter( in, average );

    int32_t i = 0,
            ie = in.getSize();
    std::complex< double > tmp, sum = 0.0;
    while ( i != ie )
    {

      tmp = std::complex< double >( std::real( in( i ) ),
                                    std::imag( in( i ) ) ) - average;
      sum += std::complex< double >( std::real( tmp ) * std::real( tmp ),
                                     std::imag( tmp ) * std::imag( tmp ) );
      ++ i;

    }
    sum /= ( double )in.getSize();
    out = std::complex< OUT >( std::sqrt( std::real( sum ) ),
                               std::sqrt( std::imag( sum ) ) );

  }
  GKG_CATCH( "template < class OUT > "
             "inline void "
             "gkg::StdDevFilter< gkg::CVector, "
             "std::complex< OUT > >::filter( "
             "const gkg::CVector& in, "
             "std::complex< OUT >& out ) const" );

}


//
// class StdDevFilter< Volume< IN >, OUT >
//

template < class IN, class OUT >
inline
void gkg::StdDevFilter< gkg::Volume< IN >, OUT >::filter(
                                   const gkg::Volume< IN >& in, OUT& out ) const
{

  try
  {

    gkg::AverageFilter< gkg::Volume< IN >, double > averageFilter;
    double average;
    averageFilter.filter( in, average );

    typename gkg::Volume< IN >::const_iterator i = in.begin(),
                                               ie = in.end();
    double sum = 0.0;
    while ( i != ie )
    {

      sum += ( ( double )*i - average ) * ( ( double )*i - average );
      ++ i;

    }
    sum /= ( double )in.size();
    out = ( OUT )std::sqrt( sum );

  }
  GKG_CATCH( "template < class IN, class OUT > "
             "inline "
             "void gkg::StdDevFilter< gkg::Volume< IN >, OUT >::filter( "
             "const gkg::Volume< IN >& in, OUT& out ) const" );

}


//
// class StdDevFilter< Volume< std::complex< IN > >, std::complex< OUT > >
//

template < class IN, class OUT >
inline
void gkg::StdDevFilter< gkg::Volume< std::complex< IN > >,
                         std::complex< OUT > >::filter(
                                    const gkg::Volume< std::complex< IN > >& in,
                                    std::complex< OUT >& out ) const
{

  try
  {

    gkg::AverageFilter< gkg::Volume< std::complex< IN > >,
                        std::complex< double > > averageFilter;
    std::complex< double > average;
    averageFilter.filter( in, average );

    typename gkg::Volume< std::complex< IN > >::const_iterator i = in.begin(),
                                                               ie = in.end();
    std::complex< double > tmp, sum = 0.0;
    while ( i != ie )
    {

      tmp = std::complex< double >( std::real( *i ), std::imag( *i ) ) -
            average;
      sum += std::complex< double >( std::real( tmp ) * std::real( tmp ),
                                     std::imag( tmp ) * std::imag( tmp ) );
      ++ i;

    }
    sum /= ( double )in.size();
    out = std::complex< OUT >( std::sqrt( std::real( sum ) ),
                               std::sqrt( std::imag( sum ) ) );

  }
  GKG_CATCH( "template < class IN, class OUT > "
             "inline void "
             "gkg::StdDevFilter< gkg::Volume< std::complex< IN > >, "
             "std::complex< OUT > >::filter( "
             "const gkg::Volume< std::complex< IN > >& in, "
             "std::complex< OUT >& out ) const" );

}


#endif
