#ifndef _gkg_processing_algobase_SumFilter_h_
#define _gkg_processing_algobase_SumFilter_h_


#include <gkg-processing-algobase/FilterFunction.h>


namespace gkg
{


template < class T > class Volume;


//
// class SumFilter< IN, OUT >
//

template < class IN, class OUT >
class SumFilter : public FilterFunction< IN, OUT >
{

  public:

    void filter( const IN& in, OUT& out ) const;


};


//
// class SumFilter< std::vector< IN >, OUT >
//

template < class IN, class OUT >
class SumFilter< std::vector< IN >, OUT > :
                                 public FilterFunction< std::vector< IN >, OUT >
{

  public:

    void filter( const std::vector< IN >& in, OUT& out ) const;

};


//
// class SumFilter< std::vector< std::complex< IN > >, std::complex< OUT > >
//

template < class IN, class OUT >
class SumFilter< std::vector< std::complex< IN > >, std::complex< OUT > > :
                       public FilterFunction< std::vector< std::complex< IN > >,
                                              std::complex< OUT > >
{

  public:

    void filter( const std::vector< std::complex< IN > >& in,
                  std::complex< OUT >& out ) const;

};


//
// class SumFilter< Vector, OUT >
//

template < class OUT >
class SumFilter< gkg::Vector, OUT > :
                                       public FilterFunction< gkg::Vector, OUT >
{

  public:

    void filter( const gkg::Vector& in, OUT& out ) const;

};


//
// class SumFilter< CVector, std::complex< OUT > >
//

template < class OUT >
class SumFilter< gkg::CVector, std::complex< OUT > > :
                       public FilterFunction< gkg::CVector,
                                              std::complex< OUT > >
{

  public:

    void filter( const gkg::CVector& in,
                  std::complex< OUT >& out ) const;

};


//
// class SumFilter< Volume< IN >, OUT >
//

template < class IN, class OUT >
class SumFilter< Volume< IN >, OUT > :
                                      public FilterFunction< Volume< IN >, OUT >
{

  public:

    void filter( const Volume< IN >& in, OUT& out ) const;

};


//
// class SumFilter< Volume< std::complex< IN > >, std::complex< OUT > >
//

template < class IN, class OUT >
class SumFilter< Volume< std::complex< IN > >, std::complex< OUT > > :
                            public FilterFunction< Volume< std::complex< IN > >,
                                                   std::complex< OUT > >
{

  public:

    void filter( const Volume< std::complex< IN > >& in,
                 std::complex< OUT >& out ) const;

};


}


#endif
