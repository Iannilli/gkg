#ifndef _gkg_processing_algobase_SumFilter_i_h_
#define _gkg_processing_algobase_SumFilter_i_h_


#include <gkg-processing-algobase/SumFilter.h>
#include <gkg-processing-algobase/FilterFunction_i.h>
#include <gkg-processing-algobase/Converter_i.h>
#include <gkg-core-io/TypeOf.h>
#include <gkg-processing-container/Volume_i.h>
#include <gkg-core-exception/Exception.h>


//
// class SumFilter< IN, OUT >
//

template < class IN, class OUT >
inline
void gkg::SumFilter< IN, OUT >::filter( const IN& in, OUT& out ) const
{

  gkg::Converter< IN, OUT > converter;
  converter.convert( in, out );

}


//
// class SumFilter< std::vector< IN >, OUT >
//

template < class IN, class OUT >
inline
void gkg::SumFilter< std::vector< IN >, OUT >::filter(
                                   const std::vector< IN >& in, OUT& out ) const
{

  try
  {

    if ( in.empty() )
    {

      throw std::runtime_error( "empty vector" );

    }

    typename std::vector< IN >::const_iterator i = in.begin(),
                                               ie = in.end();
    double sum = 0.0;
    while ( i != ie )
    {

      sum += ( double )*i;
      ++ i;

    }
    out = ( OUT )( sum );

  }
  GKG_CATCH( "template < class IN, class OUT > "
             "inline "
             "void gkg::SumFilter< std::vector< IN >, OUT >::filter( "
             "const std::vector< IN >& in, OUT& out ) const" );

}


//
// class SumFilter< std::vector< std::complex< IN > >, std::complex< OUT > >
//

template < class IN, class OUT >
inline
void gkg::SumFilter< std::vector< std::complex< IN > >,
                         std::complex< OUT > >::filter(
                                    const std::vector< std::complex< IN > >& in,
                                    std::complex< OUT >& out ) const
{

  try
  {

    if ( in.empty() )
    {

      throw std::runtime_error( "empty vector" );

    }

    typename std::vector< std::complex< IN > >::const_iterator i = in.begin(),
                                                               ie = in.end();
    std::complex< double > sum = 0.0;
    while ( i != ie )
    {

      sum += std::complex< double >( std::real( *i ), std::imag( *i ) );
      ++ i;

    }
    out = std::complex< OUT >( std::real( sum ), std::imag( sum ) );

  }
  GKG_CATCH( "template < class IN, class OUT > "
             "inline void "
             "gkg::SumFilter< std::vector< std::complex< IN > >, "
             "std::complex< OUT > >::filter( "
             "const std::vector< std::complex< IN > >& in, "
             "std::complex< OUT >& out ) const" );

}


//
// class SumFilter< Vector, OUT >
//

template < class OUT >
inline
void gkg::SumFilter< gkg::Vector, OUT >::filter(
                                         const gkg::Vector& in, OUT& out ) const
{

  try
  {

    int32_t i = 0,
            ie = in.getSize();
    double sum = 0.0;
    while ( i != ie )
    {

      sum += ( double )in( i );
      ++ i;

    }
    out = ( OUT )( sum );

  }
  GKG_CATCH( "template < class OUT > "
             "inline "
             "void gkg::SumFilter< gkg::Vector, OUT >::filter( "
             "const gkg::Vector& in, OUT& out ) const" );

}


//
// class SumFilter< gkg::CVector, std::complex< OUT > >
//

template < class OUT >
inline
void gkg::SumFilter< gkg::CVector,
                         std::complex< OUT > >::filter(
                                    const gkg::CVector& in,
                                    std::complex< OUT >& out ) const
{

  try
  {

    int32_t i = 0,
            ie = in.getSize();
    std::complex< double > sum = 0.0;
    while ( i != ie )
    {

      sum += std::complex< double >( std::real( in( i ) ),
                                     std::imag( in ( i ) ) );
      ++ i;

    }
    out = std::complex< OUT >( std::real( sum ), std::imag( sum ) );

  }
  GKG_CATCH( "template < class OUT > "
             "inline void "
             "gkg::SumFilter< gkg::CVector, "
             "std::complex< OUT > >::filter( "
             "const gkg::CVector& in, "
             "std::complex< OUT >& out ) const" );

}


//
// class SumFilter< Volume< IN >, OUT >
//

template < class IN, class OUT >
inline
void gkg::SumFilter< gkg::Volume< IN >, OUT >::filter(
                                   const gkg::Volume< IN >& in, OUT& out ) const
{

  try
  {

    typename gkg::Volume< IN >::const_iterator i = in.begin(),
                                               ie = in.end();
    double sum = 0.0;
    while ( i != ie )
    {

      sum += ( double )*i;
      ++ i;

    }
    out = ( OUT )( sum );

  }
  GKG_CATCH( "template < class IN, class OUT > "
             "inline "
             "void gkg::SumFilter< gkg::Volume< IN >, OUT >::filter( "
             "const gkg::Volume< IN >& in, OUT& out ) const" );

}


//
// class SumFilter< Volume< std::complex< IN > >, std::complex< OUT > >
//

template < class IN, class OUT >
inline
void gkg::SumFilter< gkg::Volume< std::complex< IN > >,
                         std::complex< OUT > >::filter(
                                    const gkg::Volume< std::complex< IN > >& in,
                                    std::complex< OUT >& out ) const
{

  try
  {

    typename gkg::Volume< std::complex< IN > >::const_iterator i = in.begin(),
                                                               ie = in.end();
    std::complex< double > sum = 0.0;
    while ( i != ie )
    {

      sum += std::complex< double >( std::real( *i ), std::imag( *i ) );
      ++ i;

    }
    out = std::complex< OUT >( std::real( sum ), std::imag( sum ) );

  }
  GKG_CATCH( "template < class IN, class OUT > "
             "inline void "
             "gkg::SumFilter< gkg::Volume< std::complex< IN > >, "
             "std::complex< OUT > >::filter( "
             "const gkg::Volume< std::complex< IN > >& in, "
             "std::complex< OUT >& out ) const" );

}


#endif
