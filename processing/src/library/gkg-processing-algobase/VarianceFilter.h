#ifndef _gkg_processing_algobase_VarianceFilter_h_
#define _gkg_processing_algobase_VarianceFilter_h_


#include <gkg-processing-algobase/FilterFunction.h>


namespace gkg
{


template < class T > class Volume;


//
// class VarianceFilter< IN, OUT >
//

template < class IN, class OUT >
class VarianceFilter : public FilterFunction< IN, OUT >
{

  public:

    void filter( const IN& in, OUT& out ) const;


};


//
// class VarianceFilter< std::vector< IN >, OUT >
//

template < class IN, class OUT >
class VarianceFilter< std::vector< IN >, OUT > :
                                 public FilterFunction< std::vector< IN >, OUT >
{

  public:

    void filter( const std::vector< IN >& in, OUT& out ) const;

};


//
// class VarianceFilter< std::vector< std::complex< IN > >,
//                       std::complex< OUT > >
//

template < class IN, class OUT >
class VarianceFilter< std::vector< std::complex< IN > >, std::complex< OUT > > :
                       public FilterFunction< std::vector< std::complex< IN > >,
                                              std::complex< OUT > >
{

  public:

    void filter( const std::vector< std::complex< IN > >& in,
                  std::complex< OUT >& out ) const;

};


//
// class VarianceFilter< Vector, OUT >
//

template < class OUT >
class VarianceFilter< gkg::Vector, OUT > :
                                       public FilterFunction< gkg::Vector, OUT >
{

  public:

    void filter( const gkg::Vector& in, OUT& out ) const;

};


//
// class VarianceFilter< CVector, std::complex< OUT > >
//

template < class OUT >
class VarianceFilter< gkg::CVector, std::complex< OUT > > :
                                    public FilterFunction< gkg::CVector,
                                                           std::complex< OUT > >
{

  public:

    void filter( const gkg::CVector& in,
                  std::complex< OUT >& out ) const;

};


//
// class VarianceFilter< Volume< IN >, OUT >
//

template < class IN, class OUT >
class VarianceFilter< Volume< IN >, OUT > :
                                      public FilterFunction< Volume< IN >, OUT >
{

  public:

    void filter( const Volume< IN >& in, OUT& out ) const;

};


//
// class VarianceFilter< Volume< std::complex< IN > >, std::complex< OUT > >
//

template < class IN, class OUT >
class VarianceFilter< Volume< std::complex< IN > >, std::complex< OUT > > :
                            public FilterFunction< Volume< std::complex< IN > >,
                                                   std::complex< OUT > >
{

  public:

    void filter( const Volume< std::complex< IN > >& in,
                 std::complex< OUT >& out ) const;

};


}


#endif
