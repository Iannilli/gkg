#include <gkg-processing-colorimetry/RandomColorGenerator.h>
#include <gkg-core-exception/Exception.h>
#include <cmath>



gkg::RandomColorGenerator::RandomColorGenerator( float saturation,
                                                 float value )
                          : _saturation( saturation ),
                            _value( value ),
                            _randomGenerator( gkg::RandomGenerator::Taus )
{

  try
  {

    if ( ( saturation < 0.0f ) || ( saturation > 1.0f ) )
    {

      throw std::runtime_error( "saturation must belong to [0.0;1.0]" );


    }

    if ( ( value < 0.0f ) || ( value > 1.0f ) )
    {

      throw std::runtime_error( "value must belong to [0.0;1.0]" );


    }

    _factory = 
       gkg::NumericalAnalysisSelector::getInstance().getImplementationFactory();

    _goldenRatioConjugate = ( ( float )std::sqrt( 5.0 ) - 1.0f ) / 2.0f;

  }
  GKG_CATCH( "gkg::RandomColorGenerator::RandomColorGenerator()" );

}



gkg::RandomColorGenerator::~RandomColorGenerator()
{
}



gkg::RGBComponent gkg::RandomColorGenerator::getRandomColor() const
{

  try
  {

    float randomHue = ( float )_factory->getUniformRandomNumber(
                                                               _randomGenerator,
                                                               0.0,
                                                               1.0 );
    randomHue = fmodf( randomHue +_goldenRatioConjugate, 1.0f );

    return this->getRGBFromHSVComponents( randomHue, _saturation, _value );

  }
  GKG_CATCH( "gkg::RGBComponent "
             "gkg::RandomColorGenerator::getRandomColor() const" );


}


gkg::RGBComponent gkg::RandomColorGenerator::getRGBFromHSVComponents(
                                                             float hue,
                                                             float saturation,
                                                             float value ) const
{

  try
  {

    int32_t hueInt32 = ( int32_t )( hue * 6.0f );
    float f = hue * 6.0f - ( float )hueInt32;
    float p = value * ( 1.0f - saturation );
    float q = value * ( 1.0f - f * saturation );
    float t = value * ( 1.0f - ( 1.0f - f ) * saturation );

    gkg::RGBComponent rgbComponent;
    switch ( hueInt32 )
    {

       case 0:

         rgbComponent.r = ( uint8_t )( 255 * value );
         rgbComponent.g = ( uint8_t )( 255 * t );
         rgbComponent.b = ( uint8_t )( 255 * p );
         break;

       case 1:

         rgbComponent.r = ( uint8_t )( 255 * q );
         rgbComponent.g = ( uint8_t )( 255 * value );
         rgbComponent.b = ( uint8_t )( 255 * p );
         break;

       case 2:

         rgbComponent.r = ( uint8_t )( 255 * p );
         rgbComponent.g = ( uint8_t )( 255 * value );
         rgbComponent.b = ( uint8_t )( 255 * t );
         break;

       case 3:

         rgbComponent.r = ( uint8_t )( 255 * p );
         rgbComponent.g = ( uint8_t )( 255 * q);
         rgbComponent.b = ( uint8_t )( 255 * value );
         break;

       case 4:

         rgbComponent.r = ( uint8_t )( 255 * t );
         rgbComponent.g = ( uint8_t )( 255 * p );
         rgbComponent.b = ( uint8_t )( 255 * value );
         break;

       case 5:

         rgbComponent.r = ( uint8_t )( 255 * value );
         rgbComponent.g = ( uint8_t )( 255 * p );
         rgbComponent.b = ( uint8_t )( 255 * q );
         break;

    }

    return rgbComponent;

  }
  GKG_CATCH( "gkg::RGBComponent gkg::RandomColorGenerator::"
             "getRGBFromHSVComponents( "
             "float hue, "
             "float saturation, "
             "float value ) const" );

}
