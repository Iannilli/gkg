#ifndef _gkg_processing_colorimetry_RandomColorGenerator_h_
#define _gkg_processing_colorimetry_RandomColorGenerator_h_


#include <gkg-processing-colorimetry/RGBComponent.h>
#include <gkg-processing-numericalanalysis/RandomGenerator.h>
#include <gkg-processing-numericalanalysis/NumericalAnalysisSelector.h>


namespace gkg
{


class RandomColorGenerator
{

  public:

    RandomColorGenerator( float saturation, float value );
    virtual ~RandomColorGenerator();

    RGBComponent getRandomColor() const;
    

    RGBComponent getRGBFromHSVComponents( float hue,
                                          float saturation,
                                          float value ) const;
    

  protected:

    float _saturation;
    float _value;
    RandomGenerator _randomGenerator;
    NumericalAnalysisImplementationFactory* _factory;
    float _goldenRatioConjugate;

};



}


#endif
