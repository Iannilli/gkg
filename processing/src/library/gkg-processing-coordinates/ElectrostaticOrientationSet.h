#ifndef _gkg_processing_coordinates_ElectrostaticOrientationSet_h_
#define _gkg_processing_coordinates_ElectrostaticOrientationSet_h_


#include <gkg-processing-coordinates/OrientationSet.h>


namespace gkg
{


//
// quick C function
//

float GkgGetUniformOrientation( int32_t orientationCount,
                                int32_t index, 
                                int32_t coordinate );

//
// class ElectrostaticOrientationSet
//

class ElectrostaticOrientationSet : public OrientationSet
{

  public:

    ElectrostaticOrientationSet( int32_t count );
    virtual ~ElectrostaticOrientationSet();


};


}


#endif
