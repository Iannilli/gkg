#include <gkg-processing-coordinates/VoxelSampler.h>


template < class T >
gkg::VoxelSampler< T >::VoxelSampler( const gkg::Vector3d< double >& resolution,
                                      int32_t pointCount )
                       : _resolution( resolution ),
                         _offsets( pointCount )
{
}


template < class T >
gkg::VoxelSampler< T >::~VoxelSampler()
{
}


template < class T >
const gkg::Vector3d< double >& gkg::VoxelSampler< T >::getResolution() const
{

  return _resolution;

}


template < class T >
int32_t gkg::VoxelSampler< T >::getPointCount() const
{

  return ( int32_t )_offsets.size();

}


template < class T >
const gkg::Vector3d< T >& 
gkg::VoxelSampler< T >::getOffset( int32_t index ) const
{

  return _offsets[ index ];

}


template < class T >
typename gkg::VoxelSampler< T >::iterator 
gkg::VoxelSampler< T >::begin()
{

  return _offsets.begin();

}


template < class T >
typename gkg::VoxelSampler< T >::iterator 
gkg::VoxelSampler< T >::end()
{

  return _offsets.end();

}


template < class T >
typename gkg::VoxelSampler< T >::const_iterator 
gkg::VoxelSampler< T >::begin() const
{

  return _offsets.begin();

}


template < class T >
typename gkg::VoxelSampler< T >::const_iterator 
gkg::VoxelSampler< T >::end() const
{

  return _offsets.end();

}


// forcing instanciation(s)

template class gkg::VoxelSampler< float >;
template class gkg::VoxelSampler< double >;
