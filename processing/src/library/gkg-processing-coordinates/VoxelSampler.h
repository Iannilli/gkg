#ifndef _gkg_processing_coordinates_VoxelSampler_h_
#define _gkg_processing_coordinates_VoxelSampler_h_


#include <gkg-processing-coordinates/Vector3d.h>
#include <vector>


namespace gkg
{


template < class T >
class VoxelSampler
{

  public:

    typedef typename std::vector< gkg::Vector3d< T > >::iterator iterator;
    typedef typename std::vector< gkg::Vector3d< T > >::const_iterator 
                                                                 const_iterator;

    const Vector3d< double >& getResolution() const;
    int32_t getPointCount() const;
    const Vector3d< T >& getOffset( int32_t index ) const;


    virtual ~VoxelSampler();

    iterator begin();
    iterator end();

    const_iterator begin() const;
    const_iterator end() const;


  protected:

    VoxelSampler( const Vector3d< double >& resolution,
                  int32_t pointCount );

    Vector3d< double > _resolution;
    std::vector< gkg::Vector3d< T > > _offsets;


};


}


#endif
