#include <gkg-processing-io/SparseCMatrixAsciiItemWriter.h>
#include <gkg-core-io/ItemWriter_i.h>
#include <gkg-core-io/DefaultItemIOFactory_i.h>


gkg::SparseCMatrixAsciiItemWriter::SparseCMatrixAsciiItemWriter()
                                  : gkg::ItemWriter< gkg::SparseCMatrix >()
{
}


gkg::SparseCMatrixAsciiItemWriter::~SparseCMatrixAsciiItemWriter()
{
}


void gkg::SparseCMatrixAsciiItemWriter::write( std::ostream& os,
                                               const gkg::SparseCMatrix* pItem,
                                               size_t count ) const
{

  gkg::ItemWriter< uint32_t >& itemW1 =
    gkg::DefaultItemIOFactory< uint32_t >::getInstance().getWriter( true,
                                                                    false );
  gkg::ItemWriter< std::complex< double > >& itemW2 =
    gkg::DefaultItemIOFactory<
               std::complex< double > >::getInstance().getWriter( true,
                                                                  false );

  const gkg::SparseCMatrix* pointer = pItem;
  gkg::SparseCMatrix::const_iterator1 s1;
  gkg::SparseCMatrix::const_iterator2 s2;
  for ( size_t i = 0; i < count; ++i, ++pointer )
  {

    itemW1.write( os, pointer->getSize1() );
    itemW1.write( os, pointer->getSize2() );
    itemW1.write( os, pointer->getNonZeroElementCount() );
    for ( s1 = pointer->begin1(); s1 != pointer->end1(); s1++ )
    {

      for ( s2 = s1.begin(); s2 != s1.end(); s2++ )
      {

        itemW1.write( os, s1.index1() );
        itemW1.write( os, s2.index2() );
        itemW2.write( os, *s2 );

      }

    }

  }

}


void gkg::SparseCMatrixAsciiItemWriter::write( gkg::largefile_ofstream& os,
                                               const gkg::SparseCMatrix* pItem,
                                               size_t count ) const
{

  gkg::ItemWriter< uint32_t >& itemW1 =
    gkg::DefaultItemIOFactory< uint32_t >::getInstance().getWriter( true,
                                                                    false );
  gkg::ItemWriter< std::complex< double > >& itemW2 =
    gkg::DefaultItemIOFactory<
               std::complex< double > >::getInstance().getWriter( true,
                                                                  false );

  const gkg::SparseCMatrix* pointer = pItem;
  gkg::SparseCMatrix::const_iterator1 s1;
  gkg::SparseCMatrix::const_iterator2 s2;
  for ( size_t i = 0; i < count; ++i, ++pointer )
  {

    itemW1.write( os, pointer->getSize1() );
    itemW1.write( os, pointer->getSize2() );
    itemW1.write( os, pointer->getNonZeroElementCount() );
    for ( s1 = pointer->begin1(); s1 != pointer->end1(); s1++ )
    {

      for ( s2 = s1.begin(); s2 != s1.end(); s2++ )
      {

        itemW1.write( os, s1.index1() );
        itemW1.write( os, s2.index2() );
        itemW2.write( os, *s2 );

      }

    }

  }

}


void gkg::SparseCMatrixAsciiItemWriter::write( gkg::largefile_fstream& fs,
                                               const gkg::SparseCMatrix* pItem,
                                               size_t count ) const
{

  gkg::ItemWriter< uint32_t >& itemW1 =
    gkg::DefaultItemIOFactory< uint32_t >::getInstance().getWriter( true,
                                                                    false );
  gkg::ItemWriter< std::complex< double > >& itemW2 =
    gkg::DefaultItemIOFactory<
               std::complex< double > >::getInstance().getWriter( true,
                                                                  false );

  const gkg::SparseCMatrix* pointer = pItem;
  gkg::SparseCMatrix::const_iterator1 s1;
  gkg::SparseCMatrix::const_iterator2 s2;
  for ( size_t i = 0; i < count; ++i, ++pointer )
  {

    itemW1.write( fs, pointer->getSize1() );
    itemW1.write( fs, pointer->getSize2() );
    itemW1.write( fs, pointer->getNonZeroElementCount() );
    for ( s1 = pointer->begin1(); s1 != pointer->end1(); s1++ )
    {

      for ( s2 = s1.begin(); s2 != s1.end(); s2++ )
      {

        itemW1.write( fs, s1.index1() );
        itemW1.write( fs, s2.index2() );
        itemW2.write( fs, *s2 );

      }

    }

  }

}
