#include <gkg-processing-io/SparseMatrixBSwapItemWriter.h>
#include <gkg-core-io/ItemWriter_i.h>
#include <gkg-core-io/DefaultItemIOFactory_i.h>


gkg::SparseMatrixBSwapItemWriter::SparseMatrixBSwapItemWriter()
                                 : gkg::ItemWriter< gkg::SparseMatrix >()
{
}


gkg::SparseMatrixBSwapItemWriter::~SparseMatrixBSwapItemWriter()
{
}


void gkg::SparseMatrixBSwapItemWriter::write( std::ostream& os,
                                              const gkg::SparseMatrix* pItem,
                                              size_t count ) const
{

  gkg::ItemWriter< uint32_t >& itemW1 =
    gkg::DefaultItemIOFactory< uint32_t >::getInstance().getWriter( false,
                                                                    true );
  gkg::ItemWriter< double >& itemW2 =
    gkg::DefaultItemIOFactory< double >::getInstance().getWriter( false,
                                                                  true );

  const gkg::SparseMatrix* pointer = pItem;
  gkg::SparseMatrix::const_iterator1 s1;
  gkg::SparseMatrix::const_iterator2 s2;
  for ( size_t i = 0; i < count; ++i, ++pointer )
  {

    itemW1.write( os, pointer->getSize1() );
    itemW1.write( os, pointer->getSize2() );
    itemW1.write( os, pointer->getNonZeroElementCount() );
    for ( s1 = pointer->begin1(); s1 != pointer->end1(); s1++ )
    {

      for ( s2 = s1.begin(); s2 != s1.end(); s2++ )
      {

        itemW1.write( os, s1.index1() );
        itemW1.write( os, s2.index2() );
        itemW2.write( os, *s2 );

      }

    }

  }

}


void gkg::SparseMatrixBSwapItemWriter::write( gkg::largefile_ofstream& os,
                                        const gkg::SparseMatrix* pItem,
                                        size_t count ) const
{

  gkg::ItemWriter< uint32_t >& itemW1 =
    gkg::DefaultItemIOFactory< uint32_t >::getInstance().getWriter( false,
                                                                    true );
  gkg::ItemWriter< double >& itemW2 =
    gkg::DefaultItemIOFactory< double >::getInstance().getWriter( false,
                                                                  true );

  const gkg::SparseMatrix* pointer = pItem;
  gkg::SparseMatrix::const_iterator1 s1;
  gkg::SparseMatrix::const_iterator2 s2;
  for ( size_t i = 0; i < count; ++i, ++pointer )
  {

    itemW1.write( os, pointer->getSize1() );
    itemW1.write( os, pointer->getSize2() );
    itemW1.write( os, pointer->getNonZeroElementCount() );
    for ( s1 = pointer->begin1(); s1 != pointer->end1(); s1++ )
    {

      for ( s2 = s1.begin(); s2 != s1.end(); s2++ )
      {

        itemW1.write( os, s1.index1() );
        itemW1.write( os, s2.index2() );
        itemW2.write( os, *s2 );

      }

    }

  }

}


void gkg::SparseMatrixBSwapItemWriter::write( gkg::largefile_fstream& fs,
                                        const gkg::SparseMatrix* pItem,
                                        size_t count ) const
{


  gkg::ItemWriter< uint32_t >& itemW1 =
    gkg::DefaultItemIOFactory< uint32_t >::getInstance().getWriter( false,
                                                                    true );
  gkg::ItemWriter< double >& itemW2 =
    gkg::DefaultItemIOFactory< double >::getInstance().getWriter( false,
                                                                  true );

  const gkg::SparseMatrix* pointer = pItem;
  gkg::SparseMatrix::const_iterator1 s1;
  gkg::SparseMatrix::const_iterator2 s2;
  for ( size_t i = 0; i < count; ++i, ++pointer )
  {

    itemW1.write( fs, pointer->getSize1() );
    itemW1.write( fs, pointer->getSize2() );
    itemW1.write( fs, pointer->getNonZeroElementCount() );
    for ( s1 = pointer->begin1(); s1 != pointer->end1(); s1++ )
    {

      for ( s2 = s1.begin(); s2 != s1.end(); s2++ )
      {

        itemW1.write( fs, s1.index1() );
        itemW1.write( fs, s2.index2() );
        itemW2.write( fs, *s2 );

      }

    }

  }

}
