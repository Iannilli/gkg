#include <gkg-processing-morphology/Neighborhood3d.h>
#include <gkg-core-exception/Exception.h>


gkg::Neighborhood3d::Neighborhood3d()
{
}


gkg::Neighborhood3d::~Neighborhood3d()
{
}


std::list< gkg::Vector3d< int32_t > > 
gkg::Neighborhood3d::getNeighborOffsets( gkg::Neighborhood3d::Type type,
                                         int32_t stride ) const
{

  try
  {

    std::list< gkg::Vector3d< int32_t > > neighborOffsets;
    switch ( type )
    {

      case gkg::Neighborhood3d::Neighborhood2d_4xy:

        {

          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( -stride, 0, 0 ) );
          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( stride, 0, 0 ) );
          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( 0, -stride, 0 ) );
          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( 0, stride, 0 ) );

        }
        break;

      case gkg::Neighborhood3d::Neighborhood2d_4xz:

        {

          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( -stride, 0, 0 ) );
          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( stride, 0, 0 ) );
          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( 0, 0, -stride ) );
          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( 0, 0, stride ) );

        }
        break;

      case gkg::Neighborhood3d::Neighborhood2d_4yz:

        {

          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( 0, -stride, 0 ) );
          neighborOffsets.push_back(  
            gkg::Vector3d< int32_t >( 0, stride, 0 ) );
          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( 0, 0, -stride ) );
          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( 0, 0, stride ) );

        }
        break;

      case gkg::Neighborhood3d::Neighborhood2d_8xy:

        {

          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( -stride, -stride, 0 ) );
          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >(  0, -stride, 0 ) );
          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( +stride, -stride, 0 ) );
          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( -stride,  0, 0 ) );
          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( +stride,  0, 0 ) );
          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( -stride, +stride, 0 ) );
          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >(  0, +stride, 0 ) );
          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( +stride, +stride, 0 ) );

        }
        break;

      case gkg::Neighborhood3d::Neighborhood2d_8xz:

        {

          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( -stride, 0, -stride ) );
          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >(  0, 0, -stride ) );
          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( +stride, 0, -stride ) );
          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( -stride, 0,  0 ) );
          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( +stride, 0,  0 ) );
          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( -stride, 0, +stride ) );
          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >(  0, 0, +stride ) );
          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( +stride, 0, +stride ) );

        }
        break;

      case gkg::Neighborhood3d::Neighborhood2d_8yz:

        {

          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( 0, -stride, -stride ) );
          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( 0,  0, -stride ) );
          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( 0, +stride, -stride ) );
          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( 0, -stride,  0 ) );
          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( 0, +stride,  0 ) );
          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( 0, -stride, +stride ) );
          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( 0,  0, +stride ) );
          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( 0, +stride, +stride ) );

        }
        break;

      case gkg::Neighborhood3d::Neighborhood3d_6:

        {

          // CAUTION : the arrangement below corresponds to the one needed by
          // Jeff's algorithm for 3D topology classification

          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( -stride, 0, 0 ) );
          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( 0, -stride, 0 ) );
          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( 0, 0, -stride ) );
          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( 0, 0, +stride ) );
          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( 0, +stride, 0 ) );
          neighborOffsets.push_back( 
            gkg::Vector3d< int32_t >( +stride, 0, 0 ) );

        }
        break;

      case gkg::Neighborhood3d::Neighborhood3d_18:

        {

          // CAUTION : the arrangement below corresponds to the one needed by
          // Jeff's algorithm for 3D topology classification
          // It starts with the 6-neighbor points followed by the 18-neighbor
          // ones

          int32_t x, y, z;
          for ( x = -stride; x <= stride; x += stride )
          {

            for ( y = -stride; y <= stride; y += stride )
            {

              for ( z = -stride; z <= stride; z += stride )
              {

                if ( ( std::abs( x ) +
                       std::abs( y ) +
                       std::abs( z ) ) == stride )
                {

                  neighborOffsets.push_back( 
		                          gkg::Vector3d< int32_t >( x, y, z ) );

                }

              }

            }

          }

          for ( x = -stride; x <= stride; x += stride )
          {

            for ( y = -stride; y <= stride; y += stride )
            {

              for ( z = -stride; z <= stride; z += stride )
              {

                if ( ( std::abs( x ) +
                       std::abs( y ) +
                       std::abs( z ) ) == 2 * stride )
                {

                  neighborOffsets.push_back( 
		                          gkg::Vector3d< int32_t >( x, y, z ) );

                }

              }

            }

          }

        }
        break;

      case gkg::Neighborhood3d::Neighborhood3d_26:

        {

          // CAUTION : the arrangement below corresponds to the one needed by
          // Jeff's algorithm for 3D topology classification
          // It starts with the 6-neighbor points followed by the 18-neighbor
          // points, and eneded with the 26-neighbor points

          int32_t x, y, z;
          for ( x = -stride; x <= stride; x += stride )
          {

            for ( y = -stride; y <= stride; y += stride )
            {

              for ( z = -stride; z <= stride; z += stride )
              {

                if ( ( std::abs( x ) + 
                       std::abs( y ) + 
                       std::abs( z ) ) == stride )
                {

                  neighborOffsets.push_back( 
		                          gkg::Vector3d< int32_t >( x, y, z ) );

                }

              }

            }

          }

          for ( x = -stride; x <= stride; x += stride )
          {

            for ( y = -stride; y <= stride; y += stride )
            {

              for ( z = -stride; z <= stride; z += stride )
              {

                if ( ( std::abs( x ) +
                       std::abs( y ) +
                       std::abs( z ) ) == 2 * stride )
                {

                  neighborOffsets.push_back( 
		                          gkg::Vector3d< int32_t >( x, y, z ) );

                }

              }

            }

          }

          for ( x = -stride; x <= stride; x += stride )
          {

            for ( y = -stride; y <= stride; y += stride )
            {

              for ( z = -stride; z <= stride; z += stride )
              {

                if ( ( std::abs( x ) +
                       std::abs( y ) +
                       std::abs( z ) ) == 3 * stride )
                {

                  neighborOffsets.push_back( 
		                          gkg::Vector3d< int32_t >( x, y, z ) );

                }

              }

            }

          }

        }
        break;

    }
    return neighborOffsets;

  }
  GKG_CATCH( "std::list< gkg::Vector3d< int32_t > > "
             "gkg::Neighborhood3d::getNeighborOffsets( "
             "gkg::Neighborhood3d::Type type,"
             "int32_t stride ) const" );

}
