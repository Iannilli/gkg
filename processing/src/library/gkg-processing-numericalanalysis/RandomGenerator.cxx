#include <gkg-processing-numericalanalysis/RandomGenerator.h>
#include <gkg-processing-numericalanalysis/RandomGeneratorImplementation.h>
#include <gkg-processing-numericalanalysis/NumericalAnalysisSelector.h>
#include <gkg-core-exception/Exception.h>


gkg::RandomGenerator::RandomGenerator( gkg::RandomGenerator::Type type )
{

  try
  {

    _randomGeneratorImplementation =
    gkg::NumericalAnalysisSelector::getInstance().getImplementationFactory()->
                              createRandomGeneratorImplementation( this, type );

  }
  GKG_CATCH( "gkg::RandomGenerator::RandomGenerator( "
             "gkg::RandomGenerator::Type type )" );

}


gkg::RandomGenerator::~RandomGenerator()
{

  delete _randomGeneratorImplementation;

}


gkg::RandomGeneratorImplementation* 
gkg::RandomGenerator::getImplementation() const
{

  return _randomGeneratorImplementation;

}
