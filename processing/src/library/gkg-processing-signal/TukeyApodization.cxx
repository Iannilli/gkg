#include <gkg-processing-signal/TukeyApodization.h>
#include <gkg-processing-signal/ApodizationFactory.h>
#include <cmath>


#define TUKEY_ALPHA    0.68


RegisterApodization( gkg, TukeyApodization, float );
RegisterApodization( gkg, TukeyApodization, double );


template < class T >
gkg::TukeyApodization< T >::TukeyApodization(
                                     int32_t count,
                                     typename gkg::Apodization< T >::Type type )
                      : gkg::Apodization< T >( count )
{

  // centered apodization
  size_t i;
  T limit = TUKEY_ALPHA * count / 2.0;

  for ( i = 0; i < this->_coefficients.size(); i++ )
  {

    if ( std::fabs( ( T )this->_coefficients.size() / 2 - i ) < limit )
    {

      this->_coefficients[ i ] = ( T )1.0;

    }
    else
    {

      this->_coefficients[ i ] = ( T )( 0.5 + 0.5 * std::cos( M_PI *
       ( std::fabs( ( double )i - ( double )this->_coefficients.size() / 2.0 ) -
         TUKEY_ALPHA * ( double )this->_coefficients.size() / 2.0 )  /
       ( ( 1 - TUKEY_ALPHA ) * ( double )this->_coefficients.size() ) ) );

    }

  } 


  if ( type == gkg::Apodization< T >::Standard )
  {

    this->rotateCoefficients();

  }

}


template < class T >
gkg::TukeyApodization< T >::~TukeyApodization()
{
}


template < class T >
std::string gkg::TukeyApodization< T >::getName() const
{

  return getStaticName();

}


template < class T >
std::string gkg::TukeyApodization< T >::getStaticName()
{

  return "tukey";

}


template class gkg::TukeyApodization< float >;
template class gkg::TukeyApodization< double >;


#undef TUKEY_ALPHA
