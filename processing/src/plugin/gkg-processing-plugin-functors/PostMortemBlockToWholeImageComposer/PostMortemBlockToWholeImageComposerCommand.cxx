#include <gkg-processing-plugin-functors/PostMortemBlockToWholeImageComposer/PostMortemBlockToWholeImageComposerCommand.h>
#include <gkg-communication-command/CommandFactory.h>
#include <gkg-communication-getopt/Application.h>
#include <gkg-processing-process/Process.h>
#include <gkg-core-io/TypeOf.h>
#include <gkg-core-io/Reader_i.h>
#include <gkg-core-io/Writer_i.h>
#include <gkg-processing-colorimetry/RGBComponent.h>
#include <gkg-processing-resampling/ResamplerFactory.h>
#include <gkg-processing-resampling/SplineResampler.h>
#include <gkg-processing-resampling/PartialVolumingQuickResampler.h>
#include <gkg-processing-transform/RigidTransform3d.h>
#include <gkg-processing-transform/AffineTransform3d.h>
#include <gkg-processing-transform/Rotation3d.h>
#include <gkg-processing-transform/VectorFieldTransform3d.h>
#include <gkg-processing-container/Volume_i.h>
#include <gkg-core-io/Eraser.h>
#include <gkg-core-exception/Exception.h>
#include <iostream>
#include <fstream>
#include <iomanip>


//
// class PostMortemBlockToWholeImageComposerProcess
//

class PostMortemBlockToWholeImageComposerProcess : public gkg::Process
{

  public:

    PostMortemBlockToWholeImageComposerProcess(
                   const std::vector< std::string >& theFileNameFovs,
                   const std::vector< std::string >&
                                    theFileNameFovToReferenceAffineTransform3ds,
                   const std::vector< std::string >&
                              theFileNameFovToReferenceDiffeoDirectTransform3ds,
                   const std::vector< std::string >&
                             theFileNameFovToReferenceDiffeoInverseTransform3ds,
                   const std::string& theFileNameOutput,
                   const std::vector< double >& theFovScalings,
                   const gkg::Vector3d< int32_t >& theReferenceSizes,
                   const gkg::Vector3d< double >& theReferenceResolutions,
                   int32_t theResamplingOrder,
                   double theBackground,
                   bool theAscii,
                   const std::string& theFormat,
                   bool theVerbose );

    const std::vector< std::string >& fileNameFovs;
    const std::vector< std::string >& fileNameFovToReferenceAffineTransform3ds;
    const std::vector< std::string >& 
                                 fileNameFovToReferenceDiffeoDirectTransform3ds;
    const std::vector< std::string >& 
                                fileNameFovToReferenceDiffeoInverseTransform3ds;
    const std::string& fileNameOutput;
    const std::vector< double >& fovScalings;
    const gkg::Vector3d< int32_t >& referenceSizes;
    const gkg::Vector3d< double >& referenceResolutions;
    int32_t resamplingOrder;
    double background;
    bool ascii;
    const std::string& format;
    bool verbose;

  private:

    template < class T >
    static void compose( gkg::Process& process,
                         const std::string& fileNameFov,
                         const gkg::AnalyzedObject&,
                         const std::string& );

    template < class T >
    static void composeItem( T& currentOutputValue,
                             const T& outputValue,
                             double currentFovScaling,
                             const gkg::Rotation3d< float >& rotation3d );


};


PostMortemBlockToWholeImageComposerProcess::
                                     PostMortemBlockToWholeImageComposerProcess(
                   const std::vector< std::string >& theFileNameFovs,
                   const std::vector< std::string >&
                                    theFileNameFovToReferenceAffineTransform3ds,
                   const std::vector< std::string >&
                              theFileNameFovToReferenceDiffeoDirectTransform3ds,
                   const std::vector< std::string >&
                             theFileNameFovToReferenceDiffeoInverseTransform3ds,
                   const std::string& theFileNameOutput,
                   const std::vector< double >& theFovScalings,
                   const gkg::Vector3d< int32_t >& theReferenceSizes,
                   const gkg::Vector3d< double >& theReferenceResolutions,
                   int32_t theResamplingOrder,
                   double theBackground,
                   bool theAscii,
                   const std::string& theFormat,
                   bool theVerbose )
                         : gkg::Process( "Volume" ),
                           fileNameFovs( theFileNameFovs ),
                           fileNameFovToReferenceAffineTransform3ds(
                                 theFileNameFovToReferenceAffineTransform3ds ),
                           fileNameFovToReferenceDiffeoDirectTransform3ds(
                            theFileNameFovToReferenceDiffeoDirectTransform3ds ),
                           fileNameFovToReferenceDiffeoInverseTransform3ds(
                           theFileNameFovToReferenceDiffeoInverseTransform3ds ),
                           fileNameOutput(
                             theFileNameOutput ),
                           fovScalings(
                             theFovScalings ),
                           referenceSizes(
                             theReferenceSizes ),
                           referenceResolutions(
                             theReferenceResolutions ),
                           resamplingOrder(
                             theResamplingOrder ),
                           background(
                             theBackground ),
                           ascii(
                             theAscii ),
                           format(
                             theFormat ),
                           verbose(
                             theVerbose )
{

  try
  {

    registerProcess( "Volume", gkg::TypeOf< int8_t >::getName(),
                     &compose< int8_t > );
    registerProcess( "Volume", gkg::TypeOf< uint8_t >::getName(),
                     &compose< uint8_t > );
    registerProcess( "Volume", gkg::TypeOf< int16_t >::getName(),
                     &compose< int16_t > );
    registerProcess( "Volume", gkg::TypeOf< uint16_t >::getName(),
                     &compose< uint16_t > );
    registerProcess( "Volume", gkg::TypeOf< int32_t >::getName(),
                     &compose< int32_t > );
    registerProcess( "Volume", gkg::TypeOf< uint32_t >::getName(),
                     &compose< uint32_t > );
    registerProcess( "Volume", gkg::TypeOf< int64_t >::getName(),
                     &compose< int64_t > );
    registerProcess( "Volume", gkg::TypeOf< uint64_t >::getName(),
                     &compose< uint64_t > );
    registerProcess( "Volume", gkg::TypeOf< float >::getName(),
                     &compose< float > );
    registerProcess( "Volume", gkg::TypeOf< double >::getName(),
                     &compose< double > );
    registerProcess( "Volume", gkg::TypeOf< gkg::RGBComponent >::getName(),
                     &compose< gkg::RGBComponent > );

  }
  GKG_CATCH( "PostMortemBlockToWholeImageComposerProcess:: "
             "PostMortemBlockToWholeImageComposerProcess( "
             "const std::vector< std::string >& theFileNameFovs, "
             "const std::vector< std::string >& "
             "theFileNameFovToReferenceAffineTransform3ds, "
             "const std::vector< std::string >& "
             "theFileNameFovToReferenceDiffeoDirectTransform3ds, "
             "const std::vector< std::string >& "
             "theFileNameFovToReferenceDiffeoInverseTransform3ds, "
             "const std::string& theFileNameOutput, "
             "const std::vector< float >& theFovScalings, "
             "const gkg::Vector3d< int32_t >& theReferenceSizes, "
             "const gkg::Vector3d< double >& theReferenceResolutions, "
             "int32_t theResamplingOrder, "
             "double theBackground, "
             "bool theAscii, "
             "const std::string& theFormat, "
             "bool theVerbose )" );

}


template < class T >
void 
PostMortemBlockToWholeImageComposerProcess::compose(
                                                gkg::Process& process,
                                                const std::string&,
                                                const gkg::AnalyzedObject&,
                                                const std::string& )
{

  try
  {

    PostMortemBlockToWholeImageComposerProcess&
      postMortemBlockToWholeImageComposerProcess = static_cast<
                       PostMortemBlockToWholeImageComposerProcess& >( process );

    const std::vector< std::string >& fileNameFovs = 
      postMortemBlockToWholeImageComposerProcess.fileNameFovs;
    const std::vector< std::string >& 
      fileNameFovToReferenceAffineTransform3ds =
                                     postMortemBlockToWholeImageComposerProcess.
                                       fileNameFovToReferenceAffineTransform3ds;
    const std::vector< std::string >& 
      fileNameFovToReferenceDiffeoDirectTransform3ds =
                                     postMortemBlockToWholeImageComposerProcess.
                                 fileNameFovToReferenceDiffeoDirectTransform3ds;
    const std::vector< std::string >& 
      fileNameFovToReferenceDiffeoInverseTransform3ds =
                                     postMortemBlockToWholeImageComposerProcess.
                                fileNameFovToReferenceDiffeoInverseTransform3ds;
    const std::string& fileNameOutput = 
      postMortemBlockToWholeImageComposerProcess.fileNameOutput;
    const std::vector< double >& fovScalings = 
      postMortemBlockToWholeImageComposerProcess.fovScalings;
    const gkg::Vector3d< int32_t >& referenceSizes = 
      postMortemBlockToWholeImageComposerProcess.referenceSizes;
    const gkg::Vector3d< double >& referenceResolutions = 
      postMortemBlockToWholeImageComposerProcess.referenceResolutions;
    int32_t resamplingOrder = 
      postMortemBlockToWholeImageComposerProcess.resamplingOrder;
    double background = 
      postMortemBlockToWholeImageComposerProcess.background;
    bool ascii = 
      postMortemBlockToWholeImageComposerProcess.ascii;
    const std::string& format = 
      postMortemBlockToWholeImageComposerProcess.format;
    bool verbose = 
      postMortemBlockToWholeImageComposerProcess.verbose;


    ////////////////////////////////////////////////////////////////////////////
    // sanity checks
    ////////////////////////////////////////////////////////////////////////////

    int32_t fovCount = ( int32_t )fileNameFovs.size();

    if ( ( int32_t )fileNameFovToReferenceAffineTransform3ds.size() !=
         fovCount )
    {

      throw std::runtime_error(
                       "inconsistent number of 3D affine transforms and FOVs" );

    }
    if ( fileNameFovToReferenceDiffeoDirectTransform3ds.empty() !=
         fileNameFovToReferenceDiffeoInverseTransform3ds.empty() )
    {

      throw std::runtime_error(
             "incoherent direct and inverse 3D diffeomorphic transformations" );

    }
    if ( !fileNameFovToReferenceDiffeoDirectTransform3ds.empty() )
    {

      if ( ( int32_t )fileNameFovToReferenceDiffeoDirectTransform3ds.size() !=
           fovCount )
      {

        throw std::runtime_error(
            "inconsistent number of 3D direct diffemorph transforms and FOVs" );

      }
      if ( ( int32_t )fileNameFovToReferenceDiffeoInverseTransform3ds.size() !=
           fovCount )
      {

        throw std::runtime_error(
           "inconsistent number of 3D inverse diffemorph transforms and FOVs" );

      }

    }

    if ( ( int32_t )fovScalings.size() != fovCount )
    {

      throw std::runtime_error( "inconsistent number of scalings and FOVs" );

    }

    if ( ( referenceSizes.x <= 0 ) ||
         ( referenceSizes.y <= 0 ) ||
         ( referenceSizes.z <= 0 ) )
    {

      throw std::runtime_error( "reference sizes must be strictly positive" );

    }
         
    if ( ( referenceResolutions.x <= 0 ) ||
         ( referenceResolutions.y <= 0 ) ||
         ( referenceResolutions.z <= 0 ) )
    {

      throw std::runtime_error(
                            "reference resolutions must be strictly positive" );

    }


    ////////////////////////////////////////////////////////////////////////////
    // allocating output volume
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

      std::cout << "allocating output volume : "
                << std::flush;

    }

    gkg::Volume< T > outputVolume( referenceSizes );
    outputVolume.setResolution( referenceResolutions );
    outputVolume.fill( T( 0 ) );

    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }


    ////////////////////////////////////////////////////////////////////////////
    // allocating a resampler
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

      std::cout << "allocating " << resamplingOrder << "-order resampler : "
                << std::flush;

    }

    gkg::Resampler< T >* 
      resampler = gkg::ResamplerFactory< T >::getInstance().getResampler(
                                                              resamplingOrder );

    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }


    ////////////////////////////////////////////////////////////////////////////
    // looping over FOVs
    ////////////////////////////////////////////////////////////////////////////

    int32_t fovIndex = 0;
    for ( fovIndex = 0; fovIndex < fovCount; fovIndex++ )
    {

      //////////////////////////////////////////////////////////////////////////
      // reading current FOV
      //////////////////////////////////////////////////////////////////////////

      if ( verbose )
      {

        std::cout << "reading current FOV '" << fileNameFovs[ fovIndex ]
                  << "' : "
                  << std::flush;

      }

      gkg::RCPointer< gkg::Volume< T > > fovVolume( new gkg::Volume< T > );
      gkg::Reader::getInstance().read( fileNameFovs[ fovIndex ],
                                       *fovVolume );
      gkg::Vector3d< double > fovResolution;
      fovVolume->getResolution( fovResolution );

      if ( verbose )
      {

        std::cout << "done" << std::endl;

      }


      //////////////////////////////////////////////////////////////////////////
      // reading 3D affine transform
      //////////////////////////////////////////////////////////////////////////

      if ( verbose )
      {

        std::cout << "reading 3D affine transform'"
                  << fileNameFovToReferenceAffineTransform3ds[ fovIndex ]
                  << "' : "
                  << std::flush;

      }

      gkg::RCPointer< gkg::Transform3d< float > > transform3d;
      gkg::Rotation3d< float > rotation3d;
      try
      {

        gkg::RigidTransform3d< float >*
          rigidTransform3d = new gkg::RigidTransform3d< float >;
        std::ifstream 
          is( fileNameFovToReferenceAffineTransform3ds[ fovIndex ].c_str() );
        rigidTransform3d->readTrm( is );
        is.close();
        rotation3d = rigidTransform3d->getRotation3d();
        transform3d.reset( rigidTransform3d );
        std::cout << "rigid transformation detected" << std::endl;

      }
      catch ( std::exception& )
      {

        gkg::AffineTransform3d< float >*
          affineTransform3d =
            new gkg::AffineTransform3d< float >;
        std::ifstream 
          is( fileNameFovToReferenceAffineTransform3ds[ fovIndex ].c_str() );
        affineTransform3d->readTrm( is );
        is.close();
        rotation3d = affineTransform3d->getRotation3d();
        transform3d.reset( affineTransform3d );
        std::cout << "affine transformation detected" << std::endl;

      }


      //////////////////////////////////////////////////////////////////////////
      // reading 3D diffeomorph transform if provided
      //////////////////////////////////////////////////////////////////////////

      gkg::RCPointer< gkg::VectorFieldTransform3d< float > > 
        vectorFieldTransform3d;

      if ( !fileNameFovToReferenceDiffeoDirectTransform3ds.empty() )
      {

        if ( verbose )
        {

          std::cout << "reading 3D diffeomorph transform'"
                    << fileNameFovToReferenceDiffeoDirectTransform3ds[
                                                                      fovIndex ]
                    << " / "
                    << fileNameFovToReferenceDiffeoInverseTransform3ds[
                                                                      fovIndex ]
                    << "' : "
                    << std::flush;

        }

        gkg::Volume< float > directVectorField;
        gkg::Reader::getInstance().read(
                     fileNameFovToReferenceDiffeoDirectTransform3ds[ fovIndex ],
                     directVectorField );
        if ( directVectorField.getSizeT() != 3 )
        {

          throw std::runtime_error( "direct vector field T size must be 3" );

        }

        gkg::Volume< float > inverseVectorField;
        gkg::Reader::getInstance().read(
                    fileNameFovToReferenceDiffeoInverseTransform3ds[ fovIndex ],
                    inverseVectorField );
        if ( inverseVectorField.getSizeT() != 3 )
        {

          throw std::runtime_error( "inverse vector field T size must be 3" );

        }

        vectorFieldTransform3d.reset(
                      new gkg::VectorFieldTransform3d< float >(
                                                         directVectorField,
                                                         inverseVectorField ) );

      }


      //////////////////////////////////////////////////////////////////////////
      // pointing to the current scaling factor
      //////////////////////////////////////////////////////////////////////////

      const double& currentFovScaling = fovScalings[ fovIndex ];
      if ( verbose )
      {

        std::cout << "applied FOV scaling : " << currentFovScaling
                  << std::endl;

      }


      //////////////////////////////////////////////////////////////////////////
      // resetting resampling if it is a spline resampler
      //////////////////////////////////////////////////////////////////////////

      gkg::SplineResampler< T >* splineResampler =
        dynamic_cast< gkg::SplineResampler< T >* >( resampler );
      if ( splineResampler )
      {

        splineResampler->reset();

      }


      //////////////////////////////////////////////////////////////////////////
      // looping over voxel of the output volume
      //////////////////////////////////////////////////////////////////////////


      if ( verbose )
      {

        std::cout << "composing : " << std::flush;

      }
      gkg::Vector3d< int32_t > outputVoxel;
      gkg::Vector3d< float > outputSite;
      gkg::Vector3d< float > fovSite;
      gkg::Vector3d< float > tmpSite;
      T outputValue = T( 0 );

      for ( outputVoxel.z = 0; outputVoxel.z < referenceSizes.z;
            outputVoxel.z++ )
      {

        if ( verbose )
        {

          if ( outputVoxel.z )
          {

            std::cout << gkg::Eraser( 20 );

          }
          std::cout << std::setw( 5 ) << outputVoxel.z + 1
                    << " / "
                    << std::setw( 5 ) << referenceSizes.z
                    << " slices" 
                    << std::flush;

        }

        outputSite.z = ( outputVoxel.z + 0.5 ) * referenceResolutions.z;
        for ( outputVoxel.y = 0; outputVoxel.y < referenceSizes.y;
              outputVoxel.y++ )
        {

          outputSite.y = ( outputVoxel.y + 0.5 ) * referenceResolutions.y;
          for ( outputVoxel.x = 0; outputVoxel.x < referenceSizes.x;
                outputVoxel.x++ )
          {

            outputSite.x = ( outputVoxel.x + 0.5 ) * referenceResolutions.x;
            outputValue = ( T )0;
/*
            resampler->resample( *fovVolume,
                                 *transform3d,
                                 background,
                                 outputSite,
                                 outputValue,
                                 &fovResolution );
*/

            if ( vectorFieldTransform3d.isNull() )
            {

              transform3d->getInverse( outputSite, fovSite );

            }
            else
            {

              vectorFieldTransform3d->getInverse( outputSite, tmpSite );
              transform3d->getInverse( tmpSite, fovSite );

            }
            gkg::PartialVolumingQuickResampler< T >::getInstance().resample(
                      *fovVolume,
                      background,
                      fovSite,
                      outputValue,
                      &fovResolution );


            composeItem( outputVolume( outputVoxel ),
                         outputValue,
                         currentFovScaling,
                         rotation3d );

          }

        }

      }

      if ( verbose )
      {

        std::cout << std::endl;

      }

    }


    ////////////////////////////////////////////////////////////////////////////
    // writing output volume
    ////////////////////////////////////////////////////////////////////////////

    if ( verbose )
    {

      std::cout << "writing output volume '"
                << fileNameOutput
                << "' : " << std::flush;

    }
    gkg::Writer::getInstance().write( fileNameOutput,
                                      outputVolume,
                                      ascii,
                                      format );
    if ( verbose )
    {

      std::cout << "done" << std::endl;

    }

  }
  GKG_CATCH( "template < class T > "
             "void "
             "PostMortemBlockToWholeImageComposerProcess::compose( "
             "gkg::Process& process, "
             "const std::string& floatingName, "
             "const gkg::AnalyzedObject&, "
             "const std::string& )" );

}


template < class T >
void PostMortemBlockToWholeImageComposerProcess::composeItem( 
                               T& currentOutputValue,
                               const T& outputValue,
                               double currentFovScaling,
                               const gkg::Rotation3d< float >& /*rotation3d*/ )
{

  try
  {

    if ( ( double )outputValue * currentFovScaling >
         ( double )currentOutputValue )
    {

      currentOutputValue = ( T )( ( double )outputValue * currentFovScaling );

    }

  }
  GKG_CATCH( "template < class T > "
             "void PostMortemBlockToWholeImageComposerProcess::composeItem( "
             "T& currentValue, "
             "const T& outputValue, "
             "double currentFovScaling, "
             "const Rotation3d< float >& rotation3d )" );

}


template <>
void
PostMortemBlockToWholeImageComposerProcess::composeItem(
                                   gkg::RGBComponent& currentOutputValue,
                                   const gkg::RGBComponent& outputValue,
                                   double currentFovScaling,
                                   const gkg::Rotation3d< float >& rotation3d )
{

  try
  {


    gkg::Vector3d< float > outputValueAsVector( ( float )outputValue.r,
                                                ( float )outputValue.g,
                                                ( float )outputValue.b );
    gkg::Vector3d< float > rotatedOutputValueAsVector;
    rotation3d.getDirect( outputValueAsVector,
                          rotatedOutputValueAsVector );

    gkg::RGBComponent
      rotatedOutputValue( ( uint8_t )std::abs( rotatedOutputValueAsVector.x ),
                          ( uint8_t )std::abs( rotatedOutputValueAsVector.y ),
                          ( uint8_t )std::abs( rotatedOutputValueAsVector.z ) );
          


    if ( ( ( ( double )rotatedOutputValue.r * ( double )rotatedOutputValue.r +
             ( double )rotatedOutputValue.g * ( double )rotatedOutputValue.g +
             ( double )rotatedOutputValue.b * ( double )rotatedOutputValue.b ) *
           currentFovScaling * currentFovScaling ) >
         ( ( double )currentOutputValue.r *
           ( double )currentOutputValue.r +
           ( double )currentOutputValue.g *
           ( double )currentOutputValue.g +
           ( double )currentOutputValue.b *
           ( double )currentOutputValue.b ) )
    {

      // we need here to flip R,G,B coordinates according to the following
      // trasnformation:
      //   r' = g
      //   g' = b
      //   b' = r

      currentOutputValue.r =
        ( uint8_t )( std::min( 255.0,
                               ( double )rotatedOutputValue.g *
                               currentFovScaling ) );
      currentOutputValue.g =
        ( uint8_t )( std::min( 255.0,
                               ( double )rotatedOutputValue.b *
                               currentFovScaling ) );
      currentOutputValue.b =
        ( uint8_t )( std::min( 255.0,
                               ( double )rotatedOutputValue.r *
                               currentFovScaling ) );

    }

  }
  GKG_CATCH( "template <> "
             "void "
             "PostMortemBlockToWholeImageComposerProcess::composeItem( "
             "gkg::RGBComponent& currentOutputValue, "
             "const gkg::RGBComponent& outputValue, "
             "double currentFovScaling, "
             "const gkg::Rotation3d< float >& rotation3d )" );

}


//
//   PostMortemBlockToWholeImageComposerCommand
//

gkg::PostMortemBlockToWholeImageComposerCommand::
                                     PostMortemBlockToWholeImageComposerCommand(
                                                              int32_t argc,
                                                              char* argv[],
                                                              bool loadPlugin,
                                                              bool removeFirst )
                                                : gkg::Command( argc, argv,
                                                                loadPlugin,
                                                                removeFirst )
{

  try
  {

    parse();

  }
  GKG_CATCH( "gkg::PostMortemBlockToWholeImageComposerCommand::"
             "PostMortemBlockToWholeImageComposerCommand( "
             "int32_t argc, char* argv[], bool loadPlugin, "
             "bool removeFirst )" );

}


gkg::PostMortemBlockToWholeImageComposerCommand::
                                     PostMortemBlockToWholeImageComposerCommand(
                      const std::vector< std::string >& fileNameFovs,
                      const std::vector< std::string >&
                                       fileNameFovToReferenceAffineTransform3ds,
                      const std::vector< std::string >&
                                 fileNameFovToReferenceDiffeoDirectTransform3ds,
                      const std::vector< std::string >&
                                fileNameFovToReferenceDiffeoInverseTransform3ds,
                      const std::string& fileNameOutput,
                      const std::vector< double >& fovScalings,
                      const gkg::Vector3d< int32_t >& referenceSizes,
                      const gkg::Vector3d< double >& referenceResolutions,
                      int32_t resamplingOrder,
                      double background,
                      bool ascii,
                      const std::string& format,
                      bool verbose )
                                                : gkg::Command()
{

  try
  {

    execute( fileNameFovs,
             fileNameFovToReferenceAffineTransform3ds,
             fileNameFovToReferenceDiffeoDirectTransform3ds,
             fileNameFovToReferenceDiffeoInverseTransform3ds,
             fileNameOutput,
             fovScalings,
             referenceSizes,
             referenceResolutions,
             resamplingOrder,
             background,
             ascii,
             format,
             verbose );

  }
  GKG_CATCH( "gkg::PostMortemBlockToWholeImageComposerCommand::"
             "PostMortemBlockToWholeImageComposerCommand( "
             "const std::vector< std::string >& fileNameFovs, "
             "const std::vector< std::string >& "
             "fileNameFovToReferenceAffineTransform3ds, "
             "const std::vector< std::string >& "
             "fileNameFovToReferenceDiffeoDirectTransform3ds, "
             "const std::vector< std::string >& "
             "fileNameFovToReferenceDiffeoInverseTransform3ds, "
             "const std::string& fileNameOutput, "
             "const std::vector< double >& fovScalings, "
             "const gkg::Vector3d< int32_t >& referenceSizes, "
             "const gkg::Vector3d< double >& referenceResolutions, "
             "int32_t resamplingOrder, "
             "double background, "
             "bool ascii, "
             "const std::string& format, "
             "bool verbose )" );

}


gkg::PostMortemBlockToWholeImageComposerCommand::
                                    PostMortemBlockToWholeImageComposerCommand(
                                             const gkg::Dictionary& parameters )
                                                : gkg::Command( parameters )
{

  try
  {

    DECLARE_VECTOR_OF_STRINGS_PARAMETER( parameters,
                                         std::vector< std::string >, 
                                         fileNameFovs );
    DECLARE_VECTOR_OF_STRINGS_PARAMETER(
                                     parameters,
                                     std::vector< std::string >, 
                                     fileNameFovToReferenceAffineTransform3ds );
    DECLARE_VECTOR_OF_STRINGS_PARAMETER(
                               parameters,
                               std::vector< std::string >, 
                               fileNameFovToReferenceDiffeoDirectTransform3ds );
    DECLARE_VECTOR_OF_STRINGS_PARAMETER(
                              parameters,
                              std::vector< std::string >, 
                              fileNameFovToReferenceDiffeoInverseTransform3ds );
    DECLARE_STRING_PARAMETER( parameters, std::string, fileNameOutput );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER( parameters,
                                           std::vector< double >,
                                           fovScalings );
    DECLARE_VECTOR_OF_INTEGERS_PARAMETER( parameters,
                                          std::vector< int32_t >,
                                          referenceSizes );
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER( parameters,
                                           std::vector< double >,
                                           referenceResolutions );
    DECLARE_INTEGER_PARAMETER( parameters, int32_t, resamplingOrder );
    DECLARE_FLOATING_PARAMETER( parameters, double, background );
    DECLARE_BOOLEAN_PARAMETER( parameters, bool, ascii );
    DECLARE_STRING_PARAMETER( parameters, std::string, format );
    DECLARE_BOOLEAN_PARAMETER( parameters, bool, verbose );

    gkg::Vector3d< int32_t > vector3dOfReferenceSizes( referenceSizes[ 0 ],
                                                       referenceSizes[ 1 ],
                                                       referenceSizes[ 2 ] );
    gkg::Vector3d< double >
      vector3dOfReferenceResolutions( referenceResolutions[ 0 ],
                                      referenceResolutions[ 1 ],
                                      referenceResolutions[ 2 ] );

    execute( fileNameFovs,
             fileNameFovToReferenceAffineTransform3ds,
             fileNameFovToReferenceDiffeoDirectTransform3ds,
             fileNameFovToReferenceDiffeoInverseTransform3ds,
             fileNameOutput,
             fovScalings,
             vector3dOfReferenceSizes,
             vector3dOfReferenceResolutions,
             resamplingOrder,
             background,
             ascii,
             format,
             verbose );

  }
  GKG_CATCH( "gkg::PostMortemBlockToWholeImageComposerCommand::"
             "PostMortemBlockToWholeImageComposerCommand( "
             "const gkg::Dictionary& parameters )" );

}


gkg::PostMortemBlockToWholeImageComposerCommand::
                                   ~PostMortemBlockToWholeImageComposerCommand()
{
}


std::string gkg::PostMortemBlockToWholeImageComposerCommand::getStaticName()
{

  try
  {

    return "PostMortemBlockToWholeImageComposer";

  }
  GKG_CATCH( "std::string gkg::PostMortemBlockToWholeImageComposerCommand::"
             "getStaticName()" );

}


void gkg::PostMortemBlockToWholeImageComposerCommand::parse()
{

  try
  {

    std::vector< std::string > fileNameFovs;
    std::vector< std::string > fileNameFovToReferenceAffineTransform3ds;
    std::vector< std::string > fileNameFovToReferenceDiffeoDirectTransform3ds;
    std::vector< std::string > fileNameFovToReferenceDiffeoInverseTransform3ds;
    std::string fileNameOutput;
    std::vector< double > fovScalings;
    std::vector< int32_t > vectorOfReferenceSizes;
    std::vector< double > vectorOfReferenceResolutions;
    int32_t resamplingOrder = 3;
    double background = 0.0;
    bool ascii = false;
    std::string format;
    bool verbose = false;


    gkg::Application application( _argc, _argv,
                                  "Post-mortem block to whole brain composer",
                                  _loadPlugin );
    application.addSeriesOption( "-i",
                                 "Block FOV filename list",
                                 fileNameFovs,
                                 1 );
    application.addSeriesOption( "-ta",
                                 "Block to whole brain 3D affine transform "
                                 "filename list",
                                 fileNameFovToReferenceAffineTransform3ds,
                                 1 );
    application.addSeriesOption( "-tdd",
                                 "Block to whole brain 3D direct diffeomorph "
                                 "transform filename list",
                                 fileNameFovToReferenceDiffeoDirectTransform3ds,
                                 0 );
    application.addSeriesOption(
                                "-tdi",
                                "Block to whole brain 3D inverse diffeomorph "
                                "transform filename list",
                                fileNameFovToReferenceDiffeoInverseTransform3ds,
                                0 );
    application.addSingleOption( "-o",
                                 "Output volume filename",
                                 fileNameOutput );
    application.addSeriesOption( "-s",
                                 "FOV scaling list",
                                 fovScalings,
                                 1 );
    application.addSeriesOption( "-referenceSizes",
                                 "Reference sizes along X, Y and Z axis",
                                 vectorOfReferenceSizes,
                                 3, 3 );
    application.addSeriesOption( "-referenceResolutions",
                                 "Reference resolutions along X, Y and Z axis",
                                 vectorOfReferenceResolutions,
                                 3, 3 );
    application.addSingleOption( "-resamplingOrder",
                                 "Resampling order (0 to 7) (default=1)",
                                 resamplingOrder,
                                 true );
    application.addSingleOption( "-b",
                                 "Background value (default=0.0)",
                                 background,
                                 true );
    application.addSingleOption( "-format",
                                 "Ouput volume format (default=input)",
                                 format,
                                 true );
    application.addSingleOption( "-ascii",
                                 "Save ouput volume in ASCII mode",
                                 ascii,
                                 true );
    application.addSingleOption( "-verbose",
                                 "Show as much information as possible",
                                 verbose,
                                 true );

    application.initialize();


    gkg::Vector3d< int32_t > referenceSizes( vectorOfReferenceSizes[ 0 ],
                                             vectorOfReferenceSizes[ 1 ],
                                             vectorOfReferenceSizes[ 2 ] );
    gkg::Vector3d< double >
      referenceResolutions( vectorOfReferenceResolutions[ 0 ],
                            vectorOfReferenceResolutions[ 1 ],
                            vectorOfReferenceResolutions[ 2 ] );

    execute( fileNameFovs,
             fileNameFovToReferenceAffineTransform3ds,
             fileNameFovToReferenceDiffeoDirectTransform3ds,
             fileNameFovToReferenceDiffeoInverseTransform3ds,
             fileNameOutput,
             fovScalings,
             referenceSizes,
             referenceResolutions,
             resamplingOrder,
             background,
             ascii,
             format,
             verbose );

  }
  GKG_CATCH_FUNCTOR( "void gkg::PostMortemBlockToWholeImageComposerCommand::"
                     "parse()" );

}


void gkg::PostMortemBlockToWholeImageComposerCommand::execute(
     const std::vector< std::string >& fileNameFovs,
     const std::vector< std::string >& fileNameFovToReferenceAffineTransform3ds,
     const std::vector< std::string >& 
                                 fileNameFovToReferenceDiffeoDirectTransform3ds,
     const std::vector< std::string >& 
                                fileNameFovToReferenceDiffeoInverseTransform3ds,
     const std::string& fileNameOutput,
     const std::vector< double >& fovScalings,
     const Vector3d< int32_t >& referenceSizes,
     const Vector3d< double >& referenceResolutions,
     int32_t resamplingOrder,
     double background,
     bool ascii,
     const std::string& format,
     bool verbose )
{

  try
  {

    ////////////////////////////////////////////////////////////////////////////
    // launching registration process
    ////////////////////////////////////////////////////////////////////////////

    PostMortemBlockToWholeImageComposerProcess
      postMortemBlockToWholeImageComposerProcess(
                                fileNameFovs,
                                fileNameFovToReferenceAffineTransform3ds,
                                fileNameFovToReferenceDiffeoDirectTransform3ds,
                                fileNameFovToReferenceDiffeoInverseTransform3ds,
                                fileNameOutput,
                                fovScalings,
                                referenceSizes,
                                referenceResolutions,
                                resamplingOrder,
                                background,
                                ascii,
                                format,
                                verbose );
    postMortemBlockToWholeImageComposerProcess.execute( fileNameFovs[ 0 ] );

  }
  GKG_CATCH( "void gkg::PostMortemBlockToWholeImageComposerCommand::execute( "
             "const std::vector< std::string >& fileNameFovs, "
             "const std::vector< std::string >& "
             "fileNameFovToReferenceAffineTransform3ds, "
             "const std::vector< std::string >& "
             "fileNameFovToReferenceDiffeoDirectTransform3ds, "
             "const std::vector< std::string >& "
             "fileNameFovToReferenceDiffeoInverseTransform3ds, "
             "const std::string& fileNameOutput, "
             "const std::vector< double >& fovScalings, "
             "const Vector3d< int32_t >& referenceSizes, "
             "const Vector3d< double >& referenceResolutions, "
             "int32_t resamplingOrder, "
             "double background, "
             "bool ascii, "
             "const std::string& format, "
             "bool verbose )" );

}


RegisterCommandCreator(
    PostMortemBlockToWholeImageComposerCommand,
    DECLARE_VECTOR_OF_STRINGS_PARAMETER_HELP( fileNameFovs ) +
    DECLARE_VECTOR_OF_STRINGS_PARAMETER_HELP( 
                                    fileNameFovToReferenceAffineTransform3ds ) +
    DECLARE_VECTOR_OF_STRINGS_PARAMETER_HELP( 
                              fileNameFovToReferenceDiffeoDirectTransform3ds ) +
    DECLARE_VECTOR_OF_STRINGS_PARAMETER_HELP( 
                             fileNameFovToReferenceDiffeoInverseTransform3ds ) +
    DECLARE_STRING_PARAMETER_HELP( fileNameOutput ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( fovScalings ) +
    DECLARE_VECTOR_OF_INTEGERS_PARAMETER_HELP( referenceSizes ) +
    DECLARE_VECTOR_OF_FLOATINGS_PARAMETER_HELP( rReferenceResolutions ) +
    DECLARE_INTEGER_PARAMETER_HELP( resamplingOrder ) +
    DECLARE_FLOATING_PARAMETER_HELP( background ) +
    DECLARE_BOOLEAN_PARAMETER_HELP( ascii ) +
    DECLARE_STRING_PARAMETER_HELP( format ) +
    DECLARE_BOOLEAN_PARAMETER_HELP( verbose ) );
