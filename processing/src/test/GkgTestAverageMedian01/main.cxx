#include <gkg-processing-algobase/AverageFilter_i.h>
#include <gkg-processing-algobase/MedianFilter_i.h>
#include <iostream>


int main()
{

  std::vector< int32_t > in1( 10 );
  in1[ 0 ] = -5;
  in1[ 1 ] = 10;
  in1[ 2 ] = 3;
  in1[ 3 ] = -16;
  in1[ 4 ] = 7;
  in1[ 5 ] = 2;
  in1[ 6 ] = 4;
  in1[ 7 ] = 9;
  in1[ 8 ] = 18;
  in1[ 9 ] = -1;

  gkg::AverageFilter< std::vector< int32_t >, double > averageFilter1;
  double average1 = 0.0;
  averageFilter1.filter( in1, average1 );

  gkg::MedianFilter< std::vector< int32_t >, float > medianFilter1;
  float median1 = 0.0;
  medianFilter1.filter( in1, median1 );

  std::cout << "average1 : " << average1 << std::endl;
  std::cout << "median1  : " << median1 << std::endl;

  std::vector< std::complex< float > > in2( 10 );
  in2[ 0 ] = std::complex< float >( 1.0, 2.0 );
  in2[ 1 ] = std::complex< float >( -3.0, 10.0 );
  in2[ 2 ] = std::complex< float >( 5.0, -2.0 );
  in2[ 3 ] = std::complex< float >( -7.0, 0.0 );
  in2[ 4 ] = std::complex< float >( 0.0, 7.0 );
  in2[ 5 ] = std::complex< float >( -15.0, 1.0 );
  in2[ 6 ] = std::complex< float >( 2.0, 3.0 );
  in2[ 7 ] = std::complex< float >( 4.0, -4.0 );
  in2[ 8 ] = std::complex< float >( 0.5, 2.5 );
  in2[ 9 ] = std::complex< float >( -6.0, 5.7 );

  gkg::AverageFilter< std::vector< std::complex< float > >,
                      std::complex< double > > averageFilter2;
  std::complex< double > average2( 0.0, 0.0 );
  averageFilter2.filter( in2, average2 );

  gkg::MedianFilter< std::vector< std::complex< float > >,
                     std::complex< float > > medianFilter2;
  std::complex< float > median2 = 0.0;
  medianFilter2.filter( in2, median2 );

  std::cout << "average2 : " << average2 << std::endl;
  std::cout << "median2  : " << median2 << std::endl;

  return EXIT_SUCCESS;

}
