#include <gkg-communication-plugin/PluginLoader.h>
#include <gkg-processing-coordinates/CartesianVoxelSampler.h>
#include <gkg-core-exception/Exception.h>
#include <iostream>


int main( int /*argc*/, char* argv[] )
{


  int result = EXIT_SUCCESS;

  try
  {

    gkg::PluginLoader::getInstance().load();

    // case of 27 point(s) with (1.0, 1.0, 1.0) voxel resolution
    
    std::cout << std::endl
              << "case of 27 point(s) with (1.0, 1.0, 1.0) voxel resolution"
              << std::endl
              << "========================================================="
              << std::endl;
    gkg::CartesianVoxelSampler< float >
      voxelSampler27_1_1_1( gkg::Vector3d< double >( 1.0, 1.0, 1.0 ), 27 );

    gkg::CartesianVoxelSampler< float >::const_iterator
      o = voxelSampler27_1_1_1.begin(),
      oe = voxelSampler27_1_1_1.end();
    while ( o != oe )
    {

      std::cout << *o << std::endl;
      ++ o;

    }

    // case of 27 point(s) with (2.0, 1.0, 3.0) voxel resolution
    std::cout << std::endl
              << "case of 27 point(s) with (2.0, 1.0, 3.0) voxel resolution"
              << std::endl
              << "========================================================="
              << std::endl;
    gkg::CartesianVoxelSampler< float >
      voxelSampler27_2_1_3( gkg::Vector3d< double >( 2.0, 1.0, 3.0 ), 27 );

    o = voxelSampler27_2_1_3.begin(),
    oe = voxelSampler27_2_1_3.end();
    while ( o != oe )
    {

      std::cout << *o << std::endl;
      ++ o;

    }


    // case of 53 point(s) with (1.0, 1.0, 1.0) voxel resolution
    std::cout << std::endl
              << "case of 53 point(s) with (1.0, 1.0, 1.0) voxel resolution"
              << std::endl
              << "========================================================="
              << std::endl;
    gkg::CartesianVoxelSampler< float >
      voxelSampler53_1_1_1( gkg::Vector3d< double >( 1.0, 1.0, 1.0 ), 53 );

    o = voxelSampler53_1_1_1.begin(),
    oe = voxelSampler53_1_1_1.end();
    while ( o != oe )
    {

      std::cout << *o << std::endl;
      ++ o;

    }


  }
  GKG_CATCH_COMMAND( result );

  return result;


}
