#!/bin/sh

$HOME/bin/webgl \
--docroot . \
--approot . \
--http-port 8080 \
--http-addr 0.0.0.0 $@
